import React, {useEffect, useState} from 'react';
import {Switch, Route, Redirect} from "react-router-dom";
import AOS from "aos";
import axios from "axios";
import AppNav from "./components/AppNav";
import HomePage from "./views/HomePage";
import Footer from "./components/Footer";
import ProductsPage from "./views/ProductsPage";
import NftProductsPage from "./views/NftProductsPage";
import ProductDetail from "./views/ProductDetail";
import NftProductDetail from "./views/NftProductDetail";
import OurDifferences from "./components/home/OurDifferences";
import Aboutus from "./views/Aboutus";
import ContactUs from "./views/ContactUs";
import Cart from "./views/Cart";
import Checkout from "./views/Checkout";
import AddProduct from './views/AddProduct';
import ProductUpdate from './views/ProductUpdate';
import SellerProducts from './views/SellerProducts';
import UserProfile from './views/UserProfile';
import UserProfileUpdate from './views/UserUpdate';
import Transactions from './views/Transactions';
import Gallery from './views/Gallery';
import Wishlist from './views/Wishlist';
import CreateWishlist from './views/CreateWishlist';
import ViewWIshlist from './views/ViewWishlist'
import BecomeSeller from './views/BecomeSeller';
import SellerNotifications from './views/SellerNotifications';
import {ImmutableXClient, Link as imxlink, ImmutableMethodResults} from '@imtbl/imx-sdk';

// import Payment from './views/Payment';

import {loadStripe} from "@stripe/stripe-js";
import {Elements} from "@stripe/react-stripe-js";
const stripePromise = loadStripe("pk_test_51KWhwZLKdDbFtRftKTbEUhKw9hfqByFlUNcTHdkyDhfMseHOIjcLu6AZAt962devq2Cc5VUTvVxsApXP61LbhtwB004pKeiOvh");

// Link SDK
const link = new imxlink(process.env.REACT_APP_ROPSTEN_LINK_URL);

function App() {
  const [wallet, setWallet] = useState('undefined');
  const [balance, setBalance] = useState(null);
  const [client, setClient] = useState(Object);
  const [history, setHistory] = useState(null);
  useEffect(() => {
    AOS.init();
    AOS.refresh();
    buildIMX();
  }, []);

  // initialise an Immutable X Client to interact with apis more easily
  const buildIMX = async () => {
    const publicApiUrl = process.env.REACT_APP_ROPSTEN_ENV_URL ?? '';
    setClient(await ImmutableXClient.build({publicApiUrl}))
  }
  const setupAccount = async () => {
    const {address, starkPublicKey} = await link.setup({});
    localStorage.setItem('WALLET_ADDRESS', address);
    localStorage.setItem('STARK_PUBLIC_KEY', starkPublicKey);
    localStorage.setItem('SHOW_BALANCE', true);
    const balance = await client.getBalance({user: address, tokenAddress: 'eth'});
    localStorage.setItem('WALLET_BALANCE', balance.balance.toString());
    window.location.reload();
  }
  const getBalance = async () => {
    const address = localStorage.getItem('WALLET_ADDRESS');
    const options = {
      method: 'GET',
      url: `${process.env.REACT_APP_ROPSTEN_ENV_URL}/balances/${address}`,
      headers: {'Content-Type': 'application/json'}
    };
    
    axios.request(options).then(function (response) {
      // console.log(response.data);
      const balance = response.data.imx;
      localStorage.setItem('WALLET_BALANCE', balance.toString());
    }).catch(function (error) {
      console.error(error);
    });
  }
  return (
    <div className="App">
      <AppNav setupAccount={setupAccount} getBalance={getBalance} client={client} link={link} />
      <Switch>
        <Route exact path='/dashboard'>
          <Redirect to='/dashboard/home' />
        </Route>
        <Route path="/dashboard/home" render={() => <HomePage />} />
        <Route path="/dashboard/profile" render={(props) => <UserProfile client={client} props={props} />} />
        <Route path="/dashboard/profileEdit/:action" component={UserProfileUpdate} />
        <Route path="/dashboard/products" render={(props) => <ProductsPage props={props}/>} />
        <Route path="/dashboard/nfts" render={() => <NftProductsPage />} />
        <Route path="/dashboard/contact-us" render={() => <ContactUs />} />
        <Route path="/dashboard/cart" render={() => <Cart />} />
        <Route path="/dashboard/product-detail/:product_id" render={() => <ProductDetail />} />
        <Route path="/dashboard/nft-product-detail/:product_id" render={() => <NftProductDetail link={link} />} />
        <Route path="/dashboard/about" render={() => <Aboutus />} />
        <Route path="/dashboard/our-diff" render={() => <OurDifferences />} />
        <Route path="/dashboard/add-product" render={() => <AddProduct link={link} />} />
        <Route path="/dashboard/update-product/:product_id" component={ProductUpdate} />
        <Route path="/dashboard/seller-products" render={(props) => <SellerProducts />} />
        <Route path="/dashboard/seller-profile/:profile_id" render={(props) => <UserProfile props={props} />} />
        <Route path="/dashboard/transactions" render={(props) => <Transactions link={link} />} />
        <Route path="/dashboard/gallery" render={(props) => <Gallery />} />
        <Route path="/dashboard/create-wishlist" render={(props) => <CreateWishlist />} />
        <Route path="/dashboard/update-wishlist/:wishlist_id" render={(props) => <CreateWishlist isUpdate={true} />} />
        <Route path="/dashboard/view-wishlist/:wishlist_id" render={(props) => <ViewWIshlist />} />
        <Route path="/dashboard/wishlist" render={(props) => <Wishlist />} />
        <Route path="/dashboard/become-seller" render={(props) => <BecomeSeller />} />
        <Route path="/dashboard/seller-notifications" render={(props) => <SellerNotifications />} />
        
        <Route path="/dashboard/checkout" render={(props) => {
          return (
            <Elements stripe={stripePromise}>
              <Checkout />
            </Elements>
          )
        }} />
      </Switch >
      <Footer />
    </div >
  );
}

export default App;
