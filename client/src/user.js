const LOGIN_STATUS = "login-status";
const LOG_OUT = "log-out";
const USER_DATA_UPDATE = "user-data-update";
const PRODUCT_SEARCH_STRING__UPDATE = "update-product-search";
const UPADTE_CART_COUNT = "update-cart-count";
const ADD_SELLER_TO_ROLES = "add-seller-to-roles";

const initialState = {
    isLogged: false,
    name: "",
    roles: [],
    token: "",
    productSearchString: '',
    cartCount: 0
};

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case LOGIN_STATUS:
            return Object.assign({}, state, { isLogged: action.payload });

        case LOG_OUT:
            return Object.assign({}, state, {
                isLogged: action.payload,
                name: "",
                roles: [],
                token: "",
                productSearchString: '',
                cartCount: 0
            });

        case USER_DATA_UPDATE:
            let dupliacteState = Object.assign({}, state)
            if (action.payload) {
                return Object.assign({},
                    state,
                    {
                        id: parseInt(action.payload.userId) || "",
                        name: action.payload.userName || "",
                        token: action.payload.accessToken || "",
                        roles: action.payload.roles || []
                    });
            }
            return dupliacteState;

        case PRODUCT_SEARCH_STRING__UPDATE:
            return Object.assign({}, state, { productSearchString: action.payload });

        case UPADTE_CART_COUNT:
            return Object.assign({}, state, { cartCount: action.payload });

        case ADD_SELLER_TO_ROLES:
            let updatedRoles = ["Seller"];;

            if (state && state.roles && Array.isArray(state.roles)) {
                updatedRoles.concat(state.roles);
            }

            return Object.assign({}, state, { roles: updatedRoles });
        
        default:
            return state;
    }
}

export function loginStatusUpdate(status) {
    return {
        type: LOGIN_STATUS,
        payload: status
    };
}

export function logoutStatusUpdate(status) {
    return {
        type: LOG_OUT,
        payload: status
    };
}

export function userDataUpdate(data) {
    return {
        type: USER_DATA_UPDATE,
        payload: data
    };
}

export function productSearchStringUpdate(status) {
    return {
        type: PRODUCT_SEARCH_STRING__UPDATE,
        payload: status
    };
}

export function updateCartCount(count) {
    return {
        type: UPADTE_CART_COUNT,
        payload: count
    };
}

export function addSellerToRoles() {
    return {
        type: ADD_SELLER_TO_ROLES
    };
}