import React, {useState, useEffect} from "react";
import {withAlert} from 'react-alert';
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import { Button, Col, Row } from "react-bootstrap";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash, faPencil, faEye } from '@fortawesome/free-solid-svg-icons';
import Tooltip from '@material-ui/core/Tooltip';
import ApiRequest from "../api_service/api_requests";
import Pagination from '@material-ui/core/TablePagination';
import "./styles.css";
import "bootstrap/dist/css/bootstrap.css";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";

import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import MUILink from '@material-ui/core/Link';

const Wishlist = props => {
  const [Wishlists, setWishlists] = useState([]);
  const [loading, setLoading] = useState(true);
  const [message, setMessage] = useState("Data fetching....")

  const fetchWishlist = () => {
    ApiRequest.getWishlists()
      .then((res) => {
        setWishlists(res.data);
        setLoading(false);

        // console.log(res);
        if (res.message === "no wishlist found") {
          setMessage("Found no wishlist.")
        }
      });
  }

  useEffect(() => {
    fetchWishlist();
  }, []);

  const deleteWishlist = wishlistId => {
    if (wishlistId) {
      ApiRequest.deleteWishlist(wishlistId)
        .then(res => {
          if (res && res.statusCode && res.statusCode === 200) {
            fetchWishlist();
            props.alert.success(res.message || "Delete Wishlist success");
          } else {
            props.alert.error(res.message || "Failed to deete Wishlist");
          }
        })
        .catch((err) => {
          // console.log('Delete Wishlist api error, ', err);
        })
    }
  }

  const columns = [
    {
      text: "#",
      formatter: (cell, row, ind) => {
        return <div className="serial">{ind + 1}</div>
      },
      headerStyle: (colum, colIndex) => {
        return {width: '50px', backgroundColor: "#131c20", color: "white"};
      }
    },
    {
      dataField: "wishListCategories",
      text: "Category",
      formatter: (cell) => {
        let value = [];

        if (cell && cell.length) {
          cell.forEach(object => {
            if (object && object.categoryName) {
              value.push(object.categoryName);
            }
          });
        }

        return value && value.length ? value.toString(', ') : '-';
      }
    },
    {
      dataField: "wishListSubCategories",
      text: "SubCategory",
      formatter: (cell) => {
        let value = [];

        if (cell && cell.length) {
          cell.forEach(object => {
            if (object && object.subCategoryName) {
              value.push(object.subCategoryName);
            }
          });
        }

        return value && value.length ? value.toString(', ') : '-';
      }
    },
    {
      dataField: "wishListDescription",
      text: "Description",
      formatter: (cell, row) => (
        <span className="text">{cell || 'NA'}</span>
      ),
    },
    {
      dataField: "wishListId",
      text: "Actions",
      formatter: (cell, row) => (
        <span>
          <Button className='btn btn-primary mr-1'
            as={Link}
            to={`/dashboard/update-wishlist/${cell || 0}`}>
            <FontAwesomeIcon icon={faPencil} />
          </Button>
          <Button className='btn btn-primary mr-1'
            as={Link}
            to={`/dashboard/view-wishlist/${cell || 0}`}>
            <FontAwesomeIcon icon={faEye} />
          </Button>
          <Button className='btn btn-primary mr-1' onClick={event => {
            event.preventDefault();
            deleteWishlist(cell);
          }}>
            <FontAwesomeIcon icon={faTrash} />
          </Button>
        </span>
      ),
    }
  ];

  // console.log(Wishlists);

  const [filters, setFilters] = useState({
    pageSize: 10,
    pageIndex: 1,
    startIndex: 0,
    endIndex: 9
  })

  const handleChangePage = (event, newPage) => {
    let filterOptions = filters;
    filterOptions.pageIndex = newPage + 1;
    filterOptions.startIndex = newPage * filters.pageSize;
    filterOptions.endIndex = ((newPage + 1) * filters.pageSize) - 1;
    setFilters({...filterOptions});
  };

  const handleChangeRowsPerPage = (event) => {
    let filterOptions = filters;
    filterOptions.pageIndex = 1;
    filterOptions.pageSize = event.target.value;
    filterOptions.startIndex = 0;
    filterOptions.endIndex = filterOptions.pageSize - 1;
    setFilters({...filterOptions});
  };

  // console.log(filters);

  const renderProducts = () => {
    if (!loading) {
      if (Wishlists && Wishlists.length > 0) {
        return (
          <div>
            <div className="list-pagination">
              <Pagination
                rowsPerPageOptions={[5, 10]}
                component="div"
                count={Wishlists.length}
                rowsPerPage={filters.pageSize}
                page={filters.pageIndex - 1}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage} />
            </div>

                        <ul className="responsive-table no-photo-table py-2">
                            {(Wishlists).map((value, index) => (
                                <li className={(index >= filters.startIndex && index <= filters.endIndex) ? "table-row" : "table-row d-none"} key={index}>
                                {/* <div className="col col-1" data-label="Job Id"><label>sr.</label><p>{index + 1}</p></div> */}
                                <div className="col col-1" data-label="#"><label>#</label>
                                    <p>{index + 1}</p>
                                </div>
                                <div className="col col-10" data-label="Amount"><label>Category</label>
                                    <p>{(value.wishListCategories)[0].categoryName}</p>
                                </div>
                                <div className="col col-10" data-label="Customer Name"><label>Sub Category</label>
                                    <p>{value.wishListSubCategories[1].subCategoryName}</p>
                                </div>
                                {/* <div className="col col-3" data-label="Amount"><label>Name</label><p><Link to={`/dashboard/product-detail/${value.productId}`}>{value.productName}</Link></p></div> */}
                                <div className="col col-10" data-label="Amount"><label>Description</label><p>{value.wishListDescription}</p></div>
                                <div className="col col-8" data-label="Payment Status"><label>Actions</label>
                                    <p>
                                        <span>
                                            <Tooltip disableFocusListener title="Edit">
                                                <Button className='btn btn-primary mr-1'
                                                    as={Link}
                                                    to={`/dashboard/update-wishlist/${value.wishListId || 0}`}>
                                                    <FontAwesomeIcon icon={faPencil} />
                                                </Button>
                                            </Tooltip>
                                            <Tooltip disableFocusListener title="View">
                                                <Button className='btn btn-primary mr-1'
                                                    as={Link}
                                                    to={`/dashboard/view-wishlist/${value.wishListId || 0}`}>
                                                    <FontAwesomeIcon icon={faEye} />
                                                </Button>
                                            </Tooltip>
                                            <Tooltip disableFocusListener title="Delete">
                                                <Button className='btn btn-primary mr-1' onClick={event => {
                                                    event.preventDefault();
                                                    deleteWishlist(value.wishListId);
                                                }}>
                                                    <FontAwesomeIcon icon={faTrash} />
                                                </Button>
                                            </Tooltip>
                                        </span>
                                    </p>
                                </div>
                                </li>
                            ))}
                        </ul>

            <div className="list-pagination">
              <Pagination
                rowsPerPageOptions={[5, 10]}
                component="div"
                count={Wishlists.length}
                rowsPerPage={filters.pageSize}
                page={filters.pageIndex - 1}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage} />
            </div>

            {/* <BootstrapTable
                            bootstrap4
                            keyField="wishListId"
                            data={Wishlists}
                            columns={columns}
                            bordered={false}
                            pagination={paginationFactory({ sizePerPage: 10 })}
                        ></BootstrapTable> */}
          </div>
        );
      } else {
        return (
          <h4 style={{textAlign: "center", padding: "20px"}}>{message}</h4>
        );
      }
    } else {
      return (
        <h4 style={{textAlign: "center", padding: "20px"}}>{message}</h4>
      );
    }
  };

  return (
    <div className="checkout">
      <div className="bg-light py-5 text-center">
        <div className="container">
          <h1 className="section-heading">Wishlist</h1>
          <Container className="my-4">
            <Row>
              <Col lg={12} className="">
                <Breadcrumbs maxItems={2} aria-label="breadcrumb">
                  <MUILink underline="hover" color="inherit" href="/dashboard/home">
                    Home
                  </MUILink>
                  <span color="inherit">
                    Wishlist
                  </span>
                </Breadcrumbs>
              </Col>
            </Row>
          </Container>
          <Row>
            <Col md={10} lg={12} xl={12} className="mx-auto">
              <Button className='float-right mt-3 rounded-0 px-4 font-weight-bold text-capitalize mt-1 mb-1 btn btn-primary btn-lg'
                as={Link}
                to="/dashboard/create-wishlist">
                Create Wishlist
              </Button>
            </Col>
          </Row>
        </div>
      </div>
      <div className="container">
        {renderProducts()}
      </div>
    </div>
  );
};

export default withAlert()(Wishlist);
