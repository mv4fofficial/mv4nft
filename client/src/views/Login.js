import React, { useState } from "react";
import { withAlert } from 'react-alert';
import { connect } from 'react-redux';
import { Col, Form, Row } from "react-bootstrap";
import { Link, withRouter } from "react-router-dom";
import { userDataUpdate, loginStatusUpdate } from '../user';
import ApiRequest from '../api_service/api_requests';
import { URLS } from "../constant";
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import { Logo } from "../components/Logo";
import Eye from "@material-ui/icons/Visibility"
import EyeOff from "@material-ui/icons/VisibilityOff"
// import { getStorage, ref, uploadBytes, getDownloadURL } from "firebase/storage";
// import "firebase/storage";
import LoginWithApp from "../components/user/LoginWithApp";

const useStyles = makeStyles((theme) => ({
  root: {
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
      width: '100%',
    },
  },
}));

class Login extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      showPassowrd: false,
      error: {}
    };

  }
  componentDidMount() {
    // console.log("sasaksjkas", this.props);
  }

  updateUsername = e => {
    if (e && e.target && e.target.value) {
      this.setState({ username: e.target.value });
    }
  }

  updatePassword = e => {
    if (e && e.target && e.target.value) {
      this.setState({ password: e.target.value });
    }
  }

  handleClickShowPassword = () => {
    this.setState({ showPassword: !this.state.showPassword });
  };

  handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  // handleUpload = async (e) => {
  //   e.preventDefault();
  //   if (e.target.files[0]) {
  //     const file = e.target.files[0];
  //     const storage = getStorage();
  //     const storageRef = ref(storage, `staging/${file.name}`);
  //     await uploadBytes(storageRef, file).then((snapshot) => {
  //       // console.log('Uploaded a blob or file!');
  //       // console.log(snapshot);
  //     });
  //     await getDownloadURL(storageRef)
  //     .then((url) => {
  //       // console.log('url', url);
  //     })
  //     .catch((error) => {
  //       // A full list of error codes is available at
  //       // https://firebase.google.com/docs/storage/web/handle-errors
  //       // eslint-disable-next-line default-case
  //       switch (error.code) {
  //         case 'storage/object-not-found':
  //           // File doesn't exist
  //           break;
  //         case 'storage/unauthorized':
  //           // User doesn't have permission to access the object
  //           break;
  //         case 'storage/canceled':
  //           // User canceled the upload
  //           break;
    
  //         // ...
    
  //         case 'storage/unknown':
  //           // Unknown error occurred, inspect the server response
  //           break;
  //       }
  //     });
  //   }
  // }

  handleSubmit = () => {
    let error = {}

    if (this.state.username === "") {
      error["username"] = "Username is required."
    }

    if (this.state.password === "") {
      error["password"] = "Password is required."
    }

    // console.log(this.state.username);
    // console.log(this.state.password);
    // console.log(Object.keys(error).length);

    if (Object.keys(error).length === 0) {
      let loginParams = {};
      loginParams.emailId = this.state.username;
      loginParams.password = this.state.password;

      ApiRequest.loginAuth(loginParams)
        .then(res => {
          // console.log(res)
          if (res && res.message) {
            if (res.data && res.statusCode && res.statusCode === 200) {
              this.props.dispatch(userDataUpdate(res.data));
              this.props.dispatch(loginStatusUpdate(true));
              this.props.alert.success("You are successfully login!");

              if (this.props && this.props.match && this.props.match.params &&
                this.props.match.params.page_number && URLS[this.props.match.params.page_number]) {
                this.props.history.push(URLS[Number(this.props.match.params.page_number)]);
                return
              }
              this.props.history.push('/dashboard');
            } else {
              // Handle error response
              this.props.alert.error(res.message);
            }
          } else {
            this.props.alert.error("Something went wrong");
          }
        })
        .catch((err) => {
          // console.log('Login api error, ', err);
          this.handleBadRequest(err);
        })
    } else {
      this.setState({ error: { ...error } });
    }
  }

  handleBadRequest = res => {
    if (res && res.errors) {
      for (var key in res.errors) {
        if (res.errors.hasOwnProperty(key) && res.errors[key] && res.errors[key][0]) {
          this.props.alert.error(res.errors[key][0] || "Something went wrong!");
          break;
        }
      }
    }
  }

  handleKeyDown = event => {
    if (event.key === 'Enter') {
      // 👇️ your logic here
      this.handleSubmit();
      // console.log('Enter key pressed ✅');
    }
  };

  render() {
    return (
      <div className="register login-bg-hero" onKeyDown={this.handleKeyDown}>
        <Container>
          <div className="login-form">
            <Col className="mx-auto p-0">
              <div className="logo-head mb-5">
                {/* <img src={StaticData.logo} width={100} alt="logo"></img> */}
                <Logo />
              </div>

              {/* <h2 className="section-heading wp-100">Login</h2> */}

              <div>
                <Form autoComplete="off">
                  <Form.Group controlId="email">
                    <TextField
                      label="Email"
                      type="email"
                      name="email"
                      placeholder="example@gmail.com"
                      variant="outlined"
                      className="wp-100"
                      onChange={this.updateUsername}
                    />
                    {/* <Form.Label>Enter email</Form.Label>
                    <Form.Control
                      className="rounded-0"
                      type="email"
                      name="email"
                      onChange={this.updateUsername}
                    /> */}
                    <p className="error-text">{this.state.error && this.state.error.username}</p>
                  </Form.Group>

                  <Form.Group controlId="password" className="mt-3 position-relative">
                    {/* <TextField
                      id="password"
                      label="Password"
                      placeholder="********"
                      multiline
                      variant="outlined"
                      className="wp-100"
                      type="password"
                      name="password"
                      onChange={this.updatePassword}
                    /> */}
                    <TextField
                      id="outlined-password-input"
                      label="Password"
                      type={(this.state.showPassword) ? "text" : "password"}
                      className="wp-100"
                      autoComplete="current-password"
                      variant="outlined"
                      onChange={this.updatePassword}
                    />
                    {(this.state.showPassword) ? <Eye onClick={this.handleClickShowPassword} style={{ fill: "#fff", top: "15px", right: "14px", position: "absolute" }}/>
                    : <EyeOff onClick={this.handleClickShowPassword} style={{ fill: "#fff", top: "15px", right: "14px", position: "absolute" }}/>}
                    <p className="error-text">{this.state.error && this.state.error.password}</p>
                    {/* <Form.Label>Enter password</Form.Label>
                    <Form.Control
                      className="rounded-0"
                      type="password"
                      name="password"
                      onChange={this.updatePassword}
                    /> */}
                  </Form.Group>
                  <div className="text-center mt-4">
                    <Button className="wp-100" variant="contained" color="primary" onClick={this.handleSubmit}>
                      Sign in
                    </Button>
                    {/* <Button className="rounded-0 py-2 px-md-5 px-3"
                      variant="primary" onClick={this.handleSubmit}>
                      Sign in
                    </Button> */}
                    <Link className="d-block mt-1 " to="/register">
                      <u> Create Account</u>
                    </Link>
                    <Link className="d-block mt-1 " to="/forgot-password">
                      <u>Forgot password?</u>
                    </Link>
                  </div>

                  <LoginWithApp props={this.props}/>
                </Form>
              </div>
            </Col>
          </div>
          <div className="login-animation-container">
            <div className="circle circle-1">
              <div className="spot spot-1"></div>
              <div className="spot spot-2"></div>
              <div className="spot spot-3"></div>
            </div>
            <div className="circle circle-2">
              <div className="spot spot-1"></div>
              <div className="spot spot-2"></div>
              <div className="spot spot-3"></div>
              <div className="spot spot-4"></div>
            </div>
          </div>
        </Container>
      </div>
    );
  }
};

const mapStateToProps = (state) => {
  return {
    user: state.userReducer
  }
}

export default withRouter(withAlert()(connect(mapStateToProps)(Login)));
