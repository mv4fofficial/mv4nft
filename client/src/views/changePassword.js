import React, {useState, useEffect} from "react";
import {Col, Container, Form, Row} from "react-bootstrap";
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import StatiData from "../components/data/constants";
import {useForm} from 'react-hook-form'
import {connect} from 'react-redux';
import {withAlert} from 'react-alert';
import {withRouter} from "react-router-dom";
import ApiRequest from '../api_service/api_requests';
import { Logo } from "../components/Logo";

const ChangePassword = (props) => {
  const [email, setEmail] = useState('');
  const [token, setToken] = useState('');

  useEffect(() => {
    if (props && props.match && props.match.params && props.match.params.email && props.match.params.token) {
      setEmail(props.match.params.email);
      setToken(props.match.params.token);
    }
  }, []);
  const {
    register,
    handleSubmit,
    formState: {errors},
  } = useForm();
  const onSubmit = (data) => {
    data.email = email;
    data.token = token;
    // console.log(data)
    ApiRequest.resetPassword(data)
      .then(res => {
        if (res && res.statusCode && res.statusCode === 200) {
          props.history.push('/login');
          props.alert.success(res.message || "Password Reset Successful!");
        } else {
          props.alert.error(res.message || "Request failed!");
        }
      })
      .catch((err) => {
        // console.log('resetPassword api error, ', err);
      })

  }

  return (
    <div className="register login-bg-hero" >
      <Container>
        <div className="login-form">
          <Col className="mx-auto p-0">
            <div className="logo-head mb-5">
              {/* <img src={StatiData.logo} width={100} alt="logo"></img> */}
              <Logo />
            </div>

            <h4 className="section-heading wp-100">Change Password</h4>

            <div>
              <Form autoComplete="off" onSubmit={handleSubmit(onSubmit)}>
                <Form.Group controlId="password">
                  <TextField
                    label="Password"
                    type="password"
                    name="password"
                    placeholder="example@gmail.com"
                    variant="outlined"
                    className="wp-100"
                    {...register('password', {required: true})}
                  />
                  {errors.password && <p className="error-text">Password is required.</p>}
                </Form.Group>
                <Form.Group controlId="confirmPassword" className="mt-3">
                  <TextField
                    label="Confirm Password"
                    type="password"
                    name="confirmPassword"
                    placeholder="example@gmail.com"
                    variant="outlined"
                    className="wp-100"
                  //{...register('confirmPassword', {required: true})}
                  />
                  {errors.confirmPassword && <p className="error-text">Confirm password is required.</p>}
                </Form.Group>
                <div className="text-center mt-4">
                  <Button className="wp-100" variant="contained" color="primary" type="submit">
                    Submit
                  </Button>
                </div>
              </Form>
            </div>
          </Col>
        </div>
      </Container>
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    user: state.userReducer
  }
}
export default withRouter(withAlert()(connect(mapStateToProps)(ChangePassword)));
