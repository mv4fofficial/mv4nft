import UserIcon from "../components/icons/defaultUser";
import ItemList from "../components/profile/itemList";
import NftItemList from "../components/profile/NftItemList";
import ItemFilter from "../components/profile/itemFilter";
import LoaderComp from "../components/Loader";
import axios from "axios";
// static demo data
import StaticData from "../components/data/constants"
import react, {useState, useEffect} from "react";
import ApiRequest from "../api_service/api_requests";

import {Nav, Navbar} from "react-bootstrap";
import {Link} from "react-router-dom";
import IconButton from "@material-ui/core/IconButton";
import FavoriteIcon from "@material-ui/icons/Favorite";
import Button from '@material-ui/core/Button';
import BuyerConfirm from "../components/profile/buyerConfirm";
import moment from "moment";
import ComingSoon from "../components/noti/ComingSoon";
import {filter} from "underscore";
import {ImmutableXClient, Link as imxlink, ImmutableMethodResults} from '@imtbl/imx-sdk';

import StripeDashboard from "../components/stripe/StripeDashboard";

import NotiOffIcon from '@material-ui/icons/NotificationsOff';
import NotiActiveIcon from '@material-ui/icons/NotificationsActive';
import RefreshIcon from '@material-ui/icons/Refresh';

import UserFollowList from "../components/profile/userFollowList";

import "../assets/css/profile.css";

const UserProfile = ({props, immuClient}) => {
  // console.log(props);
  let profileId = (props.hasOwnProperty("match") && props.match.params && props.match.params.profile_id)
    ? props.match.params.profile_id
    : 0;
  // console.log("Profile ID", profileId);

  const [filters, setFilters] = useState({
    nftAvailability: true,
    priceFrom: 0,
    priceTo: 0,
    categories: [0],
    subCategories: [0],
    tags: [
      "string"
    ],
    orderBy: 0,
    pageIndex: 0,
    pageSize: 10,
  })

  const [showLoader, setShowLoader] = useState(true)
  const loaderShowHide = (value) => {
    setShowLoader(value);
  }

  const [readMore, setReadMore] = useState(false)
  const seeMoreSeeLess = () => {
    setReadMore(!readMore)
  }

  let tabs = [
    "Items", "Activities", "Status", "Wishlist", "Transactions","Nfts"
  ];
  if (profileId != 0) {
    tabs = [
      "Items", "Nfts", "Activities", "Status", "Nfts"
    ];
  }

  const [allProducts, setAllProducts] = useState([]);
  const [wishlistProducts, setWishlistProducts] = useState([]);
  const [category, setCategory] = useState([]);
  const [subCategory, setSubCategory] = useState([]);

  const [userWishlists, setUserWishLists] = useState([]);

  const [sellerConfrim, setSellerConfirm] = useState()
  const [showSellerConfrim, setShowSellerConfirm] = useState(false)
  const [allNftProducts, setAllNftProducts] = useState([]);
  // const [tabStep, setTabStep] = useState(0);
  const [client, setClient] = useState(Object);

  const [tabStep, setTabStep] = useState(window.location.hash ? tabs.indexOf((window.location.hash).replace("#", "")) : 0)
  const changeTab = (e) => {
    setTabStep(parseInt(e.target.id))

    let wishList = [];
    setUserWishLists([...wishList]);

    if (parseInt(e.target.id) === 0 || parseInt(e.target.id) === 3) {
      ApiRequest.getUserProductWishList(profileId).then((res) => {
        const products = res.data.map(e => {
          wishList = [...wishList, e.product.productId]
          return e.product
        })
        // console.log(products)
        setWishlistProducts(products);
        setUserWishLists([...wishList]);
      });
    }
  }

  const [user, setUser] = useState({
    roles: [],
    country: {}
  });

  useEffect(() => {
    loaderShowHide(true)
    if (profileId != 0) {
      ApiRequest.viewUserById(profileId)
        .then(res => {
          if (res && res.data && res.statusCode && res.statusCode === 200) {
            // console.log("View other user", res.data)
            let user = {
              firstName: res.data.firstName || (res.data.name).split(" ")[0],
              lastName: res.data.lastName || (res.data.name).split(" ")[1],
              name: res.data.name || "",
              emailId: res.data.emailId || "",
              country: res.data.country,
              phone_code: res.data.country.phone_code,
              phoneNumber: res.data.phoneNumber || "",
              dateOfBirth: res.data.dateOfBirth || "",
              roles: res.data.roles || [],
              backgroundPicture: res.data.backgroundPicture || "",
              profilePicture: res.data.profilePicture || "",
              description: res.data.bio || res.data.description,
              stripeAccount: res.data.stripeAccount || "",
              stripeStatus: res.data.stripeStatus,
              isRequestPending: res.data.isRequestPending || false,
              // itemsCreated: res.data.itemsCreated,
              // itemsOwned: res.data.itemsOwned,
              // itemsSold: res.data.itemsOwned,
              // itemsBought: res.data.itemsBought,
              isByself: false,
            }
            setUser({...user})
          } else {
            if (res && res.message) {
              // this.props.alert.error("Error fetching user details");
            }
          }
        })
        .catch((err) => {
          // console.log('Error fetching user details, ', err);
        })
    } else {
      ApiRequest.viewUser()
        // ApiRequest.viewUserById(2)
        .then(res => {
          if (res && res.data && res.statusCode && res.statusCode === 200) {
            // console.log("View user", res.data)
            let user = {
              firstName: res.data.firstName || (res.data.name).split(" ")[0],
              lastName: res.data.lastName || (res.data.name).split(" ")[1],
              name: res.data.name || "",
              emailId: res.data.emailId || "",
              country: res.data.country,
              phone_code: res.data.country.phone_code,
              phoneNumber: res.data.phoneNumber || "",
              dateOfBirth: res.data.dateOfBirth || "",
              roles: res.data.roles || [],
              backgroundPicture: res.data.backgroundPicture || "",
              profilePicture: res.data.profilePicture || "",
              description: res.data.bio || res.data.description,
              createdDate: res.data.createdDate || "",
              isRequestPending: res.data.isRequestPending || false,
              itemsCreated: res.data.itemsCreated,
              itemsOwned: res.data.itemsOwned,
              itemsSold: res.data.itemsSold,
              itemsBought: res.data.itemsBought,
              stripeAccount: res.data.stripeAccount || "",
              stripeStatus: res.data.stripeStatus,
              isByself: true,
            }
            setUser({...user});
          }
        })
        .catch((err) => {
          // console.log('Error fetching user details, ', err);
        })
    }


    ApiRequest.getsellerProducts(profileId, filters).then((res) => {
      // console.log("getsellerproduct", res.data.data);
      setAllProducts([...res.data.data]);
    });

    // ApiRequest.getUserProductWishList(profileId).then((res) => {
    //   let wishList = userWishlists;
    //   const products = res.data.map(e => {
    //     wishList = [...wishList, e.product.productId]
    //     return e.product
    //   })
    //   // console.log(products)
    //   setWishlistProducts(products);
    //   setUserWishLists(wishList);
    // });

    ApiRequest.getCategory()
      .then(res => {
        if (res) {
          setCategory(res);
        }
      })
      .catch((err) => {
        // console.log('Categories fetch api error, ', err);
      })

    ApiRequest.getSubCategory()
      .then(res => {
        if (res) {
          setSubCategory(res);
        }
      })
      .catch((err) => {
        // console.log('Categories fetch api error, ', err)
      })

    //loadNftProducts();
    getNftProducts();
    // console.log('my nfts', allNftProducts);
  }, [profileId, filters]);

  const loadNftProducts = async () => {
    const publicApiUrl = process.env.REACT_APP_ROPSTEN_ENV_URL ?? '';
    const client = await ImmutableXClient.build({publicApiUrl});
    const address = localStorage.getItem('WALLET_ADDRESS');
    setAllNftProducts(await client.getAssets({user: address, sell_orders: true}));
  }
  const getNftProducts = () => {
    const address = localStorage.getItem('WALLET_ADDRESS');
    const options = {
      method: 'GET',
      url: `${process.env.REACT_APP_ROPSTEN_ENV_URL}/assets`,
      params: {sell_orders: 'true', collection: '0x0eBF1f438F279365CF0e2F0888627b0CD830Aa16', user: address},
      headers: {'Content-Type': 'application/json'}
    };
    var self = this;
    axios.request(options).then(function (res) {
      if (res && res.data) {
        setAllNftProducts(res.data.result);
      }
      loaderShowHide(false)
    }).catch(function (error) {
      console.error(error);
      loaderShowHide(false)
    });
  }


  const handleChange = (filters) => {
    // console.log(e.target.getAttribute("data-name"))
    // console.log(e.target.getAttribute("data-value"))

    // let filterOption = filters;
    // filterOption[e.target.getAttribute("data-name")] = e.target.getAttribute("data-value");
    setFilters(filters)
  }

  // console.log(filters)
  // console.log(user)
  // console.log(userWishlists)

  // useEffect(() => {
  //   let filterOptions = filters;
  //   // here is to call api
  //   ApiRequest.getsellerProducts(profileId, filterOptions).then((res) => {
  //     setAllProducts(res.data);
  //   });

  // }, [filters, profileId])
  // console.log("country!!!", user.country.name)

  const [popupData, setPopupData] = useState({
    title: "Confirmation",
    msg: "Your are qualify for seller role. Do u want to upgrade?"
  })
  const [actionDirection, setActionDirection] = useState("edit")
  const becomeSeller = () => {
    if (user.roles[0] === "Buyer") {
      if ((user.profilePicture != "") && (user.backgroundPicture != "") && (user.description != null)) {
        setActionDirection("upgrade")
        setShowSellerConfirm(true)
      } else {
        setActionDirection("edit")
        setPopupData({
          title: "Missing Information",
          msg: "Your need to fill up some information. Go to profile update?"
        })
        setShowSellerConfirm(true)
      }
    }
  }

  useEffect(() => {
    // console.log(sellerConfrim, user)
    if (sellerConfrim) {
      if (actionDirection === "edit") {
        props.history.push("/dashboard/profileEdit/upgrade")
      } else {
        // call to upgrade to seller

        let userUpdateData = {
          firstName: (user.name).split(" ")[0],
          lastName: (user.name).split(" ")[1],
          phoneNumber: parseInt(user.phoneNumber),
          countryId: user.country.id,
          dateOfBirth: user.dateOfBirth, // (data.dateOfBirth) ? (new Date(data.dateOfBirth)).getTime() : (new Date(user.dateOfBirth)).getTime(),
          profilePicture: user.profilePicture,
          backgroundPicture: user.backgroundPicture,
          description: user.description,
          extention: ".webp",
          bio: user.description,
          roleId: 2
        }

        // console.log(userUpdateData)

        ApiRequest.upgraderUserRole(userUpdateData)
          .then(res => {
            if (res && res.statusCode && res.statusCode === 200) {
              props.history.push('/dashboard/profile');
            } else {
              if (res && res.message) {
                // this.props.alert.error("Error fetching user details");
              }
            }
          })
          .catch((err) => {
            // console.log('Error fetching user details, ', err);
          })
      }
    } else {
      setShowSellerConfirm(false)
    }
  }, [sellerConfrim])

  const [isFollow, setFollow] = useState(false);
  const [isFollowLoading, setFollowLoading] = useState(false);
  const [followQty, setFollowQty] = useState([]);
  const [followeeQty, setFolloweeQty] = useState([]);
  const [followeeList, setFolloweeList] = useState([]);

  useEffect(() => {
    let userId = profileId;
    if (user.isByself) {
      userId = JSON.parse(localStorage.getItem('$@meta@$')).userReducer.id;
    }
    ApiRequest.getFollow({
      "followerId": userId,
      "emailStatus": true
    })
    .then(res => {
      // console.log(res)
      if (res && res.statusCode && res.statusCode === 200) {
        if (res.data.followData.length > 0) {
          setFollow(true)
        }

        setFollowQty(res.data.followQty);

        let count = [];
        (res.data.followeeQty).forEach(value => {
          // console.log(value.FollowerId);
          if (!count.includes(value.FollowerId)) {
            count.push(value.FollowerId);
          }
        });
        setFolloweeQty(count);

        // console.log(count);
        ApiRequest.getFollowees({
          "userIds": count
        })
        .then(res => {
          // console.log(res)
          if (res && res.statusCode && res.statusCode === 200) {
            setFolloweeList(res.data);
          } else {
            if (res && res.message) {
              // this.props.alert.error("Error fetching user details");
            }
          }
        })
        .catch((err) => {
          // console.log('Error fetching user details, ', err);
        })

      } else {
        if (res && res.message) {
          // this.props.alert.error("Error fetching user details");
        }
      }
    })
    .catch((err) => {
      // console.log('Error fetching user details, ', err);
    })

    
  }, [user, isFollow]);

  const followUnfollow = () => {
    if (profileId != 0 && !isFollowLoading) {
      setFollowLoading(true)
      ApiRequest.setFollow({
        "followerId": profileId,
        "emailStatus": true
      })
      .then(res => {
        // console.log(res);
        if (res && res.statusCode && res.statusCode === 200) {
          // if (res.data.length > 0) {
            setFollow(!isFollow)
            setFollowLoading(false)
          // }
        } else {
          if (res && res.message) {
            // this.props.alert.error("Error fetching user details");
          }
        }
      })
      .catch((err) => {
        // console.log('Error fetching user details, ', err);
      })
    }
  }

  const [showFollowListModal, setShowFollowListModal] = useState(false);

  const closeFollowerListModal = () => {
    setShowFollowListModal(false)
  }

  const [showFolloweeListModal, setShowFolloweeListModal] = useState(false);

  const closeFolloweeListModal = () => {
    setShowFolloweeListModal(false)
  }

  return (
    <LoaderComp show={showLoader}>
      <div className="user-profile">
        <div className="my-4 container">
          <div className="user-header">
            <div className="user-banner">
              {user.backgroundPicture && <img className="banner-image" src={user.backgroundPicture} style={{width: "100%", borderRadius: "15px"}} />}
              {/* <img src="" /> user banner photo */}
              {/* <Nav className="user-edit-icon">
                                <Nav.Link as={Link} to="/dashboard/profileEdit">
                                    <EditIcon/>
                                </Nav.Link>
                            </Nav> */}


              <div className="user-profile">
                {user.profilePicture ? <img src={user.profilePicture} style={{width: "100%", borderRadius: "50%"}} /> : <UserIcon />}
              </div>
              {(profileId === 0) && <div className="profile-edit-btn-control">

                <Nav.Link as={Link} to="/dashboard/profileEdit/edit">
                  <Button
                    type="button"
                    className=""
                    variant="contained"
                    color="primary"
                    size="small"
                  >
                    {"Edit"}
                  </Button>
                </Nav.Link>

                {(Object.keys(user).length > 0 && !user.isRequestPending && (user.roles.length > 0 && user.roles[0] === "Buyer")) &&
                  <Nav.Link onClick={becomeSeller}>
                    <Button
                      type="button"
                      className=""
                      variant="contained"
                      color="primary"
                      size="small"
                      onClick={becomeSeller}
                    >
                      {"Become Seller"}
                    </Button>
                  </Nav.Link>
                }

                {(Object.keys(user).length > 0 && user.isRequestPending) &&
                  <Nav.Link>
                    <Button
                      type="button"
                      className=""
                      variant="contained"
                      color="primary"
                      size="small"
                    >
                      Request is pending...
                    </Button>
                  </Nav.Link>
                }
              </div>}

              <div className="user-info">
                <div>
                  <h4>{user.firstName+" "+user.lastName}</h4>
                  <span>{"( " + moment(user.createdDate).format('MMM DD, YYYY') + " )"}</span>
                  {user.isByself && <span>{user.emailId}</span>
                    // <a href={`mailto:${user.emailId}`} style={{ color: "#fff", textDecoration: "underline", cursor: "pointer" }} target="_blank">{user.emailId}</a>
                  }
                </div>
                {(profileId != 0) && <div className="follow">
                  <Button className={(isFollow) ? "followed mr-3" : "follow mr-3"} variant="contained" startIcon={(isFollowLoading) ? <RefreshIcon className="rotate"/> :(isFollow) ? <NotiActiveIcon /> : <NotiOffIcon />}
                    onClick={(event) => {
                      event.preventDefault();
                      followUnfollow();
                    }}
                  >
                  {(isFollow) ? "Followed" : "Follow"}
                  </Button>
                </div>}
              </div>
            </div>

            <div className="user-data">
              <div className="data-card-container">
                <div className="data-card" onClick={(event) => { if (followeeQty.length > 0) { event.preventDefault(); setShowFolloweeListModal(true); } }}>
                  <label>Following</label>
                  {(followeeQty.length > 0) ?
                    <label style={{cursor: "pointer", textDecoration: "underline"}}>{followeeQty.length}</label>
                  :
                    <label>{followeeQty.length}</label>
                  }
                </div>
                <div className="data-card" onClick={(event) => { if (followQty.length > 0) { event.preventDefault(); setShowFollowListModal(true); } }}>
                  <label>Follower</label>
                  {(followQty.length > 0) ?
                    <label style={{cursor: "pointer", textDecoration: "underline"}}>{followQty.length}</label>
                  :
                  <label>{followQty.length}</label>
                  }
                </div>
                <div className="data-card">
                  <label>Items Owned</label>
                  <label>{user.itemsOwned || 0}</label>
                </div>
                {((Object.keys(user).length > 0) && (user.roles[0] == "Seller")) &&
                  <>
                    <div className="data-card">
                      <label>Created</label>
                      <label>{user.itemsCreated || 0}</label>
                    </div>
                    <div className="data-card">
                      <label>Sold</label>
                      <label>{user.itemsSold || 0}</label>
                    </div>
                  </>
                }

                <div className="data-card">
                  <label>Bought</label>
                  <label>{user.itemsBought || 0}</label>
                </div>
                {user.country != null &&
                  <div className="data-card">
                    <label>Country</label>
                    <label>{user.country.name}</label>
                  </div>
                }
                {user.isByself && <div className="data-card">
                  <label>Contact</label>
                  <label>{user.phone_code + " " + user.phoneNumber}</label>
                </div>}
              </div>
            </div>

            <div className="user-description">
              {/* <p onClick={seeMoreSeeLess}>{(readMore) ? StaticData.description : (StaticData.description).slice(0, 600)+ (((StaticData.description).length > 600) ? "..." : "")}</p> */}
              <p>{(user.bio) ? user.bio : (user.description) ? user.description : StaticData.description}</p>
            </div>
          </div>

          {/* {((Object.keys(user).length > 0) && (user.roles[0] == "Seller")) &&  */}
          <div className="user-body">
            <div className="tabs">
              {tabs.map((tab, index) => {
                return <div key={index} id={index} className={`tab tab-${index} ${(tabStep === index) ? "active" : ""}`} onClick={changeTab}><Nav.Link as={Link} to={`/dashboard/profile#${tab}`}>{tab}</Nav.Link></div>
              })}
            </div>

            <div className="tabs-content">
              {(tabStep === 0) && <ItemFilter category={category} subCategory={subCategory} handleChange={handleChange} filters={filters} />}
              {(tabStep === 0) && <ItemList data={allProducts} userWishlists={userWishlists} />}
              {(tabStep === 1) && <ComingSoon />}
              {(tabStep === 2) && <ComingSoon />}
              {(tabStep === 3) && <ItemList data={wishlistProducts} userWishlists={userWishlists} />}
              {(tabStep === 4) && <StripeDashboard />}
              {(tabStep === 5) && <NftItemList data={allNftProducts} userWishlists={[]} />}
            </div>
          </div>
          {/* } */}
        </div>
      </div>
      <BuyerConfirm open={showSellerConfrim} handleClose={setShowSellerConfirm} setSellerConfirm={setSellerConfirm} data={popupData} />

      <UserFollowList title={"Followers"} data={followQty} open={showFollowListModal} handleClose={closeFollowerListModal} />
      <UserFollowList title={"Followings"} data={followeeList} open={showFolloweeListModal} handleClose={closeFolloweeListModal} />
    </LoaderComp>
  )
}
export default UserProfile;
