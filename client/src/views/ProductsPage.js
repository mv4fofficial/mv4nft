import React from "react";
import { connect } from 'react-redux';
import { withRouter } from "react-router-dom";
import { Form, Col, Container, Row } from "react-bootstrap";
// import Pagination from "react-js-pagination";
import Pagination from '@material-ui/core/TablePagination';
import ProductsFilter from "../components/products/ProductsFilter";
import ProductsPriceFilter from "../components/products/ProductsPriceFilter";
import ProductsList from "../components/products/ProductsList";
import { productSearchStringUpdate } from '../user';
import LoaderComp from '../components/Loader';
import ApiRequest from '../api_service/api_requests';

import '../assets/css/product-list.css';

import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import MUILink from '@material-ui/core/Link';

class ProductsPage extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      categoriesList: [],
      subCategoriesList: [],
      data: [],
      recommendedForYou: [],

      categories: [],
      subCategories: [],
      tags: [],

      selectedCategories: [],

      priceFrom: 0,
      priceTo: 0,
      nftAvailability: true,
      filter: this.props.user.productSearchString,
      pageIndex: 1,
      pageSize: 12,
      totalSize: 0,

      loaderMessage: '',
      displayLoader: false,

      sliderValue: [0, 10],
      isSticky: false,

      user: JSON.parse(localStorage.getItem('$@meta@$'))
    };
  }

  componentDidMount() {
    this.getCategories();
    this.getSubCategories();
    this.getProducts();
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.user && this.props.user.productSearchString && this.props.user.productSearchString !== prevProps.user.productSearchString) {
      this.setState({ filter: this.props.user.productSearchString });
    }

    if (prevState.filter !== this.state.filter) {
      this.getProducts();
    }
    window.addEventListener('scroll', this.scrollSpy);
  }

  componentWillUnmount() {
    this.props.dispatch(productSearchStringUpdate(""));
    window.addEventListener('scroll', this.scrollSpy);
  }

  scrollSpy = (e) => {
    if (window.scrollY > 370) {
      this.setState({isSticky: true})
    } else {
      this.setState({isSticky: false})
    }
  }

  getCategories = () => {
    ApiRequest.getCategory()
      .then(res => {
        if (res) {
          this.setState({ categoriesList: res });
        }
      })
      .catch((err) => {
        // console.log('Categories fetch api error, ', err);
      })
  }

  getSubCategories = () => {
    ApiRequest.getSubCategory()
      .then(res => {
        if (res) {
          this.setState({ subCategoriesList: res });
        }
      })
      .catch((err) => {
        // console.log('Categories fetch api error, ', err);
      })
  }

  getProducts = () => {

    this.setState({
      data: [],
      pageIndex: 1,
      totalSize: 0
    });

    this.setState({ loaderMessage: "Fetching Products", displayLoader: true });

    let categories = [];
    let subCategories = [];

    if (this.state.selectedCategories) {
      this.state.selectedCategories.forEach(category => {
        let arr = category.split('-');
        if (arr && arr.length) {
          if (categories.indexOf(arr[0]) < 0) {
            categories.push(arr[0]);
          }
          if (subCategories.indexOf(arr[1]) < 0) {
            subCategories.push(arr[1]);
          }
        }
      });
    }

    let payload = {
      categories: categories,
      subCategories: subCategories,
      tags: this.state.tags,
      priceFrom: parseInt(this.state.priceFrom), // this.state.sliderValue[0] * 100,
      priceTo: parseInt(this.state.priceTo), // this.state.sliderValue[1] * 100,
      nftAvailability: this.state.nftAvailability,
      filter: this.state.filter || "",
      pageIndex: this.state.pageIndex,
      pageSize: this.state.pageSize
    };

    // console.log(payload);

    if (this.state.user.hasOwnProperty("userReducer")) {
      ApiRequest.recommendedForYou()
        .then(res => {
          // console.log(res)
          if (res && res.data) {
            this.setState({
              recommendedForYou: res.data.products
            });
          }
        })
        .catch((err) => {
          // console.log('Login api error, ', err);
        })
    }
    
    ApiRequest.getProducts(payload)
      .then(res => {
        if (res && res.data && res.data.data) {
          this.setState({
            data: res.data.data,
            pageIndex: res.data.pageIndex || 1,
            totalSize: res.data.totalRecords || 0
          });
        }
        this.setState({ loaderMessage: "", displayLoader: false });
      })
      .catch((err) => {
        // console.log('Login api error, ', err);
        this.setState({ loaderMessage: "", displayLoader: false });
      })
  }

  selectCategory = event => {
    if (event && event.target && event.target.id) {
      let category = event.target.id;
      let selectedCategories = new Set(this.state.selectedCategories);

      if (selectedCategories.has(category)) {
        selectedCategories.delete(category);
      } else {
        selectedCategories.add(category);
      }

      this.setState({ selectedCategories: selectedCategories }); // this.getProducts
    }
  }

  priceChange = (value) => {
    this.setState({ priceFrom: (value[0]), priceTo: (value[1]) }); // this.getProducts
    // if (event && event.target && event.target.value && event.target.id && !isNaN(event.target.value)) {
    //   if (event.target.id === "Lowest") {
    //     this.setState({ priceFrom: Number(event.target.value) }, this.getProducts);
    //   }
    //   if (event.target.id === "Highest") {
    //     this.setState({ priceTo: Number(event.target.value) }, this.getProducts);
    //   }
    // }
  }

  paginationChange = (event, page) => {
    this.setState({ pageIndex: page + 1 }, this.getProducts);
  }

  handleChangeRowsPerPage = (event) => {
    this.setState({ pageIndex: 1, pageSize: event.target.value }, this.getProducts);
  }

  onChangeNFT = (e) => {
    // console.log(e.target.checked)
    this.setState({ nftAvailability: e.target.checked });
  }

  sliderHandleChange = (event, newValue, activeThumb) => {
    const minDistance = 10;

    if (!Array.isArray(newValue)) {
    return;
    }

    if (newValue[1] - newValue[0] < minDistance) {
        if (activeThumb === 0) {
            const clamped = Math.min(newValue[0], 100 - minDistance);
            // setValue2([clamped, clamped + minDistance]);
            this.setState({ sliderValue: [clamped, clamped + minDistance] });
        } else {
            const clamped = Math.max(newValue[1], minDistance);
            // setValue2([clamped - minDistance, clamped]);
            this.setState({ sliderValue: [clamped - minDistance, clamped] });
        }
    } else {
        // setValue2(newValue);
        this.setState({ sliderValue: newValue });
    }

    // this.priceChange(this.state.sliderValue);
  };

  

  render() {
    // console.log(this.state);
    return (
      <LoaderComp show={this.state.displayLoader} message={this.state.loaderMessage}>
        <div className="products-page">
          <div className="bg-light pt-3 text-center">
            <h1 className="section-heading">Products</h1>
          </div>
          <Container className="my-4">
            <Row>
              <Col lg={12} className="">
                <Breadcrumbs maxItems={2} aria-label="breadcrumb">
                  <MUILink underline="hover" color="inherit" href="/dashboard/home">
                    Home
                  </MUILink>
                  <span color="inherit">
                    Products
                  </span>
                </Breadcrumbs>
              </Col>
            </Row>
          </Container>
          <Container className="mb-4">
            <Row className="border-bottom">
              <div className="col-12 border-bottom">
                <Form className="products-filter">
                  <ProductsPriceFilter value={this.state} priceChange={this.priceChange} sliderHandleChange={this.sliderHandleChange} getProducts={this.getProducts} onChangeNFT={this.onChangeNFT} />
                  <ProductsFilter state={this.state}
                    selectCategory={this.selectCategory} />
                </Form>
                <Form className={`products-filter border-bottom sticky ${(this.state.isSticky) ? 'show' : ''}`} >
                  <Container>
                    <ProductsPriceFilter value={this.state} priceChange={this.priceChange} sliderHandleChange={this.sliderHandleChange} getProducts={this.getProducts} onChangeNFT={this.onChangeNFT} />
                    <ProductsFilter state={this.state}
                      selectCategory={this.selectCategory} />
                  </Container>
                </Form>
              </div>
              {this.state.displayLoader ?
                <Col md={8} lg={9} xl={10} className="no-products-text">
                  <span>{this.state.loaderMessage}</span></Col>
              :
              this.state.data.length > 0 ?
                <Col className="mt-3 col-12">
                  <Col className="products-pagination">
                    <div className="list-pagination">
                      <Pagination 
                          rowsPerPageOptions={[4, 8, 12]}
                          component="div"
                          count={this.state.totalSize}
                          rowsPerPage={this.state.pageSize}
                          page={this.state.pageIndex - 1}
                          onPageChange={this.paginationChange}
                          onRowsPerPageChange={this.handleChangeRowsPerPage} />
                    </div>
                  </Col>
                  <ProductsList data={this.state.data} state={this.state}  />
                  <Col className="products-pagination">
                    <div className="list-pagination">
                      <Pagination 
                          rowsPerPageOptions={[4, 8, 12]}
                          component="div"
                          count={this.state.totalSize}
                          rowsPerPage={this.state.pageSize}
                          page={this.state.pageIndex - 1}
                          onPageChange={this.paginationChange}
                          onRowsPerPageChange={this.handleChangeRowsPerPage} />
                    </div>
                    {/* <Pagination
                      activeClass="pagination-active"
                      activePage={this.state.pageIndex}
                      itemsCountPerPage={this.state.pageSize}
                      totalItemsCount={this.state.totalSize}
                      pageRangeDisplayed={5}
                      onChange={this.paginationChange}
                    /> */}
                  </Col>
                </Col>
                : <Col md={8} lg={9} xl={10} className="no-products-text">
                  <span>No Products found</span></Col>}
            </Row>

            {(this.state.user.hasOwnProperty("userReducer")) && <>
              <div className="my-4">
                <h3  className="text-left">Recommended Section</h3>
              </div>
              <Row>
                <Col className="mt-3 col-12">
                  <ProductsList data={this.state.recommendedForYou} state={this.state} />
                </Col>
              </Row>
            </>}
          </Container>
        </div>
      </LoaderComp>
    )
  }
};

const mapStateToProps = (state) => {
  return {
    user: state.userReducer
  }
}

export default withRouter(connect(mapStateToProps)(ProductsPage));