import React from 'react';
import { Col, Container, Row, Form, Button } from "react-bootstrap";
import { Link, withRouter } from "react-router-dom";
import { withAlert } from 'react-alert';
import Select from "react-select";
import { connect } from 'react-redux';
// import ImageUploading from 'react-images-uploading';
import { addSellerToRoles } from '../user';
import LoaderComp from '../components/Loader';
import ApiRequest from '../api_service/api_requests';
import '../assets/css/become-seller.css';

const colourStyles = {
    color: "red",
    control: (styles, { isFocused, isSelected }) => ({
        ...styles,
        padding: "0 8px",
        backgroundColor: "#fff",
        borderColor: isFocused || isSelected ? "#939cc4" : "#ced4da",
        boxShadow: isFocused && "0 0 0 0.2rem rgba(76, 88 ,140,.25)",
        //   borderColor: "#ced4da",
        ":hover": {
            ...styles[":active"],
            borderColor: "#ced4da"
        },
        ":focus": {
            boxShadow: "0 0 0 0.2rem rgba(76, 88 ,140,.25)"
        },
        //   maxWidth: "200px"
        borderRadius: 0
    }),
    option: (styles, { data, isDisabled, isFocused, isSelected }) => {
        return {
            ...styles,
            backgroundColor: isDisabled ? null : isSelected ? "#e1e4e6" : isFocused,
            ":active": {
                ...styles[":active"],
                backgroundColor: !isDisabled && "#e1e4e6"
            },
            ":hover": {
                ...styles[":active"],
                backgroundColor: "#e1e4e6"
            }
        };
    },
    placeholder: (styles) => ({
        ...styles,
        color: "#131C20"
    }),
    singleValue: (styles) => ({
        ...styles,
        color: "#131C20"
    }),
    menuList: (styles) => ({
        ...styles,
        color: "#282829"
    }),
    indicatorSeparator: (styles) => ({
        ...styles,
        display: "none"
    }),
    dropdownIndicator: (styles, { isFocused }) => ({
        ...styles,
        color: isFocused ? "#131C20" : "#131C20",
        ":hover": {
            ...styles[":active"],
            color: "#131C20"
        }
    })
};

class BecomeSeller extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            questions: [],

            categoriesList: [],
            subCategoriesList: [],
            imageLimit: 2,

            questionOne: '',
            questionTwo: '',
            bio: '',
            base64: '',
            selectedCategories: [],
            selectedSubCategories: [],

            loaderMessage: '',
            displayLoader: false
        }
    }

    componentDidMount() {
        this.getSellerQuestions();
        this.getCategories();
        this.getSubCategories();
    }

    getSellerQuestions = () => {
        this.setState({ loaderMessage: "Fetching Seller form data", displayLoader: true });
        ApiRequest.getSellerQuestions()
            .then(res => {
                if (res && res.data && res.data.length) {
                    this.setState({ questions: res.data });
                } else {
                    this.props.alert.error(res.message || "Failed to fetch Seller form data");
                }
                this.setState({ loaderMessage: "", displayLoader: false });
            })
            .catch((err) => {
                // console.log('Seller question fetch api error, ', err);
                this.setState({ loaderMessage: "", displayLoader: false });
                if (this.props && this.props.alert) {
                    this.props.alert.error(err.message || "Failed to fetch Seller form data");
                }
            })
    }

    getCategories = () => {
        ApiRequest.getCategory()
            .then(res => {
                if (res && res.length) {
                    let categoriesList = [];
                    res.forEach(data => {
                        if (data && data.id && data.categoryName) {
                            categoriesList.push({ label: data.categoryName, value: data.id });
                        }
                    })

                    this.setState({ categoriesList: categoriesList });
                }
            })
            .catch((err) => {
                // console.log('Categories fetch api error, ', err);
            })
    }

    getSubCategories = () => {
        ApiRequest.getSubCategory()
            .then(res => {
                if (res && res.length) {
                    let subCategoriesList = [];
                    res.forEach(data => {
                        if (data && data.id && data.subCategoryName) {
                            subCategoriesList.push({ label: data.subCategoryName, value: data.id });
                        }
                    })

                    this.setState({ subCategoriesList: subCategoriesList });
                }
            })
            .catch((err) => {
                // console.log('Sub Categories fetch api error, ', err);
            })
    }

    categoryChange = data => {
        this.setState({ selectedCategories: data });
    }

    subCategoryChange = data => {
        this.setState({ selectedSubCategories: data });
    }

    handleSubmit = () => {
        if (!(this.state.selectedCategories && this.state.selectedCategories.length)) {
            this.props.alert.error("Please select Category");
            return;
        }

        if (!(this.state.selectedSubCategories && this.state.selectedSubCategories.length)) {
            this.props.alert.error("Please select Sub Category");
            return;
        }

        let payload = {
            bio: this.state.bio,
            categories: this.state.selectedCategories.map(data => data.value),
            subCategories: this.state.selectedSubCategories.map(data => data.value),
            becomeSellerQuestions: [
                {
                    questionId: this.state.questions[0].questionId || 0,
                    answer: this.state.questionOne || ''
                },
                {
                    questionId: this.state.questions[2].questionId || 0,
                    answer: this.state.questionTwo || ''
                },
                {
                    questionId: this.state.questions[3].questionId || 0,
                    answer: this.state.base64 || ''
                }
            ]
        };

        this.setState({ loaderMessage: "Saving Seller information", displayLoader: true });

        ApiRequest.becomeSeller(payload)
            .then(res => {
                if (res && res.statusCode && res.statusCode === 200) {
                    this.props.dispatch(addSellerToRoles());
                    this.props.history.push('/dashboard/profile');
                    this.props.alert.success(res.message || "Seller information saved successfully");
                } else {
                    this.props.alert.error(res.message || "Failed to save Seller information");
                }
                this.setState({ loaderMessage: "", displayLoader: false });
            })
            .catch((err) => {
                // console.log('Wishlist api error, ', err);
                this.setState({ loaderMessage: "", displayLoader: false });
                if (this.props && this.props.alert) {
                    this.props.alert.error(err.message || "Failed to save Seller info");
                }
            })
    }

    formValueChange = event => {
        if (event.target.name && event.target.value) {
            this.setState({ [event.target.name]: event.target.value });
        }
    }

    fileSelectEvent = (event) => {
        if (event.target.files && event.target.files.length > 0) {
            // Type check
            if (event.target.files[0].type === "image/png" ||
                event.target.files[0].type === "image/jpg" || event.target.files[0].type === "image/jpeg") {
                // Size Check
                if (event.target.files[0].size / 1000 <= 5000) {
                    // let fileName = event.target.files[0].name;
                    // let fileType = event.target.files[0].type;
                    // let fileSize = event.target.files[0].size;

                    let fileReader = new FileReader();
                    fileReader.onload = function (fileLoadedEvent) {
                        let base64 = "";
                        base64 = fileLoadedEvent.target.result.replace(/^data:image\/(png|jpeg|jpg);base64,/, '');

                        this.setState({ base64: base64 });
                    }.bind(this);

                    fileReader.readAsDataURL(event.target.files[0]);
                } else {
                    this.props.alert.error("File which you have select is more than 5Mb. Please choose file less than 5Mb");
                    event.target.value = "";
                }
            } else {
                this.props.alert.error("Only Images are allowed");
                event.target.value = "";
            }
        } else {
            // console.log("No File selected");
        }
    }

    render() {
        return (
            <LoaderComp show={this.state.displayLoader} message={this.state.loaderMessage}>
                <div className="mb-5 register">
                    <Container className="pt-4 pb-5">
                        <Row>
                            <Col md={10} lg={6} xl={5} className="mx-auto">
                                <h1 className=" pt-5 pb-0 mb-5 section-heading">Become Seller</h1>
                                <div>
                                    <Form autoComplete="off">
                                        <Row>
                                            <Col md={11} className="mt-3 mt-md-0 m-b-1-rem">
                                                <Form.Group controlId="questionOne">
                                                    <Form.Label>
                                                        {this.state.questions[0] && this.state.questions[0].question ? this.state.questions[0].question : "Question"}
                                                    </Form.Label>
                                                    <Form.Control
                                                        className="rounded-0"
                                                        type="text"
                                                        name="questionOne"
                                                        onChange={this.formValueChange} />
                                                </Form.Group>
                                            </Col>
                                        </Row>

                                        <Row>
                                            <Col md={11} className="mt-3 mt-md-0 m-b-1-rem">
                                                <Form.Group controlId="category">
                                                    <Form.Label>
                                                        {this.state.questions[1] && this.state.questions[1].question ? `${this.state.questions[1].question} *` : "Question"}
                                                    </Form.Label>
                                                    <Select
                                                        autoComplete="off"
                                                        autoFocus="off"
                                                        styles={colourStyles}
                                                        placeholder="Select Category"
                                                        name="category"
                                                        options={this.state.categoriesList}
                                                        onChange={this.categoryChange}
                                                        isMulti={true}
                                                    />
                                                </Form.Group>
                                            </Col>
                                        </Row>

                                        <Row>
                                            <Col md={11} className="mt-3 mt-md-0 m-b-1-rem">
                                                <Form.Group controlId="subCategory">
                                                    <Form.Label>
                                                        {this.state.questions[4] && this.state.questions[4].question ? `${this.state.questions[4].question} *` : "Question"}
                                                    </Form.Label>
                                                    <Select
                                                        autoComplete="off"
                                                        autoFocus="off"
                                                        styles={colourStyles}
                                                        placeholder="Select SubCategory"
                                                        name="subCategory"
                                                        options={this.state.subCategoriesList}
                                                        onChange={this.subCategoryChange}
                                                        isMulti={true}
                                                    />
                                                </Form.Group>
                                            </Col>
                                        </Row>

                                        <Row>
                                            <Col md={11} className="mt-3 mt-md-0 m-b-1-rem">
                                                <Form.Group controlId="bio">
                                                    <Form.Label>Bio</Form.Label>
                                                    <Form.Control
                                                        className="rounded-0"
                                                        as="textarea"
                                                        rows={4}
                                                        name="bio"
                                                        onChange={this.formValueChange} />
                                                </Form.Group>
                                            </Col>
                                        </Row>

                                        <Row>
                                            <Col md={11} className="mt-3 mt-md-0 m-b-1-rem">
                                                <Form.Group controlId="questionTwo">
                                                    <Form.Label>
                                                        {this.state.questions[2] && this.state.questions[2].question ? this.state.questions[2].question : "Question"}
                                                    </Form.Label>
                                                    <Form.Control
                                                        className="rounded-0"
                                                        type="text"
                                                        name="questionTwo"
                                                        onChange={this.formValueChange} />
                                                </Form.Group>
                                            </Col>
                                        </Row>

                                        <Row>
                                            <Col md={11} className="mt-3 mt-md-0 m-b-1-rem">
                                                <Form.Group controlId="uploadImg">
                                                    <Form.Label>
                                                        {this.state.questions[3] && this.state.questions[3].question ? this.state.questions[3].question : "Question"}
                                                    </Form.Label>
                                                    <Form.Control
                                                        className="rounded-0 file-upload"
                                                        type="file"
                                                        name="uploadImg"
                                                        accept="image/png, image/jpg, image/jpeg, application/pdf"
                                                        onChange={this.fileSelectEvent} />
                                                </Form.Group>
                                            </Col>
                                        </Row>
                                    </Form>
                                </div>

                                <div className="buttons-form mt-4 ">
                                    <Link
                                        className="rounded-0 py-2 px-4 mr-3 btn btn-primary"
                                        variant="primary"
                                        to="/dashboard/profile">
                                        Cancel
                                    </Link>
                                    <Button
                                        onClick={this.handleSubmit}
                                        className="rounded-0 py-2 px-4"
                                        variant="primary">
                                        Make me Seller
                                    </Button>
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </div>
            </LoaderComp>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.userReducer
    }
}

export default withAlert()(withRouter(connect(mapStateToProps)(BecomeSeller)));