import React, { useState, useEffect } from "react";
import { Col, Container, Form, Row, Nav } from "react-bootstrap";
import { Link } from "react-router-dom";
// import Select from "react-select";
import {withAlert} from 'react-alert';
import {withRouter} from "react-router-dom";
import ProductsConfirm from "../components/products/ProductsConfirm"
//import { useHistory } from 'react-router-dom';
// import categories from "../data/categories";
import ApiRequest from "../api_service/api_requests";

import ReactTagInput from "@pathofdev/react-tag-input";
import "@pathofdev/react-tag-input/build/index.css";

import {makeStyles} from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import {useForm} from 'react-hook-form';

import LoaderComp from '../components/Loader';
import SimpleImageSlider from "react-simple-image-slider";
import {FirebaseImageUplaod} from "../components/firebase/ImageUpload";
import {ethers} from 'ethers';
import {ImmutableXClient, ImmutableMethodResults, MintableERC721TokenType} from '@imtbl/imx-sdk';

import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import MUILink from '@material-ui/core/Link';

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: "8px 0",
    minWidth: 120,
    width: "100%"
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  root: {
    flexGrow: 1,
  },
}));

const AddProduct = (props) => {
  const {
    register,
    handleSubmit,
    formState: {errors},
    getValues,
  } = useForm();

  const classes = useStyles();
  //const history = useHistory();
  const [loading, setLoading] = useState(true)
  const [tags, setTags] = useState([]);
  const [img, setImg] = useState();
  const [categories, setCategories] = useState([]);
  const [subCategories, setSubCategories] = useState([]);
  const [reqObj, setReqObj] = useState({
    "nftStatus": true,
    "forSale": true,
    "categoryId": [],
    "category": [],
    "subCategoryId": [],
    "subCategory": [],
    "tags": [],
    "rarity": null,
    "subProductImages": []
  });
  const [disabled, setDisabled] = useState(false);
  const [initials, setInitials] = useState("Add Product")

  const [productModal, setProductModal] = useState(false)
  const openModal = () => {
    setProductModal(true)
  }
  const closeModal = () => {
    setProductModal(false)
  }

  const [images, setImages] = useState([]);
  const [imageIndex, setImageIndex] = useState(0)

  const delay = ms => new Promise(
    resolve => setTimeout(resolve, ms)
  );

  let categoryId = null;
  const [loginUser, setLoginUser] = useState({});
  const [stripe, setStripe] = useState({});
  const [stripeStatus, setStripeStatus] = useState(0); // 0 mean no stripe account, 1 mean stripe account is invalid, 2 mean green light

  useEffect(() => {
    ApiRequest.getCategory()
      .then((res) => {
        if (res && res) {
          setCategories(res);
        } else {
          // console.log("There are some errors getting categories", res);
        }
      })
      .catch((err) => {
        // console.log("Error in get categories", err);
      });

    ApiRequest.getSubCategory()
      .then((res) => {
        if (res && res) {
          setSubCategories(res);
        } else {
          // console.log("There are some errors getting categories", res);
        }
      })
      .catch((err) => {
        // console.log("Error in get categories", err);
      });

    ApiRequest.viewUser()
      .then(res => {
        if (res && res.data && res.statusCode && res.statusCode === 200) {
          // console.log("View other user", res.data)
          setLoginUser({...res.data})
        } else {
          if (res && res.message) {
            // this.props.alert.error("Error fetching user details");
          }
        }
      })
      .catch((err) => {
        // console.log('Error fetching user details, ', err);
      })
  }, []);

  useEffect(() => {
    if (loginUser.stripeAccount != null) {
        setStripeStatus(1);
        ApiRequest.checkAccount({
            email: "",
            accountId: loginUser.stripeAccount
        })
        .then(res => {
            setStripe({...res.data})
            setLoading(false);
        })
        .catch((err) => {
            // console.log('Error fetching user details, ', err);
        })
    }
  }, [loginUser])

  useEffect(() => {
    if (stripe.charges_enabled && stripe.payouts_enabled) {
      setStripeStatus(2);
    }
  }, [stripe])

  const isClearable = true;

  const isSearchable = true;

  const colourStyles = {
    color: "red",
    control: (styles, {isFocused, isSelected}) => ({
      ...styles,
      padding: "0 8px",
      backgroundColor: "#fff",
      borderColor: isFocused || isSelected ? "#939cc4" : "#ced4da",
      boxShadow: isFocused && "0 0 0 0.2rem rgba(76, 88 ,140,.25)",
      //   borderColor: "#ced4da",
      ":hover": {
        ...styles[":active"],
        borderColor: "#ced4da",
      },
      ":focus": {
        boxShadow: "0 0 0 0.2rem rgba(76, 88 ,140,.25)",
      },
      //   maxWidth: "200px"
      borderRadius: 0,
    }),
    option: (styles, {data, isDisabled, isFocused, isSelected}) => {
      return {
        ...styles,
        backgroundColor: isDisabled ? null : isSelected ? "#131C20" : isFocused,
        ":active": {
          ...styles[":active"],
          backgroundColor: !isDisabled && "#131C20",
        },
        // ":hover": {
        //   ...styles[":active"],
        //   backgroundColor: !isDisabled && "#eee"
        // }
      };
    },
    placeholder: (styles) => ({
      ...styles,
      color: "#131C20",
    }),
    singleValue: (styles) => ({
      ...styles,
      color: "#131C20",
    }),
    menuList: (styles) => ({
      ...styles,
      color: "#282829",
    }),
    indicatorSeparator: (styles) => ({
      ...styles,
      display: "none",
    }),
    dropdownIndicator: (styles, {isFocused}) => ({
      ...styles,
      color: isFocused ? "#131C20" : "#131C20",
      ":hover": {
        ...styles[":active"],
        color: "#131C20",
      },
    }),
  };

  const [thumbnail, setThumbnail] = useState(null)
  const makeThumbnail = () => {
    setThumbnail(imageIndex)
  }

  const handleInputs = (event) => {
    // console.log(event.target.files[0].type.split("/")[1]);
    let File = event?.target?.files[0];
    let obj = reqObj;
    let fileType = File.type.split("/")[1];
    obj["fileType"] = fileType;
    obj["fileExtension"] = `.${fileType}`;

    let imagesList = images;
    setImages([...[]]);
    // console.log(imagesList);
    if (File) {
      let reader = new FileReader();
      reader.onload = async () => {
        let result = reader.result;
        imagesList = imagesList.concat([{
          url: result,
          fileType: fileType,
          fileExtension: fileType
        }]); // imagesList.push({url: result})
        // var strImage = result.replace(/^data:application\/[a-z]+;base64,/, "");
        // setImg(strImage);
        // console.log("Image ", strImage.split("base64,")[1]);
        // obj["productImage"] = strImage.split("base64,")[1];

        await delay(1);
        // console.log(obj);
        // console.log(imagesList);
        setImages([...imagesList]);
        if (thumbnail == null) {
          setThumbnail(0)
        }
        // setReqObj({...obj});
        // fieldsValidation(obj);
      };
      reader.readAsDataURL(File);
    }
  };

  const showConfirm = () => {
    setProductModal(true);
  }

  const handleFields = (e, field) => {
    // console.log(e,field);

    let obj = reqObj;

    if (field === "categoryId" || field === "subCategoryId") {
      // console.log(field)
      // console.log(e)
      if (e) {
        let valueAry = (e.target.value).split(",");
        // console.log(valueAry);
        obj[field] = [valueAry[0]];
        if (field === "categoryId") {
          obj.category = [valueAry[1]];
        } else {
          obj.subCategory = [valueAry[1]];
        }
      } else if (e == null) {
        delete obj[field];
        if (field === "categoryId") {
          delete obj.category;
        } else {
          delete obj.subCategory;
        }
      }
    } else if (field === "tags") {
      setTags(e);
      if (e.length > 0) {
        obj[field] = e;
      } else {
        delete obj[field];
      }
    } else
      if (field === "nftStatus" || field === "forSale") {
        obj[field] = e.target.checked;
      } else {
        if (e.target.value == "") {
          delete obj[field];
        } else {
          obj[field] = e.target.value.trim();
        }
      }

    // obj["nftStatus"] = true;
    setReqObj({...obj});
    // console.log(obj);
    // fieldsValidation(obj);
  };

  const fieldsValidation = (fieldsObj) => {
    if ("tags" in fieldsObj == false) {
      setDisabled(true);
    } else if ("productName" in fieldsObj == false) {
      setDisabled(true);
    } else if ("categoryId" in fieldsObj == false) {
      setDisabled(true);
    } else if ("subCategoryId" in fieldsObj == false) {
      setDisabled(true);
    } else if ("price" in fieldsObj == false) {
      setDisabled(true);
    } else if ("link" in fieldsObj == false) {
      setDisabled(true);
    } else if ("description" in fieldsObj == false) {
      setDisabled(true);
    } else if ("productImage" in fieldsObj == false) {
      // console.log("inside product Image");
      setDisabled(true);
    } else {
      setDisabled(false);
    }
  };

  const margins = {
    top: {maringTop: "15px"},
    left: {maringLeft: "10px"},
  };

  const removeImage = async () => {
    let updateImagesList = images.filter((value, index) => {
      return index != imageIndex;
    })

    setImages([...[]]);

    await delay(1);

    if (updateImagesList.length < 2) {
      setImageIndex(0)
    }
    setImages([...updateImagesList]);
  }

  const slidePhoto = (idx) => {
    setImageIndex(idx)
  }

  const slidePhotoByNav = (toRight) => {
    if (toRight) {
      if (imageIndex === images.length - 1) {
        setImageIndex(0)
      } else {
        setImageIndex(imageIndex + 1)
      }
    } else {
      if (imageIndex === 0) {
        setImageIndex(images.length - 1)
      } else {
        setImageIndex(imageIndex - 1)
      }
    }
  }

  const onSubmit = (data) => {
    setLoading(true)
    // console.log(reqObj)
    // console.log(data)
    // console.log(images)
    // console.log(imageIndex)

    let obj = reqObj;
    obj["fileType"] = images[thumbnail].fileType;
    obj["fileExtension"] = images[thumbnail].fileExtension;
    obj["productImage"] = (images[thumbnail].url).replace("data:image/" + images[thumbnail].fileType + ";base64,", "");
    obj["productName"] = data.productName;

    obj["category"] = [((data.categoryId).split(","))[1]];
    obj["categoryId"] = [((data.categoryId).split(","))[0]];

    obj["subCategory"] = [((data.subCategoryId).split(","))[1]];
    obj["subCategoryId"] = [((data.subCategoryId).split(","))[0]];

    obj["price"] = data.price;
    obj["quantity"] = reqObj.quantity;
    obj["link"] = data.link;
    obj["tags"] = reqObj.tags;
    obj["rarity"] = reqObj.rarity;
    obj["nftStatus"] = reqObj.nftStatus;
    obj["forSale"] = reqObj.forSale;
    obj["description"] = data.description;

    obj["subProductImages"] = images;
    obj["thumbnail"] = thumbnail;

    setReqObj({...obj});
    setLoading(false)
    showConfirm();
  };

  const saveProduct = async () => {
    setLoading(true)
    setInitials("Loading....");
    setDisabled(true);

    let prodParams = reqObj;
    prodParams["quantity"] = 1;

    let imagesFilteredList = images.filter((value, index) => {
      return index != thumbnail
    })
    let subProductImagesFirebaseLink = [];

    if (imagesFilteredList.length > 0) {
      imagesFilteredList.map((image, index) => {
        FirebaseImageUplaod(image, (url) => {
          image.url = url;
          // console.log("image", url);
          subProductImagesFirebaseLink.push(url);

          if (index >= imagesFilteredList.length - 1) {
            // console.log("images", [...images]);

            prodParams["subProductImages"] = [...subProductImagesFirebaseLink];

            FirebaseImageUplaod({
              url: prodParams['productImage'],
              fileType: prodParams['fileType'],
              fileExtension: prodParams['fileExtension']
            }, (url) => {
              // to upload thumbnail photo
              // console.log("image", url);

              prodParams['productImage'] = url;
              delete prodParams['fileType'];
              if (prodParams["nftStatus"] == true) {
                prodParams["forSale"] = false;
                prodParams["quantity"] = 0;
              }
              ApiRequest.addProduct(prodParams)
                .then(async (res) => {
                  if (res && res.statusCode && res.statusCode === 200) {
                    if (prodParams["nftStatus"] == true) {
                      await mintv2(res.data, "MV4F");
                      props.history.push('/dashboard/nfts');
                    } else {
                      props.history.push('/dashboard/seller-products');

                    }
                    // console.log("Successfully Submit", res.data);
                    props.alert.success("Product Successfully Added");

                  } else {
                    // console.log("Something wrong see!!!");
                    alert("Product Already exists!");
                    setInitials("Add Product");
                    setDisabled(false);
                  }
                })
                .catch((err) => {
                  // console.log("Add product api error, ", err);
                  setDisabled(false);
                  setInitials("Add Product");
                });

              setLoading(false);
            })
          }
        })
      });
    } else {
      prodParams["subProductImages"] = [...[]];

      FirebaseImageUplaod({
        url: prodParams['productImage'],
        fileType: prodParams['fileType'],
        fileExtension: prodParams['fileExtension']
      }, (url) => {
        // to upload thumbnail photo
        // console.log("image", url);

        prodParams['productImage'] = url;
        delete prodParams['fileType'];
        ApiRequest.addProduct(prodParams)
          .then(async (res) => {
            if (res && res.statusCode && res.statusCode === 200) {
              // console.log("Successfully Submit", res.data);
              if (prodParams["nftStatus"] == true) {
                await mintv2(res.data, "MV4F");
                props.history.push('/dashboard/nfts');
              } else {
                props.history.push('/dashboard/seller-products');

              }
              props.alert.success("Product Successfully Added");
            } else {
              // console.log("Something wrong see!!!");
              alert("Product Already exists!");
              setInitials("Add Product");
              setDisabled(false);
            }
          })
          .catch((err) => {
            // console.log("Add product api error, ", err);
            setDisabled(false);
            setInitials("Add Product");
          });

        setLoading(false);
      })
    }
    const wallet = localStorage.getItem('WALLET_ADDRESS');
    const sellNFT = async (sellAmount, sellTokenId, sellTokenAddress) => {
      await props.link.sell({
        amount: sellAmount,
        tokenId: sellTokenId,
        tokenAddress: sellTokenAddress
      })
    };
    const mintv2 = async (mintTokenId, mintBlueprint) => {
      // initialise a client with the minter for your NFT smart contract
      const provider = new ethers.providers.JsonRpcProvider(`https://eth-ropsten.alchemyapi.io/v2/${process.env.REACT_APP_ALCHEMY_API_KEY}`);

      //if you want to mint on a back end server you can also provide the private key of your wallet directly to the minter. 
      //Please note: you should never share your private key and so ensure this is only done on a server that is not accessible from the internet
      const minterPrivateKey = process.env.REACT_APP_MINTER_PK ?? ''; // registered minter for your contract
      const minter = new ethers.Wallet(minterPrivateKey).connect(provider);
      //const minter = new ethers.providers.Web3Provider(window.ethereum).getSigner(); //get Signature from Metamask wallet
      const publicApiUrl = process.env.REACT_APP_ROPSTEN_ENV_URL ?? '';
      const starkContractAddress = process.env.REACT_APP_ROPSTEN_STARK_CONTRACT_ADDRESS ?? '';
      const registrationContractAddress = process.env.REACT_APP_ROPSTEN_REGISTRATION_ADDRESS ?? '';
      const minterClient = await ImmutableXClient.build({
        publicApiUrl,
        signer: minter,
        starkContractAddress,
        registrationContractAddress,
      })

      // mint any number of NFTs to specified wallet address (must be registered on Immutable X first)
      const token_address = process.env.REACT_APP_TOKEN_ADDRESS ?? ''; // contract registered by Immutable
      const royaltyRecieverAddress = process.env.REACT_APP_ROYALTY_ADDRESS ?? '';
      const tokenReceiverAddress = process.env.REACT_APP_TOKEN_RECEIVER_ADDRESS ?? '';
      const wallet = localStorage.getItem('WALLET_ADDRESS');
      const result = await minterClient.mintV2([{
        users: [{
          etherKey: wallet,
          tokens: [{
            id: mintTokenId.toString(),
            blueprint: mintBlueprint,
            // overriding royalties for specific token
            royalties: [{
              recipient: wallet.toLowerCase(),
              percentage: 3.5
            }],
          }]
        }],
        contractAddress: token_address.toLowerCase(),

        // globally set royalties
        royalties: [{
          recipient: wallet.toLowerCase(),
          percentage: 4.0
        }]
      }]
      );
      await sellNFT(parseFloat(reqObj["quantity"]), mintTokenId.toString(), token_address.toLowerCase());
      // console.log(`Token minted: ${result}`);
    };
    const random = () => {
      const min = 1;
      const max = 1000000000;
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    // const imageName = generateImageName(30);
    // let prodParams = reqObj;
    // const metadata = {
    //   contentType: `image/${prodParams['fileType']}`,
    // };
    // const storage = getStorage();
    // let enviroment = 'staging';
    // if (process.env.REACT_APP_NODE_ENV === 'production') {
    //   enviroment = 'production';
    // }
    // const storageRef = ref(storage, `${enviroment}/${imageName}${prodParams['fileExtension']}`);
    // await uploadString(storageRef, prodParams['productImage'], 'base64', metadata).then((snapshot) => {
    //   // console.log('Uploaded a base64 string!');
    // });
    // await getDownloadURL(storageRef)
    // .then((url) => {
    //   // console.log('url', url);
    //   prodParams['productImage'] = url;
    //   delete prodParams['fileType'];
    //   ApiRequest.addProduct(prodParams)
    //   .then((res) => {
    //     if (res && res.statusCode && res.statusCode === 200) {
    //       // console.log("Successfully Submit", res.data);
    //       props.alert.success("Product Successfully Added");
    //       props.history.push('/dashboard/seller-products');

    //     } else {
    //       // console.log("Something wrong see!!!");
    //       alert("Product Already exists!");
    //       setInitials("Add Product");
    //       setDisabled(false);
    //     }
    //   })
    //   .catch((err) => {
    //     // console.log("Add product api error, ", err);
    //     setDisabled(false);
    //     setInitials("Add Product");
    //   });
    // })
    // .catch((error) => {
    //   // A full list of error codes is available at
    //   // https://firebase.google.com/docs/storage/web/handle-errors
    //   // eslint-disable-next-line default-case
    //   switch (error.code) {
    //     case 'storage/object-not-found':
    //       // File doesn't exist
    //       break;
    //     case 'storage/unauthorized':
    //       // User doesn't have permission to access the object
    //       break;
    //     case 'storage/canceled':
    //       // User canceled the upload
    //       break;

    //     // ...

    //     case 'storage/unknown':
    //       // Unknown error occurred, inspect the server response
    //       break;
    //   }
    // });
  };

  const rarities = [
    "Common",
    "Uncommon",
    "Rare",
    "Super Rare",
    "Epic",
    "Legendary",
  ];

  return (
    <LoaderComp show={loading} message={""}>
      {stripeStatus === 2 ? 
      <div className="checkout">
        <div className="bg-light pt-3 text-center">
          <h1 className="section-heading">Add Product</h1>
        </div>
        <Container className="my-4">
          <Row>
            <Col lg={12} className="">
              <Breadcrumbs maxItems={2} aria-label="breadcrumb">
                <MUILink underline="hover" color="inherit" href="/dashboard/home">
                  Home
                </MUILink>
                <span color="inherit">
                  Add Product
                </span>
              </Breadcrumbs>
            </Col>
          </Row>
        </Container>
        <Container className="my-4">
          <Row>
            <Col lg={12} className="">
              <Form className="py-4" onSubmit={handleSubmit(onSubmit)}>
                <Grid container spacing={3}>
                  <Grid item xs={12} sm={6}>
                    <Form.Group controlId="Image">
                      <FormControl variant="outlined" className={classes.formControl}>
                        <input
                          type="file"
                          accept="*"
                          id="image-upload"
                          style={{display: "none"}}
                          onChange={(e) => handleInputs(e)}
                        ></input>
                        <div className="image-upload-control" style={{justifyContent: "center"}}>
                          {images.length > 0 ? <SimpleImageSlider
                            width={"100%"}
                            height={300}
                            images={images}
                            showBullets={(images.length > 1) ? true : false}
                            showNavs={(images.length > 1) ? true : false}
                            startIndex={imageIndex}
                            onClick={(idx, event) => slidePhoto(idx)}
                            onClickNav={(toright) => slidePhotoByNav(toright)}
                            onClickBullets={(idx) => slidePhoto(idx)}
                          />
                            :
                            <div className="image-slide-empty-container"></div>
                          }
                          {/* {img && <img src={img} width="50%" />} */}
                          {images.length > 0 && <span className="remove-img" onClick={removeImage}>x</span>}
                          {images.length > 0 && thumbnail != imageIndex && <span className="set-thumbnail" onClick={makeThumbnail}>Set as thumbnail</span>}
                          <label htmlFor="image-upload" className="mt-4 image-upload-label">Pick Picture</label>
                        </div>
                        {errors.image && <p className="error-text">Image is required.</p>}
                      </FormControl>
                    </Form.Group>
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <Form.Group controlId="ProductName">
                      <FormControl variant="outlined" className={classes.formControl}>
                        <TextField
                          label="Product Name"
                          type="text"
                          name="productName"
                          variant="outlined"
                          className="wp-100"
                          required
                          // onChange={(e) => handleFields(e, "productName")}
                          {...register('productName', {required: true})}
                        />
                        {errors.productName && <p className="error-text">Product Name is required.</p>}
                      </FormControl>
                      {/* <Form.Control
                      className="rounded-0"
                      type="text"
                      placeholder="Product Name to display"
                      onChange={(e) => handleFields(e, "productName")}
                    /> */}
                    </Form.Group>
                    <Form.Group controlId="SelectCategory">
                      <FormControl variant="outlined" className={classes.formControl}>
                        <InputLabel id="category-dropdown-label" required>Category</InputLabel>
                        <Select
                          label="Category"
                          labelId="category-dropdown-label"
                          id="category-dropdown"
                          value={(reqObj.categoryId.length > 0) ? reqObj.categoryId[0] + "," + reqObj.category[0] : ""}
                          variant="outlined"
                          {...register('categoryId', {required: true})}
                          // onChange={(e) => categoryId = e}
                          onChange={(e) => handleFields(e, "categoryId")}
                        >
                          {/* <MenuItem value="">
                            <em>None</em>
                          </MenuItem> */}
                          {categories.map((category, index) => (
                            <MenuItem key={index} value={category.id + "," + category.categoryName}>{category.categoryName}</MenuItem>
                          ))}
                        </Select>
                        {errors.categoryId && <p className="error-text">Category is required.</p>}
                      </FormControl>
                      {/* <Select
                        styles={colourStyles}
                        placeholder="Select Category"
                        className="coin-select"
                        classNamePrefix="select"
                        defaultValue={categories[0]}
                        isClearable={isClearable}
                        isSearchable={isSearchable}
                        onChange={(e) => handleFields(e, "categoryId")}
                        name="category"
                        options={categories}
                        getOptionLabel={(coin) => coin.categoryName}
                        getOptionValue={(coin) => coin.id}
                      /> */}
                    </Form.Group>
                    <Form.Group controlId="SelectSubCategory">
                      {/* { (getValues("categoryId") != "") ? getValues("categoryId") : 'none'} */}

                      <FormControl variant="outlined" className={classes.formControl}>
                        <InputLabel id="sub-category-dropdown-label" required>Sub Category</InputLabel>
                        <Select
                          label="Sub Category"
                          labelId="sub-category-dropdown-label"
                          id="sub-category-dropdown"
                          // onChange={(e) => handleFields(e, "subCategoryId")}
                          value={(reqObj.subCategoryId.length > 0) ? reqObj.subCategoryId[0] + "," + reqObj.subCategory[0] : ""}
                          variant="outlined"
                          {...register('subCategoryId', {required: true})}
                          onChange={(e) => handleFields(e, "subCategoryId")}
                        >
                          {
                            // (reqObj.categoryId.length > 0 ) && 
                            subCategories.map((subCategory, index) => {
                              if (parseInt(reqObj.categoryId[0]) === subCategory.categoryId) {
                                return <MenuItem key={index} value={subCategory.id + "," + subCategory.subCategoryName}>{subCategory.subCategoryName}</MenuItem>
                              }
                            })}
                        </Select>
                        {errors.subCategoryId && <p className="error-text">Sub category is required.</p>}
                      </FormControl>
                      {/* <Select
                        styles={colourStyles}
                        placeholder="Select Sub Category"
                        className="coin-select"
                        classNamePrefix="select"
                        isClearable={isClearable}
                        isSearchable={isSearchable}
                        onChange={(e) => handleFields(e, "subCategoryId")}
                        name="subCategory"
                        options={subCategories}
                        getOptionLabel={(coin) => coin.subCategoryName}
                        getOptionValue={(coin) => coin.id}
                      /> */}
                    </Form.Group>
                    {reqObj.nftStatus == false &&
                      <Form.Group controlId="Price">
                        <FormControl variant="outlined" className={classes.formControl}>
                          <TextField
                            label="Price (SGD)"
                            type="number"
                            name="price"
                            variant="outlined"
                            className="wp-100"
                            required
                            // onChange={(e) => handleFields(e, "price")}
                            {...register('price', {required: true, min: 0})}
                          />
                          {errors.price && errors.price.type === "required" && <p className="error-text">Price is required.</p>}
                          {errors.price && errors.price.type === "min" && <p className="error-text">Price should be greater than 0.</p>}
                        </FormControl>
                        {/* <Form.Control
                          className="rounded-0 mt-2"
                          type="number"
                          onChange={(e) => handleFields(e, "price")}
                          placeholder="Price"
                        /> */}
                      </Form.Group>
 
                    }
                    {reqObj.nftStatus == true &&
                      <Form.Group controlId="Quantity">
                        <FormControl variant="outlined" className={classes.formControl}>
                          <TextField
                            label="Price (ETH)"
                            name="quantity"
                            variant="outlined"
                            className="wp-100"
                            required
                            // onChange={(e) => handleFields(e, "quantity")}
                            {...register('quantity', {})}
                          />
                        </FormControl>
                        {/* <Form.Control
                          className="rounded-0 mt-2"
                          type="number"
                          onChange={(e) => handleFields(e, "price")}
                          placeholder="Price"
                        /> */}
                      </Form.Group>
                    }
                    {reqObj.nftStatus == true &&
                      <Form.Group controlId="SelectRarity">
                        <FormControl variant="outlined" className={classes.formControl}>
                          <InputLabel id="rarity-dropdown-label" required>Rarity</InputLabel>
                          <Select
                            label="Rarity"
                            labelId="rarity-dropdown-label"
                            id="rarity-dropdown"
                            value={(reqObj.rarity != null) ? reqObj.rarity : ""}
                            variant="outlined"
                            {...register('rarity', {required: true})}
                            // onChange={(e) => categoryId = e}
                            onChange={(e) => handleFields(e, "rarity")}
                          >
                            {rarities.map((rarity, index) => (
                              <MenuItem key={index} value={rarity}>{rarity}</MenuItem>
                            ))}
                          </Select>
                        </FormControl>
                      </Form.Group>

                    }

                    <Form.Group controlId="Link">
                      <FormControl variant="outlined" className={classes.formControl}>
                        <TextField
                          label="Link"
                          type="text"
                          name="link"
                          variant="outlined"
                          className="wp-100"
                          required
                          // onChange={(e) => handleFields(e, "link")}
                          {...register('link', {required: true})}
                        />
                        {errors.link && <p className="error-text">Link is required.</p>}
                      </FormControl>
                      {/* <Form.Control
                        className="rounded-0 mt-2"
                        type="text"
                        onChange={(e) => handleFields(e, "link")}
                        placeholder="Product Link"
                      /> */}
                    </Form.Group>
                  </Grid>
                  <Grid item xs={12} sm={12}>
                    <Form.Group controlId="Tags">
                      <FormControl variant="outlined" className={classes.formControl}>
                        <ReactTagInput
                          tags={tags}
                          onChange={(newTags) => handleFields(newTags, "tags")}
                        //onChange={(newTags) => setTags(newTags)} 
                        />
                        <label className={"tagsLabel"}>Tags</label>
                      </FormControl>
                    </Form.Group>
                  </Grid>
                  <Grid item xs={12}>
                    <Form.Group className="mt-2">
                      <FormControlLabel
                        // checked={reqObj.forSale}
                        control={<Switch color="primary" defaultChecked />}
                        label="For sale"
                        labelPlacement="start"
                        onChange={(e) => handleFields(e, "forSale")}
                      // {...register('forSale')}
                      />

                        <FormControlLabel
                          // checked={reqObj.nftStatus}
                          control={<Switch color="primary" defaultChecked />}
                          label="NFT Available"
                          labelPlacement="start"
                          onChange={(e) => handleFields(e, "nftStatus")}
                        // {...register('nftStatus')}
                        />
                      </Form.Group>
                    </Grid>
                    <Grid item xs={12}>
                      <Form.Group controlId="Description">
                        <FormControl variant="outlined" className={classes.formControl}>
                          <textarea
                            // value={(e) => // console.log("Text Area value", e)}
                            // onChange={(e) => handleFields(e, "description")}
                            rows={5}
                            className="textareaField wp-100"
                            {...register('description')}
                          />
                          <label className="textareaLabel">Description</label>
                        </FormControl>
                      </Form.Group>
                    </Grid>
                    <Grid item xs={12} className="text-center mt-3">
                      {/* <Button
                        className="rounded-0 py-2 px-4 mt-4"
                        variant="primary"
                        disabled={disabled}
                        onClick={handleSubmit}
                      >
                        {initials}
                      </Button> */}
                      <Button
                        type="submit"
                        className=""
                        variant="contained"
                        color="primary"
                        // onClick={showConfirm}
                        disabled={disabled}
                      >
                        {initials}
                      </Button>
                    </Grid>
                  </Grid>
                </Form>
              </Col>
            </Row>
          </Container>
          {(productModal) && <ProductsConfirm open={productModal} openModal={openModal} closeModal={closeModal} data={reqObj} handleSubmit={saveProduct} />}
        </div>
      :
      stripeStatus === 1 ?
        <div className="checkout">
          <div className="bg-light pt-3 text-center">
            <h1 className="section-heading">Add Product</h1>
          </div>
          <Breadcrumbs maxItems={2} aria-label="breadcrumb">
            <MUILink underline="hover" color="inherit" href="/dashboard/home">
              Home
            </MUILink>
            <span color="inherit">
              Add Product
            </span>
          </Breadcrumbs>
          <Container className="my-4">
            <Row>
              <Col lg={12} className="">
                <Form className="py-4">
                  <Grid container spacing={3}>
                    <Grid item xs={12} sm={12}>
                      <p className="pt-3 text-center">You can't add the product right now.</p>
                      <p className="pt-3 text-center">Your stripe account is invalid. Please go to stripe and complete your account first.</p>
                      <Nav.Link as={Link} to="/dashboard/profile#Transactions" className="text-center">
                        <Button
                          type="button"
                          className=""
                          variant="contained"
                          color="primary"
                        >
                          {"Check Stripe"}
                        </Button>
                      </Nav.Link>
                    </Grid>
                  </Grid>
                </Form>
              </Col>
            </Row>
          </Container>
        </div>
      :
        <div className="checkout">
          <div className="bg-light pt-3 text-center">
            <h1 className="section-heading">Add Product</h1>
          </div>
          <Container className="my-4">
            <Row>
              <Col lg={12} className="">
                <Form className="py-4">
                  <Grid container spacing={3}>
                    <Grid item xs={12} sm={12}>
                      <p className="pt-3 text-center">You can't add the product right now.</p>
                      <p className="pt-3 text-center">Your don't have stripe account, please create it first.</p>
                      <Nav.Link as={Link} to="/dashboard/profile#Transactions" className="text-center">
                        <Button
                          type="button"
                          className=""
                          variant="contained"
                          color="primary"
                        >
                          {"Check Stripe"}
                        </Button>
                      </Nav.Link>
                    </Grid>
                  </Grid>
                </Form>
              </Col>
            </Row>
          </Container>
        </div>
      }
    </LoaderComp>
  );
};

//export default AddProduct;
export default withRouter(withAlert()(AddProduct));
