/* eslint-disable jsx-a11y/alt-text */
import React, { useState, useEffect } from "react";
import "./styles.css";
// import { FaEdit } from "react-icons/fa";
// import { AiFillDelete } from "react-icons/ai";
import "bootstrap/dist/css/bootstrap.css";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
// import BootstrapTable from "react-bootstrap-table-next";
// import paginationFactory from "react-bootstrap-table2-paginator";
import ApiRequest from "../api_service/api_requests";
import { withAlert } from 'react-alert';
import { withRouter } from "react-router-dom";
import { Link } from "react-router-dom";
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { Col, Container, Form, Row, Nav } from "react-bootstrap";
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from "@material-ui/icons/Edit"

import moment from 'moment'

import Pagination from '@material-ui/core/TablePagination';

import BuyerConfirm from "../components/profile/buyerConfirm";
// import { filter } from "underscore";
// import Stack from '@mui/material/Stack';

import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import MUILink from '@material-ui/core/Link';

const SellerProducts = (props) => {

  let popupData = {
    title: "Confirmation",
    msg: "Are you sure to delete this item?"
  }
  const [AllProducts, setAllProducts] = useState([]);
  const [loading, setLoading] = useState(true);
  const [deleted, setDeleted] = useState(false);
  const [message, setMessage] = useState("Data fetching...")
  const [filters, setFilters] = useState({
    nftAvailability: true,
    priceFrom: 0,
    priceTo: 0,
    categories: [0],
    subCategories: [0],
    tags: [
      "string"
    ],
    orderBy: 0,
    pageIndex: 1,
    pageSize: 10,
  });

  const [showSellerConfrim, setShowSellerConfirm] = useState(false)
  const [sellerConfrim, setSellerConfirm] = useState(false)
  const [deleteId, setDeleteId] = useState(0)

  useEffect(() => {
    // console.log("here")
    ApiRequest.getsellerProducts(0, filters).then((res) => {
      // console.log(res.data)
      setAllProducts(res.data.data);
      setLoading(false);
      setDeleted(false);

      if (res.data.data.length === 0) {
        setMessage("Found no product.")
      }
    });
  }, [deleted, filters]);

  const handleDelete = (e) => {
    // console.log("handle Delete click", e);
    setShowSellerConfirm(true);
    setDeleteId(e);
    // if (window.confirm("You want to Delete Product")) {
    //   ApiRequest.deleteProduct(e).then((res) => {
    //     if (res.statusCode === 200) {
    //       setDeleted(true);
    //       props.alert.success("Product Successfully Delete");
    //       //window.location.reload();
    //     } else {
    //       alert("Something went Wrong!");
    //     }
    //   });
    // }
  };

  useEffect(() => {
    if (sellerConfrim) {
      ApiRequest.deleteProduct(deleteId).then((res) => {
        if (res.statusCode === 200) {
          setDeleted(true);
          setShowSellerConfirm(false);
          props.alert.success("Product Successfully Delete");
          //window.location.reload();
        } else {
          props.alert.error("Something went Wrong!");
          // alert("Something went Wrong!");
        }
      });
    }
  }, [sellerConfrim])

  const userFeedback = (result) => {
    if (!result) {
      setShowSellerConfirm(false);
    }
    setSellerConfirm(result)
  }

  const saveStatus = (e, id, field, anotherToggle) => {
    // console.log(e.target.checked)
    // console.log(id)
    // console.log(AllProducts)

    let prodParams = {};
    prodParams[field] = e.target.checked;

    if (field === "forSale") {
      prodParams["nftStatus"] = anotherToggle;
    } 
    if (field === "nftStatus") {
      prodParams["forSale"] = anotherToggle;
    }

    prodParams["productId"] = id;
    prodParams["DeletedImages"] = [];
    prodParams["SubProductImages"] = [];

    ApiRequest.updateProduct(prodParams)
      .then((res) => {
        if (res && res.statusCode && res.statusCode === 200) {
          props.alert.success("Product Successfully Update");
          
          let products = AllProducts;
          
          products.map((value) => {
            // console.log(value);
            if ( value.productId === id ) {
              value[field] = !value[field];
            }
          });

          setAllProducts([...products])

        } else {
          // console.log("Something wrong see!!!");
          props.alert.error("Something went Wrong!");
          // alert("Something went wrong");
        }
      })
      .catch((err) => {
        // console.log("Add product api error, ", err);
        // alert("Something went wrong");
        props.alert.error("Something went Wrong!");
      });
  }

  // const columns = [
  //   {
  //     dataField: "",
  //     text: "#",
  //     formatter: (cell, row, ind) => {
  //       return <div>{ind + 1}</div>;
  //     },
  //     headerStyle: (colum, colIndex) => {
  //       return { width: "50px", backgroundColor: "#131c20", color: "white" };
  //     },
  //   },
  //   {
  //     dataField: "tags",
  //     text: "Actions",
  //     formatter: (cell, row) => {
  //       let prodId = row.productId;
  //       return (
  //         <div>
  //           <Link to={`/dashboard/update-product/${row.productId}`}>
  //             <Tooltip disableFocusListener title="Edit product">
  //               <IconButton className="p-0">
  //                 <EditIcon/>
  //               </IconButton>
  //             </Tooltip>
  //           </Link>
  //           <a
  //             style={{ marginLeft: "10px" }}
  //             onClick={() => handleDelete(prodId)}
  //           >
  //             <Tooltip disableFocusListener title="Delete product">
  //               <IconButton className="p-0">
  //                 <DeleteIcon/>
  //               </IconButton>
  //             </Tooltip>
  //           </a>
  //         </div>
  //       );
  //     },
  //   },
  //   {
  //     dataField: "productName",
  //     text: "Name",
  //     formatter: (cell, row) => {
  //       return (
  //         <Link to={`/dashboard/product-detail/${row.productId}`}>
  //           {row.productName}
  //         </Link>
  //       );
  //     },
  //   },
  //   {
  //     dataField: "productImage",
  //     text: "Image",
  //     formatter: (cell, row) => {
  //       // let extention = row.extention && row.extention != "" ? row.extention.split(".")[1] : "";
  //       return (
  //         <Link to={`/dashboard/product-detail/${row.productId}`}>
  //           <img
  //             width="65"
  //             height="50"
  //             // src={`data:image/${extention};base64,${cell}`}
  //             src={row.productImage}
  //           />
  //         </Link>
  //       );
  //     },
  //   },
  //   {
  //     dataField: "price",
  //     text: "Price",
  //   },
  //   {
  //     dataField: "forSale",
  //     text: "Sale",
  //     formatter: (cell, row) => {
  //       return (
  //         <FormControlLabel
  //           checked={(!row.forSale) ? false : true}
  //           control={<Switch color="primary" />}
  //           onChange={(e) => saveStatus(e, row.productId, "forSale", row.nftStatus)}
  //         />
  //       );
  //     },
  //   },
  //   {
  //     dataField: "nftStatus",
  //     text: "NFT",
  //     formatter: (cell, row) => {
  //       return (
  //         <FormControlLabel
  //           checked={(!row.nftStatus) ? false : true}
  //           control={<Switch color="primary" />}
  //           onChange={(e) => saveStatus(e, row.productId, "nftStatus", row.forSale)}
  //         />
  //       );
  //     },
  //   },
  //   {
  //     dataField: "createdDate",
  //     text: "Created",
  //     // sort: true,
  //     formatter: (cell, row) => {
  //       if (row.createdDate) {
  //         return (
  //           <span>{moment(row.createdDate).format('MMM DD, YYYY hh:mm:ss')}</span> // for styling see styles.css in current folder
  //         )
  //       } else {
  //         return (<></>)
  //       }
        
  //     },
  //   },
  //   {
  //     dataField: "updatedDate",
  //     text: "Updated",
  //     // sort: true,
  //     formatter: (cell, row) => {
  //       // console.log(row)
  //       if (row.updatedDate) {
  //         return (
  //           <span>{moment(row.updatedDate).fromNow()}</span> // for styling see styles.css in current folder
  //         )
  //       } else {
  //         return (<></>)
  //       }
        
  //     },
  //   },
  // ];

  // const renderProducts = () => {
  //   if (!loading) {
  //     if (AllProducts.length > 0) {
  //       return (
  //         <BootstrapTable
  //           bootstrap4
  //           keyField="productId"
  //           data={AllProducts}
  //           columns={columns}
  //           bordered={false}
  //           pagination={paginationFactory({ sizePerPage: 10 })}
  //         ></BootstrapTable>
  //       );
  //     } else {
  //       return (
  //         <h4 style={{ textAlign: "center", padding: "20px" }}>No Data!</h4>
  //       );
  //     }
  //   } else {
  //     return (
  //       <h4 style={{ textAlign: "center", padding: "20px" }}>Loading...</h4>
  //     );
  //   }
  // };

  const handleChangePage = (event, newPage) => {
    let filterOptions = filters;
    filterOptions.pageIndex = newPage + 1;
    setFilters({...filterOptions});
  };

  const handleChangeRowsPerPage = (event) => {
    let filterOptions = filters;
    filterOptions.pageIndex = 1;
    filterOptions.pageSize = event.target.value;
    setFilters({...filterOptions});
  };

  // console.log(AllProducts)

  return (
    <div className="checkout">
      <div className="bg-light pt-3 text-center">
        <h1 className="section-heading">Seller Products</h1>
      </div>
      <Container className="my-4">
        <Row>
          <Col lg={12} className="">
            <Breadcrumbs maxItems={2} aria-label="breadcrumb">
              <MUILink underline="hover" color="inherit" href="/dashboard/home">
                Home
              </MUILink>
              <span color="inherit">
                Seller Products
              </span>
            </Breadcrumbs>
          </Col>
        </Row>
      </Container>
      <Container className="my-4">
        {(AllProducts.length === 0) ?
          <div>
            <h2><small>{message}</small></h2>
          </div>
        :
          <div>
            <div className="list-pagination">
              <Pagination 
                rowsPerPageOptions={[5, 10]}
                component="div"
                count={AllProducts.totalRecords}
                rowsPerPage={filters.pageSize}
                page={filters.pageIndex - 1}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage} />
            </div>
            <ul className="responsive-table py-2">
              {(AllProducts).map((value, index) => (
                <li className="table-row" key={index}>
                  {/* <div className="col col-1" data-label="Job Id"><label>sr.</label><p>{index + 1}</p></div> */}
                  <div className="col col-2" data-label="Amount">
                    <div>
                      <Link to={`/dashboard/product-detail/${value.productId}`}>
                        <img
                          width="65"
                          height="50"
                          // src={`data:image/${extention};base64,${cell}`}
                          src={value.productImage}
                        />
                      </Link>
                    </div>
                  </div>
                  <div className="col col-7" data-label="Customer Name"><label>Name</label>
                    <div>
                      <Link to={`/dashboard/product-detail/${value.productId}`}>{value.productName}</Link>
                      <div>
                        <Link to={`/dashboard/update-product/${value.productId}`}>
                          <Tooltip disableFocusListener title="Edit product">
                            <IconButton className="p-0">
                              <EditIcon/>
                            </IconButton>
                          </Tooltip>
                        </Link>
                        <a
                          style={{ marginLeft: "10px" }}
                          onClick={() => handleDelete(value.productId)}
                        >
                          <Tooltip disableFocusListener title="Delete product">
                            <IconButton className="p-0">
                              <DeleteIcon/>
                            </IconButton>
                          </Tooltip>
                        </a>
                      </div>
                    </div>
                  </div>
                  {/* <div className="col col-3" data-label="Amount"><label>Name</label><p><Link to={`/dashboard/product-detail/${value.productId}`}>{value.productName}</Link></p></div> */}
                  <div className="col col-5" data-label="Amount"><label>Price (SGD)</label><p>{value.price}</p></div>
                  <div className="col col-1" data-label="Amount"><label>Sale</label>
                    <div>
                      <FormControlLabel
                        checked={(!value.forSale) ? false : true}
                        control={<Switch color="primary" />}
                        onChange={(e) => saveStatus(e, value.productId, "forSale", value.nftStatus)}
                      />
                    </div>
                  </div>
                  <div className="col col-1" data-label="Amount"><label>NFT</label>
                    <div>
                      <FormControlLabel
                        checked={(!value.nftStatus) ? false : true}
                        control={<Switch color="primary" />}
                        onChange={(e) => saveStatus(e, value.productId, "nftStatus", value.forSale)}
                      />
                    </div>
                  </div>
                  <div className="col col-10" data-label="Amount"><label>Created</label><p>{moment(value.createdDate).format('MMM DD, YYYY hh:mm:ss')}</p></div>
                  <div className="col col-6" data-label="Payment Status"><label>Updated</label><p>{moment(value.updatedDate).fromNow()}</p></div>
                </li>
              ))}
              
            </ul>
            <div className="list-pagination">
                <Pagination 
                  rowsPerPageOptions={[5, 10]}
                  component="div"
                  count={AllProducts.totalRecords}
                  rowsPerPage={filters.pageSize}
                  page={filters.pageIndex - 1}
                  onPageChange={handleChangePage}
                  onRowsPerPageChange={handleChangeRowsPerPage} />
            </div>
          </div>
        }
      </Container>
      {/* <div className="container pt-3 seller-products-table">
        {renderProducts()}
      </div> */}

      <BuyerConfirm open={showSellerConfrim} handleClose={setShowSellerConfirm} setSellerConfirm={userFeedback} data={popupData}/>
    </div>
  );
};

export default withRouter(withAlert()(SellerProducts));