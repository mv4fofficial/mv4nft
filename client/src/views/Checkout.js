import React from "react";
import { Col, Container, Form, Row } from "react-bootstrap";
// import Select from "react-select";
import { withAlert } from 'react-alert';
// import countries from "../data/countries";
import { colourStyles } from '../constant';
import Payment from '../components/checkouts/Payment';
import LoaderComp from '../components/Loader';
import ApiRequest from '../api_service/api_requests';
import * as Constants from '../constant';
import '../assets/css/checkout.css';

import Grid from '@material-ui/core/Grid';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import { makeStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: "8px 0",
    minWidth: 120,
    width: "100%"
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  root: {
    flexGrow: 1,
  },
  cropContainer: {
      position: 'relative',
      minWidth: "400px",
      width: '100%',
      height: 200,
      background: '#333',
      [theme.breakpoints.up('sm')]: {
        height: 400,
      },
  },
      cropButton: {
      flexShrink: 0,
      marginLeft: 16,
  },
      controls: {
      padding: 16,
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'stretch',
      [theme.breakpoints.up('sm')]: {
          flexDirection: 'row',
          alignItems: 'center',
      },
  },
      sliderContainer: {
      display: 'flex',
      flex: '1',
      alignItems: 'center',
  },
      sliderLabel: {
      [theme.breakpoints.down('xs')]: {
          minWidth: 65,
      },
  },
  slider: {
      padding: '22px 0px',
      marginLeft: 16,
      [theme.breakpoints.up('sm')]: {
          flexDirection: 'row',
          alignItems: 'center',
          margin: '0 16px',
      },
  },
}));

const Item = props => {
  let data = props.data || {};
  let { productName, productPrice, productImage } = data;
  let productTmg = productImage ? productImage : '';

  return (
    <div className="item-checkout d-flex align-items-center border-bottom py-3 mb-2">
      <div className="checkout-img-product position-relative">
        <img
          src={productTmg || 'https://cdn.shopify.com/s/files/1/0609/6152/1863/products/DiRenJie_300x.png'}
          alt="product-img" />
        <div className="qty position-absolute">1</div>
      </div>
      <div className="info pl-3">
        <div className="font-weight-bold">{productName || 'NA'}</div>
        <div className="sub-line">Tang Dynasty Heros</div>
      </div>
      <div className="price-checkout-item ml-auto">
        <strong>${productPrice || 0}</strong>
      </div>
    </div>
  );
}

class Checkout extends React.PureComponent {
  constructor() {
    super();
    this.state = {
      firstName: "user",
      lastName: "user",
      name: "",
      emailId: "",
      address: '',
      landmark: '',
      city: '',
      postal: '',
      phoneNumber: 9876543120,
      country: "India",
      dateOfBirth: "2022-03-15T17:35:13.004Z",
      profilePhoto: "https://cdn.shopify.com/s/files/1/0609/6152/1863/products/DiRenJie_300x.png",

      items: [],
      countries: []
    };
  }
  componentDidMount() {
    this.viewUser();
    this.getCartItems();
  }

  getCartItems = () => {
    this.setState({ loaderMessage: "Fetching Checkout details", displayLoader: true });
    ApiRequest.viewCartItems()
      .then(res => {
        if (res && res.data && res.statusCode && res.statusCode === 200) {
          this.setState({ items: res.data });
        } else {
          if (res && res.message) {
            this.props.alert.error("Error fetching cart items");
          }
        }
        this.setState({ loaderMessage: "", displayLoader: false });
      })
      .catch((err) => {
        this.setState({ loaderMessage: "", displayLoader: false });
        // console.log('Error fetching cart items, ', err);
      })
  }

  viewUser = () => {
    ApiRequest.viewUser()
      .then(res => {
        if (res && res.data && res.statusCode && res.statusCode === 200) {
          // console.log(res.data);
          let obj = {
            firstName: (res.data.name).split(" ")[0],
            lastName: (res.data.name).split(" ")[1],
            emailId: res.data.emailId,
            phoneNumber: res.data.phoneNumber,
            country: res.data.country.id,
            dateOfBirth: res.data.dateOfBirth,
            profilePhoto: res.data.profilePicture,
          }
          this.setState(obj);
        } else {
          if (res && res.message) {
            this.props.alert.error("Error fetching user details");
          }
        }
      })
      .catch((err) => {
        // console.log('Error fetching user details, ', err);
      })

    ApiRequest.getCountry()
      .then(res => {
          if (res && res.length) {
            let countries = res.map(object => {
              return { label: object.name, value: object.id, code: object.phone_code }
            });
            this.setState({countries : countries});
          }
      })
      .catch((err) => {
          // console.log('Categories fetch api error, ', err);
      })
  }

  formValueChange(event, field) {
    this.setState({
      [field]: event.target.value 
    })
  }

  render() {
    const isClearable = true;
    const isSearchable = true;

    let ids = [];
    let items = [];
    let totalPrice = 0;

    if (this.state.items && this.state.items.length) {
      this.state.items.forEach((item, index) => {
        if (item) {

          if (item.productPrice && !isNaN(item.productPrice)) {
            totalPrice += Math.round(Number(item.productPrice) * 100) / 100;
          }

          if (item.id) {
            ids.push(item.id);
          }

          items.push(<Item key={item.id || index} data={item} />);
        }
      });
    }

    let state = this.state || {};

    return (
      <LoaderComp show={this.state.displayLoader} message={this.state.loaderMessage}>
        <div className="checkout">
          <div className="bg-light py-5 text-center">
            <h1 className="section-heading">Check out</h1>
          </div>
          <Container className="my-4">
            <Row>
              <Col lg={7} className="pr-lg-5">
                <Form className="py-4" autoComplete="off">

                  <h5 className="mb-3">Contact Informations</h5>
                  <Form.Group controlid="email">
                    <FormControl variant="outlined" className="wp-100">
                      <TextField
                          label="Email"
                          type="text"
                          name="emailId"
                          variant="outlined"
                          className="wp-100"
                          value={state.emailId}
                          readOnly
                      />
                    </FormControl>
                  </Form.Group>
                  {/* <Form.Group controlid="formBasicEmail">
                    <h5 className="mb-3">Contact Informations</h5>
                    <Form.Control
                      name="emailId"
                      className="rounded-0"
                      type="text"
                      placeholder="Email addres or phone number"
                      defaultValue={state.emailId || ''}
                      onChange={this.formValueChange}
                    />
                  </Form.Group> */}
                  <Form.Group controlid="formBasicCheckbox" className="wp-100">
                    <Form.Check
                      className="rounded-0 mt-2"
                      type="checkbox"
                      label="Check me out for new offers"
                    />
                  </Form.Group>

                  <h5 className="mt-4 mb-3">Billing address</h5>

                  <Grid container spacing={3}>
                  {/* <Form.Group controlid="selectCountry">
                    <Form.Label>Country</Form.Label>
                    <Select
                      styles={colourStyles}
                      placeholder="Select Country"
                      className="coin-select"
                      classNamePrefix="select"
                      defaultValue={state.country || ''}
                      //   isDisabled={isDisabled}
                      //   isLoading={isLoading}
                      isClearable={isClearable}
                      //   isRtl={isRtl}
                      isSearchable={isSearchable}
                      name="coin"
                      options={countries}
                      getOptionLabel={(coin) => (
                        <span>
                          {" "}
                          {coin.icon} {coin.name}
                        </span>
                      )}
                      getOptionValue={(coin) => coin.name}
                    />
                  </Form.Group> */}
                  <Grid item xs={12} sm={6} className="mt-4">
                    <FormControl controlid="firstName" variant="outlined" className="wp-100">
                      <TextField
                          label="First name"
                          type="text"
                          name="firstName"
                          variant="outlined"
                          className="wp-100"
                          value={(state.firstName === "user") ? "" : state.firstName}
                          onChange={(e) => this.formValueChange(e, "firstName")}
                      />
                    </FormControl>
                  </Grid>
                  <Grid item xs={12} sm={6} className="mt-4">
                    <FormControl controlid="lastName" variant="outlined" className="wp-100">
                      <TextField
                          label="Last Name"
                          type="text"
                          name="lastName"
                          variant="outlined"
                          className="wp-100"
                          value={(state.lastName === "user") ? "" : state.lastName}
                          onChange={(e) => this.formValueChange(e, "lastName")}
                      />
                    </FormControl>
                  </Grid>

                  <Grid item xs={12} sm={6} className="mt-4">
                    <Form.Group controlid="Country">
                      <FormControl variant="outlined" className="wp-100">
                        <InputLabel id="country-dropdown-label" required>Country</InputLabel>
                        <Select
                            label="Country"
                            labelId="country-dropdown-label"
                            id="country-dropdown"
                            value={state.country || ''}
                            variant="outlined"
                            onChange={(e) => this.formValueChange(e, "country")}
                        >
                            {state.countries.map((country, index) => (
                                <MenuItem key={index} value={country.value}>{country.label}</MenuItem>
                            ))}
                        </Select>
                      </FormControl>
                    </Form.Group>
                  </Grid>

                  <Grid item xs={12} sm={6} className="mt-4">
                    <FormControl controlid="phone" variant="outlined" className="wp-100">
                      <TextField
                          label="Contact Number"
                          type="text"
                          name="phoneNumber"
                          variant="outlined"
                          className="wp-100"
                          value={(state.phoneNumber === 9876543120 ? "" : state.phoneNumber)}
                          placeholder="Contact Number"
                          onChange={(e) => this.formValueChange(e, "phoneNumber")}
                      />
                    </FormControl>
                  </Grid>

                  <Grid item xs={12} sm={12} className="mt-4">
                    <FormControl controlid="address" variant="outlined" className="wp-100">
                      <TextField
                          label="Address"
                          type="text"
                          name="address"
                          variant="outlined"
                          className="wp-100"
                          value={state.address}
                          placeholder="Address"
                          onChange={(e) => this.formValueChange(e, "address")}
                      />
                    </FormControl>
                  </Grid>

                  <Grid item xs={12} sm={12} className="mt-4">
                    <FormControl controlid="landmark" variant="outlined" className="wp-100">
                      <TextField
                          label="Landmark"
                          type="text"
                          name="landmark"
                          variant="outlined"
                          className="wp-100"
                          value={state.landmark}
                          placeholder="Apartment, suite, ect, (optional)"
                          onChange={(e) => this.formValueChange(e, "landmark")}
                      />
                    </FormControl>
                  </Grid>


                  <Grid item xs={12} sm={6} className="mt-4">
                    <FormControl controlid="city" variant="outlined" className="wp-100">
                      <TextField
                          label="City"
                          type="text"
                          name="city"
                          variant="outlined"
                          className="wp-100"
                          placeholder="City"
                          value={state.city}
                          onChange={(e) => this.formValueChange(e, "city")}
                      />
                    </FormControl>
                  </Grid>

                  <Grid item xs={12} sm={6} className="mt-4">
                    <FormControl controlid="postal" variant="outlined" className="wp-100">
                      <TextField
                          label="Postal"
                          type="number"
                          name="postal"
                          variant="outlined"
                          className="wp-100"
                          value={state.postal}
                          placeholder="Postal code"
                          onChange={(e) => this.formValueChange(e, "postal")}
                      />
                    </FormControl>
                  </Grid>

                  <Grid item xs={12} sm={12} className="mt-4">
                    <Payment ids={ids} totalPrice={totalPrice} />
                  </Grid>

                </Grid>
                  {/* <Row className="mt-3">
                    <Col md={6}>
                      <Form.Group controlid="firstname">
                        <Form.Control
                          name="name"
                          className="rounded-0 mt-2"
                          type="text"
                          placeholder="First name"
                          defaultValue={state.name}
                          onChange={this.formValueChange} />
                      </Form.Group>
                    </Col>
                    <Col md={6} className="mt-3 mt-md-0">
                      <Form.Group controlid="lastname">
                        <Form.Control
                          name="name"
                          className="rounded-0 mt-2"
                          type="text"
                          placeholder="Last name"
                          defaultValue={state.name}
                          onChange={this.formValueChange} />
                      </Form.Group>
                    </Col>
                  </Row> */}
                  {/* <Form.Group controlid="address" className="mt-3">
                    <Form.Control
                      name="address"
                      className="rounded-0 mt-2"
                      type="text"
                      placeholder="Address"
                      defaultValue={this.state.address}
                      onChange={this.formValueChange} />
                  </Form.Group> */}
                  {/* <Form.Group controlid="apartmentetc" className="mt-3">
                    <Form.Control
                      name="landmark"
                      className="rounded-0 mt-2"
                      type="text"
                      placeholder="Apartment, suite, ect, (optional)"
                      defaultValue={this.state.landmark}
                      onChange={this.formValueChange} />
                  </Form.Group> */}
                  {/* <Row className="mt-3">
                    <Col md={6}>
                      <Form.Group controlid="city">
                        <Form.Control
                          name="city"
                          className="rounded-0 mt-2"
                          type="text"
                          placeholder="City"
                          defaultValue={this.state.city}
                          onChange={this.formValueChange} />
                      </Form.Group>
                    </Col>
                    <Col md={6} className="mt-3 mt-md-0">
                      <Form.Group controlid="postal">
                        <Form.Control
                          name="postal"
                          className="rounded-0 mt-2"
                          type="text"
                          placeholder="Postal code"
                          defaultValue={this.state.postal}
                          onChange={this.formValueChange} />
                      </Form.Group>
                    </Col>
                  </Row> */}
                  {/* <Payment ids={ids} totalPrice={totalPrice} /> */}
                </Form>
              </Col>
              <Col lg={5} className="mt-4">
                <div className="items-incheckout">{items}</div>
                <div>
                  <div className="total-amount d-flex justify-content-between align-items-center py-2">
                    <h5>Sub Total</h5>
                    <p className="mb-0 font-weight-bold">${parseFloat(parseFloat(totalPrice) - ((parseFloat(process.env.REACT_APP_STRIPE_FEE)*parseFloat(totalPrice)) / 100)).toFixed(2)}</p>
                  </div>
                  <div className="total-amount d-flex justify-content-between align-items-center py-2">
                    <h5>Stripe Fee</h5>
                    <p className="mb-0 font-weight-bold">${(parseFloat(process.env.REACT_APP_STRIPE_FEE)*parseFloat(totalPrice) / 100).toFixed(2)}</p>
                  </div>
                  <div className="total-amount d-flex justify-content-between align-items-center py-2">
                    <h5>Total</h5>
                    <p className="mb-0 font-weight-bold">${totalPrice}</p>
                  </div>
                </div>
              </Col>
            </Row>
          </Container>
        </div>
      </LoaderComp>
    )
  }
};

export default withAlert()(Checkout);