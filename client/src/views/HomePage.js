import React from "react";

import { Button, Col, Container, Row } from "react-bootstrap";
import { Link } from "react-router-dom";
import DiscoverMoreSlider from "../components/DiscoverMoreSlider";
// import Footer from "../components/Footer";
import PhasesDevelopment from "../components/PhasesDevelopment";
import ProductsSlider from "../components/ProductsSlider";
import SubscribeUs from "../components/SubscribeUs";
import Supporters from "../components/Supporters";
import TopFollower from "../components/home/TopFollower";

const HomePage = () => {
  return (
    <div className="home-page ">
      <div className="home-hero d-flex justify-content-center align-items-center">
        <Container>
          <div className="hero-content text-center  ">
            <div
              data-aos="fade-up"
              data-aos-duration="900"
              data-aos-delay="200"
            >
              <h1 className="mb-3">MV4F</h1>
              <p>Your Assets Ally For the Connected Metaverse Future</p>
            </div>
            <Button
              as={Link}
              to="products"
              data-aos="zoom-in-up"
              data-aos-duration="900"
              data-aos-delay="500"
              variant="outline-light rounded-0 px-4 font-weight-bold text-capitalize mt-1"
            >
              View all assets
            </Button>
          </div>
        </Container>
      </div>

      {/* Join Us Section Here */}
      <Container>
        <div
          className="join-us text-center "
          data-aos="fade-up"
          data-aos-duration="900"
          data-aos-delay="200"
        >
          <h2 className=" mb-3 pb-1 h-xl">
            LIST, <span className="bg-gr">DISCOVER</span> AND BUY ASSETS
          </h2>
          <p>
            A Leading Marketplace and Library of Game Ready 3D & NFT assets.
          </p>
          {/* <Button
            variant="primary"
            className="mt-3 rounded-0 px-4 font-weight-bold text-capitalize mt-1 mb-1"
          >
            Join us Now
          </Button> */}
        </div>
      </Container>


      {/* Featured Assets */}
      <div data-aos="zoom-in-up" className="follower-section bg-primary p-5">
        <Container>
          <h2
            data-aos="fade-up"
            data-aos-duration="900"
            //   data-aos-delay="200"
            className="text-white section-heading"
          >
            Top Follower
          </h2>
          {/* <Row>
            <Col lg={4}>
            
              <ProductCard />
            </Col>
          </Row> */}
          <div data-aos="fade-up">
            <TopFollower />
          </div>
        </Container>
      </div>
      {/* Featured Assets */}


      {/* Featured Assets */}
      <Container>
        <h2
          data-aos="fade-up"
          data-aos-duration="900"
          //   data-aos-delay="200"
          className="section-heading mt-4 pt-4"
        >
          Featured Assets
        </h2>
        {/* <Row>
          <Col lg={4}>
           
            <ProductCard />
          </Col>
        </Row> */}
        <div data-aos="fade-up">
          <ProductsSlider />
        </div>
      </Container>
      {/* Featured Assets */}


      {/* <Container className="mt-5">
        <h2
          data-aos="fade-up"
          data-aos-duration="900"
          //   data-aos-delay="200"
          className="section-heading"
        >
          Discover More
        </h2>
        <div data-aos="fade-up">
          <DiscoverMoreSlider />
        </div>
      </Container> */}

      {/* Banner */}
      <div data-aos="zoom-in-up" className="bg-primary banner-info">
        <Container className="text-center">
          <Row>
            <Col className="mx-auto" lg={7}>
              <h2 className="text-white mb-3 h-xl">
                MV4F - Metaverse For the Future
              </h2>
              <p className="text-light pt-1">
                Boundaries are only a figment of humans' imagination and
                constraints. Soon, the locks will be lifted and all would be
                free, to transit, transact and to live life across the world,
                and metaverses.
              </p>
            </Col>
          </Row>
        </Container>
      </div>

      {/* Development phases */}
      <Container className="mb-5">
        <h2
          data-aos="fade-up"
          data-aos-duration="900"
          //   data-aos-delay="200"
          className="section-heading"
        >
          Development Phases
        </h2>
        <PhasesDevelopment />
      </Container>

      {/* Subscribe Us */}
      {/* <SubscribeUs /> */}

      {/* Our Supporters */}
      <Supporters />
    </div>
  );
};

export default HomePage;
