import React from "react";
import { connect } from 'react-redux';
import { Button, Col, Container, Row } from "react-bootstrap";
import { Link } from "react-router-dom";
import { withAlert } from 'react-alert';

import CartItem from "../components/CartItem";
import LoaderComp from '../components/Loader';
import { updateCartCount } from '../user';
import ApiRequest from '../api_service/api_requests';
import '../assets/css/cart.css';

class Cart extends React.PureComponent {
  constructor() {
    super();
    this.state = {
      data: [],

      loaderMessage: '',
      displayLoader: false
    };
  }

  componentDidMount() {
    this.getCartItems();
  }

  getCartItems = () => {
    this.setState({ loaderMessage: "Fetching Cart Items", displayLoader: true });
    ApiRequest.viewCartItems()
      .then(res => {
        if (res && res.data && res.statusCode && res.statusCode === 200) {
          this.setState({ data: res.data });
          this.props.dispatch(updateCartCount(res.data.length || 0));
        } else {
          if (res && res.message) {
            this.props.alert.error();
          }
        }
        this.setState({ loaderMessage: "", displayLoader: false });
      })
      .catch((err) => {
        this.setState({ loaderMessage: "", displayLoader: false });
        // console.log('View Cart api error, ', err);
      })
  }

  getClientSecret = (totalPrice) => {
    let payload = {
      orderAmount: totalPrice
    };
    ApiRequest.getClientSecret(payload)
    .then(res => {
      // console.log(res)
    })
    .catch((err) => {
        this.setState({ loaderMessage: "", displayLoader: false });
        // console.log('Payment api error, ', err);
    })
  }

  render() {
    let totalPrice = 0;
    let cartItems = this.state.data.map((cartItem, index) => {
      if (cartItem) {
        let price = cartItem.productPrice || 0;

        if (!isNaN(price)) {
          totalPrice += Math.round(Number(price) * 100) / 100;
        }
      }

      return <CartItem key={index} data={cartItem} getCartItems={this.getCartItems} />;
    });

    let itemsPresent = false;

    if (this.state.data && this.state.data.length) {
      itemsPresent = true;
    }

    return (
      <LoaderComp show={this.state.displayLoader} message={this.state.loaderMessage}>
        <div className="cart">
          {
            itemsPresent ? <Container>
              <div className="d-flex justify-content-between align-items-center py-4 flex-wrap">
                <h1 className="">Your Cart</h1>
                <Link to="/dashboard/products" className="font-weight-bold font-18">
                  <u>Continue shopping</u>
                </Link>
              </div>
              <Row className="justify-content-between header-row border-bottom py-3">
                <Col xs={6}>Product</Col>
                <Col xs={3} className="d-none d-sm-block">
                  Quantity
                </Col>
                <Col className="text-right" xs={3}>
                  Total
                </Col>
              </Row>

              {cartItems}

              <div
                className="subtotal mt-4 text-center text-sm-right">
                <div className="mb-0 d-flex justify-content-center  justify-content-sm-end ">
                  <h6 className="mr-1">Subtotal: </h6>
                  <span> ${totalPrice} SGD</span>
                </div>
                <div className="text-center text-sm-right">
                  <small className="mb-0">
                    Taxes and shipping calculated at checkout
                  </small>
                </div>
                <Button
                  // className="checkout-btn rounded-0 mt-3 mb-4"
                  // variant="primary"
                  // onClick={() => this.getClientSecret(totalPrice)}
                  as={Link}
                  to="/dashboard/checkout"
                >
                  Check out
                </Button>
              </div>
            </Container> : <Container className="empty-list-err">No Items found in Cart</Container>
          }
        </div>
      </LoaderComp>
    )
  }
};

const mapStateToProps = (state) => {
  return {
    user: state.userReducer
  }
}

export default withAlert()(connect(mapStateToProps)(Cart));