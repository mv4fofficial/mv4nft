import React from 'react';
import { Col, Container, Row, Form } from "react-bootstrap";
import { Link, withRouter } from "react-router-dom";
import { withAlert } from 'react-alert';
import LoaderComp from '../components/Loader';
import ApiRequest from '../api_service/api_requests';
import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import Button from '@material-ui/core/Button';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';

class CreateWishlist extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            categoriesList: [],
            subCategoriesList: [],

            selectedCategories: [],
            selectedSubCategories: [],
            description: '',

            loaderMessage: '',
            displayLoader: false
        }
    }

    componentDidMount() {
        this.getCategories();
        this.getSubCategories();

        if (this.props.isUpdate) {
            this.getWishlist();
        }
    }

    getWishlist = () => {
        if (this.props && this.props.match && this.props.match.params && this.props.match.params.wishlist_id) {
            this.setState({ loaderMessage: "Fetching Wishlist", displayLoader: true });
            ApiRequest.getWishlist(this.props.match.params.wishlist_id)
                .then(res => {
                    if (res && res.statusCode && res.statusCode === 200 && res.data) {
                        let data = res.data;
                        let categories = [];
                        let subCategories = [];

                        if (data.wishListCategories && data.wishListCategories.length) {
                            data.wishListCategories.forEach(object => {
                                if (object.categoryId && object.categoryName) {
                                    categories.push({
                                        label: object.categoryName,
                                        value: object.categoryId
                                    });
                                }
                            });
                        }

                        if (data.wishListSubCategories && data.wishListSubCategories.length) {
                            data.wishListSubCategories.forEach(object => {
                                if (object.subCategoryId && object.subCategoryName) {
                                    subCategories.push({
                                        label: object.subCategoryName,
                                        value: object.subCategoryId
                                    });
                                }
                            });
                        }

                        this.setState({
                            selectedCategories: categories,
                            selectedSubCategories: subCategories,
                            description: data.wishListDescription || ''
                        })

                    } else {
                        this.props.alert.error(res.message || "Failed to fetch Wishlist");
                    }
                    this.setState({ loaderMessage: "", displayLoader: false });
                })
                .catch((err) => {
                    // console.log('Wishlist fetch api error, ', err);
                    this.setState({ loaderMessage: "", displayLoader: false });
                })
        }
    }

    getCategories = () => {
        ApiRequest.getCategory()
            .then(res => {
                if (res && res.length) {
                    let categoriesList = [];
                    res.forEach(data => {
                        if (data && data.id && data.categoryName) {
                            categoriesList.push({ label: data.categoryName, value: data.id });
                        }
                    })

                    this.setState({ categoriesList: categoriesList });
                }
            })
            .catch((err) => {
                // console.log('Categories fetch api error, ', err);
            })
    }

    getSubCategories = () => {
        ApiRequest.getSubCategory()
            .then(res => {
                if (res && res.length) {
                    let subCategoriesList = [];
                    res.forEach(data => {
                        if (data && data.id && data.subCategoryName) {
                            subCategoriesList.push({ label: data.subCategoryName, value: data.id, categoryId: data.categoryId});
                        }
                    })

                    this.setState({ subCategoriesList: subCategoriesList });
                }
            })
            .catch((err) => {
                // console.log('Categories fetch api error, ', err);
            })
    }

    categoryChange = data => {
        this.setState({ selectedCategories: [data.target.value] });
    }

    subCategoryChange = data => {
        this.setState({ selectedSubCategories: [data.target.value] });
    }

    inputChange = event => {
        if (event && event.target && event.target.value) {
            this.setState({ description: event.target.value });
        }
    }

    handleSubmit = () => {
        if (this.props.isUpdate) {
            if (!(this.props && this.props.match && this.props.match.params && this.props.match.params.wishlist_id)) {
                this.props.alert.error("Wishlist ID not found for update");
                return
            }
        }

        if (!(this.state.selectedCategories && this.state.selectedCategories.length)) {
            this.props.alert.error("Please select Category");
            return;
        }

        if (!(this.state.selectedSubCategories && this.state.selectedSubCategories.length)) {
            this.props.alert.error("Please select Sub Category");
            return;
        }

        this.setState({ loaderMessage: "Uploading Wishlist", displayLoader: true });

        let payload = {
            categories: this.state.selectedCategories, //this.state.selectedCategories.map(data => data.value),
            subCategories: this.state.selectedSubCategories, //this.state.selectedSubCategories.map(data => data.value),
            description: this.state.description || ''
        };

        if (this.props.isUpdate) {
            payload.wishListId = this.props.match.params.wishlist_id;
        }

        ApiRequest.saveWishlist(payload, this.props.isUpdate)
            .then(res => {
                if (res && res.statusCode && res.statusCode === 200) {
                    this.props.history.push('/dashboard/wishlist');
                    this.props.alert.success(res.message || "Wishlist saved successfully");
                } else {
                    this.props.alert.error(res.message || "Failed to save Wishlist");
                }
                this.setState({ loaderMessage: "", displayLoader: false });
            })
            .catch((err) => {
                // console.log('Wishlist api error, ', err);
                this.setState({ loaderMessage: "", displayLoader: false });
                if (this.props && this.props.alert) {
                    this.props.alert.error(err.message || "Failed to save Wishlist");
                }
            })
    }

    backHistory = () => {
        this.props.history.goBack();
    }

    render() {
        // console.log(this.state.selectedCategories)
        // console.log(this.state.subCategoriesList)
        return (
            <LoaderComp show={this.state.displayLoader} message={this.state.loaderMessage}>
                <div className="mb-5 register">
                    <div className="pt-3 text-center">
                        <h1 className="section-heading">Create Wishlist</h1>
                    </div>
                    <Container className="pt-4 pb-5">
                        <Row>
                            <Col md={10} lg={6} xl={5} className="mx-auto">
                                <div>
                                    <Form autoComplete="off">
                                        {/* <Row>
                                            <Col md={11} className="mt-3 mt-md-0 m-b-1-rem">
                                                <Form.Group controlId="category">
                                                    <Form.Label>Category</Form.Label>
                                                    <Select
                                                        autoComplete="off"
                                                        autoFocus="off"
                                                        styles={colourStyles}
                                                        placeholder="Select Category"
                                                        name="category"
                                                        options={this.state.categoriesList}
                                                        onChange={this.categoryChange}
                                                        isMulti={true}
                                                        value={this.state.selectedCategories} />
                                                </Form.Group>
                                            </Col>
                                        </Row>

                                        <Row>
                                            <Col md={11} className="mt-3 mt-md-0 m-b-1-rem">
                                                <Form.Group controlId="subCategory">
                                                    <Form.Label>SubCategory</Form.Label>
                                                    <Select
                                                        autoComplete="off"
                                                        autoFocus="off"
                                                        styles={colourStyles}
                                                        placeholder="Select SubCategory"
                                                        name="subCategory"
                                                        options={this.state.subCategoriesList}
                                                        onChange={this.subCategoryChange}
                                                        isMulti={true}
                                                        value={this.state.selectedSubCategories} />
                                                </Form.Group>
                                            </Col>
                                        </Row> */}

                                        {/* <Row>
                                            <Col md={11} className="mt-3 mt-md-0 m-b-1-rem">
                                                <Form.Group controlId="description">
                                                    <Form.Label>Description</Form.Label>
                                                    <Form.Control
                                                        className="rounded-0"
                                                        as="textarea"
                                                        rows={6}
                                                        onChange={this.inputChange}
                                                        value={this.state.description} />
                                                </Form.Group>
                                            </Col>
                                        </Row> */}

                                        <Grid container spacing={3}>
                                            <Grid item xs={12} sm={12} className="mb-3">
                                                <Form.Group controlId="SelectCategory">
                                                    <FormControl variant="outlined" className='wp-100'>
                                                        <InputLabel id="category-dropdown-label" required>Category</InputLabel>
                                                        <Select
                                                            label="Category"
                                                            labelId="category-dropdown-label"
                                                            id="category-dropdown"
                                                            variant="outlined"
                                                            value={this.state.selectedCategories}
                                                            onChange={(e) => this.categoryChange(e)}
                                                        >
                                                            {this.state.categoriesList.map((category, index) => (
                                                                <MenuItem key={index} value={category.value}>{category.label}</MenuItem>
                                                            ))}
                                                        </Select>
                                                    </FormControl>
                                                </Form.Group>
                                            </Grid>
                                            <Grid item xs={12} sm={12} className="mb-3">
                                                <Form.Group controlId="SelectSubCategory">
                                                    <FormControl variant="outlined" className='wp-100'>
                                                        <InputLabel id="sub-category-dropdown-label" required>Sub Category</InputLabel>
                                                        <Select
                                                            label="Sub Category"
                                                            labelId="sub-category-dropdown-label"
                                                            id="sub-category-dropdown"
                                                            variant="outlined"
                                                            value={this.state.selectedSubCategories}
                                                            onChange={(e) => this.subCategoryChange(e)}
                                                        >
                                                            {this.state.subCategoriesList.map((subCategory, index) => {
                                                                if (parseInt(this.state.selectedCategories[0]) === subCategory.categoryId) {
                                                                    return <MenuItem key={index} value={subCategory.value}>{subCategory.label}</MenuItem>
                                                                }
                                                            })}
                                                        </Select>
                                                    </FormControl>
                                                </Form.Group>
                                            </Grid>
                                            <Grid item xs={12} sm={12} className="mb-3">
                                                <Form.Group controlId="description">
                                                    <FormControl variant="outlined" className='wp-100'>
                                                        <div>
                                                            <textarea
                                                                rows={5}
                                                                className="textareaField wp-100"
                                                                onChange={e => this.inputChange(e)}
                                                                value={this.state.description}
                                                            ></textarea>
                                                        </div>
                                                    </FormControl>
                                                </Form.Group>
                                            </Grid>
                                        </Grid>
                                        <Grid item xs={12} className="text-center mt-3">
                                            <Button
                                                type="button"
                                                className="mr-2"
                                                variant="contained"
                                                color="default"
                                                onClick={this.backHistory}
                                            >
                                                    Cancel
                                            </Button>
                                            <Button
                                                type="button"
                                                className=""
                                                variant="contained"
                                                color="primary"
                                                onClick={this.handleSubmit}
                                            >
                                                Save Wishlist
                                            </Button>
                                        </Grid>
                                    </Form>
                                </div>

                                {/* <div className="buttons-form mt-4 ">
                                    <Link
                                        className="rounded-0 py-2 px-4 mr-3 btn btn-primary"
                                        variant="primary"
                                        to="/dashboard/wishlist">
                                        Cancel
                                    </Link>
                                    <Button
                                        onClick={this.handleSubmit}
                                        className="rounded-0 py-2 px-4"
                                        variant="primary">
                                        Save Wishlist
                                    </Button>
                                </div> */}
                            </Col>
                        </Row>
                    </Container>
                </div>
            </LoaderComp>
        )
    }
}


export default withAlert()(withRouter(CreateWishlist));