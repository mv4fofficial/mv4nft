import React, { useState } from "react";
import { Col, Container, Form, Row } from "react-bootstrap";
import DatePicker from "react-datepicker";
import { withAlert } from 'react-alert';
import { Link, withRouter } from "react-router-dom";
import Select from "react-select";
import LoaderComp from '../components/Loader';
import ApiRequest from '../api_service/api_requests';
import "react-datepicker/dist/react-datepicker.css";

import Button from '@material-ui/core/Button';
import { COUNTRIES } from "../data/countryList";
import Eye from "@material-ui/icons/Visibility"
import EyeOff from "@material-ui/icons/VisibilityOff"
import InputAdornment from "@material-ui/core/InputAdornment"
import IconButton from "@material-ui/core/IconButton"

const colourStyles = {
  color: "red",
  control: (styles, { isFocused, isSelected }) => ({
    ...styles,
    padding: "0 8px",
    backgroundColor: "#fff",
    borderColor: isFocused || isSelected ? "#939cc4" : "#ced4da",
    boxShadow: isFocused && "0 0 0 0.2rem rgba(76, 88 ,140,.25)",
    //   borderColor: "#ced4da",
    ":hover": {
      ...styles[":active"],
      borderColor: "#ced4da"
    },
    ":focus": {
      boxShadow: "0 0 0 0.2rem rgba(76, 88 ,140,.25)"
    },
    //   maxWidth: "200px"
    borderRadius: 0
  }),
  option: (styles, { data, isDisabled, isFocused, isSelected }) => {
    return {
      ...styles,
      backgroundColor: isDisabled ? null : isSelected ? "#e1e4e6" : isFocused,
      ":active": {
        ...styles[":active"],
        backgroundColor: !isDisabled && "#e1e4e6"
      },
      ":hover": {
        ...styles[":active"],
        backgroundColor: "#e1e4e6"
      }
    };
  },
  placeholder: (styles) => ({
    ...styles,
    color: "#131C20"
  }),
  singleValue: (styles) => ({
    ...styles,
    color: "#131C20"
  }),
  menuList: (styles) => ({
    ...styles,
    color: "#282829"
  }),
  indicatorSeparator: (styles) => ({
    ...styles,
    display: "none"
  }),
  dropdownIndicator: (styles, { isFocused }) => ({
    ...styles,
    color: isFocused ? "#131C20" : "#131C20",
    ":hover": {
      ...styles[":active"],
      color: "#131C20"
    }
  })
};

const isClearable = true;
const isSearchable = true;

const CategoryCheckBox = props => {
  const { data, index, category, categoryChange } = props || {};
  const { id, categoryName } = data || {};

  let checked = false;
  if (category && category.length) {
    checked = category.indexOf(`${id}`) > -1 ? true : false;
  }

  return <Form.Check
    className="mb-1"
    value={id || index}
    type="checkbox"
    name="category"
    id={`category-${id || index}`}
    label={categoryName || "NA"}
    checked={checked}
    onChange={categoryChange}
  />
}

const SubCategoryCheckBox = props => {
  const { data, index, subCategory, subCategoryChange } = props || {};
  const { id, subCategoryName } = data || {};

  let checked = false;
  if (subCategory && subCategory.length) {
    checked = subCategory.indexOf(`${id}`) > -1 ? true : false;
  }

  return <Form.Check
    className="mb-1"
    value={id || index}
    type="checkbox"
    name="subCategory"
    id={`sub-category-${id || index}`}
    label={subCategoryName || "NA"}
    checked={checked}
    onChange={subCategoryChange}
  />
}

class Register extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      formStep: 1,
      dateOfBirth: (new Date()).setFullYear( (new Date()).getFullYear() - 15 ),
      countryId: '',
      countryCode: '',
      category: [],
      subCategory: [],
      lookingFor: '',

      countries: [],
      roles: [],
      categoriesList: [],
      subCategoriesList: [],

      selectedRole: '',

      loaderMessage: '',
      displayLoader: false,

      isShowPassword: false,
      isShowConfirmPassword: false,
    };
  }

  componentDidMount() {
    this.getCountry();
    this.getRoles();
    this.getCategories();
    this.getSubCategories();

    // this.state.dateOfBirth.focus();
  }

  getCountry = () => {
    ApiRequest.getCountry()
      .then(res => {
        if (res && res.length) {
          let countries = res.map(object => {
            return { label: object.name, value: object.id, code: object.phone_code }
          });
          this.setState({ countries });
        }
      })
      .catch((err) => {
        // console.log('Categories fetch api error, ', err);
      })
  }

  getRoles = () => {
    this.setState({ loaderMessage: "Fetching form details", displayLoader: true });
    ApiRequest.getRoles()
      .then(res => {
        if (res && res.data && res.data[0] && res.data[0].roleId) {
          this.setState({ roles: (res.data).filter(value => {
            return value.roleName === "Buyer"
          }) });
        }
        this.setState({ loaderMessage: "", displayLoader: false });
      })
      .catch((err) => {
        this.setState({ loaderMessage: "", displayLoader: false });
        // console.log('Categories fetch api error, ', err);
      })
  }

  getCategories = () => {
    ApiRequest.getCategory()
      .then(res => {
        if (res) {
          this.setState({ categoriesList: res });
        }
      })
      .catch((err) => {
        // console.log('Categories fetch api error, ', err);
      })
  }

  getSubCategories = () => {
    ApiRequest.getSubCategory()
      .then(res => {
        if (res) {
          this.setState({ subCategoriesList: res });
        }
      })
      .catch((err) => {
        // console.log('Categories fetch api error, ', err);
      })
  }

  onDateChange = date => {
    this.setState({ dateOfBirth: date });
  }

  countrySelect = country => {
    this.setState({ countryId: country.value || '', countryCode: country.phone_code || '' });
  }

  roleSelect = role => {
    this.setState({ selectedRole: role.roleId || '' });
  }

  categoryChange = event => {
    let category = [].concat(this.state.category);

    if (event && event.target && event.target.value) {
      let value = event.target.value;

      if (category && category.length) {
        if (category.indexOf(value) > -1) {
          let categoryTemp = [];
          category.forEach(data => {
            if (data !== value) {
              categoryTemp.push(data);
            }
          })
          this.setState({ category: categoryTemp });
          return;
        }
      }

      category.push(value);
      this.setState({ category: category });
    }
  }

  subCategoryChange = event => {
    let subCategory = [].concat(this.state.subCategory);

    if (event && event.target && event.target.value) {
      let value = event.target.value;

      if (subCategory && subCategory.length) {
        if (subCategory.indexOf(value) > -1) {
          let subCategoryTemp = [];
          subCategory.forEach(data => {
            if (data !== value) {
              subCategoryTemp.push(data);
            }
          })
          // console.log(subCategoryTemp);
          this.setState({ subCategory: subCategoryTemp });
          return;
        }
      }

      subCategory.push(value);
      // console.log(subCategory);
      this.setState({ subCategory: subCategory });
    }
  }

  formValueChange = event => {
    if (event.target.name && event.target.value) {
      this.setState({ [event.target.name]: event.target.value });
    }
  }

  handleSubmit = () => {
    if (!this.state.countryId) {
      this.props.alert.error("Please select country");
      return;
    }

    if (!this.state.firstName) {
      this.props.alert.error("Please enter FirstName");
      return;
    }

    if (!this.state.lastName) {
      this.props.alert.error("Please enter LastName");
      return;
    }

    if (this.state.emailId) {
      // eslint-disable-next-line
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      var result = re.test(String(this.state.emailId).toLowerCase());
      if (!result) {
        this.props.alert.error("Enter valid Email ID");
        return;
      }
    } else {
      this.props.alert.error("Please enter Email ID");
      return;
    }

    if (!this.state.password) {
      this.props.alert.error("Please enter Password");
      return;
    }

    if (!this.state.contactNumber) {
      this.props.alert.error("Please enter Phone Number");
      return;
    }

    if (!this.state.selectedRole) {
      this.props.alert.error("Please select role");
      return;
    }

    this.setState({ loaderMessage: "Creating User Acoount", displayLoader: true });
    let payload = {
      emailId: this.state.emailId,
      password: this.state.password,
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      dateOfBirth: this.state.dateOfBirth,
      contactNumber: this.state.contactNumber,
      countryId: this.state.countryId,
      countryCode: this.state.countryCode,
      isAdmin: this.state.selectedRole === 1 ? true : false,
      roleId: this.state.selectedRole,
      categories: this.state.category,
      subCategories: this.state.subCategory
    };

    // console.log(payload)

    ApiRequest.registerUser(payload)
      .then(res => {
        if (res && res.statusCode && res.statusCode === 200) {
          this.props.history.push('/login');
          this.props.alert.success(res.message || "Registration Successful!");
        } else {
          this.props.alert.error(res.message || "Registration failed!");
        }
        this.setState({ loaderMessage: "", displayLoader: false });
      })
      .catch((err) => {
        // console.log('Login api error, ', err);
        this.setState({ loaderMessage: "", displayLoader: false });
        if (this.props && this.props.alert) {
          this.handleBadRequest(err);
        }
      })
  }

  handleBadRequest = res => {
    if (res && res.errors) {
      for (var key in res.errors) {
        if (res.errors.hasOwnProperty(key) && res.errors[key] && res.errors[key][0]) {
          this.props.alert.error(res.errors[key][0] || "Something went wrong!");
          break;
        }
      }
    }
  }

  showPassword = () => {
    this.setState({ isShowPassword: !this.state.isShowPassword })
  }

  showConfirmPassword = () => {
    this.setState({ isShowConfirmPassword: !this.state.isShowConfirmPassword })
  }

  handleKeyDown = event => {
    if (event.key === 'Enter') {
      // 👇️ your logic here
      if (this.state.formStep === 1) {
        this.setState({ formStep: 2 });
      } else {
        this.handleSubmit();
      }
    }
  };

  render() {
    return (
      <LoaderComp show={this.state.displayLoader} message={this.state.loaderMessage}>
        <div className="register login-bg-hero" onKeyDown={this.handleKeyDown}>
          <Container className="pt-4 pb-5">
            <Row>
              <div className="register-form">
                <h1 className=" pt-5 pb-0 mb-5 section-heading">Create Account</h1>
                <div className="steps">
                  <p className="mb-0">Step {this.state.formStep}/2</p>
                  <p className="font-weight-bold font-20 mt-1">
                    Tell us about yourself
                  </p>
                </div>
                <div>
                  <Form autoComplete="off">
                    <div className={`${this.state.formStep === 2 && "d-none"}`}>
                      <Row>
                        <Col md={6}>
                          <Form.Group controlId="firstname">
                            <Form.Label>First name</Form.Label>
                            <Form.Control
                              className="rounded-0"
                              type="text"
                              name="firstName"
                              onChange={this.formValueChange} />
                          </Form.Group>
                        </Col>
                        <Col md={6} className="mt-3 mt-md-0">
                          <Form.Group controlId="lastname">
                            <Form.Label>Last name</Form.Label>
                            <Form.Control
                              className="rounded-0"
                              type="text"
                              name="lastName"
                              onChange={this.formValueChange} />
                          </Form.Group>
                        </Col>
                        <Col md={6}>
                          <Form.Group controlId="birthday">
                            <Form.Label>Birthday</Form.Label>
                            <DatePicker
                              className="form-control rounded-0"
                              autoComplete="off"
                              selected={this.state.dateOfBirth}
                              onChange={this.onDateChange}
                              showMonthDropdown
                              showYearDropdown
                            />
                          </Form.Group>
                        </Col>
                        <Col md={6} className="mt-3 mt-md-0">
                          <Form.Group controlId="country">
                            <Form.Label>Country</Form.Label>
                            <Select
                              styles={colourStyles}
                              placeholder="Select Country"
                              className="country-select"
                              classNamePrefix="select"
                              isSearchable={isSearchable}
                              name="country"
                              autoComplete="off"
                              autoFocus="off"

                              options={this.state.countries.map((e) => {
                                return {
                                  label: e.label,
                                  value: e.value,
                                  phone_code: e.code,
                                };
                              })}
                              onChange={this.countrySelect}
                            />
                          </Form.Group>
                        </Col>

                        <Col md={6}>
                          <Form.Group className="mt-3" controlId="email">
                            <Form.Label>Email Address</Form.Label>
                            <Form.Control
                              className="rounded-0"
                              type="email"
                              name="emailId"
                              onChange={this.formValueChange} />
                          </Form.Group>
                        </Col>

                        <Col md={6} className="mt-3 mt-md-0">
                          <Form.Group className="mt-3" controlId="phoneNumber">
                            <Form.Label>Phone Number</Form.Label>
                            <Form.Control
                              className="rounded-0"
                              name="contactNumber"
                              onChange={this.formValueChange} />
                          </Form.Group>
                        </Col>

                        <Col md={6}>
                          <Form.Group className="mt-3 position-relative password-field" controlId="password">
                            <Form.Label>Password</Form.Label>
                            <Form.Control
                              className="rounded-0"
                              type={(this.state.isShowPassword) ? "text" : "password"}
                              name="password"
                              onChange={this.formValueChange}
                            />
                            {(this.state.isShowPassword) ? <Eye onClick={this.showPassword}/> : <EyeOff onClick={this.showPassword}/>}
                          </Form.Group>
                        </Col>
                        <Col md={6} className="mt-3 mt-md-0">
                          <Form.Group className="mt-3 position-relative password-field" controlId="confirmPassword">
                            <Form.Label>Confirm Password</Form.Label>
                            <Form.Control
                              className="rounded-0"
                              type={(this.state.isShowConfirmPassword) ? "text" : "password"}
                              name="confirmPassword"
                              onChange={this.formValueChange}
                            />
                            {(this.state.isShowConfirmPassword) ? <Eye onClick={this.showConfirmPassword}/> : <EyeOff onClick={this.showConfirmPassword}/>}
                          </Form.Group>
                        </Col>
                      </Row>
                      <Form.Group controlId="newsLetter" className="my-3">
                        <Form.Check
                          type="checkbox"
                          label="Subscribe to our newslette"
                        />
                      </Form.Group>
                      
                    </div>

                    <div className={`${this.state.formStep === 1 && "d-none"}`}>
                      <Form.Group className="mt-3" controlId="role">
                        <Form.Label>Role</Form.Label>
                        <Select
                          autoComplete="off"
                          autoFocus="off"
                          styles={colourStyles}
                          placeholder="Select Role"
                          className="role-select"
                          classNamePrefix="select"
                          defaultValue={this.state.roles[0] || null}
                          isClearable={isClearable}
                          isSearchable={isSearchable}
                          name="role"
                          options={this.state.roles}
                          getOptionLabel={(role) => (
                            <span>{role.roleName}</span>
                          )}
                          getOptionValue={(role) => role.roleId}
                          onChange={this.roleSelect}
                        />
                      </Form.Group>
                      <Row>
                        <Col md={6}>
                          <h5 className="mb-2 font-weight-bold my-4">
                            What are you looking for?
                          </h5>

                          <div className="mb-1 font-weight-bold"><label>Category</label></div>
                          <Form.Group className="flex-box flex-wrap mb-3" controlId="subCategory">
                            {(this.state.categoriesList && this.state.categoriesList.length > 0) ?
                              this.state.categoriesList.map((data, index) => {
                                return (
                                  <CategoryCheckBox key={data.id} data={data}
                                    index={index} category={this.state.category}
                                    categoryChange={this.categoryChange} />
                                )
                              })
                              : null}

                          </Form.Group>

                          <div className="mb-1 font-weight-bold"><label>Sub Category</label></div>
                          <Form.Group className="flex-box flex-wrap" controlId="subCategory">
                            {(this.state.subCategoriesList && this.state.subCategoriesList.length > 0) ?
                              this.state.subCategoriesList.map((data, index) => {
                                if (this.state.category.includes(data.categoryId.toString())) {
                                  return (
                                    <SubCategoryCheckBox key={data.id} data={data}
                                      index={index} subCategory={this.state.subCategory}
                                      subCategoryChange={this.subCategoryChange} />
                                  )
                                }
                                return null
                              })
                              : null}
                          </Form.Group>
                        </Col>
                      </Row>
                    </div>

                    <div className="buttons-form mt-4 ">
                      <div className={`${this.state.formStep === 2 && "d-none"}`}>
                        {/* <Link
                          className="MuiButtonBase-root MuiButton-root MuiButton-contained MuiButton-containeddefault mr-3"
                          variant="cancel"
                          to="/login"
                        >
                          Cancel
                        </Link> */}
                        <Button 
                          className="mr-3"
                          variant="contained"
                          onClick={() => {
                            this.props.history.push("/login")
                          }}
                        >
                          Cancel
                        </Button>
                        <Button 
                          className=""
                          variant="contained"
                          color="primary"
                          onClick={event => {
                            event.preventDefault();
                            this.setState({ formStep: 2 });
                          }}
                        >
                          Next
                        </Button>
                        {/* <Link
                          className="rounded-0 py-2 px-4 mr-3 btn btn-primary"
                          variant="primary"
                          to="/login"
                        >
                          Cancel
                        </Link>
                        <Button
                          onClick={event => {
                            event.preventDefault();
                            this.setState({ formStep: 2 });
                          }}
                          className="rounded-0 py-2 px-4"
                          variant="primary"
                        >
                          Next
                        </Button> */}
                      </div>

                      <div className={`${this.state.formStep === 1 && "d-none"}`}>
                        <Button 
                          className="mr-3"
                          variant="contained"
                          onClick={event => {
                            event.preventDefault();
                            this.setState({ formStep: 1 });
                          }}
                        >
                          Back
                        </Button>
                        <Button 
                          className=""
                          variant="contained"
                          color="primary"
                          onClick={this.handleSubmit}
                        >
                          Create Account
                        </Button>
                        {/* <Button
                          onClick={event => {
                            event.preventDefault();
                            this.setState({ formStep: 1 });
                          }}
                          className="rounded-0 py-2 px-4 mr-3"
                          variant="primary"
                        >
                          Back
                        </Button>
                        <Button
                          // type="submit"
                          className="rounded-0 py-2 px-4"
                          variant="primary"
                          onClick={this.handleSubmit}
                        >
                          Create Account
                        </Button> */}
                      </div>
                    </div>
                  </Form>
                </div>
              </div>
            </Row>
          </Container>
        </div>
      </LoaderComp>
    );
  }
};

export default withRouter(withAlert()(Register));