import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import PhasesDevelopment from "../components/PhasesDevelopment";
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Link from '@material-ui/core/Link';

const Aboutus = () => {
  return (
    <div className="mb-5 about-us">
      <Container className="pt-4 pb-5">
        <Row>
          <Col lg={8} className="mx-auto">
            <div className="pt-3 text-center">
              <h1 className="section-heading">About us</h1>
            </div>
            <Breadcrumbs maxItems={2} aria-label="breadcrumb">
              <Link underline="hover" color="inherit" href="/dashboard/home">
                Home
              </Link>
              <span color="inherit">
                About us
              </span>
            </Breadcrumbs>
            <div className="mt-4 text-center">
              <h2 className="my-4">Vision</h2>
              <p>
              Providing all users with mixed reality and connected metaverse future, by creating the central metaverse that allows for our users assets to transact and transit freely, interoperating with multi-metaverses.
              <br/><br/>
              The world is limitless.
              </p>
            </div>
            <div className="mt-5 text-center">
              <h2 className="my-4 ">Mission</h2>
              <p>
              To create and facilitate a vibrant metaverse enthusiast community through our marketplace of game-ready assets (2D &3D), rewarding experience (P2E game app) and unique identity (NFT).
              </p>
            </div>
          </Col>
        </Row>
      </Container>
      <div className="my-5">
        <img
          className="w-100 img-sky"
          src="https://cdn.shopify.com/s/files/1/0609/6152/1863/files/smart-g51ffdf447_1920_1100x.jpg"
          alt="banner"
          width="1920"
          height="1214.0"
        />
      </div>
      <Container className="py-4">
        <PhasesDevelopment />
      </Container>
    </div>
  );
};

export default Aboutus;
