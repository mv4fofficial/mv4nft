import React from "react";
import {connect} from 'react-redux';
import {withRouter} from "react-router-dom";
import {Form, Col, Container, Row} from "react-bootstrap";
// import Pagination from "react-js-pagination";
import Pagination from '@material-ui/core/TablePagination';
import ProductsFilter from "../components/products/NftProductsFilter";
import ProductsPriceFilter from "../components/products/ProductsPriceFilter";
import NftProductsList from "../components/products/NftProductsList";
import {productSearchStringUpdate} from '../user';
import LoaderComp from '../components/Loader';
import ApiRequest from '../api_service/api_requests';
import axios from "axios";
import '../assets/css/product-list.css';

import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import MUILink from '@material-ui/core/Link';

class ProductsPage extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      categoriesList: [],
      subCategoriesList: [],
      data: [],

      categories: [],
      subCategories: [],
      tags: [],
      selectedCategories: [],
      sub_category: [],
      priceFrom: 0,
      priceTo: 0,
      nftAvailability: true,
      filter: this.props.user.productSearchString,
      pageIndex: 1,
      pageSize: 12,
      totalSize: 0,

      loaderMessage: '',
      displayLoader: false,

      sliderValue: [0, 10],
      isSticky: false,
    };
  }

  componentDidMount() {
    this.getCategories();
    this.getSubCategories();
    this.getProducts();
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.user && this.props.user.productSearchString && this.props.user.productSearchString !== prevProps.user.productSearchString) {
      this.setState({filter: this.props.user.productSearchString});
    }

    if (prevState.filter !== this.state.filter) {
      this.getProducts();
    }
    window.addEventListener('scroll', this.scrollSpy);
  }

  componentWillUnmount() {
    this.props.dispatch(productSearchStringUpdate(""));
    window.addEventListener('scroll', this.scrollSpy);
  }

  scrollSpy = (e) => {
    if (window.scrollY > 370) {
      this.setState({isSticky: true})
    } else {
      this.setState({isSticky: false})
    }
  }

  getCategories = () => {
    ApiRequest.getCategory()
      .then(res => {
        if (res) {
          this.setState({categoriesList: res});
        }
      })
      .catch((err) => {
        // console.log('Categories fetch api error, ', err);
      })
  }

  getSubCategories = () => {
    ApiRequest.getSubCategory()
      .then(res => {
        if (res) {
          this.setState({subCategoriesList: res});
        }
      })
      .catch((err) => {
        // console.log('Categories fetch api error, ', err);
      })
  }

  getProducts = (category = []) => {

    this.setState({ loaderMessage: "Fetching Products", displayLoader: true });
    
    this.setState({
      data: [],
      pageIndex: 1,
      totalSize: 0,
      loaderMessage: "Fetching Products",
      displayLoader: true
    });
    let metadata = null;
    const sub_category = Array.from(category);
    // this.setState({loaderMessage: "Fetching Products", displayLoader: true});
    if (sub_category.length > 0) {
      metadata = {"sub_category": sub_category};
    }
    const options = {
      method: 'GET',
      url: `${process.env.REACT_APP_ROPSTEN_ENV_URL}/assets`,
      params: {sell_orders: 'true', collection: '0x0eBF1f438F279365CF0e2F0888627b0CD830Aa16', metadata},
      headers: {'Content-Type': 'application/json'}
    };
    var self = this;
    axios.request(options).then(function (res) {
      if (res && res.data) {
        self.setState({
          data: res.data.result.filter(e => e.orders != null),
          pageIndex: res.data.pageIndex || 1,
          totalSize: res.data.totalRecords || 0
        });
      }
      self.setState({loaderMessage: "", displayLoader: false});
    }).catch(function (error) {
      console.error(error);
    });
  }

  selectCategory = (event, subCategoryName) => {
    if (event && event.target && event.target.id) {
      let category = event.target.id;
      let selectedCategories = new Set(this.state.selectedCategories);

      if (selectedCategories.has(category)) {
        selectedCategories.delete(category);
      } else {
        selectedCategories.add(category);
      }
      const sub_category = new Set(this.state.sub_category);
      if (sub_category.has(subCategoryName)) {
        sub_category.delete(subCategoryName);
      } else {
        sub_category.add(subCategoryName);
      }
      this.setState({selectedCategories: selectedCategories}); // this.getProducts
      this.setState({sub_category});
      this.getProducts(sub_category);
    }
  }

  priceChange = (value) => {
    this.setState({priceFrom: (value[0]), priceTo: (value[1])}); // this.getProducts
    // if (event && event.target && event.target.value && event.target.id && !isNaN(event.target.value)) {
    //   if (event.target.id === "Lowest") {
    //     this.setState({ priceFrom: Number(event.target.value) }, this.getProducts);
    //   }
    //   if (event.target.id === "Highest") {
    //     this.setState({ priceTo: Number(event.target.value) }, this.getProducts);
    //   }
    // }
  }

  paginationChange = (event, page) => {
    this.setState({pageIndex: page + 1}, this.getProducts);
  }

  handleChangeRowsPerPage = (event) => {
    this.setState({pageIndex: 1, pageSize: event.target.value}, this.getProducts);
  }

  onChangeNFT = (e) => {
    // console.log(e.target.checked)
    this.setState({nftAvailability: e.target.checked});
  }

  sliderHandleChange = (event, newValue, activeThumb) => {
    const minDistance = 10;

    if (!Array.isArray(newValue)) {
      return;
    }

    if (newValue[1] - newValue[0] < minDistance) {
      if (activeThumb === 0) {
        const clamped = Math.min(newValue[0], 100 - minDistance);
        // setValue2([clamped, clamped + minDistance]);
        this.setState({sliderValue: [clamped, clamped + minDistance]});
      } else {
        const clamped = Math.max(newValue[1], minDistance);
        // setValue2([clamped - minDistance, clamped]);
        this.setState({sliderValue: [clamped - minDistance, clamped]});
      }
    } else {
      // setValue2(newValue);
      this.setState({sliderValue: newValue});
    }

    // this.priceChange(this.state.sliderValue);
  };



  render() {
    // console.log(this.state.data);
    return (
      <LoaderComp show={this.state.displayLoader} message={this.state.loaderMessage}>
        <div className="products-page">
          <div className="bg-light pt-3 text-center">
            <h1 className="section-heading">NFTs</h1>
          </div>
          <Container className="my-4">
            <Row>
              <Col lg={12} className="">
                <Breadcrumbs maxItems={2} aria-label="breadcrumb">
                  <MUILink underline="hover" color="inherit" href="/dashboard/home">
                    Home
                  </MUILink>
                  <span color="inherit">
                    NFTs
                  </span>
                </Breadcrumbs>
              </Col>
            </Row>
          </Container>
          <Container className="my-4">
            <Row>
              <div className="col-12 border-bottom">
                <Form className="products-filter">
                  <ProductsFilter state={this.state}
                    selectCategory={this.selectCategory} />
                </Form>
                <Form className={`products-filter border-bottom sticky ${(this.state.isSticky) ? 'show' : ''}`} >
                  <Container>
                    <ProductsFilter state={this.state}
                      selectCategory={this.selectCategory} />
                  </Container>
                </Form>
              </div>
              {this.state.displayLoader ?
                <Col md={8} lg={9} xl={10} className="no-products-text">
                  <span>{this.state.loaderMessage}</span></Col>
              :
              this.state.data.length > 0 ?
                <Col className="mt-3 col-12">
                  <Col className="products-pagination">
                    <div className="list-pagination">
                      {/* <Pagination
                        rowsPerPageOptions={[4, 8, 12]}
                        component="div"
                        count={this.state.totalSize}
                        rowsPerPage={this.state.pageSize}
                        page={this.state.pageIndex - 1}
                        onPageChange={this.paginationChange}
                        onRowsPerPageChange={this.handleChangeRowsPerPage} /> */}
                    </div>
                  </Col>
                  <NftProductsList state={this.state} />
                  <Col className="products-pagination">
                    <div className="list-pagination">
                      <Pagination
                        rowsPerPageOptions={[4, 8, 12]}
                        component="div"
                        count={this.state.totalSize}
                        rowsPerPage={this.state.pageSize}
                        page={this.state.pageIndex - 1}
                        onPageChange={this.paginationChange}
                        onRowsPerPageChange={this.handleChangeRowsPerPage} />
                    </div>
                    {/* <Pagination
                      activeClass="pagination-active"
                      activePage={this.state.pageIndex}
                      itemsCountPerPage={this.state.pageSize}
                      totalItemsCount={this.state.totalSize}
                      pageRangeDisplayed={5}
                      onChange={this.paginationChange}
                    /> */}
                  </Col>
                </Col>
                : <Col md={8} lg={9} xl={10} className="no-products-text">
                  <span>No Products found</span></Col>}
            </Row>
          </Container>
        </div>
      </LoaderComp>
    )
  }
};

const mapStateToProps = (state) => {
  return {
    user: state.userReducer
  }
}

export default withRouter(connect(mapStateToProps)(ProductsPage));
