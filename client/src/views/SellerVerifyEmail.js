import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { withAlert } from 'react-alert';
import { withRouter } from "react-router-dom";
import { Button } from "react-bootstrap";
import { userDataUpdate } from '../user';
import ApiRequest from '../api_service/api_requests';
import '../assets/css/verify-email.css';

function SellerVerifyEmail(props) {
    const [email, setEmail] = useState('');
    const [token, setToken] = useState('');

    useEffect(() => {
        if (props && props.match && props.match.params && props.match.params.email && props.match.params.token) {
            setEmail(props.match.params.email);
            setToken(props.match.params.token);
        }
    }, []);

    const verifyEmail = () => {
        if (token) {
            ApiRequest.verifySeller(token)
                .then(res => {
                    if (res && res.data && res.statusCode && res.statusCode === 200) {
                        props.alert.success(res.message || 'Verified successfully');
                        props.dispatch(userDataUpdate(res.data));
                        props.history.push("/");
                    } else {
                        props.alert.error(res.message || 'Something went wrong');
                    }
                })
                .catch((err) => {
                    // console.log('Verify API api error, ', err);
                })
        }
    }
    const cancelRequest = () => {
        props.history.push("/");
    }

    return (
        < div className='verify-email'>
            <Button className='btn btn-success btn-lg' onClick={verifyEmail}>Verify</Button>
            <Button className='btn btn-lg cancel-btn' onClick={cancelRequest}>Cancel</Button>
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        user: state.userReducer
    }
}

export default withRouter(withAlert()(connect(mapStateToProps)(SellerVerifyEmail)));