import React, {useState, useEffect} from "react";
import {Col, Container, Form, Row} from "react-bootstrap";
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import StatiData from "../components/data/constants";
import ApiRequest from '../api_service/api_requests';
import {useForm} from 'react-hook-form'
import {connect} from 'react-redux';
import {withAlert} from 'react-alert';
import { Link, withRouter } from "react-router-dom";
import { Logo } from "../components/Logo";

const ForgotPassword = (props) => {

  const {
    register,
    handleSubmit,
    formState: {errors},
  } = useForm();
  const onSubmit = (data) => {
    // console.log(data)
    ApiRequest.forgotPassword(data)
      .then(res => {
        if (res && res.statusCode && res.statusCode === 200) {
          props.history.push('/login');
          props.alert.success(res.message || "Please check your email to reset password.");
        } else {
          props.alert.error(res.message || "Request failed!");
        }
      })
      .catch((err) => {
        // console.log('forgotPassword api error, ', err);
      })
  }

  return (
    <div className="register login-bg-hero" >
      <Container>
        <div className="login-form">
          <Col className="mx-auto p-0">
            <div className="logo-head mb-5">
              <img src={StatiData.logo} width={100} alt="logo"></img>
            </div>

            <h4 className="section-heading wp-100">Forgot Password</h4>

            <div>
              <Form autoComplete="off" onSubmit={handleSubmit(onSubmit)}>
                <Form.Group controlId="email">
                  <TextField
                    label="Email"
                    type="email"
                    name="email"
                    placeholder="example@gmail.com"
                    variant="outlined"
                    className="wp-100"
                    {...register('email', {required: true})}
                  />
                  {errors.email && <p className="error-text">Email is required.</p>}
                </Form.Group>
                <div className="text-center mt-4">
                  <Button className="wp-100" variant="contained" color="primary" type="submit">
                    Sent Link
                  </Button>
                </div>
                <Link className="d-block mt-1 " to="/login">
                  <u>Login</u>
                </Link>
              </Form>
            </div>
          </Col>
        </div>
      </Container>
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    user: state.userReducer
  }
}
export default withRouter(withAlert()(connect(mapStateToProps)(ForgotPassword)));
