import React, { useState, useEffect } from "react";
import { Col, Container, Form, Row } from "react-bootstrap";
import { withAlert } from 'react-alert';
import { withRouter } from "react-router-dom";
import ApiRequest from "../api_service/api_requests";
import ReactTagInput from "@pathofdev/react-tag-input";
import "@pathofdev/react-tag-input/build/index.css";
import LoaderComp from '../components/Loader';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import ProductsConfirm from '../components/products/ProductsConfirm'
import { useForm } from 'react-hook-form';
import SimpleImageSlider from "react-simple-image-slider";
import { Delay } from "../components/delay";
import { FirebaseImageUplaod, FirebaseImageDelete } from "../components/firebase/ImageUpload";
import { Link } from "react-router-dom";
import { result } from "underscore";

import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import MUILink from '@material-ui/core/Link';

const useStyles = makeStyles((theme) => ({
    formControl: {
        margin: "8px 0",
        minWidth: 120,
        width: "100%"
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
    root: {
        flexGrow: 1,
    },
}));

const ProductUpdate = (props) => {
    let params = props;
    // console.log("Props in update product", params);

    const [loading, setLoading] = useState(true)

    const [tags, setTags] = useState([]);
    const [dbImg, setDbImg] = useState(null);
    const [dbImgExt, setDbImgExt] = useState(null);
    const [img, setImg] = useState(null);
    const [imgExt, setImgExt] = useState(null);

    const [categories, setCategories] = useState([]);
    const [subCategories, setSubCategories] = useState([]);
    const [selectedCatg, setSelectedCatg] = useState(null);
    const [subSelectedCatg, setSubSelectedCatg] = useState(null);
    const [productObj, setProductObj] = useState({});
    const [reqObj, setReqObj] = useState({
        "productId": "",
        "productName": "Product Name",
        "category": [
            {
                "categoryId": "",
                "categoryName": ""
            }
        ],
        "subCategory": [
            {
                "id": "",
                "subCategoryId": "",
                "subCategoryName": "",

            }
        ],
        "tags": [],
        "productPicture": null,
        "extention": "",
        "description": "",
        "price": 0,
        "quantity": 0,
        "link": "www.example.com",
        "gasFee": null,
        "createdDate": "",
        "sellerId": "",
        "seller": "",
        "sellerEmail": "",
        "fileExtention": "",
        "productImage": null,
        "categoryId": [],
        "subCategoryId": []
    });
    const [disabled, setDisabled] = useState(false);
    const [initials, setInitials] = useState("Update Product");
    // const [loader, setLoader] = useState(true);
    const [msg, setMsg] = useState("Fetching Product Details")

    const [productModal, setProductModal] = useState(false)
    const openModal = () => {
        setProductModal(true)
    }
    const closeModal = () => {
        setProductModal(false)
    }

    const [productImages, setProductImages] = useState([])
    const [removeProductImages, setRemoveProductImages] = useState([])
    const [removeProductImagesUrl, setRemoveProductImagesUrl] = useState([])
    const [imageIndex, setImageIndex] = useState(0)
    const [thumbnail, setThumbnail] = useState(imageIndex)
    const [oldThumbnail, setOldThumbnail] = useState({})
    const makeThumbnail = () => {
        setOldThumbnail(productImages[0])
        setThumbnail(imageIndex)
    }

    const classes = useStyles();

    // console.log(reqObj);

    const [loginUser, setLoginUser] = useState({});

    useEffect(() => {

        FirebaseImageDelete("https://firebasestorage.googleapis.com/v0/b/mv4f-marketplace.appspot.com/o/staging%2F0nZ7YdXC4jpAyKjmNk8JT1bAcqcpMs.png?alt=media&token=543f75ec-0884-441f-afcf-34bbc25d737e", (result) => {
            // console.log(result)
        });

        let prodId = props.match.params.product_id;

        ApiRequest.viewUser()
            .then(res => {
                if (res && res.data && res.statusCode && res.statusCode === 200) {
                    setLoginUser(res.data);
                } else {
                    if (res && res.message) {
                        this.props.alert.error("Error fetching user details");
                    }
                }
            })
            .catch((err) => {
                // console.log('Error fetching user details, ', err);
            })

        ApiRequest.getCategory()
            .then((res) => {
                if (res) {
                    // console.log("Categories!!1");
                    setCategories(res);
                } else {
                    // console.log("There are some errors getting categories", res);
                }
            })
            .catch((err) => {
                // console.log("Error in get categories", err);
            });

        ApiRequest.getSubCategory()
            .then((res) => {
                if (res) {
                    setSubCategories(res);
                } else {
                    // console.log("There are some errors getting categories", res);
                }
            })
            .catch((err) => {
                // console.log("Error in get categories", err);
            });

        ApiRequest.viewProduct(prodId)
            .then((res) => {
                // setLoader(false);
                let obj = res.data;

                // console.log("Product Detials", res.data);
                obj["fileExtention"] = res.data && res.data.extention;
                obj["productImage"] = res.data && res.data.productPicture;

                obj["productName"] = res.data && res.data.productName;

                let catgId = res.data && res.data.category[0]?.categoryId;
                obj["categoryId"] = catgId && [catgId];

                let subCatgId = res.data && res.data.subCategory[0]?.subCategoryId;
                obj["subCategoryId"] = subCatgId && [subCatgId];

                obj["link"] = res.data && res.data.link;
                obj["quantity"] = res.data && res.data.quantity;

                obj["sellerId"] = res.data && res.data.sellerId;
                obj["sellerEmail"] = res.data && res.data.sellerEmail;

                let fetchTags =
                    res.data.tags && res.data.tags.map((tag) => tag.TagName);
                // console.log("TAgsss", fetchTags);
                setTags(fetchTags);

                obj["tags"] = fetchTags;
                setReqObj({ ...obj });

                let productImages = [{
                    url: res.data.productPicture
                }];
                if (res.data.subProductPictures != null) {
                    res.data.subProductPictures.map((image) => {
                      productImages.push({
                        url: image.productPicture,
                        id: image.id
                      })
                    })
                  } else {
                    productImages.push(
                      {
                        url: "https://firebasestorage.googleapis.com/v0/b/mv4f-marketplace.appspot.com/o/staging%2F0nZ7YdXC4jpAyKjmNk8JT1bAcqcpMs.png?alt=media&token=543f75ec-0884-441f-afcf-34bbc25d737e",
                      },
                      {
                        url: "https://firebasestorage.googleapis.com/v0/b/mv4f-marketplace.appspot.com/o/staging%2F1Tmh38kAxJg8UKL3ggIGMM8xR6V2Io.jpeg?alt=media&token=31c9a364-bc3b-47ee-b1db-52e4c4bd7e51",
                      }
                    );
                }
                setProductImages([...productImages]);
                setDbImg(res.data.productPicture);
                setDbImgExt(res.data.extention);

                let catg = res.data.category[0];
                catg["id"] = res.data.category[0]?.catgoryId;
                // setSelectedCatg(catg);

                let subCatg = res.data.subCategory[0];
                subCatg["id"] = res.data.subCategory[0]?.subCategoryId;
                // setSubSelectedCatg(subCatg);
                setLoading(false)
            })
            .catch((err) => {
                setLoading(false)
                // console.log("Error in prod details", err)
            });

    }, []);
    const isClearable = true;

    const isSearchable = true;

    const colourStyles = {
        color: "red",
        control: (styles, { isFocused, isSelected }) => ({
            ...styles,
            padding: "0 8px",
            backgroundColor: "#fff",
            borderColor: isFocused || isSelected ? "#939cc4" : "#ced4da",
            boxShadow: isFocused && "0 0 0 0.2rem rgba(76, 88 ,140,.25)",
            ":hover": {
                ...styles[":active"],
                borderColor: "#ced4da",
            },
            ":focus": {
                boxShadow: "0 0 0 0.2rem rgba(76, 88 ,140,.25)",
            },
            borderRadius: 0,
        }),
        option: (styles, { data, isDisabled, isFocused, isSelected }) => {
            return {
                ...styles,
                backgroundColor: isDisabled ? null : isSelected ? "#131C20" : isFocused,
                ":active": {
                    ...styles[":active"],
                    backgroundColor: !isDisabled && "#131C20",
                },
                // ":hover": {
                //   ...styles[":active"],
                //   backgroundColor: !isDisabled && "#eee"
                // }
            };
        },
        placeholder: (styles) => ({
            ...styles,
            color: "#131C20",
        }),
        singleValue: (styles) => ({
            ...styles,
            color: "#131C20",
        }),
        menuList: (styles) => ({
            ...styles,
            color: "#282829",
        }),
        indicatorSeparator: (styles) => ({
            ...styles,
            display: "none",
        }),
        dropdownIndicator: (styles, { isFocused }) => ({
            ...styles,
            color: isFocused ? "#131C20" : "#131C20",
            ":hover": {
                ...styles[":active"],
                color: "#131C20",
            },
        }),
    };

    const handleInputs = (event) => {
        let File = event?.target?.files[0];
        let obj = reqObj;
        let fileType = `${File.type.split("/")[1]}`;
        obj["fileExtention"] = fileType;

        let imagesList = productImages;
        setProductImages([...[]]);

        if (File) {
            let reader = new FileReader();
            reader.onload = async () => {
                let result = reader.result;
                imagesList = imagesList.concat([{
                    url: result,
                    fileType: fileType,
                    fileExtension: fileType
                }]);

                // var strImage = result.replace(/^data:application\/[a-z]+;base64,/, "");
                // obj["productImage"] = strImage.split("base64,")[1];
                // setImg(strImage.split("base64,")[1]);
                // setImgExt(fileType);

                // console.log(obj);
                await Delay(1);
                setReqObj({ ...obj });
                setProductImages([...imagesList]);
                if (thumbnail == null) {
                    setThumbnail(0)
                }
                // fieldsValidation(obj);
            };
            reader.readAsDataURL(File);
        }
    };

    const handleFields = (e, field) => {
        let obj = reqObj;
        // console.log(e);
        if (field == "categoryId" || field == "subCategoryId") {
            let value = e.target.value.split(",");
            if (field == "categoryId") {
                // setSelectedCatg(value[0]);

                obj["category"] = [{
                    "categoryId": value[0],
                    "categoryName": value[1]
                }]
            }
            if (field == "subCategoryId") {
                // setSubSelectedCatg(value[0]);

                obj["subCategory"] = [{
                    "subCategoryId": value[0],
                    "subCategoryName": value[1]
                }]
            }
            if (e) {
                obj[field] = [value[0]];
            } else if (e == null) {
                delete obj[field];
            }
        } else if (field == "tags") {
            setTags(e);
            if (e.length > 0) {
                obj[field] = e;
            } else {
                delete obj[field];
            }
        } else {
            // console.log("Des", e.target.value);
            if (e.target.value == "") {
                delete obj[field];
            } else {
                obj[field] = e.target.value;
            }
        }
        // console.log("Obj", obj);
        setReqObj({ ...obj });
    };

    const margins = {
        top: { maringTop: "15px" },
        left: { maringLeft: "10px" },
    };

    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm({
        productName: reqObj.productName
    });

    const onSubmit = (data) => {
        setLoading(true)
        // console.log(reqObj)
        // console.log(data)
        // console.log(productImages)
        // console.log(thumbnail)

        let obj = {
            // obj["fileType"] = images[imageIndex].fileType;
            // obj["fileExtension"] = images[imageIndex].fileExtension;
            // obj["productImage"] = (images[imageIndex].url).replace("data:image/"+images[imageIndex].fileType+";base64,", "");
            
            "productId": reqObj.productId,
            "nftStatus": true,
            "forSale": true,
            "categoryId": reqObj.categoryId,
            "category": [reqObj.category[0].categoryName],
            "subCategoryId": reqObj.subCategoryId,
            "subCategory": [reqObj.subCategory[0].subCategoryName],
            "tags": reqObj.tags,

            "fileType": ((productImages[thumbnail].url).includes("firebasestorage.googleapis.com")) ? "jpeg" : productImages[thumbnail].fileType,
            "fileExtension": ((productImages[thumbnail].url).includes("firebasestorage.googleapis.com")) ? "jpeg" : productImages[thumbnail].fileExtention,
            "productImage": (productImages[thumbnail].url).replace("data:image/"+productImages[thumbnail].fileType+";base64,", ""),
            "dbImage": dbImg,
            "productName": reqObj.productName,
            "price": reqObj.price,
            "quantity": reqObj.quantity,
            "link": reqObj.link,
            "description": reqObj.description,
            "subProductImages": productImages,
            "deletedImages": removeProductImages,
            "thumbnail": thumbnail
        }

        setProductObj(obj);
        setLoading(false)
        showConfirm();
    }

    const updateProductData = () => {
        setLoading(true)
        setInitials("Loading....");
        setDisabled(true);
        let prodParams = productObj;
        delete prodParams["dbImage"];
        delete prodParams["fileExtension"];
        delete prodParams["fileType"];

        let imagesFilteredList = productImages.filter((value, index) => {
            return index != thumbnail
        })
        let subProductImagesFirebaseLink = [];
        // console.log(imagesFilteredList);
        prodParams["quantity"] = parseInt(prodParams["quantity"]);
        prodParams["deletedImages"] = removeProductImages;

        if (imagesFilteredList.length > 0) {
            imagesFilteredList.map((image, index) => {
                FirebaseImageUplaod(image, (url) => {

                    if (!(image.url).includes("firebasestorage.googleapis.com")) {
                        image.url = url;
                        subProductImagesFirebaseLink.push(url);    
                    }
                    

                    if (index >= imagesFilteredList.length - 1) {
                        // console.log("subProductImagesFirebaseLink", [...subProductImagesFirebaseLink]);
            
                        prodParams["subProductImages"] = [...subProductImagesFirebaseLink];
                        FirebaseImageUplaod({
                            url: prodParams['productImage'],
                            fileType: prodParams['fileType'],
                            fileExtension: prodParams['fileExtension']
                        }, async (thumbnailUrl) => {

                            if (thumbnail > 0) {
                                prodParams["deletedImages"] = [...prodParams["deletedImages"], productImages[thumbnail].id];
                                prodParams["subProductImages"] = [...prodParams["subProductImages"], oldThumbnail.url];
                            }

                            prodParams['productImage'] =  thumbnailUrl;
                            
                            ApiRequest.updateProduct(prodParams)
                            .then((res) => {
                                if (res && res.statusCode && res.statusCode === 200) {
                                    if (removeProductImagesUrl.length > 0) {
                                        removeProductImagesUrl.map((value, i) => {
                                            FirebaseImageDelete(value, (result) => {
                                                // console.log(result)
                                                if (i >= removeProductImagesUrl.length - 1) {
                                                    props.alert.success("Product Successfully Update");
                                                    props.history.push('/dashboard/seller-products');
                                                }
                                            })
                                        })
                                    } else {
                                        props.alert.success("Product Successfully Update");
                                        props.history.push('/dashboard/seller-products');
                                    }
                                } else {
                                    // console.log("Something wrong see!!!");
                                    alert("Something went wrong");
                                    setInitials("Update Product");
                                    setDisabled(false);
                                }
                            })
                            .catch((err) => {
                                // console.log("Add product api error, ", err);
                                alert("Something went wrong");
                                setDisabled(false);
                                setInitials("Update Product");
                            });

                            await Delay(1000);
                            setLoading(false)
                        })
                    }
                })
            })
        } else {
            prodParams["subProductImages"] = [];
            FirebaseImageUplaod({
                url: prodParams['productImage'],
                fileType: prodParams['fileType'],
                fileExtension: prodParams['fileExtension']
            }, async (url) => {
                prodParams['productImage'] =  url;
                // console.log("image", url);
                // console.log("prodParams", prodParams);
                
                ApiRequest.updateProduct(prodParams)
                .then((res) => {
                    if (res && res.statusCode && res.statusCode === 200) {

                        if (removeProductImagesUrl.length > 0) {
                            removeProductImagesUrl.map((value, i) => {
                                FirebaseImageDelete(value, (result) => {
                                    // console.log(result)
                                    if (i >= removeProductImagesUrl.length - 1) {
                                        props.alert.success("Product Successfully Update");
                                        props.history.push('/dashboard/seller-products');
                                    }
                                })
                            })
                        } else {
                            props.alert.success("Product Successfully Update");
                            props.history.push('/dashboard/seller-products');
                        }
                    } else {
                        // console.log("Something wrong see!!!");
                        alert("Something went wrong");
                        setInitials("Update Product");
                        setDisabled(false);
                    }
                })
                .catch((err) => {
                    // console.log("Add product api error, ", err);
                    alert("Something went wrong");
                    setDisabled(false);
                    setInitials("Update Product");
                });

                await Delay(1000);
                setLoading(false);
            })
        }
    };

    const showConfirm = () => {
        setProductModal(true);
    }

    // const removeImage = () => {
    //     setImg(null);
    // }

    const removeImage = async () => {
        // console.log(productImages)
        let removeImageList = [...removeProductImages];
        let removeImageUrlList = [...removeProductImagesUrl];
        let updateImagesList = productImages.filter( (value, index) => {
            return index != imageIndex;
        })

        if (typeof productImages[imageIndex].id != "undefined") {
            removeImageList.push(productImages[imageIndex].id);
            removeImageUrlList.push(productImages[imageIndex].url);
        }
    
        setProductImages([...[]]);
    
        await Delay(1);
    
        if (updateImagesList.length < 2) {
            setImageIndex(0)
        } else
        if (updateImagesList.length === imageIndex) {
            setImageIndex(updateImagesList.length - 1)
        }
        if (imageIndex < thumbnail) {
            setThumbnail(thumbnail - 1)
        }
        setProductImages([...updateImagesList]);
        setRemoveProductImages([...removeImageList]);
        setRemoveProductImagesUrl([...removeImageUrlList])
    }
    
    const slidePhoto = (idx) => {
        setImageIndex(idx)
    }
    
    const slidePhotoByNav  = (toRight) => {
        if (toRight) {
          if (imageIndex === productImages.length - 1) {
            setImageIndex(0)
          } else {
            setImageIndex(imageIndex + 1)
          }
        } else {
          if (imageIndex === 0) {
            setImageIndex(productImages.length - 1)
          } else {
            setImageIndex(imageIndex - 1)
          }
        }
    }

    // console.log(reqObj)
    // console.log(loginUser)

    const goToList = () => {
        // console.log(props)
        props.history.push("/dashboard/seller-products")
    }
    return (
        <LoaderComp show={loading} message={msg}>
            {(reqObj.sellerEmail === loginUser.emailId) ? <div className="checkout">
                <div className="bg-light pt-3 text-center">
                    <h1 className="section-heading">Update Product</h1>
                </div>
                <Container className="my-4">
                    <Row>
                    <Col lg={12} className="">
                        <Breadcrumbs maxItems={2} aria-label="breadcrumb">
                        <MUILink underline="hover" color="inherit" href="/dashboard/home">
                            Home
                        </MUILink>
                        <span color="inherit">
                            Update Product
                        </span>
                        </Breadcrumbs>
                    </Col>
                    </Row>
                </Container>
                <Container className="my-4">
                    <Row>
                        <Col lg={12} className="pr-lg-5">
                            <Form className="py-4" onSubmit={handleSubmit(onSubmit)}>

                                <Grid container spacing={3}>
                                    <Grid item xs={12} sm={6}>
                                        <Form.Group controlId="Image">
                                            <FormControl variant="outlined" className={classes.formControl}>
                                                <input
                                                    type="file"
                                                    accept="image/*"
                                                    id="image-upload"
                                                    style={{ display: "none" }}
                                                    {...register('image')}
                                                    onChange={(e) => handleInputs(e)}
                                                ></input>
                                                <div className="image-upload-control" style={{ justifyContent: "space-between" }}>
                                                    {/* {(dbImgExt && dbImg && img == null) && <img src={dbImg} width="50%" />}
                                                    {imgExt && img && <img src={`data:image/${imgExt.split(".")[1]};base64,${img}`} width="50%" />} */}
                                                    {productImages.length > 0 ? <SimpleImageSlider
                                                            width={"100%"}
                                                            height={300}
                                                            images={productImages}
                                                            showBullets={(productImages.length > 1) ? true : false}
                                                            showNavs={(productImages.length > 1) ? true : false}
                                                            startIndex={imageIndex}
                                                            onClick={(idx) => slidePhoto(idx)}
                                                            onClickNav={(toright) => slidePhotoByNav(toright)}
                                                            onClickBullets={(idx) => slidePhoto(idx)}
                                                        />
                                                    :
                                                        <div className="image-slide-empty-container"></div>
                                                    }
                                                    {productImages.length > 0 && thumbnail != imageIndex && <span className="remove-img" onClick={removeImage}>x</span>}
                                                    {productImages.length > 0 && thumbnail != imageIndex && <span className="set-thumbnail" onClick={makeThumbnail}>Set as thumbnail</span>}
                                                    <label htmlFor="image-upload" className="image-upload-label mt-4">Pick Picture</label>
                                                </div>
                                                {errors.image && <p className="error-text">Image is required.</p>}
                                            </FormControl>
                                        </Form.Group>
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <Form.Group controlId="ProductName">
                                            <FormControl variant="outlined" className={classes.formControl}>
                                                <TextField
                                                    label="Product Name"
                                                    type="text"
                                                    name="productName"
                                                    variant="outlined"
                                                    className="wp-100"
                                                    {...register('productName', { required: true })}
                                                    value={reqObj.productName}
                                                    onChange={(e) => handleFields(e, "productName")}
                                                />
                                                {errors.productName && <p className="error-text">Product Name is required.</p>}
                                            </FormControl>
                                        </Form.Group>
                                        <Form.Group controlId="SelectCategory">
                                            <FormControl variant="outlined" className={classes.formControl}>
                                                <InputLabel id="category-dropdown-label" required>Category</InputLabel>
                                                <Select
                                                    label="Category"
                                                    labelId="category-dropdown-label"
                                                    id="category-dropdown"
                                                    value={(Object.keys(reqObj).length > 0) ? reqObj.category[0].categoryId + "," + reqObj.category[0].categoryName : ""}
                                                    // onChange={(e) => handleFields(e, "categoryId")}
                                                    variant="outlined"
                                                    {...register('categoryId', { required: true })}
                                                    onChange={(e) => handleFields(e, "categoryId")}
                                                >
                                                    {categories.map((category, index) => (
                                                        <MenuItem key={index} value={category.id + "," + category.categoryName}>{category.categoryName}</MenuItem>
                                                    ))}
                                                </Select>
                                                {errors.categoryId && <p className="error-text">Category is required.</p>}
                                            </FormControl>
                                        </Form.Group>
                                        <Form.Group controlId="SelectSubCategory">
                                            <FormControl variant="outlined" className={classes.formControl}>
                                                <InputLabel id="sub-category-dropdown-label" required>Sub Category</InputLabel>
                                                <Select
                                                    label="Sub Category"
                                                    labelId="sub-category-dropdown-label"
                                                    id="sub-category-dropdown"
                                                    value={(Object.keys(reqObj).length > 0) ? reqObj.subCategory[0].subCategoryId + "," + reqObj.subCategory[0].subCategoryName : ""}
                                                    variant="outlined"
                                                    {...register('subCategoryId', { required: true })}
                                                    onChange={(e) => handleFields(e, "subCategoryId")}
                                                >
                                                    {subCategories.map((subCategory, index) => {
                                                        if (parseInt(reqObj.categoryId) === subCategory.categoryId) {
                                                        return <MenuItem key={index} value={subCategory.id + "," + subCategory.subCategoryName}>{subCategory.subCategoryName}</MenuItem>
                                                        }
                                                    })}
                                                </Select>
                                                {errors.subCategoryId && <p className="error-text">Sub category is required.</p>}
                                            </FormControl>
                                        </Form.Group>
                                        <Form.Group controlId="Price">
                                            <FormControl variant="outlined" className={classes.formControl}>
                                                <TextField
                                                    label="Price (SGD)"
                                                    type="number"
                                                    name="price"
                                                    variant="outlined"
                                                    className="wp-100"
                                                    value={(reqObj.price)}
                                                    // onChange={(e) => handleFields(e, "price")}
                                                    {...register('price', { required: true, min: 0 })}
                                                    onChange={(e) => handleFields(e, "price")}
                                                />
                                                {/* {errors.price && <p className="error-text">Price is required.</p>} */}
                                                {errors.price && errors.price.type === "required" && <p className="error-text">Price is required.</p>}
                                                {errors.price && errors.price.type === "min" && <p className="error-text">Price should be greater than 0.</p>}
                                            </FormControl>
                                        </Form.Group>
                                        <Form.Group controlId="Quantity">
                                            <FormControl variant="outlined" className={classes.formControl}>
                                                <TextField
                                                    label="Quantity"
                                                    type="number"
                                                    name="quantity"
                                                    variant="outlined"
                                                    className="wp-100"
                                                    value={reqObj.quantity}
                                                    // onChange={(e) => handleFields(e, "price")}
                                                    {...register('quantity', { required: true, min: 0 })}
                                                    onChange={(e) => handleFields(e, "quantity")}
                                                />
                                                {/* {errors.quantity && <p className="error-text">Quantity is required.</p>} */}
                                                {errors.quantity && errors.quantity.type === "required" && <p className="error-text">Quantity is required.</p>}
                                                {errors.quantity && errors.quantity.type === "min" && <p className="error-text">Quantity should be greater than 0.</p>}
                                            </FormControl>
                                        </Form.Group>
                                        <Form.Group controlId="Link">
                                            <FormControl variant="outlined" className={classes.formControl}>
                                                <TextField
                                                    label="Link"
                                                    type="text"
                                                    name="link"
                                                    variant="outlined"
                                                    className="wp-100"
                                                    value={reqObj.link}
                                                    {...register('link', { required: true })}
                                                    onChange={(e) => handleFields(e, "link")}
                                                />
                                                {errors.link && <p className="error-text">Link is required.</p>}
                                            </FormControl>
                                        </Form.Group>
                                    </Grid>
                                    <Grid item xs={12} sm={12}>
                                        <Form.Group controlId="Tags">
                                            <FormControl variant="outlined" className={classes.formControl}>
                                                <ReactTagInput
                                                    tags={tags}
                                                    onChange={(newTags) => handleFields(newTags, "tags")}
                                                />
                                                <label className={"tagsLabel"}>Tags</label>
                                            </FormControl>
                                        </Form.Group>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Form.Group className="mt-2">
                                            <FormControlLabel
                                                checked={reqObj.forSale}
                                                control={<Switch color="primary" defaultChecked />}
                                                label="For sale"
                                                labelPlacement="start"
                                                onChange={(e) => handleFields(e, "forSale")}
                                            />

                                            <FormControlLabel
                                                // checked={reqObj.nftStatus}
                                                control={<Switch color="primary" defaultChecked />}
                                                label="NFT Available"
                                                labelPlacement="start"
                                                onChange={(e) => handleFields(e, "nftStatus")}
                                            />
                                        </Form.Group>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Form.Group controlId="Description">
                                            <FormControl variant="outlined" className={classes.formControl}>
                                                <textarea
                                                    rows={5}
                                                    className="textareaField wp-100"
                                                    defaultValue={reqObj.description}
                                                    {...register('description')}
                                                    onChange={(e) => handleFields(e, "description")}
                                                />
                                                <label className="textareaLabel">Description</label>
                                            </FormControl>
                                        </Form.Group>
                                    </Grid>
                                    <Grid item xs={12} className="text-center mt-5">
                                        <Button
                                            type="submit"
                                            className=""
                                            variant="contained"
                                            color="primary"
                                        >
                                            {initials}
                                        </Button>
                                    </Grid>
                                </Grid>
                            </Form>
                        </Col>
                    </Row>
                </Container>
                {(productModal) && <ProductsConfirm open={productModal} openModal={openModal} closeModal={closeModal} data={productObj} handleSubmit={updateProductData} />}
            </div>
            :
                <div className="checkout">
                    <div className="bg-light pt-3 text-center">
                        <h1 className="section-heading">Update Product</h1>
                    </div>
                    <Container className="my-4">
                        <Row>
                            <Col lg={12} className="pr-lg-5 text-center">
                                <p>You are not access for this product.</p>

                                <Button
                                    type="button"
                                    className=""
                                    variant="contained"
                                    color="primary"
                                    onClick={goToList}
                                >
                                    Go to list
                                </Button>
                            </Col>
                        </Row>
                    </Container>
                </div>
            }
        </LoaderComp>
    );
};

export default withRouter(withAlert()(ProductUpdate));