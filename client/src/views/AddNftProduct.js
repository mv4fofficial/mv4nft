import React, {useState, useEffect} from "react";
import {Col, Container, Form, Row} from "react-bootstrap";
// import Select from "react-select";
import axios from "axios";
import {withAlert} from 'react-alert';
import {withRouter} from "react-router-dom";


const AddNftProduct = (props) => {
  const [fileImg, setFileImg] = useState(null);
  const sendJSONtoIPFS = async (ImgHash) => {

    try {

      const resJSON = await axios({
        method: "post",
        url: "https://api.pinata.cloud/pinning/pinJsonToIPFS",
        data: {
          "name": "test",
          "description": 100,
          "image": ImgHash
        },
        headers: {
          'pinata_api_key': `${process.env.REACT_APP_PINATA_API_KEY}`,
          'pinata_secret_api_key': `${process.env.REACT_APP_PINATA_API_SECRET}`,
        },
      });

      // console.log("final ", `ipfs://${resJSON.data.IpfsHash}`)
      const tokenURI = `ipfs://${resJSON.data.IpfsHash}`;
      // console.log("Token URI", tokenURI);

    } catch (error) {
      // console.log("JSON to IPFS: ")
      // console.log(error);
    }


  }
  const sendFileToIPFS = async (e) => {
    if (fileImg) {
      try {

        const formData = new FormData();
        formData.append('file', fileImg);
        formData.append('pinataOptions', '{"cidVersion": 0}');
        formData.append('pinataMetadata', '{"name": "MyFile", "keyvalues": {"company": "Pinata"}}');
        const resFile = await axios({
          method: "post",
          url: "https://api.pinata.cloud/pinning/pinFileToIPFS",
          data: formData,
          headers: {
            'pinata_api_key': `${process.env.REACT_APP_PINATA_API_KEY}`,
            'pinata_secret_api_key': `${process.env.REACT_APP_PINATA_API_SECRET}`,
            "Content-Type": "multipart/form-data"
          },
        });

        const ImgHash = `ipfs://${resFile.data.IpfsHash}`;
        // console.log(ImgHash);
        await sendJSONtoIPFS(ImgHash);
      } catch (error) {
        // console.log("Error sending File to IPFS: ")
        // console.log(error)
      }
    }
  }

  const pinByCid = async () => {
    var data = JSON.stringify({
      "hashToPin": "QmPh8czne9QeTngX8ypD1KwAL6QtARS2miR9kvQJWeZMMr",
      "pinataMetadata": {
        "name": "MyCustomName",
        "keyvalues": {
          "customKey": "customValue",
          "customKey2": "customValue2"
        }
      }
    });
    const resFile = await axios({
      method: "post",
      url: "https://api.pinata.cloud/pinning/pinByHash",
      data,
      headers: {
        'pinata_api_key': `${process.env.REACT_APP_PINATA_API_KEY}`,
        'pinata_secret_api_key': `${process.env.REACT_APP_PINATA_API_SECRET}`,
        "Content-Type": "application/json"
      },
    });
    // console.log(resFile);
  }

  return (
    <form onSubmit={sendFileToIPFS}>
      <input type="file" onChange={(e) => setFileImg(e.target.files[0])} required />
      <button type='submit' >Mint NFT</button>
      <button onClick={pinByCid} >Pin By Cid</button>
    </form>
  );
};

//export default AddProduct;
export default withRouter(withAlert()(AddNftProduct));
