import React from 'react';
import { withAlert } from 'react-alert';
import { withRouter } from "react-router-dom";
import { Col, Container, Row, Form } from "react-bootstrap";
import Select from "react-select";
import ProductsList from "../components/products/ProductsList";
import LoaderComp from '../components/Loader';
import ApiRequest from '../api_service/api_requests';
import '../assets/css/product-list.css';

const colourStyles = {
    color: "red",
    control: (styles, { isFocused, isSelected }) => ({
        ...styles,
        padding: "0 8px",
        backgroundColor: "#fff",
        borderColor: isFocused || isSelected ? "#939cc4" : "#ced4da",
        boxShadow: isFocused && "0 0 0 0.2rem rgba(76, 88 ,140,.25)",
        //   borderColor: "#ced4da",
        ":hover": {
            ...styles[":active"],
            borderColor: "#ced4da"
        },
        ":focus": {
            boxShadow: "0 0 0 0.2rem rgba(76, 88 ,140,.25)"
        },
        //   maxWidth: "200px"
        borderRadius: 0
    }),
    option: (styles, { data, isDisabled, isFocused, isSelected }) => {
        return {
            ...styles,
            backgroundColor: isDisabled ? null : isSelected ? "#e1e4e6" : isFocused,
            ":active": {
                ...styles[":active"],
                backgroundColor: !isDisabled && "#e1e4e6"
            },
            ":hover": {
                ...styles[":active"],
                backgroundColor: "#e1e4e6"
            }
        };
    },
    placeholder: (styles) => ({
        ...styles,
        color: "#131C20"
    }),
    singleValue: (styles) => ({
        ...styles,
        color: "#131C20"
    }),
    menuList: (styles) => ({
        ...styles,
        color: "#282829"
    }),
    indicatorSeparator: (styles) => ({
        ...styles,
        display: "none"
    }),
    dropdownIndicator: (styles, { isFocused }) => ({
        ...styles,
        color: isFocused ? "#131C20" : "#131C20",
        ":hover": {
            ...styles[":active"],
            color: "#131C20"
        }
    })
};

class Gallery extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            categoriesList: [],
            subCategoriesList: [],
            data: [],

            selectedCategories: [],
            selectedSubCategories: [],

            loaderMessage: '',
            displayLoader: false
        };
    }

    componentDidMount() {
        this.getCategories();
        this.getSubCategories();
        this.getUserOrder();
    }

    getCategories = () => {
        ApiRequest.getCategory()
            .then(res => {
                if (res && res.length) {
                    let categoriesList = [];
                    res.forEach(data => {
                        if (data && data.id && data.categoryName) {
                            categoriesList.push({ label: data.categoryName, value: data.id });
                        }
                    })

                    this.setState({ categoriesList: categoriesList });
                }
            })
            .catch((err) => {
                // console.log('Categories fetch api error, ', err);
            })
    }

    getSubCategories = () => {
        ApiRequest.getSubCategory()
            .then(res => {
                if (res && res.length) {
                    let subCategoriesList = [];
                    res.forEach(data => {
                        if (data && data.id && data.subCategoryName) {
                            subCategoriesList.push({ label: data.subCategoryName, value: data.id });
                        }
                    })

                    this.setState({ subCategoriesList: subCategoriesList });
                }
            })
            .catch((err) => {
                // console.log('Categories fetch api error, ', err);
            })
    }

    getUserOrder = () => {
        this.setState({ loaderMessage: "Fetching user details", displayLoader: true });

        let payload = {
            categoryId: this.state.selectedCategories.map(object => object.value),
            subCategoryId: this.state.selectedSubCategories.map(object => object.value)
        };

        ApiRequest.getUserOrder(payload)
            .then(res => {
                if (res && res.data && res.statusCode && res.statusCode === 200) {
                    this.setState({ data: res.data });
                } else {
                    if (res && res.message) {
                        this.props.alert.error("Error fetching user order details");
                    }
                }
                this.setState({ loaderMessage: "", displayLoader: false });
            })
            .catch((err) => {
                this.setState({ loaderMessage: "", displayLoader: false });
                // console.log('Error fetching user order details, ', err);
            })
    }

    categoryChange = data => {
        this.setState({ selectedCategories: data }, this.getUserOrder);
    }

    subCategoryChange = data => {
        this.setState({ selectedSubCategories: data }, this.getUserOrder);
    }

    render() {
        return (
            <LoaderComp show={this.state.displayLoader} message={this.state.loaderMessage}>
                <div className="products-page">
                    <div className="bg-light pt-4 pb-md-5 text-center mb-3 mb-md-5">
                        <h2 className="pb-2 mt-md-5 mb-5 section-heading">Gallery </h2>
                    </div>
                    <Container className="mb-3">
                        <Row>
                            <Col md={3} className="mt-3 mt-md-0 m-b-1-rem">
                                <Form.Group controlId="category">
                                    <Form.Label>Category</Form.Label>
                                    <Select
                                        autoComplete="off"
                                        autoFocus="off"
                                        styles={colourStyles}
                                        placeholder="Select Category"
                                        name="category"
                                        options={this.state.categoriesList}
                                        onChange={this.categoryChange}
                                        isMulti={true}
                                    />
                                </Form.Group>
                            </Col>

                            <Col md={3} className="mt-3 mt-md-0 m-b-1-rem">
                                <Form.Group controlId="subCategory">
                                    <Form.Label>SubCategory</Form.Label>
                                    <Select
                                        autoComplete="off"
                                        autoFocus="off"
                                        styles={colourStyles}
                                        placeholder="Select SubCategory"
                                        name="subCategory"
                                        options={this.state.subCategoriesList}
                                        onChange={this.subCategoryChange}
                                        isMulti={true}
                                    />
                                </Form.Group>
                            </Col>
                        </Row>

                        <Row>
                            {this.state.data.length > 0 ?
                                <Col md={12} lg={12} xl={12}>
                                    <ProductsList state={this.state} />
                                </Col>
                                : <Col md={12} lg={12} xl={12} className="no-products-text">
                                    <span>No Products found</span></Col>}
                        </Row>
                    </Container>
                </div>
            </LoaderComp>
        )
    }
}

export default withAlert()(withRouter(Gallery));