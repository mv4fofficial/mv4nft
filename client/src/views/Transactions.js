import React, { useState, useEffect } from "react";
import "./styles.css";
import { FaEdit } from "react-icons/fa";
import { AiFillDelete } from "react-icons/ai";
import "bootstrap/dist/css/bootstrap.css";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import ApiRequest from "../api_service/api_requests";
import { Link } from "react-router-dom";
import moment from "moment";

import Pagination from '@material-ui/core/TablePagination';

import { Col, Container, Row } from "react-bootstrap";

import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import MUILink from '@material-ui/core/Link';

const Transactions = () => {

  const [payments, setPayments] = useState([]);
  const [loading, setLoading] = useState(true);
  const [message, setMessage] = useState("Data fetching....")

  useEffect(() => {
    ApiRequest.getsellerPaymentDetails()
    .then(res => {
      // console.log(res.data)
      setPayments(res.data);
      setLoading(false);

      if (res.data.length === 0) {
        setMessage("Found no transaction.")
      }
    })
  }, []);

  const productsGenerator = (quantity) => {
    const items = [];
    let desc =
      "dfbsjkbsjkbkbkfgkfgkdf mf bdfm bkdf bdfm bmdf bkdf bmd bmd bkd bkdf bkd fjb df bdfjk bdfjk bd";
    for (let i = 1; i < quantity; i++) {
      items.push({
        id: i,
        image: "23/3/2022",
        name: `Product Name #${i}`,
        amount: "$100",
        buyer: `Buyer - ${i}`,
        status: "Paid",
        view: "edit",
      });
    }
    return items;
  };

  const products = productsGenerator(100);
  const columns = [
    {
      dataField: "orderId",
      text: "Transaction ID",
      style: {
        alignItems: "center",
      },
      formatter: (cell, row) => <span className="text">#{cell}</span>,
    },
    {
      dataField: "orderDate",
      text: "Date",
      formatter: (cell, row) => <span className="text">{moment(cell).format('MMM DD, YYYY hh:mm:ss')}</span>,
    },
    {
      dataField: "productName",
      text: "Product Name",
      // sort: true,
      formatter: (cell, row) => (
        <Link to={`/dashboard/product-detail/`}>
          <span className="text">{cell}</span>
        </Link> // for styling see styles.css in current folder
      ),
    },
    {
      dataField: "amount",
      text: "Amount",
      // sort: true,
      formatter: (cell, row) => (
        <span className="text">{cell}</span> // for styling see styles.css in current folder
      ),
    },
    {
      dataField: "userName",
      text: "Buyer",
      // sort: true,
      formatter: (cell, row) => (
        <Link to={`/dashboard/seller-profile/`}>
          <span className="text">{cell}</span>
        </Link> // for styling see styles.css in current folder
      ),
    },
    {
      dataField: "status",
      text: "Status",
      // sort: true,
      formatter: (cell, row) => (
        <span
          style={{
            background: "green",
            padding: "5px 10px",
            borderRadius: "15px",
            color: "white",
            fontWeight: "600"
          }}
        >
          {'Paid'}
        </span> // for styling see styles.css in current folder
      ),
    },
    {
      dataField: "view",
      text: "View",
      formatter: (cell, row) => (
        <div style={{padding: "0px 15px", fontSize: "20px"}}>
          -
        </div>
      ),
    },
  ];

  const renderTransactions = () => {
    if(!loading){
      if(payments.length > 0){
        return (<BootstrapTable
          bootstrap4
          keyField="id"
          data={payments}
          columns={columns}
          bordered={false}
          pagination={paginationFactory({ sizePerPage: 10 })}
        ></BootstrapTable>)
      }else{
        return <h4 style={{textAlign:"center", padding:"20px"}}>No Data!</h4>
      }
    }else{
      return <h4 style={{textAlign:"center", padding:"20px"}}>Loading...</h4>
    }
  }

  // console.log(payments);

  const [filters, setFilters] = useState({
    pageSize: 10,
    pageIndex: 1,
    startIndex: 0,
    endIndex: 9
  })

  const handleChangePage = (event, newPage) => {
    let filterOptions = filters;
    filterOptions.pageIndex = newPage + 1;
    filterOptions.startIndex = newPage * filters.pageSize;
    filterOptions.endIndex = ((newPage + 1) * filters.pageSize) - 1;
    setFilters({...filterOptions});
  };

  const handleChangeRowsPerPage = (event) => {
    let filterOptions = filters;
    filterOptions.pageIndex = 1;
    filterOptions.pageSize = event.target.value;
    filterOptions.startIndex = 0;
    filterOptions.endIndex = filterOptions.pageSize - 1;
    setFilters({...filterOptions});
  };

  // console.log(filters);

  return (
    <div className="checkout">
      <div className="bg-light pt-3 text-center">
        <h1 className="section-heading">Transactions</h1>
      </div>
      <Container className="my-4">
        <Row>
          <Col lg={12} className="">
            <Breadcrumbs maxItems={2} aria-label="breadcrumb">
              <MUILink underline="hover" color="inherit" href="/dashboard/home">
                Home
              </MUILink>
              <span color="inherit">
                Transactions
              </span>
            </Breadcrumbs>
          </Col>
        </Row>
      </Container>
      <Container className="my-4">
      {(Object.keys((payments)).length === 0) ?
        <div>
          <h2><small>{message}</small></h2>
        </div>
      :
        <div>
            <div className="list-pagination">
              <Pagination 
                rowsPerPageOptions={[5, 10]}
                component="div"
                count={payments.length}
                rowsPerPage={filters.pageSize}
                page={filters.pageIndex - 1}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage} />
            </div>
            <ul className="responsive-table py-2">
              {(payments).map((value, index) => (
                <li className={(index >= filters.startIndex && index <= filters.endIndex) ? "table-row" : "table-row d-none"} key={index}>
                  {/* <div className="col col-1" data-label="Job Id"><label>sr.</label><p>{index + 1}</p></div> */}
                  <div className="col col-2" data-label="Amount">
                    <div>
                      <Link to={`/dashboard/product-detail/${value.productId}`}>
                        <img
                          width="65"
                          height="50"
                          // src={`data:image/${extention};base64,${cell}`}
                          src={value.productIamge}
                        />
                      </Link>
                    </div>
                  </div>
                  <div className="col col-10" data-label="Amount"><label>Date</label><p>{moment(value.orderDate).format('MMM DD, YYYY hh:mm:ss')}</p></div>
                  <div className="col col-7" data-label="Customer Name"><label>Name</label>
                    <div>
                      <Link to={`/dashboard/product-detail/${value.productId}`}>{value.productName}</Link>
                    </div>
                  </div>
                  {/* <div className="col col-3" data-label="Amount"><label>Name</label><p><Link to={`/dashboard/product-detail/${value.productId}`}>{value.productName}</Link></p></div> */}
                  <div className="col col-5" data-label="Amount"><label>Amount (SGD)</label><p>{value.amount}</p></div>
                  <div className="col col-8" data-label="Payment Status"><label>Buyer</label><p>
                    <Link to={`/dashboard/seller-profile/${value.buyerId}`}>{(value.userName)}</Link>
                  </p></div>
                  <div className="col col-4" data-label="Payment Status"><label>Status</label>
                    <p>
                      <span
                        style={{
                          background: "green",
                          padding: "2px 10px",
                          borderRadius: "15px",
                          color: "white",
                          fontWeight: "600",
                          fontSize: "14px"
                        }}
                      >
                        {'Paid'}
                      </span>
                    </p>
                  </div>
                </li>
              ))}
              
            </ul>
            <div className="list-pagination">
              <Pagination 
                rowsPerPageOptions={[5, 10]}
                component="div"
                count={payments.length}
                rowsPerPage={filters.pageSize}
                page={filters.pageIndex - 1}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage} />
          </div>
        </div>
      }
        {/* <div className="transaction-table">
          {renderTransactions()}
        </div> */}
      </Container>
    </div>
  );
};

export default Transactions;