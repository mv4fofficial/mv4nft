/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable no-script-url */
/* eslint-disable jsx-a11y/alt-text */
import React, { useState, useEffect } from "react";
import "./styles.css";
import { FaEdit } from "react-icons/fa";
import { AiFillDelete } from "react-icons/ai";
import "bootstrap/dist/css/bootstrap.css";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import filterFactory, { selectFilter } from "react-bootstrap-table2-filter";
import ApiRequest from "../api_service/api_requests";
import { Link } from "react-router-dom";
import { Col, Container, Row, Form } from "react-bootstrap";
import Select from "react-select";

const SellerProfile = (props) => {
  // console.log("Props in seller", props.props.match.params);
  const [AllProducts, setAllProducts] = useState([]);
  const [sortProducts, setSortProducts] = useState([]);
  const [loading, setLoading] = useState(true);
  const [userData, setUserData] = useState(null);
  const [catg, setCatg] = useState([]);

  useEffect(() => {
    let profileId = props.props.match.params
      ? props.props.match.params.profile_id
      : 0;
    // console.log("Profile ID", props.props.match.params);
    ApiRequest.getsellerProducts(profileId).then((res) => {
      setAllProducts(res.data);
      setSortProducts(res.data);
      setLoading(false);
    });
    ApiRequest.viewUserById(profileId).then((res) => {
      // console.log("User Data", res.data);
      setUserData(res.data);
    });
    ApiRequest.getAllCategories().then((res) => {
      let catg = [{ value: "all products", label: "Select Catgegory" }];
      res.data.map((data) =>
        catg.push({ value: data.categoryName, label: data.categoryName })
      );
      // console.log("catg", catg);
      setCatg(catg);
    });
  }, []);

  const handleDelete = (e) => {
    // console.log("handle Delete click", e);
    if (window.confirm("You want to Delete Product")) {
      ApiRequest.deleteProduct(e).then((res) => {
        if (res.statusCode === 200) {
          window.location.reload();
        } else {
          // alert("Something went Wrong!");
          props.alert.error("Something went Wrong!");
        }
      });
    }
  };
  const selectOptions = catg;

  const columns = [
    {
      text: "#",
      formatter: (cell, row, ind) => {
        return <div className="serial">{ind + 1}</div>;
      },
      headerStyle: (colum, colIndex) => {
        return { width: "50px", backgroundColor: "#131c20", color: "white" };
      },
    },
    {
      dataField: "productId",
      text: "Product ID",
    },
    {
      dataField: "productName",
      text: "Product Name",
    },
    {
      dataField: "categories",
      text: "Category",
    },
    {
      dataField: "productImage",
      text: "Product Image",
      formatter: (cell, row) => {
        let extention = row.extention && row.extention != "" ? row.extention.split(".")[1] : "";
        return (
          <Link to={`/dashboard/product-detail/${row.productId}`}>
            <img
              width="80"
              height="50"
              src={row.productImage}
            />
          </Link>
        );
      },
    },
    {
      dataField: "description",
      text: "Product Details",
      // sort: true,
      formatter: (cell, row) => (
        <span className="text">{cell}</span> // for styling see styles.css in current folder
      ),
    },
    {
      dataField: "tags",
      text: "Actions",
      formatter: (cell, row) => {
        let prodId = row.productId;
        return (
          <div>
            <Link to={`/dashboard/update-product/${row.productId}`}>
              <FaEdit />
            </Link>
            <a
              href="javascript:void(0)"
              style={{ marginLeft: "10px" }}
              onClick={() => handleDelete(prodId)}
            >
              <AiFillDelete />
            </a>
          </div>
        );
      },
    },
  ];

  const renderProducts = () => {
    if (!loading) {
      if (AllProducts.length > 0) {
        return (
          <BootstrapTable
            bootstrap4
            keyField="productId"
            data={sortProducts}
            columns={columns}
            bordered={false}
            pagination={paginationFactory({ sizePerPage: 10 })}
          ></BootstrapTable>
        );
      } else {
        return (
          <h4 style={{ textAlign: "center", padding: "20px" }}>No Data!</h4>
        );
      }
    } else {
      return (
        <h4 style={{ textAlign: "center", padding: "20px" }}>Loading...</h4>
      );
    }
  };

  const categoryChange = (e) => {
    // console.log("Category Change", e);
    let updateList = [];
    if (e.value != "all products") {
      updateList = AllProducts.filter((prod) => prod.categories[0] == e.value);
    } else {
      updateList = AllProducts;
    }
    setSortProducts(updateList);
  };

  return (
    <div>
      <div className="bg-light" style={{ height: "200px", width: "100%" }}>
        {userData && (
          <div
            style={{
              height: "200px",
              width: "100%",
              WebkitBoxAlign: "center",
              WebkitBoxPack: "center",
              overflow: "hidden",
            }}
          >
            {userData.extention && userData.extention != "" && (
              <img
                src={`data:image/${userData.extention.split(".")[1]};base64,${
                  userData.profilePhoto
                }`}
                style={{
                  height: "100%",
                  transition: "opacity 400ms ease 0s",
                  width: "100%",
                }}
              />
            )}
          </div>
        )}
      </div>
      <div className="text-center">
        <div
          className="bg-light"
          style={{
            width: "100px",
            height: "100px",
            position: "relative",
            borderRadius: "100px",
            top: "-36px",
            margin: "0 auto",
            border: "2px solid rgb(229, 232, 235)",
          }}
        >
          {userData && (
            <div style={{ color: "black", fontSize: "56px" }}>
              {userData.name && userData.name != " "
                ? userData.name.split(" ")[0][0].toUpperCase()
                : ""}
            </div>
          )}
        </div>
      </div>
      <div style={{ marginBottom: "50px", position: "relative", top: "-20px" }}>
        {userData && (
          <div
            className="text-center"
            style={{
              fontWeight: "600",
              fontSize: "40px",
              letterSpacing: "0px",
              color: "rgb(4, 17, 29)",
              maxWidth: "500px",
              margin: "0 auto",
            }}
          >
            {userData.name && userData.name != " " ? userData.name : ""}
          </div>
        )}
        {userData && (
          <div
            className="text-center"
            style={{
              maxWidth: "155px",
              margin: "8px auto",
              border: "1px solid black",
              borderRadius: "5px",
            }}
          >
            {userData.phoneNumber ? userData.phoneNumber : ""}
          </div>
        )}
        {userData && (
          <div
            className="text-center"
            style={{
              fontSize: "16px",
              color: "rgb(112, 122, 131)",
              maxWidth: "500px",
              margin: "10px auto",
            }}
          >
            {userData.bio ? userData.bio : ""}
          </div>
        )}
      </div>
      {userData && (
        <div style={{ margin: "0 10px" }}>
          <Row>
            <Col md={3} className="mt-3 mt-md-0 m-b-1-rem">
              <Form.Group controlId="category">
                <Form.Label>Category</Form.Label>
                <Select
                  autoComplete="off"
                  autoFocus="off"
                  // styles={colourStyles}
                  placeholder="Select Category"
                  name="category"
                  options={catg}
                  onChange={(e) => categoryChange(e)}
                />
              </Form.Group>
            </Col>
          </Row>
        </div>
      )}
      {renderProducts()}
    </div>
  );
};

export default SellerProfile;
