// no more use
/* eslint-disable jsx-a11y/alt-text */
import React, { useState, useEffect } from "react";
import { Col, Container, Form, Row } from "react-bootstrap";
// import Select from "react-select";
import { withAlert } from 'react-alert';
import { withRouter } from "react-router-dom";
// import categories from "../data/categories";
import ApiRequest from "../api_service/api_requests";

import ReactTagInput from "@pathofdev/react-tag-input";
import "@pathofdev/react-tag-input/build/index.css";

import LoaderComp from '../components/Loader';

import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import ProductsConfirm from '../components/products/ProductsConfirm'
// import ProductsConfirm from '../components/products/ProductsConfirm'

import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import MUILink from '@material-ui/core/Link';

import { useForm } from 'react-hook-form';

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: "8px 0",
    minWidth: 120,
    width: "100%"
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  root: {
    flexGrow: 1,
  },
}));

const UpdateProduct = (props) => {
  let params = props;
  // console.log("Props in update product", params);

  const [tags, setTags] = useState([]);
  const [img, setImg] = useState(null);
  const [imgExt, setImgExt] = useState(null);
  const [categories, setCategories] = useState([]);
  const [subCategories, setSubCategories] = useState([]);
  const [selectedCatg, setSelectedCatg] = useState(null);
  const [subSelectedCatg, setSubSelectedCatg] = useState(null);
  const [productObj, setProductObj] = useState({});
  const [reqObj, setReqObj] = useState({
    "productId": "",
    "productName": "",
    "category": [
      {
        "categoryId": "",
        "categoryName": ""
      }
    ],
    "subCategory": [
      {
        "id": "",
        "subCategoryId": "",
        "subCategoryName": "",
          
      }
    ],
    "tags": [],
    "productPicture": null,
    "extention": "",
    "description": "",
    "price": 0,
    "gasFee": null,
    "createdDate": "",
    "sellerId": "",
    "seller": "",
    "sellerEmail": "",
    "fileExtention": "",
    "productImage": null,
    "categoryId": [],
    "subCategoryId": []
});
  const [disabled, setDisabled] = useState(false);
  const [initials, setInitials] = useState("Update Product");
  const [loader, setLoader] = useState(true);
  const [msg, setMsg] = useState("Fetching Product Details")

  const [productModal, setProductModal] = useState(false)
  const openModal = () => {
    setProductModal(true)
  }
  const closeModal = () => {
    setProductModal(false)
  }

  const classes = useStyles();

  // console.log(reqObj);

  useEffect(() => {
    let prodId = props.match.params.product_id;
    ApiRequest.getCategory()
      .then((res) => {
        if (res) {
          // console.log("Categories!!1");
          setCategories(res);
        } else {
          // console.log("There are some errors getting categories", res);
        }
      })
      .catch((err) => {
        // console.log("Error in get categories", err);
      });

    ApiRequest.getSubCategory()
      .then((res) => {
        if (res) {
          setSubCategories(res);
        } else {
          // console.log("There are some errors getting categories", res);
        }
      })
      .catch((err) => {
        // console.log("Error in get categories", err);
      });

    ApiRequest.viewProduct(prodId)
      .then((res) => {
        setLoader(false);
        let obj = res.data;
        // console.log("Product Detials", res.data);
        obj["fileExtention"] = res.data && res.data.extention;
        obj["productImage"] = res.data && res.data.productPicture;

        let catgId = res.data && res.data.category[0]?.categoryId;
        obj["categoryId"] = catgId && [catgId];

        let subCatgId = res.data && res.data.subCategory[0]?.subCategoryId;
        obj["subCategoryId"] = subCatgId && [subCatgId];

        let fetchTags =
          res.data.tags && res.data.tags.map((tag) => tag.TagName);
        // console.log("TAgsss", fetchTags);
        setTags(fetchTags);

        obj["tags"] = fetchTags;
        setReqObj({...obj});

        setImg(res.data.productPicture);
        setImgExt(res.data.extention);

        let catg = res.data.category[0];
        catg["id"] = res.data.category[0]?.catgoryId;
        setSelectedCatg(catg);

        let subCatg = res.data.subCategory[0];
        subCatg["id"] = res.data.subCategory[0]?.subCategoryId;
        setSubSelectedCatg(subCatg);
      })
      .catch((err) => // console.log("Error in prod details", err));

  }, []);
  const isClearable = true;

  const isSearchable = true;

  const colourStyles = {
    color: "red",
    control: (styles, { isFocused, isSelected }) => ({
      ...styles,
      padding: "0 8px",
      backgroundColor: "#fff",
      borderColor: isFocused || isSelected ? "#939cc4" : "#ced4da",
      boxShadow: isFocused && "0 0 0 0.2rem rgba(76, 88 ,140,.25)",
      //   borderColor: "#ced4da",
      ":hover": {
        ...styles[":active"],
        borderColor: "#ced4da",
      },
      ":focus": {
        boxShadow: "0 0 0 0.2rem rgba(76, 88 ,140,.25)",
      },
      //   maxWidth: "200px"
      borderRadius: 0,
    }),
    option: (styles, { data, isDisabled, isFocused, isSelected }) => {
      return {
        ...styles,
        backgroundColor: isDisabled ? null : isSelected ? "#131C20" : isFocused,
        ":active": {
          ...styles[":active"],
          backgroundColor: !isDisabled && "#131C20",
        },
        // ":hover": {
        //   ...styles[":active"],
        //   backgroundColor: !isDisabled && "#eee"
        // }
      };
    },
    placeholder: (styles) => ({
      ...styles,
      color: "#131C20",
    }),
    singleValue: (styles) => ({
      ...styles,
      color: "#131C20",
    }),
    menuList: (styles) => ({
      ...styles,
      color: "#282829",
    }),
    indicatorSeparator: (styles) => ({
      ...styles,
      display: "none",
    }),
    dropdownIndicator: (styles, { isFocused }) => ({
      ...styles,
      color: isFocused ? "#131C20" : "#131C20",
      ":hover": {
        ...styles[":active"],
        color: "#131C20",
      },
    }),
  };

  const handleInputs = (event) => {
    let File = event?.target?.files[0];
    let obj = reqObj;
    let fileType = `.${File.type.split("/")[1]}`;
    obj["fileExtention"] = fileType;
    if (File) {
      let reader = new FileReader();
      reader.onload = () => {
        let result = reader.result;
        var strImage = result.replace(/^data:application\/[a-z]+;base64,/, "");
        obj["productImage"] = strImage.split("base64,")[1];
        setImg(strImage.split("base64,")[1]);
        setImgExt(fileType);

        // console.log(obj);
        setReqObj({...obj});
        // fieldsValidation(obj);
      };
      reader.readAsDataURL(File);
    }
  };

  const updateProductData = () => {
    setInitials("Loading....");
    setDisabled(true);
    let prodParams = productObj;
    
    ApiRequest.updateProduct(prodParams)
      .then((res) => {
        if (res && res.statusCode && res.statusCode === 200) {
          props.alert.success("Product Successfully Update");
          props.history.push('/dashboard/seller-products');
        } else {
          // console.log("Something wrong see!!!");
          // alert("Something went wrong");
          props.alert.error("Something went Wrong!");
          setInitials("Update Product");
          setDisabled(false);
        }
      })
      .catch((err) => {
        // console.log("Add product api error, ", err);
        // alert("Something went wrong");
        props.alert.error("Something went Wrong!");
        setDisabled(false);
        setInitials("Update Product");
      });
  };

  const handleFields = (e, field) => {
    let obj = reqObj;
    // console.log(e);
    if (field == "categoryId" || field == "subCategoryId") {
      let value = e.target.value.split(",");
      if (field == "categoryId") {
        setSelectedCatg(value[0]);

        obj["category"] = [{
          "categoryId":   value[0],
          "categoryName": value[1]
        }]
      }
      if (field == "subCategoryId") {
        setSubSelectedCatg(value[0]);

        obj["subCategory"] = [{
          "subCategoryId":   value[0],
          "subCategoryName": value[1]
        }]
      }
      if (e) {
        obj[field] = [value[0]];
      } else if (e == null) {
        delete obj[field];
      }
    } else if (field == "tags") {
      setTags(e);
      if (e.length > 0) {
        obj[field] = e;
      } else {
        delete obj[field];
      }
    } else {
      // console.log("Des", e.target.value);
      if (e.target.value == "") {
        delete obj[field];
      } else {
        obj[field] = e.target.value;
      }
    }
    // console.log("Obj", obj);
    setReqObj({...obj});
    // fieldsValidation(obj);
  };

  const fieldsValidation = (fieldsObj) => {
    if ("tags" in fieldsObj == false) {
      setDisabled(true);
    } else if ("productName" in fieldsObj == false) {
      setDisabled(true);
    } else if ("categoryId" in fieldsObj == false) {
      setDisabled(true);
    } else if ("subCategoryId" in fieldsObj == false) {
      setDisabled(true);
    } else if ("price" in fieldsObj == false) {
      setDisabled(true);
    } else if ("link" in fieldsObj == false) {
      setDisabled(true);
    } else if ("description" in fieldsObj == false) {
      setDisabled(true);
    } else if ("productPicture" in fieldsObj == false) {
      // console.log("inside product Image");
      setDisabled(true);
    } else {
      setDisabled(false);
    }
  };

  const margins = {
    top: { maringTop: "15px" },
    left: { maringLeft: "10px" },
  };

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onSubmit = (data) => {
    // console.log(reqObj)
    // console.log(data)

    let obj = {
      "productId":            reqObj.productId,
      "nftStatus":      true,
      "forSale":              true,
      "categoryId":           reqObj.categoryId,
      "category":             [reqObj.category[0].categoryName],
      "subCategoryId":        reqObj.subCategoryId,
      "subCategory":          [reqObj.subCategory[0].subCategoryName],
      "tags":                 reqObj.tags,
      "fileType":             "jpeg",
      "fileExtension":        reqObj.fileExtention,
      "productImage":         reqObj.productImage,
      "productName":          reqObj.productName,
      "price":                reqObj.price,
      "quantity":             reqObj.quantity,
      "link":                 reqObj.link,
      "description":          reqObj.description
    }

    setProductObj(obj);

    showConfirm();
  }

  const showConfirm = () => {
    setProductModal(true);
  }

  return (
    <LoaderComp show={loader} message={msg}>
    <div className="checkout">
      <div className="bg-light pt-3 text-center">
        <h1 className="section-heading">Update Product</h1>
      </div>
      <Container className="my-4">
        <Row>
          <Col lg={12} className="">
            <Breadcrumbs maxItems={2} aria-label="breadcrumb">
              <MUILink underline="hover" color="inherit" href="/dashboard/home">
                Home
              </MUILink>
              <span color="inherit">
                Update Product
              </span>
            </Breadcrumbs>
          </Col>
        </Row>
      </Container>
      <Container className="my-4">
        <Row>
          <Col lg={12} className="pr-lg-5">
            <Form className="py-4" onSubmit={handleSubmit(onSubmit)}>

              <Grid container spacing={3}>
                <Grid item xs={12} sm={6}>
                  <Form.Group controlId="Image">
                    <FormControl variant="outlined" className={classes.formControl}>
                      <input
                        type="file"
                        accept="image/*"
                        id="image-upload"
                        style={{ display: "none" }}
                        {...register('image')}
                        onChange={(e) => handleInputs(e)}
                      ></input>
                      <div className="image-upload-control">
                        {imgExt && img && <img src={img} width="50%" />}
                        <label htmlFor="image-upload" className="image-upload-label">Pick Picture</label>
                      </div>
                      {errors.image && <p className="error-text">Image is required.</p>}
                    </FormControl>
                  </Form.Group>
                </Grid>
                <Grid item xs={12} sm={6}>
                  <Form.Group controlId="ProductName">
                    <FormControl variant="outlined" className={classes.formControl}>
                      <TextField
                        label="Product Name"
                        type="text"
                        name="productName"
                        variant="outlined"
                        className="wp-100"
                        value={reqObj.productName}
                        required
                        {...register('productName', { required: true })}
                        onChange={(e) => handleFields(e, "productName")}
                      />
                      {errors.productName && <p className="error-text">Product Name is required.</p>}
                    </FormControl>
                  {/* <Form.Control
                    className="rounded-0"
                    type="text"
                    placeholder="Product Name to display"
                    onChange={(e) => handleFields(e, "productName")}
                  /> */}
                  </Form.Group>
                  <Form.Group controlId="SelectCategory">
                    <FormControl variant="outlined" className={classes.formControl}>
                      <InputLabel id="category-dropdown-label" required>Category</InputLabel>
                      <Select
                        label="Category"
                        labelId="category-dropdown-label"
                        id="category-dropdown"
                        value={(Object.keys(reqObj).length > 0) ? reqObj.category[0].categoryId+","+reqObj.category[0].categoryName : ""}
                        // onChange={(e) => handleFields(e, "categoryId")}
                        variant="outlined"
                        {...register('categoryId', { required: true })}
                        onChange={(e) => handleFields(e, "categoryId")}
                      >
                        {/* <MenuItem value="">
                          <em>None</em>
                        </MenuItem> */}
                        {categories.map((category, index) => (
                          <MenuItem key={index} value={category.id+","+category.categoryName}>{category.categoryName}</MenuItem>
                        ))}
                      </Select>
                      {errors.categoryId && <p className="error-text">Category is required.</p>}
                    </FormControl>
                    {/* <Select
                      styles={colourStyles}
                      placeholder="Select Category"
                      className="coin-select"
                      classNamePrefix="select"
                      defaultValue={categories[0]}
                      isClearable={isClearable}
                      isSearchable={isSearchable}
                      onChange={(e) => handleFields(e, "categoryId")}
                      name="category"
                      options={categories}
                      getOptionLabel={(coin) => coin.categoryName}
                      getOptionValue={(coin) => coin.id}
                    /> */}
                  </Form.Group>
                  <Form.Group controlId="SelectSubCategory">
                    <FormControl variant="outlined" className={classes.formControl}>
                      <InputLabel id="sub-category-dropdown-label" required>Sub Category</InputLabel>
                      <Select
                        label="Sub Category"
                        labelId="sub-category-dropdown-label"
                        id="sub-category-dropdown"
                        value={(Object.keys(reqObj).length > 0) ? reqObj.subCategory[0].subCategoryId+","+reqObj.subCategory[0].subCategoryName : ""}
                        // onChange={(e) => handleFields(e, "subCategoryId")}
                        variant="outlined"
                        {...register('subCategoryId', { required: true })}
                        onChange={(e) => handleFields(e, "subCategoryId")}
                      >
                        {/* <MenuItem value="">
                          <em>None</em>
                        </MenuItem> */}
                        {subCategories.map((subCategory, index) => (
                          <MenuItem key={index} value={subCategory.id+","+subCategory.subCategoryName}>{subCategory.subCategoryName}</MenuItem>
                        ))}
                      </Select>
                      {errors.subCategoryId && <p className="error-text">Sub category is required.</p>}
                    </FormControl>
                    {/* <Select
                      styles={colourStyles}
                      placeholder="Select Sub Category"
                      className="coin-select"
                      classNamePrefix="select"
                      isClearable={isClearable}
                      isSearchable={isSearchable}
                      onChange={(e) => handleFields(e, "subCategoryId")}
                      name="subCategory"
                      options={subCategories}
                      getOptionLabel={(coin) => coin.subCategoryName}
                      getOptionValue={(coin) => coin.id}
                    /> */}
                  </Form.Group>
                </Grid>
                <Grid item xs={12} sm={6}>
                  <Form.Group controlId="Price">
                    <FormControl variant="outlined" className={classes.formControl}>
                      <TextField
                        label="Price (SGD)"
                        type="number"
                        name="price"
                        variant="outlined"
                        className="wp-100"
                        value={reqObj.price}
                        required
                        // onChange={(e) => handleFields(e, "price")}
                        {...register('price', { required: true })}
                        onChange={(e) => handleFields(e, "price")}
                      />
                      {errors.price && <p className="error-text">Price is required.</p>}
                    </FormControl>
                    {/* <Form.Control
                      className="rounded-0 mt-2"
                      type="number"
                      onChange={(e) => handleFields(e, "price")}
                      placeholder="Price"
                    /> */}
                  </Form.Group>
                </Grid>
                <Grid item xs={12} sm={6}>
                  <Form.Group controlId="Quantity">
                    <FormControl variant="outlined" className={classes.formControl}>
                      <TextField
                        label="Quantity"
                        type="number"
                        name="quantity"
                        variant="outlined"
                        className="wp-100"
                        value={reqObj.quantity}
                        required
                        // onChange={(e) => handleFields(e, "quantity")}
                        {...register('quantity', { required: true })}
                        onChange={(e) => handleFields(e, "quantity")}
                      />
                      {errors.quantity && <p className="error-text">Quantity is required.</p>}
                    </FormControl>
                    {/* <Form.Control
                      className="rounded-0 mt-2"
                      type="number"
                      onChange={(e) => handleFields(e, "price")}
                      placeholder="Price"
                    /> */}
                  </Form.Group>
                </Grid>
                <Grid item xs={12} sm={6}>
                  <Form.Group controlId="Link">
                    <FormControl variant="outlined" className={classes.formControl}>
                      <TextField
                        label="Link"
                        type="text"
                        name="link"
                        variant="outlined"
                        className="wp-100"
                        value={reqObj.link}
                        required
                        // onChange={(e) => handleFields(e, "link")}
                        {...register('link', { required: true })}
                        onChange={(e) => handleFields(e, "link")}
                      />
                      {errors.link && <p className="error-text">Link is required.</p>}
                    </FormControl>
                    {/* <Form.Control
                      className="rounded-0 mt-2"
                      type="text"
                      onChange={(e) => handleFields(e, "link")}
                      placeholder="Product Link"
                    /> */}
                  </Form.Group>
                </Grid>
                <Grid item xs={12} sm={6}>
                  <Form.Group controlId="Tags">
                    <FormControl variant="outlined" className={classes.formControl}>
                      <ReactTagInput
                        tags={tags}
                        onChange={(newTags) => handleFields(newTags, "tags")}
                        //onChange={(newTags) => setTags(newTags)} 
                      />
                      <label className={"tagsLabel"}>Tags</label>
                    </FormControl>
                  </Form.Group>
                </Grid>
                <Grid item xs={12}>
                  <Form.Group className="mt-3">
                    <FormControlLabel
                      checked={reqObj.forSale}
                      control={<Switch color="primary" defaultChecked />}
                      label="For sale"
                      labelPlacement="start"
                      onChange={(e) => handleFields(e, "forSale")}
                      // {...register('forSale')}
                    />

                    <FormControlLabel
                      // checked={reqObj.nftStatus}
                      control={<Switch color="primary" defaultChecked />}
                      label="NFT Available"
                      labelPlacement="start"
                      onChange={(e) => handleFields(e, "nftStatus")}
                      // {...register('nftStatus')}
                    />
                  </Form.Group>
                </Grid>
                <Grid item xs={12}>
                  <Form.Group controlId="Description">
                    <FormControl variant="outlined" className={classes.formControl}>
                      <textarea
                        // value={(e) => // console.log("Text Area value", e)}
                        // onChange={(e) => handleFields(e, "description")}
                        rows={5}
                        className="textareaField wp-100"
                        defaultValue={reqObj.description}
                        {...register('description')}
                        onChange={(e) => handleFields(e, "description")}
                      />
                      <label className="textareaLabel">Description</label>
                    </FormControl>
                  </Form.Group>
                </Grid>
                <Grid item xs={12} className="text-center mt-5">
                  {/* <Button
                    className="rounded-0 py-2 px-4 mt-4"
                    variant="primary"
                    disabled={disabled}
                    onClick={handleSubmit}
                  >
                    {initials}
                  </Button> */}
                  <Button
                    type="submit"
                    className=""
                    variant="contained"
                    color="primary"
                    // onClick={showConfirm}
                  >
                    {initials}
                  </Button>
                </Grid>
              </Grid>

              {/* <Form.Group controlId="formBasicEmail">
                <h5 className="mb-3">
                  Product Name
                  <span
                    style={{
                      color: "red",
                      fontSize: "14px",
                      position: "relative",
                      top: "-3px",
                    }}
                  >
                    *
                  </span>
                </h5>
                <Form.Control
                  defaultValue={reqObj.productName}
                  className="rounded-0"
                  type="text"
                  placeholder="Product Name to display"
                  onChange={(e) => handleFields(e, "productName")}
                />
              </Form.Group>
              <h5 className="mt-4 mb-3">
                Product Image
                <span
                  style={{
                    color: "red",
                    fontSize: "14px",
                    position: "relative",
                    top: "-3px",
                  }}
                >
                  *
                </span>
              </h5>
              <Form.Group controlId="apartmentetc" className="mt-3">
                <input
                  type="file"
                  accept="image/*"
                  onChange={(e) => handleInputs(e)}
                ></input>
                <div style={{ marginTop: "15px" }}>
                  {imgExt && img && (
                    <img
                      src={`data:image/${imgExt.split(".")[1]};base64,${img}`}
                      width="50%"
                    />
                  )}
                  {imgExt && img && (
                    <img
                      src={`data:image/${imgExt.split(".")[1]};base64,${img}`}
                      width="30%"
                      style={{ marginLeft: "10px" }}
                    />
                  )}
                </div>
              </Form.Group>
              <h5 className="mt-4 mb-3">
                Category
                <span
                  style={{
                    color: "red",
                    fontSize: "14px",
                    position: "relative",
                    top: "-3px",
                  }}
                >
                  *
                </span>
              </h5>
              <Form.Group controlId="selectCategory">
                <Select
                  styles={colourStyles}
                  placeholder="Select Category"
                  className="coin-select"
                  classNamePrefix="select"
                  defaultValue={(e) => // console.log("default value", e)}
                  isClearable={isClearable}
                  isSearchable={isSearchable}
                  onChange={(e) => handleFields(e, "categoryId")}
                  name="category"
                  options={categories}
                  getOptionLabel={(coin) => coin.categoryName}
                  getOptionValue={(coin) => coin.id}
                  value={selectedCatg && selectedCatg}
                />
              </Form.Group>
              <h5 className="mt-4 mb-3">
                Sub Category
                <span
                  style={{
                    color: "red",
                    fontSize: "14px",
                    position: "relative",
                    top: "-3px",
                  }}
                >
                  *
                </span>
              </h5>
              <Form.Group controlId="selectSubCategory">
                <Select
                  styles={colourStyles}
                  placeholder="Select Sub Category"
                  className="coin-select"
                  classNamePrefix="select"
                  isClearable={isClearable}
                  isSearchable={isSearchable}
                  onChange={(e) => handleFields(e, "subCategoryId")}
                  name="subCategory"
                  options={subCategories}
                  getOptionLabel={(coin) => coin.subCategoryName}
                  getOptionValue={(coin) => coin.id}
                  value={subSelectedCatg && subSelectedCatg}
                />
              </Form.Group>
              <h5 className="mt-4 mb-3">
                Tags
                <span
                  style={{
                    color: "red",
                    fontSize: "14px",
                    position: "relative",
                    top: "-3px",
                  }}
                >
                  *
                </span>
              </h5>
              <Form.Group controlId="address" className="mt-3">
                <ReactTagInput
                  tags={tags}
                  onChange={(newTags) => handleFields(newTags, "tags")}
                />
              </Form.Group>
              <h5 className="mt-4 mb-3">
                Description
                <span
                  style={{
                    color: "red",
                    fontSize: "14px",
                    position: "relative",
                    top: "-3px",
                  }}
                >
                  *
                </span>
              </h5>
              <Form.Group controlId="apartmentetc" className="mt-3">
                <textarea
                  defaultValue={reqObj ? reqObj.description : ""}
                  onChange={(e) => handleFields(e, "description")}
                  rows={7}
                  cols={100}
                />
              </Form.Group>
              <h5 className="mt-4 mb-3">
                Price
                <span
                  style={{
                    color: "red",
                    fontSize: "14px",
                    position: "relative",
                    top: "-3px",
                  }}
                >
                  *
                </span>
              </h5>
              <Form.Group controlId="apartmentetc" className="mt-3">
                <Form.Control
                  className="rounded-0 mt-2"
                  type="number"
                  onChange={(e) => handleFields(e, "price")}
                  placeholder="Price"
                  defaultValue={reqObj ? reqObj.price : ""}
                />
              </Form.Group>
              <h5 className="mt-4 mb-3">
                Link
                <span
                  style={{
                    color: "red",
                    fontSize: "14px",
                    position: "relative",
                    top: "-3px",
                  }}
                >
                  *
                </span>
              </h5>
              <Form.Group controlId="apartmentetc" className="mt-3">
                <Form.Control
                  className="rounded-0 mt-2"
                  type="text"
                  onChange={(e) => handleFields(e, "link")}
                  placeholder="Product Link"
                />
              </Form.Group>
              <Button
                className="rounded-0 py-2 px-4 mt-4"
                variant="primary"
                disabled={disabled}
                onClick={handleSubmit}
              >
                {initials}
              </Button> */}
            </Form>
          </Col>
        </Row>
      </Container>
      {(productModal) && <ProductsConfirm open={productModal} openModal={openModal} closeModal={closeModal} data={productObj} handleSubmit={updateProductData} />}
    </div>
    </LoaderComp>
  );
};

//export default UpdateProduct;
//export default withAlert()(withRouter(UpdateProduct));
export default withRouter(withAlert()(UpdateProduct));