import React, { useState, useEffect } from 'react';
import { Col, Container, Row } from "react-bootstrap";
import { withRouter } from "react-router-dom";
import { withAlert } from 'react-alert';
import LoaderComp from '../components/Loader';
import ApiRequest from '../api_service/api_requests';

function ViewWIshlist(props) {
    const [wishlist, setWishlist] = useState([]);
    const [displayLoader, setDisplayLoader] = useState(false);
    const [loaderMessage, setLoaderMessage] = useState('');

    const fetchWishlist = () => {
        if (props && props.match && props.match.params && props.match.params.wishlist_id) {
            setLoaderMessage("Fetching Wishlist");
            setDisplayLoader(true);

            ApiRequest.getWishlist(props.match.params.wishlist_id)
                .then((res) => {
                    if (res && res.statusCode && res.statusCode === 200 && res.data) {
                        setWishlist(res.data);

                    } else {
                        props.alert.error(res.message || "Failed to fetch Wishlist");
                    }
                    setLoaderMessage("");
                    setDisplayLoader(false);
                })
                .catch((err) => {
                    // console.log('Wishlist fetch api error, ', err);
                    setLoaderMessage("");
                    setDisplayLoader(false);
                })
        }
    }

    useEffect(() => {
        fetchWishlist();
    });

    let categories = [];
    if (wishlist && wishlist.wishListCategories && wishlist.wishListCategories.length) {
        wishlist.wishListCategories.forEach(object => {
            if (object.categoryName) {
                categories.push(object.categoryName);
            }
        });
    }

    let subCategories = [];
    if (wishlist && wishlist.wishListSubCategories && wishlist.wishListSubCategories.length) {
        wishlist.wishListSubCategories.forEach(object => {
            if (object.subCategoryName) {
                subCategories.push(object.subCategoryName);
            }
        });
    }

    let description = wishlist && wishlist.wishListDescription ? wishlist.wishListDescription : 'Not Available';

    return (
        <LoaderComp show={displayLoader} message={loaderMessage}>
            <div className="mb-5 register">
                <Container className="pt-4 pb-5">
                    <Row>
                        <Col md={10} lg={6} xl={5} className="mx-auto">
                            <h1 className=" pt-5 pb-0 mb-5 section-heading">View Wishlist</h1>

                            <Col md={10} className="mt-3 mt-md-0 m-b-1-rem pr-0 pl-0">
                                <h4>Categories</h4>
                                <div className='mb-4'>{categories.toString('. ')}</div>
                                <h4>Sub Categories</h4>
                                <div className='mb-4'>{subCategories.toString('. ')}</div>
                                <h4>Description</h4>
                                <div className='mb-4'>{description}</div>
                            </Col>
                        </Col>
                    </Row>
                </Container>
            </div>
        </LoaderComp>
    )
}

export default withAlert()(withRouter(ViewWIshlist));