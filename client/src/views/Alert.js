import React, { Component } from 'react';
import { Alert } from 'reactstrap';
import '../assets/css/loader.css';

import DoneIcon from '@material-ui/icons/CheckCircle';
import WarningIcon from '@material-ui/icons/Warning';

class AlertTemplate extends Component {
  constructor(props) {
    super(props);

    this.state = {
      visible: true,
      message: props.message,
      type: props.options.type,
      // dismiss: props.options.timeout,
      style: props.style
    };

    this.onDismiss = this.onDismiss.bind(this);
  }

  onDismiss() {
    this.setState({ visible: false });
  }

  render() {
    let style = Object.assign({}, this.state.style);
    let dangerStyle = Object.assign(style, {backgroundColor: '#ffc2c2'});

    return this.state.type === 'error' ? (
      <Alert color="danger" style={dangerStyle} isOpen={this.state.visible} toggle={this.onDismiss}>
        <WarningIcon className='fail-icon mr-3'/>{this.state.message}
      </Alert>
    ) : (
      <Alert color={this.state.type} style={this.state.style} isOpen={this.state.visible} toggle={this.onDismiss}>
        <DoneIcon className='success-icon mr-3'/>{this.state.message}
      </Alert>
    );
  }
}

export default AlertTemplate;
