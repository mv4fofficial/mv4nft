import UserIcon from "../components/icons/defaultUser";
import LoaderComp from "../components/Loader";

// static demo data
import StaticData from "../components/data/constants"
import react, { useState, useEffect } from "react";
import ApiRequest from "../api_service/api_requests";

import EditIcon from "@material-ui/icons/Edit"
import { Col, Container, Form, Row } from "react-bootstrap"
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import { makeStyles } from '@material-ui/core/styles';
import { getOrientation } from 'get-orientation/browser'
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import { useForm } from 'react-hook-form';
import moment from "moment";
import BuyerConfirm from "../components/profile/buyerConfirm";
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';

import ImageCrop from "../components/profile/imageCrop";
import { getStorage, ref, uploadString, getDownloadURL } from "firebase/storage";

import StripeDashboard from "../components/stripe/StripeDashboard";


const ORIENTATION_TO_ANGLE = {
    '3': 90,
    '6': 90,
    '8': -90,
}

const useStyles = makeStyles((theme) => ({
    formControl: {
      margin: "8px 0",
      minWidth: 120,
      width: "100%"
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
    root: {
      flexGrow: 1,
    },
    cropContainer: {
        position: 'relative',
        minWidth: "400px",
        width: '100%',
        height: 200,
        background: '#333',
        [theme.breakpoints.up('sm')]: {
          height: 400,
        },
    },
    cropButton: {
        flexShrink: 0,
        marginLeft: 16,
    },
    controls: {
        padding: 16,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'stretch',
        [theme.breakpoints.up('sm')]: {
            flexDirection: 'row',
            alignItems: 'center',
        },
    },
    sliderContainer: {
        display: 'flex',
        flex: '1',
        alignItems: 'center',
    },
    sliderLabel: {
        [theme.breakpoints.down('xs')]: {
            minWidth: 65,
        },
    },
    slider: {
        padding: '22px 0px',
        marginLeft: 16,
        [theme.breakpoints.up('sm')]: {
            flexDirection: 'row',
            alignItems: 'center',
            margin: '0 16px',
        },
    },
}));

const UserProfileUpdate = (props) => {

    const [croppedProfile, setCroppedProfile] = useState(null)
    const [croppedBanner, setCroppedBanner] = useState(null)
    const [initials, setInitials] = useState("Submit")
    const classes = useStyles()

    const [userStep, setUserStep] = useState(1)

    // const [sellerConfrim, setSellerConfirm] = useState(1)
    // const [showSellerConfrim, setShowSellerConfirm] = useState(false)

    const [userUpdateInfo, setUserUpdateInfo] = useState({
        firstName: "",
        lastName: "",
        phoneNumber: 0,
        countryId: 0,
        dateOfBirth: "", // (data.dateOfBirth) ? (new Date(data.dateOfBirth)).getTime() : (new Date(user.dateOfBirth)).getTime(),
        profilePicture: "",
        backgroundPicture: "",
        description: "",
        extention: ".webp",
        bio: "",
        stripeStatus: false
    })
    const {
        register,
        handleSubmit,
        formState: { errors }
    } = useForm();
    const onSubmit = async (data) => {
        setInitials("Loading...")
        // console.log(data)

        // let userUpdateData = {...data};
        let profileUrl = (croppedProfile != null) ? await uploadImageToFirebase(user.id, "profile", (croppedProfile).split("base64,")[1]) : user.profilePicture
        let bannerUrl = (croppedBanner != null) ? await uploadImageToFirebase(user.id, "banner", (croppedBanner).split("base64,")[1]) : user.backgroundPicture

        let userUpdateData = {
            firstName: (user.name).split(" ")[0],
            lastName: (user.name).split(" ")[1],
            phoneNumber: (data.phoneNumber) ? parseInt(data.phoneNumber) : parseInt(user.phoneNumber),
            countryId: (data.countryId) ? parseInt(data.countryId) : parseInt(user.country.id),
            dateOfBirth: (data.dateOfBirth) ? data.dateOfBirth : user.dateOfBirth, // (data.dateOfBirth) ? (new Date(data.dateOfBirth)).getTime() : (new Date(user.dateOfBirth)).getTime(),
            profilePicture: profileUrl,
            backgroundPicture: bannerUrl,
            description: (data.description) ? data.description : user.description,
            extention: ".webp",
            bio: (data.description) ? data.description : user.description
        }

        // console.log(userUpdateData)
        // setUserUpdateInfo({...userUpdateData})

        ApiRequest.updateUser(userUpdateData)
        .then(res => {
            // console.log(res)
            if (res && res.statusCode && res.statusCode === 200) {
                
                if (Object.keys(props.match.params).length > 0 && props.match.params.action === "upgrade") {
                    if (userUpdateData.profilePicture != "" && userUpdateData.profilePicture != null &&
                        userUpdateData.backgroundPicture != "" && userUpdateData.backgroundPicture != null && 
                        userUpdateData.description != "" && userUpdateData.description != null) {

                        userUpdateData["roleId"] = 2;
                        ApiRequest.upgraderUserRole(userUpdateData)
                        .then(res => {
                            if (res && res.statusCode && res.statusCode === 200) {
                                props.history.push('/dashboard/profile');
                            } else {
                                if (res && res.message) {
                                    // this.props.alert.error("Error fetching user details");
                                }
                            }
                        })
                        .catch((err) => {
                            // console.log('Error fetching user details, ', err);
                        })
                    }
                    
                }
                props.history.push('/dashboard/profile');
            } else {
                if (res && res.message) {
                    // this.props.alert.error("Error fetching user details");
                }
            }
        })
        .catch((err) => {
            // console.log('Error fetching user details, ', err);
        })
        
        // if (user.roles === 3) {
        //     if (userUpdateData.profilePicture != "" && userUpdateData.backgroundPicture != "" && (userUpdateData.description != "" || userUpdateData.bio != "")) {
        //         setShowSellerConfirm(true)
        //     }
        // } else {
        //     userUpdateData["roleId"] = 2;
        //     setSellerConfirm(3)
        // }
    }

    // const updateUser = (fieldsValue) => {

    // remove this feature
    // for buyer to wanna upgrade to seller confirmation
    // useEffect(() => {
    //     let readyToSave = false;

    //     // console.log(userUpdateInfo);

    //     if (user.roles === 3) {
    //         if (sellerConfrim == 3) {
    //             userUpdateInfo["roleId"] = 3;
    //             readyToSave = true
    //         } else
    //         if (sellerConfrim == 2) {
    //             userUpdateInfo["roleId"] = 3;
    //             readyToSave = true
    //         }
    //     } else {
    //         if (sellerConfrim != 1) {
    //             readyToSave = true
    //         }
    //     }
    //     if (readyToSave) {
    //         ApiRequest.updateUser(userUpdateInfo)
    //         .then(res => {
    //             if (res && res.statusCode && res.statusCode === 200) {
    //                 props.history.push('/dashboard/profile');
    //             } else {
    //                 if (res && res.message) {
    //                     // this.props.alert.error("Error fetching user details");
    //                 }
    //             }
    //         })
    //         .catch((err) => {
    //             // console.log('Error fetching user details, ', err);
    //         })
    //     }
    // }, [sellerConfrim]);

    const [showLoader, setShowLoader] = useState(true)
    const loaderShowHide = () => {
        setShowLoader(!showLoader);
    }

    const [user, setUser] = useState({
        firstName: "User",
        lastName: "mv4f",
        name: "User",
        emailId: "user@gmail.com",
        countryId: 1,
        phoneNumber: "912341234",
        dateOfBirth: "2000-01-01",
        backgroundPicture: "",
        profilePicture: "",
        description: "",
        wallet: null,
    })
    const [fetchuser, setFetchuser] = useState(false)
    const [countries, setCountries] = useState([])

    const [description, setDescription] = useState("")
    const [count, setCount] = useState(0)
    const discriptionCharCount = (e) => {
        if (e.target.value.length <= StaticData.userDescMaxText) {
            setCount(e.target.value.length)
        } else {
            e.target.value = (e.target.value).substring(0, description);
        }

        setDescription(e.target.value)
        handleFields(e, "description")
        handleFields(e, "bio")
    }

    useEffect(() => {
        // console.log({fetchuser});
        if (!fetchuser) {
            ApiRequest.viewUser()
            .then(res => {
                if (res && res.data && res.statusCode && res.statusCode === 200) {
                    // console.log(res.data)
                    // console.log(res.data.country.id)

                    let role;
                    switch (res.data.roles[0]) {
                        case "Admin":
                            role = 1;
                            break;
                        case "Seller":
                            role = 2;
                            break;
                        case "Buyer":
                            role = 3;
                            break;
                    
                        default:
                            break;
                    }
                    let user = {
                        firstName: (res.data.name).split(" ")[0], // res.data.firstName || res.data.name,
                        lastName: (res.data.name).split(" ")[1], // res.data.lastName || res.data.name,
                        name: res.data.name || "",
                        emailId: res.data.emailId || "",
                        countryId: res.data.country.id,
                        phoneNumber: res.data.phoneNumber || "",
                        dateOfBirth: res.data.dateOfBirth || "2000-01-01",
                        roles: role,
                        backgroundPicture: res.data.backgroundPicture || "",
                        profilePicture: res.data.profilePicture || "",
                        description: res.data.bio || res.data.description || "",
                        stripeStatus: res.data.stripeStatus,
                        wallet: res.data.wallet,
                    }
                    setUser(user);
                    setFetchuser(true)
                    setDescription(user.description)
                    setCount(user.description.length)
                    loaderShowHide()
                } else {
                    if (res && res.message) {
                        // this.props.alert.error("Error fetching user details");
                    }
                }
            })
            .catch((err) => {
                // console.log('Error fetching user details, ', err);
            })

            ApiRequest.getCountry()
            .then(res => {
                if (res && res.length) {
                  let countries = res.map(object => {
                    return { label: object.name, value: object.id, code: object.phone_code }
                  });
                  setCountries(countries);
                }
            })
            .catch((err) => {
                // console.log('Categories fetch api error, ', err);
            })
        }
    }, [fetchuser]);

    const [showCropModal, setShowCropModal] = useState(false)
    const [width, setWidth] = useState(1)
    const [height, setHeight] = useState(1)
    const [src, setSrc] = useState()
    const [title, setTitle] = useState("")
    const [ratioLabel, setRatioLabel] = useState("")

    const closeModal = () => {
        setShowCropModal(false)
    }

    const changeBanner = (e) => {
        setShowCropModal(true)
        setWidth(97)
        setHeight(18)
        setTitle("Banner Picture")
        setRatioLabel("7:1")
    }

    const changeProfile = async (e) => {
        setShowCropModal(true)
        setWidth(3)
        setHeight(3)
        setTitle("Profile Picture")
        setRatioLabel("1:1")
    }

    const handleFields = (e, field) => {
        let data = user;
        data[field] = e.target.value;
        setUser({...data})
    }

    // console.log(user)

    const backHistory = () => {
        props.history.goBack();
    }

    // const userFeedback = (result) => {
    //     if (result) {
    //         setSellerConfirm(3)
    //     } else {
    //         setSellerConfirm(2)
    //     }
    // }

    useEffect(() => {
        if (user.stripeStatus || user.wallet != null) { // check more available payment method
            setUserStep(2)
        }
    }, [user]);
    // console.log("userStep", userStep)

    return (
        <LoaderComp show={showLoader}>
            <div className="user-profile">
                <div className="my-4 container">
                    <div className="user-header">
                        <div className="user-banner">
                            {croppedBanner ? <img className="banner-image" src={croppedBanner} style={{ width: "100%", borderRadius: "15px" }}/> : (user.backgroundPicture && <img className="banner-image" src={user.backgroundPicture} style={{ width: "100%", borderRadius: "15px" }}/>) }
                            <div className="edit-banner">
                                <label onClick={changeBanner}>
                                    <Tooltip disableFocusListener title="Edit banner">
                                        <IconButton className="p-0">
                                            <EditIcon/>
                                        </IconButton>
                                    </Tooltip>
                                </label>
                            </div>

                            <div className="user-profile">
                                {croppedProfile ? <img src={croppedProfile} style={{ width: "100%", borderRadius: "50%" }} /> : (user.profilePicture) ? <img src={user.profilePicture} style={{ width: "100%", borderRadius: "50%" }} /> : <UserIcon />}
                                <div className="edit-profile">
                                    <label onClick={changeProfile}>
                                    <Tooltip disableFocusListener title="Edit profile">
                                        <IconButton className="p-0">
                                            <EditIcon/>
                                        </IconButton>
                                    </Tooltip>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div className="user-description">

                            <Row className="mt-5 pt-5">
                                <Col lg={12} className="">
                                    <Form className="py-4" onSubmit={handleSubmit(onSubmit)}>
                                        <Grid container spacing={3}>
                                            <Grid item xs={12}>
                                                <Form.Group controlId="Stripe">
                                                    <FormControl variant="outlined" className={classes.formControl}>
                                                        <div>
                                                            <StripeDashboard list={false}/>
                                                        </div>
                                                    </FormControl>
                                                </Form.Group>
                                            </Grid>

                                            <Grid item xs={12}>
                                                <Form.Group controlId="NFT" className="nav-buttons">
                                                    <FormControl variant="outlined" className={classes.formControl}>
                                                        <div className="d-flex justify-content-end">
                                                            <Button
                                                                type="button"
                                                                className=""
                                                                variant="contained"
                                                                color="primary"
                                                                size="small"
                                                                onClick={props.setupAccount}
                                                            >
                                                                Connect Wallet
                                                            </Button>
                                                        </div>
                                                    </FormControl>
                                                </Form.Group>
                                            </Grid>
                                            
                                            {(userStep == 2) &&
                                                <>
                                                    <Grid item xs={12} sm={6}>
                                                        <Form.Group controlId="firstName">
                                                            <FormControl variant="outlined" className={classes.formControl}>
                                                                <TextField
                                                                    label="First Name"
                                                                    type="text"
                                                                    name="firstName"
                                                                    variant="outlined"
                                                                    className="wp-100"
                                                                    value={user.firstName}
                                                                    required
                                                                    {...register('firstName', { required: true })}
                                                                    onChange={(e) => handleFields(e, "firstName")}
                                                                />
                                                                {errors.firstName && <p className="error-text">First Name is required.</p>}
                                                            </FormControl>
                                                        </Form.Group>
                                                    </Grid>
                                                    <Grid item xs={12} sm={6}>
                                                        <Form.Group controlId="lastName">
                                                            <FormControl variant="outlined" className={classes.formControl}>
                                                                <TextField
                                                                    label="Last Name"
                                                                    type="text"
                                                                    name="lastName"
                                                                    variant="outlined"
                                                                    className="wp-100"
                                                                    value={user.lastName}
                                                                    required
                                                                    {...register('lastName', { required: true })}
                                                                    onChange={(e) => handleFields(e, "lastName")}
                                                                />
                                                                {errors.lastName && <p className="error-text">Last Name is required.</p>}
                                                            </FormControl>
                                                        </Form.Group>
                                                    </Grid>
                                                    <Grid item xs={12} sm={6}>
                                                        <Form.Group controlId="Country">
                                                            <FormControl variant="outlined" className={classes.formControl}>
                                                                <InputLabel id="country-dropdown-label" required>Country</InputLabel>
                                                                <Select
                                                                    label="Country"
                                                                    labelId="country-dropdown-label"
                                                                    id="country-dropdown"
                                                                    value={user.countryId}
                                                                    // onChange={(e) => handleFields(e, "categoryId")}
                                                                    variant="outlined"
                                                                    {...register('countryId', { required: true })}
                                                                    onChange={(e) => handleFields(e, "countryId")}
                                                                >
                                                                    {countries.map((country, index) => (
                                                                        <MenuItem key={index} value={country.value}>{country.label}</MenuItem>
                                                                    ))}
                                                                </Select>
                                                                {errors.countryId && <p className="error-text">Country is required.</p>}
                                                            </FormControl>
                                                        </Form.Group>
                                                    </Grid>
                                                    <Grid item xs={12} sm={6}>
                                                        <Form.Group controlId="dateOfBirth">
                                                            <FormControl variant="outlined" className={classes.formControl}>
                                                                <TextField
                                                                    label="Date Of Birth"
                                                                    type="date"
                                                                    name="dateOfBirth"
                                                                    variant="outlined"
                                                                    className="wp-100"
                                                                    value={moment(user.dateOfBirth).format('YYYY-MM-DD')}
                                                                    required
                                                                    {...register('dateOfBirth', { required: true })}
                                                                    onChange={(e) => handleFields(e, "dateOfBirth")}
                                                                />
                                                                {errors.dateOfBirth && <p className="error-text">Date Of Birth is required.</p>}
                                                            </FormControl>
                                                        </Form.Group>
                                                    </Grid>
                                                    <Grid item xs={12} sm={6}>
                                                        <Form.Group controlId="phoneNumber">
                                                            <FormControl variant="outlined" className={classes.formControl}>
                                                                <TextField
                                                                    label="Phone Number"
                                                                    type="text"
                                                                    name="phoneNumber"
                                                                    variant="outlined"
                                                                    className="wp-100"
                                                                    value={user.phoneNumber}
                                                                    required
                                                                    {...register('phoneNumber', { required: true })}
                                                                    onChange={(e) => handleFields(e, "phoneNumber")}
                                                                />
                                                                {errors.phoneNumber && <p className="error-text">Phone Number is required.</p>}
                                                            </FormControl>
                                                        </Form.Group>
                                                    </Grid>
                                                    <Grid item xs={12} sm={6}>
                                                        <Form.Group controlId="Email">
                                                            <FormControl variant="outlined" className={classes.formControl}>
                                                                <TextField
                                                                    label="Email"
                                                                    type="text"
                                                                    name="email"
                                                                    variant="outlined"
                                                                    className="wp-100"
                                                                    value={user.emailId}
                                                                    required
                                                                    {...register('emailId', { required: true })}
                                                                    onChange={(e) => handleFields(e, "emailId")}
                                                                />
                                                                {errors.emailId && <p className="error-text">Email is required.</p>}
                                                            </FormControl>
                                                        </Form.Group>
                                                    </Grid>
                                                    <Grid item xs={12}>
                                                        <Form.Group controlId="Email">
                                                            <FormControl variant="outlined" className={classes.formControl}>
                                                                <div>
                                                                    <textarea defaultValue={description}
                                                                        {...register('description')}
                                                                        onChange={e => discriptionCharCount(e)} 
                                                                    ></textarea>
                                                                    <span className="char-count">{count}/{StaticData.userDescMaxText}</span>
                                                                </div>
                                                            </FormControl>
                                                        </Form.Group>
                                                    </Grid>
                                                </>
                                            }

                                            
                                            {(userStep == 2) &&
                                                <Grid item xs={12} className="text-center mt-5">
                                                    <Button
                                                        type="button"
                                                        className="mr-2"
                                                        variant="contained"
                                                        color="default"
                                                        onClick={backHistory}
                                                    >
                                                        Cancel
                                                    </Button>
                                                    <Button
                                                        type="submit"
                                                        className=""
                                                        variant="contained"
                                                        color="primary"
                                                    >
                                                        {initials}
                                                    </Button>
                                                </Grid>
                                            }
                                        </Grid>
                                    </Form>
                                </Col>
                            </Row>
                        </div>
                    </div>
                </div>
            </div>
            <ImageCrop classes={classes} title={title} ratioLabel={ratioLabel} image={src} open={showCropModal} setShowCropModal={setShowCropModal}
                handleClose={closeModal} width={width} height={height}
                setCroppedProfile={setCroppedProfile} setCroppedBanner={setCroppedBanner} />

            {/* <BuyerConfirm open={showSellerConfrim} handleClose={setShowSellerConfirm} setSellerConfirm={userFeedback}/> */}
        </LoaderComp>
    )
}

// function readFile(file) {
//     return new Promise((resolve) => {
//       const reader = new FileReader()
//       reader.addEventListener('load', () => resolve(reader.result), false)
//       reader.readAsDataURL(file)
//     })
// }

async function uploadImageToFirebase(id, type, image) {
    let returnData;
    const imageName = type+"-"+generateImageNames(30)+".jpg";
    const metadata = {
        contentType: `image/webp`,
    };
    const storage = getStorage();
    let enviroment = 'staging';
    if (process.env.REACT_APP_NODE_ENV === 'production') {
      enviroment = 'production';
    }
    const storageRef = ref(storage, `${enviroment}/${imageName}${"webp"}`);

    await uploadString(storageRef, image, 'base64', metadata).then((snapshot) => {
        // console.log('Uploaded a base64 string!');
    });
    await getDownloadURL(storageRef)
    .then((url) => {
        returnData = url;
    });

    return returnData;
}

function generateImageNames(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() *
        charactersLength));
    }
    return result;
}

export default UserProfileUpdate;