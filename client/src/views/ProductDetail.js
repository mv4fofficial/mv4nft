import React from "react";
import {Link} from "react-router-dom";
import {connect} from 'react-redux';
import {Col, Container, Row} from "react-bootstrap";
import Lightbox from "react-image-lightbox";
import {withRouter} from "react-router-dom";
import {withAlert} from 'react-alert';

// import { ReactComponent as MinusIcon } from "../assets/img/icons/minusIcon.svg";
// import { ReactComponent as PlusIcon } from "../assets/img/icons/plusIcon.svg";
import ProductsSlider from "../components/ProductsSlider";
import LoaderComp from '../components/Loader';
import {updateCartCount} from '../user';
import ApiRequest from '../api_service/api_requests';
import * as Constants from '../constant';
import "react-image-lightbox/style.css";
import '../assets/css/product-list.css';

import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Chip from '@material-ui/core/Chip';
import Grid from "@material-ui/core/Grid";
import Avatar from "@material-ui/core/Avatar";

import UserIcon from "../components/icons/defaultUser";
import SimpleImageSlider from "react-simple-image-slider";

import {Store} from "../store";

class ProductDetail extends React.PureComponent {
  constructor() {
    super();
    this.state = {
      isOpen: false,
      itemCount: 1,

      seller: '',
      sellerId: '',
      sellerEmail: '',
      productId: '',
      name: '',
      category: [],
      subCategory: [],
      tags: [],
      productPicture: '',
      showImageUrl: '',
      subProductImages: [],
      price: '',
      description: '',

      loaderMessage: '',
      displayLoader: false
    };
  }

  componentDidMount() {
    this.getProductDetails();
  }

  getProductDetails = () => {
    if (this.props && this.props.match && this.props.match.params && this.props.match.params.product_id) {
      this.setState({loaderMessage: "Fetching Product detail", displayLoader: true});
      ApiRequest.viewProduct(this.props.match.params.product_id)
        .then(res => {
          if (res && res.data && res.statusCode && res.statusCode === 200) {
            let data = res.data;
            // console.log(data);
            let productId = data.productId || '';
            let name = data.productName || '';
            let category = data.category || '';
            let subCategory = data.subCategory || '';
            let tags = data.tags || '';
            let productPicture = data.productPicture ? data.productPicture : 'https://cdn.shopify.com/s/files/1/0609/6152/1863/products/Colosseum.png';
            let price = data.price || 0;
            let seller = data.seller || '';
            let sellerId = data.sellerId || 0;
            let sellerEmail = data.sellerEmail || "";
            let description = data.description || "";
            let subProductImages = [{
              url: data.productPicture
            }];

            if (data.subProductPictures != null) {
              data.subProductPictures.map((image) => {
                subProductImages.push({
                  url: image.productPicture
                })
              })
            } else {
              subProductImages.push(
                {
                  url: "https://firebasestorage.googleapis.com/v0/b/mv4f-marketplace.appspot.com/o/staging%2F0nZ7YdXC4jpAyKjmNk8JT1bAcqcpMs.png?alt=media&token=543f75ec-0884-441f-afcf-34bbc25d737e",
                },
                {
                  url: "https://firebasestorage.googleapis.com/v0/b/mv4f-marketplace.appspot.com/o/staging%2F1Tmh38kAxJg8UKL3ggIGMM8xR6V2Io.jpeg?alt=media&token=31c9a364-bc3b-47ee-b1db-52e4c4bd7e51",
                }
              );
            }


            this.setState({productId, name, category, subCategory, tags, productPicture, price, seller, sellerId, sellerEmail, description, subProductImages});
          }
          this.setState({loaderMessage: "", displayLoader: false});
        })
        .catch((err) => {
          this.setState({loaderMessage: "", displayLoader: false});
          // console.log('Fetch Product api error, ', err);
        })

      // console.log("login user", Store.getState().userReducer.name)
    }
  }

  addToCart = (isCheckOut) => {
    if (this.state.productId && !isNaN(this.state.productId)) {
      ApiRequest.addToCart(Number(this.state.productId))
        .then(res => {
          if (isCheckOut) {
            this.getCartItems();
            this.props.history.push("/dashboard/checkout");
            return;
          }

          if (res && res.message) {
            if (res.statusCode && res.statusCode === 200) {
              this.props.alert.success(res.message);
              this.getCartItems();
            } else {
              this.props.alert.error(res.message);
            }
          } else {
            this.props.alert.error("Something went wrong");
          }
        })
        .catch((err) => {
          // console.log('Add cart api error, ', err);
        })
    } else {
      // console.log("Product ID not found");
      if (this.props && this.props.alert) {
        this.props.alert.error("Procut Id not found");
      }
    }
  }

  getCartItems = () => {
    ApiRequest.viewCartItems()
      .then(res => {
        if (res && res.data && res.statusCode && res.statusCode === 200) {
          this.props.dispatch(updateCartCount(res.data.length || 0));
        }
      })
      .catch((err) => {
        // console.log('View Cart api error, ', err);
      })
  }

  setOpen = () => this.setState({isOpen: true});
  setClose = () => this.setState({isOpen: false});
  incItemCount = () => this.setState({itemCount: this.state.itemCount + 1});
  decItemCount = () => {
    if (this.state.itemCount > 1) {
      this.setState({itemCount: this.state.itemCount - 1});
    }
  }

  showPhoto = (idx) => {
    this.setState({isOpen: true, showImageUrl: this.state.subProductImages[idx].url});
  }

  render() {
    const images = [this.state.productPicture];
    // const [photoIndex, setPhotoIndex] = useState(0);

    return (
      <LoaderComp show={this.state.displayLoader} message={this.state.loaderMessage}>
        <Container className="product-detail">
          <Row className="pt-3">
            <Col lg={8} className="my-4 pr-lg-4  ">
              <div
                className="image-upload-control light-box-contianer bg-light p-2 product-img text-center"
              // onClick={this.setOpen}
              >
                {/* <div className="expend-icon position-absolute">
                  <svg
                    stroke="currentColor"
                    fill="currentColor"
                    strokeWidth="0"
                    viewBox="0 0 24 24"
                    height="1em"
                    width="1em"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path fill="none" d="M0 0h24v24H0V0z"></path>
                    <path d="M15 3l2.3 2.3-2.89 2.87 1.42 1.42L18.7 6.7 21 9V3h-6zM3 9l2.3-2.3 2.87 2.89 1.42-1.42L6.7 5.3 9 3H3v6zm6 12l-2.3-2.3 2.89-2.87-1.42-1.42L5.3 17.3 3 15v6h6zm12-6l-2.3 2.3-2.87-2.89-1.42 1.42 2.89 2.87L15 21h6v-6z"></path>
                  </svg>
                </div> */}

                {this.state.subProductImages.length > 0 ? <SimpleImageSlider
                  width={"100%"}
                  height={500}
                  images={this.state.subProductImages}
                  showBullets={(this.state.subProductImages.length > 1) ? true : false}
                  showNavs={(this.state.subProductImages.length > 1) ? true : false}
                  startIndex={0}
                  onClick={(idx) => this.showPhoto(idx)}
                />
                  :
                  <div className="image-slide-empty-container"></div>
                }
                {/* <img className="wp-100 product-img" src={this.state.productPicture} alt="" /> */}
              </div>

              {this.state.isOpen && (<Lightbox mainSrc={this.state.showImageUrl} onCloseRequest={this.setClose} />)}
            </Col>

            <Col lg={4} className="mt-lg-4 mb-lg-4 mt-2 mb-4">
              <div className="product-info">
                <div className="d-flex mt-2 mb-4 border-bottom pb-2 d-flex align-items-center">
                  <Link to={`/dashboard/seller-profile/${this.state.sellerId}`} className="product-card__body--seller">
                    <Avatar className="mr-2" alt="Remy Sharp"><UserIcon /></Avatar>
                  </Link>
                  <Link to={`/dashboard/seller-profile/${this.state.sellerId}`} className="product-card__body--seller">
                    {this.state.seller || 'NA'}
                  </Link>
                </div>

                <Grid container spacing={3} className="mt-2">
                  <Grid item xs={12} className="mb-3">
                    <h2 className="mb-2 product-detial-label">{this.state.name}</h2>
                  </Grid>
                </Grid>

                <Grid container spacing={3} className="mt-2">
                  <Grid item xs={12} sm={6}>
                    <p className="mb-2 product-detial-label">Price</p>
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <div className="font-weight-bold font-20 mb-1">${this.state.price} GSD</div>
                  </Grid>

                  {/* <p className="product-detial-label mb-0 mt-2"> Price</p>
                <div className="font-weight-bold font-20 mb-1">${this.state.price} GSD</div> */}
                  {/* <p className="product-detial-label mb-2 mt-3"> Quntity</p> */}

                  <Grid item xs={12} sm={6}>
                    <p className="mb-2 product-detial-label">Categories</p>
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    {this.state.category && (
                      this.state.category.map(category => {
                        if (category && category.categoryId && category.categoryName) {
                          return <Chip key={category.categoryId} id={category.categoryId} label={category.categoryName} color="primary" size="small" />;
                          // <Button key={category.categoryId} id={category.categoryId} className="tag px-3">{category.categoryName}</Button>
                        }
                        return null;
                      })
                    )}
                  </Grid>

                  {/* <div className="style mt-2 pt-1">
                  <p className="mb-2 product-detial-label ">Categories</p>
                  {this.state.category && (
                    this.state.category.map(category => {
                      if (category && category.categoryId && category.categoryName) {
                        return <Chip key={category.categoryId} id={category.categoryId} label={category.categoryName} color="primary" size="small"/>;
                        // <Button key={category.categoryId} id={category.categoryId} className="tag px-3">{category.categoryName}</Button>
                      }
                      return null;
                    })
                  )}
                </div> */}

                  <Grid item xs={12} sm={6}>
                    <p className="mb-2 product-detial-label">Sub Categories</p>
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    {this.state.subCategory && (
                      this.state.subCategory.map(subCategory => {
                        if (subCategory && subCategory.subCategoryId && subCategory.subCategoryName) {
                          return <Chip key={subCategory.subCategoryId} id={subCategory.subCategoryId} label={subCategory.subCategoryName} color="primary" size="small" />;
                          // <Button key={subCategory.subCategoryId} id={subCategory.subCategoryId} className="tag px-3">{subCategory.subCategoryName}</Button>
                        }
                        return null;
                      })
                    )}
                  </Grid>

                  {/* <div className="style mt-2 pt-1">
                  <p className="mb-2 product-detial-label ">Sub Categories</p>
                  {this.state.subCategory && (
                    this.state.subCategory.map(subCategory => {
                      if (subCategory && subCategory.subCategoryId && subCategory.subCategoryName) {
                        return <Chip key={subCategory.subCategoryId} id={subCategory.subCategoryId} label={subCategory.subCategoryName} color="primary" size="small"/>;
                        // <Button key={subCategory.subCategoryId} id={subCategory.subCategoryId} className="tag px-3">{subCategory.subCategoryName}</Button>
                      }
                      return null;
                    })
                  )}
                </div> */}

                  <Grid item xs={12} sm={6}>
                    <p className="mb-2 product-detial-label">Tags</p>
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    {this.state.tags && (
                      this.state.tags.map(tag => {
                        if (tag && tag.tagId && tag.TagName) {
                          return <Chip key={tag.tagId} id={tag.tagId} label={tag.TagName} color="primary" size="small" />;
                          // <Button key={tag.tagId} id={tag.tagId} className="tag px-3">{tag.TagName}</Button>
                        }
                        return null;
                      })
                    )}
                  </Grid>

                  {/* <div className="style mt-2 pt-1">
                  <p className="mb-2 product-detial-label ">Tags</p>
                  {this.state.tags && (
                    this.state.tags.map(tag => {
                      if (tag && tag.tagId && tag.TagName) {
                        return <Chip key={tag.tagId} id={tag.tagId} label={tag.TagName} color="primary" size="small"/>;
                        // <Button key={tag.tagId} id={tag.tagId} className="tag px-3">{tag.TagName}</Button>
                      }
                      return null;
                    })
                  )}
                </div> */}

                  {/* <p className="product-detial-label mb-0 mt-3"> Quntity</p>

                <div className="cart-items-details d-flex align-items-center mt-2 mb-3">
                  <button
                    className="plus rounded-0"
                    onClick={this.decItemCount}>
                    <MinusIcon />
                  </button>
                  <div className="mint-amount rounded-0 mx-2 text-center d-flex justify-content-center align-items-center">
                    <span>{this.state.itemCount}</span>
                  </div>
                  <button className="plus rounded-0" onClick={this.incItemCount}>
                    <PlusIcon />
                  </button>
                </div> */}
                  {/* </div> */}

                  {/* <div className="product-btns mt-3 pt-1"> */}
                  {/* <Stack direction="row" spacing={2}> */}


                  {/* </Stack> */}

                  {Store.getState().userReducer.name != this.state.sellerEmail && <>
                    <Grid item xs={12} sm={6} className="mt-5">
                      <Button variant="contained" className="wp-100"
                        color="primary"
                        size="small"
                        onClick={event => {
                          event.preventDefault();
                          this.addToCart(false);
                        }}
                      >
                        Add to cart
                      </Button>
                    </Grid>
                    <Grid item xs={12} sm={6} className="mt-5">
                      <Button variant="contained" className="wp-100"
                        color="primary"
                        size="small"
                        onClick={event => {
                          event.preventDefault();
                          this.addToCart(true);
                        }}
                      >
                        Buy now
                      </Button>
                    </Grid>
                  </>}
                  <Grid item xs={12} sm={12} className={(Store.getState().userReducer.name != this.state.sellerEmail) ? "mt-2" : "mt-5"}>
                    <Button variant="contained" className="wp-100"
                      color="default"
                      size="small"
                      onClick={(event) => this.props.history.push("/dashboard/products")}
                    >
                      Go to Shop
                    </Button>
                  </Grid>
                </Grid>
                {/* <Button className="w-100 rounded-0" variant="outline-primary" onClick={event => {
                  event.preventDefault();
                  this.addToCart(false);
                }}>Add to cart</Button>
                <Button className="w-100 rounded-0 mt-2" variant="primary " onClick={event => {
                  event.preventDefault();
                  this.addToCart(true);
                }}>Buy now</Button> */}
              </div>

              {/* <a href="#ff" className="share-btn font-20 text-primary mt-3 d-inline-block">
                <svg
                  stroke="currentColor"
                  fill="currentColor"
                  strokeWidth="0"
                  viewBox="0 0 24 24"
                  height="1em"
                  width="1em"
                  xmlns="http://www.w3.org/2000/svg">
                  <g>
                    <path fill="none" d="M0 0h24v24H0z"></path>
                    <path d="M4 19h16v-5h2v6a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1v-6h2v5zm8-9H9a5.992 5.992 0 0 0-4.854 2.473A8.003 8.003 0 0 1 12 6V2l8 6-8 6v-4z"></path>
                  </g>
                </svg>
              </a> */}
            </Col>
          </Row>
          <Row className="col-12">
            <Typography variant="body1" gutterBottom>
              {this.state.description}
            </Typography>
          </Row>
          <h2 className="section-heading mt-4">May you also Like</h2>
          <ProductsSlider categories={this.state.category} subCategories={this.state.subCategory} />
          <div className="mb-5"></div>
        </Container>
      </LoaderComp>
    )
  }
};

const mapStateToProps = (state) => {
  return {
    user: state.userReducer
  }
}

export default withRouter(withAlert()(connect(mapStateToProps)(ProductDetail)));
