import React from "react";
import { Button, Col, Container, Form, Row } from "react-bootstrap";

const ContactUs = () => {
  return (
    <div className="contat-us d-flex justify-content-center align-items-center">
      <Container>
        <Row className=" h-100">
          <Col lg={7} className="mx-auto">
            <h2
              data-aos="zoom-in-up"
              data-aos-duration="900"
              className="section-heading"
            >
              Contact us
            </h2>
            <Form
              //   className="p-5"
              data-aos="zoom-in-up"
              data-aos-duration="900"
              data-aos-delay="200"
            >
              <Row>
                <Col md={6}>
                  <Form.Group controlId="name">
                    <Form.Label>Enter Name</Form.Label>
                    <Form.Control
                      className="rounded-0"
                      type="text"
                      placeholder="Your name"
                    />
                  </Form.Group>
                </Col>
                <Col md={6} className="mt-3 mt-md-0">
                  <Form.Group controlId="email">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control
                      className="rounded-0"
                      type="email"
                      placeholder="Enter email"
                    />
                  </Form.Group>
                </Col>
                <Col md={12} className="mt-3">
                  <Form.Group controlId="number">
                    <Form.Label>Phone Number</Form.Label>
                    <Form.Control
                      className="rounded-0"
                      type="text"
                      placeholder="Phone number"
                    />
                  </Form.Group>
                </Col>
                <Col md={12} className="mt-3">
                  <Form.Group controlId="comment">
                    <Form.Label>Comment</Form.Label>
                    <Form.Control
                      className="rounded-0"
                      as="textarea"
                      rows={3}
                    />
                  </Form.Group>
                </Col>
                <Col md={12} className="mt-4">
                  <Button
                    className="rounded-0 px-4"
                    variant="dark"
                    type="submit"
                  >
                    Send
                  </Button>
                </Col>
              </Row>
            </Form>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default ContactUs;
