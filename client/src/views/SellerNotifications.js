import React, {useState, useEffect} from "react";
import "./styles.css";
import {FaEdit} from "react-icons/fa";
import {AiFillDelete} from "react-icons/ai";
import "bootstrap/dist/css/bootstrap.css";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import ApiRequest from "../api_service/api_requests";
import { Link } from "react-router-dom";
import { Col, Container, Row } from "react-bootstrap";

import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import MUILink from '@material-ui/core/Link';

const SellerNotifications = () => {
  const [AllProducts, setAllProducts] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    ApiRequest.getSellerNotifications().then((res) => {
      setAllProducts(res.data);
      setLoading(false);
    });
  }, []);

  const handleDelete = (e) => {
    // console.log("handle Delete click", e);
    if (window.confirm("You want to Delete Product")) {
      ApiRequest.deleteProduct(e).then((res) => {
        if (res.statusCode === 200) {
          window.location.reload();
        } else {
          alert("Something went Wrong!");
        }
      });
    }
  };

  const columns = [
    {
      text: "#",
      formatter: (cell, row, ind) => {
        return <div class="serial">{ind + 1}</div>
      },
      headerStyle: (colum, colIndex) => {
        return {width: '50px', backgroundColor: "#131c20", color: "white"};
      }
    },
    {
      dataField: "requestedByUser",
      text: "User",
      formatter: (cell) => {
        return <div>{cell}</div>
      }
    },
    {
      dataField: "category",
      text: "Category",
      formatter: (cell) => {
        return <div>{cell[0].categoryName}</div>
      }
    },
    {
      dataField: "subCategory",
      text: "Category",
      formatter: (cell) => {
        return <div>{cell[0].subCategoryName}</div>
      }
    },
    {
      dataField: "description",
      text: "Product Details",
      // sort: true,
      formatter: (cell, row) => (
        <span className="text">{cell}</span> // for styling see styles.css in current folder
      ),
    },
  ];

  const renderProducts = () => {
    if (!loading) {
      if (AllProducts.length > 0) {
        return (
          <BootstrapTable
            bootstrap4
            keyField="productId"
            data={AllProducts}
            columns={columns}
            bordered={false}
            pagination={paginationFactory({sizePerPage: 10})}
          ></BootstrapTable>
        );
      } else {
        return (
          <h4 style={{textAlign: "center", padding: "20px"}}>No Data!</h4>
        );
      }
    } else {
      return (
        <h4 style={{textAlign: "center", padding: "20px"}}>Loading...</h4>
      );
    }
  };

  return (
    <div className="checkout">
      <div className="bg-light py-5 text-center">
        <h1 className="section-heading">Seller Notifications</h1>
      </div>
      <Container className="my-4">
        <Row>
          <Col lg={12} className="">
            <Breadcrumbs maxItems={2} aria-label="breadcrumb">
              <MUILink underline="hover" color="inherit" href="/dashboard/home">
                Home
              </MUILink>
              <span color="inherit">
                Seller Notifications
              </span>
            </Breadcrumbs>
          </Col>
        </Row>
      </Container>
      {renderProducts()}
    </div>
  );
};

export default SellerNotifications;
