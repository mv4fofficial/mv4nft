import React from 'react';
import { connect } from 'react-redux';
import { withAlert } from 'react-alert';
import { Link, withRouter } from "react-router-dom";
import { Button, Col, Container, Row } from "react-bootstrap";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faImage } from '@fortawesome/free-solid-svg-icons';
// import moment from 'moment';
import LoaderComp from '../components/Loader';
import ApiRequest from '../api_service/api_requests';
import '../assets/css/profile.css';
import * as Constants from '../constant';

import userProfileImage from "../assets/img/user.png"

import ImageList from '@material-ui/core/ImageList';
import ImageListItem from '@material-ui/core/ImageListItem';
// import ImageList from '@mui/material/ImageList';
// import ImageListItem from '@mui/material/ImageListItem';

const Product = props => {
    let productId = 1;
    let name = '';
    let price = 0;
    let seller = '';
    let sellerId = 0;
    let imgUrl = 'https://cdn.shopify.com/s/files/1/0609/6152/1863/products/DiRenJie_300x.png';

    if (props && props.data) {
        let data = props.data;

        productId = data.productId || productId;
        name = data.productName || name;
        price = Math.round(Number(data.price || price) * 100) / 100;
        seller = data.seller || seller;
        sellerId = data.sellerId || sellerId;
        imgUrl = data.productPicture ? data.productPicture : imgUrl;
    }
    return (
        <Row className="border-bottom py-3">
            <Col sm={6}>
                <div className="cart_product d-flex align-items-center">
                    <div className="p-2 bg-light">
                        <Link to={`/dashboard/product-detail/${productId}`}>
                            <img src={imgUrl} alt={name} />
                        </Link>
                    </div>
                    <div className="pl-3 pl-md-4">
                        <Link to={`/dashboard/product-detail/${productId}`}>
                            <h6 className="mb-2 pb-1">{name}</h6>
                        </Link>
                        <p className="mb-0">${price}</p>
                        {/* <p className="mb-1">Style: Tang Dynasty Heros</p> */}

                        <Link to={`/dashboard/seller-profile/${sellerId}`} className="product-card__body--seller">
                            {seller || 'NA'}
                        </Link>
                    </div>
                </div>
            </Col>
            <Col sm={3}>
                <div className="cart-items-details d-flex align-items-center mt-sm-0 mt-3 mb-3">
                    <div className="mint-amount rounded-0 mx-2 text-center d-flex justify-content-center align-items-center">
                        <span>1</span>
                    </div>
                </div>
            </Col>
            <Col sm={3}>
                <div className="cart-item__price">${price}</div>
            </Col>
        </Row>
    )
}

class Profile extends React.PureComponent {
    constructor() {
        super()
        this.state = {
            name: "",
            emailId: "",
            address: '',
            landmark: '',
            city: '',
            postal: '',
            phoneNumber: 9876543120,
            country: "India",
            dateOfBirth: "2022-03-15T17:35:13.004Z",
            profilePhoto: "https://cdn.shopify.com/s/files/1/0609/6152/1863/products/DiRenJie_300x.png",

            orders: [],

            loaderMessage: '',
            displayLoader: false
        }
    }
    componentDidMount() {
        this.getUserProfile();

        if (this.props && this.props.user && this.props.user.roles && this.props.user.roles[0] && this.props.user.roles[0] === "Buyer") {
            this.getUserOrder();
        }
    }

    getUserProfile = () => {
        ApiRequest.viewUser()
            .then(res => {
                if (res && res.data && res.statusCode && res.statusCode === 200) {
                    this.setState(res.data);
                } else {
                    if (res && res.message) {
                        this.props.alert.error("Error fetching user details");
                    }
                }
            })
            .catch((err) => {
                // console.log('Error fetching user details, ', err);
            })
    }

    getUserOrder = () => {
        this.setState({ loaderMessage: "Fetching user details", displayLoader: true });

        let payload = {
            categoryId: [],
            subCategoryId: []
        };

        ApiRequest.getUserOrder(payload)
            .then(res => {
                if (res && res.data && res.statusCode && res.statusCode === 200) {
                    this.setState({ orders: res.data });
                } else {
                    if (res && res.message) {
                        this.props.alert.error("Error fetching user order details");
                    }
                }
                this.setState({ loaderMessage: "", displayLoader: false });
            })
            .catch((err) => {
                this.setState({ loaderMessage: "", displayLoader: false });
                // console.log('Error fetching user order details, ', err);
            })
    }

    render() {
        let isBuyer = false;
        let isSeller = false;
        let canBeSeller = true;
        let props = this.props;

        const itemData = [
            {
              img: 'https://images.unsplash.com/photo-1551963831-b3b1ca40c98e',
              title: 'Breakfast',
            },
            {
              img: 'https://images.unsplash.com/photo-1551782450-a2132b4ba21d',
              title: 'Burger',
            },
            {
              img: 'https://images.unsplash.com/photo-1522770179533-24471fcdba45',
              title: 'Camera',
            },
            {
              img: 'https://images.unsplash.com/photo-1444418776041-9c7e33cc5a9c',
              title: 'Coffee',
            },
            {
              img: 'https://images.unsplash.com/photo-1533827432537-70133748f5c8',
              title: 'Hats',
            },
            {
              img: 'https://images.unsplash.com/photo-1558642452-9d2a7deb7f62',
              title: 'Honey',
            },
            {
              img: 'https://images.unsplash.com/photo-1516802273409-68526ee1bdd6',
              title: 'Basketball',
            },
            {
              img: 'https://images.unsplash.com/photo-1518756131217-31eb79b20e8f',
              title: 'Fern',
            },
            {
              img: 'https://images.unsplash.com/photo-1597645587822-e99fa5d45d25',
              title: 'Mushrooms',
            },
            {
              img: 'https://images.unsplash.com/photo-1567306301408-9b74779a11af',
              title: 'Tomato basil',
            },
            {
              img: 'https://images.unsplash.com/photo-1471357674240-e1a485acb3e1',
              title: 'Sea star',
            },
            {
              img: 'https://images.unsplash.com/photo-1589118949245-7d38baf380d6',
              title: 'Bike',
            },
        ];

        if (props && props.user && props.user.roles) {
            if (props.user.roles[0] && props.user.roles[0] === "Buyer") {
                isBuyer = true;
            }
            if (props.user.roles[1] && props.user.roles[1] === "Buyer") {
                isBuyer = true;
            }
            if (props.user.roles[2] && props.user.roles[2] === "Buyer") {
                isBuyer = true;
            }

            if (props.user.roles[0] && props.user.roles[0] === "Seller") {
                isSeller = true;
            }
            if (props.user.roles[1] && props.user.roles[1] === "Seller") {
                isSeller = true;
            }
            if (props.user.roles[2] && props.user.roles[2] === "Seller") {
                isSeller = true;
            }

            if (props.user.roles[0] && (isSeller || props.user.roles[0] === "Admin")) {
                canBeSeller = false;
            }
            if (props.user.roles[1] && (isSeller || props.user.roles[1] === "Admin")) {
                canBeSeller = false;
            }
            if (props.user.roles[2] && (isSeller || props.user.roles[0] === "Admin")) {
                canBeSeller = false;
            }
        }


        return (
            <LoaderComp show={this.state.displayLoader} message={this.state.loaderMessage}>
                <div className="profile">
                    <Container className="">
                        <Row>
                            <Col sm={12} md={4} lg={4} xl={4} className="mx-auto">
                                {canBeSeller ? <Button className='float-right mt-3 rounded-0 px-4 font-weight-bold text-capitalize mt-1 mb-1 btn btn-primary btn-lg'
                                    as={Link}
                                    to="/dashboard/become-seller">
                                    Become a Seller
                                </Button> : null}
                                {isBuyer ? <Button className='float-right mt-3 rounded-0 px-4 font-weight-bold text-capitalize mt-1 mb-1 btn btn-primary btn-lg mr-lg-2'
                                    title='Gallery'
                                    onClick={event => {
                                        event.preventDefault();
                                        this.props.history.push("/dashboard/gallery");
                                    }}>
                                    <FontAwesomeIcon icon={faImage} className="fa-lg" />
                                </Button> : null}
                                {!(canBeSeller || isBuyer) ? <br></br> : null}
                            </Col>
                            <Col sm={12} md={4} lg={4} xl={4} className="mx-auto">
                                <div className='profile-card'>
                                    <img src={userProfileImage} alt="user-profile" />
                                    <span className='name'>{this.state.name}</span>
                                    <span className='email-id'>{this.state.emailId}</span>
                                    <span className='phone-number'>{this.state.phoneNumber}</span>
                                    <span className='country'>{this.state.country}</span>
                                </div>
                            </Col>
                            {(isSeller) &&
                            <Col sm={12} md={8} lg={8} xl={8} className="mx-auto">
                                <ImageList sx={{ width: 500, height: 450 }} cols={3} rowHeight={164}>
                                    {itemData.map((item) => (
                                        <ImageListItem key={item.img}>
                                        <img
                                            src={`${item.img}?w=164&h=164&fit=crop&auto=format`}
                                            srcSet={`${item.img}?w=164&h=164&fit=crop&auto=format&dpr=2 2x`}
                                            alt={item.title}
                                            loading="lazy"
                                        />
                                        </ImageListItem>
                                    ))}
                                </ImageList>
                            </Col>}
                        </Row>

                        {isBuyer ? <div className="cart">
                            <Container>
                                <div className="d-flex justify-content-between align-items-center py-4 flex-wrap">
                                    <h1 className="">Order History</h1>
                                    <Link to="/dashboard/products" className="font-weight-bold font-18">
                                        <u>Continue shopping</u>
                                    </Link>
                                </div>

                                {(this.state.orders && this.state.orders.length > 0 ?
                                    <Row className="justify-content-between header-row border-bottom py-3">
                                        <Col xs={6}>Product</Col>
                                        <Col xs={3} className="d-none d-sm-block">
                                            Quantity
                                        </Col>
                                        <Col className="text-right" xs={3}>
                                            Total
                                        </Col>
                                    </Row> :
                                    <Row className="justify-content-between header-row border-bottom py-3">
                                        <Col md={12} lg={12} xl={12} className="no-products-text">
                                            <span>No history found</span>
                                        </Col>
                                    </Row>
                                )}

                                {this.state.orders.map((order, index) => <Product key={index} data={order} />)}
                            </Container>
                        </div> : <br></br>}
                    </Container>
                </div >
            </LoaderComp >
        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.userReducer
    }
}

export default withAlert()(withRouter(connect(mapStateToProps)(Profile)));