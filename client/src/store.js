import {createStore, applyMiddleware, compose, combineReducers} from 'redux';
import {routerReducer} from 'react-router-redux';
import userReducer from './user';
import thunkMiddleware from 'redux-thunk';
import {createLogger} from 'redux-logger';

const appReducer = combineReducers({userReducer, routing: routerReducer});

var middlewares = [];
middlewares.push(thunkMiddleware);

if (process.env.NODE_ENV !== "production") {
  const loggerMiddleware = createLogger()
  middlewares.push(loggerMiddleware)
}

const enhancer = compose(
  applyMiddleware(...middlewares));

const rootReducer = (state, action) => {
  return appReducer(state, action)
};

const persistedState = localStorage.getItem('$@meta@$') ? JSON.parse(localStorage.getItem('$@meta@$')) : {}

// let initialState = deepExtend(createStore(rootReducer, enhancer).getState(), persistedState);
let initialState = persistedState;
export const Store = createStore(rootReducer, initialState, enhancer);