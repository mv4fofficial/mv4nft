// From https://bitbucket.org/atlassian/atlaskit-mk-2/raw/4ad0e56649c3e6c973e226b7efaeb28cb240ccb0/packages/core/select/src/data/countries.js
export const COUNTRIES = [
    {
        "Id": 1,
        "Name": "Afghanistan",
        "PhoneCode": "+93",
        "CountryCode": "AF",
        "IsRestricted": false
    },
    {
        "Id": 2,
        "Name": "Aland Islands",
        "PhoneCode": "+358",
        "CountryCode": "AX",
        "IsRestricted": false
    },
    {
        "Id": 3,
        "Name": "Albania",
        "PhoneCode": "+355",
        "CountryCode": "AL",
        "IsRestricted": false
    },
    {
        "Id": 4,
        "Name": "Algeria",
        "PhoneCode": "+213",
        "CountryCode": "DZ",
        "IsRestricted": false
    },
    {
        "Id": 5,
        "Name": "AmericanSamoa",
        "PhoneCode": "+1684",
        "CountryCode": "AS",
        "IsRestricted": false
    },
    {
        "Id": 6,
        "Name": "Andorra",
        "PhoneCode": "+376",
        "CountryCode": "AD",
        "IsRestricted": false
    },
    {
        "Id": 7,
        "Name": "Angola",
        "PhoneCode": "+244",
        "CountryCode": "AO",
        "IsRestricted": false
    },
    {
        "Id": 8,
        "Name": "Anguilla",
        "PhoneCode": "+1264",
        "CountryCode": "AI",
        "IsRestricted": false
    },
    {
        "Id": 9,
        "Name": "Antarctica",
        "PhoneCode": "+672",
        "CountryCode": "AQ",
        "IsRestricted": false
    },
    {
        "Id": 10,
        "Name": "Antigua and Barbuda",
        "PhoneCode": "+1268",
        "CountryCode": "AG",
        "IsRestricted": false
    },
    {
        "Id": 11,
        "Name": "Argentina",
        "PhoneCode": "+54",
        "CountryCode": "AR",
        "IsRestricted": false
    },
    {
        "Id": 12,
        "Name": "Armenia",
        "PhoneCode": "+374",
        "CountryCode": "AM",
        "IsRestricted": false
    },
    {
        "Id": 13,
        "Name": "Aruba",
        "PhoneCode": "+297",
        "CountryCode": "AW",
        "IsRestricted": false
    },
    {
        "Id": 14,
        "Name": "Australia",
        "PhoneCode": "+61",
        "CountryCode": "AU",
        "IsRestricted": false
    },
    {
        "Id": 15,
        "Name": "Austria",
        "PhoneCode": "+43",
        "CountryCode": "AT",
        "IsRestricted": false
    },
    {
        "Id": 16,
        "Name": "Azerbaijan",
        "PhoneCode": "+994",
        "CountryCode": "AZ",
        "IsRestricted": false
    },
    {
        "Id": 17,
        "Name": "Bahamas",
        "PhoneCode": "+1242",
        "CountryCode": "BS",
        "IsRestricted": false
    },
    {
        "Id": 18,
        "Name": "Bahrain",
        "PhoneCode": "+973",
        "CountryCode": "BH",
        "IsRestricted": false
    },
    {
        "Id": 19,
        "Name": "Bangladesh",
        "PhoneCode": "+880",
        "CountryCode": "BD",
        "IsRestricted": false
    },
    {
        "Id": 20,
        "Name": "Barbados",
        "PhoneCode": "+1246",
        "CountryCode": "BB",
        "IsRestricted": false
    },
    {
        "Id": 21,
        "Name": "Belarus",
        "PhoneCode": "+375",
        "CountryCode": "BY",
        "IsRestricted": false
    },
    {
        "Id": 22,
        "Name": "Belgium",
        "PhoneCode": "+32",
        "CountryCode": "BE",
        "IsRestricted": false
    },
    {
        "Id": 23,
        "Name": "Belize",
        "PhoneCode": "+501",
        "CountryCode": "BZ",
        "IsRestricted": false
    },
    {
        "Id": 24,
        "Name": "Benin",
        "PhoneCode": "+229",
        "CountryCode": "BJ",
        "IsRestricted": false
    },
    {
        "Id": 25,
        "Name": "Bermuda",
        "PhoneCode": "+1441",
        "CountryCode": "BM",
        "IsRestricted": false
    },
    {
        "Id": 26,
        "Name": "Bhutan",
        "PhoneCode": "+975",
        "CountryCode": "BT",
        "IsRestricted": false
    },
    {
        "Id": 27,
        "Name": "Bolivia, Plurinational State of",
        "PhoneCode": "+591",
        "CountryCode": "BO",
        "IsRestricted": false
    },
    {
        "Id": 28,
        "Name": "Bosnia and Herzegovina",
        "PhoneCode": "+387",
        "CountryCode": "BA",
        "IsRestricted": false
    },
    {
        "Id": 29,
        "Name": "Botswana",
        "PhoneCode": "+267",
        "CountryCode": "BW",
        "IsRestricted": false
    },
    {
        "Id": 30,
        "Name": "Brazil",
        "PhoneCode": "+55",
        "CountryCode": "BR",
        "IsRestricted": false
    },
    {
        "Id": 31,
        "Name": "British Indian Ocean Territory",
        "PhoneCode": "+246",
        "CountryCode": "IO",
        "IsRestricted": false
    },
    {
        "Id": 32,
        "Name": "Brunei Darussalam",
        "PhoneCode": "+673",
        "CountryCode": "BN",
        "IsRestricted": false
    },
    {
        "Id": 33,
        "Name": "Bulgaria",
        "PhoneCode": "+359",
        "CountryCode": "BG",
        "IsRestricted": false
    },
    {
        "Id": 34,
        "Name": "Burkina Faso",
        "PhoneCode": "+226",
        "CountryCode": "BF",
        "IsRestricted": false
    },
    {
        "Id": 35,
        "Name": "Burundi",
        "PhoneCode": "+257",
        "CountryCode": "BI",
        "IsRestricted": false
    },
    {
        "Id": 36,
        "Name": "Cambodia",
        "PhoneCode": "+855",
        "CountryCode": "KH",
        "IsRestricted": false
    },
    {
        "Id": 37,
        "Name": "Cameroon",
        "PhoneCode": "+237",
        "CountryCode": "CM",
        "IsRestricted": false
    },
    {
        "Id": 38,
        "Name": "Canada",
        "PhoneCode": "+1",
        "CountryCode": "CA",
        "IsRestricted": false
    },
    {
        "Id": 39,
        "Name": "Cape Verde",
        "PhoneCode": "+238",
        "CountryCode": "CV",
        "IsRestricted": false
    },
    {
        "Id": 40,
        "Name": "Cayman Islands",
        "PhoneCode": "+ 345",
        "CountryCode": "KY",
        "IsRestricted": false
    },
    {
        "Id": 41,
        "Name": "Central African Republic",
        "PhoneCode": "+236",
        "CountryCode": "CF",
        "IsRestricted": false
    },
    {
        "Id": 42,
        "Name": "Chad",
        "PhoneCode": "+235",
        "CountryCode": "TD",
        "IsRestricted": false
    },
    {
        "Id": 43,
        "Name": "Chile",
        "PhoneCode": "+56",
        "CountryCode": "CL",
        "IsRestricted": false
    },
    {
        "Id": 44,
        "Name": "China",
        "PhoneCode": "+86",
        "CountryCode": "CN",
        "IsRestricted": false
    },
    {
        "Id": 45,
        "Name": "Christmas Island",
        "PhoneCode": "+61",
        "CountryCode": "CX",
        "IsRestricted": false
    },
    {
        "Id": 46,
        "Name": "Cocos (Keeling) Islands",
        "PhoneCode": "+61",
        "CountryCode": "CC",
        "IsRestricted": false
    },
    {
        "Id": 47,
        "Name": "Colombia",
        "PhoneCode": "+57",
        "CountryCode": "CO",
        "IsRestricted": false
    },
    {
        "Id": 48,
        "Name": "Comoros",
        "PhoneCode": "+269",
        "CountryCode": "KM",
        "IsRestricted": false
    },
    {
        "Id": 49,
        "Name": "Congo",
        "PhoneCode": "+242",
        "CountryCode": "CG",
        "IsRestricted": false
    },
    {
        "Id": 50,
        "Name": "Congo, The Democratic Republic of the Congo",
        "PhoneCode": "+243",
        "CountryCode": "CD",
        "IsRestricted": false
    },
    {
        "Id": 51,
        "Name": "Cook Islands",
        "PhoneCode": "+682",
        "CountryCode": "CK",
        "IsRestricted": false
    },
    {
        "Id": 52,
        "Name": "Costa Rica",
        "PhoneCode": "+506",
        "CountryCode": "CR",
        "IsRestricted": false
    },
    {
        "Id": 53,
        "Name": "Cote d'Ivoire",
        "PhoneCode": "+225",
        "CountryCode": "CI",
        "IsRestricted": false
    },
    {
        "Id": 54,
        "Name": "Croatia",
        "PhoneCode": "+385",
        "CountryCode": "HR",
        "IsRestricted": false
    },
    {
        "Id": 55,
        "Name": "Cuba",
        "PhoneCode": "+53",
        "CountryCode": "CU",
        "IsRestricted": false
    },
    {
        "Id": 56,
        "Name": "Cyprus",
        "PhoneCode": "+357",
        "CountryCode": "CY",
        "IsRestricted": false
    },
    {
        "Id": 57,
        "Name": "Czech Republic",
        "PhoneCode": "+420",
        "CountryCode": "CZ",
        "IsRestricted": false
    },
    {
        "Id": 58,
        "Name": "Denmark",
        "PhoneCode": "+45",
        "CountryCode": "DK",
        "IsRestricted": false
    },
    {
        "Id": 59,
        "Name": "Djibouti",
        "PhoneCode": "+253",
        "CountryCode": "DJ",
        "IsRestricted": false
    },
    {
        "Id": 60,
        "Name": "Dominica",
        "PhoneCode": "+1767",
        "CountryCode": "DM",
        "IsRestricted": false
    },
    {
        "Id": 61,
        "Name": "Dominican Republic",
        "PhoneCode": "+1849",
        "CountryCode": "DO",
        "IsRestricted": false
    },
    {
        "Id": 62,
        "Name": "Ecuador",
        "PhoneCode": "+593",
        "CountryCode": "EC",
        "IsRestricted": false
    },
    {
        "Id": 63,
        "Name": "Egypt",
        "PhoneCode": "+20",
        "CountryCode": "EG",
        "IsRestricted": false
    },
    {
        "Id": 64,
        "Name": "El Salvador",
        "PhoneCode": "+503",
        "CountryCode": "SV",
        "IsRestricted": false
    },
    {
        "Id": 65,
        "Name": "Equatorial Guinea",
        "PhoneCode": "+240",
        "CountryCode": "GQ",
        "IsRestricted": false
    },
    {
        "Id": 66,
        "Name": "Eritrea",
        "PhoneCode": "+291",
        "CountryCode": "ER",
        "IsRestricted": false
    },
    {
        "Id": 67,
        "Name": "Estonia",
        "PhoneCode": "+372",
        "CountryCode": "EE",
        "IsRestricted": false
    },
    {
        "Id": 68,
        "Name": "Ethiopia",
        "PhoneCode": "+251",
        "CountryCode": "ET",
        "IsRestricted": false
    },
    {
        "Id": 69,
        "Name": "Falkland Islands (Malvinas)",
        "PhoneCode": "+500",
        "CountryCode": "FK",
        "IsRestricted": false
    },
    {
        "Id": 70,
        "Name": "Faroe Islands",
        "PhoneCode": "+298",
        "CountryCode": "FO",
        "IsRestricted": false
    },
    {
        "Id": 71,
        "Name": "Fiji",
        "PhoneCode": "+679",
        "CountryCode": "FJ",
        "IsRestricted": false
    },
    {
        "Id": 72,
        "Name": "Finland",
        "PhoneCode": "+358",
        "CountryCode": "FI",
        "IsRestricted": false
    },
    {
        "Id": 73,
        "Name": "France",
        "PhoneCode": "+33",
        "CountryCode": "FR",
        "IsRestricted": false
    },
    {
        "Id": 74,
        "Name": "French Guiana",
        "PhoneCode": "+594",
        "CountryCode": "GF",
        "IsRestricted": false
    },
    {
        "Id": 75,
        "Name": "French Polynesia",
        "PhoneCode": "+689",
        "CountryCode": "PF",
        "IsRestricted": false
    },
    {
        "Id": 76,
        "Name": "Gabon",
        "PhoneCode": "+241",
        "CountryCode": "GA",
        "IsRestricted": false
    },
    {
        "Id": 77,
        "Name": "Gambia",
        "PhoneCode": "+220",
        "CountryCode": "GM",
        "IsRestricted": false
    },
    {
        "Id": 78,
        "Name": "Georgia",
        "PhoneCode": "+995",
        "CountryCode": "GE",
        "IsRestricted": false
    },
    {
        "Id": 79,
        "Name": "Germany",
        "PhoneCode": "+49",
        "CountryCode": "DE",
        "IsRestricted": false
    },
    {
        "Id": 80,
        "Name": "Ghana",
        "PhoneCode": "+233",
        "CountryCode": "GH",
        "IsRestricted": false
    },
    {
        "Id": 81,
        "Name": "Gibraltar",
        "PhoneCode": "+350",
        "CountryCode": "GI",
        "IsRestricted": false
    },
    {
        "Id": 82,
        "Name": "Greece",
        "PhoneCode": "+30",
        "CountryCode": "GR",
        "IsRestricted": false
    },
    {
        "Id": 83,
        "Name": "Greenland",
        "PhoneCode": "+299",
        "CountryCode": "GL",
        "IsRestricted": false
    },
    {
        "Id": 84,
        "Name": "Grenada",
        "PhoneCode": "+1473",
        "CountryCode": "GD",
        "IsRestricted": false
    },
    {
        "Id": 85,
        "Name": "Guadeloupe",
        "PhoneCode": "+590",
        "CountryCode": "GP",
        "IsRestricted": false
    },
    {
        "Id": 86,
        "Name": "Guam",
        "PhoneCode": "+1671",
        "CountryCode": "GU",
        "IsRestricted": false
    },
    {
        "Id": 87,
        "Name": "Guatemala",
        "PhoneCode": "+502",
        "CountryCode": "GT",
        "IsRestricted": false
    },
    {
        "Id": 88,
        "Name": "Guernsey",
        "PhoneCode": "+44",
        "CountryCode": "GG",
        "IsRestricted": false
    },
    {
        "Id": 89,
        "Name": "Guinea",
        "PhoneCode": "+224",
        "CountryCode": "GN",
        "IsRestricted": false
    },
    {
        "Id": 90,
        "Name": "Guinea-Bissau",
        "PhoneCode": "+245",
        "CountryCode": "GW",
        "IsRestricted": false
    },
    {
        "Id": 91,
        "Name": "Guyana",
        "PhoneCode": "+595",
        "CountryCode": "GY",
        "IsRestricted": false
    },
    {
        "Id": 92,
        "Name": "Haiti",
        "PhoneCode": "+509",
        "CountryCode": "HT",
        "IsRestricted": false
    },
    {
        "Id": 93,
        "Name": "Holy See (Vatican City State)",
        "PhoneCode": "+379",
        "CountryCode": "VA",
        "IsRestricted": false
    },
    {
        "Id": 94,
        "Name": "Honduras",
        "PhoneCode": "+504",
        "CountryCode": "HN",
        "IsRestricted": false
    },
    {
        "Id": 95,
        "Name": "Hong Kong",
        "PhoneCode": "+852",
        "CountryCode": "HK",
        "IsRestricted": false
    },
    {
        "Id": 96,
        "Name": "Hungary",
        "PhoneCode": "+36",
        "CountryCode": "HU",
        "IsRestricted": false
    },
    {
        "Id": 97,
        "Name": "Iceland",
        "PhoneCode": "+354",
        "CountryCode": "IS",
        "IsRestricted": false
    },
    {
        "Id": 98,
        "Name": "India",
        "PhoneCode": "+91",
        "CountryCode": "IN",
        "IsRestricted": false
    },
    {
        "Id": 99,
        "Name": "Indonesia",
        "PhoneCode": "+62",
        "CountryCode": "ID",
        "IsRestricted": false
    },
    {
        "Id": 100,
        "Name": "Iran, Islamic Republic of Persian Gulf",
        "PhoneCode": "+98",
        "CountryCode": "IR",
        "IsRestricted": false
    },
    {
        "Id": 101,
        "Name": "Iraq",
        "PhoneCode": "+964",
        "CountryCode": "IQ",
        "IsRestricted": false
    },
    {
        "Id": 102,
        "Name": "Ireland",
        "PhoneCode": "+353",
        "CountryCode": "IE",
        "IsRestricted": false
    },
    {
        "Id": 103,
        "Name": "Isle of Man",
        "PhoneCode": "+44",
        "CountryCode": "IM",
        "IsRestricted": false
    },
    {
        "Id": 104,
        "Name": "Israel",
        "PhoneCode": "+972",
        "CountryCode": "IL",
        "IsRestricted": false
    },
    {
        "Id": 105,
        "Name": "Italy",
        "PhoneCode": "+39",
        "CountryCode": "IT",
        "IsRestricted": false
    },
    {
        "Id": 106,
        "Name": "Jamaica",
        "PhoneCode": "+1876",
        "CountryCode": "JM",
        "IsRestricted": false
    },
    {
        "Id": 107,
        "Name": "Japan",
        "PhoneCode": "+81",
        "CountryCode": "JP",
        "IsRestricted": false
    },
    {
        "Id": 109,
        "Name": "Jersey",
        "PhoneCode": "+44",
        "CountryCode": "JE",
        "IsRestricted": false
    },
    {
        "Id": 110,
        "Name": "Jordan",
        "PhoneCode": "+962",
        "CountryCode": "JO",
        "IsRestricted": false
    },
    {
        "Id": 111,
        "Name": "Kazakhstan",
        "PhoneCode": "+77",
        "CountryCode": "KZ",
        "IsRestricted": false
    },
    {
        "Id": 112,
        "Name": "Kenya",
        "PhoneCode": "+254",
        "CountryCode": "KE",
        "IsRestricted": false
    },
    {
        "Id": 113,
        "Name": "Kiribati",
        "PhoneCode": "+686",
        "CountryCode": "KI",
        "IsRestricted": false
    },
    {
        "Id": 114,
        "Name": "Korea, Democratic People's Republic of Korea",
        "PhoneCode": "+850",
        "CountryCode": "KP",
        "IsRestricted": false
    },
    {
        "Id": 115,
        "Name": "Korea, Republic of South Korea",
        "PhoneCode": "+82",
        "CountryCode": "KR",
        "IsRestricted": false
    },
    {
        "Id": 116,
        "Name": "Kuwait",
        "PhoneCode": "+965",
        "CountryCode": "KW",
        "IsRestricted": false
    },
    {
        "Id": 117,
        "Name": "Kyrgyzstan",
        "PhoneCode": "+996",
        "CountryCode": "KG",
        "IsRestricted": false
    },
    {
        "Id": 118,
        "Name": "Laos",
        "PhoneCode": "+856",
        "CountryCode": "LA",
        "IsRestricted": false
    },
    {
        "Id": 119,
        "Name": "Latvia",
        "PhoneCode": "+371",
        "CountryCode": "LV",
        "IsRestricted": false
    },
    {
        "Id": 120,
        "Name": "Lebanon",
        "PhoneCode": "+961",
        "CountryCode": "LB",
        "IsRestricted": false
    },
    {
        "Id": 121,
        "Name": "Lesotho",
        "PhoneCode": "+266",
        "CountryCode": "LS",
        "IsRestricted": false
    },
    {
        "Id": 122,
        "Name": "Liberia",
        "PhoneCode": "+231",
        "CountryCode": "LR",
        "IsRestricted": false
    },
    {
        "Id": 123,
        "Name": "Libyan Arab Jamahiriya",
        "PhoneCode": "+218",
        "CountryCode": "LY",
        "IsRestricted": false
    },
    {
        "Id": 124,
        "Name": "Liechtenstein",
        "PhoneCode": "+423",
        "CountryCode": "LI",
        "IsRestricted": false
    },
    {
        "Id": 125,
        "Name": "Lithuania",
        "PhoneCode": "+370",
        "CountryCode": "LT",
        "IsRestricted": false
    },
    {
        "Id": 126,
        "Name": "Luxembourg",
        "PhoneCode": "+352",
        "CountryCode": "LU",
        "IsRestricted": false
    },
    {
        "Id": 127,
        "Name": "Macao",
        "PhoneCode": "+853",
        "CountryCode": "MO",
        "IsRestricted": false
    },
    {
        "Id": 128,
        "Name": "Macedonia",
        "PhoneCode": "+389",
        "CountryCode": "MK",
        "IsRestricted": false
    },
    {
        "Id": 129,
        "Name": "Madagascar",
        "PhoneCode": "+261",
        "CountryCode": "MG",
        "IsRestricted": false
    },
    {
        "Id": 130,
        "Name": "Malawi",
        "PhoneCode": "+265",
        "CountryCode": "MW",
        "IsRestricted": false
    },
    {
        "Id": 131,
        "Name": "Malaysia",
        "PhoneCode": "+60",
        "CountryCode": "MY",
        "IsRestricted": false
    },
    {
        "Id": 132,
        "Name": "Maldives",
        "PhoneCode": "+960",
        "CountryCode": "MV",
        "IsRestricted": false
    },
    {
        "Id": 133,
        "Name": "Mali",
        "PhoneCode": "+223",
        "CountryCode": "ML",
        "IsRestricted": false
    },
    {
        "Id": 134,
        "Name": "Malta",
        "PhoneCode": "+356",
        "CountryCode": "MT",
        "IsRestricted": false
    },
    {
        "Id": 135,
        "Name": "Marshall Islands",
        "PhoneCode": "+692",
        "CountryCode": "MH",
        "IsRestricted": false
    },
    {
        "Id": 136,
        "Name": "Martinique",
        "PhoneCode": "+596",
        "CountryCode": "MQ",
        "IsRestricted": false
    },
    {
        "Id": 137,
        "Name": "Mauritania",
        "PhoneCode": "+222",
        "CountryCode": "MR",
        "IsRestricted": false
    },
    {
        "Id": 138,
        "Name": "Mauritius",
        "PhoneCode": "+230",
        "CountryCode": "MU",
        "IsRestricted": false
    },
    {
        "Id": 139,
        "Name": "Mayotte",
        "PhoneCode": "+262",
        "CountryCode": "YT",
        "IsRestricted": false
    },
    {
        "Id": 140,
        "Name": "Mexico",
        "PhoneCode": "+52",
        "CountryCode": "MX",
        "IsRestricted": false
    },
    {
        "Id": 141,
        "Name": "Micronesia, Federated States of Micronesia",
        "PhoneCode": "+691",
        "CountryCode": "FM",
        "IsRestricted": false
    },
    {
        "Id": 142,
        "Name": "Moldova",
        "PhoneCode": "+373",
        "CountryCode": "MD",
        "IsRestricted": false
    },
    {
        "Id": 143,
        "Name": "Monaco",
        "PhoneCode": "+377",
        "CountryCode": "MC",
        "IsRestricted": false
    },
    {
        "Id": 144,
        "Name": "Mongolia",
        "PhoneCode": "+976",
        "CountryCode": "MN",
        "IsRestricted": false
    },
    {
        "Id": 145,
        "Name": "Montenegro",
        "PhoneCode": "+382",
        "CountryCode": "ME",
        "IsRestricted": false
    },
    {
        "Id": 146,
        "Name": "Montserrat",
        "PhoneCode": "+1664",
        "CountryCode": "MS",
        "IsRestricted": false
    },
    {
        "Id": 147,
        "Name": "Morocco",
        "PhoneCode": "+212",
        "CountryCode": "MA",
        "IsRestricted": false
    },
    {
        "Id": 148,
        "Name": "Mozambique",
        "PhoneCode": "+258",
        "CountryCode": "MZ",
        "IsRestricted": false
    },
    {
        "Id": 149,
        "Name": "Myanmar",
        "PhoneCode": "+95",
        "CountryCode": "MM",
        "IsRestricted": false
    },
    {
        "Id": 150,
        "Name": "Namibia",
        "PhoneCode": "+264",
        "CountryCode": "NA",
        "IsRestricted": false
    },
    {
        "Id": 151,
        "Name": "Nauru",
        "PhoneCode": "+674",
        "CountryCode": "NR",
        "IsRestricted": false
    },
    {
        "Id": 152,
        "Name": "Nepal",
        "PhoneCode": "+977",
        "CountryCode": "NP",
        "IsRestricted": false
    },
    {
        "Id": 153,
        "Name": "Netherlands",
        "PhoneCode": "+31",
        "CountryCode": "NL",
        "IsRestricted": false
    },
    {
        "Id": 154,
        "Name": "Netherlands Antilles",
        "PhoneCode": "+599",
        "CountryCode": "AN",
        "IsRestricted": false
    },
    {
        "Id": 155,
        "Name": "New Caledonia",
        "PhoneCode": "+687",
        "CountryCode": "NC",
        "IsRestricted": false
    },
    {
        "Id": 156,
        "Name": "New Zealand",
        "PhoneCode": "+64",
        "CountryCode": "NZ",
        "IsRestricted": false
    },
    {
        "Id": 157,
        "Name": "Nicaragua",
        "PhoneCode": "+505",
        "CountryCode": "NI",
        "IsRestricted": false
    },
    {
        "Id": 158,
        "Name": "Niger",
        "PhoneCode": "+227",
        "CountryCode": "NE",
        "IsRestricted": false
    },
    {
        "Id": 159,
        "Name": "Nigeria",
        "PhoneCode": "+234",
        "CountryCode": "NG",
        "IsRestricted": false
    },
    {
        "Id": 160,
        "Name": "Niue",
        "PhoneCode": "+683",
        "CountryCode": "NU",
        "IsRestricted": false
    },
    {
        "Id": 161,
        "Name": "Norfolk Island",
        "PhoneCode": "+672",
        "CountryCode": "NF",
        "IsRestricted": false
    },
    {
        "Id": 162,
        "Name": "Northern Mariana Islands",
        "PhoneCode": "+1670",
        "CountryCode": "MP",
        "IsRestricted": false
    },
    {
        "Id": 163,
        "Name": "Norway",
        "PhoneCode": "+47",
        "CountryCode": "NO",
        "IsRestricted": false
    },
    {
        "Id": 164,
        "Name": "Oman",
        "PhoneCode": "+968",
        "CountryCode": "OM",
        "IsRestricted": false
    },
    {
        "Id": 165,
        "Name": "Pakistan",
        "PhoneCode": "+92",
        "CountryCode": "PK",
        "IsRestricted": false
    },
    {
        "Id": 166,
        "Name": "Palau",
        "PhoneCode": "+680",
        "CountryCode": "PW",
        "IsRestricted": false
    },
    {
        "Id": 167,
        "Name": "Palestinian Territory, Occupied",
        "PhoneCode": "+970",
        "CountryCode": "PS",
        "IsRestricted": false
    },
    {
        "Id": 168,
        "Name": "Panama",
        "PhoneCode": "+507",
        "CountryCode": "PA",
        "IsRestricted": false
    },
    {
        "Id": 169,
        "Name": "Papua New Guinea",
        "PhoneCode": "+675",
        "CountryCode": "PG",
        "IsRestricted": false
    },
    {
        "Id": 170,
        "Name": "Paraguay",
        "PhoneCode": "+595",
        "CountryCode": "PY",
        "IsRestricted": false
    },
    {
        "Id": 171,
        "Name": "Peru",
        "PhoneCode": "+51",
        "CountryCode": "PE",
        "IsRestricted": false
    },
    {
        "Id": 172,
        "Name": "Philippines",
        "PhoneCode": "+63",
        "CountryCode": "PH",
        "IsRestricted": false
    },
    {
        "Id": 173,
        "Name": "Pitcairn",
        "PhoneCode": "+872",
        "CountryCode": "PN",
        "IsRestricted": false
    },
    {
        "Id": 174,
        "Name": "Poland",
        "PhoneCode": "+48",
        "CountryCode": "PL",
        "IsRestricted": false
    },
    {
        "Id": 175,
        "Name": "Portugal",
        "PhoneCode": "+351",
        "CountryCode": "PT",
        "IsRestricted": false
    },
    {
        "Id": 176,
        "Name": "Puerto Rico",
        "PhoneCode": "+1939",
        "CountryCode": "PR",
        "IsRestricted": false
    },
    {
        "Id": 177,
        "Name": "Qatar",
        "PhoneCode": "+974",
        "CountryCode": "QA",
        "IsRestricted": false
    },
    {
        "Id": 178,
        "Name": "Romania",
        "PhoneCode": "+40",
        "CountryCode": "RO",
        "IsRestricted": false
    },
    {
        "Id": 179,
        "Name": "Russia",
        "PhoneCode": "+7",
        "CountryCode": "RU",
        "IsRestricted": false
    },
    {
        "Id": 180,
        "Name": "Rwanda",
        "PhoneCode": "+250",
        "CountryCode": "RW",
        "IsRestricted": false
    },
    {
        "Id": 181,
        "Name": "Reunion",
        "PhoneCode": "+262",
        "CountryCode": "RE",
        "IsRestricted": false
    },
    {
        "Id": 182,
        "Name": "Saint Barthelemy",
        "PhoneCode": "+590",
        "CountryCode": "BL",
        "IsRestricted": false
    },
    {
        "Id": 183,
        "Name": "Saint Helena, Ascension and Tristan Da Cunha",
        "PhoneCode": "+290",
        "CountryCode": "SH",
        "IsRestricted": false
    },
    {
        "Id": 184,
        "Name": "Saint Kitts and Nevis",
        "PhoneCode": "+1869",
        "CountryCode": "KN",
        "IsRestricted": false
    },
    {
        "Id": 185,
        "Name": "Saint Lucia",
        "PhoneCode": "+1758",
        "CountryCode": "LC",
        "IsRestricted": false
    },
    {
        "Id": 186,
        "Name": "Saint Martin",
        "PhoneCode": "+590",
        "CountryCode": "MF",
        "IsRestricted": false
    },
    {
        "Id": 187,
        "Name": "Saint Pierre and Miquelon",
        "PhoneCode": "+508",
        "CountryCode": "PM",
        "IsRestricted": false
    },
    {
        "Id": 188,
        "Name": "Saint Vincent and the Grenadines",
        "PhoneCode": "+1784",
        "CountryCode": "VC",
        "IsRestricted": false
    },
    {
        "Id": 189,
        "Name": "Samoa",
        "PhoneCode": "+685",
        "CountryCode": "WS",
        "IsRestricted": false
    },
    {
        "Id": 190,
        "Name": "San Marino",
        "PhoneCode": "+378",
        "CountryCode": "SM",
        "IsRestricted": false
    },
    {
        "Id": 191,
        "Name": "Sao Tome and Principe",
        "PhoneCode": "+239",
        "CountryCode": "ST",
        "IsRestricted": false
    },
    {
        "Id": 192,
        "Name": "Saudi Arabia",
        "PhoneCode": "+966",
        "CountryCode": "SA",
        "IsRestricted": false
    },
    {
        "Id": 193,
        "Name": "Senegal",
        "PhoneCode": "+221",
        "CountryCode": "SN",
        "IsRestricted": false
    },
    {
        "Id": 194,
        "Name": "Serbia",
        "PhoneCode": "+381",
        "CountryCode": "RS",
        "IsRestricted": false
    },
    {
        "Id": 195,
        "Name": "Seychelles",
        "PhoneCode": "+248",
        "CountryCode": "SC",
        "IsRestricted": false
    },
    {
        "Id": 196,
        "Name": "Sierra Leone",
        "PhoneCode": "+232",
        "CountryCode": "SL",
        "IsRestricted": false
    },
    {
        "Id": 197,
        "Name": "Singapore",
        "PhoneCode": "+65",
        "CountryCode": "SG",
        "IsRestricted": false
    },
    {
        "Id": 198,
        "Name": "Slovakia",
        "PhoneCode": "+421",
        "CountryCode": "SK",
        "IsRestricted": false
    },
    {
        "Id": 199,
        "Name": "Slovenia",
        "PhoneCode": "+386",
        "CountryCode": "SI",
        "IsRestricted": false
    },
    {
        "Id": 200,
        "Name": "Solomon Islands",
        "PhoneCode": "+677",
        "CountryCode": "SB",
        "IsRestricted": false
    },
    {
        "Id": 201,
        "Name": "Somalia",
        "PhoneCode": "+252",
        "CountryCode": "SO",
        "IsRestricted": false
    },
    {
        "Id": 202,
        "Name": "South Africa",
        "PhoneCode": "+27",
        "CountryCode": "ZA",
        "IsRestricted": false
    },
    {
        "Id": 203,
        "Name": "South Sudan",
        "PhoneCode": "+211",
        "CountryCode": "SS",
        "IsRestricted": false
    },
    {
        "Id": 204,
        "Name": "South Georgia and the South Sandwich Islands",
        "PhoneCode": "+500",
        "CountryCode": "GS",
        "IsRestricted": false
    },
    {
        "Id": 205,
        "Name": "Spain",
        "PhoneCode": "+34",
        "CountryCode": "ES",
        "IsRestricted": false
    },
    {
        "Id": 206,
        "Name": "Sri Lanka",
        "PhoneCode": "+94",
        "CountryCode": "LK",
        "IsRestricted": false
    },
    {
        "Id": 207,
        "Name": "Sudan",
        "PhoneCode": "+249",
        "CountryCode": "SD",
        "IsRestricted": false
    },
    {
        "Id": 208,
        "Name": "Suriname",
        "PhoneCode": "+597",
        "CountryCode": "SR",
        "IsRestricted": false
    },
    {
        "Id": 209,
        "Name": "Svalbard and Jan Mayen",
        "PhoneCode": "+47",
        "CountryCode": "SJ",
        "IsRestricted": false
    },
    {
        "Id": 210,
        "Name": "Swaziland",
        "PhoneCode": "+268",
        "CountryCode": "SZ",
        "IsRestricted": false
    },
    {
        "Id": 211,
        "Name": "Sweden",
        "PhoneCode": "+46",
        "CountryCode": "SE",
        "IsRestricted": false
    },
    {
        "Id": 212,
        "Name": "Switzerland",
        "PhoneCode": "+41",
        "CountryCode": "CH",
        "IsRestricted": false
    },
    {
        "Id": 213,
        "Name": "Syrian Arab Republic",
        "PhoneCode": "+963",
        "CountryCode": "SY",
        "IsRestricted": false
    },
    {
        "Id": 214,
        "Name": "Taiwan",
        "PhoneCode": "+886",
        "CountryCode": "TW",
        "IsRestricted": false
    },
    {
        "Id": 215,
        "Name": "Tajikistan",
        "PhoneCode": "+992",
        "CountryCode": "TJ",
        "IsRestricted": false
    },
    {
        "Id": 216,
        "Name": "Tanzania, United Republic of Tanzania",
        "PhoneCode": "+255",
        "CountryCode": "TZ",
        "IsRestricted": false
    },
    {
        "Id": 217,
        "Name": "Thailand",
        "PhoneCode": "+66",
        "CountryCode": "TH",
        "IsRestricted": false
    },
    {
        "Id": 218,
        "Name": "Timor-Leste",
        "PhoneCode": "+670",
        "CountryCode": "TL",
        "IsRestricted": false
    },
    {
        "Id": 219,
        "Name": "Togo",
        "PhoneCode": "+228",
        "CountryCode": "TG",
        "IsRestricted": false
    },
    {
        "Id": 220,
        "Name": "Tokelau",
        "PhoneCode": "+690",
        "CountryCode": "TK",
        "IsRestricted": false
    },
    {
        "Id": 221,
        "Name": "Tonga",
        "PhoneCode": "+676",
        "CountryCode": "TO",
        "IsRestricted": false
    },
    {
        "Id": 222,
        "Name": "Trinidad and Tobago",
        "PhoneCode": "+1868",
        "CountryCode": "TT",
        "IsRestricted": false
    },
    {
        "Id": 223,
        "Name": "Tunisia",
        "PhoneCode": "+216",
        "CountryCode": "TN",
        "IsRestricted": false
    },
    {
        "Id": 224,
        "Name": "Turkey",
        "PhoneCode": "+90",
        "CountryCode": "TR",
        "IsRestricted": false
    },
    {
        "Id": 225,
        "Name": "Turkmenistan",
        "PhoneCode": "+993",
        "CountryCode": "TM",
        "IsRestricted": false
    },
    {
        "Id": 226,
        "Name": "Turks and Caicos Islands",
        "PhoneCode": "+1649",
        "CountryCode": "TC",
        "IsRestricted": false
    },
    {
        "Id": 227,
        "Name": "Tuvalu",
        "PhoneCode": "+688",
        "CountryCode": "TV",
        "IsRestricted": false
    },
    {
        "Id": 228,
        "Name": "Uganda",
        "PhoneCode": "+256",
        "CountryCode": "UG",
        "IsRestricted": false
    },
    {
        "Id": 229,
        "Name": "Ukraine",
        "PhoneCode": "+380",
        "CountryCode": "UA",
        "IsRestricted": false
    },
    {
        "Id": 230,
        "Name": "United Arab Emirates",
        "PhoneCode": "+971",
        "CountryCode": "AE",
        "IsRestricted": false
    },
    {
        "Id": 231,
        "Name": "United Kingdom",
        "PhoneCode": "+44",
        "CountryCode": "GB",
        "IsRestricted": false
    },
    {
        "Id": 232,
        "Name": "United States",
        "PhoneCode": "+1",
        "CountryCode": "US",
        "IsRestricted": false
    },
    {
        "Id": 233,
        "Name": "Uruguay",
        "PhoneCode": "+598",
        "CountryCode": "UY",
        "IsRestricted": false
    },
    {
        "Id": 234,
        "Name": "Uzbekistan",
        "PhoneCode": "+998",
        "CountryCode": "UZ",
        "IsRestricted": false
    },
    {
        "Id": 235,
        "Name": "Vanuatu",
        "PhoneCode": "+678",
        "CountryCode": "VU",
        "IsRestricted": false
    },
    {
        "Id": 236,
        "Name": "Venezuela, Bolivarian Republic of Venezuela",
        "PhoneCode": "+58",
        "CountryCode": "VE",
        "IsRestricted": false
    },
    {
        "Id": 237,
        "Name": "Vietnam",
        "PhoneCode": "+84",
        "CountryCode": "VN",
        "IsRestricted": false
    },
    {
        "Id": 238,
        "Name": "Virgin Islands, British",
        "PhoneCode": "+1284",
        "CountryCode": "VG",
        "IsRestricted": false
    },
    {
        "Id": 239,
        "Name": "Virgin Islands, U.S.",
        "PhoneCode": "+1340",
        "CountryCode": "VI",
        "IsRestricted": false
    },
    {
        "Id": 240,
        "Name": "Wallis and Futuna",
        "PhoneCode": "+681",
        "CountryCode": "WF",
        "IsRestricted": false
    },
    {
        "Id": 241,
        "Name": "Yemen",
        "PhoneCode": "+967",
        "CountryCode": "YE",
        "IsRestricted": false
    },
    {
        "Id": 242,
        "Name": "Zambia",
        "PhoneCode": "+260",
        "CountryCode": "ZM",
        "IsRestricted": false
    },
    {
        "Id": 243,
        "Name": "Zimbabwe",
        "PhoneCode": "+263",
        "CountryCode": "ZW"
    }
];