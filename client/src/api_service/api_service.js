import axios from 'axios';
import { URLS } from '../constant';

const Header = {
    'Content-Type': 'application/json'
};

const getAccessToken = () => {
    let user = JSON.parse(localStorage.getItem('$@meta@$'));

    if (user && user.userReducer && user.userReducer.token) {
        return user.userReducer.token;
    } else {
        console.warn('No token found');
        return null;
    }
}

const getAuthHeader = () => {
    let header = Header;
    let token = getAccessToken();

    if (token) {
        header = Object.assign({}, header, { Authorization: `Bearer ${token}` });
    } else {
        header = Object.assign({}, header, {});
    }

    return header
}

export default function ajax(url, method, data = null) {
    return new Promise((resolve, reject) => {
        let apiUrl = process.env.REACT_APP_API_URL + url;
        // let apiUrl = 'http://server.mv4f.io' + url;

        const headers = getAuthHeader();

        var options = {
            url: apiUrl,
            method: method,
            headers
        };

        if (data) {
            options.data = data;
        }

        axios(options).then((res) => {
            resolve(res.data);
        }).catch((err) => {
            if (err.response) {
                switch (err.response.status) {
                    case 500:
                        console.error('Internal Server Error')
                        reject(err);
                        break
                    case 401:
                    case 403:
                        console.error('Unauthorized');
                        localStorage.removeItem('$@meta@$');
                        // window.location.reload();
                        let path = window.location.pathname;
                        let subPath = URLS.indexOf(path) > -1 ? `/${URLS.indexOf(path)}` : '';
                        window.location.href = `/login${subPath}`;
                        break
                    case 400:
                        console.error('Bad request');
                        reject(err.response.data);
                        break
                    default:
                        reject(err);
                        break
                }
            }
        })
    })
}