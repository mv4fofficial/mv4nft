import ajax from './api_service';

class ApiRequest {
  async loginAuth(data) {
    const loginUrl = '/api/Account/Login';
    return await ajax(loginUrl, 'PUT', data);
  }
  async registerUser(data) {
    const registerUrl = '/api/Account/Register';
    return await ajax(registerUrl, 'POST', data);
  }
  async forgotPassword(data) {
    const forgotPasswordUrl = '/api/Account/ForgotPassword';
    return await ajax(forgotPasswordUrl, 'POST', data);
  }
  async resetPassword(data) {
    const resetPasswordUrl = '/api/Account/ResetPassword';
    return await ajax(resetPasswordUrl, 'POST', data);
  }
  async viewUser() {
    const viewProfileUrl = `/api/Account/ViewProfile`;
    return await ajax(viewProfileUrl, 'GET');
  }
  async viewUserById(userId = 0) {
    const viewProfileUrl = `/api/Account/ViewProfile?id=${userId}`;
    return await ajax(viewProfileUrl, 'GET');
  }
  async updateUser(data) {
    const updateUserUrl = '/api/Account/UpdateProfile';
    return await ajax(updateUserUrl, 'POST', data);
  }
  async upgraderUserRole(data) {
    const UpgradeUserRoleUrl = '/api/Account/UpgradeUserRole';
    return await ajax(UpgradeUserRoleUrl, 'POST', data);
  }
  async thirdPartyRegisterAndLogin(data) {
    const thirdPartyRegisterAndLoginUrl = `/api/Account/ThirdPartyRegisterAndLogin`;
    return await ajax(thirdPartyRegisterAndLoginUrl, 'PUT', data);
  }
  async setFollow(data) {
    const setFollowUrl = '/api/Account/FollowUser';
    return await ajax(setFollowUrl, 'POST', data);
  }
  async getFollow(data) {
    const getFollowUrl = '/api/Account/GetFollow';
    return await ajax(getFollowUrl, 'POST', data);
  }
  async recommendedForYou() {
    const recommendedForYouUrl = '/api/Account/RecommendedForYou';
    return await ajax(recommendedForYouUrl, 'GET');
  }
  async getFollowees(data) {
    const followeesUrl = '/api/Account/GetUsers';
    return await ajax(followeesUrl, 'POST', data);
  }
  async getTopFollower(data) {
    const getFollowerUrl = '/api/Home/GetTopFollower';
    return await ajax(getFollowerUrl, 'GET');
  }
  


  async openStripeAccount(data) {
    const openStripeAccountUrl = `/api/Payment/CreateAccount`;
    return await ajax(openStripeAccountUrl, 'POST', data);
  }
  async completeStripeAccount(data) {
    const completeStripeAccountUrl = `/api/Payment/CompleteAccount`;
    return await ajax(completeStripeAccountUrl, 'POST', data);
  }
  async checkAccount(data) {
    const checkAccountUrl = `/api/Payment/CheckAccount`;
    return await ajax(checkAccountUrl, 'POST', data);
  }
  async updateCompleteAccountStatus(data) {
    const updateCompleteAccountStatusUrl = `/api/Payment/UpdateCompleteAccountStatus`;
    return await ajax(updateCompleteAccountStatusUrl, 'POST', data);
  }
  async loginStripeAccount(data) {
    const loginStripeAccountUrl = `/api/Payment/CreateLoginLink`;
    return await ajax(loginStripeAccountUrl, 'POST', data);
  }
  async fetchTransactions(data) {
    const fetchTransactionsUrl = `/api/Payment/FetchTransactions`;
    return await ajax(fetchTransactionsUrl, 'POST', data);
  }

  async getAllCategories() {
    const categoriesUrl = '/api/Admin/ViewAllCategories';
    return await ajax(categoriesUrl, 'GET');
  }
  async getProducts(data) {
    const productsUrl = '/api/Home/AllProducts';
    return await ajax(productsUrl, 'POST', data);
  }
  async viewProduct(productId) {
    const viewProductUrl = `/api/Home/ViewProductById?Id=${productId}`;
    return await ajax(viewProductUrl, 'GET');
  }
  async getCategory() {
    const productsUrl = '/api/DropDown/GetCategories';
    return await ajax(productsUrl, 'GET');
  }
  async getSubCategory() {
    const productsUrl = '/api/DropDown/GetSubCategories';
    return await ajax(productsUrl, 'GET');
  }
  async addToCart(productId) {
    const productsUrl = `/api/Buyer/AddToCart?productId=${productId}`;
    return await ajax(productsUrl, 'POST');
  }
  async viewCartItems() {
    const viewCartUrl = `/api/Buyer/ViewCart`;
    return await ajax(viewCartUrl, 'GET');
  }
  async deleteCartItem(productId) {
    const deleteCartItem = `/api/Buyer/RemoveFromCart?cartId=${productId}`;
    return await ajax(deleteCartItem, 'POST');
  }
  async getClientSecret(data) {
    const getClientSecretUrl = '/api/Payment/GetClientSecret';
    return await ajax(getClientSecretUrl, 'POST', data);
  }
  async placeOrder(data) {
    const placeOrderUrl = '/api/Payment/PlaceOrder';
    return await ajax(placeOrderUrl, 'POST', data);
  }
  async getAccountBalance(payload) {
    const accountBalanceUrl = '/api/Payment/GetBalance';
    return await ajax(accountBalanceUrl, 'POST', payload);
  }
  async getUserOrder(payload) {
    const userOrdersUrl = '/api/Buyer/ViewUserProcuts';
    return await ajax(userOrdersUrl, 'POST', payload);
  }
  async getUserProductWishList(payload) {
    const userProductWishListUrl = '/api/Buyer/ViewProductWishLists';
    return await ajax(userProductWishListUrl, 'GET', payload);
  }
  async addProductWishList(data) {
    const productWishListUrl = `/api/Buyer/AddProductWishList`;
    return await ajax(productWishListUrl, 'POST', data);
  }
  async deleteProductWishList(data) {
    const productWishListUrl = `/api/Buyer/DeleteProductWishList`;
    return await ajax(productWishListUrl, 'POST', data);
  }
  async addProduct(productObj) {
    const addProduct = `/api/seller/AddProduct`;
    return await ajax(addProduct, 'POST', productObj);
  }
  async updateProduct(productObj) {
    const addProduct = `/api/seller/UpdateProduct`;
    return await ajax(addProduct, 'POST', productObj);
  }
  async getsellerProducts(sellerId = 0, data) {
    const productsUrl = `/api/Seller/GetSellerProduct?id=${sellerId}`;
    return await ajax(productsUrl, 'POST', data);
  }
  async getsellerPaymentDetails() {
    const productsUrl = '/api/Seller/PaymentDetailsForSeller';
    return await ajax(productsUrl, 'GET');
  }
  async deleteProduct(prodId) {
    const deleteProduct = `/api/seller/DeleteProuct?productId=${prodId}`;
    return await ajax(deleteProduct, 'DELETE');
  }
  async getCountry() {
    const productsUrl = '/api/DropDown/GetCountry';
    return await ajax(productsUrl, 'GET');
  }
  async getRoles() {
    const rolesUrl = '/api/Home/AllRoles';
    return await ajax(rolesUrl, 'GET');
  }
  async becomeSeller(payload) {
    const becomeSellerUrl = '/api/Account/BecomeSeller';
    return await ajax(becomeSellerUrl, 'POST', payload);
  }
  async verifySeller(token) {
    const verifySellerUrl = `/api/Account/VerifySellerEmail?token=${token}`;
    return await ajax(verifySellerUrl, 'POST');
  }
  async verifyRegistration(token) {
    const verifyRegistrationUrl = `/api/Account/VerifyEmail?token=${token}`;
    return await ajax(verifyRegistrationUrl, 'POST');
  }
  async saveWishlist(data, isUpdate) {
    let saveWishlistUrl = '/api/Buyer/AddWishListItem';

    if (isUpdate) {
      saveWishlistUrl = '/api/Buyer/UpdateWishList';
    }

    return await ajax(saveWishlistUrl, 'POST', data);
  }
  async getSellerQuestions() {
    const sellerQuestionUrl = '/api/Account/SellerQuestions';
    return await ajax(sellerQuestionUrl, 'GET');
  }
  async getSellerNotifications() {
    const sellerNotifications = `/api/seller/GetAllSellerNoitification`;
    return await ajax(sellerNotifications, 'GET');
  }
  async getWishlist(id) {
    const getWishlistUrl = `/api/Buyer/UserWishListById?Id=${id}`;
    return await ajax(getWishlistUrl, 'POST');
  }
  async getWishlists() {
    const wishlistUrl = `/api/Buyer/UserWishList`;
    return await ajax(wishlistUrl, 'GET');
  }
  async deleteWishlist(id) {
    const deleteWishlistUrl = `/api/Buyer/DeleteWishListItem?wishListId=${id}`;
    return await ajax(deleteWishlistUrl, 'DELETE');
  }
}

export default new ApiRequest();
