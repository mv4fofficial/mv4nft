const colourStyles = {
    color: "red",
    control: (styles, { isFocused, isSelected }) => ({
        ...styles,
        padding: "0 8px",
        backgroundColor: "#fff",
        borderColor: isFocused || isSelected ? "#939cc4" : "#ced4da",
        boxShadow: isFocused && "0 0 0 0.2rem rgba(76, 88 ,140,.25)",
        //   borderColor: "#ced4da",
        ":hover": {
            ...styles[":active"],
            borderColor: "#ced4da"
        },
        ":focus": {
            boxShadow: "0 0 0 0.2rem rgba(76, 88 ,140,.25)"
        },
        //   maxWidth: "200px"
        borderRadius: 0
    }),
    option: (styles, { data, isDisabled, isFocused, isSelected }) => {
        return {
            ...styles,
            backgroundColor: isDisabled ? null : isSelected ? "#e1e4e6" : isFocused,
            ":active": {
                ...styles[":active"],
                backgroundColor: !isDisabled && "#e1e4e6"
            },
            ":hover": {
                ...styles[":active"],
                backgroundColor: !isDisabled && "#e1e4e6"
            }
        };
    },
    placeholder: (styles) => ({
        ...styles,
        color: "#131C20"
    }),
    singleValue: (styles) => ({
        ...styles,
        color: "#131C20"
    }),
    menuList: (styles) => ({
        ...styles,
        color: "#282829"
    }),
    indicatorSeparator: (styles) => ({
        ...styles,
        display: "none"
    }),
    dropdownIndicator: (styles, { isFocused }) => ({
        ...styles,
        color: isFocused ? "#131C20" : "#131C20",
        ":hover": {
            ...styles[":active"],
            color: "#131C20"
        }
    })
};

const IMG_PREFIX = "data:image/png;base64, ";

const URLS = [
    '',
    "/dashboard/home",
    "/dashboard/profile",
    "/dashboard/products",
    "/dashboard/contact-us",
    "/dashboard/cart",
    "/dashboard/about",
    "/dashboard/our-diff",
    "/dashboard/add-product",
    "/dashboard/update-product",
    "/dashboard/seller-products",
    "/dashboard/seller-profile",
    "/dashboard/transactions",
    "/dashboard/gallery",
    "/dashboard/create-wishlist",
    "/dashboard/wishlist",
    "/dashboard/become-seller",
    "/dashboard/seller-notifications",
    "/dashboard/checkout"
];

export { colourStyles, IMG_PREFIX, URLS };