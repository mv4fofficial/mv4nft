import React from "react";
import ReactDOM from "react-dom";

import {ReactReduxFirebaseProvider} from 'react-redux-firebase';
import firebase from "./firebase";
import thunkMiddleware from 'redux-thunk';
import {createLogger} from 'redux-logger';
import {createStore, applyMiddleware, compose, combineReducers} from 'redux';
import {Provider} from 'react-redux';
// import deepExtend from 'deep-extend';
// import { connectedRouterRedirect } from 'redux-auth-wrapper/history4/redirect';
import {routerReducer} from 'react-router-redux';
import {BrowserRouter as Router, Route, Switch, Redirect} from 'react-router-dom';

import {transitions, positions, Provider as AlertProvider} from 'react-alert'
// import AlertTemplate from 'react-alert-template-basic';
import AlertTemplate from './views/Alert';

import userReducer from './user';
import App from "./App";
import Login from "./views/Login";
import Register from "./views/Register";
import SellerVerifyEmail from './views/SellerVerifyEmail';
import RegVerifyEmail from './views/RegVerifyEmail';
import ForgotPassword from "./views/forgotPassword";
import ChangePassword from "./views/changePassword";

import "./index.css";
import "./App.scss";
import "aos/dist/aos.css";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

const appReducer = combineReducers({userReducer, routing: routerReducer});

var middlewares = [];
middlewares.push(thunkMiddleware);

if (process.env.NODE_ENV !== "production") {
  const loggerMiddleware = createLogger()
  middlewares.push(loggerMiddleware)
}

const enhancer = compose(
  applyMiddleware(...middlewares));

const rootReducer = (state, action) => {
  return appReducer(state, action)
};

const persistedState = localStorage.getItem('$@meta@$') ? JSON.parse(localStorage.getItem('$@meta@$')) : {}

// let initialState = deepExtend(createStore(rootReducer, enhancer).getState(), persistedState);
let initialState = persistedState;
const store = createStore(rootReducer, initialState, enhancer);

store.subscribe(() => {
  localStorage.setItem('$@meta@$', JSON.stringify(store.getState()))
});

const options = {
  position: positions.TOP_CENTER,
  timeout: 6000,
  offset: '30px',
  transition: transitions.SCALE
}

const rrfProps = {
  firebase,
  config: {
    userProfile: "users"
  },
  dispatch: store.dispatch,
}


// const userIsAuthenticated = connectedRouterRedirect({
//   redirectPath: '/login',
//   authenticatedSelector: state => state.userReducer.isLogged ? true : false,
//   wrapperDisplayName: 'UserIsAuthenticated'
// });

// const userIsNotAuthenticated = connectedRouterRedirect({
//   redirectPath: '/dashboard',
//   allowRedirectBack: false,
//   authenticatedSelector: state => state.userReducer.isLogged ? false : true,
//   wrapperDisplayName: 'UserIsNotAuthenticated'
// });

// const LoginAuthWrapper = userIsNotAuthenticated(Login);
// const DashboardAuthWrapper = userIsAuthenticated(App);

const Application = () => {
  return (
    <Switch>
      <Route exact path='/'>
        <Redirect to='/dashboard' />
      </Route>
      <Route exact path='/login' component={(props) => <Login />} />
      <Route exact path='/login/:page_number' component={(props) => <Login />} />
      <Route exact path='/forgot-password' component={(props) => <ForgotPassword />} />
      <Route exact path='/change-password/:email/:token' component={(props) => <ChangePassword />} />

      <Route path='/dashboard' component={(props) => <App />} />
      <Route path="/register" render={(props) => <Register />} />
      <Route path="/registration-verify-email/:email/:token" render={(props) => <RegVerifyEmail />} />
      <Route path="/seller-verify-email/:email/:token" render={(props) => <SellerVerifyEmail />} />
      <Route path='*'>
        <Redirect to='/' />
      </Route>
    </Switch>
  )
};

ReactDOM.render(
  <AlertProvider template={AlertTemplate} {...options}>
    <Provider store={store}>
      <ReactReduxFirebaseProvider {...rrfProps}>
        <Router>
          <div>
            <Route path="/" component={Application} />
          </div>
        </Router>
      </ReactReduxFirebaseProvider>
    </Provider>
  </AlertProvider>,
  document.getElementById("root")
);
