import { useState, useEffect } from 'react';
import ApiRequest from '../../api_service/api_requests';
import { Nav, Container } from "react-bootstrap";
import { Link } from "react-router-dom";

const WarningBuyer = () => {
    
    const [LoginUser, setUser] = useState({
        firstName: "User",
        lastName: "mv4f",
        name: "User",
        emailId: "user@gmail.com",
        country: "Singapore",
        phoneNumber: "912341234",
        dateOfBirth: "2000-01-01",
        roles: []
    })

    useEffect(() => {
        if (LoginUser == null) {
            ApiRequest.viewUser()
            .then(res => {
                if (res && res.data && res.statusCode && res.statusCode === 200) {
                    setUser(res.data);
                } else {
                    if (res && res.message) {
                        // this.props.alert.error("Error fetching user details");
                    }
                }
            })
            .catch((err) => {
                // console.log('Error fetching user details, ', err);
            })
        }
    })
    
    if (LoginUser.roles.length > 0) {
        if (LoginUser.roles[0] === "Buyer") {
            return (
                <Container>
                    <div className='user-notify'>
                        <i>You can upgrade to seller. Fill missing data to become seller </i>
                        <i>
                            <Nav.Link as={Link} to="/dashboard/profileEdit/edit" style={{ display: "inherit", padding: "0", textDecoration: "underline" }}>
                                click here
                            </Nav.Link>.
                        </i>
                    </div>
                </Container>
            )
        }
    }

    return <></>
}
export default WarningBuyer;