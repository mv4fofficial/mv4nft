import UpcomingIcon from '@mui/icons-material/Upcoming';
import "../../assets/css/coming-soon.css"

const ComingSoon = () => {

    return <>
        <div className='coming-soon'>
            <UpcomingIcon />
            <label>COMING SOON!</label>
        </div>
    </>
}

export default ComingSoon;