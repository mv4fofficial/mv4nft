import React from "react";
import {Col, Row} from "react-bootstrap";
// import SingleProduct from "./SingleProduct";
import NftProductCard from "../NftProductCard";
import '../../assets/css/product-list.css';
import {useEffect, useState} from "react"
import ApiRequest from '../../api_service/api_requests';


const scrollSpy = (e) => {
  // console.log(window.scrollY);
  checkCardPosition();
}

const checkCardPosition = () => {
  let cards = document.querySelectorAll(".product-list.b4-animate");

  cards.forEach(card => {
    if (card.getBoundingClientRect().top < window.scrollY + window.innerHeight) {
      card.classList.remove("b4-animate");
    }
  });
}

window.addEventListener('scroll', scrollSpy);

const ProductsList = (props) => {

  let productList = [];
  // let productWishList = []
  const [wishListProducts, setWishlistProducts] = useState([])

  if (props.state && props.state.data) {
    productList = props.state.data;
  }

  useEffect(() => {
    // code to run after render goes here
    checkCardPosition();
  });

  return (
    <Row>
      {productList.map((product, index) => {
        return (
          <Col className="product-list b4-animate" sm={4} md={4} lg={3} key={product.productId || index}>
            <NftProductCard data={product} userWishlists={wishListProducts.map(e => e.productId)} />
          </Col>
        )
      })}
    </Row>
  );
};

export default ProductsList;
