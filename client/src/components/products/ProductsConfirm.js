import Modal from '@material-ui/core/Modal';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import SimpleImageSlider from "react-simple-image-slider";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: '850px',
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

const ProductsConfirm = ({open, openModal, closeModal, data, handleSubmit}) => {
    // console.log(data);

    let imagesOrderOne = data.subProductImages.filter((image, index) => {
        return data.thumbnail == index
    })

    let imagesOrderTwo = data.subProductImages.filter((image, index) => {
        return data.thumbnail != index
    })

    let images = imagesOrderOne.concat(imagesOrderTwo);
    // console.log(images);

    return (
    <Modal
        open={open}
        onClose={closeModal}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
        className='product-modal'
    >
        <Box sx={style}>
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <table className='wp-100 desktop-view'>
                        <tbody>
                            <tr>
                                <th colSpan={4}><h4>Product Confirm</h4></th>
                            </tr>
                            <tr>
                                <td colSpan={2} rowSpan={5} className="table-image">
                                {/* <img src={( (text.includes("firebasestorage")) ? data.dbImage : "data:image/jpeg;base64,"+data.productImage)} width="200" /> */}
                                    {/* <img src={((data.productImage == null) ? data.dbImage : (((data.productImage).includes("firebasestorage")) ? "" : ((data.productImage).includes("data:image/"+data.fileType+";base64,")) ? "" : "data:image/"+data.fileType+";base64,")+data.productImage)} width="200" /> */}
                                    <SimpleImageSlider
                                        width={"374px"}
                                        height={"210px"}
                                        images={images}
                                        showBullets={true}
                                        showNavs={true}
                                        startIndex={0}
                                    />
                                </td>
                                <th>Product Name</th>
                                <td>{data.productName}</td>
                            </tr>
                            <tr>
                                <th>Category</th>
                                <td>
                                    {data.category.map((category, index) => {
                                        return (
                                            <label key={index}>{category}</label>
                                        )
                                    })}
                                </td>
                            </tr>
                            <tr>
                                <th>Sub Category</th>
                                <td>
                                    {data.subCategory.map((subCategory, index) => {
                                        return (
                                            <label key={index}>{subCategory}</label>
                                        )
                                    })}
                                </td>
                            </tr>
                            <tr>
                                <th>Quantity</th>
                                <td>{data.quantity}</td>
                            </tr>
                            <tr>
                                <th>Price</th>
                                <td>{data.price}</td>
                            </tr>
                            <tr>
                                <th>Stripe Fee</th>
                                <td>{process.env.REACT_APP_STRIPE_FEE+"%"}</td>
                                <th>NFT Amount</th>
                                <td>{parseFloat(parseFloat(data.price) - ((parseFloat(process.env.REACT_APP_STRIPE_FEE)*parseFloat(data.price)) / 100)).toFixed(2)}</td>
                            </tr>
                            <tr>
                                <th>For sale</th>
                                <td>{(data.forSale) ? "Yes" : "No" }</td>
                                <th>NFT Available</th>
                                <td>{(data.nftStatus) ? "Yes" : "No" }</td>
                            </tr>
                            <tr>
                                <th>Tags</th>
                                <td>
                                    {data.tags.map((tag, index) => {
                                        return (
                                            <>
                                                {(index > 0) ? ", " : ""}<label key={index}>{tag}</label>
                                            </>
                                        )
                                    })}
                                </td>
                                <th>Link</th>
                                <td><a href={data.link} target="_blank">Product Link</a></td>
                            </tr>
                            <tr>
                                <th>Description</th>
                                <td colSpan={3}>{data.description}</td>
                            </tr>
                            <tr>
                                <th colSpan={4} className="text-center pt-2">
                                    <Button variant="contained" color="primary" onClick={handleSubmit}>
                                        Submit
                                    </Button>
                                </th>
                            </tr>
                        </tbody>
                    </table>

                    <table className='mobile-view'>
                        <tbody>
                            <tr>
                                <th colSpan={2}><h4>Product Confirm</h4></th>
                            </tr>
                            <tr>
                                <td colSpan={2} className="table-image">
                                {/* <img src={( (text.includes("firebasestorage")) ? data.dbImage : "data:image/jpeg;base64,"+data.productImage)} width="200" /> */}
                                    {/* <img src={((data.productImage == null) ? data.dbImage : (((data.productImage).includes("firebasestorage")) ? "" : ((data.productImage).includes("data:image/"+data.fileType+";base64,")) ? "" : "data:image/"+data.fileType+";base64,")+data.productImage)} width="200" /> */}
                                    <SimpleImageSlider
                                        width={"374px"}
                                        height={"210px"}
                                        images={images}
                                        showBullets={true}
                                        showNavs={true}
                                        startIndex={0}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <th>Product Name</th>
                                <td>{data.productName}</td>
                            </tr>
                            <tr>
                                <th>Category</th>
                                <td>
                                    {data.category.map((category, index) => {
                                        return (
                                            <label key={index}>{category}</label>
                                        )
                                    })}
                                </td>
                            </tr>
                            <tr>
                                <th>Sub Category</th>
                                <td>
                                    {data.subCategory.map((subCategory, index) => {
                                        return (
                                            <label key={index}>{subCategory}</label>
                                        )
                                    })}
                                </td>
                            </tr>
                            <tr>
                                <th>Quantity</th>
                                <td>{data.quantity}</td>
                            </tr>
                            <tr>
                                <th>Price</th>
                                <td>{data.price}</td>
                            </tr>
                            <tr>
                                <th>For sale</th>
                                <td>{(data.forSale) ? "Yes" : "No" }</td>
                            </tr>
                            <tr>
                                <th>NFT Available</th>
                                <td>{(data.nftStatus) ? "Yes" : "No" }</td>
                            </tr>
                            <tr>
                                <th>Tags</th>
                                <td>
                                    {data.tags.map((tag, index) => {
                                        return (
                                            <>
                                                {(index > 0) ? ", " : ""}<label key={index}>{tag}</label>
                                            </>
                                        )
                                    })}
                                </td>
                            </tr>
                            <tr>
                                <th>Link</th>
                                <td><a href={data.link} target="_blank">Product Link</a></td>
                            </tr>
                            <tr>
                                <th>Description</th>
                                <td colSpan={3}>{data.description}</td>
                            </tr>
                            <tr>
                                <th colSpan={4} className="text-center pt-2">
                                    <Button variant="contained" color="primary" onClick={handleSubmit}>
                                        Submit
                                    </Button>
                                </th>
                            </tr>
                        </tbody>
                    </table>
                </Grid>
            </Grid>
        </Box>
        
          {/* <Typography id="product-name" sx={{ mt: 2 }}>
            {data.productName}
          </Typography>
          <Typography id="product-category" sx={{ mt: 2 }}>
            {data.category.map((category, index) => {
                // console.log(category)
                return (
                    <label key={index}>{category}</label>
                )
            })}
          </Typography>
          <Typography id="product-sub-category" sx={{ mt: 2 }}>
            {data.subCategory.map((subCategory, index) => (
                <label key={index}>{subCategory}</label>
            ))}
          </Typography> */}
    </Modal>
    )
}

export default ProductsConfirm;