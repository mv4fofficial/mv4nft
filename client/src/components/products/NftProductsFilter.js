import React, {useState, useEffect} from "react";
import {Form, Nav, NavDropdown} from "react-bootstrap";

const SubCategory = props => {
  let {subCategory, categoryId, subCategoryId, selectCategory, selectedCategories} = props;
  let category = `${categoryId}-${subCategoryId}`;
  // href="#action/3.2"
  return (
    <NavDropdown.Item className="filter-item__level2" href="#action/3.2">
      <Form.Check className="p-0" type="checkbox" checked={selectedCategories.has(category) ? true : false} id={category}
        label={subCategory.subCategoryName || "NA"}
        onClick={(event) => selectCategory(event, subCategory.subCategoryName)}
        onChange={(event) => selectCategory(event, subCategory.subCategoryName)} />
    </NavDropdown.Item>
  )
}

const Category = props => {
  let {id, category, subCategories, selectCategory, selectedCategories} = props;

  const [isShow, setIsShow] = useState(false)

  return (
    <NavDropdown autoClose={false} key={id} id={id} className="filter-item__dropdown pr-4 mr-2"
      onMouseEnter={() => setIsShow(true)}
      onMouseLeave={() => setIsShow(false)}
      show={isShow}
      title={category.categoryName || "NA"}
    >
      {subCategories.filter(e => e.categoryId === category.id).map(subCategory => {
        if (subCategory && subCategory.id) {
          return <SubCategory key={`${id}-${subCategory.id}`} categoryId={id} subCategoryId={subCategory.id}
            id={subCategory.id} subCategory={subCategory} selectCategory={selectCategory}
            selectedCategories={selectedCategories} />
        } else {
          return null;
        }
      })}
    </NavDropdown>
  )
}

const ProductsFilter = props => {
  let categories = [];
  let subCategories = [];
  let selectedCategories = new Set();

  if (props) {
    categories = props.state.categoriesList || [];
    subCategories = props.state.subCategoriesList || [];
    selectedCategories = props.state.selectedCategories ? new Set(props.state.selectedCategories) : selectedCategories;
  }

  return (
    <Nav className="mt-3 mb-3 d-flex mt-md-2 justify-content-md-start filter-nav">
      {categories.map(category => {
        if (category && category.id) {
          return <Category key={category.id} id={category.id} category={category}
            subCategories={subCategories}
            selectCategory={props.selectCategory}
            selectedCategories={selectedCategories} />
        } else {
          return null;
        }
      })}
    </Nav>
  );
};

export default ProductsFilter;
