import React, { useState, useEffect } from "react";
import Box from '@material-ui/core/Box';
import Slider from '@material-ui/core/Slider';
import Button from '@material-ui/core/Button';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';

function valuetext(data) {
    return [`${data[0] * 100}`, `${data[1] * 100}`];
}

function ProductsPriceFilter(props) {
    const [priceFrom, setPriceFrom] = useState(0)
    const [priceTo, setPriceTo] = useState(0)

    useEffect(() => {
        props.priceChange([priceFrom, priceTo])
    }, [priceFrom, priceTo])

    return (
        <div className="d-flex align-items-center position-relative mt-3 price-row">
            {/* <div className="mb-3">
                <p className="font-weight-bold mb-2">Price ( min - {valuetext(props.value.sliderValue)[0]} ~ max - {valuetext(props.value.sliderValue)[1]} ) </p>
                <Box sx={{ width: 400, margin: "0px 12px" }}>
                    <Slider
                        getAriaLabel={() => 'Minimum distance shift'}
                        getAriaValueText={valuetext}
                        value={props.value.sliderValue}
                        step={10}
                        marks
                        onChange={props.sliderHandleChange}
                        valueLabelDisplay="on"
                    />
                </Box>
            </div> */}
            <div className="mb-3">
                <p className="font-weight-bold mb-2">Price</p>
                <div className="price-container">
                    <input type="number" value={props.value.priceFrom} placeholder="Price from" onChange={(e) => setPriceFrom(e.target.value)} />
                    <span>~</span>
                    <input type="number" value={props.value.priceTo} placeholder="Price to" onChange={(e) => setPriceTo(e.target.value)} />
                </div>
            </div>
            <div className="col-md-3">
                <FormControlLabel
                    className="mt-3"
                    label="NFT Available"
                    control={(props.value.nftAvailability) ? <Switch color="primary" checked/> : <Switch color="primary"/>}
                    onChange={props.onChangeNFT}
                />
            </div>
            <div className="position-absolute right-20">
                <Button className="" variant="contained" color="primary" onClick={props.getProducts}>
                    Search
                </Button>
            </div>
        </div>
    );
};

export default ProductsPriceFilter;
