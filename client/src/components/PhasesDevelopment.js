import React, { useState } from "react";
import { Col, Row } from "react-bootstrap";
import Modal from '@material-ui/core/Modal';
import { Tabs, Tab, Box } from "@material-ui/core";
import PropTypes from "prop-types";

import "../assets/css/phase.css"


function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <p>{children}</p>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

const PhasesDevelopment = () => {

  const [open, setOpen] = useState(false);
  const closeModal = () => {
    setOpen(false)
  }

  const [value, setValue] = useState('0');
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const phases = [
    {
      title: "Phase 1",
      image: "https://firebasestorage.googleapis.com/v0/b/mv4f-marketplace.appspot.com/o/images%2Fphase%201.jpeg?alt=media&token=7cbd3c95-c336-453e-a52c-3e89c3d8ac98",
      body: [
        `Get your unique MV4F NFT! Meant to be the extension of our physical world, the MV4F NFTs are mirror representations of what you would see in the world today.<br/><br/>Our NFTs are represented by 4 major job classes that would be the building blocks of our metaverse; The Merchants (Business men/women); The Farmers (Gamers); The Creators (Artists) and The Wizards (Artists & Coders). What is more, we are transforming the light and dark we see in the world into what you will see in the Light and Dark NFT variations within each class. Whether are you a Light Farmer or a Dark Merchant, you will become the foundation of our metaverse.<br/><br/>Each job class and variation will be released in succession.<br/><br/>Join us, as we build up a whole new open world of the digital space.`
      ]
    },
    {
      title: "Phase 2",
      image: "https://firebasestorage.googleapis.com/v0/b/mv4f-marketplace.appspot.com/o/images%2FPhase%202.jpg?alt=media&token=8833c6d1-a8fb-44fc-8aef-ed474fded1b7",
      body: [
        "Your NFT, now becomes your identity in our community and metaverse. Participate in the spoils of our MV4F Marketplace as a Merchant class; be more handsomely rewarded whenever you upload and transact an asset as a Creator; Farm more wins and rewards in our game app being a Farmer; make your name and fortune through transforming our assets into the open world format in the identity of Wizard, the potentials are boundless.<br/><br/>See, and take part in the genesis of MV4F’s metaverse economy and community. Make your mark as the pioneers and pushers."
      ]
    },
    {
      title: "Phase 3",
      image: "https://firebasestorage.googleapis.com/v0/b/mv4f-marketplace.appspot.com/o/images%2FPhase%203.jpg?alt=media&token=51527e0d-47d4-4e19-8ffd-fcf8060bfa4b",
      body: [
        "Get ready as the metaverse and new world comes to life. Rock your identity proudly and get ready to be able to transfer the assets (think your outfit, skins, accessories, weapons and many more) into our other partnered metaverses."
      ]
    }
  ];

  const showDetail = (index) => {
    setValue(index)
    setOpen(true)
  }

  return (
    <>
      <Row className="phase-development">
        {
          phases.map((value, index) => (
            <Col lg={4} key={index} className="mb-3">
              <div
                data-aos="fade-up"
                data-aos-duration="600"
                data-aos-delay="200"
                className="bg-light dp-card position-relative mb-3 mt-2 h-100 b-0"
              >
                <div className="dp-card__header">
                  <img
                    className="w-100"
                    src={value.image}
                    alt={value.title}
                  />
                </div>
                <div className="dp-card__body p-3">
                  <h3 className="pb-1">{value.title}</h3>
                  <div className={(value.body.join("").length > 156) ? "phase-line-limit" : ""}>
                    {value.body.map((v, i) => (
                      <p className="m-0" key={i} dangerouslySetInnerHTML={{ __html: v }} ></p>
                    ))}
                  </div>
                  {(value.body.join("").length > 156) ? 
                      <div className="see-more" onClick={() => showDetail(index)}><label>see more</label></div>
                    : ""}
                </div>
              </div>
            </Col>
          ))
        }
      </Row>
      
      <Modal
        open={open}
        onClose={closeModal}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
        className='phase-modal'
      >
        <Col lg={4} className="mx-auto bg-white min-w-520 outline-none">
          <Box sx={{ width: '100%' }} className="pt-4">
            <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
              <Tabs value={parseInt(value)} onChange={handleChange} aria-label="basic tabs example">
                {phases.map((v, index) => (
                  <Tab key={index} label={v.title} {...a11yProps(index)} />
                ))}
              </Tabs>
            </Box>
            {phases.map((v, index) => {
              return (parseInt(value) === index) && 
              <TabPanel value={parseInt(value)} index={index} key={index}>
                <Col lg={12} id={index}>
                  <div
                    data-aos="fade-up"
                    data-aos-duration="600"
                    data-aos-delay="200"
                    className={`dp-card position-relative mb-3 mt-2 h-100 b-0`}
                  >
                    <div className="dp-card__header">
                      <img
                        className="w-100"
                        src={v.image}
                        alt={v.title}
                      />
                    </div>
                    <div className="dp-card__body p-3">
                      <h3 className="pb-1">{v.title}</h3>
                      <div>
                        {v.body.map((v, i) => (
                          <p className="m-0" key={i} dangerouslySetInnerHTML={{ __html: v }} ></p>
                        ))}
                      </div>
                    </div>
                  </div>
                </Col>
              </TabPanel>
            })}
          </Box>
        </Col>
        {/* <Col lg={4} className="margin-auto bg-white">
          <div className="user-body">
            <div className="tabs">
              {phases.map((value, index) => (
                <div key={index} id={index} className={`tab tab-${index} ${(parseInt(value) === index) ? "active" : ""}`} onClick={() => changeValue(index)}>{value.title}</div>
              ))}
            </div>

            <div className="tabs-content">
              {phases.map((value, index) => {
                return (parseInt(value) === index) && <Col lg={12} key={index} id={index}>
                  <div
                    data-aos="fade-up"
                    data-aos-duration="600"
                    data-aos-delay="200"
                    className={`dp-card position-relative mb-3 mt-2 h-100 b-0`}
                  >
                    <div className="dp-card__header">
                      <img
                        className="w-100"
                        src={value.image}
                        alt={value.title}
                      />
                    </div>
                    <div className="dp-card__body p-3">
                      <h3 className="pb-1">{value.title}</h3>
                      <div>
                        {value.body.map((v, i) => (
                          <p key={i}>{v}</p>
                        ))}
                      </div>
                      {(value.body.join("").length > 156) ? 
                          <div className="see-more" onClick={showDetail}><label>see more</label></div>
                        : ""}
                    </div>
                  </div>
                </Col>
              })}
            </div>
          </div>
        </Col> */}
      </Modal>
    </>
  );
};

export default PhasesDevelopment;
