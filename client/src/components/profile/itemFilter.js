import react, { useState } from "react"
import DownArrow from "../icons/downArrow";
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { Form, Nav, NavDropdown } from "react-bootstrap";
import ArrowDropUpIcon from '@material-ui/icons/ArrowDropUp';

const ItemFilter = ({category, subCategory, handleChange, filters}) => {

    const [showAvailability, setShowAvailability] = useState(false)
    const [showPrice, setShowPrice] = useState(false)
    const [showCategory, setShowCategory] = useState(false)
    const [showSubCategory, setShowSubCategory] = useState(false)

    const [filtersOption, setfiltersOption] = useState({
        nftAvailability: true,
        priceFrom: 0,
        priceTo: 0,
        categories: [0],
        subCategories: [0],
        tags: [
          "string"
        ],
        orderBy: 0,
        pageIndex: 0,
        pageSize: 10,
    })

    const changeFilter = (e) => {
        let filter = {...filtersOption}
        filter[e.target.getAttribute("data-name")] = e.target.getAttribute("data-value")
        setfiltersOption(filter)

        handleChange(filter)
    }
    const categoryFilter = (value) => {
        let filter = {...filtersOption}
        filter["categories"] = [value]
        setfiltersOption(filter)

        handleChange(filter)
    }
    const subCategoryFilter = (value) => {
        let filter = {...filtersOption}
        filter["categories"] = [value.categoryId]
        filter["subCategories"] = [value.id]
        setfiltersOption(filter)

        handleChange(filter)
    }
    
    // console.log(filtersOption)

    return (
        <div className="filter-container">
            <div className="filter-row filter-row-1">
                <div style={{ display: "flex", position: "relative" }}>
                    <label>Filter:</label>

                    {/* <div>
                        <select
                            id="demo-simple-select-standard"
                            onChange={handleChange}
                            name="availability"
                        >
                            <option value={0}>{"Availability"}</option>
                            <option value={true}>{"True"}</option>
                            <option value={false}>{"False"}</option>
                            
                        </select>
                    </div> */}

                    <div className="dropdown-filter">
                        <label>Availability</label>
                        <ArrowDropUpIcon className="arrow" />
                        <div className="dropdown-content left">
                            <div className={(filtersOption.nftAvailability === true) ? "selected" : ""}>
                                <span data-name="availability" data-value={true} onClick={(e) => changeFilter(e)}>True</span>
                            </div>
                            <div className={(filtersOption.nftAvailability === false) ? "selected" : ""}>
                                <span data-name="availability" data-value={false} onClick={(e) => changeFilter(e)}>False</span>
                            </div>
                        </div>
                    </div>

                    <div className="dropdown-filter">
                        <label>Price</label>
                        <ArrowDropUpIcon className="arrow" />
                        <div className="dropdown-content left">
                            <div className="dropdown-price-filter">
                                <input type="number" placeholder="Price from" data-name="priceFrom" value={filtersOption.priceFrom} onChange={(e) => changeFilter(e)} />
                            </div>
                            <p className="range-text">~</p>
                            <div className="dropdown-price-filter">
                                <input type="number" placeholder="Price to" data-name="priceTo" value={filtersOption.priceTo} onChange={(e) => changeFilter(e)}/>
                            </div>
                        </div>
                    </div>

                    <div className="dropdown-filter">
                        <label>Category</label>
                        <ArrowDropUpIcon className="arrow" />
                        <div className="dropdown-content right">
                            {category.map((cat, index) => (
                                <div key={index} className={(filtersOption.categories[0] == cat.id) ? "selected" : ""}>
                                    <span data-name="categories" data-value={cat.id} onClick={(e) => categoryFilter(cat.id)}>{cat.categoryName}</span>
                                    <div className="sub-dropdown-content right">
                                        {subCategory.map((subCat, subIndex) => {
                                            if (subCat.categoryId === cat.id) {
                                                return <div key={subIndex} className={(filtersOption.subCategories[0] == subCat.id) ? "selected" : ""}>
                                                    <span data-name="subCategories" data-value={[subCat.id]} onClick={(e) => subCategoryFilter(subCat)}>{subCat.subCategoryName}</span>
                                                </div>
                                            }
                                        })}
                                    </div>
                                </div>
                            ))}
                        </div>
                    </div>

                    {/* <div className="dropdown-filter">
                        <label>Sub Category</label>
                        <div className="dropdown-content right">
                            {subCategory.map((subCat, subIndex) => (
                                <div key={subIndex} className={(filtersOption.subCategories[0] == subCat.id) ? "selected" : ""}>
                                    <span data-name="subCategories" data-value={[subCat.id]} onClick={(e) => subCategoryFilter(subCat.id)}>{subCat.subCategoryName}</span>
                                </div>
                            ))}
                        </div>
                    </div> */}

                    {/* <div>
                        <select
                            id="demo-simple-select-standard"
                            onChange={handleChange}
                            name="category"
                        >
                            <option value={0}>{"Category"}</option>
                            {category.map((cat, index) => (
                                <option key={index} value={cat.id}>{cat.categoryName}</option>
                            ))}
                        </select>
                    </div> */}

                    {/* <div>
                        <select
                            id="demo-simple-select-standard"
                            onChange={handleChange}
                            name="subCategory"
                        >
                            <option value={0}>{"Sub Category"}</option>
                            {subCategory.map((subCat, subIndex) => (
                                <option key={subIndex} value={subCat.id}>{subCat.subCategoryName}</option>
                            ))}
                        </select>
                    </div> */}

                    {/* <Nav className="mt-3 mb-3 d-flex mt-md-2 justify-content-md-start filter-nav p-0">
                        <NavDropdown autoClose={false} className="filter-item__dropdown pr-4 mr-2 p-0" 
                            onMouseEnter={() => setShowAvailability(true)}
                            onMouseLeave={() => setShowAvailability(false)}
                            show={showAvailability}
                            title={"Availability"}
                        >
                            <NavDropdown.Item className="filter-item__level2">
                                <Form.Check className="p-0" type="checkbox"
                                    checked={availability ? true : false}
                                    name="availability"
                                    value={true}
                                    label={"True"}
                                    onChange={(e) => changeFilter(e)} />
                            </NavDropdown.Item>
                            <NavDropdown.Item className="filter-item__level2">
                                <Form.Check className="p-0" type="checkbox"
                                    checked={!availability ? true : false}
                                    name="availability"
                                    value={false}
                                    label={"False"}
                                    onChange={(e) => changeFilter(e)} />
                            </NavDropdown.Item>
                        </NavDropdown>

                        <NavDropdown autoClose={false} className="filter-item__dropdown pr-4 mr-2 p-0" 
                            onMouseEnter={() => setShowPrice(true)}
                            onMouseLeave={() => setShowPrice(false)}
                            show={showPrice}
                            title={"Price"}
                        >
                            <NavDropdown.Item className="filter-item__level2">
                                <input type="number" />
                            </NavDropdown.Item>
                            <NavDropdown.Item className="filter-item__level2">
                                <input type="number" />
                            </NavDropdown.Item>
                        </NavDropdown>

                        {/* <NavDropdown autoClose={false} className="filter-item__dropdown pr-4 mr-2 p-0" 
                            onMouseEnter={() => setShowCategory(true)}
                            onMouseLeave={() => setShowCategory(false)}
                            show={showCategory}
                            title={"Category"}
                        >
                            <NavDropdown.Item className="filter-item__level2">
                                <Form.Check className="p-0" type="checkbox"
                                    label={"True"}
                                    value={true}
                                    // onClick={handleChange}
                                />
                            </NavDropdown.Item>
                            <NavDropdown.Item className="filter-item__level2">
                                <Form.Check className="p-0" type="checkbox"
                                    label={"False"}
                                    value={false}
                                    // onClick={handleChange}
                                />
                            </NavDropdown.Item>
                        </NavDropdown>

                        <NavDropdown autoClose={false} className="filter-item__dropdown pr-4 mr-2 p-0" 
                            onMouseEnter={() => setShowSubCategory(true)}
                            onMouseLeave={() => setShowSubCategory(false)}
                            show={showSubCategory}
                            title={"Sub Category"}
                        >
                            <NavDropdown.Item className="filter-item__level2">
                                <Form.Check className="p-0" type="checkbox"
                                    label={"True"}
                                    value={true}
                                    // onClick={handleChange}
                                />
                            </NavDropdown.Item>
                            <NavDropdown.Item className="filter-item__level2">
                                <Form.Check className="p-0" type="checkbox"
                                    label={"False"}
                                    value={false}
                                    // onClick={handleChange}
                                />
                            </NavDropdown.Item>
                        </NavDropdown>
                    </Nav> */}

                    {/* // <label key={index} id={index} onClick={}>{value} <DownArrow /></label> */}
                        
                </div>
                <div className="d-flex">
                    <label>Sort By:</label>
                    <div className="dropdown-filter">
                        <label>Order</label>
                        <div className="dropdown-content left" style={{ right: "0" }}>
                            <div className={(filtersOption.order == "Alphabetically, A~Z") ? "selected" : ""}>
                                <span data-name="orderBy" data-value={"Alphabetically ASC"} onClick={(e) => changeFilter(e)}>{"Alphabetically, A~Z"}</span>
                            </div>
                            <div className={(filtersOption.order == "Alphabetically, Z~A") ? "selected" : ""}>
                                <span data-name="orderBy" data-value={"Alphabetically DESC"} onClick={(e) => changeFilter(e)}>{"Alphabetically, Z~A"}</span>
                            </div>
                            <div className={(filtersOption.order == "Price ASC") ? "selected" : ""}>
                                <span data-name="orderBy" data-value={"price ASC"} onClick={(e) => changeFilter(e)}>{"Price ASC"}</span>
                            </div>
                            <div className={(filtersOption.order == "Price DESC") ? "selected" : ""}>
                                <span data-name="orderBy" data-value={"price DESC"} onClick={(e) => changeFilter(e)}>{"Price DESC"}</span>
                            </div>
                            <div className={(filtersOption.order == "Date ASC") ? "selected" : ""}>
                                <span data-name="orderBy" data-value={"createdDate ASC"} onClick={(e) => changeFilter(e)}>{"Date ASC"}</span>
                            </div>
                            <div className={(filtersOption.order == "Date DESC") ? "selected" : ""}>
                                <span data-name="orderBy" data-value={"createdDate DESC"} onClick={(e) => changeFilter(e)}>{"Date DESC"}</span>
                            </div>
                        </div>
                    </div>
                    
                    {/* <select
                        id="demo-simple-select-standard"
                        onChange={handleChange}
                        name="order"
                    >
                        <option value={1}>{"Alphabetically, A~Z"}</option>
                        <option value={2}>{"Alphabetically, Z~A"}</option>
                        <option value={3}>{"Price ASC"}</option>
                        <option value={4}>{"Price DESC"}</option>
                        <option value={5}>{"Date ASC"}</option>
                        <option value={6}>{"Date DESC"}</option>
                    </select> */}
                </div>
            </div>
        </div>
    )
}

export default ItemFilter;