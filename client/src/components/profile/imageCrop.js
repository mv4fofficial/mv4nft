import React, { useState, useCallback } from 'react'
import Cropper from 'react-easy-crop'
import Button from '@material-ui/core/Button'
import { withStyles } from '@material-ui/core/styles'
import { getOrientation } from 'get-orientation/browser'
import { getCroppedImg, getRotatedImage } from './canvasUtils'
import { styles } from './styles';
import PropTypes from 'prop-types';
import { styled } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import { Icon } from '@material-ui/core'

const ORIENTATION_TO_ANGLE = {
  '3': 90,
  '6': 90,
  '8': -90,
}

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
    '& .MuiDialogContent-root': {
      padding: theme.spacing(2),
    },
    '& .MuiDialogActions-root': {
      padding: theme.spacing(1),
    },
  }));

const BootstrapDialogTitle = (props) => {
    const { children, onClose, ...other } = props;
  
    return (
      <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
        {children}
        {onClose ? (
          <IconButton
            aria-label="close"
            onClick={onClose}
            sx={{
              position: 'absolute',
              right: 8,
              top: 8,
              color: (theme) => theme.palette.grey[500],
            }}
          >
            <CloseIcon />
          </IconButton>
        ) : null}
      </DialogTitle>
    );
};
  
BootstrapDialogTitle.propTypes = {
    children: PropTypes.node,
    onClose: PropTypes.func.isRequired,
};

const ImageCrop = ({ classes, title, ratioLabel, image, open, setShowCropModal, handleClose, width, height, setCroppedProfile, setCroppedBanner }) => {
    const [imageSrc, setImageSrc] = useState(image)
    const [crop, setCrop] = useState({ x: 0, y: 0 })
    const [rotation, setRotation] = useState(0)
    const [zoom, setZoom] = useState(1)
    const [croppedAreaPixels, setCroppedAreaPixels] = useState(null)
    const [croppedImage, setCroppedImage] = useState(null)

    const onCropComplete = useCallback((croppedArea, croppedAreaPixels) => {
        setCroppedAreaPixels(croppedAreaPixels)
    }, [])

    const showCroppedImage = useCallback(async () => {
        try {
            const croppedImage = await getCroppedImg(
                imageSrc,
                croppedAreaPixels,
                rotation
            )
            // console.log('donee')
            // console.log(croppedImage)

            setCroppedImage(croppedImage.base64)
            if (width == height) {
                setCroppedProfile(croppedImage.base64)
            } else {
                setCroppedBanner(croppedImage.base64)
            }
        } catch (e) {
            console.error(e)
        }
        setShowCropModal(false)
        setImageSrc(null)
    }, [imageSrc, croppedAreaPixels, rotation])

    const onClose = useCallback(() => {
        setCroppedImage(null)
        if (width == height) {
            setCroppedProfile(null)
        } else {
            setCroppedBanner(null)
        }
    }, [])

    const onFileChange = async (e) => {
        if (e.target.files && e.target.files.length > 0) {
            const file = e.target.files[0]
            let imageDataUrl = await readFile(file)

            // apply rotation if needed
            const orientation = await getOrientation(file)
            const rotation = ORIENTATION_TO_ANGLE[orientation]
            if (rotation) {
                imageDataUrl = await getRotatedImage(imageDataUrl, rotation)
            }

            setImageSrc(imageDataUrl)
            setZoom(1)
        }
    }

    const closeModalBox = () => {
        setImageSrc("")
        handleClose(false)
    }

    const downZoom = () => {
        if (zoom > 1) {
            setZoom(zoom - 0.2)
        }
    }

    const upZoom = () => {
        if (zoom < 3) {
            setZoom(zoom + 0.2)
        }
    }

    return (
        <div className='image-crop-container'>
            <BootstrapDialog
                onClose={() => closeModalBox()}
                aria-labelledby="customized-dialog-title"
                open={open}
            >
                <BootstrapDialogTitle id="customized-dialog-title" onClose={() => closeModalBox()}>
                    {title}
                </BootstrapDialogTitle>
                <DialogContent dividers>
                    <div className='mb-2'>
                        <i className='text-default'>Best resolution use {ratioLabel}</i>
                    </div>
                {imageSrc ? (
                    <React.Fragment>
                    <div className={classes.cropContainer}>
                        <Cropper
                            image={imageSrc}
                            crop={crop}
                            rotation={rotation}
                            zoom={zoom}
                            zoomWithScroll={true}
                            restrictPosition={true}
                            aspect={width / height}
                            cropShape={(width === height) ? "round" : "rect" }
                            onCropChange={setCrop}
                            onRotationChange={setRotation}
                            onCropComplete={onCropComplete}
                            onZoomChange={setZoom}
                        />
                    </div>
                    <div className='mt-2'>
                        {zoom > 1 ? <Icon onClick={downZoom} className="pb-1 mr-2 bg-secondary text-white resize-icon minus">-</Icon> : <Icon className="pb-1 mr-2 bg-secondary text-white resize-icon minus disabled">-</Icon>}
                        {zoom < 3 ? <Icon onClick={upZoom}  className="px-2 pb-1 mr-2 bg-secondary text-white resize-icon plus">+</Icon> : <Icon className="px-2 pb-1 mr-2 bg-secondary text-white resize-icon disabled plus">+</Icon>}
                        <i>Scroll can zoom in and out.</i>
                    </div>
                    <div className={"justify-content-center "+classes.controls}>
                        <Button
                            onClick={() => setImageSrc("")}
                            variant="contained"
                            color="default"
                            classes={{ root: classes.cropButton }}
                            className="mr-2 crop-image-btn"
                        >
                        Cancel
                        </Button>
                        <Button
                            onClick={showCroppedImage}
                            variant="contained"
                            color="primary"
                            classes={{ root: classes.cropButton }}
                            width={95}
                            className="crop-image-btn"
                        >
                        OK
                        </Button>
                    </div>
                    {/* <ImgDialog img={croppedImage} onClose={onClose} /> */}
                    </React.Fragment>
                ) : (
                    <div className='photo-picker'>
                        <input id="file" type="file" onChange={onFileChange} accept="image/*" />
                        <label htmlFor='file'>Pick Photo</label>
                    </div>
                    
                )}
                </DialogContent>
            </BootstrapDialog>
        </div>
    )
}

function readFile(file) {
    return new Promise((resolve) => {
        const reader = new FileReader()
        reader.addEventListener('load', () => resolve(reader.result), false)
        reader.readAsDataURL(file)
    })
}

export default withStyles(styles)(ImageCrop)
// const StyledDemo = withStyles(styles)(Demo)

// const rootElement = document.getElementById('root')
// ReactDOM.render(<StyledDemo />, rootElement)