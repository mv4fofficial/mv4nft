import React, { useState, useCallback } from 'react'
import { withStyles } from '@material-ui/core/styles'
import { styles } from './styles';
import PropTypes from 'prop-types';
import { styled } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import { Link } from "react-router-dom";

const ORIENTATION_TO_ANGLE = {
  '3': 90,
  '6': 90,
  '8': -90,
}

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
    '& .MuiDialogContent-root': {
      padding: theme.spacing(2),
    },
    '& .MuiDialogActions-root': {
      padding: theme.spacing(1),
    },
  }));

const BootstrapDialogTitle = (props) => {
    const { children, onClose, ...other } = props;
  
    return (
      <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
        {children}
        {onClose ? (
          <IconButton
            aria-label="close"
            onClick={onClose}
            sx={{
              position: 'absolute',
              right: 8,
              top: 8,
              color: (theme) => theme.palette.grey[500],
            }}
          >
            <CloseIcon />
          </IconButton>
        ) : null}
      </DialogTitle>
    );
};
  
BootstrapDialogTitle.propTypes = {
    children: PropTypes.node,
    onClose: PropTypes.func.isRequired,
};

const UserFollowList = ({ title, data, open, handleClose }) => {
    
    // console.log("Follow list", data)

    const closeModalBox = () => {
        handleClose(false)
    }

    return (
        <div className='image-crop-container'>
            <BootstrapDialog
                onClose={() => closeModalBox()}
                aria-labelledby="customized-dialog-title"
                open={open}
            >
                <BootstrapDialogTitle id="customized-dialog-title" onClose={() => closeModalBox()}>
                    {title}
                </BootstrapDialogTitle>
                <DialogContent dividers>
                    <ul style={{listStyle: "none"}}>
                        {(data).map((value, index) => {
                            if (value.hasOwnProperty("User")) {
                              return <li className={(index < data.length - 1) ? "my-2 border-bottom pb-2" : "my-2 pb-2"} key={index}>
                                <Link to={`/dashboard/seller-profile/${value.User.Id}`} style={{cursor: "pointer"}}>
                                  <img src={value.User.ProfilePicture} width={45} height={45} style={{borderRadius: "50%"}}/>
                                  <label className='ml-2' style={{cursor: "pointer"}}>{value.User.FirstName+((value.User.LastName != null) ? " "+value.User.LastName : "")}</label>
                                </Link>
                              </li>
                            } else {
                              return <li className={(index < data.length - 1) ? "my-2 border-bottom pb-2" : "my-2 pb-2"} key={index}>
                                <Link to={`/dashboard/seller-profile/${value.Id}`} style={{cursor: "pointer"}}>
                                  <img src={value.ProfilePicture} width={45} height={45} style={{borderRadius: "50%"}}/>
                                  <label className='ml-2' style={{cursor: "pointer"}}>{value.FirstName+((value.LastName != null) ? " "+value.LastName : "")}</label>
                                </Link>
                              </li>
                            }
                            
                          })}
                    </ul>
                </DialogContent>
            </BootstrapDialog>
        </div>
    )
}

export default withStyles(styles)(UserFollowList)