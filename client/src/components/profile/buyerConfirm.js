import React, { useState, useCallback } from 'react'
import Button from '@material-ui/core/Button'
import { withStyles } from '@material-ui/core/styles'
import { styles } from './styles';
import PropTypes from 'prop-types';
import { styled } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
    '& .MuiDialogContent-root': {
      padding: theme.spacing(2),
    },
    '& .MuiDialogActions-root': {
      padding: theme.spacing(1),
    },
  }));

const BootstrapDialogTitle = (props) => {
    const { children, onClose, ...other } = props;
  
    return (
      <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
        {children}
        {onClose ? (
          <IconButton
            aria-label="close"
            onClick={onClose}
            sx={{
              position: 'absolute',
              right: 8,
              top: 8,
              color: (theme) => theme.palette.grey[500],
            }}
          >
            <CloseIcon />
          </IconButton>
        ) : null}
      </DialogTitle>
    );
};
  
BootstrapDialogTitle.propTypes = {
    children: PropTypes.node,
    onClose: PropTypes.func.isRequired,
};

const BuyerConfirm = ({ open, handleClose, setSellerConfirm, data }) => {
    return (
        <div className='image-crop-container'>
            <BootstrapDialog
                onClose={() => handleClose(false)}
                aria-labelledby="customized-dialog-title"
                open={open}
            >
                <BootstrapDialogTitle id="customized-dialog-title" onClose={() => handleClose(false)}>
                    {(data) ? data.title :"Confirmation"}
                </BootstrapDialogTitle>
                <DialogContent dividers>
                    <React.Fragment>
                        <p>{(data) ? data.msg :"Your are qualify for seller role. Do u want to upgrade?"}</p>
                        <div className='d-flex justify-content-end'>
                            <Button
                                type="button"
                                variant="contained"
                                color="default"
                                onClick={() => setSellerConfirm(false)}
                            >
                                No
                            </Button>
                            <Button
                                type="button"
                                className='ml-2'
                                variant="contained"
                                color="primary"
                                onClick={() => setSellerConfirm(true)}
                            >
                                Yes
                            </Button>
                        </div>
                    </React.Fragment>
                </DialogContent>
            </BootstrapDialog>
        </div>
    )
}

export default withStyles(styles)(BuyerConfirm)