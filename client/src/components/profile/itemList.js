import ProductCard from "../ProductCard";
import { Col } from "react-bootstrap";

const ItemList = ({ data, userWishlists }) => {
    // console.log(data)
    // console.log(userWishlists)
    return (
        <div className="image-lists" col={12}>
            {(data.length > 0) && data.map((value, index) => (
                <Col lg={3} md={4} sm={6} xs={12} key={index}>
                    <ProductCard key={index} data={value} page={"profile"} userWishlists={userWishlists} />
                </Col>
            ))}
        </div>
    )
}
export default ItemList;