import StaticData from "./data/constants"
import packageJson from '../../package.json';

export const Logo = () => (
  <div style={{display: "flex", alignItems: "flex-end"}}>
    <img
      src={StaticData.logo}
      alt="logo"
      width={100}
    />
    {/* <div> */}
    <i style={{verticalAlign: "bottom", fontSize: "10px", color: "#fff"}}><small className="version-label">{process.env.REACT_APP_VERSION}</small></i>
    {/* </div> */}
  </div>
)
