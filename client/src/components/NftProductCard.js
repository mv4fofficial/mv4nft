import React from "react";
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import {withAlert} from "react-alert";
import {updateCartCount} from "../user";
import ApiRequest from "../api_service/api_requests";
import * as Constants from "../constant";

import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Avatar from "@material-ui/core/Avatar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import {blue, purple, red} from "@material-ui/core/colors";
import FavoriteIcon from "@material-ui/icons/Favorite";
import {useState, useEffect} from "react";
import ShareIcon from "@material-ui/icons/Share";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import Button from "@material-ui/core/Button";
import ShoppingBasket from "@material-ui/icons/ShoppingBasket";

import userProfileImage from "../assets/img/user.png";

import {Store} from "../store";

const addToCart = (event, props) => {
  // console.log(props.data.productId)
  if (props.data.productId && !isNaN(props.data.productId)) {
    ApiRequest.addToCart(Number(props.data.productId))
      .then((res) => {
        if (res && res.message) {
          if (res.statusCode && res.statusCode === 200) {
            props.alert.success(res.message);
            getCartItems(props);
          } else {
            props.alert.error(res.message);
          }
        } else {
          props.alert.error("Something went wrong");
        }
      })
      .catch((err) => {
        // console.log("Add cart api error, ", err);
      });
  } else {
    // console.log("Product ID not found");
  }
};

const getCartItems = (props) => {
  ApiRequest.viewCartItems()
    .then((res) => {
      if (res && res.data && res.statusCode && res.statusCode === 200) {
        props.dispatch(updateCartCount(res.data.length || 0));
      }
    })
    .catch((err) => {
      // console.log("View Cart api error, ", err);
    });
};

const ProductCard = (props) => {
  const [wishListToggle, setWishListToggle] = useState(false);
  const [isDisable, setIsDisabled] = useState(false);
  // const [wishListProducts, setWishlistProducts] = useState([])

  let productId = "";
  let productName = "";
  let productPicture =
    "https://cdn.shopify.com/s/files/1/0609/6152/1863/products/DiRenJie_300x.png";
  let seller;
  let sellerId;
  let price = 0;
  let createdDate = "";
  let description = "";

  const addProductWishList = (productId) => {
    setIsDisabled(true);
    ApiRequest.addProductWishList({productId})
      .then((res) => {
        if (res && res.statusCode && res.statusCode == 200) {
          // alert("this product successfully added to your wishlist")
          setWishListToggle(true);
          setIsDisabled(false);
        }
      })
      .catch((err) => {
        // console.log("add product wishlist error, ", err);
        setIsDisabled(false);
      });

    // console.log("hello");
  };

  const deleteProductWishList = (productId) => {
    setIsDisabled(true);
    ApiRequest.deleteProductWishList({productId})
      .then((res) => {
        if (res && res.statusCode && res.statusCode == 200) {
          // alert("this product remove from your wishlist")
          setWishListToggle(false);
          setIsDisabled(false);
        }
      })
      .catch((err) => {
        // console.log("add product wishlist error, ", err);
        setIsDisabled(false);
      });

    // console.log("hello");
  };
  // let gasFee;
  // let seller;
  if (props && props.data) {
    productId = props.data.token_id || "";
    productName = props.data.name || "";
    productPicture = props.data.image_url;
    // productPicture = props.data.productPicture ? `${Constants.IMG_PREFIX}${props.data.productPicture}` : productPicture;
    seller = props.data.user || "";
    sellerId = props.data.sellerId || "";
    price = props.data.orders != null ? props.data.orders.sell_orders[0].buy_quantity : "";
    createdDate = new Date(props.data.created_at) || "";
    // gasFee = props.data.gasFee || '';
    // seller = props.data.seller || '';
  }
  useEffect(() => {
    if (typeof props.userWishlists != "undefined" &&
      props.userWishlists.includes(productId)
    ) {
      setWishListToggle(true);
    }
  }, [productId]);

  // console.log(props.data);

  return (
    <Card className="mb-4 card-color" sx={{maxWidth: 345}}>
      {/* {(!props.hasOwnProperty("page")) &&  */}
      <CardHeader
        avatar={
          <Avatar sx={{bgcolor: red[500]}} aria-label="recipe">
            <img src={userProfileImage} alt="user-profile" width="100%" />
          </Avatar>
        }
        // action={
        //   <IconButton aria-label="settings">
        //     <MoreVertIcon />
        //   </IconButton>
        // }
        title={
          (
            <Link
              to={`/dashboard/seller-profile/${sellerId}`}
              className="product-card__body--seller"
            >
              {`${seller.substring(0, 6)}...${seller.substring(seller.length - 4)}`}
            </Link>
          ) || "NA"
        }
        subtitle={""}
      />
      {/* } */}


      <Link to={`/dashboard/nft-product-detail/${productId}`}>
        <CardMedia
          component="img"
          height="194"
          image={productPicture}
          alt="Paella dish"
        />
      </Link>
      <CardContent>
        <Typography variant="body2">
          {
            <Link to={`/dashboard/product-detail/${productId}`}>
              {" "}
              {productName}{" "}
            </Link>
          }
        </Typography>
        <Typography variant="body2">
          {createdDate.toLocaleString("default", {month: "long"}) +
            " " +
            createdDate.getDate() +
            ", " +
            createdDate.getFullYear()}
        </Typography>
        <Typography variant="body2">ETH {price / 1000000000000000000}</Typography>
      </CardContent>
      <CardActions disableSpacing>
        {/* <IconButton aria-label="share">
          <ShareIcon />
        </IconButton> */}
        {/* <IconButton color="default" aria-label="add to cart" id={productId} onClick={event => {
              event.preventDefault();
              addToCart(event, props);
            }}>
          <ShoppingBasket />
        </IconButton> */}
      </CardActions>
    </Card>
    // <div className="product-card p-2">
    //   <div className="product-card__header">
    //     <img
    //       className="w-100 product-img"
    //       src={productPicture}
    //       alt="product"
    //     />
    //     <div className="btn-div position-absolute">
    //       <Button
    //         id={productId}
    //         className=" rounded-0 text-capitalize"
    //         variant="light"
    //         onClick={event => {
    //           event.preventDefault();
    //           addToCart(event, props);
    //         }}>
    //         Add to cart
    //       </Button>
    //     </div>
    //   </div>
    //   <div className="product-card__body">
    //     <div className="product-card__body--name">
    //       <Link to={`/dashboard/product-detail/${productId}`}> {productName} </Link>
    //     </div>
    //     <div className="product-card__body--price">${price} GSD</div>

    //     <Link to={`/dashboard/seller-profile/${sellerId}`} className="product-card__body--seller">
    //       {seller || 'NA'}
    //     </Link>
    //   </div>
    //   <div className="mt-4"></div>
    // </div>
  );
};

const mapStateToProps = (state) => {
  return {
    user: state.userReducer,
  };
};

export default withAlert()(connect(mapStateToProps)(ProductCard));
