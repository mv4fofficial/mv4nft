import React from "react";
import Slider from "react-slick";
import DiscoverItem from "./DiscoverItem";

const DiscoverMoreSlider = () => {
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1000,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };
  const styles = {
    marginLeft: "-0.5rem",
    marginRight: "-0.5rem"
  };
  return (
    <div style={styles}>
      <Slider {...settings}>
        <DiscoverItem />
        <DiscoverItem />
        <DiscoverItem />
      </Slider>
    </div>
  );
};

export default DiscoverMoreSlider;
