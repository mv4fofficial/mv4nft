import React from "react";
import {
  Button,
  Col,
  Container,
  Form,
  FormControl,
  Row
} from "react-bootstrap";

const SubscribeUs = () => {
  return (
    <div className="subscribe-us position-relative d-flex align-items-center">
      <div className="sub-overlay position-absolute"></div>
      <div className="subscribe-us__content w-100">
        <Container className="h-100">
          <Row className="h-100 align-items-center">
            <Col lg={7} className="mx-auto">
              <div
                data-aos="fade-up"
                data-aos-duration="700"
                // data-aos-delay="200"
              >
                <h2 className="">Subscribe to our updates</h2>
                <p>
                  Get informed about the hottest trends and assets available
                </p>
              </div>

              <Form
                data-aos="zoom-in-up"
                data-aos-duration="900"
                data-aos-delay="400"
                className="d-flex"
              >
                <FormControl
                  className="bg-light rounded-0"
                  placeholder="Email"
                />
                <Button type="submit" className="rounded-0 px-4">
                  Subcribe
                  <svg
                    stroke="currentColor"
                    fill="currentColor"
                    strokeWidth="0"
                    viewBox="0 0 16 16"
                    height="1em"
                    width="1em"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      fillRule="evenodd"
                      d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z"
                    ></path>
                  </svg>
                </Button>
              </Form>
            </Col>
          </Row>
        </Container>
      </div>
    </div>
  );
};

export default SubscribeUs;
