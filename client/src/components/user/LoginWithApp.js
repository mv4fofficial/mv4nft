import {signInWithGoogle, signInWithFacebook} from "../../firebase"
import {Facebook, Google} from "@mui/icons-material";

// import Stack from '@material-ui/types/Stack';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';

import ApiRequest from "../../api_service/api_requests";
import { userDataUpdate, loginStatusUpdate } from "../../user";
import { URLS } from "../../constant";
import '../../assets/css/app-login.css';

const continueWithGoogle = (props) => {
  // console.log(props);
  signInWithGoogle((result) => {
    // console.log("Google login return data from firebase", result)

    let obj = {
      uuid: result.user.uid,
      firstName: result.user.providerData[0].displayName,
      profilePicture: result.user.providerData[0].photoURL,
      emailId: result.user.providerData[0].email,
      isEmailVerified: true,
      providerId: result.user.providerData[0].providerId,
    }

    // console.log("Data to login or register", obj)
    ApiRequest.thirdPartyRegisterAndLogin(obj)
      .then(res => {
        // console.log("Google login return data from api", res)

        if (res && res.data && res.statusCode && res.statusCode === 200) {
          //props.dispatch(loginStatusUpdate(true));

          props.dispatch(userDataUpdate(res.data));
          props.dispatch(loginStatusUpdate(true));

          if (props && props.match && props.match.params &&
            props.match.params.page_number && URLS[props.match.params.page_number]) {
            props.history.push(URLS[Number(props.match.params.page_number)]);
            return
          }
          props.alert.success("You are successfully login!");
          props.history.push('/dashboard');
        }
      })
      .catch((err) => {
        // console.log('Login api error, ', err);
      })
  })
}

const continueWithFacebook = (props) => {
  signInWithFacebook((result) => {
    // console.log("Facebook login return data from firebase", result)

    let obj = {
      uuid: result.user.uid,
      firstName: result.user.providerData[0].displayName,
      profilePicture: result.user.providerData[0].photoURL,
      emailId: result.user.providerData[0].email,
      isEmailVerified: true,
      providerId: result.user.providerData[0].providerId,
    }

    // console.log("Data to login or register", obj)
    ApiRequest.thirdPartyRegisterAndLogin(obj)
      .then(res => {
        // console.log("Facebook login return data from api", res)

        if (res && res.data && res.statusCode && res.statusCode === 200) {

          props.dispatch(userDataUpdate(res.data));
          props.dispatch(loginStatusUpdate(true));
          
          if (props && props.match && props.match.params &&
            props.match.params.page_number && URLS[props.match.params.page_number]) {
            props.history.push(URLS[Number(props.match.params.page_number)]);
            return
          }
          props.alert.success("You are successfully login!");
          props.history.push('/dashboard');
        }
      })
      .catch((err) => {
        // console.log('Login api error, ', err);
      })
  })
}

const LoginWithApp = ({props}) => {
  return (
    <div className="mt-5">
      <label>Login with</label>
      <div className="d-flex justify-content-center">
        <Tooltip title="Google">
          <IconButton aria-label="Google login" className="app-icon google-icon" onClick={() => continueWithGoogle(props)}>
            <Google />
          </IconButton>
        </Tooltip>
        <Tooltip title="Facebook">
          <IconButton aria-label="Facebook Login" className="app-icon facebook-icon" onClick={() => continueWithFacebook(props)}>
            <Facebook />
          </IconButton>
        </Tooltip>
      </div>
    </div>

  )
}

export default LoginWithApp;
