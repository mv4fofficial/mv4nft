import React from 'react';
import Loader from 'react-loader-advanced';
import '../assets/css/loader.css';

function LoaderComp(props) {
    let message = <div style={{fontSize: '1.2rem'}}><div className='loader' />{props.message || "Loading"}</div>;
    let backgroundStyle = { position: 'fixed' };
    let messageStyle = { fontSize: '2rem' };

    return (
        <div>
            <div className={"loader-container "+((props.show) ? "show" : "hide")}>
                <h2 className={"title "+(props.show || false)}>
                    <span className="title-word title-word-1">M</span>
                    <span className="title-word title-word-2">V</span>
                    <span className="title-word title-word-3">4</span>
                    <span className="title-word title-word-4">F</span>
                </h2>
            </div>
            {props.children}
        </div>
        // <Loader show={props.show || false} message={message} backgroundStyle={backgroundStyle} messageStyle={messageStyle}>
        //     {props.children}
        // </Loader>
    )
}

export default LoaderComp;