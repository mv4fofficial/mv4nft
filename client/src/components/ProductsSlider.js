import React, { useState, useEffect } from "react";
import Slider from "react-slick";
import { Row } from "react-bootstrap";
import ApiRequest from '../api_service/api_requests';
import { Col } from "react-bootstrap";
import { Carousel } from "react-bootstrap";

import ProductCard from "./ProductCard";

const settings = {
  dots: true,
  infinite: true,
  speed: 500,
  slidesToShow: 4,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 748,
      settings: {
        slidesToShow: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
};
const styles = {
  marginLeft: "-0.5rem",
  marginRight: "-0.5rem"
};

const ProductsSlider = props => {
  const [data, setData] = useState([]);

  useEffect(() => {
    let payload = {
      categories: props.categories || [],
      subCategories: props.subCategories || [],
      tags: [],
      priceFrom: 0,
      priceTo: 0,
      nftAvailability: true,
      filter: "",
      pageIndex: 1,
      pageSize: 4
    };

    ApiRequest.getProducts(payload)
      .then(res => {
        // console.log(res.data.data)
        if (res && res.data && res.data.data) {
          setData(res.data.data);
        }
      })
      .catch((err) => {
        // console.log('Products fetch api error, ', err);
      })
  }, []);

  return (data.length > 0 ?
    <div style={styles}>
      {/* To Replace with Slider */}
      {/* <Row  {...settings}> */}
      <Row className="col-12">
        {data.map((object, index) => <Col lg={3} md={3} sm={4} key={index}><ProductCard data={object} /></Col>)}
      </Row>
    </div>
    : <Col md={8} lg={9} xl={10} className="no-products-text">
      <span>No Products found</span></Col>
  );
};

export default ProductsSlider;
