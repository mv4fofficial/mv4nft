import React, {useState, useEffect} from "react";
import {ImmutableXClient, ImmutableMethodResults, ERC721TokenType, ETHTokenType, ImmutableRollupStatus} from '@imtbl/imx-sdk';
import {connect} from "react-redux";
import {Button, Container, Form, Nav, Navbar, Modal} from "react-bootstrap";
import {Link} from "react-router-dom";
import useWindowDimensions from "../hooks/useWindowDismension";
import SearchModal from "./search/SearchModal";
import Logout from "./Logout";
import {updateCartCount} from "../user";
import ApiRequest from "../api_service/api_requests";
import "../assets/css/dashboard.css";
import WarningBuyer from "./noti/WarningBuyer";
import {Logo} from "./Logo";
import TextField from '@material-ui/core/TextField';
import UserIcon from "./icons/defaultUser";
import Box from '@material-ui/core/Box';
import Avatar from '@material-ui/core/Avatar';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
// import LogoutIcon from '@material-ui/icons/Logout';
import ArrowDropUpIcon from '@material-ui/icons/ArrowDropUp';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEye } from '@fortawesome/free-solid-svg-icons'


const AppNav = (props) => {
  const {width} = useWindowDimensions();
  const [mobileMenu, setMobileMenu] = useState(false);
  const [show, setShow] = useState(false);
  const [showBalance, setShowBalance] = useState(true);
  const [showDepositModal, setShowDepositModal] = useState(false);
  const [depositAmount, setDepositAmount] = useState(0);
  const [showWithdrawModel, setShowWithdrawModal] = useState(false);
  const [withdrawAmount, setWithdrawAmount] = useState(0);
  const [notifications, setNotifications] = useState(null);
  const hideModal = () => {
    setShow(false);
  };


  const handleMenuClick = () => {
    setMobileMenu(!mobileMenu);
  };

  let isSeller = false;

  if (
    props &&
    props.user &&
    props.user.roles &&
    props.user.roles[0] &&
    props.user.roles[0] === "Seller"
  ) {
    isSeller = true;
  }
  if (
    props &&
    props.user &&
    props.user.roles &&
    props.user.roles[1] &&
    props.user.roles[1] === "Seller"
  ) {
    isSeller = true;
  }
  if (
    props &&
    props.user &&
    props.user.roles &&
    props.user.roles[2] &&
    props.user.roles[2] === "Seller"
  ) {
    isSeller = true;
  }

  let isBuyer = false;

  if (
    props &&
    props.user &&
    props.user.roles &&
    props.user.roles[0] &&
    props.user.roles[0] === "Buyer"
  ) {
    isBuyer = true;
  }
  if (
    props &&
    props.user &&
    props.user.roles &&
    props.user.roles[1] &&
    props.user.roles[1] === "Buyer"
  ) {
    isBuyer = true;
  }
  if (
    props &&
    props.user &&
    props.user.roles &&
    props.user.roles[2] &&
    props.user.roles[2] === "Buyer"
  ) {
    isBuyer = true;
  }

  let isLogged = false;
  if (props && props.user && props.user.isLogged) {
    isLogged = true;
  }

  const [loginUser, setLoginUser] = useState({});

  useEffect(() => {
    if (isBuyer) {
      ApiRequest.viewCartItems()
        .then((res) => {
          if (res && res.data && res.statusCode && res.statusCode === 200) {
            props.dispatch(updateCartCount(res.data.length || 0));
          }
        })
        .catch((err) => {
          // console.log("View Cart api error, ", err);
        });
    }

    if (isSeller) {
      ApiRequest.getSellerNotifications().then((res) => {
        // console.log("Seller notifications", res);
        setNotifications(res.data.length);
      });
    }

    if (isLogged) {
      ApiRequest.viewUser()
        .then(res => {
          if (res && res.data && res.statusCode && res.statusCode === 200) {
            setLoginUser(res.data);
          } else {
            if (res && res.message) {
              this.props.alert.error("Error fetching user details");
            }
          }
        })
        .catch((err) => {
          // console.log('Error fetching user details, ', err);
        })
    }
    props.getBalance();
    setShowBalance((localStorage.getItem('SHOW_BALANCE') == 'true' ? true : false));
  }, []);

  const [anchorEl, setAnchorEl] = React.useState(null);

  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const paperProps = {
    elevation: 0,
    sx: {
      overflow: 'visible',
      filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',
      mt: 1.5,
      '& .MuiAvatar-root': {
        width: 22,
        height: 22,
        ml: -0.5,
        mr: 1,
      },
      '&:before': {
        content: '""',
        display: 'block',
        position: 'absolute',
        top: 0,
        right: 14,
        width: 10,
        height: 10,
        bgcolor: 'background.paper',
        transform: 'translateY(-50%) rotate(45deg)',
        zIndex: 0,
      },
    },
  };

  // console.log(loginUser);

  // deposit eth
  const depositETH = async () => {
    await props.link.deposit({
      type: ETHTokenType.ETH,
      amount: depositAmount,
    });
    props.getBalance();
  };
  // prepare an eth withdrawal
  const prepareWithdrawalETH = async () => {
    await props.link.prepareWithdrawal({
      type: ETHTokenType.ETH,
      amount: withdrawAmount,
    })
  };
  // complete an eth withdrawal
  const completeWithdrawalETH = async () => {
    await props.link.completeWithdrawal({
      type: ETHTokenType.ETH,
    })
  };
  

  return (
    <>
      <WarningBuyer />
      <SearchModal show={show} hideModal={hideModal} setShow={setShow} />
      {width > 991 ? (
        <Navbar variant="dark" bg="dark" expand="lg" className="py-3 p-0">
          <Container>
            <Navbar.Brand as={Link} to="/">
              <Logo />
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="mr-auto ml-4">
                <Nav.Link as={Link} to="/dashboard/home">
                  Home
                </Nav.Link>
                <Nav.Link as={Link} to="/dashboard/products">
                  Explore
                </Nav.Link>
                <Nav.Link as={Link} to="/dashboard/nfts">
                  NFTs
                </Nav.Link>
                {!isLogged ? (
                  <Nav.Link as={Link} to="/dashboard/our-diff">
                    Our Difference
                  </Nav.Link>
                ) : null}
                {/* {!isLogged ? (
                  <Nav.Link as={Link} to="/login">
                    Login
                  </Nav.Link>
                ) : null} */}
                {isSeller ? (
                  <div className="menu-item">
                    <Nav.Link>
                      Products
                    </Nav.Link>
                    <div className="sub-menu-items">
                      <Nav.Link as={Link} to="/dashboard/add-product">
                        Add Product
                      </Nav.Link>
                      <Nav.Link as={Link} to="/dashboard/seller-products">
                        View Products
                      </Nav.Link>
                      <Nav.Link as={Link} to="/dashboard/transactions">
                        Transactions
                      </Nav.Link>
                      <Nav.Link onClick={() => {props.link.history({})}}>
                        Nft Transactions
                      </Nav.Link>
                    </div>
                  </div>
                ) : null}
                {/* {isSeller && (
                  <Nav.Link as={Link} to={`/dashboard/seller-profile`}>
                    View Profile
                  </Nav.Link>
                )} */}
                <Nav.Link as={Link} to="/dashboard/about">
                  About
                </Nav.Link>
                {/* <Nav.Link as={Link} to="/dashboard/contact-us">
                  Contact
                </Nav.Link>


                </Nav.Link> */}
                {/* <NavDropdown title="Dropdown" id="basic-nav-dropdown">
            <NavDropdown.Item to="#action/3.1">Action</NavDropdown.Item>
            <NavDropdown.Item to="#action/3.2">
              Another action
            </NavDropdown.Item>
            <NavDropdown.Item to="#action/3.3">Something</NavDropdown.Item>
            <NavDropdown.Divider />
            <NavDropdown.Item to="#action/3.4">
              Separated link
            </NavDropdown.Item>
          </NavDropdown> */}
              </Nav>
              <Form inline="true" className="nav-buttons flex-box">
                <Button
                  onClick={() => setShow(true)}
                  variant="outline-light"
                  className="mr-3 p-0 icon-menu search d-none"
                >
                  <svg
                    stroke="currentColor"
                    fill="currentColor"
                    strokeWidth="0"
                    viewBox="0 0 512 512"
                    height="1em"
                    width="1em"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path d="M456.69 421.39L362.6 327.3a173.81 173.81 0 0034.84-104.58C397.44 126.38 319.06 48 222.72 48S48 126.38 48 222.72s78.38 174.72 174.72 174.72A173.81 173.81 0 00327.3 362.6l94.09 94.09a25 25 0 0035.3-35.3zM97.92 222.72a124.8 124.8 0 11124.8 124.8 124.95 124.95 0 01-124.8-124.8z"></path>
                  </svg>
                </Button>
                {!isLogged && (
                  <Button
                    as={Link}
                    to="/login"
                    variant="outline-light"
                    style={{position: "relative"}}
                  >
                    Login
                  </Button>
                )}
                {isLogged && localStorage.getItem('WALLET_ADDRESS') == null ? (
                  <Button
                    className="mr-3 p-1"
                    variant="outline-light"
                    style={{position: "relative"}}
                    onClick={props.setupAccount}
                  >
                    Connect Wallet
                  </Button>
                ) : null}
                <Modal show={showDepositModal} onHide={() => {setShowDepositModal(false)}}>
                  <Modal.Header closeButton>
                    <Modal.Title>Deposit</Modal.Title>
                  </Modal.Header>
                  <Modal.Body>
                    <TextField
                      label="Price (ETH)"
                      type="text"
                      name="price"
                      placeholder="Enter the price in eth."
                      variant="outlined"
                      className="wp-100"
                      onChange={(event) => {setDepositAmount(event.target.value)}}
                    />
                  </Modal.Body>
                  <Modal.Footer>
                    <Button variant="secondary" onClick={() => {setShowDepositModal(false)}}>
                      Close
                    </Button>
                    <Button variant="contained"
                      color="primary"
                      size="small"
                      onClick={event => {
                        event.preventDefault();
                        depositETH();
                      }}
                    >
                      Confirm
                    </Button>

                  </Modal.Footer>
                </Modal>
                <Modal show={showWithdrawModel} onHide={() => {setShowWithdrawModal(false)}}>
                  <Modal.Header closeButton>
                    <Modal.Title>Prepare Withdrawl</Modal.Title>
                  </Modal.Header>
                  <Modal.Body>
                    <TextField
                      label="Price (ETH)"
                      type="text"
                      name="price"
                      placeholder="Enter the price in eth."
                      variant="outlined"
                      className="wp-100"
                      onChange={(event) => {setWithdrawAmount(event.target.value)}}
                    />
                  </Modal.Body>
                  <Modal.Footer>
                    <Button variant="secondary" onClick={() => {setShowWithdrawModal(false)}}>
                      Close
                    </Button>
                    <Button variant="contained"
                      color="primary"
                      size="small"
                      onClick={event => {
                        event.preventDefault();
                        prepareWithdrawalETH();
                      }}
                    >
                      Confirm
                    </Button>

                  </Modal.Footer>
                </Modal>
                {localStorage.getItem('WALLET_ADDRESS') != null ?
                  (<IconButton
                    size="small"
                    className="pb-2"
                    onClick={() => {
                      localStorage.setItem('SHOW_BALANCE', !showBalance);
                      setShowBalance(!showBalance);
                    }}
                  >
                    <FontAwesomeIcon style={{color: "gray"}} size="sm" icon={faEye} />
                  </IconButton>) : null
                }
                {localStorage.getItem('WALLET_ADDRESS') != null ? (
                  <div className="menu-item">
                    {showBalance == true ?
                      (<Navbar.Text className="mr-3 p-1"><b className="mr-1">ETH</b>{(localStorage.getItem('WALLET_BALANCE') / 1000000000000000000).toString()}</Navbar.Text>)
                      : (<Navbar.Text className="mr-3 p-1"><b className="mr-1">ETH</b>XXXXXXX</Navbar.Text>)
                    }
                    <div className="sub-menu-items">
                      <Nav.Link onClick={() => {setShowDepositModal(true)}}>
                        Deposit
                      </Nav.Link>
                      <Nav.Link onClick={() => {setShowWithdrawModal(true)}}>
                        Prepare Withdraw
                      </Nav.Link>
                      <Nav.Link onClick={() => {completeWithdrawalETH()}}>
                        Complete Withdraw
                      </Nav.Link>

                    </div>
                  </div>
                ) : null}
                {isLogged ? (
                  <Button
                    as={Link}
                    to="/dashboard/cart"
                    className="rounded-0 mr-3 p-0 icon-menu"
                    variant="outline-light"
                  >
                    <svg
                      stroke="currentColor"
                      fill="currentColor"
                      strokeWidth="0"
                      viewBox="0 0 16 16"
                      height="1em"
                      width="1em"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .49.598l-1 5a.5.5 0 0 1-.465.401l-9.397.472L4.415 11H13a.5.5 0 0 1 0 1H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l.84 4.479 9.144-.459L13.89 4H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"></path>
                    </svg>
                    <span className="notification badge">
                      {props.user.cartCount || 0}
                    </span>
                  </Button>
                ) : null}
                {isLogged ? (
                  // <Button
                  //   as={Link}
                  //   to="/dashboard/profile"
                  //   className="rounded-0 mr-3 p-0 icon-menu"
                  //   variant="outline-light"
                  // >
                  //   <svg
                  //     stroke="currentColor"
                  //     fill="currentColor"
                  //     strokeWidth="0"
                  //     viewBox="0 0 512 512"
                  //     height="1em"
                  //     width="1em"
                  //     xmlns="http://www.w3.org/2000/svg"
                  //   >
                  //     <path d="M224 256c70.7 0 128-57.31 128-128s-57.3-128-128-128C153.3 0 96 57.31 96 128S153.3 256 224 256zM274.7 304H173.3C77.61 304 0 381.6 0 477.3c0 19.14 15.52 34.67 34.66 34.67h378.7C432.5 512 448 496.5 448 477.3C448 381.6 370.4 304 274.7 304z"/>
                  //   </svg>
                  // </Button>

                  <div style={{position: 'relative'}}>
                    <Box sx={{display: 'flex', alignItems: 'center', textAlign: 'center', border: '0 !important'}}>
                      <Tooltip title="Account settings">
                        <IconButton
                          onClick={handleClick}
                          size="small"
                          sx={{ml: 2}}
                          aria-controls={open ? 'account-menu' : undefined}
                          aria-haspopup="true"
                          aria-expanded={open ? 'true' : undefined}
                        >
                          <Avatar sx={{width: 25, height: 25}}>
                            {isLogged ? <img src={loginUser.profilePicture} className="wp-100" /> : <UserIcon />}
                            {/* <svg
                              stroke="currentColor"
                              fill="currentColor"
                              strokeWidth="0"
                              viewBox="0 0 512 512"
                              height="1em"
                              width="1em"
                              xmlns="http://www.w3.org/2000/svg"
                            >
                              <path d="M224 256c70.7 0 128-57.31 128-128s-57.3-128-128-128C153.3 0 96 57.31 96 128S153.3 256 224 256zM274.7 304H173.3C77.61 304 0 381.6 0 477.3c0 19.14 15.52 34.67 34.66 34.67h378.7C432.5 512 448 496.5 448 477.3C448 381.6 370.4 304 274.7 304z"/>
                            </svg> */}
                          </Avatar>
                        </IconButton>
                      </Tooltip>
                    </Box>
                    <ArrowDropUpIcon className={`menu-anchor ${open ? "" : "d-none"}`} />
                    <Menu
                      anchorEl={anchorEl}
                      id="account-menu"
                      open={open}
                      onClose={handleClose}
                      onClick={handleClose}
                      PaperProps={paperProps}
                      transformOrigin={{horizontal: 'right', vertical: 'top'}}
                      anchorOrigin={{horizontal: 'right', vertical: 'bottom'}}
                      style={{top: "65px"}}
                    >
                      <MenuItem>
                        <Nav.Link as={Link} to="/dashboard/profile" style={{display: "flex", alignItems: "center"}}>
                          <Avatar className="mr-2"><img src={loginUser.profilePicture} className="wp-100" /></Avatar><span>Profile</span>
                        </Nav.Link>
                      </MenuItem>
                      <Divider />
                      <MenuItem>
                        <Nav.Link as={Link} to="/dashboard/seller-notifications">
                          Notification
                        </Nav.Link>
                      </MenuItem>
                      {isBuyer && (
                        <MenuItem>
                          <Nav.Link as={Link} to="/dashboard/wishlist">
                            Wishlist
                          </Nav.Link>
                        </MenuItem>
                      )}
                      {(
                        <MenuItem>
                          <Nav.Link as={Link} to="/dashboard/profile#Transactions">
                            Stripe
                          </Nav.Link>
                        </MenuItem>
                      )}
                      {/* {loginUser.stripeAccount != null && !loginUser.stripeStatus && (
                        <MenuItem>
                          <Nav.Link onClick={completeAccount}>
                            Complete Stripe
                          </Nav.Link>
                        </MenuItem>
                      )} */}
                      {isLogged &&
                        <Logout />
                      }
                    </Menu>
                  </div>
                  // <FontAwesomeIcon icon="fa-solid fa-user" />
                ) : null}
                {/* {isLogged && isSeller && (
                  <Button
                    as={Link}
                    to="/dashboard/seller-notifications"
                    className="rounded-0 mr-3 p-0 icon-menu"
                    variant="outline-light"
                    style={{ position: "relative" }}
                  >
                    <div
                      style={{
                        minWidth: "12px",
                        minHeight: "12px",
                        backgroundColor: "red",
                        borderRadius: "15px",
                        position: "absolute",
                        fontSize: "10px",
                        textAlign: "center",
                        color: "white",
                        padding: "2px 5px",
                        top: "23px",
                        right: "0px",
                      }}
                    >
                      {notifications != null ? notifications : 0}
                    </div>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="16"
                      height="16"
                      fill="currentColor"
                      className="bi bi-bell-fill"
                      viewBox="0 0 16 16"
                    >
                      <path d="M8 16a2 2 0 0 0 2-2H6a2 2 0 0 0 2 2zm.995-14.901a1 1 0 1 0-1.99 0A5.002 5.002 0 0 0 3 6c0 1.098-.5 6-2 7h14c-1.5-1-2-5.902-2-7 0-2.42-1.72-4.44-4.005-4.901z" />
                    </svg>
                  </Button>
                )} */}
                {/* {isBuyer ? (
                  <Button
                    as={Link}
                    to="/dashboard/wishlist"
                    className="rounded-0 mr-3 p-0 icon-menu"
                    variant="outline-light"
                  >
                    <svg
                      stroke="currentColor"
                      fill="currentColor"
                      strokeWidth="0"
                      viewBox="0 0 500 500"
                      height="1em"
                      width="1em"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path d="M458.4 64.3C400.6 15.7 311.3 23 256 79.3 200.7 23 111.4 15.6 53.6 64.3-21.6 127.6-10.6 230.8 43 285.5l175.4 178.7c10 10.2 23.4 15.9 37.6 15.9 14.3 0 27.6-5.6 37.6-15.8L469 285.6c53.5-54.7 64.7-157.9-10.6-221.3zm-23.6 187.5L259.4 430.5c-2.4 2.4-4.4 2.4-6.8 0L77.2 251.8c-36.5-37.2-43.9-107.6 7.3-150.7 38.9-32.7 98.9-27.8 136.5 10.5l35 35.7 35-35.7c37.8-38.5 97.8-43.2 136.5-10.6 51.1 43.1 43.5 113.9 7.3 150.8z" />
                    </svg>
                  </Button>
                ) : null} */}
                {/* {isLogged ? <Logout /> : null} */}
              </Form>
            </Navbar.Collapse>
          </Container>
        </Navbar>
      ) : (
        <nav
          className={`mobile-nav bg-dark py-3 text-light ${mobileMenu ? "fixed-top" : ""
            }`}
        >
          <Container className="d-flex justify-content-between align-items-center">
            <div
              onClick={handleMenuClick}
              className="font-25 mb-nav-icon text-light"
            >
              {mobileMenu ? (
                <svg
                  stroke="currentColor"
                  fill="currentColor"
                  strokeWidth="0"
                  viewBox="0 0 24 24"
                  height="1em"
                  width="1em"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    fill="none"
                    stroke="currentColor"
                    strokeWidth="2"
                    d="M3,3 L21,21 M3,21 L21,3"
                  ></path>
                </svg>
              ) : (
                <svg
                  stroke="currentColor"
                  fill="currentColor"
                  strokeWidth="0"
                  viewBox="0 0 512 512"
                  height="1em"
                  width="1em"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path d="M432 176H80c-8.8 0-16-7.2-16-16s7.2-16 16-16h352c8.8 0 16 7.2 16 16s-7.2 16-16 16zM432 272H80c-8.8 0-16-7.2-16-16s7.2-16 16-16h352c8.8 0 16 7.2 16 16s-7.2 16-16 16zM432 368H80c-8.8 0-16-7.2-16-16s7.2-16 16-16h352c8.8 0 16 7.2 16 16s-7.2 16-16 16z"></path>
                </svg>
              )}
            </div>
            <div style={{marginRight: "auto", marginLeft: "10px"}}>
              {/* <img
                src={StaticData.logo}
                width={100}
                alt="logo"
              /> */}
              <Logo />
            </div>
            <div className="btns-nav">
              <Form inline="true" className="nav-buttons flex-box">
                <Button
                  onClick={() => setShow(true)}
                  variant="outline-light"
                  className="mr-2 mr-sm-3 p-0 icon-menu search d-none"
                >
                  <svg
                    stroke="currentColor"
                    fill="currentColor"
                    strokeWidth="0"
                    viewBox="0 0 512 512"
                    height="1em"
                    width="1em"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path d="M456.69 421.39L362.6 327.3a173.81 173.81 0 0034.84-104.58C397.44 126.38 319.06 48 222.72 48S48 126.38 48 222.72s78.38 174.72 174.72 174.72A173.81 173.81 0 00327.3 362.6l94.09 94.09a25 25 0 0035.3-35.3zM97.92 222.72a124.8 124.8 0 11124.8 124.8 124.95 124.95 0 01-124.8-124.8z"></path>
                  </svg>
                </Button>
                {/* isLogged && isBuyer */}
                {isLogged ? (
                  <Button
                    as={Link}
                    to="cart"
                    className="mr-2 mr-sm-3 p-0 icon-menu"
                    variant="outline-light"
                    style={{width: "4rem"}}
                  >
                    <svg
                      stroke="currentColor"
                      fill="currentColor"
                      strokeWidth="0"
                      viewBox="0 0 16 16"
                      height="1em"
                      width="1em"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .49.598l-1 5a.5.5 0 0 1-.465.401l-9.397.472L4.415 11H13a.5.5 0 0 1 0 1H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l.84 4.479 9.144-.459L13.89 4H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"></path>
                    </svg>
                    <span className="notification badge">
                      {props.user.cartCount || 0}
                    </span>
                  </Button>
                ) : null}
                {/* {isLogged ? <Logout /> : null} */}
              </Form>
            </div>
          </Container>
          <div
            className="mobile-nav-menu"
            style={{
              transform: mobileMenu ? "translateY(0)" : "translateY(100%)",
            }}
          >
            <Nav className="mr-auto">
              <MenuItem>
                <Nav.Link
                  as={Link}
                  onClick={() => setMobileMenu(false)}
                  to="home"
                >
                  Home
                </Nav.Link>
              </MenuItem>
              <MenuItem>
                <Nav.Link
                  as={Link}
                  onClick={() => setMobileMenu(false)}
                  to="products"
                >
                  Explore
                </Nav.Link>
              </MenuItem>
              <MenuItem>
                <Nav.Link
                  onClick={() => setMobileMenu(false)}
                  to="our-diff"
                  as={Link}
                >
                  Our Difference
                </Nav.Link>
              </MenuItem>
              {!isLogged && (
                <MenuItem>
                  <Nav.Link
                    as={Link}
                    onClick={() => setMobileMenu(false)}
                    to="/login"
                  >
                    Login
                  </Nav.Link>
                </MenuItem>
              )}
              {isLogged && isSeller && (
                <MenuItem>
                  <Nav.Link
                    as={Link}
                    onClick={() => setMobileMenu(false)}
                    to="add-product"
                  >
                    Add Product
                  </Nav.Link>
                </MenuItem>
              )}
              {isLogged && isSeller && (
                <MenuItem>
                  <Nav.Link
                    as={Link}
                    onClick={() => setMobileMenu(false)}
                    to="seller-products"
                  >
                    View Products
                  </Nav.Link>
                </MenuItem>
              )}
              {isLogged && isSeller && (
                <MenuItem>
                  <Nav.Link
                    as={Link}
                    onClick={() => setMobileMenu(false)}
                    to="profile"
                  >
                    View Profile
                  </Nav.Link>
                </MenuItem>
              )}

              {isLogged && isBuyer && (
                <MenuItem>
                  <Nav.Link
                    as={Link}
                    onClick={() => setMobileMenu(false)}
                    to="wishlist"
                  >
                    Wishlist
                  </Nav.Link>
                </MenuItem>
              )}

              {isLogged && isBuyer && (
                <MenuItem>
                  <Nav.Link
                    as={Link}
                    onClick={() => setMobileMenu(false)}
                    to="profile"
                  >
                    Profile
                  </Nav.Link>
                </MenuItem>
              )}
              {isLogged && isSeller && (
                <MenuItem>
                  <Nav.Link
                    as={Link}
                    onClick={() => setMobileMenu(false)}
                    to="transactions"
                  >
                    Transactions
                  </Nav.Link>
                </MenuItem>
              )}
              <MenuItem>
                <Nav.Link
                  as={Link}
                  onClick={() => setMobileMenu(false)}
                  to="about"
                >
                  About
                </Nav.Link>
              </MenuItem>
              {/* <MenuItem>
                <Nav.Link
                  as={Link}
                  onClick={() => setMobileMenu(false)}
                  to="contact-us"
                >
                  Contact
                </Nav.Link>
              </MenuItem> */}
              {isLogged &&
                <Logout />
              }
              {/* <NavDropdown title="Dropdown" id="basic-nav-dropdown">
            <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
            <NavDropdown.Item href="#action/3.2">
              Another action
            </NavDropdown.Item>
            <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
            <NavDropdown.Divider />
            <NavDropdown.Item href="#action/3.4">
              Separated link
            </NavDropdown.Item>
          </NavDropdown> */}
            </Nav>
          </div>
        </nav>
      )}
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    user: state.userReducer,
  };
};

export default connect(mapStateToProps)(AppNav);
