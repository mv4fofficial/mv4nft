import React from "react";

const DiscoverItem = () => {
  return (
    <div className="m-2 discover-item">
      <div className="img-container">
        <img
          className="w-100"
          src="https://cdn.shopify.com/s/files/1/0609/6152/1863/files/Guns_275x.png"
          alt="dicover cat displahy"
        />
      </div>
      <a href="#f" className="d-inline-block btn-with-arrow mt-3">
        <span className=" font-18 font-weight-bold">Guns </span>
        <svg
          stroke="currentColor"
          fill="currentColor"
          strokeWidth="0"
          viewBox="0 0 16 16"
          height="1em"
          width="1em"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            fillRule="evenodd"
            d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z"
          ></path>
        </svg>
      </a>
    </div>
  );
};

export default DiscoverItem;
