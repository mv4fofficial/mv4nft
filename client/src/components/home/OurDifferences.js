import React from "react";
import { Col, Container, Row } from "react-bootstrap";

const OurDifferences = () => {
  return (
    <div className="our-differences pb-5">
      <Container>
        <Row>
          <Col lg={8} className="mx-auto">
            <div className="pt-3 text-center">
              <h1 className="section-heading">Uniquely MV4F</h1>
            </div>
            <div className="mt-4 text-center">
              <h2 className="my-4">Never miss any assets</h2>
              <p>
                If unfortunately none of our assets fit your exact needs, you can tag an existing asset that is closest to your desires, list the parameters and attributes changes you require through a simple checklist, and you will be notified immediately once our algorithm picks up any new assets listed that is of a closer match to your desires.
              </p>
            </div>
            <div className="mt-5 text-center">
              <h2 className="my-4 ">We succeed with you</h2>
              <p>
              We want to be part of your success and journey, and further help you lower your developmental costs by allowing you to buy our assets through up to 80% of YOUR tokens, with the remaining in BTC, ETH or SOL.
              </p>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default OurDifferences;
