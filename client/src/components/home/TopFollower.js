import * as React from 'react';
import { useEffect, useState } from "react";

import { styled } from '@mui/material/styles';
import Paper from '@mui/material/Paper';
import ApiRequest from '../../api_service/api_requests';
import Grid from '@mui/material/Unstable_Grid2';
import { Col, Container, Row } from "react-bootstrap";
import { Link } from "react-router-dom";

import VerifiedIcon from '@mui/icons-material/Verified';
import MilitaryTechIcon from '@mui/icons-material/MilitaryTech';

import "../../assets/css/top-follower.css"

const Item = styled(Paper)(({ theme }) => ({
    // backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    // padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
}));


const TopFollower = () => {

    const [followerLists, setFollowerLists] = useState([]);

    useEffect(() => {
        ApiRequest.getTopFollower().then(res => {
            // console.log(res.data)
            if (res && res.data) {
                setFollowerLists(res.data);
            }
          })
          .catch((err) => {
            // console.log('Products fetch api error, ', err);
          })
    }, []);

    // console.log(followerLists);

    const [carouselIndex, setCarouselIndex] = useState(0);

    const prevCarousel = () => {
        if (carouselIndex > 0) {
            setCarouselIndex(carouselIndex - 1);
        } else {
            setCarouselIndex(followerLists.length - 1);
        }
    }

    const nextCarousel = () => {
        if (carouselIndex < followerLists.length - 1) {
            setCarouselIndex(carouselIndex + 1);
        } else {
            setCarouselIndex(0);
        }
    }

    const changeCarousel = (index) => {
        setCarouselIndex(index);
    }


    const [touchPosition, setTouchPosition] = useState(null)
    // ...
    const handleTouchStart = (e) => {
        const touchDown = e.touches[0].clientX
        setTouchPosition(touchDown)
    }
    // 

    // 
    const handleTouchMove = (e) => {
        const touchDown = touchPosition
    
        if(touchDown === null) {
            return
        }
    
        const currentTouch = e.touches[0].clientX
        const diff = touchDown - currentTouch
    
        if (diff > 5) {
            nextCarousel()
        }
    
        if (diff < -5) {
            prevCarousel();
        }
    
        setTouchPosition(null)
    }
    // 

    return <>
        <Container className="my-4 follower-container">
            <Row>
                <Col lg={12} style={{overflowY: "hidden"}}>
                    <Grid container spacing={1} className="top-seller-grid desktop">
                        {followerLists.map((v, i) => {
                            if (i == 0) {
                                return <Grid xs={2} md={3} mdOffset={0} className={`text-center top-seller pb-4 rank-${i}`} key={i}>
                                    <Link to={`/dashboard/seller-profile/${v.Id}`} style={{cursor: "pointer"}}>
                                        <div>
                                            <div style={{position: "relative"}}>
                                                <img src={v.ProfilePicture} width={150}/>
                                                <VerifiedIcon/>
                                            </div>
                                            <span>{`# ${i+1}`}</span>
                                            <p>{v.FirstName} {v.LastName}</p>
                                            <span>{v.FollowerCount > 1 ? v.FollowerCount+" followers" : v.FollowerCount+" follower"} </span>
                                        </div>
                                    </Link>
                                </Grid>
                            } else {
                                return <Grid xs={2} md={2} mdOffset="auto" className={`text-center top-seller rank-${i}`} key={i}>
                                    <Link to={`/dashboard/seller-profile/${v.Id}`} style={{cursor: "pointer"}}>
                                        <div>
                                            <div style={{position: "relative"}}>
                                                <img src={v.ProfilePicture} width={130}/>
                                                <VerifiedIcon/>
                                            </div>
                                            <span>{`# ${i+1}`}</span>
                                            <p>{v.FirstName} {v.LastName}</p>
                                            <span>{v.FollowerCount > 1 ? v.FollowerCount+" followers" : v.FollowerCount+" follower"} </span>
                                        </div>
                                    </Link>
                                </Grid>
                            }
                        })}
                    </Grid>

                    <div id="carouselExampleIndicators" className="carousel slide mobile" data-ride="carousel"
                        onTouchStart={handleTouchStart}
                        onTouchMove={handleTouchMove}
                    >
                        <div className="carousel-inner">
                        {followerLists.map((v, i) => {
                            return <div className={(i === carouselIndex) ? "carousel-item active" : "carousel-item"}>
                                <Link to={`/dashboard/seller-profile/${v.Id}`} style={{cursor: "pointer"}}>
                                    <div className='text-center'>
                                        <div style={{position: "relative"}}>
                                            <img src={v.ProfilePicture} width={130}/>
                                            <VerifiedIcon/>
                                        </div>
                                        <span>{`# ${i+1}`}</span>
                                        <p>{v.FirstName} {v.LastName}</p>
                                        <span>{v.FollowerCount > 1 ? v.FollowerCount+" followers" : v.FollowerCount+" follower"} </span>
                                    </div>
                                </Link>
                            </div>
                        })}
                            {/* <div className="carousel-item active">
                                <img className="d-block w-100" src="..." alt="First slide" />
                            </div>
                            <div className="carousel-item">
                                <img className="d-block w-100" src="..." alt="Second slide" />
                            </div>
                            <div className="carousel-item">
                                <img className="d-block w-100" src="..." alt="Third slide" />
                            </div> */}
                        </div>
                        <a className="carousel-control-prev" role="button" data-slide="prev" onClick={prevCarousel}>
                            <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span className="sr-only">Previous</span>
                        </a>
                        <a className="carousel-control-next" role="button" data-slide="next"  onClick={nextCarousel}>
                            <span className="carousel-control-next-icon" aria-hidden="true"></span>
                            <span className="sr-only">Next</span>
                        </a>
                        <ol className="carousel-indicators mt-3">
                        {followerLists.map((v, i) => (
                            <li data-target="#carouselExampleIndicators" data-slide-to={i}
                                className={(i === carouselIndex) ? "active" : ""}
                                onClick={() => changeCarousel(i)}></li>
                        ))}
                        </ol>
                    </div>
                </Col>
            </Row>
        </Container>
    </>
}

export default TopFollower;