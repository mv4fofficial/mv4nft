import React from "react";
import { Col, Container, Row } from "react-bootstrap";

const Footer = () => {
  return (
    <footer>
      <div className="footer-top">
        <Container>
          <Row>
            <Col lg={4} className="mb-lg-0 mb-4">
              <h4>About us</h4>
              <p>
                We want to provide a mixed reality and connected metaverse future, by creating the central metaverse that allows for our users assets to transact and transit freely, interoperating with multi-metaverses.
              </p>
              <ul className="list-unstyled">
                <li>
                  <a
                    className="underline font-weight-bold"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <u>MV4F</u>
                  </a>
                </li>
              </ul>
            </Col>
            <Col lg={4} className="mb-lg-0 mb-4">
              <h4>Social Channels</h4>
              <p>Follow us on the social handles</p>
              <ul className="d-flex list-unstyled align-items-center">
                <li className="mr-3">
                  <a
                    className="font-20"
                    href="https://www.facebook.com/MV4F-Metaverse-for-the-Future-102750739037051/?hc_ref=ARRqq9_W7wrEulC8eS4TgTqlOJkax2duZJCsbSYd01oBxUxJDFw19JQ8MQr0CJbKlR0&fref=nf&__xts__[0]=68.ARAWG7RiGueAhLHSoq2EooW_pYiXezyMCnBD69UahcWEKDuWNueZdFDc9NzbFWl-EEfHasOxodeTbaNXZc2gTYephOSGh3XzSzuzinZGa0KATBkI8YeBsAnB5HcieIcLqS0YSk_krWTNuaPr7vLQzVQQi2X76K4GB0qCBR-UMPg8ukBLwY7w-bm3kylEtZQy628JEeb0iu9_HmV7D1eITSqcN42agHzFWWdTmoVE1lhuHJQ_pCBSfhD_IBdH3Jrr68UU4uarZLgmgpD_eDAWCEm00TnYXZA"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="currentColor"
                      viewBox="0 0 24 24"
                      width="20px"
                      height="20px"
                    >
                      <path d="M19,3H5C3.895,3,3,3.895,3,5v14c0,1.105,0.895,2,2,2h7.621v-6.961h-2.343v-2.725h2.343V9.309 c0-2.324,1.421-3.591,3.495-3.591c0.699-0.002,1.397,0.034,2.092,0.105v2.43h-1.428c-1.13,0-1.35,0.534-1.35,1.322v1.735h2.7 l-0.351,2.725h-2.365V21H19c1.105,0,2-0.895,2-2V5C21,3.895,20.105,3,19,3z"/>
                    </svg>
                  </a>
                </li>
                <li className="mr-3">
                  <a
                    className="font-20"
                    href="https://twitter.com/Mv4f_io"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <svg
                      stroke="currentColor"
                      fill="currentColor"
                      strokeWidth="0"
                      viewBox="0 0 512 512"
                      height="1em"
                      width="1em"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z"></path>
                    </svg>
                  </a>
                </li>
                {/* <li className="mb-1">
                  <a
                    className="font-20 "
                    href="#ff"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <svg
                      stroke="currentColor"
                      fill="currentColor"
                      strokeWidth="0"
                      viewBox="0 0 24 24"
                      height="1em"
                      width="1em"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        fillRule="evenodd"
                        d="M22.0367422,22 L17.8848745,22 L17.8848745,15.5036305 C17.8848745,13.9543347 17.85863,11.9615082 15.7275829,11.9615082 C13.5676669,11.9615082 13.237862,13.6498994 13.237862,15.3925291 L13.237862,22 L9.0903683,22 L9.0903683,8.64071385 L13.0707725,8.64071385 L13.0707725,10.4673257 L13.1276354,10.4673257 C13.6813927,9.41667396 15.0356049,8.3091593 17.0555507,8.3091593 C21.2599073,8.3091593 22.0367422,11.0753215 22.0367422,14.6734319 L22.0367422,22 Z M4.40923804,6.81585163 C3.07514653,6.81585163 2,5.73720584 2,4.40748841 C2,3.07864579 3.07514653,2 4.40923804,2 C5.73720584,2 6.81585163,3.07864579 6.81585163,4.40748841 C6.81585163,5.73720584 5.73720584,6.81585163 4.40923804,6.81585163 L4.40923804,6.81585163 Z M6.48604672,22 L2.32980492,22 L2.32980492,8.64071385 L6.48604672,8.64071385 L6.48604672,22 Z"
                      ></path>
                    </svg>
                  </a>
                </li> */}
              </ul>
            </Col>
            <Col lg={4} className="mb-lg-0 mb-4">
              <h4>Useful Links</h4>
              <p>There are some quick links check these out </p>
            </Col>
          </Row>
        </Container>
      </div>
      <div className="coryp-right">
        <p className="mb-0">© 2022, MV4F</p>
      </div>
    </footer>
  );
};

export default Footer;
