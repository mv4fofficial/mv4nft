import { getStorage, ref, uploadString, getDownloadURL, deleteObject } from "firebase/storage";

const generateImageName = (length) => {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() *
        charactersLength));
    }
    return result;
}

export const FirebaseImageUplaod = async (data, successCallBack) => {
  // console.log('data', data);
  if (!(data.url).includes("firebasestorage.googleapis.com")) {
    data.url = (data.url).replace(`data:image/${data.fileType};base64,`, "");
    let enviroment = 'staging';
    const imageName = generateImageName(30);
    const metadata = {
      contentType: `image/${data.fileType}`,
    };
    const storage = getStorage();
    if (process.env.REACT_APP_NODE_ENV === 'production') {
      enviroment = 'production';
    }
    const storageRef = ref(storage, `${enviroment}/${imageName}${data.fileExtension}`);
    await uploadString(storageRef, data.url, 'base64', metadata).then((snapshot) => {
      // console.log('Uploaded a base64 string!');
    });
    await getDownloadURL(storageRef)
    .then((url) => {
      // console.log('url', url);
      successCallBack(url)
    })
    .catch((error) => {
      // A full list of error codes is available at
      // https://firebase.google.com/docs/storage/web/handle-errors
      // eslint-disable-next-line default-case
      switch (error.code) {
        case 'storage/object-not-found':
          // File doesn't exist
          break;
        case 'storage/unauthorized':
          // User doesn't have permission to access the object
          break;
        case 'storage/canceled':
          // User canceled the upload
          break;
  
        // ...
  
        case 'storage/unknown':
          // Unknown error occurred, inspect the server response
          break;
      }
    });
  } else {
    successCallBack(data.url)
  }
}

export const FirebaseImageDelete = async (url, successCallBack) => {
  const storage = getStorage();

  // Create a reference to the file to delete
  const desertRef = ref(storage, url);

  // Delete the file
  deleteObject(desertRef).then((result) => {
    // File deleted successfully
    successCallBack(result)
  }).catch((error) => {
    // Uh-oh, an error occurred!
  });
}