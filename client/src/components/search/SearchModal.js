import React from "react";
import { connect } from 'react-redux';
import { withRouter } from "react-router-dom";
import { Button, Form, Modal } from "react-bootstrap";
import { productSearchStringUpdate } from '../../user';

class SearchModal extends React.PureComponent {
  constructor() {
    super();
    this.state = {
      searchText: ""
    };
  }

  updateSearchText = event => {
    if (event && event.target && event.target.value) {
      this.setState({ searchText: event.target.value });
    }
  }

  searchProduct = event => {
    if (this.state.searchText) {
      this.props.dispatch(productSearchStringUpdate(this.state.searchText));
      this.props.history.push("/dashboard/products");
      this.props.hideModal();
    }
  }

  render() {
    const { show, setShow, hideModal } = this.props;
    return (
      <>
        <Modal
          show={show}
          onHide={() => setShow(false)}
          dialogClassName="search-modal"
          aria-labelledby="example-custom-modal-styling-title"
        >
          <Modal.Body>
            <Form autoComplete="off">
              <Form.Group
                controlId="formBasicEmail"
                className="position-relative d-flex">
                <Form.Control
                  autoComplete="off"
                  type="search"
                  placeholder="Search"
                  value={this.state.searchText}
                  onChange={this.updateSearchText} />

                <Button type="submit" className="search-btn " onClick={event => {
                  event.preventDefault();
                  this.searchProduct(event);
                }}>
                  <svg
                    stroke="currentColor"
                    fill="currentColor"
                    strokeWidth="0"
                    viewBox="0 0 512 512"
                    height="1em"
                    width="1em"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path d="M456.69 421.39L362.6 327.3a173.81 173.81 0 0034.84-104.58C397.44 126.38 319.06 48 222.72 48S48 126.38 48 222.72s78.38 174.72 174.72 174.72A173.81 173.81 0 00327.3 362.6l94.09 94.09a25 25 0 0035.3-35.3zM97.92 222.72a124.8 124.8 0 11124.8 124.8 124.95 124.95 0 01-124.8-124.8z"></path>
                  </svg>
                </Button>
              </Form.Group>
            </Form>
            <Button
              onClick={hideModal}
              variant="dark"
              className="rounded-circle position-absolute btn-close"
            >
              <svg
                stroke="currentColor"
                fill="currentColor"
                strokeWidth="0"
                viewBox="0 0 512 512"
                height="1em"
                width="1em"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fill="none"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="32"
                  d="M368 368L144 144m224 0L144 368"
                ></path>
              </svg>
            </Button>
          </Modal.Body>
        </Modal>
      </>
    );
  }
};

const mapStateToProps = (state) => {
  return {
    user: state.userReducer
  }
}

export default withRouter(connect(mapStateToProps)(SearchModal));
