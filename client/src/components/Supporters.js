import React from "react";
import { Col, Container, Row } from "react-bootstrap";

const Supporters = () => {
  return (
    <Container className="supporters">
      <Row className="align-items-center">
        <Col lg={6}>
          <h2>Supported by some of the biggest players</h2>
          <p>
            MV4F is proudly supported by one of the biggest names in the NFT and
            blockchain space, Animoca brands, through Brinc's Launchpad Luna
            program!
            <span className="d-block mt-3"></span>
            <strong>
              We are thrilled to share our vision with more of others!
            </strong>
          </p>
        </Col>
        <Col lg={6}>
          <img
            src="https://cdn.shopify.com/s/files/1/0609/6152/1863/files/MV4F_750x.png?v=1643204333"
            alt="supporters"
            className="w-100"
          />
        </Col>
      </Row>
    </Container>
  );
};

export default Supporters;
