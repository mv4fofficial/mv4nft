export const Delay = ms => new Promise(
    resolve => setTimeout(resolve, ms)
);