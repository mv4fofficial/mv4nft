const StaticData = {
    logo: "https://firebasestorage.googleapis.com/v0/b/mv4f-marketplace.appspot.com/o/images%2FLogo.png?alt=media&token=ac143c0c-32b0-4496-9463-a157653e423b",
    bannerHero: "https://firebasestorage.googleapis.com/v0/b/mv4f-marketplace.appspot.com/o/images%2FHome%20Page%20Red_Empty.jpg?alt=media&token=1f94e113-f047-40f7-bc58-1fbe17dc9c56",
    userDescMaxText: 600,
    description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
}
export default StaticData;