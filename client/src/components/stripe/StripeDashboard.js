import React, {useState, useEffect} from "react";

import ApiRequest from "../../api_service/api_requests";

import Button from '@material-ui/core/Button';
import {Link} from "react-router-dom";
import moment from "moment";
import Pagination from '@material-ui/core/TablePagination';

import "../../assets/css/stripe.css";

const StripeDashboard = ({list = true}) => {

  const [loginUser, setLoginUser] = useState({});
  const [stripe, setStripe] = useState({});

  const [transactions, setTransactions] = useState([]);

  const [accountBalance, setAccountBalance] = useState({});

  const [loading, setLoading] = useState(true);

  useEffect(() => {
    ApiRequest.viewUser()
      .then(res => {
        if (res && res.data && res.statusCode && res.statusCode === 200) {
          // console.log("View other user", res.data)
          setLoginUser({...res.data})
        } else {
          if (res && res.message) {
            // this.props.alert.error("Error fetching user details");
          }
        }
      })
      .catch((err) => {
        // console.log('Error fetching user details, ', err);
      })
  }, [])

  useEffect(() => {
    if (loginUser.stripeAccount != null) {
      ApiRequest.checkAccount({
        email: "",
        accountId: loginUser.stripeAccount
      })
        .then(res => {
          // console.log(res.data)
          setStripe({...res.data})
        })
        .catch((err) => {
          // console.log('Error fetching user details, ', err);
        })
    }
  }, [loginUser])

  useEffect(() => {
    if (stripe.charges_enabled && stripe.payouts_enabled && !loginUser.stripeStatus) {
      ApiRequest.updateCompleteAccountStatus()
        .then(res => {
          // console.log(res)
            // console.log("Stripe account ", res);

            ApiRequest.completeStripeAccount({
                email: "",
                accountId: res.data,
                returnUrl: window.location.href
            })
            .then(completeRes => {
                // console.log("Stripe complete ", completeRes);
                setLoading(false)
                window.open(completeRes.data.url);
                
            })
            .catch((err) => {
                // console.log('Error fetching user details, ', err);
                setLoading(false)
            })
        })
        .catch((err) => {
          // console.log('Error fetching user details, ', err);
        })
    } else
      if (stripe.charges_enabled && stripe.payouts_enabled && loginUser.stripeStatus) {
        ApiRequest.fetchTransactions({
          email: "",
          accountId: loginUser.stripeAccount
        })
          .then(res => {
            // console.log(res)
            setTransactions([...res.data])
          })
          .catch((err) => {
            // console.log('Error fetching user details, ', err);
          })

        ApiRequest.getAccountBalance({
          email: "",
          accountId: loginUser.stripeAccount
        })
          .then(res => {
            // console.log("getAccountBalance", res)
            setAccountBalance({...res.data});
            setLoading(false)
          })
          .catch((err) => {
            // console.log('Error fetching user details, ', err);
            setLoading(false)
          })
      }
  }, [stripe])

  const openStripeAccount = () => {
    setLoading(true)
    ApiRequest.openStripeAccount({
      email: loginUser.emailId,
      accountId: ""
    })
      .then(res => {
        // console.log("Stripe account ", res);

        ApiRequest.completeStripeAccount({
          email: "",
          accountId: res.data,
          returnUrl: window.location.href
        })
          .then(completeRes => {
            // console.log("Stripe complete ", completeRes);
            setLoading(false)
            window.open(completeRes.data.url);

          })
          .catch((err) => {
            // console.log('Error fetching user details, ', err);
            setLoading(false)
          })
      })
      .catch((err) => {
        // console.log('Error fetching user details, ', err);
      })
  }

  const completeAccount = () => {
    setLoading(true)
    ApiRequest.completeStripeAccount({
      email: "",
      accountId: loginUser.stripeAccount,
      returnUrl: window.location.href
    })
      .then(completeRes => {
        // console.log("Stripe complete ", completeRes);
        setLoading(false)
        window.open(completeRes.data.url);
      })
      .catch((err) => {
        // console.log('Error fetching user details, ', err);
        setLoading(false)
      })
  }

  const loginAccount = () => {
    setLoading(true)
    ApiRequest.loginStripeAccount({
      email: "",
      accountId: loginUser.stripeAccount
    })
      .then(completeRes => {
        // console.log("Stripe complete ", completeRes);
        setLoading(false)
        window.open(completeRes.data.url);
      })
      .catch((err) => {
        // console.log('Error fetching user details, ', err);
        setLoading(false)
      })
  }

  // console.log("accountBalance", accountBalance)

  const [filters, setFilters] = useState({
    pageSize: 10,
    pageIndex: 1,
    startIndex: 0,
    endIndex: 9
  })

  const handleChangePage = (event, newPage) => {
    let filterOptions = filters;
    filterOptions.pageIndex = newPage + 1;
    filterOptions.startIndex = newPage * filters.pageSize;
    filterOptions.endIndex = ((newPage + 1) * filters.pageSize) - 1;
    setFilters({...filterOptions});
  };

  const handleChangeRowsPerPage = (event) => {
    let filterOptions = filters;
    filterOptions.pageIndex = 1;
    filterOptions.pageSize = event.target.value;
    filterOptions.startIndex = 0;
    filterOptions.endIndex = filterOptions.pageSize - 1;
    setFilters({...filterOptions});
  };

  return <>
    <div className="stripe-dashboard">
      {loading ?
        <div className="d-flex justify-content-between">
          <div className="d-flex">
            <p>Please wait a moment, stripe information still fetching!</p>
          </div>
        </div>
        :
        loginUser.stripeAccount != null ?
          <>
            <div className="d-flex justify-content-between">
              <div className="d-flex">
                {(stripe.charges_enabled || stripe.payouts_enabled) &&
                  <div className="box">
                    <span>Balance :</span>
                    <span>{(Object.keys(accountBalance).length > 0) ? (accountBalance.available[0].currency).toUpperCase() + " " + accountBalance.available[0].amount : 0}</span>
                  </div>
                }
                <div className="box">
                  <span>Charges :</span>
                  <span>{stripe.charges_enabled ? "Enabled" : "Disabled"}</span>
                </div>
                <div className="box">
                  <span>Payputs :</span>
                  <span>{stripe.payouts_enabled ? "Enabled" : "Disabled"}</span>
                </div>
              </div>
              {(!stripe.charges_enabled || !stripe.payouts_enabled) ?
                <>
                  <Button
                    type="button"
                    className=""
                    variant="contained"
                    color="primary"
                    size="small"
                    onClick={completeAccount}
                  >
                    {"Complete Stripe"}
                  </Button>
                </>
                :
                <>
                  <Button
                    type="button"
                    className=""
                    variant="contained"
                    color="primary"
                    size="small"
                    onClick={loginAccount}
                  >
                    {"Login Stripe"}
                  </Button>
                </>
              }
            </div>
            {(stripe.charges_enabled || stripe.payouts_enabled) && list &&
              <div>
                {transactions.length > 0 ?
                  <>
                    <div className="list-pagination">
                      <Pagination
                        rowsPerPageOptions={[5, 10]}
                        component="div"
                        count={transactions.length}
                        rowsPerPage={filters.pageSize}
                        page={filters.pageIndex - 1}
                        onPageChange={handleChangePage}
                        onRowsPerPageChange={handleChangeRowsPerPage} />
                    </div>
                    <ul className="responsive-table stripe-transaction-table py-2">
                      {(transactions).map((value, index) => (
                        <li className={(index >= filters.startIndex && index <= filters.endIndex) ? "table-row" : "table-row d-none"} key={index}>
                          {/* <div className="col col-1" data-label="Job Id"><label>sr.</label><p>{index + 1}</p></div> */}
                          {/* <div className="col col-3" data-label="Amount"><label>Name</label><p><Link to={`/dashboard/product-detail/${value.productId}`}>{value.productName}</Link></p></div> */}
                          <div className="col col-8" data-label="Payment Status"><label>Seller</label>
                            <p>
                              <Link to={`/dashboard/seller-profile/${value.UserId}`}>{(value.SellerName)}</Link>
                            </p>
                          </div>
                          <div className="col col-8" data-label="Payment Status"><label>Buyer</label>
                            <p>
                              <Link to={`/dashboard/seller-profile/${value.UserId}`}>{(value.BuyerName)}</Link>
                            </p>
                          </div>
                          <div className="col col-5" data-label="Amount"><label>Amount (SGD)</label><p>{value.Amount}</p></div>
                          <div className="col col-6" data-label="Amount"><label>Net Amount (SGD)</label><p>{value.NetAmount}</p></div>
                          <div className="col col-10" data-label="Amount"><label>Due Date</label><p>{moment(value.DueDate).format('MMM DD, YYYY')}</p></div>
                          <div className="col col-4" data-label="Payment Status"><label>Status</label>
                            <p>
                              {(value.IsPaid) ?
                                <span
                                  style={{
                                    background: "green",
                                    padding: "2px 10px",
                                    borderRadius: "15px",
                                    color: "white",
                                    fontWeight: "600",
                                    fontSize: "14px"
                                  }}
                                >
                                  {"Transfer"}
                                </span>
                                :
                                <span
                                  style={{
                                    background: "#d39e00",
                                    padding: "2px 10px",
                                    borderRadius: "15px",
                                    color: "white",
                                    fontWeight: "600",
                                    fontSize: "14px"
                                  }}
                                >
                                  {"Pending"}
                                </span>
                              }
                            </p>
                          </div>
                        </li>
                      ))}
                    </ul>
                    <div className="list-pagination">
                      <Pagination
                        rowsPerPageOptions={[5, 10]}
                        component="div"
                        count={transactions.length}
                        rowsPerPage={filters.pageSize}
                        page={filters.pageIndex - 1}
                        onPageChange={handleChangePage}
                        onRowsPerPageChange={handleChangeRowsPerPage} />
                    </div>
                  </>
                  :
                  <ul className="responsive-table stripe-transaction-table py-2">
                    <li>
                      <p>No transaction found</p>
                    </li>
                  </ul>
                }
              </div>
            }
          </>
          :
          <div className="d-flex justify-content-between">
            <div className="d-flex">
              <p>You have no stripe account. Create first!!</p>
            </div>
            <div>
              <Button
                type="button"
                className=""
                variant="contained"
                color="primary"
                size="small"
                onClick={openStripeAccount}
              >
                {"Create Stripe"}
              </Button>
            </div>
          </div>
      }

    </div>

  </>
}

export default StripeDashboard;
