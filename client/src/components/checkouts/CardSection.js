/**
* Use the CSS tab above to style your Element's container.
*/
import React from 'react';
import { CardElement } from '@stripe/react-stripe-js';
import '../../assets/css/payment.css';
import FormControl from '@material-ui/core/FormControl';
import { Form } from "react-bootstrap";

const CARD_ELEMENT_OPTIONS = {
    style: {
        base: {
            color: "#32325d",
            fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
            fontSmoothing: "antialiased",
            fontSize: "16px",
            "::placeholder": {
                color: "#aab7c4",
            },
        },
        invalid: {
            color: "#fa755a",
            iconColor: "#fa755a",
        },
    },
};
function CardSection() {
    return (
        <div className="custom-form-control">
            <label className='custom-control-label'>Card details</label>
            <CardElement options={CARD_ELEMENT_OPTIONS} />
        </div>
        

        // <Form.Group controlid="cardDetails">
        //     <FormControl variant="outlined" className="wp-100">
        //         <CardElement options={CARD_ELEMENT_OPTIONS} />
        //     </FormControl>
        // </Form.Group>
    );
};
export default CardSection;