import React from 'react';
import { Form } from "react-bootstrap";
import Button from '@material-ui/core/Button';
import { ElementsConsumer, CardElement } from '@stripe/react-stripe-js';
import { withAlert } from 'react-alert';
import { withRouter } from "react-router-dom";
import CardSection from './CardSection';
import LoaderComp from '../Loader';
import ApiRequest from '../../api_service/api_requests';

class CheckoutForm extends React.Component {
    constructor() {
        super();
        this.state = {
            loaderMessage: '',
            displayLoader: false
        };
    }

    handleSubmit = async (event) => {
        // We don't want to let default form submission happen here,
        // which would refresh the page.
        event.preventDefault();

        const { stripe, elements } = this.props

        if (!stripe || !elements) {
            // Stripe.js has not yet loaded.
            // Make  sure to disable form submission until Stripe.js has loaded.
            return;
        }

        const card = elements.getElement(CardElement);
        // console.log("CardElement", CardElement);
        const result = await stripe.createToken(card);

        if (result.error) {
            // Show error to your customer.
            // console.log(result.error.message);
            this.props.alert.error(result.error.message);
        } else {
            // Send the token to your server.
            // This function does not exist yet; we will define it in the next step.
            this.proceedPayment(result.token);
        }
    };

    proceedPayment = token => {
        if (token && token.id) {
            this.setState({ loaderMessage: "Placing Order", displayLoader: true });
            let payload = {
                tokenId: token.id,
                orderAmount: this.props.totalPrice || 0,
                cartIds: this.props.ids || []
            };

            ApiRequest.placeOrder(payload)
                .then(res => {
                    if (res && res.message) {
                        if (res.statusCode && res.statusCode === 201) {
                            this.props.alert.success(res.message);
                            this.props.history.push('/dashboard/profile');
                        } else {
                            this.props.alert.error(res.message);
                        }
                    } else {
                        this.props.alert.error("Something went wrong");
                    }
                    this.setState({ loaderMessage: "", displayLoader: false });
                })
                .catch((err) => {
                    this.setState({ loaderMessage: "", displayLoader: false });
                    // console.log('Payment api error, ', err);
                })
        } else {
            this.props.alert.error("Missing payment details");
        }
    }

    render() {
        return (
            <LoaderComp show={this.state.displayLoader} message={this.state.loaderMessage}>
                {/* <Row className="mt-3">
                    <Col md={6}> */}
                        <Form.Group controlId="postal">
                            <CardSection />
                            <Button
                                type="submit"
                                className="mt-5"
                                variant="contained"
                                color="primary"
                                onClick={this.handleSubmit}
                                disabled={!this.props.stripe}>
                                Place Order
                            </Button>
                        </Form.Group>
                    {/* </Col>
                </Row> */}
            </LoaderComp>
        );
    }
}

function Payment(props) {
    return (
        <ElementsConsumer>
            {({ stripe, elements }) => (
                <CheckoutForm stripe={stripe} elements={elements} {...props} />
            )}
        </ElementsConsumer>
    );
}

export default withRouter(withAlert()(Payment));