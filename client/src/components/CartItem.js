import React from "react";
import { Col, Row } from "react-bootstrap";
import { withAlert } from 'react-alert';
import { ReactComponent as MinusIcon } from "../assets/img/icons/minusIcon.svg";
import { ReactComponent as PlusIcon } from "../assets/img/icons/plusIcon.svg";
import ApiRequest from '../api_service/api_requests';
import * as Constants from '../constant';

class CartItem extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      itemCount: 1
    };
  }

  incQty = () => {
    this.setState({ itemCount: this.state.itemCount + 1 });
  }

  decQty = () => {
    if (this.state.itemCount > 1) {
      this.setState({ itemCount: this.state.itemCount - 1 });
    }
  }

  deleteItem = productId => {
    ApiRequest.deleteCartItem(productId)
      .then(res => {
        if (res && res.message) {
          if (res.statusCode && res.statusCode === 200) {
            this.props.getCartItems();
            this.props.alert.success(res.message);
          } else {
            this.props.alert.error(res.message);
          }
        } else {
          this.props.alert.error("Something went wrong");
        }
      })
      .catch((err) => {
        // console.log('View Cart api error, ', err);
      })
  }

  render() {
    let productId = 1;
    let name = '';
    let price = 0;
    let imgUrl = 'https://cdn.shopify.com/s/files/1/0609/6152/1863/products/DiRenJie_300x.png';

    if (this.props && this.props.data) {
      let data = this.props.data;

      productId = data.id || productId;
      name = data.productName || name;
      price = Math.round(Number(data.productPrice || price) * 100) / 100;
      imgUrl = data.productImage ? data.productImage : imgUrl;
    }

    return (
      <Row className="border-bottom py-3">
        <Col sm={6}>
          <div className="cart_product d-flex align-items-center">
            <div className="p-2 bg-dark d-flex" style={{ width: "100px", height: "100px" }}>
              <img src={imgUrl} alt={name} />
            </div>
            <div className="pl-3 pl-md-4">
              <h6 className="mb-2 pb-1">{name}</h6>
              <p className="mb-0">${price}</p>
              {/* <p className="mb-1">Style: Tang Dynasty Heros</p> */}
              {/* <Link to={`/dashboard/seller-profile/${1}`} className="product-card__body--seller">
                {'NA'}
              </Link> */}
            </div>
          </div>
        </Col>
        <Col sm={3}>
          <div className="cart-items-details d-flex align-items-center mt-sm-0 mt-3 mb-3">
            {/* <button
              className="plus rounded-0"
              onClick={this.decQty}>
              <MinusIcon />
            </button> */}
            <div className="mint-amount rounded-0 mx-2 text-center d-flex justify-content-center align-items-center">
              <span>{this.state.itemCount}</span>
            </div>
            {/* <button
              className="plus rounded-0 "
              onClick={this.incQty}>
              <PlusIcon />
            </button> */}
            <span className="remove-cart-item"
              onClick={event => {
                event.preventDefault();
                this.deleteItem(productId);
              }}>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 16 16"
                aria-hidden="true"
                focusable="false"
                role="presentation"
                className="icon icon-remove">
                <path
                  d="M14 3h-3.53a3.07 3.07 0 00-.6-1.65C9.44.82 8.8.5 8 .5s-1.44.32-1.87.85A3.06 3.06 0 005.53 3H2a.5.5 0 000 1h1.25v10c0 .28.22.5.5.5h8.5a.5.5 0 00.5-.5V4H14a.5.5 0 000-1zM6.91 1.98c.23-.29.58-.48 1.09-.48s.85.19 1.09.48c.2.24.3.6.36 1.02h-2.9c.05-.42.17-.78.36-1.02zm4.84 11.52h-7.5V4h7.5v9.5z"
                  fill="currentColor"></path>
                <path
                  d="M6.55 5.25a.5.5 0 00-.5.5v6a.5.5 0 001 0v-6a.5.5 0 00-.5-.5zM9.45 5.25a.5.5 0 00-.5.5v6a.5.5 0 001 0v-6a.5.5 0 00-.5-.5z"
                  fill="currentColor"></path>
              </svg>
            </span>
          </div>
        </Col>
        <Col sm={3}>
          <div className="cart-item__price">${price}</div>
        </Col>
      </Row>
    )
  }
};

export default withAlert()(CartItem);