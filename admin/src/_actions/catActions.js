import {
    POST_ADD_CATEGORY_REQUEST, GET_ADD_CATEGORY_SUCCESS, GET_ADD_CATEGORY_FAILURE,
    POST_CATEGORIES_REQUEST, GET_CATEGORIES_SUCCESS, GET_CATEGORIES_FAILURE,
    POST_CATEGORY_DELETE_REQUEST, GET_CATEGORY_DELETE_SUCCESS, GET_CATEGORY_DELETE_FAILURE,
    POST_CATEGORY_GET_REQUEST, GET_CATEGORY_GET_SUCCESS, GET_CATEGORY_GET_FAILURE,
    POST_UPDATE_CATEGORY_REQUEST, GET_UPDATE_CATEGORY_SUCCESS, GET_UPDATE_CATEGORY_FAILURE,
    POST_CATEGORY_IMAGE_REQUEST, GET_CATEGORY_IMAGE_SUCCESS, GET_CATEGORY_IMAGE_FAILURE,
    POST_PARENT_CATEGORIES_REQUEST, GET_PARENT_CATEGORIES_SUCCESS, GET_PARENT_CATEGORIES_FAILURE,
    POST_SUB_CATEGORIES_REQUEST, GET_SUB_CATEGORIES_SUCCESS, GET_SUB_CATEGORIES_FAILURE,
    POST_ADD_SUBCATEGORY_REQUEST,GET_ADD_SUBCATEGORY_SUCCESS,GET_ADD_SUBCATEGORY_FAILURE,
    POST_SUBCATEGORIES_REQUEST,GET_SUBCATEGORIES_SUCCESS,GET_SUBCATEGORIES_FAILURE,POST_UPDATE_SUBCATEGORY_REQUEST,
    GET_UPDATE_SUBCATEGORY_SUCCESS,GET_UPDATE_SUBCATEGORY_FAILURE,POST_SUBCATEGORY_DELETE_REQUEST,
    GET_SUBCATEGORY_DELETE_SUCCESS,GET_SUBCATEGORY_DELETE_FAILURE
} from '../_actions/_type';
import {
    URL_ADMIN_GET_CATEGORIES_LIST, URL_ADMIN_GET_SUB_CATS_LIST, URL_ADMIN_GET_PARENT_CAT_LIST,
    URL_ADMIN_ADD_CATEGORY, URL_ADMIN_DELETE_CATEGORY, URL_ADMIN_GET_SINGLE_CATEGORY,
    URL_ADMIN_UPDATE_CATEGORY,URL_ADMIN_ADD_SUBCATEGORY,
    URL_ADMIN_GET_SUBCATEGORIES_LIST,URL_ADMIN_DELETE_SUBCATEGORY,URL_ADMIN_UPDATE_SUBCATEGORY
} from '../utils/admin-api-url.js';
import { reactLocalStorage } from 'reactjs-localstorage';
import { toast } from 'react-toastify';
import axios from 'axios';


// Update Category
export const addCategory = (catData, history) => async dispatch => {
    try {
        dispatch({ type: POST_ADD_CATEGORY_REQUEST, payload: true });
        const response = await axios.post(URL_ADMIN_ADD_CATEGORY, catData, {
            headers: {
                Accept: 'application/json',
                'Content-type': 'application/json',
                'Authorization': `bearer ${reactLocalStorage.getObject('access-token')}`
            }
        });
        if (response.data.statusCode === 200) {
            dispatch({ type: GET_ADD_CATEGORY_SUCCESS, payload: response.data });
            history.push('/categories-list');
            toast.success(response.data.message, { position: toast.POSITION.TOP_RIGHT });
        } else {
            toast.error(response.data.message, { position: toast.POSITION.TOP_RIGHT });
        }
    } catch (error) {
        dispatch({ type: GET_ADD_CATEGORY_FAILURE, payload: error });
    }
};

// Get All users list
export const getCategories = () => async dispatch => {
    try {
        dispatch({ type: POST_CATEGORIES_REQUEST, payload: true });
        const response = await axios.get(URL_ADMIN_GET_CATEGORIES_LIST, {}, {
            headers: {
                Accept: 'application/json',
                'Content-type': 'application/json',
                'Authorization': `bearer ${reactLocalStorage.getObject('access-token')}`
            }
        });
        if (response.data.statusCode === 200) {
            dispatch({ type: GET_CATEGORIES_SUCCESS, payload: response.data.data });
        } else {
            toast.error(response.message, { position: toast.POSITION.TOP_RIGHT });
        }
    } catch (error) {
        dispatch({ type: GET_CATEGORIES_FAILURE, payload: error });
    }
};

// Delete Category
export const deleteCategory = ID => async dispatch => {
    try {
        dispatch({ type: POST_CATEGORY_DELETE_REQUEST, payload: true });
        const response = await axios.delete(`${URL_ADMIN_DELETE_CATEGORY}?Id=${ID}`, {
            headers: {
                Accept: 'application/json',
                'Content-type': 'application/json',
                'Authorization': `bearer ${reactLocalStorage.getObject('access-token')}`
            }
        });
        if (response.data.statusCode === 200) {
            dispatch({ type: GET_CATEGORY_DELETE_SUCCESS, payload: response.data });
            toast.success(response.data.message, { position: toast.POSITION.TOP_RIGHT });
        } else {
            toast.error(response.data.message, { position: toast.POSITION.TOP_RIGHT });
        }
    } catch (error) {
        dispatch({ type: GET_CATEGORY_DELETE_FAILURE, payload: error });
    }
};


// Update Category
export const updateCategory = (catData, history) => async dispatch => {
    try {
        dispatch({ type: POST_UPDATE_CATEGORY_REQUEST, payload: true });
        const response = await axios.put(URL_ADMIN_UPDATE_CATEGORY, catData, {
            headers: {
                Accept: 'application/json',
                'Content-type': 'application/json',
                'Authorization': `bearer ${reactLocalStorage.getObject('access-token')}`
            }
        });
        console.log("asjas",response);
        if (response.data.statusCode === 200) {
            dispatch({ type: GET_UPDATE_CATEGORY_SUCCESS, payload: response.data });
            history.push('/categories-list');
            toast.success(response.data.message, { position: toast.POSITION.TOP_RIGHT });
        } else {
            toast.error(response.data.message, { position: toast.POSITION.TOP_RIGHT });
        }
    } catch (error) {
        dispatch({ type: GET_UPDATE_CATEGORY_FAILURE, payload: error });
    }
};
// start Sub category section 

// Delete Sub Category
export const deleteSubCategory = ID => async dispatch => {
    try {
        dispatch({ type: POST_SUBCATEGORY_DELETE_REQUEST, payload: true });
        const response = await axios.delete(`${URL_ADMIN_DELETE_SUBCATEGORY}?Id=${ID}`, {
            headers: {
                Accept: 'application/json',
                'Content-type': 'application/json',
                'Authorization': `bearer ${reactLocalStorage.getObject('access-token')}`
            }
        });
        if (response.data.statusCode === 200) {
            dispatch({ type: GET_SUBCATEGORY_DELETE_SUCCESS, payload: response.data });
            toast.success(response.data.message, { position: toast.POSITION.TOP_RIGHT });
        } else {
            toast.error(response.data.message, { position: toast.POSITION.TOP_RIGHT });
        }
    } catch (error) {
        dispatch({ type: GET_SUBCATEGORY_DELETE_FAILURE, payload: error });
    }
};

// Update Sub Category
export const updateSubCategory = (catData, history) => async dispatch => {
    try {
        dispatch({ type: POST_UPDATE_SUBCATEGORY_REQUEST, payload: true });
        const response = await axios.put(URL_ADMIN_UPDATE_SUBCATEGORY, catData, {
            headers: {
                Accept: 'application/json',
                'Content-type': 'application/json',
                'Authorization': `bearer ${reactLocalStorage.getObject('access-token')}`
            }
        });
        if (response.data.statusCode === 200) {
            dispatch({ type: GET_UPDATE_SUBCATEGORY_SUCCESS, payload: response.data });
            history.push('/sub-categories-list');
            toast.success(response.data.message, { position: toast.POSITION.TOP_RIGHT });
        } else {
            toast.error(response.data.message, { position: toast.POSITION.TOP_RIGHT });
        }
    } catch (error) {
        dispatch({ type: GET_UPDATE_SUBCATEGORY_FAILURE, payload: error });
    }
};


// Update Category
export const addSubCategory = (catData, history) => async dispatch => {
    try {
        dispatch({ type: POST_ADD_SUBCATEGORY_REQUEST, payload: true });
        const response = await axios.post(URL_ADMIN_ADD_SUBCATEGORY, catData, {
            headers: {
                Accept: 'application/json',
                'Content-type': 'application/json',
                'Authorization': `bearer ${reactLocalStorage.getObject('access-token')}`
            }
        });
        if (response.data.statusCode === 200) {
            dispatch({ type: GET_ADD_SUBCATEGORY_SUCCESS, payload: response.data });
            history.push('/sub-categories-list');
            toast.success(response.data.message, { position: toast.POSITION.TOP_RIGHT });
        } else {
            toast.error(response.data.message, { position: toast.POSITION.TOP_RIGHT });
        }
    } catch (error) {
        dispatch({ type: GET_ADD_SUBCATEGORY_FAILURE, payload: error });
    }
};

// Get All users list
export const getSubCategories = () => async dispatch => {
    try {
        dispatch({ type: POST_SUBCATEGORIES_REQUEST, payload: true });
        const response = await axios.get(URL_ADMIN_GET_SUBCATEGORIES_LIST, {}, {
            headers: {
                Accept: 'application/json',
                'Content-type': 'application/json',
                'Authorization': `bearer ${reactLocalStorage.getObject('access-token')}`
            }
        });
        console.log('response',response);
        if (response.status === 200) {
            dispatch({ type: GET_SUBCATEGORIES_SUCCESS, payload: response.data });
        } else {
            toast.error(response.message, { position: toast.POSITION.TOP_RIGHT });
        }
    } catch (error) {
        dispatch({ type: GET_SUBCATEGORIES_FAILURE, payload: error });
    }
};