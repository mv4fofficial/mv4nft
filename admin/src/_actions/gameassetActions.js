import {
  POST_ADD_GAMEASSET_REQUEST, GET_ADD_GAMEASSET_SUCCESS, GET_ADD_GAMEASSET_FAILURE,
  POST_GAMEASSETS_REQUEST, GET_GAMEASSETS_SUCCESS, GET_GAMEASSETS_FAILURE,
} from '../_actions/_type';
import {
  URL_GAME_ADD_GAMEASSET, URL_GAME_VIEW_GAMEASSETS
} from '../utils/admin-api-url.js';
import {reactLocalStorage} from 'reactjs-localstorage';
import {toast} from 'react-toastify';
import axios from 'axios';


// Add Game Asset 
export const addGameAsset = (data, history) => async dispatch => {
  try {
    dispatch({type: POST_ADD_GAMEASSET_REQUEST, payload: true});
    const response = await axios.post(URL_GAME_ADD_GAMEASSET, data, {
      headers: {
        Accept: 'application/json',
        'Content-type': 'application/json',
        'Authorization': `bearer ${reactLocalStorage.getObject('access-token')}`
      }
    });
    if (response.data.statusCode === 200) {
      dispatch({type: GET_ADD_GAMEASSET_SUCCESS, payload: response.data});
      history.push('/gameassets-list');
      toast.success(response.data.message, {position: toast.POSITION.TOP_RIGHT});
    } else {
      toast.error(response.data.message, {position: toast.POSITION.TOP_RIGHT});
    }
  } catch (error) {
    dispatch({type: GET_ADD_GAMEASSET_FAILURE, payload: error});
  }
};

// Get All gameassets list
export const getGameassets = () => async dispatch => {
  try {
    dispatch({type: POST_GAMEASSETS_REQUEST, payload: true});
    const response = await axios.get(URL_GAME_VIEW_GAMEASSETS, {}, {
      headers: {
        Accept: 'application/json',
        'Content-type': 'application/json',
        'Authorization': `bearer ${reactLocalStorage.getObject('access-token')}`
      }
    });
    if (response.data.statusCode === 200) {
      dispatch({type: GET_GAMEASSETS_SUCCESS, payload: response.data.data});
    } else {
      toast.error(response.message, {position: toast.POSITION.TOP_RIGHT});
    }
  } catch (error) {
    dispatch({type: GET_GAMEASSETS_FAILURE, payload: error});
  }
};


