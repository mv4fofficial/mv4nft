import {
    POST_ADMIN_LOGIN_REQUEST, GET_ADMIN_LOGIN_SUCCESS, GET_ADMIN_LOGIN_FAILURE,
} from './_type';
import {
    URL_ADMIN_LOGIN
} from '../utils/admin-api-url.js';
import { reactLocalStorage } from 'reactjs-localstorage';
import { toast } from 'react-toastify';
import axios from 'axios';


// Admin User login
export const login = (userData, history) => async dispatch => {
    try {
        dispatch({ type: POST_ADMIN_LOGIN_REQUEST, payload: true });
        const response = await axios.put(URL_ADMIN_LOGIN, userData, {
            headers: { 'Content-Type': 'application/json' }
        });
        if (response.data.statusCode === 200) {
            dispatch({ type: GET_ADMIN_LOGIN_SUCCESS, payload: response.data });
            reactLocalStorage.setObject('access-token', response.data.data.accessToken);
            reactLocalStorage.setObject('Auth', response.data.data);
            toast.success(response.data.message, { position: toast.POSITION.TOP_RIGHT });
            history.push('/dashboard');
        } else {
            toast.error(response.data.message, { position: toast.POSITION.TOP_RIGHT });
        }
    } catch (error) {
        dispatch({ type: GET_ADMIN_LOGIN_FAILURE, payload: error });
    }
};

