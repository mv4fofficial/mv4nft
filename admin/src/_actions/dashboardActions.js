import {
    POST_DASHBOARD_REQUEST, GET_DASHBOARD_SUCCESS, GET_DASHBOARD_FAILURE,
    POST_EARNING_REPORT_REQUEST, GET_EARNING_REPORT_SUCCESS, GET_EARNING_REPORT_FAILURE,
    POST_TRANS_REPORT_REQUEST,GET_TRANS_REPORT_SUCCESS,GET_TRANS_REPORT_FAILURE,
    POST_STRIPE_TRANS_REPORT_REQUEST,GET_STRIPE_TRANS_REPORT_SUCCESS,GET_STRIPE_TRANS_REPORT_FAILURE,
    POST_STRIPE_ACCOUNTS_REPORT_REQUEST,GET_STRIPE_ACCOUNTS_REPORT_SUCCESS,GET_STRIPE_ACCOUNTS_REPORT_FAILURE,
    POST_STRIPE_BALANCE_REPORT_REQUEST,GET_STRIPE_BALANCE_REPORT_SUCCESS,GET_STRIPE_BALANCE_REPORT_FAILURE,
    POST_PRODUCT_REPORT_REQUEST,GET_PRODUCT_REPORT_SUCCESS,GET_PRODUCT_REPORT_FAILURE
} from '../_actions/_type';
import {
    URL_ADMIN_GET_DASHBOARD_DATA, URL_ADMIN_EARNING_REPORT,URL_ADMIN_TRANS_REPORT,URL_ADMIN_PRODUCT_REPORT,URL_ADMIN_STRIPE_TRANS_REPORT,URL_ADMIN_STRIPE_ACCOUNTS_REPORT,
    URL_ADMIN_STRIPE_BALANCE_REPORT
} from '../utils/admin-api-url.js';
import { toast } from 'react-toastify';
import { reactLocalStorage } from 'reactjs-localstorage';
import axios from 'axios';

// Get All dashboard content
export const getdashboard = () => async dispatch => {
    try {
        dispatch({ type: POST_DASHBOARD_REQUEST, payload: true });
        const response = await axios.post(URL_ADMIN_GET_DASHBOARD_DATA, {}, {
            headers: {
                Accept: 'application/json',
                'Content-type': 'application/json',
                'x-access-token': reactLocalStorage.getObject('access-token')
            }
        });
        if (response.data.status === true) {
            dispatch({ type: GET_DASHBOARD_SUCCESS, payload: response.data.data });
        } else {
            toast.error(response.message, { position: toast.POSITION.TOP_RIGHT });
        }
    } catch (error) {
        dispatch({ type: GET_DASHBOARD_FAILURE, payload: error });
    }
};

//Get earning report.
export const getEarings = (data) => async dispatch => {
    try {
        dispatch({ type: POST_EARNING_REPORT_REQUEST, payload: true });
        const response = await axios.post(URL_ADMIN_EARNING_REPORT, {data}, {
            headers: {
                Accept: 'application/json',
                'Content-type': 'application/json',
                'x-access-token': reactLocalStorage.getObject('access-token')
            }
        });
        if (response.data.status === true) {
            dispatch({ type: GET_EARNING_REPORT_SUCCESS, payload: response.data.data });
        } else {
            toast.error(response.message, { position: toast.POSITION.TOP_RIGHT });
        }
    } catch (error) {
        dispatch({ type: GET_EARNING_REPORT_FAILURE, payload: error });
    }
};

//Get Transactions reports.
export const getTransactions = (data) => async dispatch => {
    try {
        dispatch({ type: POST_TRANS_REPORT_REQUEST, payload: true });
        const response = await axios.get(URL_ADMIN_TRANS_REPORT, {
            headers: {
                Accept: 'application/json',
                'Content-type': 'application/json',
                'Authorization': `bearer ${reactLocalStorage.getObject('access-token')}`
            }
        });
        if (response.data.statusCode === 200) {
            dispatch({ type: GET_TRANS_REPORT_SUCCESS, payload: response.data.data });
        } else {
            toast.error(response.message, { position: toast.POSITION.TOP_RIGHT });
        }
    } catch (error) {
        dispatch({ type: GET_TRANS_REPORT_FAILURE, payload: error });
    }
};

//Get Stripe Transactions reports.
export const getStripeTransactions = (data) => async dispatch => {
    try {
        dispatch({ type: POST_STRIPE_TRANS_REPORT_REQUEST, payload: true });
        const response = await axios.post(URL_ADMIN_STRIPE_TRANS_REPORT, {
            headers: {
                Accept: 'application/json',
                'Content-type': 'application/json',
                'Authorization': `bearer ${reactLocalStorage.getObject('access-token')}`
            }
        });
        if (response.data.statusCode === 200) {
            dispatch({ type: GET_STRIPE_TRANS_REPORT_SUCCESS, payload: response.data.data.data });
        } else {
            toast.error(response.message, { position: toast.POSITION.TOP_RIGHT });
        }
    } catch (error) {
        console.log(error);
        dispatch({ type: GET_STRIPE_TRANS_REPORT_FAILURE, payload: error });
    }
};

//Get Stripe Transactions reports.
export const getBalance = (data) => async dispatch => {
    try {
        dispatch({ type: POST_STRIPE_BALANCE_REPORT_REQUEST, payload: true });
        const response = await axios.post(URL_ADMIN_STRIPE_BALANCE_REPORT, {data}, {
            headers: {
                Accept: 'application/json',
                'Content-type': 'application/json',
                'Authorization': `bearer ${reactLocalStorage.getObject('access-token')}`
            }
        });
        if (response.data.statusCode === 200) {
            dispatch({ type: GET_STRIPE_BALANCE_REPORT_SUCCESS, payload: response.data.data });
        } else {
            toast.error(response.message, { position: toast.POSITION.TOP_RIGHT });
        }
    } catch (error) {
        console.log(error);
        dispatch({ type: GET_STRIPE_BALANCE_REPORT_FAILURE, payload: error });
    }
};

//Get Stripe User Account reports.
export const getStripeAccountList = (data) => async dispatch => {
    try {
        dispatch({ type: POST_STRIPE_ACCOUNTS_REPORT_REQUEST, payload: true });
        const response = await axios.post(URL_ADMIN_STRIPE_ACCOUNTS_REPORT, {
            headers: {
                Accept: 'application/json',
                'Content-type': 'application/json',
                'Authorization': `bearer ${reactLocalStorage.getObject('access-token')}`
            }
        });
        if (response.data.statusCode === 200) {
            dispatch({ type: GET_STRIPE_ACCOUNTS_REPORT_SUCCESS, payload: response.data.data.data });
        } else {
            toast.error(response.message, { position: toast.POSITION.TOP_RIGHT });
        }
    } catch (error) {
        console.log(error);
        dispatch({ type: GET_STRIPE_ACCOUNTS_REPORT_FAILURE, payload: error });
    }
};

//Get getProducts reports.
export const getProducts = (data) => async dispatch => {
    try {
        dispatch({ type: POST_PRODUCT_REPORT_REQUEST, payload: true });
        const response = await axios.post(URL_ADMIN_PRODUCT_REPORT,data, {
            headers: {
                Accept: 'application/json',
                'Content-type': 'application/json',
                'Authorization': `bearer ${reactLocalStorage.getObject('access-token')}`
            }
        });
        if (response.data.statusCode === 200) {
            dispatch({ type: GET_PRODUCT_REPORT_SUCCESS, payload: response.data.data.data });
        } else {
            toast.error(response.message, { position: toast.POSITION.TOP_RIGHT });
        }
    } catch (error) {
        dispatch({ type: GET_PRODUCT_REPORT_FAILURE, payload: error });
    }
};

