import {
    POST_USERS_REQUEST, GET_USERS_SUCCESS, GET_USERS_FAILURE,
    POST_USER_REQUESTS_REQUEST, GET_USER_REQUESTS_SUCCESS, GET_USER_REQUESTS_FAILURE,
    POST_ACTION_REQUEST_REQUEST, GET_ACTION_REQUEST_SUCCESS, GET_ACTION_REQUEST_FAILURE,
} from '../_actions/_type';
import {
    URL_ADMIN_GET_USERS_LIST, URL_ADMIN_GET_USER_REQUEST_LIST, URL_ADMIN_ACTION_USER_REQUEST
} from '../utils/admin-api-url.js';
import { reactLocalStorage } from 'reactjs-localstorage';
import { toast } from 'react-toastify';
import axios from 'axios';


// Get All users list
export const getUsers = () => async dispatch => {
    try {
        dispatch({ type: POST_USERS_REQUEST, payload: true });
        const response = await axios.get(URL_ADMIN_GET_USERS_LIST, {
            headers: {
                Accept: 'application/json',
                'Content-type': 'application/json',
                'Authorization': `bearer ${reactLocalStorage.getObject('access-token')}`
            }
        });
        if (response.data.statusCode === 200) {
            dispatch({ type: GET_USERS_SUCCESS, payload: response.data.data });
        } else {
            toast.error(response.message, { position: toast.POSITION.TOP_RIGHT });
        }
    } catch (error) {
        dispatch({ type: GET_USERS_FAILURE, payload: error });
    }
};

export const getUserRequests = () => async dispatch => {
    try {
        dispatch({ type: POST_USER_REQUESTS_REQUEST, payload: true });
        const response = await axios.get(URL_ADMIN_GET_USER_REQUEST_LIST, {
            headers: {
                Accept: 'application/json',
                'Content-type': 'application/json',
                'Authorization': `bearer ${reactLocalStorage.getObject('access-token')}`
            }
        });
        if (response.data.statusCode === 200) {
            dispatch({ type: GET_USER_REQUESTS_SUCCESS, payload: response.data.data });
        } else {
            toast.error(response.message, { position: toast.POSITION.TOP_RIGHT });
        }
    } catch (error) {
        dispatch({ type: GET_USER_REQUESTS_FAILURE, payload: error });
    }
};

export const actionUserRequest = (data) => async dispatch => {
    try {
        dispatch({ type: POST_ACTION_REQUEST_REQUEST, payload: true });
        const response = await axios.post(URL_ADMIN_ACTION_USER_REQUEST,data, {
            headers: {
                Accept: 'application/json',
                'Content-type': 'application/json',
                'Authorization': `bearer ${reactLocalStorage.getObject('access-token')}`
            }
        });
        if (response.data.statusCode === 200) {
            dispatch({ type: GET_ACTION_REQUEST_SUCCESS, payload: response.data.data });
        } else {
            toast.error(response.message, { position: toast.POSITION.TOP_RIGHT });
        }
        return response;
    } catch (error) {
        dispatch({ type: GET_ACTION_REQUEST_FAILURE, payload: error });
    }
};

