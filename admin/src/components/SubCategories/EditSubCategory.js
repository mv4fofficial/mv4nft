import React,{Component} from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { API_URL } from '../../utils/admin-api-url';
import 'react-toastify/dist/ReactToastify.css';
import Header from '../CommonTemplates/Header.js';
import Sidebar from '../CommonTemplates/SideBar.js';
import {  getSubCategories, updateSubCategory } from '../../_actions/catActions.js';

class EditSubCategory extends Component { 
    state = {
        readOnly: true,
        fields: {},
        errors: {},
        form_data:{},
        catImage:'',
    }

    async componentDidMount() {
        console.log("this",this.props);
        const id = this.props.match.params.id;
        await this.props.getSubCategories();
        var catdata =  this.findArrayElementByCat(this.props.categories.subCategories,id)
        var fields = this.state.fields;
        Object.keys(catdata)
            .forEach(function eachKey(key) {               
                fields[key] = catdata[key];                
            });
        this.setState(fields);
    }

     findArrayElementByCat = (array, id) =>  {
       return array.find((element) => {
         return element.id == id;
        })
      }

 
    // Input handle  
    handleChange = (e) => { 
        let fields = this.state.fields;
        fields[ e.target.name ] = e.target.value; 
        this.setState(fields);
    }

    handleValidation(){
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;
        if(!fields[ 'subCategoryName' ].match(/^[a-zA-Z ]+$/)){
            formIsValid = false;
            errors[ 'subCategoryName' ] = 'Please enter sub category name.';
         }
      
        this.setState({errors: errors});
        return formIsValid;
    }

    //Handle Fom Submit 
    handleSubmit = async (e) => {
        e.preventDefault();
        if(this.handleValidation()){
            
            const catData = {
                subCategoryId        : this.props.match.params.id,
                subCategoryName        : this.state.fields[ 'subCategoryName' ],
            };
            await this.props.updateSubCategory(catData, this.props.history);
        }           
    }

    render() {
        return (
            <React.Fragment>
                <div className="container-fluid">
                    <div className="row">
                        <Sidebar/>
                        <main className="main-content col-lg-10 col-md-9 col-sm-12 p-0 offset-lg-2 offset-md-3">
                                <Header/>
                                <div className="main-content-container container-fluid px-4">
                                    <div className="page-header row no-gutters py-4">
                                        <div className="col-12 col-sm-4 text-center text-sm-left mb-0">
                                            <span className="text-uppercase page-subtitle">Overview</span>
                                            <h3 className="page-title">Edit Sub Category</h3>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-lg-12">
                                            <div className="card card-small mb-4">
                                                <div className="card-header border-bottom">
                                                    <h6 className="m-0">Sub Category Details</h6>
                                                </div>
                                                <ul className="list-group list-group-flush">
                                                    <li className="list-group-item p-3">
                                                        <div className="row">
                                                            <div className="col">
                                                                <form onSubmit={this.handleSubmit}>
                                                                    <div className="form-row">
                                                                        <div className="form-group col-md-6">
                                                                            <label for="feFirstName">Sub Category Name</label>
                                                                            <input type="text" className="form-control" id="subCategoryName" name="subCategoryName" placeholder="Full Name" value={this.state.fields[ 'subCategoryName' ]} onChange={this.handleChange}/> 
                                                                            <span style={ { color : 'red' } }>{this.state.errors[ 'categorsubCategoryNameyName' ]}</span>
                                                                        </div>
                                                                    </div>
                                                                  
                                                                    <button type="submit" className="btn btn-success">Update</button>&nbsp;
                                                                    <Link to="/sub-categories-list" className="btn btn-danger">Cancel</Link>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                            {/* <Footer/> */}
                        </main>
                    </div>
                </div>
            </React.Fragment>   
        );
    }
}

const mapStateToProps = state => ({
    categories  : state.categories,
    subCategories  : state.categories.subCategories
});
const actionCreators = {
    getSubCategories       : getSubCategories,
    updateSubCategory      : updateSubCategory,
};

export default connect(mapStateToProps, actionCreators )(EditSubCategory);
