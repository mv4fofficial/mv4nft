import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { ToastContainer } from 'react-toastify';
import Header from '../CommonTemplates/Header.js';
import Sidebar from '../CommonTemplates/SideBar.js';
import 'react-toastify/dist/ReactToastify.css';
import { Select, MenuItem } from '@material-ui/core';
import { getCategories, getSubCategories, addSubCategory } from '../../_actions/catActions.js';

class AddSubCategory extends Component {
    state = {
        readOnly: true,
        fields: {},
        errors: {},
        catImage: '',
        Categories: [],
    }

    async componentDidMount() {
        await this.props.getSubCategories();
        await this.props.getCategories();
        this.setState({ Categories: this.props.categories });
    }

    // Input handle  
    handleChange = (e) => {
        let fields = this.state.fields;
        fields[e.target.name] = e.target.value;
        this.setState({ errors: '' });
        this.setState(fields);
    }

    // Validation 
    handleValidation() {
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;
        if (!fields['subCategoryName']) {
            formIsValid = false;
            errors['subCategoryName'] = 'Please enter name.';
        } else if (!fields['subCategoryName'].match(/^[a-zA-Z ]+$/)) {
            formIsValid = false;
            errors['subCategoryName'] = 'Please enter valid name.';
        }
        // if(!fields[ 'description' ]) {
        //     formIsValid = false;
        //     errors[ 'description' ] = 'Please enter category description.';
        // }
        // if(!fields[ 'parent_id' ]) {
        //     formIsValid = false;
        //     errors[ 'parent' ] = 'Please select perent category.';
        // }
        // if(!this.state.catImage) {
        //     formIsValid = false;
        //     errors[ 'cat_image' ] = 'Please select category image.';
        // }
        this.setState({ errors: errors });
        return formIsValid;
    }



    // Handle Submit 
    handleSubmit = async (e) => {
        e.preventDefault();
        if (this.handleValidation()) {
            const catData = {
                categoryId: this.state.fields['categoryId'],
                subCategoryName: this.state.fields['subCategoryName'],
            };
            await this.props.addSubCategory(catData, this.props.history);
        }
    }
    render() {
        return (
            <React.Fragment>
                <div className="container-fluid">
                    <div className="row">
                        <Sidebar />
                        <main className="main-content col-lg-10 col-md-9 col-sm-12 p-0 offset-lg-2 offset-md-3">
                            <Header />
                            <div className="main-content-container container-fluid px-4">
                                <div className="page-header row no-gutters py-4">
                                    <div className="col-12 col-sm-4 text-center text-sm-left mb-0">
                                        <span className="text-uppercase page-subtitle">Overview</span>
                                        <h3 className="page-title">Add Sub Category</h3>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-lg-12">
                                        <div className="card card-small mb-4">
                                            <div className="card-header border-bottom">
                                                <h6 className="m-0">Sub Category Details</h6>
                                            </div>
                                            <ul className="list-group list-group-flush">
                                                <li className="list-group-item p-3">
                                                    <div className="row">
                                                        <div className="col">
                                                            <form onSubmit={this.handleSubmit} enctype='multipart/form-data'>
                                                                <div className="form-row">
                                                                    <div className="form-group col-md-6">
                                                                        <label for="feCategoryName">Choose Category</label>
                                                                        <Select
                                                                            labelId="demo-simple-select-label"
                                                                            id="categoryId"
                                                                            name="categoryId"
                                                                            onChange={this.handleChange}
                                                                        >
                                                                            {
                                                                                this.props.categories.categories.map((e) => {
                                                                                    return (
                                                                                        <MenuItem value={e.categoryId}>{e.categoryName}</MenuItem>
                                                                                    )
                                                                                })
                                                                            }

                                                                        </Select>
                                                                    </div>
                                                                </div>
                                                                <div className="form-row">
                                                                    <div className="form-group col-md-6">
                                                                        <label for="feFirstName">Sub Category Name</label>
                                                                        <input type="text" className="form-control" id="subCategoryName" name="subCategoryName" maxLength='15' placeholder="Sub Category Name" value={this.state.fields['subCategoryName']} onChange={this.handleChange} />
                                                                        <span style={{ color: 'red' }}>{this.state.errors['subCategoryName']}</span>
                                                                    </div>
                                                                </div>

                                                                <button type="submit" className="btn btn-success">Submit</button>&nbsp;
                                                                <Link to="/sub-categories-list" className="btn btn-danger">Cancel</Link>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <ToastContainer />
                            {/* <Footer/> */}
                        </main>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    categories: state.categories,

});
const actionCreators = {
    getCategories: getCategories,
    getSubCategories: getSubCategories,
    addSubCategory: addSubCategory

};

export default connect(mapStateToProps, actionCreators)(AddSubCategory);