import React,{Component} from 'react';
import { Link } from 'react-router-dom';
import ReactModal from 'react-modal';
import { connect } from 'react-redux';
import { API_URL } from '../../utils/admin-api-url';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { MDBDataTable } from 'mdbreact';
import DeleteRoundedIcon from '@material-ui/icons/DeleteRounded';
import EditIcon from '@material-ui/icons/Edit';
import SideBar from '../CommonTemplates/SideBar.js';
import Header from '../CommonTemplates/Header.js';
import { Fab } from 'react-tiny-fab';
import 'react-tiny-fab/dist/styles.css';
import { getCategories, deleteCategory } from '../../_actions/catActions.js';

export class CategoriesList extends Component { 
    state = {
        readOnly    : true,
        showModal   : false,
        id          : '',
        categories  : [],
    }
    async componentDidMount() {
        await this.props.getCategories();
        this.setState({categories:this.props.categories.categories});
    }

    handleOpenModal = id => {
        this.setState({ showModal: true, id });
    };

    handleCloseModal = () => {
        this.setState({ showModal: false });
    };

    handleConfirmDelete = async () => {
        this.setState({ showModal: false });
        await this.props.deleteCategory(this.state.id);
        await this.props.getCategories();
        this.setState({categories:this.props.categories.categories});
    };

    addNew = () => {
        this.props.history.push('add-category');
    }

    render() {
        const categories = this.state.categories;
        let displayContent = '';
        let i = 1;
        displayContent = categories.map(rowInfo => ( { 
            no: i++,
            categoryId: rowInfo.categoryId,
            categoryName: rowInfo.categoryName,
            image: 
                <div>
                    { rowInfo.image == null
                        ?
                        ''
                        :
                        <img src={API_URL+rowInfo.image} height='50' width='50' alt='' />
                    }
                </div>,
            action:
                <div>
                    <button className="btn btn-danger" onClick={() => this.handleOpenModal(rowInfo.categoryId)}><DeleteRoundedIcon /></button>&nbsp;
                    <Link to={`/edit-category/${(rowInfo.categoryId)}`} className="btn btn-warning"><EditIcon /></Link>
                </div>  
        } ));

        const data = {
            columns: [
                {
                    label: 'S.No',
                    field: 'no',
                    sort: 'asc',
                    width: 150
                },
                {
                    label: 'Category Id',
                    field: 'categoryId',
                    sort: 'asc',
                    width: 150
                },
                {
                    label: 'Category Name',
                    field: 'categoryName',
                    sort: 'asc',
                    width: 150
                },
                // {
                //     label: 'Image',
                //     field: 'image',
                //     sort: 'asc',
                //     width: 150
                // },
                {
                    label: 'Action',
                    field: 'action',
                    sort: 'asc',
                    width: 100
                }
            ],
            rows: displayContent
        };

        return (
            <React.Fragment>
                <div className="container-fluid">
                    <div className="row">
                        <SideBar/>
                        <main className="main-content col-lg-10 col-md-9 col-sm-12 p-0 offset-lg-2 offset-md-3">
                             
                                <Header/>
                                <div className="main-content-container container-fluid px-4">
                                    <Fab
                                        icon="Add new"
                                        alwaysShowTitle = {false}
                                        onClick={this.addNew}
                                        >
                                    </Fab>
                                    

                                    <div className="page-header row no-gutters py-4">
                                        <div className="col-12 col-sm-4 text-center text-sm-left mb-0">
                                            <span className="text-uppercase page-subtitle">Overview</span>
                                            <h3 className="page-title">Category List</h3>
                                        </div>
                                    </div>

                                    
                                    
                                    <div className="row">
                                        <div className="col">
                                            <div className="card card-small mb-4">
                                                <div className="card-header border-bottom">
                                                    <h6 className="m-0">Active Categories</h6>
                                                    {/* <Link to="/add-category" className="btn btn-info pull-right"><AddIcon /></Link> */}
                                                </div>
                                                <div className="card-body pb-3 text-center">
                                                    <MDBDataTable  striped bordered hover data={data}/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <ToastContainer />
                            {/* <Footer/> */}
                            <ReactModal isOpen={this.state.showModal} contentLabel="onRequestClose Example" onAfterOpen={this.afterOpenModal}
                                onRequestClose={this.handleCloseModal} shouldCloseOnOverlayClick={false} style={customStyles} >
                                <h3>Are you sure delete this category ?</h3>
                                <div className="btn-modal-container">
                                    <ul>
                                        <li>
                                            <button
                                                className="btn btn-sm btn-warning btn-modal-close"
                                                onClick={this.handleCloseModal}
                                            >
                                                Cancel
                                            </button>
                                        </li>
                                        <li>
                                            {' '}
                                            <button
                                                className="btn btn-sm btn-danger btn-modal-confirm"
                                                onClick={this.handleConfirmDelete}
                                            >
                                                Delete
                                            </button>
                                        </li>
                                    </ul>
                                </div>
                            </ReactModal>
                        </main>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

const customStyles = {
    content: {
        color: 'darkred',
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)'
    },
    overlay: {
        backgroundColor: '#444',
        opacity: '0.9'
    }
};

const mapStateToProps = state => ({
    categories  : state.categories
});
const actionCreators = {
    getCategories   : getCategories,
    deleteCategory  : deleteCategory,
};

export default connect(mapStateToProps, actionCreators )(CategoriesList);
