import React, {Component} from 'react';
import Header from '../CommonTemplates/Header.js';
import Dashboard from './Dashboard.js';
import SideBar from '../CommonTemplates/SideBar.js';
class Home extends Component {
    render(){
        return (
            <div className='container-fluid'>
                <div className="row">
                    <SideBar />
                    {/* <Header /> */}
                    <Dashboard />
                </div>
            </div>
        );
    }
}
export default Home;