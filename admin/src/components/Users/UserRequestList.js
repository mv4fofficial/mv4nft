import React,{Component} from 'react';
import { Link } from 'react-router-dom';
import ReactModal from 'react-modal';
import { connect } from 'react-redux';
import { API_URL } from '../../utils/admin-api-url';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { MDBDataTable } from 'mdbreact';
import DeleteRoundedIcon from '@material-ui/icons/DeleteRounded';
import EditIcon from '@material-ui/icons/Edit';
import CheckIcon from '@material-ui/icons/Check';
import ClearIcon from '@material-ui/icons/Clear';
import SideBar from '../CommonTemplates/SideBar.js';
import Header from '../CommonTemplates/Header.js';
import moment from 'moment';
import { Fab } from 'react-tiny-fab';
import 'react-tiny-fab/dist/styles.css';
import { getUserRequests, actionUserRequest } from '../../_actions/userActions';

export class UserRequestList extends Component { 
    state = {
        readOnly            : true,
        showModal           : false,
        id                  : '',
        userRequests        : [],
        actionUserRequest   : {},
        confirmAction       : false,
        actionRow           : {}
    }
    async componentDidMount() {
        await this.props.getUserRequests();
        this.setState({userRequests : this.props.userRequests || []});
        console.log({userRequests: this.state.userRequests});
    }

    handleOpenModal = id => {
        this.setState({ showModal: true, id });
    };

    handleCloseModal = () => {
        this.setState({ showModal: false, confirmAction: false });
    };
    
    actionUserRequest = (userId, requestType, status) => {
        this.setState({ showModal: true, actionRow: {userId, requestType, status} });
        // this.props.actionUserRequest({userId, requestType, status})
        // .then(res => {
        //     if (res.status === 200) {
        //         let userRequests = this.state.userRequests;

        //         userRequests.map(req => {
        //             if (userId == req.userId && requestType == req.requestType) {
        //                 req.status = status
        //             }
        //         })
        //         this.setState({userRequests : [...userRequests]});
        //     }
        // })
    }

    confirmAction = () => {
        let {userId, requestType, status} = this.state.actionRow;
        this.props.actionUserRequest({userId, requestType, status})
        .then(res => {
            if (res.status === 200) {
                let userRequests = this.state.userRequests;

                userRequests.map(req => {
                    if (userId == req.userId && requestType == req.requestType) {
                        req.status = status
                    }
                })
                this.setState({userRequests : [...userRequests]});
            }
        })
    }

    handleConfirm = async () => {
        this.setState({ showModal: false, confirmAction: true });
        this.confirmAction();
    };

    render() {
        const userRequests = this.state.userRequests;
        let displayContent = '';
        let i = 1;
        
        displayContent = userRequests.map(rowInfo => ( {
            no: i++,
            userName: rowInfo.userName,
            requestType: rowInfo.requestType,
            description: rowInfo.description,
            status: rowInfo.status,
            createdDate: moment(rowInfo.createdDate).format('MMM DD, YYYY hh:mm:ss'),
            // image: 
            //     <div>
            //         { rowInfo.image == null
            //             ?
            //             ''
            //             :
            //             <img src={API_URL+rowInfo.image} height='50' width='50' alt='' />
            //         }
            //     </div>,
            action:
            (rowInfo.status == "pending") ?
                <div>
                    {/* <button className="btn btn-danger" onClick={() => this.handleOpenModal(rowInfo.categoryId)}><DeleteRoundedIcon /></button>&nbsp; */}
                    <button title="Reject" className="btn btn-danger p-2" onClick={() => this.actionUserRequest(rowInfo.userId, rowInfo.requestType, 'reject')}>
                        <ClearIcon />
                    </button>&nbsp;
                    <button title="Approve" className="btn btn-success p-2" onClick={() => this.actionUserRequest(rowInfo.userId, rowInfo.requestType, 'approve')}><CheckIcon /></button>
                    {/* <Link to={`/edit-category/${(rowInfo.categoryId)}`} className="btn btn-warning"><EditIcon /></Link> */}
                </div>
                :
                <></>
        } ));

        const data = {
            columns: [
                {
                    label: 'S.No',
                    field: 'no',
                    sort: 'asc',
                    width: 150
                },
                {
                    label: 'User Name',
                    field: 'userName',
                    sort: 'asc',
                    width: 150
                },
                {
                    label: 'Request Type',
                    field: 'requestType',
                    sort: 'asc',
                    width: 150
                },
                {
                    label: 'Description',
                    field: 'description',
                    sort: 'asc',
                    width: 200
                },
                {
                    label: 'Status',
                    field: 'status',
                    sort: 'asc',
                    width: 150
                },
                {
                    label: 'Requested Date',
                    field: 'createdDate',
                    sort: 'asc',
                    width: 100
                },
                {
                    label: 'Action',
                    field: 'action',
                    sort: 'asc',
                    width: 100
                }

            ],
            rows: displayContent
        };

        return (
            <React.Fragment>
                <div className="container-fluid">
                    <div className="row">
                        <SideBar/>
                        <main className="main-content col-lg-10 col-md-9 col-sm-12 p-0 offset-lg-2 offset-md-3">
                            
                                <Header/>
                                <div className="main-content-container container-fluid px-4">

                                    <div className="page-header row no-gutters py-4">
                                        <div className="col-12 col-sm-4 text-center text-sm-left mb-0">
                                            <span className="text-uppercase page-subtitle">Overview</span>
                                            <h3 className="page-title">Request List</h3>
                                        </div>
                                    </div>

                                    
                                    
                                    <div className="row">
                                        <div className="col">
                                            <div className="card card-small mb-4">
                                                <div className="card-header border-bottom">
                                                    <h6 className="m-0">Active Requests</h6>
                                                    {/* <Link to="/add-category" className="btn btn-info pull-right"><AddIcon /></Link> */}
                                                </div>
                                                <div className="card-body pb-3 text-center">
                                                    {userRequests.length > 0 &&
                                                    <MDBDataTable  striped bordered hover data={data}/>
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <ToastContainer />
                            {/* <Footer/> */}
                            <ReactModal isOpen={this.state.showModal} contentLabel="onRequestClose Example" onAfterOpen={this.afterOpenModal}
                                onRequestClose={this.handleCloseModal} shouldCloseOnOverlayClick={false} style={customStyles} >
                                <h3>Are you sure!</h3>
                                <div className="btn-modal-container">
                                    <ul>
                                        <li>
                                            <button
                                                className="btn btn-sm btn-warning btn-modal-close"
                                                onClick={this.handleCloseModal}
                                            >
                                                Cancel
                                            </button>
                                        </li>
                                        <li>
                                            {' '}
                                            <button
                                                className="btn btn-sm btn-danger btn-modal-confirm"
                                                onClick={this.handleConfirm}
                                            >
                                                Continue
                                            </button>
                                        </li>
                                    </ul>
                                </div>
                            </ReactModal>
                        </main>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

const customStyles = {
    content: {
        color: 'darkred',
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)'
    },
    overlay: {
        backgroundColor: '#444',
        opacity: '0.9'
    }
};

const mapStateToProps = state => ({
    userRequests  : state.users.userRequests,
    actionUserRequest : state.users.actionUserRequest,
});
const actionCreators = {
    getUserRequests   : getUserRequests,
    actionUserRequest : actionUserRequest,
};

export default connect(mapStateToProps, actionCreators )(UserRequestList);
