import React, {Component} from 'react';
import { ToastContainer, toast } from 'react-toastify';
import { connect } from 'react-redux';
import 'react-toastify/dist/ReactToastify.css';
import { authActions } from '../../_actions/auth.js';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fields:[],
            errors:[],
        };
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange = e => {
        let fields = this.state.fields;
        fields[ e.target.name ] = e.target.value;
        this.setState();
    }

    login = e => {
        e.preventDefault();
        if(this.handleValidation()){
            const userData = {
                username: this.state.fields[ 'username' ],
                password: this.state.fields[ 'password' ],
            };
            this.props.login(userData);
        }
    }

    handleValidation () {
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;
        if(!fields[ 'username' ]){
            formIsValid = false;
            errors[ 'username' ] = 'Please enter username.';
        }
        if(!fields[ 'password' ]) {
            formIsValid = false;
            errors[ 'password' ] = 'Please enter password.';
        }
        this.setState({ errors: errors });
        return formIsValid;
    }

    render(){
        return (
            <div className='container-fluid admin-login-page'>
                <div className='row'>
                    <div className='col-md-4 col-md-offset-4'>
                        <div className='admin-login-form'>
                            <div className='center-block well'>
                                <h4 className='text-center'>Grocery App</h4>
                                <hr />
                                <form onSubmit={ this.login }>
                                    <div className='form-group'>
                                        <input type='text' className='form-control' name='username' onChange={this.handleChange} placeholder='Enter your username'  />
                                        <p style={ { color: 'red' } }>{this.state.errors[ 'username' ]}</p>
                                    </div>
                                    <div className='form-group'>
                                        <input type='text' className='form-control' name='password' onChange={this.handleChange} placeholder='Enter your password' />
                                        <p style={ { color: 'red' } }>{this.state.errors[ 'password' ]}</p>
                                        <a href='#' >Forgot Password</a>
                                    </div>
                                    <div className='form-group'>
                                        <input type='submit' value='Login' className='btn btn-primary form-control' />
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <ToastContainer />
            </div>
        );
    }
}
function mapState(state) {
    
}
const actionCreators = {
    login: authActions.login
};
export default connect(mapState, actionCreators)(Login);