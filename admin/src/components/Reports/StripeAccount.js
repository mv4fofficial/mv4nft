import React,{Component} from 'react';
import { connect } from 'react-redux';
import 'react-toastify/dist/ReactToastify.css';
import { MDBDataTable } from 'mdbreact';
import SideBar from '../CommonTemplates/SideBar.js';
import Header from '../CommonTemplates/Header.js';
import 'react-confirm-alert/src/react-confirm-alert.css';
import { getStripeAccountList } from '../../_actions/dashboardActions.js';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import moment from 'moment';


export class StripeAccount extends Component { 
    state = {
        loading : true
    }
    async componentDidMount() {
        console.log("PSOS",this.props);
        await this.props.getStripeAccountList('data');
        this.setState({loading: false});
        console.log("aaaa",this.props.transactions);
    }

    render() {
        let displayContent = '';
        let i = 1;
        displayContent = this.props.transactions.map(rowInfo => ( { 
            no: (<span className={(rowInfo.payouts_enabled && rowInfo.charges_enabled) ? "" : "text-danger"}>{i++}</span>),
            id: (<span className={(rowInfo.payouts_enabled && rowInfo.charges_enabled) ? "" : "text-danger"}>{rowInfo.id}</span>),
            date: (<span className={(rowInfo.payouts_enabled && rowInfo.charges_enabled) ? "" : "text-danger"}>{moment.unix(new Date(rowInfo.created)).format('MMM DD, YYYY')}</span>),
            email: (<span className={(rowInfo.payouts_enabled && rowInfo.charges_enabled) ? "" : "text-danger"}>{rowInfo.email}</span>),
            country: (<span className={(rowInfo.payouts_enabled && rowInfo.charges_enabled) ? "" : "text-danger"}>{rowInfo.country}</span>),
            defaultCurrency: (<span className={(rowInfo.payouts_enabled && rowInfo.charges_enabled) ? "text-uppercase" : "text-danger text-uppercase"}>{rowInfo.default_currency}</span>),
            type: (<span className={(rowInfo.payouts_enabled && rowInfo.charges_enabled) ? "" : "text-danger"}>{rowInfo.type}</span>),
            payout: (<span className={(rowInfo.payouts_enabled && rowInfo.charges_enabled) ? "" : "text-danger"}>{(rowInfo.payouts_enabled) ? "Enable" : "Disable"}</span>),
            charge: (<span className={(rowInfo.payouts_enabled && rowInfo.charges_enabled) ? "" : "text-danger"}>{(rowInfo.charges_enabled) ? "Enable" : "Disable"}</span>),
            view: (<a href={process.env.REACT_APP_STRIPE_URL+'connect/accounts'+rowInfo.id} target="_blank">View</a>)
            // name: rowInfo.userName,
            // order_id: '#'+rowInfo.orderId,
            // order_date: rowInfo.createdDate,
            // total: rowInfo.amount,
            // transactionStatus: rowInfo.transactionStatus
        } ));
        
        const data = {
            columns: [
                {
                    label: 'S.No',
                    field: 'no',
                    sort: 'asc'
                },
                {
                    label: 'Account ID',
                    field: 'id',
                },
                {
                    label: 'Email',
                    field: 'email',
                    width: 150
                },
                {
                    label: 'Payout',
                    field: 'payout',
                },
                {
                    label: 'Charge',
                    field: 'charge'
                },
                {
                    label: 'Country',
                    field: 'country',
                },
                {
                    label: 'Default Currency',
                    field: 'defaultCurrency',
                },
                {
                    label: 'Account Type',
                    field: 'type',
                    width: 250
                },
                {
                    label: 'Created At',
                    field: 'date',
                    width: 150
                },
                {
                    label: 'View',
                    field: 'view',
                }
            ],
            rows: displayContent
        };

        return (
            <React.Fragment>
                <div className="container-fluid">
                    <div className="row">
                        <SideBar/>
                        <main className="main-content col-lg-10 col-md-9 col-sm-12 p-0 offset-lg-2 offset-md-3">
                            <Header/>
                            <div className="main-content-container container-fluid px-4">
                                <div className="page-header row no-gutters py-4 justify-content-between">
                                    <div className="col-12 col-sm-4 text-center text-sm-left mb-0">
                                        <span className="text-uppercase page-subtitle">Overview</span>
                                        <h3 className="page-title">Stripe Connect Accounts List</h3>
                                    </div>
                                    <ReactHTMLTableToExcel  
                                        className="btn btn-info"  
                                        table="earnings"  
                                        filename="ReportExcel"  
                                        sheet="Sheet"
                                        buttonText="Export excel" />
                                </div>
                                <div className="row">
                                    <div className="col">
                                        <div className="card card-small mb-4">
                                            <div className="card-header border-bottom">
                                                <h6 className="m-0">Stripe Connect Accounts List</h6>
                                                
                                            </div>
                                            <div className="card-body pb-3 text-center">
                                                {this.state.loading ? <p>Please wait a moment</p> : <MDBDataTable  striped bordered hover data={data} id='earnings' />}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </main>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    transactions: state.dashboards.transactions
});
const actionCreators = { getStripeAccountList: getStripeAccountList };

export default connect(mapStateToProps, actionCreators )(StripeAccount);
