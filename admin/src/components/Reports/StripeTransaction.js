import React,{Component} from 'react';
import { connect } from 'react-redux';
import 'react-toastify/dist/ReactToastify.css';
import { MDBDataTable } from 'mdbreact';
import SideBar from '../CommonTemplates/SideBar.js';
import Header from '../CommonTemplates/Header.js';
import 'react-confirm-alert/src/react-confirm-alert.css';
import { getStripeTransactions } from '../../_actions/dashboardActions.js';
import { getBalance } from '../../_actions/dashboardActions.js';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import moment from 'moment';
import CallMadeIcon from '@material-ui/icons/CallMade';
import CallReceivedIcon from '@material-ui/icons/CallReceived';


export class StripeTransactions extends Component { 
    state = {
        loading : true
    }

    async componentDidMount() {
        console.log("PSOS",this.props);
        await this.props.getStripeTransactions('data');
        await this.props.getBalance({
            email: "",
            accountId: "",
            returnUrl: ""
        });
        this.setState({loading: false});
        console.log("balance",this.props.balance);
    }

    render() {
        let displayContent = '',
            balanceContent = [{
                available: "",
                connectReserved: "",
                pending: ""
            }];
        let i = 1;
        displayContent = this.props.transactions.map(rowInfo => ( { 
            no: (<>{i++}<span width="20">{(parseFloat(rowInfo.net) < 0) ? <CallMadeIcon className='table-svg'/> : <CallReceivedIcon className='table-svg'/>}</span></>),
            id: rowInfo.id,
            netAmount: (<span className={(parseFloat(rowInfo.net) > 0) ? "text-success text-uppercase" : "text-danger text-uppercase"}>{(rowInfo.currency)+" "+(parseFloat(rowInfo.net / 100).toFixed(2))}</span>),
            amount: (<span className={(parseFloat(rowInfo.amount) > 0) ? "text-success text-uppercase" : "text-danger text-uppercase"}>{(rowInfo.currency)+" "+(parseFloat(rowInfo.amount/100).toFixed(2))}</span>),
            date: moment.unix(new Date(rowInfo.available_on)).format('MMM DD, YYYY'),
            type: rowInfo.type,
            fee: (<span className="text-uppercase">{(rowInfo.currency)+" "+parseFloat(rowInfo.fee / 100).toFixed(2)}</span>),
            feeDetails: (rowInfo.fee > 0) ? rowInfo.fee_details[0].description : "",
            status: rowInfo.status, // Collected Fee
            source: (<a href={process.env.REACT_APP_STRIPE_URL+((rowInfo.type == "payout") ? 'payouts/' : ((rowInfo.type == "transfer") ? 'connect/transfers/' : (rowInfo.type == "charge") ? 'payments/' : 'connect/application_fees' ))+rowInfo.source} target="_blank">View</a>),
            // name: rowInfo.userName,
            // order_id: '#'+rowInfo.orderId,
            // order_date: rowInfo.createdDate,
            // total: rowInfo.amount,
            // transactionStatus: rowInfo.transactionStatus
        } ));
        
        if (typeof this.props.balance != "undefined") {
            balanceContent = [
                { 
                    available: (<span className="text-uppercase">{this.props.balance.available[0].currency + " - " + this.props.balance.available[0].amount}</span>),
                    connectReserved: (<span className="text-uppercase">{this.props.balance.connect_reserved[0].currency + " - " + this.props.balance.connect_reserved[0].amount}</span>),
                    pending:(<span className="text-uppercase">{this.props.balance.pending[0].currency + " - " + this.props.balance.pending[0].amount}</span>)
                }
            ];
        }
        
        const data = {
            columns: [
                {
                    label: 'S.No',
                    field: 'no',
                    sort: 'asc'
                },
                {
                    label: 'Transaction ID',
                    field: 'id',
                },
                {
                    label: 'Net Amount',
                    field: 'netAmount',
                    width: 150
                },
                {
                    label: 'Amount',
                    field: 'amount',
                    width: 150
                },
                {
                    label: 'Fee',
                    field: 'fee',
                },
                {
                    label: 'Fee Details',
                    field: 'feeDetails',
                },
                {
                    label: 'Effect Date',
                    field: 'date',
                    width: 250
                },
                {
                    label: 'Transaction Type',
                    field: 'type',
                },
                {
                    label: 'Status',
                    field: 'status'
                },
                {
                    label: 'See Detail',
                    field: 'source'
                }
            ],
            rows: displayContent
        };

        const balanceData = {
            columns: [
                {
                    label: 'Available Amount',
                    field: 'available',
                },
                {
                    label: 'Connect Reserved Amount',
                    field: 'connectReserved',
                },
                {
                    label: 'Pending Amount',
                    field: 'pending',
                }
            ],
            rows: balanceContent
        };

        return (
            <React.Fragment>
                <div className="container-fluid">
                    <div className="row">
                        <SideBar/>
                        <main className="main-content col-lg-10 col-md-9 col-sm-12 p-0 offset-lg-2 offset-md-3">
                            <Header/>
                            <div className="main-content-container container-fluid px-4">
                                <div className="page-header row no-gutters py-4 justify-content-between">
                                    <div className="col-12 col-sm-4 text-center text-sm-left mb-0">
                                        <span className="text-uppercase page-subtitle">Overview</span>
                                        <h3 className="page-title">Stripe Transactions List</h3>
                                    </div>
                                    <ReactHTMLTableToExcel  
                                        className="btn btn-info"  
                                        table="earnings"  
                                        filename="ReportExcel"  
                                        sheet="Sheet"
                                        buttonText="Export excel" />
                                </div>
                                <div className="row">
                                    <div className="col">
                                        <div className="card card-small mb-4">
                                            <div className="card-header border-bottom">
                                                <h6 className="m-0">Stripe Balance</h6>
                                            </div>
                                            <div className="card-body pb-3 text-center">
                                                {this.state.loading ? <p>Please wait a moment</p> : <MDBDataTable  striped bordered hover data={balanceData} paging={false} searching={false} sortable={false} id='earnings' />}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col">
                                        <div className="card card-small mb-4">
                                            <div className="card-header border-bottom">
                                                <h6 className="m-0">Stripe Transactions List</h6>
                                            </div>
                                            <div className="card-body pb-3 text-center">
                                                {this.state.loading ? <p>Please wait a moment</p> : <MDBDataTable  striped bordered hover data={data} id='earnings' />}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </main>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    transactions: state.dashboards.transactions,
    balance: state.dashboards.balance
});
const actionCreators = { getStripeTransactions: getStripeTransactions, getBalance: getBalance };

export default connect(mapStateToProps, actionCreators )(StripeTransactions);
