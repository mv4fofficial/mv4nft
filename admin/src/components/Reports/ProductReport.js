import React,{Component} from 'react';
import { connect } from 'react-redux';
import 'react-toastify/dist/ReactToastify.css';
import { MDBDataTable } from 'mdbreact';
import SideBar from '../CommonTemplates/SideBar.js';
import Header from '../CommonTemplates/Header.js';
import 'react-confirm-alert/src/react-confirm-alert.css';
import { getProducts } from '../../_actions/dashboardActions.js';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';  


export class ProductReport extends Component { 
    // state = {
    //     readOnly    : true,
    //     showModal   : false,
    //     id          : ''
    // }
    async componentDidMount() {
        console.log("PSOS",this.props);
        let data = {
            "categories": [
              0
            ],
            "subCategories": [
              0
            ],
            "tags": [
              "string"
            ],
            "priceFrom": 0,
            "priceTo": 0,
            "nftAvailability": true,
            "filter": "string",
            "pageIndex": 0,
            "pageSize": 10
          }
        
        await this.props.getProducts(data);
       
    }

    render() {
        let displayContent = '';
        let i = 1;
        displayContent = this.props.products.map(rowInfo => ( { 
            no: i++,
            name: rowInfo.productName,
            productId: rowInfo.productId,
            seller:rowInfo.seller,
            sellerEmail:rowInfo.sellerEmail,
            createdDate: rowInfo.createdDate,
            price: rowInfo.price,
            image:
            <div>
                { rowInfo.productPicture == null
                    ?
                    ''
                    :
                    <img src={rowInfo.productPicture} height='50' width='50' alt='' />
                }
            </div>
        } ));
        
        const data = {
            columns: [
                {
                    label: 'S.No',
                    field: 'no',
                    sort: 'asc',
                    width: 150
                },
                {
                    label: 'Product name',
                    field: 'name',
                },
                {
                    label: 'Picture',
                    field: 'image',
                },
                {
                    label: 'Product Id',
                    field: 'productId',
                    width: 150
                },
                {
                    label: 'Price',
                    field: 'price',
                    sort: 'asc',
                    width: 150
                },
                {
                    label: 'Seller name',
                    field: 'seller',
                    sort: 'asc',
                    width: 150
                },
                {
                    label: 'Seller Email',
                    field: 'sellerEmail',
                    sort: 'asc',
                    width: 150
                },
                {
                    label: 'Created Date',
                    field: 'createdDate',
                }
                
            ],
            rows: displayContent
        };

        return (
            <React.Fragment>
                <div className="container-fluid">
                    <div className="row">
                        <SideBar/>
                        <main className="main-content col-lg-10 col-md-9 col-sm-12 p-0 offset-lg-2 offset-md-3">
                            <Header/>
                            <div className="main-content-container container-fluid px-4">
                                <div className="page-header row no-gutters py-4 justify-content-between">
                                    <div className="col-12 col-sm-4 text-center text-sm-left mb-0">
                                        <span className="text-uppercase page-subtitle">Overview</span>
                                        <h3 className="page-title">Products List</h3>
                                    </div>
                                    <ReactHTMLTableToExcel  
                                        className="btn btn-info"  
                                        table="earnings"  
                                        filename="ReportExcel"  
                                        sheet="Sheet"
                                        buttonText="Export excel" />
                                </div>
                                <div className="row">
                                    <div className="col">
                                        <div className="card card-small mb-4">
                                            <div className="card-header border-bottom">
                                                <h6 className="m-0">Products List</h6>
                                                
                                            </div>
                                            <div className="card-body pb-3 text-center">
                                                <MDBDataTable  striped bordered hover data={data} id='earnings' />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </main>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    products: state.dashboards.products
});
const actionCreators = { getProducts: getProducts };

export default connect(mapStateToProps, actionCreators )(ProductReport);
