import React,{Component} from 'react';
import { connect } from 'react-redux';
import 'react-toastify/dist/ReactToastify.css';
import { MDBDataTable } from 'mdbreact';
import SideBar from '../CommonTemplates/SideBar.js';
import Header from '../CommonTemplates/Header.js';
import 'react-confirm-alert/src/react-confirm-alert.css';
import { getTransactions } from '../../_actions/dashboardActions.js';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';  


export class Transactions extends Component { 
    // state = {
    //     readOnly    : true,
    //     showModal   : false,
    //     id          : ''
    // }
    async componentDidMount() {
        console.log("PSOS",this.props);
        await this.props.getTransactions('data');
        console.log("aaaa",this.props.transactions);
    }

    render() {
        let displayContent = '';
        let i = 1;
        displayContent = this.props.transactions.map(rowInfo => ( { 
            no: i++,
            name: rowInfo.userName,
            order_id: '#'+rowInfo.orderId,
            order_date: rowInfo.createdDate,
            total: rowInfo.amount,
            transactionStatus: rowInfo.transactionStatus
        } ));
        
        const data = {
            columns: [
                {
                    label: 'S.No',
                    field: 'no',
                    sort: 'asc',
                    width: 150
                },
                {
                    label: 'OrderID',
                    field: 'order_id',
                },
                {
                    label: 'Name',
                    field: 'name',
                    width: 150
                },
                {
                    label: 'Amount',
                    field: 'total',
                    sort: 'asc',
                    width: 150
                },
                {
                    label: 'Order Date',
                    field: 'order_date',
                },
                {
                    label: 'Transaction Status',
                    field: 'transactionStatus',
                }
            ],
            rows: displayContent
        };

        return (
            <React.Fragment>
                <div className="container-fluid">
                    <div className="row">
                        <SideBar/>
                        <main className="main-content col-lg-10 col-md-9 col-sm-12 p-0 offset-lg-2 offset-md-3">
                            <Header/>
                            <div className="main-content-container container-fluid px-4">
                                <div className="page-header row no-gutters py-4 justify-content-between">
                                    <div className="col-12 col-sm-4 text-center text-sm-left mb-0">
                                        <span className="text-uppercase page-subtitle">Overview</span>
                                        <h3 className="page-title">Transactions List</h3>
                                    </div>
                                    <ReactHTMLTableToExcel  
                                        className="btn btn-info"  
                                        table="earnings"  
                                        filename="ReportExcel"  
                                        sheet="Sheet"
                                        buttonText="Export excel" />
                                </div>
                                <div className="row">
                                    <div className="col">
                                        <div className="card card-small mb-4">
                                            <div className="card-header border-bottom">
                                                <h6 className="m-0">Transactions List</h6>
                                                
                                            </div>
                                            <div className="card-body pb-3 text-center">
                                                <MDBDataTable  striped bordered hover data={data} id='earnings' />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </main>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    transactions: state.dashboards.transactions
});
const actionCreators = { getTransactions: getTransactions };

export default connect(mapStateToProps, actionCreators )(Transactions);
