import React,{Component} from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { ToastContainer } from 'react-toastify';
import Header from '../CommonTemplates/Header.js';
import Sidebar from '../CommonTemplates/SideBar.js';
import 'react-toastify/dist/ReactToastify.css';
import { getOrder, updateOrder } from '../../_actions/orderActions.js';

class EditOrder extends Component {
    state = {
        readOnly : true,
        fields   : {},
        errors   : {},
    }

    async componentDidMount() {
        const id = this.props.match.params.id;
        await this.props.getOrder(id);
        var data = this.props.order;
        var fields = this.state.fields;
        Object.keys(data)
            .forEach(function eachKey(key) {               
                fields[key] = data[key];                   
            });
        this.setState(fields);
    }
        
    // Input handle  
    handleChange = (e) => {
        let fields = this.state.fields;
        fields[e.target.name] = e.target.value;
        this.setState({errors:''}); 
        this.setState(fields);
    }

    // Validation 
    handleValidation(){
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;
        if(!fields[ 'status' ]) {
            formIsValid = false;
            errors[ 'status' ] = 'Please select status.';
        } else if (fields[ 'status' ] === ''){
            formIsValid = false;
            errors[ 'status' ] = 'Please select status.';
        }
        this.setState({errors: errors});
        return formIsValid;
    }

    //Handle Submit 
    handleSubmit = async (e) => {
        e.preventDefault();
        if(this.handleValidation()){
            const data = {
                order_id: this.state.fields[ 'id' ],
                device_id: this.state.fields[ 'device_id' ],
                user_id: this.state.fields[ 'user_id' ],
                status: this.state.fields[ 'status' ],
            };
            await this.props.updateOrder(data, this.props.history);
        }              
    }
    render() {
        return (
            <React.Fragment>
                <div className="container-fluid">
                    <div className="row">
                        <Sidebar/>
                        <main className="main-content col-lg-10 col-md-9 col-sm-12 p-0 offset-lg-2 offset-md-3">
                                <Header/>
                                <div className="main-content-container container-fluid px-4">
                                    <div className="page-header row no-gutters py-4">
                                        <div className="col-12 col-sm-4 text-center text-sm-left mb-0">
                                            <span className="text-uppercase page-subtitle">Overview</span>
                                            <h3 className="page-title">Edit Order</h3>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-lg-12">
                                            <div className="card card-small mb-4">
                                                <div className="card-header border-bottom">
                                                    <h6 className="m-0">Order Details</h6>
                                                </div>
                                                <ul className="list-group list-group-flush">
                                                    <li className="list-group-item p-3">
                                                        <div className="row">
                                                            <div className="col">
                                                                <form onSubmit={this.handleSubmit} enctype='multipart/form-data'>
                                                                    <div className="form-row">
                                                                        <div className="form-group col-md-6">
                                                                            <label for="feName">Total</label>
                                                                            <p>
                                                                                {this.state.fields['total']}
                                                                            </p>
                                                                        </div>
                                                                        <div className="form-group col-md-6">
                                                                            <label for="feName">Sub Total</label>
                                                                            <p>
                                                                                {this.state.fields['sub_total']}
                                                                            </p>
                                                                        </div>
                                                                        <div className="form-group col-md-6">
                                                                            <label for="feName">Discount</label>
                                                                            <p>
                                                                                {this.state.fields['discount']}
                                                                            </p>
                                                                        </div>
                                                                        <div className="form-group col-md-6">
                                                                            <label for="feName">Shipping Address</label>
                                                                            <p>
                                                                                {this.state.fields['shipping_address']}
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <div className="form-row">
                                                                        <div className="form-group col-md-6">
                                                                            <label for="feStatus">Status</label>
                                                                            <select id="status" name="status" className="form-control" value={this.state.fields[ 'status' ]} onChange={this.handleChange}>
                                                                                <option value=''>Select</option>
                                                                                <option value='pending'>Pending</option>
                                                                                <option value='paid'>Paid</option>
                                                                                <option value='packed'>Packed</option>
                                                                                <option value='dispatched'>Dispatched</option>
                                                                                <option value='delivered'>Delivered</option>
                                                                            </select>
                                                                            <span style={{color: 'red' }}>{this.state.errors[ 'status' ]}</span>
                                                                        </div>
                                                                    </div>
                                                                    <button type="submit" className="btn btn-success">Update</button>&nbsp;
                                                                    <Link to="/orders-list" className="btn btn-danger">Cancel</Link>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                            <ToastContainer />
                            {/* <Footer/> */}
                        </main>
                    </div>
                </div>
            </React.Fragment>   
        );
    }
}

const mapStateToProps = state => ({
    order: state.orders.order,
});
const actionCreators = {
    getOrder: getOrder,
    updateOrder: updateOrder,
};
export default connect(mapStateToProps, actionCreators)(EditOrder);