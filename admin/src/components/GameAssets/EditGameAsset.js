import React,{Component} from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { API_URL } from '../../utils/admin-api-url';
import 'react-toastify/dist/ReactToastify.css';
import Header from '../CommonTemplates/Header.js';
import Sidebar from '../CommonTemplates/SideBar.js';
import { getCategories, updateCategory } from '../../_actions/catActions.js';

class EditCategory extends Component { 
    state = {
        readOnly: true,
        fields: {},
        errors: {},
        form_data:{},
        catImage:'',
    }

    async componentDidMount() {
        const id = this.props.match.params.id;
        //await this.props.getParentCatList(id);
        await this.props.getCategories();
        var catdata =  this.findArrayElementByCat(this.props.categories.categories,id)
        var fields = this.state.fields;
        Object.keys(catdata)
            .forEach(function eachKey(key) {               
                fields[key] = catdata[key];                
            });
        this.setState(fields);
    }

     findArrayElementByCat = (array, id) =>  {
       return array.find((element) => {
         return element.categoryId == id;
        })
      }

 
    // Input handle  
    handleChange = (e) => { 
        let fields = this.state.fields;
        fields[ e.target.name ] = e.target.value; 
        this.setState(fields);
    }

    handleValidation(){
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;
        if(!fields[ 'categoryName' ]){
            formIsValid = false;
            errors[ 'categoryName' ] = 'Please enter category name.';
         }
        // else if(!fields[ 'name' ].match(/^[a-zA-Z ]+$/)){
        //     formIsValid = false;
        //     errors[ 'name' ] = 'Please enter valid category name.';
        // }
        // if(!fields[ 'description' ]){
        //     formIsValid = false;
        //     errors[ 'description' ] = 'Please enter description.';
        // }
        // if(fields[ 'parent_id' ] === ''){
        //     formIsValid = false;
        //     errors[ 'category' ] = 'Please select parent category.';
        // }
        // if(!this.state.catImage){
        //     formIsValid = false;
        //     errors[ 'image' ] = 'Please select image.';
        // } else if(this.state.catImage === 'null') {
        //     formIsValid = false;
        //     errors[ 'image' ] = 'Please select image.';
        // }
        this.setState({errors: errors});
        return formIsValid;
    }

    //Handle Fom Submit 
    handleSubmit = async (e) => {
        e.preventDefault();
        if(this.handleValidation()){
            
            const catData = {
                categoryId          : this.props.match.params.id,
                categoryName        : this.state.fields[ 'categoryName' ],
            };
            await this.props.updateCategory(catData, this.props.history);
        }           
    }

    render() {
        return (
            <React.Fragment>
                <div className="container-fluid">
                    <div className="row">
                        <Sidebar/>
                        <main className="main-content col-lg-10 col-md-9 col-sm-12 p-0 offset-lg-2 offset-md-3">
                                <Header/>
                                <div className="main-content-container container-fluid px-4">
                                    <div className="page-header row no-gutters py-4">
                                        <div className="col-12 col-sm-4 text-center text-sm-left mb-0">
                                            <span className="text-uppercase page-subtitle">Overview</span>
                                            <h3 className="page-title">Edit Category</h3>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-lg-12">
                                            <div className="card card-small mb-4">
                                                <div className="card-header border-bottom">
                                                    <h6 className="m-0">Category Details</h6>
                                                </div>
                                                <ul className="list-group list-group-flush">
                                                    <li className="list-group-item p-3">
                                                        <div className="row">
                                                            <div className="col">
                                                                <form onSubmit={this.handleSubmit}>
                                                                    <div className="form-row">
                                                                        <div className="form-group col-md-6">
                                                                            <label for="feFirstName">Category Name</label>
                                                                            <input type="text" className="form-control" id="categoryName" name="categoryName" placeholder="Full Name" value={this.state.fields[ 'categoryName' ]} onChange={this.handleChange}/> 
                                                                            <span style={ { color : 'red' } }>{this.state.errors[ 'categoryName' ]}</span>
                                                                        </div>
                                                                    </div>
                                                                  
                                                                    <button type="submit" className="btn btn-success">Update</button>&nbsp;
                                                                    <Link to="/categories-list" className="btn btn-danger">Cancel</Link>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                            {/* <Footer/> */}
                        </main>
                    </div>
                </div>
            </React.Fragment>   
        );
    }
}

const mapStateToProps = state => ({
    category    : state.categories.category,
    categories  : state.categories,
    parentCats  : state.categories.parentCats,
});
const actionCreators = {
    getCategories       : getCategories,
    updateCategory      : updateCategory,
    //uploadCategoryImage : uploadCategoryImage,
    //getParentCatList    : getParentCatList
};

export default connect(mapStateToProps, actionCreators )(EditCategory);
