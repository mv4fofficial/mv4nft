import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import { store } from './store';
import './App.css';
import Users from '../src/components/Users/UsersList.js';
import UserRequestList from '../src/components/Users/UserRequestList.js';
import UserProfile from '../src/components/Users/UserProfile.js';
import Login from '../src/components/Auth/Login.js';
import Dashboard from '../src/components/Templates/Dashboard.js';
import AddCategory from '../src/components/Categories/AddCategory.js';
import CategoriesList from '../src/components/Categories/CategoriesList.js';
import EditCategory from '../src/components/Categories/EditCategory.js';
import AddSubCategory from '../src/components/SubCategories/AddSubCategory.js';
import SubCategoriesList from '../src/components/SubCategories/SubCategoriesList.js';
import EditSubCategory from '../src/components/SubCategories/EditSubCategory.js';
import TotalEarnings from '../src/components/Reports/TotalEarnings.js';
import Transaction from '../src/components/Reports/Transactions';
import StripeTransaction from './components/Reports/StripeTransaction';
import StripeAccount from './components/Reports/StripeAccount';
import GameAssetsList from '../src/components/GameAssets/GameAssetsList.js';
import AddGameAsset from '../src/components/GameAssets/AddGameAsset.js';
import Products from '../src/components/Reports/ProductReport';

function App() {
    return (
        <Provider store={store}>
            <Router >
                <Switch>
                    <Route exact path ='/' component={Login} />
                    <Route path ='/user-profile' component={UserProfile} />
                    <Route path ='/dashboard' component={Dashboard} />
                    <Route path ='/users-list' component={Users} />
                    <Route path ='/user-request-list' component={UserRequestList} />
                    <Route path ='/add-category' component={AddCategory} />
                    <Route path ='/categories-list' component={CategoriesList} />
                    <Route path ='/edit-category/:id' component={EditCategory} />

                    <Route path ='/add-sub-category' component={AddSubCategory} />
                    <Route path ='/sub-categories-list' component={SubCategoriesList} />
                    <Route path ='/edit-sub-category/:id' component={EditSubCategory} />
                    <Route path ='/earning-reports' component={TotalEarnings} />
                    <Route path ='/transactions' component={Transaction} />
                    <Route path ='/stripe-transactions' component={StripeTransaction} />
                    <Route path ='/stripe-accounts' component={StripeAccount} />
                    <Route path ='/products' component={Products} />
                    <Route path ='/gameassets-list' component={GameAssetsList} />
                    <Route path ='/add-gameasset' component={AddGameAsset} />
                </Switch>
            </Router>
        </Provider>
    );
}

export default App;
