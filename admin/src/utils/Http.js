class Http {
    static async post(url, dataBody) {
        const response = await fetch(url, {
            method: 'POST',
            body: JSON.stringify(dataBody),
            headers: {
            'Content-type': 'application/json; charset=UTF-8'
            }
        });
  
        let data = await response;
        if (data.status === 400 || data.status === 404 || data.status === 403) {
            const error = await data.json();
            console.log(error);
            const subString = error.split('"')[1].split('"')[0];
            const errors = { error: { [subString]: error } };
            throw new Error(JSON.stringify(errors));
        }
        data = await response.json();
        return data;
    }
}  
export default Http;
  