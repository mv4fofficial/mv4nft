//****Base Url ****//

// export const API_URL = 'http://server.mv4f.io/';
export const API_URL = process.env.REACT_APP_API_URL;

// Auth API. 
export const URL_ADMIN_LOGIN = API_URL + 'api/Account/Login';
export const URL_ADMIN_LOGOUT = API_URL + 'api/admin/logout';

// Users API
export const URL_ADMIN_ADD_USER = API_URL + 'api/admin/add-user';
export const URL_ADMIN_GET_SINGLE_USER = API_URL + 'api/Account/ViewProfile';
export const URL_ADMIN_GET_USERS_LIST = API_URL + 'api/Admin/ViewAllUsersByRole';
export const URL_ADMIN_GET_USER_REQUEST_LIST = API_URL + 'api/Account/ViewUserRequests';
export const URL_ADMIN_ACTION_USER_REQUEST = API_URL + 'api/Admin/ActionUserRequest'

// Categories API
export const URL_ADMIN_ADD_CATEGORY = API_URL + 'api/Admin/AddCatagory';
export const URL_ADMIN_GET_CATEGORIES_LIST = API_URL + 'api/Admin/ViewAllCategories';
export const URL_ADMIN_DELETE_CATEGORY = API_URL + 'api/Admin/DeleteCatagory';
export const URL_ADMIN_GET_SINGLE_CATEGORY = API_URL + 'api/admin/get-category';
export const URL_ADMIN_UPDATE_CATEGORY = API_URL + 'api/Admin/UpdateCatagory';


// Sub Categories API
export const URL_ADMIN_ADD_SUBCATEGORY = API_URL + 'api/Admin/AddSubCatagory';
export const URL_ADMIN_GET_SUBCATEGORIES_LIST = API_URL + 'api/DropDown/GetSubCategories';
export const URL_ADMIN_DELETE_SUBCATEGORY = API_URL + 'api/Admin/DeleteSubCatagory';
export const URL_ADMIN_UPDATE_SUBCATEGORY = API_URL + 'api/Admin/UpdateSubCatagory';
//dashboard API.
export const URL_ADMIN_GET_DASHBOARD_DATA = API_URL + 'api/admin/get-dashboard-data';

// Reports API.
export const URL_ADMIN_EARNING_REPORT = API_URL + 'api/admin/get-earning-reports';
export const URL_ADMIN_TRANS_REPORT = API_URL + 'api/Admin/ViewAllPaymentDetails';
export const URL_ADMIN_PRODUCT_REPORT = API_URL + 'api/Home/AllProducts';
export const URL_ADMIN_STRIPE_TRANS_REPORT = API_URL + 'api/Payment/FetchStripeTransactions';
export const URL_ADMIN_STRIPE_ACCOUNTS_REPORT = API_URL + 'api/Payment/FetchStripeConnectAccountList';
export const URL_ADMIN_STRIPE_BALANCE_REPORT = API_URL + 'api/Payment/GetBalance';

// Game API. 
export const URL_GAME_VIEW_GAMEASSETS = API_URL + 'api/Game/ViewGameAssets';
export const URL_GAME_ADD_GAMEASSET = API_URL + 'api/Game/AddGameAsset';


