import {  
    POST_DASHBOARD_REQUEST, GET_DASHBOARD_SUCCESS, GET_DASHBOARD_FAILURE,
    POST_EARNING_REPORT_REQUEST, GET_EARNING_REPORT_SUCCESS, GET_EARNING_REPORT_FAILURE,
    POST_TRANS_REPORT_REQUEST,GET_TRANS_REPORT_SUCCESS,GET_TRANS_REPORT_FAILURE,
    POST_STRIPE_TRANS_REPORT_REQUEST,GET_STRIPE_TRANS_REPORT_SUCCESS,GET_STRIPE_TRANS_REPORT_FAILURE,
    POST_STRIPE_ACCOUNTS_REPORT_REQUEST,GET_STRIPE_ACCOUNTS_REPORT_SUCCESS,GET_STRIPE_ACCOUNTS_REPORT_FAILURE,
    POST_STRIPE_BALANCE_REPORT_REQUEST,GET_STRIPE_BALANCE_REPORT_SUCCESS,GET_STRIPE_BALANCE_REPORT_FAILURE,
    POST_PRODUCT_REPORT_REQUEST,GET_PRODUCT_REPORT_SUCCESS,GET_PRODUCT_REPORT_FAILURE
} from '../_actions/_type'; 

export const initialState = {
    dashboards: [],
    earnings: [],
    transactions: [],
    products:[],
    errors: {}
};
export default function dashboards (state = initialState, action) {
    switch (action.type) {
        case POST_DASHBOARD_REQUEST:
            return {
                ...state,
                loading: true
            };
        case GET_DASHBOARD_SUCCESS:
            return {
                ...state,
                dashboards: action.payload,
                loading: false
            };
        case GET_DASHBOARD_FAILURE:
            return {
                ...state,
                loading: false
            };
        case POST_EARNING_REPORT_REQUEST:
            return {
                ...state,
                loading: true
            };
        case GET_EARNING_REPORT_SUCCESS:
            return {
                ...state,
                earnings: action.payload,
                loading: false
            };
        case GET_EARNING_REPORT_FAILURE:
            return {
                ...state,
                loading: false
            };

        case POST_TRANS_REPORT_REQUEST:
            return {
                ...state,
                loading: true
            };
        case GET_TRANS_REPORT_SUCCESS:
            return {
                ...state,
                transactions: action.payload,
                loading: false
            };
        case GET_TRANS_REPORT_FAILURE:
            return {
                ...state,
                loading: false
            }; 
        case POST_STRIPE_TRANS_REPORT_REQUEST:
            return {
                ...state,
                loading: true
            };
        case GET_STRIPE_TRANS_REPORT_SUCCESS:
            return {
                ...state,
                transactions: action.payload,
                loading: false
            };
        case GET_STRIPE_TRANS_REPORT_FAILURE:
            return {
                ...state,
                loading: false
            };
        case POST_STRIPE_ACCOUNTS_REPORT_REQUEST:
            return {
                ...state,
                loading: true
            };
        case GET_STRIPE_ACCOUNTS_REPORT_SUCCESS:
            return {
                ...state,
                transactions: action.payload,
                loading: false
            };
        case GET_STRIPE_ACCOUNTS_REPORT_FAILURE:
            return {
                ...state,
                loading: false
            }; 
        case POST_STRIPE_BALANCE_REPORT_REQUEST:
            return {
                ...state,
                loading: true
            };
        case GET_STRIPE_BALANCE_REPORT_SUCCESS:
            return {
                ...state,
                balance: action.payload,
                loading: false
            };
        case GET_STRIPE_BALANCE_REPORT_FAILURE:
            return {
                ...state,
                loading: false
            }; 

        case POST_PRODUCT_REPORT_REQUEST:
        return {
            ...state,
            loading: true
        };
        case GET_PRODUCT_REPORT_SUCCESS:
            return {
                ...state,
                products: action.payload,
                loading: false
            };
        case GET_PRODUCT_REPORT_FAILURE:
            return {
                ...state,
                loading: false
        }; 
        default:
            return state;
    }
}
