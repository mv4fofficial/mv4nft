import {
  POST_ADD_GAMEASSET_REQUEST, GET_ADD_GAMEASSET_SUCCESS, GET_ADD_GAMEASSET_FAILURE,
  POST_GAMEASSETS_REQUEST, GET_GAMEASSETS_SUCCESS, GET_GAMEASSETS_FAILURE
} from '../_actions/_type';

export const initialState = {
  gameassets: [],
  gameassetCreated: false,
  loading: false,
  errors: {},
};
export default function categories(state = initialState, action) {
  console.log('initialState', initialState);
  switch (action.type) {
    case POST_GAMEASSETS_REQUEST:
      return {
        ...state,
        loading: true
      };
    case GET_GAMEASSETS_SUCCESS:
      return {
        ...state,
        gameassets: action.payload,
        loading: false
      };
    case GET_GAMEASSETS_FAILURE:
      return {
        ...state,
        loading: false,
        errors: action.payload
      };
    case POST_ADD_GAMEASSET_REQUEST:
      return {
        ...state,
        loading: true
      };
    case GET_ADD_GAMEASSET_SUCCESS:
      return {
        ...state,
        gameassetCreated: true,
        loading: false
      };
    case GET_ADD_GAMEASSET_FAILURE:
      return {
        ...state,
        loading: false
      };
    default:
      return state;
  }
}
