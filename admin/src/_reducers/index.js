import {combineReducers} from 'redux';
import auth from './auth.js';
import users from './users.js';
import categories from './categories.js';
import gameassets from './gameassets.js'
import dashboards from './dashboards.js';

export default combineReducers({
  categories,
  auth,
  users,
  dashboards,
  gameassets,
});
