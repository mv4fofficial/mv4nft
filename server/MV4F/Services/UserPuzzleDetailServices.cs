﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using MV4F.Entities;
using MV4F.IRepository;
using MV4F.IServices;
using MV4F.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MV4F.Services
{
    public class UserPuzzleDetailServices : IUserPuzzleDetailServices
    {
        private readonly IConfiguration _configuration;
        private readonly IEmailService _emailService;
        private readonly IStorage _storage;
        private readonly ICurrentUser _currentUser;
        private readonly MV4FContext _context;

        public UserPuzzleDetailServices(IConfiguration configuration,
                              IEmailService emailService,
                              IStorage storage,
                              ICurrentUser currentUser,
                              MV4FContext context)
        {
            _configuration = configuration;
            _emailService = emailService;
            _storage = storage;
            _currentUser = currentUser;
            _context = context;
        }

        public async Task<ServiceResponse<List<UserPuzzleDetailViewModel>>> ViewUserPuzzleDetails()
        {
            List<UserPuzzleDetailViewModel> puzzleDetailList;

            puzzleDetailList = await _context.UserPuzzleDetail
                .Where(x => x.UserId == _currentUser.UserID)
                .Include(x => x.PuzzleDetail)
                .Select(x => new UserPuzzleDetailViewModel
                {
                    Id = x.Id,
                    CreatedDate = x.CreatedDate,
                    UpdatedDate = x.UpdatedDate,
                    PuzzleDetailViewModel = new PuzzleDetailViewModel
                    {
                        Id = x.PuzzleDetail.Id,
                        RemainingHints = x.PuzzleDetail.RemainingHints,
                        Timer = x.PuzzleDetail.Timer,
                        ElapsedTime = x.PuzzleDetail.ElapsedTime
                    }
                }).ToListAsync();

            return new ServiceResponse<List<UserPuzzleDetailViewModel>>(200, "user's puzzle details", puzzleDetailList);
        }

        public async Task<ApiResponse> AddUserPuzzleDetail(AddUserPuzzleDetailModel model)
        {
            UserPuzzleDetail userPuzzle = new()
            {
                UserId = _currentUser.UserID,
                CreatedDate = DateTime.Now,
                UpdatedDate = DateTime.Now,
                PuzzleId = model.PuzzleId
            };

            await _context.AddAsync(userPuzzle);
            await _context.SaveChangesAsync();

            return new ApiResponse("added user puzzle details Successfully", 200);
        }
    }
}