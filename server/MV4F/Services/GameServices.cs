﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using MV4F.Entities;
using MV4F.IRepository;
using MV4F.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace MV4F.Repository
{
    public class GameServices : IGameServices
    {
        private readonly IConfiguration _configuration;
        private readonly IEmailService _emailService;
        private readonly IStorage _storage;
        private readonly ICurrentUser _currentUser;
        private readonly MV4FContext _context;

        public GameServices(IConfiguration configuration,
                                IEmailService emailService,
                                IStorage storage,
                                ICurrentUser currentUser,
                                MV4FContext context)
        {
            _configuration = configuration;
            _emailService = emailService;
            _storage = storage;
            _currentUser = currentUser;
            _context = context;
        }

        public async Task<ServiceResponse<List<GameAssetsViewModel>>> ViewGameAssets(string type)
        {
            List<GameAssetsViewModel> gameAssets;
            if (type == null)
            {
                gameAssets = await _context.GameAssets.Select(x => new GameAssetsViewModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    Type = x.Type,
                    Image = x.Image,
                    CoinPrice = x.CoinPrice,
                    DiamondPrice = x.DiamondPrice,
                    CreatedDate = x.CreatedDate,
                    UpdatedDate = x.UpdatedDate,
                }).ToListAsync();


            }
            else
            {
                gameAssets = await _context.GameAssets.Where(x => x.Type == type)
                                                            .Select(x => new GameAssetsViewModel
                                                            {
                                                                Id = x.Id,
                                                                Name = x.Name,
                                                                Type = x.Type,
                                                                Image = x.Image,
                                                                CoinPrice = x.CoinPrice,
                                                                DiamondPrice = x.DiamondPrice,
                                                                CreatedDate = x.CreatedDate,
                                                                UpdatedDate = x.UpdatedDate,
                                                            }).ToListAsync();




            }
            return new ServiceResponse<List<GameAssetsViewModel>>(200, "game assets", gameAssets);
        }

        public async Task<ServiceResponse<GameAssetsViewModel>> ViewGameAssetById(long id)
        {
            var gameAsset = await _context.GameAssets.Where(x => x.Id == id)
                                                        .Select(x => new GameAssetsViewModel
                                                        {
                                                            Id = x.Id,
                                                            Name = x.Name,
                                                            Type = x.Type,
                                                            Image = x.Image,
                                                            CoinPrice = x.CoinPrice,
                                                            DiamondPrice = x.DiamondPrice,
                                                            CreatedDate = x.CreatedDate,
                                                            UpdatedDate = x.UpdatedDate,
                                                        }).FirstOrDefaultAsync();
            return new ServiceResponse<GameAssetsViewModel>(200, "game asset by id", gameAsset);
        }

        public async Task<ApiResponse> AddGameAsset(AddGameAssetModel model)
        {
            GameAssets newGameAsset = new()
            {
                Name = model.Name,
                Type = model.Type,
                Image = model.Image,
                CoinPrice = model.CoinPrice,
                DiamondPrice = model.DiamondPrice,
                CreatedDate = DateTime.Now,
                UpdatedDate = DateTime.Now,
            };

            await _context.AddAsync(newGameAsset);
            await _context.SaveChangesAsync();

            return new ApiResponse("added Successfully", 200);

        }

    }
}
