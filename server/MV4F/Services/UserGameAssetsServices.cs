﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

using MV4F.Entities;
using MV4F.IRepository;
using MV4F.Models;

using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace MV4F.Repository
{
    public class UserGameAssetsServices : IUserGameAssetsServices
    {
        private readonly IConfiguration _configuration;
        private readonly IEmailService _emailService;
        private readonly IStorage _storage;
        private readonly ICurrentUser _currentUser;
        private readonly MV4FContext _context;

        public UserGameAssetsServices(IConfiguration configuration,
                                IEmailService emailService,
                                IStorage storage,
                                ICurrentUser currentUser,
                                MV4FContext context)
        {
            _configuration = configuration;
            _emailService = emailService;
            _storage = storage;
            _currentUser = currentUser;
            _context = context;
        }

        public async Task<ServiceResponse<List<UserGameAssetsViewModel>>> ViewUserGameAssets()
        {
            List<UserGameAssetsViewModel> gameAssets;
          
            gameAssets = await _context.UserGameAssets
                .Where(x => x.UserId == _currentUser.UserID)
                .Include(x => x.GameAssets)
                .Select(x => new UserGameAssetsViewModel
                {
                    Id = x.Id,
                    GameAssetId = x.GameAssetId,
                    CreatedDate = x.CreatedDate,
                    UpdatedDate = x.UpdatedDate,
                    GameAssets = new GameAssetsViewModel {
                      Id = x.GameAssets.Id,
                      Name = x.GameAssets.Name,
                      Image = x.GameAssets.Image,
                      CoinPrice = x.GameAssets.CoinPrice,
                      DiamondPrice = x.GameAssets.DiamondPrice,
                    },
                    Progress = x.Progress
                }).ToListAsync();

            return new ServiceResponse<List<UserGameAssetsViewModel>>(200, "user's game assets", gameAssets);
        }

        public async Task<ServiceResponse<List<UserGameAssetsViewModel>>> ViewInProgressPuzzles()
        {
            List<UserGameAssetsViewModel> gameAssets;
          
            gameAssets = await _context.UserGameAssets
                .Where(x => x.UserId == _currentUser.UserID && x.Progress < 100)
                .Include(x => x.GameAssets)
                .Select(x => new UserGameAssetsViewModel
                {
                    Id = x.Id,
                    GameAssetId = x.GameAssetId,
                    CreatedDate = x.CreatedDate,
                    UpdatedDate = x.UpdatedDate,
                    GameAssets = new GameAssetsViewModel {
                      Id = x.GameAssets.Id,
                      Name = x.GameAssets.Name,
                      Image = x.GameAssets.Image,
                      CoinPrice = x.GameAssets.CoinPrice,
                      DiamondPrice = x.GameAssets.DiamondPrice,
                    },
                    Progress = x.Progress
                }).ToListAsync();

            return new ServiceResponse<List<UserGameAssetsViewModel>>(200, "user's game assets", gameAssets);
        }


        public async Task<ApiResponse> AddUserGameAsset(AddUserGameAssetModel model)
        {
            UserGameAssets newUserGameAsset = new()
            {
                UserId = _currentUser.UserID,
                GameAssetId = model.GameAssetId,
                Progress = model.Progress,
                CreatedDate = DateTime.Now,
                UpdatedDate = DateTime.Now,
            };

            await _context.AddAsync(newUserGameAsset);
            await _context.SaveChangesAsync();

            return new ApiResponse("added game assets Successfully", 200);

        }

        public async Task<ApiResponse> UpdateProgress(ProgressUpdateModel model)
        {
            UserGameAssets userGameAsset = await _context.UserGameAssets
                .Where(x => x.UserId == _currentUser.UserID && x.GameAssets.Id == model.GameAssetId)
                .FirstOrDefaultAsync();

            if(userGameAsset == null) 
                return new ApiResponse("Resource Not Found", StatusCodes.Status404NotFound);

            userGameAsset.Progress = model.Progress;
            userGameAsset.UpdatedDate = DateTime.Now;

            await _context.SaveChangesAsync();

            return new ApiResponse("updated progress", 200);
        }
    }
}
