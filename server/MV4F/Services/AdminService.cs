﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using MV4F.Entities;
using MV4F.IRepository;
using MV4F.IServices;
using MV4F.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MV4F.Services
{
    public class AdminService : IAdminService
    {
        private readonly IConfiguration _configuration;
        private readonly MV4FContext _context;
        private readonly IStorage _storage;

        public AdminService(IConfiguration configuration,
                            MV4FContext context,
                            IStorage storage)
        {
            _configuration = configuration;
            _context = context;
            _storage = storage;
        }

        public async Task<ApiResponse> AddCatagory(AddCategoryModel model)
        {
            var checkCatagory = await _context.CategoryMaster.Where(x => x.Name == model.CategoryName).FirstOrDefaultAsync();
            if (checkCatagory != null)
                return new ApiResponse("Category Already exists", 400);

            CategoryMaster newCategory = new()
            {
                Name = model.CategoryName,
            };

            await _context.AddAsync(newCategory);
            await _context.SaveChangesAsync();

            return new ApiResponse("added Successfully", 200);
        }


        public async Task<ApiResponse> AddTags(AddTagModel model)
        {
            var checkCatagory = await _context.TagsMaster.Where(x => x.TagName.ToLower() == model.TagName.ToLower())
                                                         .FirstOrDefaultAsync();
            if (checkCatagory != null)
                return new ApiResponse("Category Already exists", 400);

            TagsMaster newCategory = new()
            {
                TagName = model.TagName,
            };

            await _context.AddAsync(newCategory);
            await _context.SaveChangesAsync();

            return new ApiResponse("added Successfully", 200);
        }

        public async Task<ApiResponse> UpdateCatagory(UpdateCategotyModel model)
        {

            var checkCatagory = await _context.CategoryMaster.Where(x => x.Id == model.CategoryId).FirstOrDefaultAsync();
            if (checkCatagory == null)
                return new ApiResponse("Category Doesnot exists", StatusCodes.Status404NotFound);

            checkCatagory.Name = model.CategoryName;

            _context.Update(checkCatagory);
            await _context.SaveChangesAsync();
            return new ApiResponse("updated successfully", 200);

        }

        public async Task<ApiResponse> UpdateTag(UpdateTagModel model)
        {
            var checkTag = await _context.TagsMaster.Where(x => x.Id == model.TagId).FirstOrDefaultAsync();
            if (checkTag == null)
                return new ApiResponse("Category Doesnot exists", StatusCodes.Status404NotFound);

            checkTag.TagName = model.TagName;

            _context.Update(checkTag);
            await _context.SaveChangesAsync();
            return new ApiResponse("updated successfully", 200);

        }

        public async Task<ApiResponse> ActionUserRequest(ActionRequestModel model)
        {

            var userRole = await _context.UserRole.Where(x => x.UserId == model.userId).FirstOrDefaultAsync();
            var userRequest = await _context.UserRequest.Where(x => x.UserId == model.userId && x.Status == "pending").FirstOrDefaultAsync();
            if (model.requestType == "seller_upgrade_request" && model.status == "approve") {
                userRole.RoleId = 2;
                _context.Update(userRequest);
                await _context.SaveChangesAsync();

            }

            userRequest.Status = model.status;

            _context.Update(userRequest);
            await _context.SaveChangesAsync();
            return new ApiResponse("updated successfully", 200);

        }


        public async Task<ServiceResponse<List<UpdateCategotyModel>>> ViewAllCategories()
        {
            var Categories = await _context.CategoryMaster.Where(x => x.IsDeleted != true).Select(x => new UpdateCategotyModel
            {
                CategoryId = x.Id,
                CategoryName = x.Name,
            }).ToListAsync();
            return new ServiceResponse<List<UpdateCategotyModel>>(StatusCodes.Status200OK, "List Of all categories", Categories);
        }


        public async Task<ApiResponse> DeleteCatagory(int Id)
        {
            var doc = await _context.CategoryMaster.Where(x => x.Id == Id).FirstOrDefaultAsync();
            if (doc == null)
                return new ApiResponse("Category not found", 400);

            doc.IsDeleted = true;
            _context.Update(doc);
            await _context.SaveChangesAsync();
            return new ApiResponse("deleted Successfully", 200);
        }

        public async Task<ApiResponse> DeleteTag(int Id)
        {
            var Tag = await _context.TagsMaster.Where(x => x.Id == Id).FirstOrDefaultAsync();
            if (Tag == null)
                return new ApiResponse("Category not found", 400);

            _context.Remove(Tag);
            await _context.SaveChangesAsync();
            return new ApiResponse("deleted Successfully", 200);
        }


        public async Task<ServiceResponse<List<RegisteredUserModel>>> ViewAllUsers(long roleId)
        {
            var CurrentUser = _configuration;
            List<RegisteredUserModel> allusers = new();
            if (roleId > 0 && roleId <= 3)
            {
                allusers = await _context.UserRole.Include(x => x.User)
                                                .ThenInclude(x => x.Country)
                                                .Include(x => x.Role)
                                                .Where(x => x.RoleId == roleId)
                                                .Select(x => new RegisteredUserModel
                                                {
                                                    UserName = x.User.FirstName + " " + x.User.LastName,
                                                    ProfilePicture = _storage.GetBase64(x.User.ProfilePicture).Base64,
                                                    Extention = _storage.GetBase64(x.User.ProfilePicture).Extention,
                                                    ContactNumber = x.User.ContactNumber,
                                                    EamilId = x.User.EmailId,
                                                    DateOfBirth = x.User.DateOfBirth,
                                                    Country = x.User.Country.Name,
                                                    RoleId = x.RoleId,
                                                    RoleName = x.Role.RoleName
                                                })
                                                .ToListAsync();
            }
            else if (roleId == 0)
            {
                allusers = await _context.UserRole.Include(x => x.User)
                                                .ThenInclude(x => x.Country)
                                                .Include(x => x.Role)
                                                .Where(x => x.RoleId != 1)
                                                .Select(x => new RegisteredUserModel
                                                {
                                                    UserName = x.User.FirstName + " " + x.User.LastName,
                                                    ProfilePicture = _storage.GetBase64(x.User.ProfilePicture).Base64,
                                                    Extention = _storage.GetBase64(x.User.ProfilePicture).Extention,
                                                    ContactNumber = x.User.ContactNumber,
                                                    EamilId = x.User.EmailId,
                                                    DateOfBirth = x.User.DateOfBirth,
                                                    Country = x.User.Country.Name,
                                                    RoleId = x.RoleId,
                                                    RoleName = x.Role.RoleName
                                                })
                                                .ToListAsync();
            }

            return new ServiceResponse<List<RegisteredUserModel>>(StatusCodes.Status200OK, "all users", allusers);
        }

        public async Task<ServiceResponse<List<PaymentLedgerResponse>>> ViewAllPaymentDetails()
        {
            var allPaymentDetails = await _context.PaymentLedger.Include(x => x.User).Select(x => new PaymentLedgerResponse
            {
                UserName = x.User.FirstName + " " + x.User.LastName,
                OrderId = x.OrderId,
                TransactionStatus = x.TransactionStatus,
                Amount = x.Amount,
                CreatedDate = x.CreatedDate
            }).ToListAsync();
            return new ServiceResponse<List<PaymentLedgerResponse>>(200, "all payment Details", allPaymentDetails);
        }


        public async Task<ApiResponse> AddSubCategory(AddSubCategoryModel model)
        {
            var checkSubCatagory = await _context.SubCategoryMasters.Where(x => x.SubCategoryName == model.SubCategoryName)
                                                                    .FirstOrDefaultAsync();
            if (checkSubCatagory != null)
                return new ApiResponse("Category Already exists", 400);

            SubCategoryMaster newSubCategory = new()
            {
                SubCategoryName = model.SubCategoryName,
                CategoryId = model.CategoryId,
            };

            await _context.AddAsync(newSubCategory);
            await _context.SaveChangesAsync();

            return new ApiResponse("added Successfully", 200);
        }

        public async Task<ApiResponse> UpdateSubCatagory(UpdateSubCategotyModel model)
        {
            var checkCatagory = await _context.SubCategoryMasters.Where(x => x.Id == model.SubCategoryId).FirstOrDefaultAsync();
            if (checkCatagory == null)
                return new ApiResponse("Category Doesnot exists", StatusCodes.Status404NotFound);

            checkCatagory.SubCategoryName = model.SubCategoryName;

            _context.Update(checkCatagory);
            await _context.SaveChangesAsync();
            return new ApiResponse("updated successfully", 200);

        }

        public async Task<ApiResponse> DeleteSubCatagory(int Id)
        {
            var doc = await _context.SubCategoryMasters.Where(x => x.Id == Id).FirstOrDefaultAsync();
            if (doc == null)
                return new ApiResponse("Category not found", 400);

            doc.IsDeleted = true;
            _context.Update(doc);
            await _context.SaveChangesAsync();
            return new ApiResponse("deleted Successfully", 200);
        }

        public async Task<ServiceResponse<ProductSummary>> ProductsSummary(ProductSummaryModel model)
        {
            var product = await _context.Product.Where(x => x.Id == model.ProductId)
                                                .Include(x => x.Seller)
                                                .Select(x => new ProductSummary
                                                {
                                                    ProductId = x.Id,
                                                    ProductTitle = x.ProductTitle,
                                                    ProductImage = x.ProductImage,
                                                    // ProductImage = _storage.GetBase64(x.ProductImage).Base64,
                                                    Extention = _storage.GetBase64(x.ProductImage).Extention,
                                                    Description = x.Description,
                                                    Price = x.Price,
                                                    NFTStatus = x.NFTStatus,
                                                    SellerId = x.SellerId,
                                                    SellerEamil = x.Seller.EmailId,
                                                    SellerName = x.Seller.FirstName + " " + x.Seller.LastName,
                                                })
                                                .FirstOrDefaultAsync();
            if (product == null)
                return new ServiceResponse<ProductSummary>(StatusCodes.Status404NotFound, "invalid Product ID", null);

            var buyers = await _context.UserProduct.Where(x => x.ProductId == model.ProductId)
                                                   .Include(x => x.User)
                                                   .Select(x => new BuyerDetails
                                                   {
                                                       BuyerId = x.UserId,
                                                       BuyerEmail = x.User.EmailId,
                                                       BuyerName = x.User.FirstName + " " + x.User.LastName
                                                   })
                                                   .ToListAsync();
            if (buyers != null)
            {
                product.Buyers = buyers;
            }

            return new ServiceResponse<ProductSummary>(StatusCodes.Status200OK, "Product Summary", product);
        }
    }
}


