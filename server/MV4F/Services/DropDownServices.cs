﻿using Microsoft.EntityFrameworkCore;
using MV4F.Entities;
using MV4F.IServices;
using MV4F.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MV4F.Services
{
    public class DropDownServices : IDropDownServices
    {
        private readonly MV4FContext _context;

        public DropDownServices(MV4FContext context)
        {
            _context = context;
        }

        public async Task<List<CategoriesDropDownModel>> GetCategories()
        {
            var Query = await _context.CategoryMaster.Where(x => x.IsDeleted == false)
                                                     .Select(x => new CategoriesDropDownModel
                                                     {
                                                         CategoryId = x.Id,
                                                         CategoryName = x.Name
                                                     })
                                                     .ToListAsync();
            return Query;
        }

        public async Task<List<CountryModel>> GetCountry()
        {
            var query = await _context.Country.Select(s => new CountryModel
            {
                Id = s.Id,
                Name = s.Name,
                CountryCode = s.CountryCode,
                PhoneCode = s.PhoneCode,
                IsRestricted = s.IsRestricted
            }).ToListAsync();
            return query;
        }

        public async Task<List<SubCategoriesDropDownModel>> GetSubCategories()
        {
            var Query = await _context.SubCategoryMasters.Where(x => x.IsDeleted == false)
                                         .Select(x => new SubCategoriesDropDownModel
                                         {
                                             SubCategoryId = x.Id,
                                             CategoryId = x.CategoryId,
                                             SubCategoryName = x.SubCategoryName
                                         })
                                         .ToListAsync();
            return Query;
        }

        public async Task<List<TagsDropDownModel>> GetTags()
        {
            var Query = await _context.TagsMaster.Select(x => new TagsDropDownModel
            {
                TagId = x.Id,
                TagName = x.TagName
            })
                                                                 .ToListAsync();
            return Query;
        }
    }
}
