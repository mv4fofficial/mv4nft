﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using MV4F.Entities;
using MV4F.IRepository;
using MV4F.IServices;
using MV4F.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MV4F.Services
{
    public class SellerServices : ISellerServices
    {
        private readonly ICurrentUser _currentUser;
        private readonly IStorage _storage;
        private readonly MV4FContext _context;

        public SellerServices(ICurrentUser currentUser,
                              IStorage storage,
                              MV4FContext context)
        {
            _currentUser = currentUser;
            _storage = storage;
            _context = context;
        }

        public async Task<ServiceResponse<long>> AddProduct(AddProductModel model)
        {
            var CheckProduct = await _context.Product.Where(x => x.ProductTitle == model.ProductName).FirstOrDefaultAsync();
            var Categories = await _context.CategoryMaster.ToListAsync();
            var SubCategories = await _context.SubCategoryMasters.ToListAsync();
            var Tags = await _context.ProductTags.Include(x => x.TagsMaster).ToListAsync();
            string category = "";
            string subcategory = "";

            if (CheckProduct != null)
                return new ServiceResponse<long>(400, "product already exists", 0);
            Product NewProduct = new()
            {
                ProductTitle = model.ProductName,
                Price = model.Price,
                GasFee = null,
                Link = model.Link,
                Quantity = model.Quantity,
                NFTStatus = model.NFTStatus,
                ForSale = model.ForSale,
                Description = model.Description,
                SellerId = _currentUser.UserID,
                ProductImage = model.ProductImage,
                Rarity = model.Rarity,
                IsDeleted = false,
                CreatedDate = DateTime.Now,
                UpdatedDate = DateTime.Now
            };
            await _context.AddAsync(NewProduct);
            await _context.SaveChangesAsync();
            if (model.CategoryId.Count > 0)
            {
                foreach (var cat in model.CategoryId)
                {
                    if (Categories.Select(x => x.Id).Contains(cat))
                    {
                        ProductCategories newcategory = new()
                        {
                            CategoryId = cat,
                            CreatedDate = DateTime.Now,
                            UpdatedDate = DateTime.Now,
                            ProductId = NewProduct.Id
                        };
                        await _context.AddAsync(newcategory);
                        await _context.SaveChangesAsync();
                    }
                    else
                    {
                        category += "some categories are not available";
                    }
                }
            }
            if (model.SubCategoryId.Count > 0)
            {
                foreach (var subcat in model.SubCategoryId)
                {
                    if (SubCategories.Select(x => x.Id).Contains(subcat))
                    {
                        ProductSubCategories newsubcategory = new()
                        {
                            SubCategoryId = subcat,
                            CreatedDate = DateTime.Now,
                            UpdatedDate = DateTime.Now,
                            ProductId = NewProduct.Id
                        };
                        await _context.AddAsync(newsubcategory);
                        await _context.SaveChangesAsync();
                    }
                    else
                    {
                        subcategory += "some sub-categories are not available";
                    }
                }
            }
            if (model.Tags.Count > 0)
            {
                foreach (var tag in model.Tags)
                {
                    if (Tags.Select(x => x.TagsMaster.TagName.ToLower()).Contains(tag.ToLower()))
                    {
                        ProductTags newsubcategory = new()
                        {
                            TagId = Tags.Where(x => x.TagsMaster.TagName.ToLower() == tag.ToLower())
                                        .Select(x => x.TagsMaster.Id)
                                        .FirstOrDefault(),
                            CreatedDate = DateTime.Now,
                            UpdatedDate = DateTime.Now,
                            ProductId = NewProduct.Id
                        };
                        await _context.AddAsync(newsubcategory);
                        await _context.SaveChangesAsync();
                    }
                    else
                    {
                        TagsMaster tagsMaster = new()
                        {
                            TagName = tag.ToLower(),
                            CreatedDate = DateTime.Now,
                            UpdatedDate = DateTime.Now
                        };
                        await _context.AddAsync(tagsMaster);
                        await _context.SaveChangesAsync();

                        ProductTags newsubcategory = new()
                        {
                            TagId = tagsMaster.Id,
                            CreatedDate = DateTime.Now,
                            UpdatedDate = DateTime.Now,
                            ProductId = NewProduct.Id
                        };
                        await _context.AddAsync(newsubcategory);
                        await _context.SaveChangesAsync();
                    }
                }
            }
            if (model.SubProductImages.Count > 0)
            {
                foreach (var image in model.SubProductImages)
                {
                    ProductImages newProductImage = new()
                    {
                        ProductImage = image,
                        CreatedDate = DateTime.Now,
                        UpdatedDate = DateTime.Now,
                        ProductId = NewProduct.Id
                    };
                    await _context.AddAsync(newProductImage);
                    await _context.SaveChangesAsync();

                }

            }
            if (category.Length > 0 || subcategory.Length > 0)
                return new ServiceResponse<long>(StatusCodes.Status400BadRequest,"some of the categories or sub categories are not available", 0);
            await _context.SaveChangesAsync();

            var WishListData = await _context.WishList.Include(x => x.WishListCategoriesAndSubCategories)
                                                      .ToListAsync();
            var prodCategories = await _context.ProductCategories.Where(x => x.ProductId == NewProduct.Id)
                                                                 .Select(x => x.CategoryId)
                                                                 .ToListAsync();
            List<Notifications> notifications = new();

            foreach (var Wish in WishListData)
            {
                if (Wish.WishListCategoriesAndSubCategories.Select(x => x.CategoryId).ToList().Equals(prodCategories))
                {
                    Notifications newNotification = new()
                    {
                        UserId = Wish.UserId,
                        SellerId = _currentUser.UserID,
                        WishListId = Wish.Id,
                        IsRead = false,
                        IsSeller = true,
                        CreatedDate = DateTime.UtcNow,
                        UpdatedDate = DateTime.UtcNow
                    };
                    notifications.Add(newNotification);
                }
            }

            var FollowUsersData = await _context.UserFollow.Select(x => new UserFollow
                                                        {
                                                            Id = x.Id,
                                                            FollowerId = x.FollowerId,
                                                            FolloweeId = x.FolloweeId,
                                                            DeleteFlag = x.DeleteFlag,
                                                            CreatedDate = x.CreatedDate
                                                        })
                                                        .Where(x => x.FollowerId == _currentUser.UserID)
                                                        .ToListAsync();
            foreach (var FollowUser in FollowUsersData)
            {
                Notifications newNotification = new()
                {
                    UserId = FollowUser.FolloweeId,
                    SellerId = _currentUser.UserID,
                    ProductId = NewProduct.Id,
                    IsRead = false,
                    IsSeller = true,
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow
                };
                notifications.Add(newNotification);
            }

            await _context.SaveChangesAsync();
            return new ServiceResponse<long>(200, "added successfully", NewProduct.Id);
        }



        public async Task<ApiResponse> UpdateProduct(EditProductModel model)
        {
            var CheckProduct = await _context.Product.Where(x => x.Id == model.ProductId &&
                                                                 x.SellerId == _currentUser.UserID)
                                                     .FirstOrDefaultAsync();
            var Categories = await _context.CategoryMaster.ToListAsync();
            var SubCategories = await _context.SubCategoryMasters.ToListAsync();
            var Tags = await _context.ProductTags.Include(x => x.TagsMaster).ToListAsync();
            string categories = "";
            string subcategories = "";
            if (CheckProduct == null)
                return new ApiResponse("No Product to Update", 400);

            // if (model.ProductImage != null)
            // {
            //     if (CheckProduct.ProductImage != null)
            //     {
            //         var removephoto = await _storage.DeleteLocalFile(CheckProduct.ProductImage);
            //     }
            //     var productImage = await _storage.SaveFile(model.ProductImage, model.FileExtension);
            //     if (productImage.StatusCode != 200)
            //         return new ApiResponse(productImage.Message, 400);

            //     CheckProduct.ProductImage = productImage.Data;
            // }
            if (model.DeletedImages.Count > 0) {
              var images = await _context.ProductImages.Where(x => model.DeletedImages.Contains(x.Id)).ToListAsync();
              _context.RemoveRange(images);

            }
            CheckProduct.ProductImage = (model.ProductImage == null) ? CheckProduct.ProductImage : model.ProductImage;
            CheckProduct.ProductTitle = (model.ProductName == null) ? CheckProduct.ProductTitle : model.ProductName;
            CheckProduct.Price = (model.Price == 0) ? CheckProduct.Price : model.Price;
            CheckProduct.GasFee = null;
            CheckProduct.Description = (model.Description == null) ? CheckProduct.Description : model.Description;
            CheckProduct.NFTStatus = model.NFTStatus;
            CheckProduct.ForSale = model.ForSale;
            CheckProduct.UpdatedDate = DateTime.UtcNow;
            CheckProduct.Link = (model.Link == null) ? CheckProduct.Link : model.Link;
            CheckProduct.Quantity = (model.Quantity == null) ? CheckProduct.Quantity : model.Quantity;
            if (model.SubProductImages.Count > 0)
            {
                foreach (var image in model.SubProductImages)
                {
                    ProductImages newProductImage = new()
                    {
                        ProductImage = image,
                        UpdatedDate = DateTime.Now,
                        ProductId =  model.ProductId,
                    };
                    await _context.AddAsync(newProductImage);

                }

            }
            if (model.CategoryId != null)
            {
                var cat = await _context.ProductCategories.Where(x => x.ProductId == model.ProductId).ToListAsync();
                _context.RemoveRange(cat);
                foreach (var category in model.CategoryId)
                {
                    if (Categories.Select(x => x.Id).Contains(category))
                    {
                        ProductCategories newcategory = new()
                        {
                            CategoryId = category,
                            CreatedDate = DateTime.Now,
                            UpdatedDate = DateTime.Now,
                            ProductId = CheckProduct.Id
                        };
                        await _context.AddAsync(newcategory);
                    }
                    else
                    {
                        categories += "some categories are not available";
                    }
                }

            }
            if (model.SubCategoryId != null)
            {
                var subcat = await _context.ProductSubCategories.Where(x => x.ProductId == model.ProductId).ToListAsync();
                _context.RemoveRange(subcat);
                foreach (var subcategory in model.SubCategoryId)
                {
                    if (SubCategories.Select(x => x.Id).Contains(subcategory))
                    {
                        ProductSubCategories newsubcategory = new()
                        {
                            SubCategoryId = subcategory,
                            CreatedDate = DateTime.Now,
                            UpdatedDate = DateTime.Now,
                            ProductId = CheckProduct.Id
                        };
                        await _context.AddAsync(newsubcategory);
                    }
                    else
                    {
                        subcategories += "some sub-categories are not available";
                    }
                }
            }
            if (model.Tags != null)
            {
                var tags = await _context.ProductTags.Where(x => x.ProductId == model.ProductId).ToListAsync();
                _context.RemoveRange(tags);
                foreach (var tag in model.Tags)
                {
                    if (Tags.Select(x => x.TagsMaster.TagName.ToLower()).Contains(tag.ToLower()))
                    {
                        ProductTags newsubcategory = new()
                        {
                            TagId = Tags.Where(x => x.TagsMaster.TagName.ToLower() == tag.ToLower()).Select(x => x.TagsMaster.Id)
                                        .FirstOrDefault(),
                            CreatedDate = DateTime.Now,
                            UpdatedDate = DateTime.Now,
                            ProductId = CheckProduct.Id
                        };
                        await _context.AddAsync(newsubcategory);
                    }
                    else
                    {
                        TagsMaster tagsMaster = new()
                        {
                            TagName = tag.ToLower(),
                            CreatedDate = DateTime.Now,
                            UpdatedDate = DateTime.Now
                        };
                        await _context.AddAsync(tagsMaster);
                        await _context.SaveChangesAsync();

                        ProductTags newProductTag = new()
                        {
                            TagId = tagsMaster.Id,
                            CreatedDate = DateTime.Now,
                            UpdatedDate = DateTime.Now,
                            ProductId = CheckProduct.Id
                        };
                        await _context.AddAsync(newProductTag);
                    }
                }
            }
            _context.Update(CheckProduct);
            await _context.SaveChangesAsync();


            var WishListData = await _context.WishList.Include(x => x.WishListCategoriesAndSubCategories)
                                                      .ToListAsync();
            var prodCategories = await _context.ProductCategories.Where(x => x.ProductId == CheckProduct.Id)
                                                                 .Select(x => x.CategoryId)
                                                                 .ToListAsync();
            List<Notifications> notifications = new();

            foreach (var Wish in WishListData)
            {
                if (Wish.WishListCategoriesAndSubCategories.Select(x => x.CategoryId).ToList().Equals(prodCategories))
                {
                    Notifications newNotification = new()
                    {
                        UserId = Wish.UserId,
                        SellerId = _currentUser.UserID,
                        WishListId = Wish.Id,
                        IsRead = false,
                        IsSeller = true,
                        CreatedDate = DateTime.UtcNow,
                        UpdatedDate = DateTime.UtcNow
                    };
                    notifications.Add(newNotification);
                }
            }
            await _context.SaveChangesAsync();
            return new ApiResponse("Successfully  Updated", 200);
        }



        public async Task<ApiResponse> DeleteProuct(long productId)
        {
            var getProduct = await (from date in _context.Product where date.Id == productId select date).FirstOrDefaultAsync();

            if (getProduct != null)
            {
                getProduct.IsDeleted = true;
                _context.Update(getProduct);
                await _context.SaveChangesAsync();
                return new ApiResponse("Successfully removed", 200);
            }
            else
                return new ApiResponse("Please pass a valid id to remove", 400);
        }


        public async Task<ServiceResponse<PagedResponse<GetProductModel>>> GetSellerProduct(FilterModel model, long id)
        {
            List<Product> products;
            if (id == 0)
            {
                products = await _context.Product.Include(x => x.Seller)
                                                .Include(x => x.ProductCategories).ThenInclude(x => x.CategoryMaster)
                                                .Include(x => x.ProductSubCategories).ThenInclude(x => x.SubCategoryMaster)
                                                .Include(x => x.ProductTags).ThenInclude(x => x.TagsMaster)
                                                .Include(x => x.UserProduct)
                                                .Include(x => x.TransactionLog)
                                                .Where(x => x.SellerId == _currentUser.UserID && x.IsDeleted == false)
                                                .ToListAsync();

                // return new ServiceResponse<List<GetProductModel>>(StatusCodes.Status200OK, "Successfully", result);
            }
            else
            {
                products = await _context.Product.Include(x => x.Seller)
                                                .Include(x => x.ProductCategories).ThenInclude(x => x.CategoryMaster)
                                                .Include(x => x.ProductSubCategories).ThenInclude(x => x.SubCategoryMaster)
                                                .Include(x => x.ProductTags).ThenInclude(x => x.TagsMaster)
                                                .Include(x => x.UserProduct)
                                                .Include(x => x.TransactionLog)
                                                .Where(x => x.SellerId == id && x.IsDeleted == false)
                                                .ToListAsync();

            }

            if (model.PriceFrom != 0 && model.PriceTo != 0)
            {
                products = products.Where(x => x.Price >= model.PriceFrom && x.Price <= model.PriceTo).ToList();
            }
            else if (model.PriceFrom != 0)
            {
                products = products.Where(x => x.Price >= model.PriceFrom).ToList();
            }
            else if (model.PriceTo != 0)
            {
                products = products.Where(x => x.Price <= model.PriceTo).ToList();
            }
            if (model.Filter == "Recently Added")
            {
                products = products.OrderByDescending(x => x.CreatedDate).ToList();
            }
            else if (model.Filter == "Popular")
            {
                products = products.OrderByDescending(x => x.UserProduct.Select(x => x.ProductId).Count()).ToList();
            }
            else if (model.Filter == "Oldest")
            {
                products = products.OrderBy(x => x.CreatedDate).ToList();
            }
            else if (model.Filter == "Highest Sold")
            {
                products = products.OrderByDescending(x => x.TransactionLog.Sum(x => x.Amount)).ToList();
            }
            else if (model.Filter == "Lowest Sold")
            {
                products = products.OrderByDescending(x => x.TransactionLog.Sum(x => x.Amount)).ToList();
            }
            else
            {
                //Recently Sold
                products = products.OrderByDescending(x => x.ProductTags.OrderByDescending(x => x.CreatedDate)
                                         .Select(x => x.CreatedDate)
                                         .FirstOrDefault())
                                         .ToList();
            }
            var newProducts = products.Select(x => new GetProductModel
            {
                Category = x.ProductCategories.Select(x => new ViewCategory
                {
                    CategoryName = x.CategoryMaster.Name,
                    CategoryId = x.CategoryMaster.Id
                }).ToList(),
                SubCategory = x.ProductSubCategories.Select(x => new ViewSubCategory
                {
                    SubCategoryName = x.SubCategoryMaster.SubCategoryName,
                    SubCategoryId = x.SubCategoryMaster.Id
                }).ToList(),
                Tags = x.ProductTags.Select(x => new ViewTags
                {
                    TagName = x.TagsMaster.TagName,
                    TagId = x.TagsMaster.Id
                }).ToList(),
                NFTStatus = x.NFTStatus,
                ForSale = x.ForSale,
                ProductId = x.Id,
                Description = x.Description,
                GasFee = x.GasFee,
                Price = x.Price,
                ProductImage = x.ProductImage,
                Link = x.Link,
                Quantity = x.Quantity,
                Seller = x.Seller.FirstName + " " + x.Seller.LastName,
                SellerEamil = x.Seller.EmailId,
                SellerId = x.Seller.Id,
                //  ProductImage = _storage.GetBase64(x.ProductImage).Base64,
                Extention = _storage.GetBase64(x.ProductImage).Extention,
                ProductName = x.ProductTitle,
                UpdatedDate = x.UpdatedDate,
                CreatedDate = x.CreatedDate

            }).ToList();




            if (model.Categories.Count != 0)
            {
                List<GetProductModel> result = new List<GetProductModel>();
                if (model.Categories.Count == 1 && model.Categories[0] == 0)
                {

                }
                else
                {
                    foreach (var cat in model.Categories)
                    {
                        var catfilter = newProducts.Where(x => x.Category.Select(x => x.CategoryId).ToList().Contains(cat)).ToList();
                        result.AddRange(catfilter);
                    }
                    newProducts = result;
                }
            }

            if (model.SubCategories.Count != 0)
            {
                List<GetProductModel> result = new List<GetProductModel>();
                if (model.SubCategories.Count == 1 && model.SubCategories[0] == 0)
                {

                }
                else
                {
                    foreach (var subcat in model.SubCategories)
                    {
                        var subcatfilter = newProducts.Where(x => x.SubCategory.Select(x => x.SubCategoryId).ToList().Contains(subcat))
                                                      .ToList();
                        result.AddRange(subcatfilter);
                    }
                    newProducts = result;
                }
            }

            if (model.Tags.Count != 0)
            {
                List<GetProductModel> result = new List<GetProductModel>();
                if (model.Tags.Count == 1 && model.Tags[0] == "string")
                {

                }
                else
                {
                    foreach (var tag in model.Tags)
                    {
                        var tagfilter = newProducts.Where(x => x.Tags.Select(x => x.TagName.ToLower()).ToList().Contains(tag))
                                                   .ToList();
                        result.AddRange(tagfilter);
                    }
                    newProducts = result;
                }
            }

            var newResult = PagedResponse<GetProductModel>.Create(newProducts, model.PageIndex, model.PageSize);
            return new ServiceResponse<PagedResponse<GetProductModel>>(StatusCodes.Status200OK, "Successfully", newResult);
            // return new ServiceResponse<List<GetProductModel>>(StatusCodes.Status400BadRequest, "invalid id", null);
        }





        public async Task<ServiceResponse<List<SellerTransactionModel>>> SellerPaymentDetails()
        {
            var sellerProducts = await _context.Product.Where(x => x.SellerId == _currentUser.UserID)
                                                       .Select(x => x.Id)
                                                       .ToListAsync();

            var paymentDetails = await _context.TransactionLog.Include(x => x.User)
                                                               .Include(x => x.Product)
                                                               .Include(x => x.UserOrders)
                                                               .Where(x => sellerProducts.Contains(x.ProductId) &&
                                                                           x.OrderStatus == "succeeded")
                                                               .Select(x => new SellerTransactionModel
                                                               {
                                                                   UserName = x.User.FirstName + " " + x.User.LastName,
                                                                   Amount = x.Amount,
                                                                   OrderDate = x.UserOrders.CreatedDate,
                                                                   OrderId = x.UserOrders.Id,
                                                                   ProductImage = x.Product.ProductImage,
                                                                   //    ProductImage = _storage.GetBase64(x.Product.ProductImage).Base64,
                                                                   Extention = _storage.GetBase64(x.Product.ProductImage).Extention,
                                                                   ProductName = x.Product.ProductTitle
                                                               })
                                                              .ToListAsync();
            return new ServiceResponse<List<SellerTransactionModel>>(StatusCodes.Status200OK,
                                                                     "transaction logs for the seller",
                                                                     paymentDetails);
        }


        public async Task<ServiceResponse<List<GetBuyerNoitificationModel>>> GetAllSellerNoitification()
        {
            var CurrentUser = _currentUser;
            var UserNotification = await _context.Notifications.Include(x => x.WishList)
                                                                .ThenInclude(x => x.WishListCategoriesAndSubCategories)
                                                                .Where(x => x.IsRead == false &&
                                                                            x.SellerId == _currentUser.UserID)
                                                                            //  && x.IsSeller == true)
                                                                .Select(x => new GetBuyerNoitificationModel
                                                                {
                                                                    Id = x.Id,
                                                                    Description = x.WishList.Description,
                                                                    Category = x.WishList.WishListCategoriesAndSubCategories
                                                                    .Where(x => x.CategoryId != null)
                                                                    .Select(x => new Category
                                                                    {
                                                                        Id = x.CategoryId,
                                                                        CategoryName = x.CategoryMaster.Name
                                                                    }).ToList(),

                                                                    SubCategory = x.WishList.WishListCategoriesAndSubCategories
                                                                    .Where(x => x.SubCategoryId != null)
                                                                    .Select(x => new SubCategory
                                                                    {
                                                                        Id = x.SubCategoryId,
                                                                        SubCategoryName = x.SubCategoryMaster.SubCategoryName
                                                                    }).ToList(),
                                                                    RequestedByUser = $"{x.User.FirstName} {x.User.LastName}",
                                                                    RequestedByUserId = x.UserId,
                                                                    RequestedDate = x.CreatedDate
                                                                })
                                                                .ToListAsync();
            if (UserNotification == null)
                return new ServiceResponse<List<GetBuyerNoitificationModel>>(StatusCodes.Status400BadRequest,
                                                                                "no notification found", null);
            return new ServiceResponse<List<GetBuyerNoitificationModel>>(StatusCodes.Status200OK,
                                                                            "all user notification", UserNotification);
        }
    }
}
