﻿

using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using MV4F.Entities;
using MV4F.IRepository;
using MV4F.IServices;
using MV4F.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MV4F.Services
{
    public class HomeServices : IHomeServices
    {
        private readonly MV4FContext _context;
        private readonly ICurrentUser _currentUser;
        private readonly IStorage _storage;

        public HomeServices(MV4FContext context, ICurrentUser currentUser, IStorage storage)
        {
            _context = context;
            _currentUser = currentUser;
            _storage = storage;
        }

        public async Task<ServiceResponse<PagedResponse<ViewProductModel>>> AllProducts(FilterModel model)
        {
            List<Product> AllProducts;
            // if (_currentUser != null) {
            //     AllProducts = await _context.Product.Include(x => x.Seller)
            //                                             .Include(x => x.ProductCategories).ThenInclude(x => x.CategoryMaster)
            //                                             .Include(x => x.ProductSubCategories).ThenInclude(x => x.SubCategoryMaster)
            //                                             .Include(x => x.ProductTags).ThenInclude(x => x.TagsMaster)
            //                                             .Include(x => x.UserProduct)
            //                                             .Include(x => x.TransactionLog)
            //                                             .Where(x => x.SellerId != _currentUser.UserID && x.IsDeleted == false)
            //                                             .ToListAsync();
            // }
            // else {
            //     AllProducts = await _context.Product.Include(x => x.Seller)
            //                                             .Include(x => x.ProductCategories).ThenInclude(x => x.CategoryMaster)
            //                                             .Include(x => x.ProductSubCategories).ThenInclude(x => x.SubCategoryMaster)
            //                                             .Include(x => x.ProductTags).ThenInclude(x => x.TagsMaster)
            //                                             .Include(x => x.UserProduct)
            //                                             .Include(x => x.TransactionLog)
            //                                             .Where(x => x.IsDeleted == false)
            //                                             .ToListAsync();
            // }
            AllProducts = await _context.Product.Include(x => x.Seller)
                                                .Include(x => x.ProductCategories).ThenInclude(x => x.CategoryMaster)
                                                .Include(x => x.ProductSubCategories).ThenInclude(x => x.SubCategoryMaster)
                                                .Include(x => x.ProductTags).ThenInclude(x => x.TagsMaster)
                                                .Include(x => x.UserProduct)
                                                .Include(x => x.TransactionLog)
                                                .Where(x => x.IsDeleted == false && x.ForSale == true && x.Price > 0)
                                                .ToListAsync();

            if (model.PriceFrom != 0 && model.PriceTo != 0)
            {
                AllProducts = AllProducts.Where(x => x.Price >= model.PriceFrom && x.Price <= model.PriceTo).ToList();
            }
            else if (model.PriceFrom != 0)
            {
                AllProducts = AllProducts.Where(x => x.Price >= model.PriceFrom).ToList();
            }
            else if (model.PriceTo != 0)
            {
                AllProducts = AllProducts.Where(x => x.Price <= model.PriceTo).ToList();
            }
            if (model.Filter == "Recently Added")
            {
                AllProducts = AllProducts.OrderByDescending(x => x.CreatedDate).ToList();
            }
            else if (model.Filter == "Popular")
            {
                AllProducts = AllProducts.OrderByDescending(x => x.UserProduct.Select(x => x.ProductId).Count()).ToList();
            }
            else if (model.Filter == "Oldest")
            {
                AllProducts = AllProducts.OrderBy(x => x.CreatedDate).ToList();
            }
            else if (model.Filter == "Highest Sold")
            {
                AllProducts = AllProducts.OrderByDescending(x => x.TransactionLog.Sum(x => x.Amount)).ToList();
            }
            else if (model.Filter == "Lowest Sold")
            {
                AllProducts = AllProducts.OrderByDescending(x => x.TransactionLog.Sum(x => x.Amount)).ToList();
            }
            else
            {
                //Recently Sold
                AllProducts = AllProducts.OrderByDescending(x => x.ProductTags.OrderByDescending(x => x.CreatedDate)
                                         .Select(x => x.CreatedDate)
                                         .FirstOrDefault())
                                         .ToList();
            }

            var newProducts = AllProducts.Select(x => new ViewProductModel
            {
                ProductId = x.Id,
                ProductName = x.ProductTitle,
                Category = x.ProductCategories.Select(x => new ViewCategory
                {
                    CategoryName = x.CategoryMaster.Name,
                    CategoryId = x.CategoryMaster.Id
                }).ToList(),
                SubCategory = x.ProductSubCategories.Select(x => new ViewSubCategory
                {
                    SubCategoryName = x.SubCategoryMaster.SubCategoryName,
                    SubCategoryId = x.SubCategoryMaster.Id
                }).ToList(),
                Tags = x.ProductTags.Select(x => new ViewTags
                {
                    TagName = x.TagsMaster.TagName,
                    TagId = x.TagsMaster.Id
                }).ToList(),
                Price = x.Price,
                GasFee = x.GasFee,
                ProductPicture = x.ProductImage,
                SubProductPictures = _context.ProductImages
                                              .Where(y => y.ProductId == x.Id)
                                              .Select(y => new SubProductPicturesModel
                                              {
                                                  Id = y.Id,
                                                  ProductPicture = y.ProductImage
                                              }).ToList(),
                Extention = _storage.GetBase64(x.ProductImage).Extention,
                Description = x.Description,
                CreatedDate = x.UpdatedDate,
                Seller = x.Seller.FirstName + " " + x.Seller.LastName,
                SellerEamil = x.Seller.EmailId,
                SellerId = x.Seller.Id,
                Link = x.Link,
                Quantity = x.Quantity
            }).ToList();




            if (model.Categories.Count != 0)
            {
                List<ViewProductModel> result = new List<ViewProductModel>();
                if (model.Categories.Count == 1 && model.Categories[0] == 0)
                {

                }
                else
                {
                    foreach (var cat in model.Categories)
                    {
                        var catfilter = newProducts.Where(x => x.Category.Select(x => x.CategoryId).ToList().Contains(cat)).ToList();
                        result.AddRange(catfilter);
                    }
                    newProducts = result;
                }
            }

            if (model.SubCategories.Count != 0)
            {
                List<ViewProductModel> result = new List<ViewProductModel>();
                if (model.SubCategories.Count == 1 && model.SubCategories[0] == 0)
                {

                }
                else
                {
                    foreach (var subcat in model.SubCategories)
                    {
                        var subcatfilter = newProducts.Where(x => x.SubCategory.Select(x => x.SubCategoryId).ToList().Contains(subcat))
                                                      .ToList();
                        result.AddRange(subcatfilter);
                    }
                    newProducts = result;
                }
            }

            if (model.Tags.Count != 0)
            {
                List<ViewProductModel> result = new List<ViewProductModel>();
                if (model.Tags.Count == 1 && model.Tags[0] == "string")
                {

                }
                else
                {
                    foreach (var tag in model.Tags)
                    {
                        var tagfilter = newProducts.Where(x => x.Tags.Select(x => x.TagName.ToLower()).ToList().Contains(tag))
                                                   .ToList();
                        result.AddRange(tagfilter);
                    }
                    newProducts = result;
                }
            }

            var newResult = PagedResponse<ViewProductModel>.Create(newProducts, model.PageIndex, model.PageSize);

            return new ServiceResponse<PagedResponse<ViewProductModel>>(StatusCodes.Status200OK, "all products", newResult);

        }



        public async Task<ServiceResponse<ViewProductModel>> ViewProductById(long Id)
        {
            var AllProducts = await _context.Product.Include(x => x.Seller)
                                                    .Include(x => x.ProductCategories).ThenInclude(x => x.CategoryMaster)
                                                    .Include(x => x.ProductSubCategories).ThenInclude(x => x.SubCategoryMaster)
                                                    .Include(x => x.ProductTags).ThenInclude(x => x.TagsMaster)
                                                    .Where(x => x.Id == Id)
                                                    .Select(x => new ViewProductModel
                                                    {
                                                        ProductId = x.Id,
                                                        ProductName = x.ProductTitle,
                                                        Category = x.ProductCategories.Select(x => new ViewCategory
                                                        {
                                                            CategoryName = x.CategoryMaster.Name,
                                                            CategoryId = x.CategoryMaster.Id
                                                        }).ToList(),
                                                        SubCategory = x.ProductSubCategories.Select(x => new ViewSubCategory
                                                        {
                                                            SubCategoryName = x.SubCategoryMaster.SubCategoryName,
                                                            SubCategoryId = x.SubCategoryMaster.Id
                                                        }).ToList(),
                                                        Tags = x.ProductTags.Select(x => new ViewTags
                                                        {
                                                            TagName = x.TagsMaster.TagName,
                                                            TagId = x.TagsMaster.Id
                                                        }).ToList(),
                                                        Price = x.Price,
                                                        GasFee = x.GasFee,
                                                        ProductPicture = x.ProductImage,
                                                        SubProductPictures = _context.ProductImages
                                                                                      .Where(y => y.ProductId == x.Id)
                                                                                      .Select(y => new SubProductPicturesModel
                                                                                      {
                                                                                          Id = y.Id,
                                                                                          ProductPicture = y.ProductImage
                                                                                      }).ToList(),

                                                        Extention = _storage.GetBase64(x.ProductImage).Extention,
                                                        Description = x.Description,
                                                        Seller = x.Seller.FirstName + " " + x.Seller.LastName,
                                                        SellerId = x.SellerId,
                                                        SellerEamil = x.Seller.EmailId,
                                                        Link = x.Link,
                                                        Quantity = x.Quantity
                                                    })
                                                    .FirstOrDefaultAsync();
            if (AllProducts == null)
                return new ServiceResponse<ViewProductModel>(StatusCodes.Status400BadRequest,
                                                             "product not found or already sold", null);
            return new ServiceResponse<ViewProductModel>(StatusCodes.Status200OK, "all products", AllProducts);
        }

        public async Task<ViewProductForNftModel> ViewProductForNftById(long Id)
        {
            var AllProducts = await _context.Product.Include(x => x.Seller)
                                                    .Include(x => x.ProductCategories).ThenInclude(x => x.CategoryMaster)
                                                    .Include(x => x.ProductSubCategories).ThenInclude(x => x.SubCategoryMaster)
                                                    .Include(x => x.ProductTags).ThenInclude(x => x.TagsMaster)
                                                    .Where(x => x.Id == Id)
                                                    .Select(x => new ViewProductForNftModel
                                                    {
                                                        ProductName = x.ProductTitle,
                                                        ProductPicture = x.ProductImage,
                                                        Description = x.Description,
                                                        CategoryName = x.ProductCategories.Select(x => new ViewCategory
                                                        {
                                                            CategoryName = x.CategoryMaster.Name,
                                                        }).FirstOrDefault().CategoryName,
                                                        SubCategoryName = x.ProductSubCategories.Select(x => new ViewSubCategory
                                                        {
                                                            SubCategoryName = x.SubCategoryMaster.SubCategoryName,
                                                        }).FirstOrDefault().SubCategoryName,
                                                        TagName = x.ProductTags.Select(x => new ViewTags
                                                        {
                                                            TagName = x.TagsMaster.TagName,
                                                        }).FirstOrDefault().TagName,   
                                                        Rarity = x.Rarity,                                     
                                                    })
                                                    .FirstOrDefaultAsync();
            return AllProducts;
        }

        public async Task<ServiceResponse<List<RolesModel>>> AllRoles()
        {
            var roles = await _context.Role.Select(x => new RolesModel
            {
                RoleId = x.Id,
                RoleName = x.RoleName
            }).ToListAsync();

            return new ServiceResponse<List<RolesModel>>(StatusCodes.Status200OK, "all roles", roles);
        }


        public async Task<ServiceResponse<InterestedCategoriesModel>> UserInterestedCategoriesAndSubCategories()
        {
            var Data = await _context.InterestedCategories.Include(x => x.CategoryMaster)
                                                          .Include(x => x.SubCategoryMaster)
                                                          .Where(x => x.UserId == _currentUser.UserID)
                                                          .ToListAsync();
            var categories = Data.Where(x => x.CategoryId != null).Select(x => new InterestedCategory
            {
                CategoryId = x.CategoryId,
                CategoryName = x.CategoryMaster.Name
            }).ToList();

            var subCategories = Data.Where(x => x.SubCategoryId != null).Select(x => new InterestedSubCategory
            {
                SubCategoryId = x.SubCategoryId,
                SubCategoryName = x.SubCategoryMaster.SubCategoryName
            }).ToList();

            var result = new InterestedCategoriesModel()
            {
                InterestedCategory = categories,
                InterestedSubCategory = subCategories
            };

            return new ServiceResponse<InterestedCategoriesModel>(StatusCodes.Status200OK, "all user Interested categories", result);
        }

        public async Task<ServiceResponse<object>> GetTopFollower()
        {
            var topFollower = await _context.User
                .Select(x => new User
                {
                    Id = x.Id,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    ProfilePicture = x.ProfilePicture,
                    FollowerCount = x.FollowerCount,
                    EmailId = x.EmailId
                })
                .OrderByDescending(x => x.FollowerCount)
                .Take(5)
                .ToListAsync();

            return new ServiceResponse<object>(200, "success", topFollower);
        }
    }
}
