﻿using MV4F.Common;
using MV4F.IServices;

namespace MV4F.Services
{
    public class FileExtentionService : IFileExtentionService
    {
        public string GetExtension(string subString)
        {
            string extension;
            switch (subString.ToUpper())
            {
                case Constants.PngSubString:
                    extension = Constants.PngExt;
                    break;
                case Constants.JpgSubString:
                    extension = Constants.JpgExt;
                    break;
                case Constants.mp4SubString:
                    extension = Constants.mp4Ext;
                    break;
                case Constants.pdfSubString:
                    extension = Constants.pdfExt;
                    break;
                case Constants.icoSubString:
                    extension = Constants.icoExt;
                    break;
                case Constants.rarSubString:
                    extension = Constants.rarExt;
                    break;
                case Constants.rtfSubString:
                    extension = Constants.rtfExt;
                    break;
                case Constants.txtSubString:
                    extension = Constants.txtExt;
                    break;
                case Constants.srtSubStringa:
                case Constants.srtSubStringb:
                    extension = Constants.srtExt;
                    break;
                default:
                    extension = string.Empty;
                    break;
            }

            return extension;
        }
    }
}
