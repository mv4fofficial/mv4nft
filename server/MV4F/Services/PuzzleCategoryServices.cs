﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using MV4F.Entities;
using MV4F.IRepository;
using MV4F.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace MV4F.Repository
{
    public class PuzzleCategoryServices : IPuzzleCategoryServices
    {
        private readonly IConfiguration _configuration;
        private readonly IEmailService _emailService;
        private readonly IStorage _storage;
        private readonly ICurrentUser _currentUser;
        private readonly MV4FContext _context;

        public PuzzleCategoryServices(IConfiguration configuration,
                                IStorage storage,
                                ICurrentUser currentUser,
                                MV4FContext context)
        {
            _configuration = configuration;
            _storage = storage;
            _currentUser = currentUser;
            _context = context;
        }

        public async Task<ServiceResponse<List<PuzzleCategoryViewModel>>> ViewPuzzleCategories()
        {
            List<PuzzleCategoryViewModel> puzzleCategoryViewModels = new List<PuzzleCategoryViewModel>();

            puzzleCategoryViewModels = await _context.PuzzleCategory.Select(x => new PuzzleCategoryViewModel
            {
                Id = x.Id,
                Name = x.Name,
                Image = x.Image,
                CreatedDate = x.CreatedDate,
                UpdatedDate = x.UpdatedDate
            }).ToListAsync();

            return new ServiceResponse<List<PuzzleCategoryViewModel>>(200, "puzzle categories", puzzleCategoryViewModels);
        }

        public async Task<ServiceResponse<PuzzleCategoryViewModel>> ViewPuzzlesByCategoryId(long id)
        {
            List<GameAssetCategoryViewModel> gameAssetCategories = new List<GameAssetCategoryViewModel>();

            gameAssetCategories = await _context.GameAssetCategory
                            .Where(x => x.CategoryId == id)
                            .Select(x => new GameAssetCategoryViewModel()
                            {
                                Id = x.Id,
                                CategoryId = x.CategoryId,
                                GameAssetId = x.GameAssetId,
                                CreatedDate = x.CreatedDate,
                                UpdatedDate = x.UpdatedDate,
                            }).ToListAsync();

            List<GameAssetsViewModel> gameAssets = new List<GameAssetsViewModel>();

            GameAssetsViewModel gameAssetsViewModel;

            for (int i = 0; i < gameAssetCategories.Count; i++)
            {
                gameAssetsViewModel = _context.GameAssets.Where(x => x.Id == id)
                    .Select(x => new GameAssetsViewModel
                    {
                        Id = x.Id,
                        Image = x.Image,
                        Type = x.Type,
                        CoinPrice = x.CoinPrice,
                        DiamondPrice = x.DiamondPrice,
                        CreatedDate = x.CreatedDate,
                        UpdatedDate = x.UpdatedDate,
                    }).FirstOrDefault();

                gameAssets.Add(gameAssetsViewModel);
            }

            Console.Write("Get Assets : " + gameAssets.Count);

            PuzzleCategoryViewModel puzzleCategory = await _context.PuzzleCategory
                                                        .Where(x => x.Id == id)
                                                        .Select(x => new PuzzleCategoryViewModel
                                                        {
                                                            Id = x.Id,
                                                            Name = x.Name,
                                                            Image = x.Image,
                                                            GameAssets = gameAssets,
                                                            CreatedDate = x.CreatedDate,
                                                            UpdatedDate = x.UpdatedDate
                                                        }).FirstOrDefaultAsync();

            return new ServiceResponse<PuzzleCategoryViewModel>(200, "puzzle category by id", puzzleCategory);
        }

        public async Task<ApiResponse> AddPuzzleCategory(PuzzleCategoryAddModel model)
        {
            PuzzleCategories newPuzzleCategory = new()
            {
                Id = model.Id,
                Name = model.Name,
                Image = model.Image,
                CreatedDate = DateTime.Now,
                UpdatedDate = DateTime.Now,
            };

            await _context.AddAsync(newPuzzleCategory);
            await _context.SaveChangesAsync();

            return new ApiResponse("added puzzle category successfully", 200);
        }
    }
}
