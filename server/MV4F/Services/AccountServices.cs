﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using MV4F.Entities;
using MV4F.IRepository;
using MV4F.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace MV4F.Repository
{
    public class AccountServices : IAccountServices
    {
        private readonly IConfiguration _configuration;
        private readonly IEmailService _emailService;
        private readonly IStorage _storage;
        private readonly ICurrentUser _currentUser;
        private readonly MV4FContext _context;

        public AccountServices(IConfiguration configuration,
                                IEmailService emailService,
                                IStorage storage,
                                ICurrentUser currentUser,
                                MV4FContext context)
        {
            _configuration = configuration;
            _emailService = emailService;
            _storage = storage;
            _currentUser = currentUser;
            _context = context;
        }

        public bool IsPrime(int candidate)
        {
            if (candidate == 1)
            {
                return false;
            }
            throw new NotImplementedException("Not implemented.");
        }

        public async Task<ApiResponse> Register(RegisterModel model)
        {
            var checkUser = await _context.User.Where(x => x.EmailId == model.EmailId).FirstOrDefaultAsync();
            var countries = await _context.Country.Select(x => x.Id).ToListAsync();
            var categories = await _context.CategoryMaster.Select(x => x.Id).ToListAsync();
            var subcategories = await _context.SubCategoryMasters.Select(x => x.Id).ToListAsync();
            if (checkUser is not null)
                return new ApiResponse("The email or phone number already exists", 400);
            if (model.ContactNumber.ToString().Length > 14)
                return new ApiResponse("Please Enter valid Phone Number", StatusCodes.Status406NotAcceptable);
            if (!countries.Contains(model.CountryId))
                return new ApiResponse("Invalid Country", StatusCodes.Status406NotAcceptable);

            Role userRole = new();
            var user = new User();
            if (model.RoleId is 1)
            {
                userRole = await _context.Role.Where(x => x.RoleName == "Admin").FirstOrDefaultAsync();
            }
            else if (model.RoleId is 2)
            {
                userRole = await _context.Role.Where(x => x.RoleName == "User").FirstOrDefaultAsync();
            }
            else if (model.RoleId is 3)
            {
                userRole = await _context.Role.Where(x => x.RoleName == "Seller").FirstOrDefaultAsync();
            }

            userRole = await _context.Role.Where(x => x.Id == model.RoleId).FirstOrDefaultAsync();
            if (userRole == null)
                return new ApiResponse("Role not found", 403);

            user.EmailId = model.EmailId;
            user.Password = Encipher(model.Password);
            user.ContactNumber = model.ContactNumber;
            user.FirstName = model.FirstName;
            user.LastName = model.LastName;
            user.CreatedDate = DateTime.UtcNow;
            user.UpdatedDate = DateTime.UtcNow;
            user.IsActive = false;
            user.DateOfBirth = model.DateOfBirth;
            user.CountryId = model.CountryId;

            await _context.AddAsync(user);
            await _context.SaveChangesAsync();

            UserRole role = new()
            {
                RoleId = userRole.Id,
                UserId = user.Id,
                CreatedDate = DateTime.UtcNow,
                UpdatedDate = DateTime.UtcNow
            };

            PasswordResetToken token = new()
            {
                Token = Guid.NewGuid().ToString(),
                Email = user.EmailId,
                ExpiryDate = DateTime.UtcNow.AddHours(3),
                CreatedDate = DateTime.UtcNow,
                UpdatedDate = DateTime.UtcNow
            };
            List<InterestedCategories> userCategories = new();

            if (model.Categories.Count != 0)
            {
                _context.RemoveRange(userCategories);
                foreach (var category in model.Categories)
                {
                    if (categories.Contains(category))
                    {
                        InterestedCategories newCat = new()
                        {
                            UserId = user.Id,
                            CategoryId = category
                        };
                        userCategories.Add(newCat);
                    }
                }
            }

            List<InterestedCategories> userSubCategories = new();
            if (model.SubCategories.Count != 0)
            {
                _context.RemoveRange(userSubCategories);
                foreach (var subCategory in model.SubCategories)
                {
                    if (subcategories.Contains(subCategory))
                    {
                        InterestedCategories newSubCat = new()
                        {
                            UserId = user.Id,
                            SubCategoryId = subCategory
                        };
                        userSubCategories.Add(newSubCat);
                    }
                }
            }
            _context.AddRange(userCategories);
            _context.AddRange(userSubCategories);
            await _context.PasswordResetToken.AddAsync(token);
            await _context.AddAsync(role);
            await _context.SaveChangesAsync();

            await _emailService.VerifyRegistrationEmail($"{user.FirstName} {user.LastName}",
            user.EmailId, $"{_configuration.GetValue<string>("Domain:React")}/registration-verify-email/{user.EmailId}/{token.Token}");

            return new ApiResponse("Registration Successful, Please Verify your Email", 200);
        }


        public async Task<ApiResponse> VerifyEmail(string token)
        {
            var prt = await _context.PasswordResetToken.Where(x => x.Token == token).FirstOrDefaultAsync();
            if (prt != null)
            {
                var appuser = await _context.User.Where(x => x.EmailId == prt.Email).FirstOrDefaultAsync();
                if (appuser != null)
                {
                    appuser.IsEmailVerified = true;
                    appuser.UpdatedDate = DateTime.UtcNow;
                    _context.Update(appuser);
                    _context.Remove(prt);
                    await _context.SaveChangesAsync();
                    return new ApiResponse("Email Verified  Successful", 200);
                }
                return new ApiResponse("Wrong reset link", 404);
            }
            return new ApiResponse("Wrong reset link", 400);
        }


        public async Task<ServiceResponse<LoginResponse>> Login(LoginModel model)
        {
            var appUser = await _context.User.Where(x => x.EmailId == model.EmailId)
                                             .Include(x => x.UserRole)
                                             .ThenInclude(x => x.Role)
                                             .FirstOrDefaultAsync();

            if (appUser is not null)
            {
                var userRole = await _context.UserRole.Where(x => x.UserId == appUser.Id)
                                                      .Select(x => x.Role.RoleName)
                                                      .FirstOrDefaultAsync();
            }

            if (appUser is not null && (Decipher(appUser.Password) == model.Password))
            {
                List<string> roles = appUser.UserRole.Select(x => x.Role.RoleName).ToList();

                var refreshToken = GenerateRefreshToken();
                appUser.RefreshToken.Add(refreshToken);
                await _context.SaveChangesAsync();
                var response = new LoginResponse
                {
                    Id = appUser.Id,
                    AccessToken = AccessToken(appUser, roles),
                    ExpiresIn = 3600,
                    RefreshToken = refreshToken.Token,
                    Roles = roles,
                    TokenType = "Bearer",
                    UserId = appUser.Id,
                    Username = $"{appUser.EmailId}"
                };
                return new ServiceResponse<LoginResponse>(StatusCodes.Status200OK, "please login using a token", response);
            }
            else
            {
                return new ServiceResponse<LoginResponse>(StatusCodes.Status404NotFound,
                                                          "please enter a valid user-name and password", null);
            }
        }

        public async Task<ServiceResponse<LoginResponse>> ThirdPartyRegisterAndLogin(ThirdPartyRegisterAndLoginModel model)
        {
            var appUser = await _context.User.Where(x => x.UUID == model.UUID)
                                             .Include(x => x.UserRole)
                                             .ThenInclude(x => x.Role)
                                             .FirstOrDefaultAsync();

            if (appUser is not null)
            {
                List<string> roles = appUser.UserRole.Select(x => x.Role.RoleName).ToList();

                var refreshToken = GenerateRefreshToken();
                appUser.RefreshToken.Add(refreshToken);
                await _context.SaveChangesAsync();
                var response = new LoginResponse
                {
                    AccessToken = AccessToken(appUser, roles),
                    ExpiresIn = 3600,
                    RefreshToken = refreshToken.Token,
                    Roles = roles,
                    TokenType = "Bearer",
                    UserId = appUser.Id,
                    Username = $"{appUser.EmailId}"
                };
                return new ServiceResponse<LoginResponse>(StatusCodes.Status200OK, "please login using a token", response);
            }
            else
            {
                var user = new User();
                Role userRole = new();
                //long roleId = 3;
                //userRole = await _context.Role.Where(x => x.RoleName == "Buyer").FirstOrDefaultAsync();

                //userRole = await _context.Role.Where(x => x.Id == roleId).FirstOrDefaultAsync();
                // if (userRole == null)
                //     return new ServiceResponse<LoginResponse>(StatusCodes.Status404NotFound,
                //                                               "Role Not Found", null);
                user.UUID = model.UUID;
                user.ProfilePicture = model.ProfilePicture;
                user.EmailId = model.EmailId;
                user.FirstName = model.FirstName;
                user.CreatedDate = DateTime.UtcNow;
                user.UpdatedDate = DateTime.UtcNow;
                user.IsEmailVerified = model.IsEmailVerified;
                user.IsActive = true;
                await _context.AddAsync(user);
                await _context.SaveChangesAsync();
                UserRole role = new()
                {
                    RoleId = 3,
                    UserId = user.Id,
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow
                };
                await _context.AddAsync(role);
                await _context.SaveChangesAsync();
                var newUser = await _context.User.Where(x => x.UUID == model.UUID)
                                                 .Include(x => x.UserRole)
                                                 .ThenInclude(x => x.Role)
                                                 .FirstOrDefaultAsync();
                List<string> roles = newUser.UserRole.Select(x => x.Role.RoleName).ToList();
                var refreshToken = GenerateRefreshToken();
                user.RefreshToken.Add(refreshToken);
                await _context.SaveChangesAsync();
                var response = new LoginResponse
                {
                    AccessToken = AccessToken(newUser, roles),
                    ExpiresIn = 3600,
                    RefreshToken = refreshToken.Token,
                    Roles = roles,
                    TokenType = "Bearer",
                    UserId = user.Id,
                    Username = $"{user.EmailId}"
                };

                return new ServiceResponse<LoginResponse>(StatusCodes.Status200OK,
                                                          "Register And Login Success", response);
            }
        }


        private string AccessToken(User appUser, List<string> roles)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_configuration.GetValue<string>("JwtOptions:SecurityKey"));
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Issuer = _configuration.GetValue<string>("JwtOptions:Issuer"),
                Audience = _configuration.GetValue<string>("JwtOptions:Audience"),
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Email, appUser.EmailId),
                    new Claim(ClaimTypes.NameIdentifier, $"{appUser.Id}"),

                    new Claim("roles", JsonConvert.SerializeObject(roles))
                }),
                Expires = DateTime.UtcNow.AddHours(9),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            roles.ForEach(x => tokenDescriptor.Subject.AddClaim(new Claim(ClaimTypes.Role, x)));
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        private static RefreshToken GenerateRefreshToken()
        {
            // Type or member is obsolete
            using var rngCryptoServiceProvider = new RNGCryptoServiceProvider();
            // Type or member is obsolete
            var randomBytes = new byte[64];
            rngCryptoServiceProvider.GetBytes(randomBytes);
            return new RefreshToken
            {
                Token = Convert.ToBase64String(randomBytes),
                Expires = DateTime.UtcNow.AddDays(7),
                Created = DateTime.UtcNow,
                UpdatedDate = DateTime.UtcNow
            };
        }

        private static string Encipher(string Password)
        {
            string key = "abcdefghijklmnopqrstuvwxyz1234567890";
            byte[] bytesBuff = Encoding.Unicode.GetBytes(Password);
            using (System.Security.Cryptography.Aes aes = System.Security.Cryptography.Aes.Create())
            {
                Rfc2898DeriveBytes crypto = new(key,
                    new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                aes.Key = crypto.GetBytes(32);
                aes.IV = crypto.GetBytes(16);
                using MemoryStream mStream = new();
                using (CryptoStream cStream = new(mStream, aes.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cStream.Write(bytesBuff, 0, bytesBuff.Length);
                    cStream.Close();
                }
                Password = Convert.ToBase64String(mStream.ToArray());
            }
            return Password;
        }

        private static string Decipher(string Password)
        {
            string key = "abcdefghijklmnopqrstuvwxyz1234567890";
            Password = Password.Replace(" ", "+");
            byte[] bytesBuff = Convert.FromBase64String(Password);
            using (Aes aes = Aes.Create())
            {
                Rfc2898DeriveBytes crypto = new(key,
                    new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                aes.Key = crypto.GetBytes(32);
                aes.IV = crypto.GetBytes(16);
                using MemoryStream mStream = new();
                using (CryptoStream cStream = new(mStream, aes.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    cStream.Write(bytesBuff, 0, bytesBuff.Length);
                    cStream.Close();
                }
                Password = Encoding.Unicode.GetString(mStream.ToArray());
            }
            return Password;
        }


        public async Task<ApiResponse> ResetPassword(string email, string token, string password)
        {
            var appuser = await _context.User.Where(x => x.EmailId == email &&
                                                         x.IsActive == false)
                                             .FirstOrDefaultAsync();
            if (appuser != null)
            {
                var prt = await _context.PasswordResetToken.Where(x => x.Email == email &&
                                                                       x.Token == token)
                                                           .FirstOrDefaultAsync();
                if (prt != null)
                {
                    appuser.Password = Encipher(password);
                    appuser.UpdatedDate = DateTime.UtcNow;
                    _context.Entry(appuser).State = EntityState.Modified;
                    _context.Remove(prt);
                    await _context.SaveChangesAsync();
                    return new ApiResponse("Password reset successful", 200);
                }
            }
            return new ApiResponse("Wrong reset link", 404);
        }


        public async Task<ServiceResponse<LoginResponse>> RefreshToken(RefreshTokenModel model)
        {
            var token = await _context.RefreshToken.Where(x => x.Token == model.RefreshToken).FirstOrDefaultAsync();
            if (token is not null && DateTime.UtcNow <= token.Expires)
            {
                var appUser = await _context.User.Where(x => x.Id == token.UserId)
                                                 .Include(x => x.UserRole)
                                                 .ThenInclude(x => x.Role)
                                                 .FirstOrDefaultAsync();
                List<string> roles = appUser.UserRole.Select(x => x.Role.RoleName).ToList();
                var rt = GenerateRefreshToken();
                appUser.RefreshToken.Add(rt);
                _context.Remove(token);
                await _context.SaveChangesAsync();

                var res = new LoginResponse
                {
                    AccessToken = AccessToken(appUser, roles),
                    ExpiresIn = 86400,
                    RefreshToken = rt.Token,
                    Roles = roles,
                    TokenType = "Bearer",
                    Username = $"{appUser.EmailId}"
                };
                return new ServiceResponse<LoginResponse>(StatusCodes.Status200OK, "", res);
            }
            else
            {
                return new ServiceResponse<LoginResponse>(StatusCodes.Status401Unauthorized, "Invalid reset token", null);
            }
        }


        public async Task<ApiResponse> ForgotPassword(GetEmail model)
        {
            var appuser = await _context.User.Where(x => x.EmailId == model.Email).FirstOrDefaultAsync();
            if (appuser != null)
            {
                PasswordResetToken token = new()
                {
                    Token = Guid.NewGuid().ToString(),
                    Email = model.Email,
                    ExpiryDate = DateTime.Now.AddHours(3),
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow
                };
                await _context.PasswordResetToken.AddAsync(token);
                await _context.SaveChangesAsync();
                await _emailService.ForgotPasswordEmail(appuser.EmailId,
                                                        $"{_configuration.GetValue<string>("Domain:React")}" +
                                                        $"/change-password/{appuser.EmailId}" +
                                                        $"/{token.Token}");

                return new ApiResponse("Password reset link sent to your email", 200);
            }
            return new ApiResponse("please enter a valid email", 400);
        }


        public async Task<ApiResponse> ChangePassword(string currentEmailId, ChangePasswordModel model)
        {
            var appuser = await _context.User.Where(x => x.EmailId == currentEmailId).FirstOrDefaultAsync();
            if (appuser is not null && (Decipher(appuser.Password) == model.CurrentPassword))
            {
                appuser.Password = Encipher(model.Password);
                appuser.UpdatedDate = DateTime.UtcNow;
                _context.Entry(appuser).State = EntityState.Modified;
                await _context.SaveChangesAsync();

                return new ApiResponse("password has been changed successfully", 200);
            }
            else
            {
                return new ApiResponse("please enter a valid password", 400);
            }
        }

        public async Task<List<ContactUsModel>> ContactUs()
        {
            var getintounch = await _context.UserContact.Select(s => new ContactUsModel
            {
                Name = s.Name,
                Email = s.EmailId,
                PhoneNumber = s.ContactNumber,
                Message = s.Comments
            }).ToListAsync();

            return getintounch;
        }


        public async Task<ApiResponse> ContactUs(ContactUsModel model)
        {
            if (model.Email is not null)
            {
                if (string.IsNullOrEmpty(model.Email) || string.IsNullOrEmpty(model.Name))
                    return new ApiResponse("emailId is mandatory", 400);

                var userContactdata = new UserContact
                {
                    Name = model.Name,
                    EmailId = model.Email,
                    ContactNumber = model.PhoneNumber,
                    Comments = model.Message,
                    CreatedDate = DateTime.UtcNow
                };

                await _context.AddRangeAsync(userContactdata);
                await _context.SaveChangesAsync();


                return new ApiResponse("Successfully Added ", 200);
            }
            return new ApiResponse("please enter a valid email", 400);
        }

        public async Task<ServiceResponse<List<UserRequestViewModel>>> ViewUserRequests()
        {
            ProfileViewModel userProfile = new();
            var userRequests = await _context.UserRequest.Include(x => x.User)
                                                        .Select(x => new UserRequestViewModel
                                                        {
                                                            UserId = x.UserId,
                                                            UserName = $"{x.User.FirstName} {x.User.LastName}",
                                                            RequestedRoleId = x.RequestedRoleId,
                                                            RequestType = x.RequestType,
                                                            Description = x.Description,
                                                            Status = x.Status,
                                                            AttachFileUrl = x.AttachFileUrl,
                                                            CreatedDate = x.CreatedDate,
                                                        }).ToListAsync();
            return new ServiceResponse<List<UserRequestViewModel>>(200, "user requests", userRequests);
        }

        public async Task<ServiceResponse<ProfileViewModel>> ViewProfile(long id)
        {
            ProfileViewModel userProfile = new();
            if (id == 0)
            {
                var userRequest = await _context.UserRequest.Where(x => x.UserId == _currentUser.UserID)
                                                                         .FirstOrDefaultAsync();
                var itemsCreated = await _context.Product.Where(x => x.SellerId == _currentUser.UserID).LongCountAsync();
                var sellerProducts = await _context.Product.Where(x => x.SellerId == _currentUser.UserID)
                                                           .Select(x => x.Id)
                                                           .ToListAsync();
                var itemsSold = await _context.TransactionLog.Include(x => x.User)
                                                                   .Include(x => x.Product)
                                                                   .Include(x => x.UserOrders)
                                                                   .Where(x => sellerProducts.Contains(x.ProductId) &&
                                                                               x.OrderStatus == "succeeded")
                                                                  .LongCountAsync();
                var itemsBought = await _context.TransactionLog.Include(x => x.User)
                                                                   .Include(x => x.Product)
                                                                   .Include(x => x.UserOrders)
                                                                   .Where(x => x.UserId == _currentUser.UserID &&
                                                                               x.OrderStatus == "succeeded")
                                                                  .LongCountAsync();
                bool isRequestPending = (userRequest != null && userRequest.Status == "pending");
                userProfile = await _context.User.Include(x => x.Country)
                                                    .Where(x => x.Id == _currentUser.UserID)
                                                    .Select(x => new ProfileViewModel
                                                    {
                                                        Name = $"{x.FirstName} {x.LastName}",
                                                        EmailId = x.EmailId,
                                                        PhoneNumber = x.ContactNumber,
                                                        ProfilePicture = x.ProfilePicture,
                                                        BackgroundPicture = x.BackgroundPicture,
                                                        Description = x.Description,
                                                        DateOfBirth = x.DateOfBirth,
                                                        CreatedDate = x.CreatedDate,
                                                        IsRequestPending = isRequestPending,
                                                        ItemsOwned = itemsCreated + itemsBought,
                                                        ItemsCreated = itemsCreated,
                                                        ItemsSold = itemsSold,
                                                        ItemsBought = itemsBought,
                                                        StripeAccount = x.StripeId,
                                                        StripeStatus = x.StripeStatus,
                                                        Country = new CountryModel
                                                        {
                                                            Id = (x.Country != null) ? x.Country.Id : 0,
                                                            Name = (x.Country != null) ? x.Country.Name : "",
                                                            CountryCode = (x.Country != null) ? x.Country.CountryCode : "",
                                                            IsRestricted = (x.Country != null) ? x.Country.IsRestricted : false,
                                                            PhoneCode = (x.Country != null) ? x.Country.PhoneCode : ""
                                                        },
                                                        Bio = x.Bio
                                                    })
                                                    .FirstOrDefaultAsync();
                var roles = _context.UserRole.Include(x => x.Role).Where(x => x.UserId == _currentUser.UserID).Select(x => x.Role.RoleName).ToList();
                userProfile.Roles = roles;
            }
            else
            {
                userProfile = await _context.User.Include(x => x.Country)
                    .Where(x => x.Id == id)
                    .Select(x => new ProfileViewModel
                    {
                        Name = x.FirstName+" "+x.LastName,
                        EmailId = x.EmailId,
                        PhoneNumber = x.ContactNumber,
                        ProfilePicture = x.ProfilePicture,
                        BackgroundPicture = x.BackgroundPicture,
                        Description = x.Description,
                        DateOfBirth = x.DateOfBirth,
                        CreatedDate = x.CreatedDate,
                        IsRequestPending = false,
                        StripeAccount = x.StripeId,
                        StripeStatus = x.StripeStatus,
                        Wallet = x.Wallet,
                        Country = new CountryModel
                        {
                            Id = (x.Country != null) ? x.Country.Id : 0,
                            Name = (x.Country != null) ? x.Country.Name : "",
                            CountryCode = (x.Country != null) ? x.Country.CountryCode : "",
                            IsRestricted = (x.Country != null) ? x.Country.IsRestricted : false,
                            PhoneCode = (x.Country != null) ? x.Country.PhoneCode : ""
                        },
                        Bio = x.Bio
                    })
                    .FirstOrDefaultAsync();
                

                var roles = _context.UserRole.Include(x => x.Role).Where(x => x.UserId == id).Select(x => x.Role.RoleName).ToList();
                userProfile.Roles = roles;
            }

            return new ServiceResponse<ProfileViewModel>(200, "user Profile", userProfile);
        }




        public async Task<ApiResponse> UpdateProfile(ProfileUpdateModel model)
        {
            var currentUser = _currentUser.UserID;
            var userdetails = await _context.User.Where(x => x.Id == currentUser)
                                                 .FirstOrDefaultAsync();

            var userRole = await _context.UserRole.Where(x => x.Id == currentUser)
                                                             .FirstOrDefaultAsync();
            if (userdetails != null)
            {
                userdetails.FirstName = model.FirstName;
                userdetails.LastName = model.LastName;
                userdetails.ContactNumber = model.PhoneNumber;
                userdetails.CountryId = model.CountryId;
                userdetails.DateOfBirth = model.DateOfBirth;
                userdetails.UpdatedDate = DateTime.UtcNow;
                userdetails.Bio = model.Bio;
                userdetails.ProfilePicture = model.ProfilePicture;
                userdetails.BackgroundPicture = model.BackgroundPicture;
                userdetails.Description = model.Description;
                // #pragma warning restore format

                // if (model.ProfilePhoto != null)
                // {
                //     var uploadPic = await _storage.SaveFile(model.ProfilePhoto, model.Extention);
                //     if (userdetails.ProfilePicture != null)
                //     {
                //         var res = await _storage.DeleteLocalFile(userdetails.ProfilePicture);
                //     }
                //     userdetails.ProfilePicture = uploadPic.Data;
                // }
                _context.Update(userdetails);
                await _context.SaveChangesAsync();

            }
            return new ApiResponse("updated successfully", 200);
        }

        public async Task<ApiResponse> UpgradeUserRole(ProfileUpdateModel model)
        {
            var currentUser = _currentUser.UserID;
            var userdetails = await _context.User.Where(x => x.Id == currentUser)
                                                 .FirstOrDefaultAsync();

            var userRole = await _context.UserRole.Where(x => x.Id == currentUser)
                                                             .FirstOrDefaultAsync();
            if (userRole.RoleId == 3 && userdetails.Description != null
            && userdetails.ProfilePicture != null && userdetails.BackgroundPicture != null)
            {
                var userRequest = new UserRequest();
                userRequest.UserId = currentUser;
                userRequest.RequestedRoleId = 2;
                userRequest.RequestType = "seller_upgrade_request";
                userRequest.Status = "pending";
                userRequest.Description = userdetails.FirstName + " " + userdetails.LastName
                + " has requested to upgrade the role from buyer to seller.";
                userRequest.AttachFileUrl = model.AttachFileUrl;
                userRequest.CreatedDate = DateTime.UtcNow;
                userRequest.UpdatedDate = DateTime.UtcNow;
                await _context.AddAsync(userRequest);
                await _context.SaveChangesAsync();
            }
            else
            {
                return new ApiResponse("Some field need to fill", 500);
            }
            return new ApiResponse("Upgrade user role request send to admin", 200);
        }

        public async Task<ApiResponse> ResendVerifyEmail(string email)
        {
            var user = _context.User.Where(x => x.EmailId == email).FirstOrDefault();
            var tok = _context.PasswordResetToken.Where(x => x.Email == email &&
                                                             x.ExpiryDate >= DateTime.UtcNow.AddMinutes(10))
                                                 .FirstOrDefault();
            PasswordResetToken token = new();
            if (tok == null)
            {
                token.Token = Guid.NewGuid().ToString();
                token.Email = user.EmailId;
                token.ExpiryDate = DateTime.UtcNow.AddHours(3);
                await _context.PasswordResetToken.AddAsync(token);
            }
            else
            {
                token = tok;
            }
            await _context.SaveChangesAsync();
            await _emailService.VerifyRegistrationEmail($"{user.FirstName} {user.LastName}",
            user.EmailId, $"{_configuration.GetValue<string>("Domain:React")}/registration-verify-email/{user.EmailId}/{token.Token}");
            return new ApiResponse("Verify Email Resent To " + email, 200);

        }

        public async Task<ServiceResponse<List<BecomeSellerQuestionsModel>>> SellerQuestions()
        {
            var questions = await _context.BecomeSellerQuestionsMasters.Select(x => new BecomeSellerQuestionsModel
            {
                QuestionId = x.Id,
                Question = x.Question
            }).ToListAsync();
            return new ServiceResponse<List<BecomeSellerQuestionsModel>>(StatusCodes.Status200OK, "BecomeSellerQuestions", questions);
        }


        public async Task<ApiResponse> BecomeSeller(BecomeSellerModel model)
        {
            var userId = _currentUser.UserID;
            var user = _context.User.Where(x => x.Id == userId).FirstOrDefault();
            var userCategory = _context.InterestedCategories.Where(x => x.UserId == userId && x.CategoryId != null).ToListAsync();
            var userSubCategory = _context.InterestedCategories.Where(x => x.UserId == userId && x.SubCategoryId != null).ToListAsync();

            //PasswordResetToken token = new();
            //token.Token = Guid.NewGuid().ToString();
            //token.Email = user.EmailId;
            //token.ExpiryDate = DateTime.UtcNow.AddHours(3);

            List<SellerAnswer> sellerAnswers = new();

            foreach (var question in model.BecomeSellerQuestions)
            {
                if (question.QuestionId != 4 && question.QuestionId != 2 && question.QuestionId != 5)
                {
                    SellerAnswer newEntry = new()
                    {
                        UserId = _currentUser.UserID,
                        QuestionId = question.QuestionId,
                        Answer = question.Answer
                    };
                    sellerAnswers.Add(newEntry);
                }
                else if (question.QuestionId == 4)
                {
                    var uploadDoc = await _storage.SaveFile(question.Answer, ".pdf");
                    SellerAnswer newEntry = new()
                    {
                        UserId = _currentUser.UserID,
                        QuestionId = question.QuestionId,
                        Answer = uploadDoc.Data
                    };
                    sellerAnswers.Add(newEntry);
                }
            }

            List<InterestedCategories> userCategories = new();

            if (model.Categories.Count != 0)
            {
                _context.RemoveRange(userCategories);
                foreach (var category in model.Categories)
                {
                    InterestedCategories newCat = new()
                    {
                        UserId = _currentUser.UserID,
                        CategoryId = category
                    };
                    userCategories.Add(newCat);
                }
            }

            List<InterestedCategories> userSubCategories = new();
            if (model.SubCategories.Count != 0)
            {
                _context.RemoveRange(userSubCategories);
                foreach (var subCategory in model.SubCategories)
                {
                    InterestedCategories newSubCat = new()
                    {
                        UserId = _currentUser.UserID,
                        SubCategoryId = subCategory
                    };
                    userSubCategories.Add(newSubCat);
                }
            }

            user.Bio = model.Bio;



            UserRole newRole = new UserRole()
            {
                RoleId = 2,
                UserId = user.Id,
                CreatedDate = DateTime.UtcNow,
                UpdatedDate = DateTime.UtcNow
            };
            await _context.AddAsync(newRole);

            _context.Update(user);
            _context.AddRange(sellerAnswers);
            _context.AddRange(userCategories);
            _context.AddRange(userSubCategories);
            //await _context.PasswordResetToken.AddAsync(token);
            await _context.SaveChangesAsync();
            //await _emailService.VerifySellerEmail($"{user.FirstName} {user.LastName}",
            //user.EmailId, $"{_configuration.GetValue<string>("Domain:Angular")}/seller-verify-email/{user.EmailId}/{token.Token}");
            return new ApiResponse("Seller Registration successfull", 200);
        }

        public async Task<ServiceResponse<NewUserRole>> VerifySellerEmail(string token)
        {
            var prt = await _context.PasswordResetToken.Where(x => x.Token == token).FirstOrDefaultAsync();
            if (prt != null)
            {
                var appuser = await _context.User.Where(x => x.EmailId == prt.Email).FirstOrDefaultAsync();
                if (appuser != null)
                {
                    UserRole newRole = new UserRole()
                    {
                        RoleId = 2,
                        UserId = appuser.Id,
                        CreatedDate = DateTime.UtcNow,
                        UpdatedDate = DateTime.UtcNow
                    };
                    await _context.AddAsync(newRole);
                    _context.Remove(prt);
                    await _context.SaveChangesAsync();
                    var userRoles = _context.UserRole.Include(x => x.Role)
                                                     .Where(x => x.UserId == appuser.Id)
                                                     .Select(x => x.Role.RoleName)
                                                     .ToList();
                    var result = new NewUserRole()
                    {
                        UserRoles = userRoles
                    };
                    return new ServiceResponse<NewUserRole>(200, "Email Verified  Successful", result);
                }
                return new ServiceResponse<NewUserRole>(400, "Wrong reset link", null);
            }
            return new ServiceResponse<NewUserRole>(400, "Wrong reset link", null);
        }

        public async Task<ApiResponse> FollowUser(UserFollowModel model)
        {
            // var save = SaveUserFollow(model);

            var userID = _currentUser.UserID;
            var alreadyExist = await _context.UserFollow.Include(x => x.User)
                .Where(x => x.FollowerId == model.FollowerId && x.FolloweeId == userID && !x.DeleteFlag)
                .Select(x => new UserFollow
                {
                    Id = x.Id,
                    FollowerId = x.FollowerId,
                    FolloweeId = x.FolloweeId,
                    EmailStatus = x.EmailStatus,
                    DeleteFlag = x.DeleteFlag,
                    CreatedDate = x.CreatedDate
                })
                .SingleOrDefaultAsync();

            var followerUser = await _context.User.Where(x => x.Id == model.FollowerId)
                .ToListAsync();

            var followeeUser = await _context.User.Where(x => x.Id == userID )
                .ToListAsync();

            if (alreadyExist == null) {
                UserFollow userFollow = new()
                {
                    FollowerId = model.FollowerId,
                    FolloweeId = userID,
                    EmailStatus = true,
                    DeleteFlag = false,
                    CreatedDate = DateTime.UtcNow
                };

                await _context.AddAsync(userFollow);
                // await _context.SaveChangesAsync();


                followerUser[0].FollowerCount = followerUser[0].FollowerCount + 1;
                _context.Update(followerUser[0]);

                followeeUser[0].FolloweeCount = followeeUser[0].FolloweeCount + 1;
                _context.Update(followeeUser[0]);

                await _context.SaveChangesAsync();

                // return userFollow;
            } else {
                alreadyExist.DeleteFlag = true;

                _context.Update(alreadyExist);
                // await _context.SaveChangesAsync();

                if (followerUser[0].FollowerCount > 0) {
                    followerUser[0].FollowerCount = followerUser[0].FollowerCount - 1;
                    _context.Update(followerUser[0]);
                }

                if (followeeUser[0].FolloweeCount > 0) {
                    followeeUser[0].FolloweeCount = followeeUser[0].FolloweeCount - 1;
                    _context.Update(followeeUser[0]);
                }

                await _context.SaveChangesAsync();
            }

            return new ApiResponse("success", 200);
        }

        // public async Task<UserFollow> SaveUserFollow(UserFollowModel model)
        // {
        //     var userID = _currentUser.UserID;
        //     var alreadyExist = await _context.UserFollow.Include(x => x.User)
        //         .Where(x => x.FollowerId == model.FollowerId && x.FolloweeId == userID && !x.DeleteFlag)
        //         .Select(x => new UserFollow
        //         {
        //             Id = x.Id,
        //             FollowerId = x.FollowerId,
        //             FolloweeId = x.FolloweeId,
        //             EmailStatus = x.EmailStatus,
        //             DeleteFlag = x.DeleteFlag,
        //             CreatedDate = x.CreatedDate,
        //             User = new User
        //             {
        //                 Id = x.User.Id,
        //                 FirstName = x.User.FirstName,
        //                 LastName = x.User.LastName,
        //                 ProfilePicture = x.User.ProfilePicture
        //             }
        //         })
        //         .SingleOrDefaultAsync();

        //     // var followerUser = await _context.User.Where(x => x.Id == model.FollowerId)
        //     //     .Select(x => new User
        //     //     {
        //     //         Id = x.Id,
        //     //         FirstName = x.FirstName,
        //     //         LastName = x.LastName,
        //     //         ProfilePicture = x.ProfilePicture,
        //     //         // FollowerCount = x.FollowerCount,
        //     //         // FolloweeCount = x.FolloweeCount
        //     //     })
        //     //     .ToListAsync();

        //     // var followeeUser = await _context.User.Where(x => x.Id == userID )
        //     //     .Select(x => new User
        //     //     {
        //     //         Id = x.Id,
        //     //         FirstName = x.FirstName,
        //     //         LastName = x.LastName,
        //     //         ProfilePicture = x.ProfilePicture,
        //     //         // FollowerCount = x.FollowerCount,
        //     //         // FolloweeCount = x.FolloweeCount
        //     //     })
        //     //     .ToListAsync();

        //     if (alreadyExist == null) {
        //         UserFollow userFollow = new()
        //         {
        //             FollowerId = model.FollowerId,
        //             FolloweeId = model.FollowerId,
        //             EmailStatus = model.EmailStatus,
        //             // DeleteFlag = false,
        //             CreatedDate = DateTime.UtcNow
        //         };

        //         await _context.AddAsync(userFollow);
        //         // await _context.SaveChangesAsync();


        //         // followerUser[0].FollowerCount = followerUser[0].FollowerCount + 1;
        //         // _context.Update(followerUser);

        //         // followeeUser[0].FolloweeCount = followeeUser[0].FolloweeCount + 1;
        //         // _context.Update(followeeUser);

        //         await _context.SaveChangesAsync();

        //         return userFollow;
        //     } else {
        //         alreadyExist.DeleteFlag = true;

        //         _context.Update(alreadyExist);
        //         // await _context.SaveChangesAsync();

        //         // if (followerUser[0].FollowerCount > 0) {
        //         //     followerUser[0].FollowerCount = followerUser[0].FollowerCount - 1;
        //         //     _context.Update(followerUser);
        //         // }

        //         // if (followeeUser[0].FolloweeCount > 0) {
        //         //     followeeUser[0].FolloweeCount = followeeUser[0].FolloweeCount - 1;
        //         //     _context.Update(followeeUser);
        //         // }

        //         await _context.SaveChangesAsync();

        //         return alreadyExist;
        //     }
        // }

        public async Task<ServiceResponse<FollowUserData>> GetFollow(UserFollowModel model)
        {
            var followData = await _context.UserFollow.Where(x => x.FollowerId == model.FollowerId && x.FolloweeId == _currentUser.UserID && !x.DeleteFlag).Select(x => new UserFollow
                {
                    Id = x.Id,
                    FollowerId = x.FollowerId,
                    FolloweeId = x.FolloweeId,
                    EmailStatus = x.EmailStatus,
                    DeleteFlag = x.DeleteFlag,
                    CreatedDate = x.CreatedDate
                })
                .ToListAsync();

            var followQty = await _context.UserFollow.Include(x => x.User)
                .Where(x => x.FollowerId == model.FollowerId && !x.DeleteFlag)
                .Select(x => new UserFollow
                {
                    Id = x.Id,
                    FollowerId = x.FollowerId,
                    FolloweeId = x.FolloweeId,
                    EmailStatus = x.EmailStatus,
                    DeleteFlag = x.DeleteFlag,
                    CreatedDate = x.CreatedDate,
                    User = new User
                    {
                        Id = x.User.Id,
                        FirstName = x.User.FirstName,
                        LastName = x.User.LastName,
                        ProfilePicture = x.User.ProfilePicture
                    }
                })
                .ToListAsync();

            var followeeQty = await _context.UserFollow.Include(x => x.User)
                .Where(x => x.FolloweeId == model.FollowerId && !x.DeleteFlag)
                .Select(x => new UserFollow
                    {
                        Id = x.Id,
                        FollowerId = x.FollowerId,
                        FolloweeId = x.FolloweeId,
                        EmailStatus = x.EmailStatus,
                        DeleteFlag = x.DeleteFlag,
                        CreatedDate = x.CreatedDate,
                        User = new User
                        {
                            Id = x.User.Id,
                            FirstName = x.User.FirstName,
                            LastName = x.User.LastName,
                            ProfilePicture = x.User.ProfilePicture
                        }
                    })
                    .ToListAsync();

            var followUserData = new FollowUserData
            {
                FollowData = followData,
                FollowQty = followQty,
                FolloweeQty = followeeQty
            };
            // return new ApiResponseWithData("success", 200, res);
            return new ServiceResponse<FollowUserData>(200, "success", followUserData);
        }

        public async Task<ServiceResponse<object>> RecommendedForYou()
        {
            /* start to take wishlist category */
            var wishLists = await _context.WishList.Include(x => x.WishListCategoriesAndSubCategories)
                                                  .ThenInclude(x => x.CategoryMaster)
                                                  .Include(x => x.WishListCategoriesAndSubCategories)
                                                  .ThenInclude(x => x.SubCategoryMaster)
                                                  .Where(x => x.UserId == _currentUser.UserID) // _currentUser.UserID)
                                                  .Select(x => new WistListResponse
                                                  {
                                                      WishListId = x.Id,
                                                      WishListDescription = x.Description,
                                                      WishListCategories = x.WishListCategoriesAndSubCategories.Select(x => new Categories
                                                      {
                                                          CategoryId = x.CategoryId,
                                                          CategoryName = x.CategoryMaster.Name
                                                      }).ToList(),
                                                      WishListSubCategories = x.WishListCategoriesAndSubCategories.Select(x => new SubCategories
                                                      {
                                                          SubCategoryId = x.SubCategoryId,
                                                          SubCategoryName = x.SubCategoryMaster.SubCategoryName
                                                      }).ToList(),
                                                      CreatedDate = x.CreatedDate
                                                  })
                                                  .ToListAsync();

            var categories = new List<long?>();
            var subCategories = new List<long?>();
            var follows = new List<long?>();

            foreach (var wishList in wishLists)
            {
                if(!categories.Contains(wishList.WishListCategories[0].CategoryId))
                {
                    categories.Add(wishList.WishListCategories[0].CategoryId);
                }

                if(!subCategories.Contains(wishList.WishListSubCategories[1].SubCategoryId))
                {
                    subCategories.Add(wishList.WishListSubCategories[1].SubCategoryId);
                }
            }
            /* end to take wishlist category */

            /* start to take wishlist category */
            var followLists = await _context.UserFollow.Where(x => x.FolloweeId == 2 && !x.DeleteFlag).Select(x => new UserFollow
                {
                    Id = x.Id,
                    FollowerId = x.FollowerId,
                    FolloweeId = x.FolloweeId,
                    EmailStatus = x.EmailStatus,
                    DeleteFlag = x.DeleteFlag,
                    CreatedDate = x.CreatedDate
                })
                .ToListAsync();

            foreach (var followList in followLists)
            {
                follows.Add(followList.FollowerId);
            }

            var transactionLogs = await _context.TransactionLog.Include(x => x.User)
                .Where(x => x.UserId == _currentUser.UserID)
                .Select(x => new TransactionLog
                {
                    Id = x.Id,
                    SellerName = x.SellerName,
                    BuyerName = x.User.FirstName + " " + x.User.LastName,
                    Amount = x.Amount,
                    ProductId = x.ProductId,
                    Destination = x.Destination,
                    DueDate = x.DueDate,
                    CreatedDate = x.CreatedDate,
                    OrderId = x.OrderId,
                    IsPaid = x.IsPaid,
                    OrderStatus = x.OrderStatus,
                    NetAmount = x.NetAmount
                })
                .OrderByDescending(x => x.Id)
                .Take(1)
                .ToListAsync();

            List<Product> ProductInfo;
            ProductInfo = await _context.Product.Include(x => x.ProductCategories).ThenInclude(x => x.CategoryMaster)
                                                .Include(x => x.ProductSubCategories).ThenInclude(x => x.SubCategoryMaster)
                                                .Where(x => x.Id == transactionLogs[0].ProductId)
                                                .ToListAsync();

            foreach (var product in ProductInfo)
            {
                foreach (var productCategories in product.ProductCategories)
                {
                    if(!categories.Contains(productCategories.CategoryMaster.Id))
                    {
                        categories.Add(productCategories.CategoryMaster.Id);
                    }
                }
                foreach (var productSubCategories in product.ProductSubCategories) {
                    if(!subCategories.Contains(productSubCategories.SubCategoryMaster.Id))
                    {
                        subCategories.Add(productSubCategories.SubCategoryMaster.Id);
                    }
                }
            }

            List<Product> products = await _context.Product.Include(x => x.Seller)
                                .Include(x => x.ProductCategories).ThenInclude(x => x.CategoryMaster)
                                .Include(x => x.ProductSubCategories).ThenInclude(x => x.SubCategoryMaster)
                                .Where(x => x.IsDeleted == false && x.ForSale == true && x.Price > 0)
                                    // categories.Any(c => x.ProductCategories[0].CategoryMaster.Id.Contains(c)) &&
                                    // subCategories.Any(s => x.ProductSubCategories[0].SubCategoryMaster.Id.Contains(s)) &&
                                    // follows.Any(f => x.SellerId == f)
                                .ToListAsync();


            var newProducts = products.Select(x => new ViewProductModel
            {
                ProductId = x.Id,
                ProductName = x.ProductTitle,
                Category = x.ProductCategories.Select(x => new ViewCategory
                {
                    CategoryName = x.CategoryMaster.Name,
                    CategoryId = x.CategoryMaster.Id
                }).ToList(),
                SubCategory = x.ProductSubCategories.Select(x => new ViewSubCategory
                {
                    SubCategoryName = x.SubCategoryMaster.SubCategoryName,
                    SubCategoryId = x.SubCategoryMaster.Id
                }).ToList(),
                Tags = x.ProductTags.Select(x => new ViewTags
                {
                    TagName = x.TagsMaster.TagName,
                    TagId = x.TagsMaster.Id
                }).ToList(),
                Price = x.Price,
                GasFee = x.GasFee,
                ProductPicture = x.ProductImage,
                SubProductPictures = _context.ProductImages
                                              .Where(y => y.ProductId == x.Id)
                                              .Select(y => new SubProductPicturesModel
                                              {
                                                  Id = y.Id,
                                                  ProductPicture = y.ProductImage
                                              }).ToList(),
                Extention = _storage.GetBase64(x.ProductImage).Extention,
                Description = x.Description,
                CreatedDate = x.UpdatedDate,
                Seller = x.Seller.FirstName + " " + x.Seller.LastName,
                SellerEamil = x.Seller.EmailId,
                SellerId = x.Seller.Id,
                Link = x.Link,
                Quantity = x.Quantity
            }).ToList();



            // var byCategory = new ViewProductModel();
            List<ViewProductModel> byCategory = new List<ViewProductModel>();
            if (categories.Count != 0)
            {
                List<ViewProductModel> result = new List<ViewProductModel>();
                if (categories.Count == 1 && categories[0] == 0)
                {

                }
                else
                {
                    foreach (var cat in categories)
                    {
                        var catfilter = newProducts.Where(x => x.Category.Select(x => x.CategoryId).ToList().Contains((long)cat)).Take(10).ToList();
                        result.AddRange(catfilter);
                    }
                    byCategory = result;
                }
            }

            // var bySubCategory = new List<object>();
            List<ViewProductModel> bySubCategory = new List<ViewProductModel>();
            if (subCategories.Count != 0)
            {
                List<ViewProductModel> result = new List<ViewProductModel>();
                if (subCategories.Count == 1 && subCategories[0] == 0)
                {

                }
                else
                {
                    foreach (var subcat in subCategories)
                    {
                        var subcatfilter = newProducts.Where(x => x.SubCategory.Select(x => x.SubCategoryId).ToList().Contains((long)subcat))
                                                      .Take(10).ToList();
                        result.AddRange(subcatfilter);
                    }
                    bySubCategory = result;
                }
            }

            // var byFollower = new List<object>();
            List<ViewProductModel> byFollower = new List<ViewProductModel>();
            if (follows.Count != 0)
            {
                List<ViewProductModel> result = new List<ViewProductModel>();
                if (follows.Count == 1 && follows[0] == 0)
                {

                }
                else
                {
                    foreach (var follow in follows)
                    {
                        // var followfilter = newProducts.Where(x => x.SellerId.Contains(follow))
                        var followfilter = newProducts.Where(x => x.SellerId == (long)follow)
                                                      .Take(10).ToList();
                        result.AddRange(followfilter);
                    }
                    byFollower = result;
                }
            }

            List<ViewProductModel> finalProducts = new List<ViewProductModel>();
            finalProducts.AddRange(byCategory.Except(finalProducts));
            finalProducts.AddRange(bySubCategory.Except(finalProducts));
            finalProducts.AddRange(byFollower.Except(finalProducts));

            if (finalProducts.Count < 4) {
                finalProducts.AddRange(newProducts.Except(finalProducts));
            }

            var recommendedForYouData = new RecommendedForYouData
            {
                Categories = categories,
                SubCategories = subCategories,
                Follows = follows,
                Products = finalProducts.Take(4),
                AllProducts = newProducts
            };

            // return new ApiResponseWithData("success", 200, res);
            return new ServiceResponse<object>(200, "success", recommendedForYouData);
        }

        public async Task<ServiceResponse<object>> GetUsers(UserList userList)
        {
            var users = await _context.User.Select(x => new User
                                            {
                                                Id = x.Id,
                                                FirstName = x.FirstName,
                                                LastName = x.LastName,
                                                ProfilePicture = x.ProfilePicture
                                            })
                                            .ToListAsync();
            if (userList.UserIds.Count != 0)
            {
                List<User> result = new List<User>();
                if (userList.UserIds.Count == 1 && userList.UserIds[0] == 0)
                {

                }
                else
                {
                    foreach (var user in userList.UserIds)
                    {
                        // var followfilter = newProducts.Where(x => x.SellerId.Contains(follow))
                        var followerList = users.Where(x => x.Id == (long)user)
                                                      .ToList();
                        result.AddRange(followerList);
                    }
                    users = result;
                }
            }

            return new ServiceResponse<object>(200, "success", users);
        }
    }
}
