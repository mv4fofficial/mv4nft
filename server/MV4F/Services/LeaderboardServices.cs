﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using MV4F.Entities;
using MV4F.IRepository;
using MV4F.IServices;
using MV4F.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MV4F.Services
{
    public class LeaderboardServices : ILeaderboardServices
    {
        private readonly IConfiguration _configuration;
        private readonly ICurrentUser _currentUser;
        private readonly IStorage _storage;
        private readonly MV4FContext _context;

        public LeaderboardServices(IConfiguration configuration,
                            ICurrentUser currentUser,
                            IStorage storage,
                            MV4FContext context)
        {
            _configuration = configuration;
            _currentUser = currentUser;
            _storage = storage;
            _context = context;
        }

        public async Task<ServiceResponse<List<PlayerScoreViewModel>>> ViewLeaderboard()
        {
            List<PlayerScoreViewModel> playerScores;

            playerScores = await _context.PlayerScore
            .Include(x => x.User)
            .Select(x => new PlayerScoreViewModel
            {
                Id = x.User.Id,
                UserName = x.User.FirstName + x.User.LastName,
                Score = x.Score
            }).ToListAsync();

            return new ServiceResponse<List<PlayerScoreViewModel>>(200, "view leaderboard", playerScores);
        }

        public async Task<ApiResponse> AddPlayerScore(PlayerScoreAddModel model)
        {
            List<UserGameAssets> userGameAssetList = await _context.UserGameAssets
             .Where(x => x.UserId == _currentUser.UserID)
             .ToListAsync();

            int score = 0;

            for (int i = 0; i < userGameAssetList.Count; i++)
            {
                if (userGameAssetList[i].Progress == 100)
                {
                    score++;
                }
            }

            PlayerScore newPlayerScore = new PlayerScore()
            {
                UserId = _currentUser.UserID,
                Score = score,
                CreatedDate = DateTime.Now,
                UpdatedDate = DateTime.Now
            };

            PlayerScore playerScore = await _context.PlayerScore
             .Where(x => x.UserId == _currentUser.UserID)
             .FirstOrDefaultAsync();

            if (playerScore == null)
            {
                playerScore = newPlayerScore;
                await _context.AddAsync(playerScore);
            } else
            {
                playerScore.Score = score;
            }

            await _context.SaveChangesAsync();

            return new ApiResponse("save player score Successfully", 200);
        }

    }
}


