﻿using Microsoft.AspNetCore.Http;
using MV4F.IRepository;
using System;
using System.Security.Claims;

namespace MV4F.Services
{
    public class CurrentUser : ICurrentUser
    {
        private readonly IHttpContextAccessor _httpContext;
        public CurrentUser(IHttpContextAccessor httpContext)
        {
            _httpContext = httpContext;
        }

        public long UserID => long.Parse(_httpContext.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value);

        public string Email => _httpContext.HttpContext.User.FindFirst(ClaimTypes.Email).Value;

        public string Name => _httpContext.HttpContext.User.FindFirst(ClaimTypes.Name).Value;

        public int GroupID => Convert.ToInt32(_httpContext.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value);
    }
}
