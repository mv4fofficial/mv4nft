﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using MV4F.Entities;
using MV4F.IRepository;
using MV4F.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace MV4F.Repository
{
    public class PuzzleDetailServices : IPuzzleDetailServices
    {
        private readonly IConfiguration _configuration;
        private readonly IEmailService _emailService;
        private readonly IStorage _storage;
        private readonly ICurrentUser _currentUser;
        private readonly MV4FContext _context;

        public PuzzleDetailServices(IConfiguration configuration,
                                IEmailService emailService,
                                IStorage storage,
                                ICurrentUser currentUser,
                                MV4FContext context)
        {
            _configuration = configuration;
            _emailService = emailService;
            _storage = storage;
            _currentUser = currentUser;
            _context = context;
        }

        public async Task<ServiceResponse<List<PuzzleDetailViewModel>>> ViewPuzzleDetails()
        {
            List<PuzzleDetailViewModel> PuzzleDetail;
           
            PuzzleDetail = await _context.PuzzleDetail.Select(x => new PuzzleDetailViewModel
                                                        {
                                                            Id = x.Id,
                                                            RemainingHints = x.RemainingHints,
                                                            Timer = x.Timer,
                                                            ElapsedTime = x.ElapsedTime,
                                                            CreatedDate = x.CreatedDate,
                                                            UpdatedDate = x.UpdatedDate,
                                                        }).ToListAsync();


            return new ServiceResponse<List<PuzzleDetailViewModel>>(200, "puzzle details", PuzzleDetail);
        }

        public async Task<ServiceResponse<PuzzleDetailViewModel>> ViewPuzzleDetailById(long id)
        {
            var PuzzleDetail = await _context.PuzzleDetail.Where(x => x.Id == id)
                                                        .Select(x => new PuzzleDetailViewModel
                                                        {
                                                            Id = x.Id,
                                                            RemainingHints = x.RemainingHints,
                                                            Timer = x.Timer,
                                                            ElapsedTime = x.ElapsedTime,
                                                            CreatedDate = x.CreatedDate,
                                                            UpdatedDate = x.UpdatedDate,
                                                        }).FirstOrDefaultAsync();
            return new ServiceResponse<PuzzleDetailViewModel>(200, "puzzle detail by id", PuzzleDetail);
        }

        public async Task<ApiResponse> AddPuzzleDetail(AddPuzzleDetailModel model)
        {
            PuzzleDetail newPuzzleDetail = new()
            {
                RemainingHints = model.RemainingHints, 
                Timer = model.Timer,
                ElapsedTime = model.ElapsedTime, 
                CreatedDate = DateTime.Now,
                UpdatedDate = DateTime.Now,
            };

            await _context.AddAsync(newPuzzleDetail);
            await _context.SaveChangesAsync();

            return new ApiResponse("added Successfully", 200);

        }

    }
}
