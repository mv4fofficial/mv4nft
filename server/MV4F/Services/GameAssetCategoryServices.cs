﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using MV4F.Entities;
using MV4F.IRepository;
using MV4F.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace MV4F.Repository
{
    public class GameAssetCategoryServices : IGameAssetCategoryServices
    {
        private readonly IConfiguration _configuration;
        private readonly IEmailService _emailService;
        private readonly IStorage _storage;
        private readonly ICurrentUser _currentUser;
        private readonly MV4FContext _context;

        public GameAssetCategoryServices(IConfiguration configuration,
                                IStorage storage,
                                ICurrentUser currentUser,
                                MV4FContext context)
        {
            _configuration = configuration;
            _storage = storage;
            _currentUser = currentUser;
            _context = context;
        }

        public async Task<ServiceResponse<List<GameAssetCategoryViewModel>>> ViewGameAssetCategories()
        {
            List<GameAssetCategoryViewModel> puzzleCategoryViewModels = new List<GameAssetCategoryViewModel>();

            puzzleCategoryViewModels = await _context.GameAssetCategory.Select(x => new GameAssetCategoryViewModel
            {
                Id = x.Id,
                CategoryId = x.CategoryId,
                GameAssetId = x.GameAssetId,
                CreatedDate = x.CreatedDate,
                UpdatedDate = x.UpdatedDate
            }).ToListAsync();

            return new ServiceResponse<List<GameAssetCategoryViewModel>>(200, "game asset categories", puzzleCategoryViewModels);
        }

        public async Task<ServiceResponse<GameAssetCategoryViewModel>> ViewGameAssetsByCategoryId(long id)
        {
            GameAssetCategoryViewModel puzzleCategory = await _context.GameAssetCategory.Where(x => x.Id == id)
                                                        .Select(x => new GameAssetCategoryViewModel
                                                        {
                                                            Id = x.Id,
                                                            CategoryId = x.CategoryId,
                                                            GameAssetId = x.GameAssetId,
                                                            CreatedDate = x.CreatedDate,
                                                            UpdatedDate = x.UpdatedDate
                                                        }).FirstOrDefaultAsync();
            return new ServiceResponse<GameAssetCategoryViewModel>(200, "game asset category by id", puzzleCategory);
        }

        public async Task<ApiResponse> AddGameAssetCategory(GameAssetCategoryAddModel model)
        {
            GameAssetCategories newGameAssetCategory = new()
            {
                Id = model.Id,
                CategoryId = model.CategoryId,
                GameAssetId = model.GameAssetId,
                CreatedDate = DateTime.Now,
                UpdatedDate = DateTime.Now,
            };

            await _context.AddAsync(newGameAssetCategory);
            await _context.SaveChangesAsync();

            return new ApiResponse("added game asset category successfully", 200);
        }
    }
}
