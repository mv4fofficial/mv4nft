﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using MV4F.Entities;
using MV4F.IRepository;
using MV4F.IServices;
using MV4F.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MV4F.Services
{
    public class UserServices : IUserServices
    {
        private readonly ICurrentUser _currentUser;
        private readonly MV4FContext _context;
        private readonly IStorage _storage;

        public UserServices(ICurrentUser currentUser, MV4FContext context, IStorage storage)
        {
            _currentUser = currentUser;
            _context = context;
            _storage = storage;
        }

        public async Task<ApiResponse> AddToCart(long productId)
        {
            var CurrentUser = _currentUser;
            var CartItems = await _context.UserCart.Where(x => x.UserId == CurrentUser.UserID && x.ProductId == productId)
                                            .ToListAsync();

            if (CartItems.Count != 0)
                return new ApiResponse("Item Already in cart", 400);
            UserCart item = new()
            {
                UserId = CurrentUser.UserID,
                ProductId = productId,
                CreatedDate = DateTime.UtcNow,
                UpdatedDate = DateTime.UtcNow
            };
            await _context.AddAsync(item);
            await _context.SaveChangesAsync();
            return new ApiResponse("item added to cart Successfully", 200);
        }

        public async Task<ApiResponse> RemoveFromCart(long cartId)
        {
            var CurrentUser = _currentUser;
            var CartItem = await _context.UserCart.Where(x => x.UserId == CurrentUser.UserID && x.Id == cartId)
                                            .FirstOrDefaultAsync();
            var OrderedProducts = await _context.UserCart.Where(x => x.OrderStatus == "succeeded" &&
                                                         x.UserId == CurrentUser.UserID)
                                                         .ToListAsync();

            if (CartItem == null)
                return new ApiResponse("Invalid cart id", 400);
            if (OrderedProducts.Count >= 1)
            {
                _context.RemoveRange(OrderedProducts);
            }
            _context.Remove(CartItem);
            await _context.SaveChangesAsync();
            return new ApiResponse("Removed Successfully", 200);
        }

        public async Task<ServiceResponse<List<ViewCartModel>>> ViewCart()
        {
            var CurrentUser = _currentUser;
            var CartItems = await _context.UserCart.Include(x => x.Product)
                                                   .ThenInclude(x => x.ProductCategories)
                                                   .ThenInclude(x => x.CategoryMaster)
                                                   .Include(x => x.Product)
                                                   .ThenInclude(x => x.ProductSubCategories)
                                                   .ThenInclude(x => x.SubCategoryMaster)
                                                   .Include(x => x.Product)
                                                   .ThenInclude(x => x.ProductTags)
                                                   .ThenInclude(x => x.TagsMaster)
                                                   .Where(x => x.UserId == CurrentUser.UserID &&
                                                         x.OrderStatus != "succeeded")
                                                 .Select(x => new ViewCartModel
                                                 {
                                                     Id = x.Id,
                                                     ProductName = x.Product.ProductTitle,
                                                     ProductImage = x.Product.ProductImage,
                                                     Extention = _storage.GetBase64(x.Product.ProductImage).Extention,
                                                     ProductPrice = x.Product.Price,
                                                     GasFee = x.Product.GasFee,
                                                     Categories = x.Product.ProductCategories.Select(x => x.CategoryMaster.Name)
                                                                                             .ToList(),
                                                     SubCategories = x.Product.ProductSubCategories
                                                                      .Select(x => x.SubCategoryMaster.SubCategoryName)
                                                                      .ToList(),
                                                     Tags = x.Product.ProductTags.Select(x => x.TagsMaster.TagName).ToList()
                                                 })
                                                 .ToListAsync();
            if (CartItems == null)
                return new ServiceResponse<List<ViewCartModel>>(403, "no data found", null);
            return new ServiceResponse<List<ViewCartModel>>(200, "all cart items", CartItems);
        }


        public async Task<ServiceResponse<List<UserOrderModel>>> ViewUserOrders()
        {
            var CurrentUser = _currentUser;
            var transactonLog = await _context.TransactionLog.Include(x => x.Product).ToListAsync();
            var UserOrders = _context.UserOrders.Where(x => x.UserId == CurrentUser.UserID)
                                                .Select(x => new UserOrderModel
                                                {
                                                    Amount = x.Amount,
                                                    OrderDate = x.CreatedDate,
                                                    OrderStatus = x.OrderStatus,
                                                    OrderId = x.Id
                                                })
                                                .ToList();
            foreach (var order in UserOrders)
            {
                order.ProductName = transactonLog.Where(x => x.OrderId == order.OrderId)
                                                 .Select(x => x.Product.ProductTitle)
                                                 .ToList();
            }
            return new ServiceResponse<List<UserOrderModel>>(StatusCodes.Status200OK, "all user orders", UserOrders);
        }

        public async Task<ServiceResponse<List<ViewUserProductModel>>> ViewUserProcuts(GetUserProductsFilter model)
        {
            var CurrentUser = _currentUser;
            var UserProducts = await _context.UserProduct.Include(x => x.Product)
                                                    .ThenInclude(x => x.Seller)
                                                    .Include(x => x.Product)
                                                    .ThenInclude(x => x.ProductCategories)
                                                    .ThenInclude(x => x.CategoryMaster)
                                                    .Include(x => x.Product)
                                                    .ThenInclude(x => x.ProductSubCategories)
                                                    .ThenInclude(x => x.SubCategoryMaster)
                                                    .Include(x => x.Product)
                                                    .ThenInclude(x => x.ProductTags)
                                                    .ThenInclude(x => x.TagsMaster)
                                                    .Where(x => x.UserId == CurrentUser.UserID)
                                                    .Select(x => new ViewUserProductModel
                                                    {
                                                        ProductId = x.Id,
                                                        ProductName = x.Product.ProductTitle,
                                                        Price = x.Product.Price,
                                                        Category = x.Product.ProductCategories.Select(x => new ViewCategory
                                                        {
                                                            CategoryName = x.CategoryMaster.Name,
                                                            CategoryId = x.CategoryMaster.Id
                                                        }).ToList(),
                                                        SubCategory = x.Product.ProductSubCategories.Select(x => new ViewSubCategory
                                                        {
                                                            SubCategoryName = x.SubCategoryMaster.SubCategoryName,
                                                            SubCategoryId = x.SubCategoryMaster.Id
                                                        }).ToList(),
                                                        Tags = x.Product.ProductTags.Select(x => new ViewTags
                                                        {
                                                            TagName = x.TagsMaster.TagName,
                                                            TagId = x.TagsMaster.Id
                                                        }).ToList(),
                                                        ProductPrice = x.Product.Price,
                                                        ProductPicture = x.Product.ProductImage,
                                                        Extention = _storage.GetBase64(x.Product.ProductImage).Extention,
                                                        Description = x.Product.Description,
                                                        SellerId = x.Product.SellerId,
                                                        Seller = $"{x.Product.Seller.FirstName} {x.Product.Seller.LastName}"
                                                    })
                                                    .ToListAsync();
            if (model.CategoryId.Count > 1 || (model.CategoryId.Count == 1 && !model.CategoryId.Contains(0)))
            {
                UserProducts = UserProducts.Where(x => model.CategoryId.Contains(x.Category.Select(x => x.CategoryId).FirstOrDefault()))
                                                    .ToList();
            }

            if (model.SubCategoryId.Count > 1 || (model.SubCategoryId.Count == 1 && !model.SubCategoryId.Contains(0)))
            {
                UserProducts = UserProducts.Where(x => model.SubCategoryId.Contains(x.SubCategory.Select(x => x.SubCategoryId).FirstOrDefault())).ToList();
            }
            if (UserProducts == null)
                return new ServiceResponse<List<ViewUserProductModel>>(StatusCodes.Status400BadRequest, "no products found", null);
            return new ServiceResponse<List<ViewUserProductModel>>(StatusCodes.Status200OK, "all user products", UserProducts);
        }

        public async Task<ApiResponse> AddAddress(AddAddressModel model)
        {
            Addresses newAddress = new Addresses
            {
                FirstName = model.FirstName,
                LastName = model.LastName,
                Address1 = model.Address1,
                Address2 = model.Address2,
                City = model.City,
                Pincode = model.Pincode,
                UserId = _currentUser.UserID,
                CreatedDate = DateTime.UtcNow,
                UpdatedDate = DateTime.UtcNow,
                Default = model.IsDefault
            };

            await _context.AddAsync(newAddress);
            await _context.SaveChangesAsync();

            return new ApiResponse("added successfully", StatusCodes.Status200OK);
        }

        public async Task<ServiceResponse<GetAddressModel>> GetDefaultAddress()
        {
            var address = await _context.Addresses.Where(x => x.UserId == _currentUser.UserID &&
                                                              x.Default == true)
                                                  .Select(x => new GetAddressModel
                                                  {
                                                      FirstName = x.FirstName,
                                                      LastName = x.LastName,
                                                      Address1 = x.Address1,
                                                      Address2 = x.Address2,
                                                      AddressId = x.Id,
                                                      City = x.City,
                                                      Pincode = x.Pincode
                                                  })
                                                  .FirstOrDefaultAsync();
            if (address == null)
                return new ServiceResponse<GetAddressModel>(StatusCodes.Status404NotFound, "no default address found", null);

            return new ServiceResponse<GetAddressModel>(StatusCodes.Status200OK, "Default Address", address);
        }

        public async Task<ApiResponse> AddProductWishList(AddProductWishListModel model)
        {
            var newItem = new ProductWishList()
            {
                CreatedDate = DateTime.UtcNow,
                UpdatedDate = DateTime.UtcNow,
                UserId = _currentUser.UserID,
                ProductId = model.ProductId,
            };
            await _context.AddAsync(newItem);
            await _context.SaveChangesAsync();

            return new ApiResponse("productwishlist added successfully", StatusCodes.Status200OK);
        }

        public async Task<ApiResponse> DeleteProductWishList(DeleteProductWishListModel model)
        {
            var CurrentUser = _currentUser;
            var getWishListDetails = await _context.ProductWishList
                                                    .Where(x => x.ProductId == model.ProductId && x.UserId == CurrentUser.UserID)
                                                    .FirstOrDefaultAsync();

            if (getWishListDetails != null)
            {
                _context.Remove(getWishListDetails);
                await _context.SaveChangesAsync();
                return new ApiResponse("Successfully removed", 200);
            }
            else
                return new ApiResponse("Please pass a valid id to remove", 400);
        }

        public async Task<ServiceResponse<List<ViewProductWishListModel>>> ViewProductWishLists()
        {
            var CurrentUser = _currentUser;
            var WishListProducts = await _context.ProductWishList
                                                    .Include(x => x.Product)
                                                    .ThenInclude(x => x.ProductCategories)
                                                    .ThenInclude(x => x.CategoryMaster)
                                                    .Include(x => x.Product)
                                                    .ThenInclude(x => x.ProductSubCategories)
                                                    .ThenInclude(x => x.SubCategoryMaster)
                                                    .Include(x => x.Product)
                                                    .ThenInclude(x => x.ProductTags)
                                                    .ThenInclude(x => x.TagsMaster)
                                                    .Where(x => x.UserId == CurrentUser.UserID)
                                                    .Select(x => new ViewProductWishListModel
                                                    {
                                                        UserId = x.UserId,
                                                        Product = new ViewProductModel
                                                        {
                                                            ProductId = x.Product.Id,
                                                            ProductName = x.Product.ProductTitle,
                                                            Price = x.Product.Price,
                                                            Category = x.Product.ProductCategories.Select(x => new ViewCategory
                                                            {
                                                                CategoryName = x.CategoryMaster.Name,
                                                                CategoryId = x.CategoryMaster.Id
                                                            }).ToList(),
                                                            SubCategory = x.Product.ProductSubCategories.Select(x => new ViewSubCategory
                                                            {
                                                                SubCategoryName = x.SubCategoryMaster.SubCategoryName,
                                                                SubCategoryId = x.SubCategoryMaster.Id
                                                            }).ToList(),
                                                            Tags = x.Product.ProductTags.Select(x => new ViewTags
                                                            {
                                                                TagName = x.TagsMaster.TagName,
                                                                TagId = x.TagsMaster.Id
                                                            }).ToList(),
                                                            ProductPicture = x.Product.ProductImage,
                                                            Extention = _storage.GetBase64(x.Product.ProductImage).Extention,
                                                            Description = x.Product.Description,
                                                            SellerId = x.Product.SellerId,
                                                            Seller = $"{x.Product.Seller.FirstName} {x.Product.Seller.LastName}"
                                                        },
                                                    })
                                                    .ToListAsync();

            return new ServiceResponse<List<ViewProductWishListModel>>(StatusCodes.Status200OK, "all user wishlist products", WishListProducts);
        }

        public async Task<ApiResponse> AddWishListItem(AddWishListModel model)
        {
            var newItem = new WishList()
            {
                CreatedDate = DateTime.UtcNow,
                Description = model.Description,
                UpdatedDate = DateTime.UtcNow,
                UserId = _currentUser.UserID,
            };
            await _context.AddAsync(newItem);
            await _context.SaveChangesAsync();

            List<WishListCategoriesAndSubCategories> newWishlist = new();
            foreach (var cat in model.Categories)
            {
                WishListCategoriesAndSubCategories category = new()
                {
                    CategoryId = cat,
                    UserId = _currentUser.UserID,
                    CreatedDate = DateTime.UtcNow,
                    WishListId = newItem.Id,
                };
                newWishlist.Add(category);
            }
            foreach (var subCategory in model.SubCategories)
            {
                WishListCategoriesAndSubCategories Subcategory = new()
                {
                    SubCategoryId = subCategory,
                    UserId = _currentUser.UserID,
                    CreatedDate = DateTime.UtcNow,
                    WishListId = newItem.Id,
                };
                newWishlist.Add(Subcategory);
            }
            _context.AddRange(newWishlist);
            await _context.SaveChangesAsync();

            var relatedSellers = await _context.InterestedCategories.Include(x => x.User)
                                                                    .ThenInclude(x => x.UserRole)
                                                                    .Where(x => x.User.UserRole.Select(x => x.RoleId).Contains(2))
                                                                    .Select(x => x.UserId)
                                                                    .Distinct()
                                                                    .ToListAsync();
            var interstedCtegories = await _context.InterestedCategories.ToListAsync();
            List<Notifications> notifications = new();

            foreach (var user in relatedSellers)
            {
                var category = interstedCtegories.Where(x => x.UserId == user && x.CategoryId != null).Select(x => x.CategoryId).ToList();
                var subcategory = interstedCtegories.Where(x => x.UserId == user && x.SubCategoryId != null).Select(x => x.SubCategoryId).ToList();
                if (model.Categories.All(category.Contains) && model.SubCategories.All(subcategory.Contains))
                {
                    Notifications newNotification = new()
                    {
                        UserId = _currentUser.UserID,
                        SellerId = user,
                        WishListId = newItem.Id,
                        IsRead = false,
                        IsSeller = false,
                        CreatedDate = DateTime.UtcNow,
                        UpdatedDate = DateTime.UtcNow
                    };
                    notifications.Add(newNotification);
                }
            }
            _context.AddRange(notifications);
            await _context.SaveChangesAsync();

            return new ApiResponse("wishlist added successfully", StatusCodes.Status200OK);
        }


        public async Task<ApiResponse> DeleteWishListItem(int wishListId)
        {
            var getWishListDetails = await _context.WishList.Where(x => x.Id == wishListId).FirstOrDefaultAsync();

            if (getWishListDetails != null)
            {
                _context.Remove(getWishListDetails);
                await _context.SaveChangesAsync();
                return new ApiResponse("Successfully removed", 200);
            }
            else
                return new ApiResponse("Please pass a valid id to remove", 400);
        }

        public async Task<ApiResponse> UpdateReadNotification(int notificationId)
        {
            var result = await _context.Notifications.Where(x => x.Id == notificationId).FirstOrDefaultAsync();
            result.IsRead = true;
            result.UpdatedDate = DateTime.UtcNow;

            _context.Update(result);
            await _context.SaveChangesAsync();

            return new ApiResponse("Successfully Updated", 200);
        }

        public async Task<ServiceResponse<List<GetBuyerNoitificationModel>>> GetAllBuyerNoitification()
        {
            var CurrentUser = _currentUser;
            var UserNotification = await _context.Notifications.Include(x => x.WishList)
                                                                .ThenInclude(x => x.WishListCategoriesAndSubCategories)
                                                                .Where(x => x.IsRead == false &&
                                                                            x.UserId == _currentUser.UserID)
                                                                            //  && x.IsSeller == false)
                                                                .Select(x => new GetBuyerNoitificationModel
                                                                {
                                                                    Id = x.Id,
                                                                    Description = x.WishList.Description,
                                                                    Category = x.WishList.WishListCategoriesAndSubCategories
                                                                                .Select(x => new Category
                                                                                {
                                                                                    Id = x.Id,
                                                                                    CategoryName = x.CategoryMaster.Name

                                                                                }).ToList(),
                                                                    SubCategory = x.WishList.WishListCategoriesAndSubCategories
                                                                                .Select(x => new SubCategory
                                                                                {
                                                                                    Id = x.Id,
                                                                                    SubCategoryName = x.SubCategoryMaster.SubCategoryName
                                                                                }).ToList()
                                                                })
                                                                .ToListAsync();
            if (UserNotification == null)
                return new ServiceResponse<List<GetBuyerNoitificationModel>>(StatusCodes.Status400BadRequest,
                                                                             "no notification found", null);
            return new ServiceResponse<List<GetBuyerNoitificationModel>>(StatusCodes.Status200OK,
                                                                         "all user notification", UserNotification);
        }


        public async Task<ApiResponse> UpdateWishList(UpdateWishListModel model)
        {
            var wishlist = await _context.WishList.Where(x => x.Id == model.WishListId).FirstOrDefaultAsync();
            if (wishlist != null)
            {
                var wishlistCategories = await _context.WishListCategoriesAndSubCategories
                                                 .Where(x => x.WishListId == model.WishListId)
                                                 .ToListAsync();
                _context.RemoveRange(wishlistCategories);
                wishlist.Description = model.Description;
                wishlist.CreatedDate = DateTime.UtcNow;
                _context.Update(wishlist);

                List<WishListCategoriesAndSubCategories> newWishlist = new();
                foreach (var cat in model.Categories)
                {
                    WishListCategoriesAndSubCategories category = new()
                    {
                        CategoryId = cat,
                        UserId = _currentUser.UserID,
                        CreatedDate = DateTime.UtcNow,
                        WishListId = wishlist.Id,
                    };
                    newWishlist.Add(category);
                }
                foreach (var subCategory in model.SubCategories)
                {
                    WishListCategoriesAndSubCategories Subcategory = new()
                    {
                        SubCategoryId = subCategory,
                        UserId = _currentUser.UserID,
                        CreatedDate = DateTime.UtcNow,
                        WishListId = wishlist.Id,
                    };
                    newWishlist.Add(Subcategory);
                }
                _context.AddRange(newWishlist);
                await _context.SaveChangesAsync();

                var relatedSellers = await _context.InterestedCategories.Include(x => x.User)
                                                                        .ThenInclude(x => x.UserRole)
                                                                        .Where(x => x.User.UserRole.Select(x => x.RoleId).Contains(2))
                                                                        .Select(x => x.UserId)
                                                                        .Distinct()
                                                                        .ToListAsync();
                var interstedCtegories = await _context.InterestedCategories.ToListAsync();
                List<Notifications> notifications = new();

                foreach (var user in relatedSellers)
                {
                    var category = interstedCtegories.Where(x => x.UserId == user && x.CategoryId != null).Select(x => x.CategoryId).ToList();
                    var subcategory = interstedCtegories.Where(x => x.UserId == user && x.SubCategoryId != null).Select(x => x.SubCategoryId).ToList();
                    if (model.Categories.All(category.Contains) && model.SubCategories.All(subcategory.Contains))
                    {
                        Notifications newNotification = new()
                        {
                            UserId = _currentUser.UserID,
                            SellerId = user,
                            WishListId = wishlist.Id,
                            IsRead = false,
                            IsSeller = false,
                            CreatedDate = DateTime.UtcNow,
                            UpdatedDate = DateTime.UtcNow
                        };
                        notifications.Add(newNotification);
                    }
                }
                _context.AddRange(notifications);
                await _context.SaveChangesAsync();

                return new ApiResponse("updated Successfully", 200);
            }
            else
            {
                return new ApiResponse("please provide a Proper Wishlist Id", StatusCodes.Status400BadRequest);
            }
        }


        public async Task<ServiceResponse<List<WistListResponse>>> UserWishList()
        {
            var wishList = await _context.WishList.Include(x => x.WishListCategoriesAndSubCategories)
                                                  .ThenInclude(x => x.CategoryMaster)
                                                  .Include(x => x.WishListCategoriesAndSubCategories)
                                                  .ThenInclude(x => x.SubCategoryMaster)
                                                  .Where(x => x.UserId == _currentUser.UserID)
                                                  .Select(x => new WistListResponse
                                                  {
                                                      WishListId = x.Id,
                                                      WishListDescription = x.Description,
                                                      WishListCategories = x.WishListCategoriesAndSubCategories.Select(x => new Categories
                                                      {
                                                          CategoryId = x.CategoryId,
                                                          CategoryName = x.CategoryMaster.Name
                                                      }).ToList(),
                                                      WishListSubCategories = x.WishListCategoriesAndSubCategories.Select(x => new SubCategories
                                                      {
                                                          SubCategoryId = x.SubCategoryId,
                                                          SubCategoryName = x.SubCategoryMaster.SubCategoryName
                                                      }).ToList(),
                                                      CreatedDate = x.CreatedDate
                                                  })
                                                  .ToListAsync();
            if (wishList.Count == 0)
                return new ServiceResponse<List<WistListResponse>>(StatusCodes.Status404NotFound, "no wishlist found", null);

            return new ServiceResponse<List<WistListResponse>>(StatusCodes.Status200OK, "User WishList", wishList);
        }

        public async Task<ServiceResponse<WistListResponse>> UserWishListById(long Id)
        {
            var wishList = await _context.WishList.Include(x => x.WishListCategoriesAndSubCategories)
                                                  .ThenInclude(x => x.CategoryMaster)
                                                  .Include(x => x.WishListCategoriesAndSubCategories)
                                                  .ThenInclude(x => x.SubCategoryMaster)
                                                  .Where(x => x.UserId == _currentUser.UserID &&
                                                              x.Id == Id)
                                                  .Select(x => new WistListResponse
                                                  {
                                                      WishListId = x.Id,
                                                      WishListDescription = x.Description,
                                                      WishListCategories = x.WishListCategoriesAndSubCategories.Select(x => new Categories
                                                      {
                                                          CategoryId = x.CategoryId,
                                                          CategoryName = x.CategoryMaster.Name
                                                      }).ToList(),
                                                      WishListSubCategories = x.WishListCategoriesAndSubCategories.Select(x => new SubCategories
                                                      {
                                                          SubCategoryId = x.SubCategoryId,
                                                          SubCategoryName = x.SubCategoryMaster.SubCategoryName
                                                      }).ToList(),
                                                      CreatedDate = x.CreatedDate
                                                  })
                                                  .FirstOrDefaultAsync();
            if (wishList == null)
                return new ServiceResponse<WistListResponse>(StatusCodes.Status404NotFound, "no wishlist found", null);

            return new ServiceResponse<WistListResponse>(StatusCodes.Status200OK, "User WishList", wishList);
        }

    }
}


