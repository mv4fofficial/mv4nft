﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using MV4F.Entities;
using MV4F.IRepository;
using MV4F.IServices;
using MV4F.Repository;

namespace MV4F.Services
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IConfiguration _configuration;
        private readonly IEmailService _emailService;
        private readonly IStorage _storage;
        private readonly ICurrentUser _currentUser;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly MV4FContext _context;
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly IFileExtentionService _fileExtentionService;

        public UnitOfWork(IConfiguration configuration,
                          IEmailService emailService,
                          IStorage storage,
                          ICurrentUser currentUser,
                          IHttpContextAccessor httpContextAccessor,
                          MV4FContext context,
                          IWebHostEnvironment webHostEnvironment,
                          IFileExtentionService fileExtentionService)
        {
            _configuration = configuration;
            _emailService = emailService;
            _storage = storage;
            _currentUser = currentUser;
            _httpContextAccessor = httpContextAccessor;
            _context = context;
            _webHostEnvironment = webHostEnvironment;
            _fileExtentionService = fileExtentionService;
        }

        public IAccountServices AccountServices =>
            new AccountServices(_configuration, _emailService, _storage, _currentUser, _context);

        public IAdminService AdminServices =>
            new AdminService(_configuration, _context, _storage);

        public IEmailService EmailServices =>
            new EmailServices(_configuration, _webHostEnvironment);

        public IStorage Storage =>
            new Storage(_configuration, _webHostEnvironment, _fileExtentionService);

        public ICurrentUser CurrentUser =>
            new CurrentUser(_httpContextAccessor);

        public ISellerServices SellerServices =>
            new SellerServices(_currentUser, _storage, _context);

        public IUserServices UserServices =>
            new UserServices(_currentUser, _context, _storage);

        public IDropDownServices DropDownServices =>
            new DropDownServices(_context);

        public IPaymentService PaymentService =>
            new PaymentService(_configuration, _context, _currentUser, _storage);

        public IHomeServices HomeServices =>
            new HomeServices(_context, _currentUser, _storage);

        public IGameServices GameServices =>
            new GameServices(_configuration, _emailService, _storage, _currentUser, _context);

         public IUserGameAssetsServices UserGameAssetsServices =>
            new UserGameAssetsServices(_configuration, _emailService, _storage, _currentUser, _context);

        public IPuzzleDetailServices PuzzleDetailServices =>
           new PuzzleDetailServices(_configuration, _emailService, _storage, _currentUser, _context);

        public IUserPuzzleDetailServices UserPuzzleDetailServices =>
           new UserPuzzleDetailServices(_configuration, _emailService, _storage, _currentUser, _context);

        public ILeaderboardServices LeaderboardServices =>
         new LeaderboardServices(_configuration, _currentUser, _storage, _context);

        public IPuzzleCategoryServices PuzzleCategoryServices =>
            new PuzzleCategoryServices(_configuration, _storage, _currentUser, _context);

        public IGameAssetCategoryServices GameAssetCategoryServices =>
           new GameAssetCategoryServices(_configuration, _storage, _currentUser, _context);

    }
}
