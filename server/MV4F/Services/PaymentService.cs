﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using MV4F.Entities;
using MV4F.IRepository;
using MV4F.IServices;
using MV4F.Models;
using Stripe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MV4F.Services
{
    public class PaymentService : IPaymentService
    {
        private readonly IConfiguration _configuration;
        private readonly MV4FContext _context;
        private readonly ICurrentUser _currentUser;
        private readonly IStorage _storage;

        public PaymentService(IConfiguration configuration,
                            MV4FContext context,
                            ICurrentUser currentUser,
                              IStorage storage
                            )
        {
            _configuration = configuration;
            _context = context;
            _currentUser = currentUser;
            _storage = storage;

        }

        private async Task<ApiResponse> MakePaymentForOrder([FromBody] PaymentGatewayModel paymentRequest)
        {

            var CheckOrderId = await _context.PaymentLedger.Where(x => x.Id == paymentRequest.OrderId &&
                                                                       x.UserId == _currentUser.UserID)
                                                           .FirstOrDefaultAsync();

            if (CheckOrderId == null)
            {
                try
                {
                    var stripeCharge = CreateCharge(paymentRequest);

                    var save = SavePaymentDetails(stripeCharge, paymentRequest.OrderId, paymentRequest.Amount);


                    if (save.Result.TransactionStatus == "succeeded")
                    {
                        return new ApiResponse("succeeded", 200);
                    }
                    if (save.Result.TransactionStatus == "pending")
                    {
                        return new ApiResponse("pending", 201);
                    }
                    if (save.Result.TransactionStatus == "failed")
                    {
                        return new ApiResponse("failed", 400);
                    }
                }
                catch (Exception ex)
                {
                    return new ApiResponse(ex.Message, 400);
                }
            }
            return new ApiResponse("Already Payment is the Done For this Order  ", 400);

        }

        private Charge CreateCharge(PaymentGatewayModel paymentRequest)
        {
            var getStripeKey = _configuration.GetValue<string>("PaymentConfiguration:SecretKey");
            StripeConfiguration.ApiKey = getStripeKey;

            var myCharge = new ChargeCreateOptions();
            myCharge.Source = paymentRequest.TokenId;
            myCharge.Amount = ((long?)(paymentRequest.Amount * 100));
            myCharge.Currency = "USD";

            myCharge.Metadata = new Dictionary<string, string>
            {
                ["OurRef"] = "product-User:" + paymentRequest.OrderId + "/Id:" + Guid.NewGuid().ToString()
            };
            var chargeService = new ChargeService();
            Charge stripeCharge = chargeService.Create(myCharge);

            return stripeCharge;
        }

        private async Task<PaymentLedger> SavePaymentDetails(Charge stripeCharge, long orderId, decimal Amount)
        {
            PaymentLedger paymentLedger = new()
            {
                CreatedDate = DateTime.UtcNow,
                UserId = _currentUser.UserID,
                TransactionStatus = stripeCharge.Status.ToString(),
                UpdatedDate = DateTime.UtcNow,
                Amount = Amount,
                OrderId = orderId,
                // Destination = "acct_1LSOraPx4FRboLpM",
            };

            await _context.PaymentLedger.AddAsync(paymentLedger);
            await _context.SaveChangesAsync();

            return paymentLedger;
        }


        public async Task<ApiResponse> PlaceOrder(PlaceOrderModel order)
        {
            try
            {
                UserOrders userOrders = new()
                {
                    CreatedDate = DateTime.UtcNow,
                    Amount = order.OrderAmount,
                    OrderStatus = "Payment Inititated",
                    UserId = _currentUser.UserID
                };
                await _context.UserOrders.AddAsync(userOrders);
                await _context.SaveChangesAsync();

                PaymentGatewayModel paymentRequest = new PaymentGatewayModel();
                paymentRequest.Amount = order.OrderAmount;
                paymentRequest.OrderId = userOrders.Id;
                paymentRequest.TokenId = order.TokenId;
                var placeOrder = MakePaymentForOrder(paymentRequest);

                if (placeOrder.Result.Message == "succeeded")
                {
                    await UpdateData(userOrders.Id, order.CartIds, placeOrder.Result.Message);
                    return new ApiResponse(placeOrder.Result.Message, 201);
                }
                if (placeOrder.Result.Message == "pending")
                {
                    await UpdateData(userOrders.Id, order.CartIds, placeOrder.Result.Message);
                    return new ApiResponse(placeOrder.Result.Message, 201);
                }
                if (placeOrder.Result.Message == "failed")
                {
                    await UpdateData(userOrders.Id, order.CartIds, placeOrder.Result.Message);
                    return new ApiResponse(placeOrder.Result.Message, 400);
                }
                else
                {
                    await UpdateData(userOrders.Id, order.CartIds, placeOrder.Result.Message);
                    return new ApiResponse(placeOrder.Result.Message, 400);
                }
            }
            catch (Exception ex)
            {
                return new ApiResponse(ex.Message, StatusCodes.Status400BadRequest);
            }
        }

        private async Task<bool> UpdateData(long id, List<long> cartIds, string message)
        {
            var getOrder = _context.UserOrders.Where(x => x.Id == id).FirstOrDefault();

            getOrder.OrderStatus = message;
            _context.UserOrders.Update(getOrder);

            if (message == "succeeded")
            {
                if (cartIds.Count > 0)
                {
                    foreach (var removeFromCart in cartIds)
                    {
                        var abc = await _context.UserCart.Where(x => x.Id == removeFromCart)
                                                         .Select(x => new { cartid = x.Id, productId = x.ProductId })
                                                         .ToListAsync();

                        var productId = abc.Where(x => x.cartid == removeFromCart).Select(x => x.productId).FirstOrDefault();

                        UserProduct document = new()
                        {
                            UserId = _currentUser.UserID,
                            OrderId = id,
                            ProductId = productId,
                            CreatedDate = DateTime.UtcNow,
                            UpdatedDate = DateTime.UtcNow,
                        };

                        var productOwner = await _context.Product.Include(x => x.Seller)
                            .Where(x => x.Id == productId)
                            .Select(x => new ViewProductModel
                            {
                                ProductId = x.Id,
                                Price = x.Price,
                                Seller = x.Seller.FirstName + " " + x.Seller.LastName,
                                SellerId = x.SellerId,
                                SellerEamil = x.Seller.StripeId
                            })
                            .FirstOrDefaultAsync();

                        TransactionLog newLog = new()
                        {
                            Amount = await _context.UserCart.Include(x => x.Product)
                                                            .Where(x => x.Id == removeFromCart)
                                                            .Select(x => x.Product.Price)
                                                            .FirstOrDefaultAsync(),
                            CreatedDate = DateTime.UtcNow,
                            OrderId = id,
                            OrderStatus = message,
                            ProductId = productId,
                            UserId = _currentUser.UserID,
                            Destination = productOwner.SellerEamil,
                            NetAmount = (productOwner.Price - (productOwner.Price * decimal.Parse(_configuration.GetValue<string>("PaymentConfiguration:Fee")) / 100)),
                            DueDate = DateTime.UtcNow.AddDays(7)
                        };

                        var getRemovableCarts = await _context.UserCart.Where(x => x.Id == removeFromCart).FirstOrDefaultAsync();
                        await _context.AddAsync(newLog);
                        _context.UserProduct.Add(document);
                        _context.UserCart.Remove(getRemovableCarts);
                    }
                }
            }
            else
            {
                if (cartIds.Count > 0)
                {
                    foreach (var cart in cartIds)
                    {
                        var getCart = await _context.UserCart.Where(x => x.UserId == _currentUser.UserID && x.Id == cart)
                                                             .FirstOrDefaultAsync();
                        getCart.OrderStatus = message;
                        getCart.OrderId = id;
                        _context.UserCart.Update(getCart);

                        var productId = _context.UserCart.Where(x => x.Id == cart).Select(x => x.ProductId).FirstOrDefault();
                        var productOwner = await _context.Product.Include(x => x.Seller)
                            .Where(x => x.Id == productId)
                            .Select(x => new ViewProductModel
                            {
                                ProductId = x.Id,
                                Price = x.Price,
                                Seller = x.Seller.FirstName + " " + x.Seller.LastName,
                                SellerId = x.SellerId,
                                SellerEamil = x.Seller.StripeId
                            })
                            .FirstOrDefaultAsync();

                        TransactionLog newLog = new()
                        {
                            Amount = await _context.UserCart.Include(x => x.Product)
                                                            .Where(x => x.Id == cart)
                                                            .Select(x => x.Product.Price)
                                                            .FirstOrDefaultAsync(),
                            CreatedDate = DateTime.UtcNow,
                            OrderId = id,
                            OrderStatus = message,
                            ProductId = productId,
                            UserId = _currentUser.UserID,
                            SellerName = productOwner.Seller,
                            Destination = productOwner.SellerEamil,
                            NetAmount = (productOwner.Price - (productOwner.Price * decimal.Parse(_configuration.GetValue<string>("PaymentConfiguration:Fee")) / 100)),
                            DueDate = DateTime.UtcNow.AddDays(7)
                        };

                    }
                }
            }
            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<ApiResponse> PaymentTransferToSeller() {

            var getStripeKey = _configuration.GetValue<string>("PaymentConfiguration:SecretKey");
            StripeConfiguration.ApiKey = getStripeKey;

            var payments = await _context.TransactionLog.Where(x => x.IsPaid == false).Where(x => x.DueDate <= DateTime.UtcNow).Select(x => new TransactionLog
            {
                Id = x.Id,
                UserId = x.UserId,
                Amount = x.Amount,
                ProductId = x.ProductId,
                Destination = x.Destination,
                DueDate = x.DueDate,
                CreatedDate = x.CreatedDate,
                OrderId = x.OrderId,
                IsPaid = x.IsPaid,
                OrderStatus = x.OrderStatus,
                NetAmount = x.NetAmount
            }).ToListAsync();


            foreach (var value in payments)
            {
                //body of foreach loop
                // Console.WriteLine(value);

                var accountId = value.Destination; // "acct_1LSOraPx4FRboLpM";
                var amount = value.NetAmount;

                var linkOptions = new AccountLinkCreateOptions
                {
                    Account = accountId,
                    RefreshUrl = "https://example.com/reauth",
                    ReturnUrl = "https://localhost:5001",
                    Type = "account_onboarding",
                };
                var linkservice = new AccountLinkService();
                var res = linkservice.Create(linkOptions);

                var options = new TransferCreateOptions
                {
                    Amount = ((long?)(amount * 100)),
                    Currency = "sgd",
                    Destination = accountId,
                    TransferGroup = "ORDER_95",
                };
                var service = new TransferService();
                service.Create(options);

                if ((int) res.StripeResponse.StatusCode != null) {
                    value.IsPaid = true;
                    _context.Update(value);
                    await _context.SaveChangesAsync();
                }
            }

            return new ApiResponse("success", 200);
        }

        public async Task<ApiResponse> TestConnect() {

            // StripeConfiguration.ApiKey = "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";
            var getStripeKey = _configuration.GetValue<string>("PaymentConfiguration:SecretKey");
            StripeConfiguration.ApiKey = getStripeKey;

            var options = new AccountCreateOptions
            {
            Type = "express",
            Country = "SG",
            Email = "win+7@example.com",
            Capabilities = new AccountCapabilitiesOptions
            {
                CardPayments = new AccountCapabilitiesCardPaymentsOptions
                {
                Requested = true,
                },
                Transfers = new AccountCapabilitiesTransfersOptions
                {
                Requested = true,
                },
            },
            };
            var service = new AccountService();
            var account = service.Create(options);

            var accountId = account.Id;

            var linkOptions = new AccountLinkCreateOptions
            {
                Account = accountId,
                RefreshUrl = "https://example.com/reauth",
                ReturnUrl = "https://staging.mv4f.io/",
                Type = "account_onboarding",
                Collect = "eventually_due",
            };
            var linkservice = new AccountLinkService();
            var res = linkservice.Create(linkOptions);
            Console.WriteLine(res);

            // var loginService = new LoginLinkService();
            // var loginLink = loginService.Create(accountId);
            // Console.WriteLine(loginLink);

            return new ApiResponse("success", 200);
        }

        public async Task<ServiceResponse<string>> CreateAccount(DataForStripeModel dataForStripe) {
            var getStripeKey = _configuration.GetValue<string>("PaymentConfiguration:SecretKey");
            StripeConfiguration.ApiKey = getStripeKey;

            var options = new AccountCreateOptions
            {
            Type = "express",
            Country = "SG",
            Email = dataForStripe.Email,
            Capabilities = new AccountCapabilitiesOptions
            {
                CardPayments = new AccountCapabilitiesCardPaymentsOptions
                {
                Requested = true,
                },
                Transfers = new AccountCapabilitiesTransfersOptions
                {
                Requested = true,
                },
            },
            };
            var service = new AccountService();
            var account = service.Create(options);

            // can move to user
            var userId = _currentUser.UserID;
            var user = _context.User.Where(x => x.Id == userId).FirstOrDefault();
            user.StripeId = account.Id;
            _context.Update(user);
            await _context.SaveChangesAsync();
            // can move to user

            return new ServiceResponse<string>(200, "success", account.Id);
        }

        public async Task<ServiceResponse<object>> CompleteAccount(DataForStripeModel dataForStripe) {

            var getStripeKey = _configuration.GetValue<string>("PaymentConfiguration:SecretKey");
            StripeConfiguration.ApiKey = getStripeKey;

            var linkOptions = new AccountLinkCreateOptions
            {
                Account = dataForStripe.AccountId,
                RefreshUrl = "https://example.com/reauth",
                ReturnUrl = dataForStripe.ReturnUrl, // _configuration.GetValue<string>("PaymentConfiguration:ReturnUrl"),
                Type = "account_onboarding",
                Collect = "eventually_due",
            };
            var linkservice = new AccountLinkService();
            var res = linkservice.Create(linkOptions);

            // return new ApiResponseWithData("success", 200, res);
            return new ServiceResponse<object>(200, "success", res);
        }

        public async Task<ServiceResponse<object>> CheckAccount(DataForStripeModel dataForStripe) {
            var getStripeKey = _configuration.GetValue<string>("PaymentConfiguration:SecretKey");
            StripeConfiguration.ApiKey = getStripeKey;
            
            var service = new AccountService();
            var account = service.Get(dataForStripe.AccountId);

            // return new ApiResponseWithData("success", 200, res);
            return new ServiceResponse<object>(200, "success", account);
        }

        public async Task<ServiceResponse<object>> GetBalance(DataForStripeModel dataForStripe) {
            var getStripeKey = _configuration.GetValue<string>("PaymentConfiguration:SecretKey");
            StripeConfiguration.ApiKey = getStripeKey;

            if (dataForStripe.AccountId == "") {
                var service = new BalanceService();
                Balance balance = service.Get();

                return new ServiceResponse<object>(200, "success", balance);
            } else {
                var requestOptions = new RequestOptions();
                requestOptions.StripeAccount = dataForStripe.AccountId;
                var bService = new BalanceService();
                Balance balance = bService.Get(requestOptions);

                return new ServiceResponse<object>(200, "success", balance);
            }
            

            // return new ApiResponseWithData("success", 200, res);
            
        }

        public async Task<ApiResponse> UpdateCompleteAccountStatus() {
            // can move to user
            var userId = _currentUser.UserID;
            var user = _context.User.Where(x => x.Id == userId).FirstOrDefault();
            user.StripeStatus = true;
            _context.Update(user);
            await _context.SaveChangesAsync();
            // can move to user

            return new ApiResponse("success", 200);
        }

        public async Task<ServiceResponse<object>> CreateLoginLink(DataForStripeModel dataForStripe) {
            var getStripeKey = _configuration.GetValue<string>("PaymentConfiguration:SecretKey");
            StripeConfiguration.ApiKey = getStripeKey;

            var service = new LoginLinkService();
            var res = service.Create(dataForStripe.AccountId);

            // return new ApiResponseWithData("success", 200, res);
            return new ServiceResponse<object>(200, "success", res);
        }

        public async Task<ServiceResponse<object>> FetchTransactions(DataForStripeModel dataForStripe) {
            
            var transactionLogs = await _context.TransactionLog.Include(x => x.User)
                .Where(x => x.Destination == dataForStripe.AccountId).Select(x => new TransactionLog
                {
                    Id = x.Id,
                    SellerName = x.SellerName,
                    BuyerName = x.User.FirstName + " " + x.User.LastName,
                    Amount = x.Amount,
                    ProductId = x.ProductId,
                    Destination = x.Destination,
                    DueDate = x.DueDate,
                    CreatedDate = x.CreatedDate,
                    OrderId = x.OrderId,
                    IsPaid = x.IsPaid,
                    OrderStatus = x.OrderStatus,
                    NetAmount = x.NetAmount
                })
                .ToListAsync();

            // return new ApiResponseWithData("success", 200, res);
            return new ServiceResponse<object>(200, "success", transactionLogs);
        }

        public async Task<ServiceResponse<object>> FetchStripeTransactions() {
            var getStripeKey = _configuration.GetValue<string>("PaymentConfiguration:SecretKey");
            StripeConfiguration.ApiKey = getStripeKey;

            var options = new BalanceTransactionListOptions
            {
                Limit = 100,
            };

            var service = new BalanceTransactionService();
            StripeList<BalanceTransaction> balanceTransactions = service.List(
            options);

            return new ServiceResponse<object>(200, "success", balanceTransactions);
        }

        public async Task<ServiceResponse<object>> FetchStripeConnectAccountList() {
            var getStripeKey = _configuration.GetValue<string>("PaymentConfiguration:SecretKey");
            StripeConfiguration.ApiKey = getStripeKey;

            var options = new AccountListOptions
            {
                Limit = 100,
            };
            var service = new AccountService();
            StripeList<Account> accounts = service.List(
            options);

            return new ServiceResponse<object>(200, "success", accounts);
        }
    }
}
