﻿namespace MV4F.Common
{
    public class Base64ToTextExtentions
    {
    }

    public class Constants
    {
        public const string PngExt = ".png";
        public const string PngSubString = "IVBOR";
        public const string JpgExt = ".jpg";
        public const string JpgSubString = "/9J/4";
        public const string mp4Ext = ".mp4";
        public const string mp4SubString = "AAAAF";
        public const string pdfExt = ".pdf";
        public const string pdfSubString = "JVBER";
        public const string icoExt = ".ico";
        public const string icoSubString = "AAABA";
        public const string rarExt = ".rar";
        public const string rarSubString = "UMFYI";
        public const string rtfExt = ".rtf";
        public const string rtfSubString = "E1XYD";
        public const string txtExt = ".txt";
        public const string txtSubString = "U1PKC";
        public const string srtExt = ".srt";
        public const string srtSubStringa = "MQOWM";
        public const string srtSubStringb = "77U/M";
    }
}
