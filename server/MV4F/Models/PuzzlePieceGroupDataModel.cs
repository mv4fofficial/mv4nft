﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace MV4F.Models
{
    public class PuzzlePieceGroupDataModel
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("pieceNameList")]
        public List<string> PieceNameList { get; set; }

        [JsonProperty("puzzleProgressDataId")]
        public long PuzzleProgressDataId { get; set; }
    }
}
