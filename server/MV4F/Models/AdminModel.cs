﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MV4F.Models
{
    public class ProductSummaryModel
    {
        [JsonProperty("productId")]
        public long ProductId { get; set; }
    }

    public class ImageConvert
    {
        [JsonProperty("base64")]
        public string Base64 { get; set; }

        [JsonProperty("extention")]
        public string Extention { get; set; }
    }


    public class ProductSummary
    {
        [JsonProperty("productId")]
        public long ProductId { get; set; }

        [JsonProperty("productTitle")]
        public string ProductTitle { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("productImage")]
        public string ProductImage { get; set; }

        [JsonProperty("extention")]
        public string Extention { get; set; }

        [JsonProperty("price")]
        public decimal Price { get; set; }

        [JsonProperty("nftStatus")]
        public bool NFTStatus { get; set; }

        [JsonProperty("sellerId")]
        public long SellerId { get; set; }

        [JsonProperty("sellerName")]
        public string SellerName { get; set; }

        [JsonProperty("sellerEmail")]
        public string SellerEamil { get; set; }

        [JsonProperty("buyers")]
        public List<BuyerDetails> Buyers { get; set; }
    }

    public class BuyerDetails
    {
        [JsonProperty("buyerId")]
        public long BuyerId { get; set; }

        [JsonProperty("buyerName")]
        public string BuyerName { get; set; }

        [JsonProperty("buyerEmail")]
        public string BuyerEmail { get; set; }
    }

    public class FilterModel
    {
        [JsonProperty("categories")]
        public List<long> Categories { get; set; }

        [JsonProperty("subCategories")]
        public List<long> SubCategories { get; set; }

        [JsonProperty("tags")]
        public List<string> Tags { get; set; }

        [JsonProperty("priceFrom")]
        public decimal PriceFrom { get; set; }

        [JsonProperty("priceTo")]
        public decimal PriceTo { get; set; }

        [JsonProperty("nftAvailability")]
        public bool NFTAvailability { get; set; }

        [JsonProperty("filter")]
        public string Filter { get; set; }

        [JsonProperty("pageIndex")]
        public int PageIndex { get; set; }

        [JsonProperty("pageSize")]
        [RegularExpression("([1-9][0-9]*)")]
        public int PageSize { get; set; }
    }


    public class AddCategoryModel
    {
        [JsonProperty("categoryName")]
        [Required]
        [RegularExpression("([A-Za-z_ ]*)")]
        public string CategoryName { get; set; }
    }
    public class ActionRequestModel
    {
        [JsonProperty("userId")]
        public long userId { get; set; }
        [JsonProperty("requestType")]
        public string requestType { get; set; }
        [JsonProperty("status")]
        public string status { get; set; }
    }
    public class AddTagModel
    {
        [JsonProperty("tagName")]
        [Required]
        [RegularExpression("([A-Za-z_ ]*)")]
        public string TagName { get; set; }
    }

    public class AddSubCategoryModel
    {
        [JsonProperty("subCategoryName")]
        [Required]
        [RegularExpression("([A-Za-z_ ]*)")]
        public string SubCategoryName { get; set; }

        [JsonProperty("categoryId")]
        [Required]
        public long CategoryId { get; set; }
    }


    public class UpdateCategotyModel
    {

        [JsonProperty("categoryId")]
        [Required]
        public long CategoryId { get; set; }

        [JsonProperty("categoryName")]
        [Required]
        [RegularExpression("([A-Za-z_ ]*)")]
        public string CategoryName { get; set; }

    }

    public class UpdateTagModel
    {

        [JsonProperty("tagId")]
        [Required]
        public long TagId { get; set; }

        [JsonProperty("tagName")]
        [Required]
        [RegularExpression("([A-Za-z_ ]*)")]
        public string TagName { get; set; }

    }

    public class UpdateSubCategotyModel
    {

        [JsonProperty("subCategoryId")]
        [Required]
        public long SubCategoryId { get; set; }

        [JsonProperty("subCategoryName")]
        [Required]
        [RegularExpression("([A-Za-z_ ]*)")]
        public string SubCategoryName { get; set; }

    }

    public class UserRequestViewModel
    {
        [JsonProperty("userId")]
        public long UserId { get; set; }

        [JsonProperty("userName")]
        public string UserName { get; set; }

        [JsonProperty("requestedRoleId")]
        public long? RequestedRoleId { get; set; }

        [JsonProperty("requestType")]
        public string RequestType  { get; set; }

        [JsonProperty("description")]
        public string Description  { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("attachFileUrl")]
        public string AttachFileUrl { get; set; }

        [JsonProperty("createdDate")]
        public DateTime CreatedDate { get; set; }

        [JsonProperty("updatedDate")]
        public DateTime UpdatedDate { get; set; }
    }

    public class ProfileViewModel
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("emailId")]
        public string EmailId { get; set; }

        [JsonProperty("phoneNumber")]
        public long? PhoneNumber { get; set; }

        [JsonProperty("country")]
        public CountryModel Country { get; set; }

        [JsonProperty("dateOfBirth")]
        public DateTime? DateOfBirth { get; set; }

        [JsonProperty("profilePicture")]
        public string ProfilePicture { get; set; }

        [JsonProperty("backgroundPicture")]
        public string BackgroundPicture { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }



        [JsonProperty("extention")]
        public string Extention { get; set; }

        [JsonProperty("bio")]
        public string Bio { get; set; }

        [JsonProperty("isRequestPending")]
        public bool IsRequestPending { get; set; }

        [JsonProperty("roles")]
        public List<string> Roles { get; set; }

        [JsonProperty("itemsOwned")]
        public long ItemsOwned { get; set; }

        [JsonProperty("itemsCreated")]
        public long ItemsCreated { get; set; }       

        [JsonProperty("itemsSold")]
        public long ItemsSold { get; set; } 

        [JsonProperty("itemsBought")]
        public long ItemsBought { get; set; }

        [JsonProperty("stripeAccount")]
        public string StripeAccount { get; set; } 

        [JsonProperty("stripeStatus")]
        public bool StripeStatus { get; set; } 

        [JsonProperty("wallet")]
        public string Wallet { get; set; } 

        [JsonProperty("createdDate")]
        public DateTime CreatedDate { get; set; }


    }

    public class ProfileUpdateModel
    {

        [JsonProperty("firstName")]
        [RegularExpression("([A-Za-z]*)")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        [RegularExpression("([A-Za-z_ ]*)")]
        public string LastName { get; set; }

        [JsonProperty("phoneNumber")]
        [RegularExpression("([6-9][0-9]*)")]
        public long PhoneNumber { get; set; }

        [JsonProperty("countryId")]
        [RegularExpression("([1-9][0-9]*)")]
        public long CountryId { get; set; }

        [JsonProperty("dateOfBirth")]
        public DateTime DateOfBirth { get; set; }

        [JsonProperty("profilePicture")]
        public string ProfilePicture { get; set; }

        [JsonProperty("backgroundPicture")]
        public string BackgroundPicture { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }


        [JsonProperty("extention")]
        public string Extention { get; set; }

        [JsonProperty("bio")]
        public string Bio { get; set; }

        [JsonProperty("role_id")]
        public long RoleId { get; set; }

        [JsonProperty("attach_file_url")]
        public string AttachFileUrl { get; set; }
    }

    public class RegisteredUserModel
    {
        [JsonProperty("userName")]
        public string UserName { get; set; }

        [JsonProperty("profilePicture")]
        public string ProfilePicture { get; set; }

        [JsonProperty("extention")]
        public string Extention { get; set; }

        [JsonProperty("contactNumber")]
        public long? ContactNumber { get; set; }

        [JsonProperty("emailId")]
        public string EamilId { get; set; }

        [JsonProperty("dateOfBirth")]
        public DateTime? DateOfBirth { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("roleId")]
        public long RoleId { get; set; }

        [JsonProperty("roleName")]
        public string RoleName { get; set; }
    }

    public class PaymentLedgerResponse
    {
        [JsonProperty("userName")]
        public string UserName { get; set; }

        [JsonProperty("orderId")]
        public long OrderId { get; set; }

        [JsonProperty("transactionStatus")]
        public string TransactionStatus { get; set; }

        [JsonProperty("amount")]
        public decimal Amount { get; set; }

        [JsonProperty("createdDate")]
        public DateTime CreatedDate { get; set; }
    }

    public class ViewUserProductModel
    {
        [JsonProperty("productId")]
        public long ProductId { get; set; }

        [JsonProperty("productName")]
        public string ProductName { get; set; }

        [JsonProperty("price")]
        public decimal Price { get; set; }

        [JsonProperty("category")]
        public List<ViewCategory> Category { get; set; }

        [JsonProperty("subCategory")]
        public List<ViewSubCategory> SubCategory { get; set; }

        [JsonProperty("tags")]
        public List<ViewTags> Tags { get; set; }

        [JsonProperty("productPrice")]
        public decimal ProductPrice { get; set; }

        [JsonProperty("productPicture")]
        public string ProductPicture { get; set; }

        [JsonProperty("extention")]
        public string Extention { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("sellerId")]
        public long SellerId { get; set; }

        [JsonProperty("seller")]
        public string Seller { get; set; }
    }

    public class SubProductPicturesModel {
      [JsonProperty("id")]
      public long Id  {get; set;}

      [JsonProperty("productPicture")]
      public string ProductPicture {get; set;}

    }

    public class ViewProductModel
    {
        [JsonProperty("productId")]
        public long ProductId { get; set; }

        [JsonProperty("productName")]
        public string ProductName { get; set; }

        [JsonProperty("category")]
        public List<ViewCategory> Category { get; set; }

        [JsonProperty("subCategory")]
        public List<ViewSubCategory> SubCategory { get; set; }

        [JsonProperty("tags")]
        public List<ViewTags> Tags { get; set; }

        [JsonProperty("productPicture")]
        public string ProductPicture { get; set; }
        
        [JsonProperty("subProductPictures")]
        public List<SubProductPicturesModel> SubProductPictures { get; set; }

        [JsonProperty("extention")]
        public string Extention { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("price")]
        public decimal Price { get; set; }

        [JsonProperty("gasFee")]
        public decimal? GasFee { get; set; }

        [JsonProperty("createdDate")]
        public DateTime CreatedDate { get; set; }

        [JsonProperty("sellerId")]
        public long SellerId { get; set; }

        [JsonProperty("seller")]
        public string Seller { get; set; }

        [JsonProperty("sellerEmail")]
        public string SellerEamil { get; set; }

        [JsonProperty("link")]
        public string Link { get; set; }

        [JsonProperty("quantity")]
        public long? Quantity { get; set; }
    }

    public class ViewProductForNftModel {
        [JsonProperty("name")]
        public string ProductName { get; set; }     

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("image_url")]
        public string ProductPicture { get; set; }

        [JsonProperty("category")]
        public string CategoryName { get; set; }

        [JsonProperty("sub_category")]
        public string SubCategoryName { get; set; }

        [JsonProperty("tag")]
        public string TagName { get; set; }

        [JsonProperty("rarity")]
        public string Rarity { get; set; }

    }

    public class ViewCategory
    {
        [JsonProperty("categoryId")]
        public long CategoryId { get; set; }

        [JsonProperty("categoryName")]
        public string CategoryName { get; set; }
    }

    public class ViewSubCategory
    {
        [JsonProperty("subCategoryId")]
        public long SubCategoryId { get; set; }

        [JsonProperty("subCategoryName")]
        public string SubCategoryName { get; set; }
    }

    public class ViewTags
    {
        [JsonProperty("tagId")]
        public long TagId { get; set; }

        [JsonProperty("TagName")]
        public string TagName { get; set; }
    }

    public class FollowUserData
    {
        [JsonProperty("followData")]
        public object FollowData { get; set; }

        [JsonProperty("followQty")]
        public object FollowQty { get; set; }

        [JsonProperty("followeeQty")]
        public object FolloweeQty { get; set; }
    }

    public class RecommendedForYouData
    {
        [JsonProperty("categories")]
        public object Categories { get; set; }

        [JsonProperty("subCategories")]
        public object SubCategories { get; set; }

        [JsonProperty("follows")]
        public object Follows { get; set; }

        [JsonProperty("products")]
        public object Products { get; set; }

        [JsonProperty("allProducts")]
        public object AllProducts { get; set; }
        
    }
}
