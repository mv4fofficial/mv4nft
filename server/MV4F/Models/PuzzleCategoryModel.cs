﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace MV4F.Models
{
    public class PuzzleCategoryViewModel
    {
        [JsonProperty("id")]
        public long Id { get; set; }
        
        [JsonProperty("name")]
        public string Name { get; set; }
       
        [JsonProperty("image")]
        public string Image { get; set; }

        //[JsonProperty("gameAssetId")]
        //public long GameAssetId { get; set; }

        [JsonProperty("gameAssets")]
        public List<GameAssetsViewModel> GameAssets { get; set; }

        [JsonProperty("createdDate")]
        public DateTime CreatedDate { get; set; }

        [JsonProperty("updatedDate")]
        public DateTime UpdatedDate { get; set; }
    }

    
    public class PuzzleCategoryAddModel
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("image")]
        public string Image { get; set; }

        [JsonProperty("createdDate")]
        public DateTime CreatedDate { get; set; }

        [JsonProperty("updatedDate")]
        public DateTime UpdatedDate { get; set; }
    }

}
