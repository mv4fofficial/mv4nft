﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace MV4F.Models
{
    public class GameAssetCategoryViewModel
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("categoryId")]
        public long CategoryId { get; set; }

        //[JsonProperty("puzzleCategory")]
        //public PuzzleCategoryViewModel puzzleCategory { get; set; }

        [JsonProperty("gameAssetId")]
        public long GameAssetId { get; set; }

        //[JsonProperty("gameAssets")]
        //public GameAssetsViewModel GameAssets { get; set; }

        [JsonProperty("createdDate")]
        public DateTime CreatedDate { get; set; }

        [JsonProperty("updatedDate")]
        public DateTime UpdatedDate { get; set; }
    }

    
    public class GameAssetCategoryAddModel
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("categoryId")]
        public long CategoryId { get; set; }

        [JsonProperty("gameAssetId")]
        public long GameAssetId { get; set; }

        [JsonProperty("createdDate")]
        public DateTime CreatedDate { get; set; }

        [JsonProperty("updatedDate")]
        public DateTime UpdatedDate { get; set; }
    }

}
