﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MV4F.Models
{
    public class APIResponses
    {
    }


    public class GetUserProductsFilter
    {
        [JsonProperty("categoryId")]
        public List<long> CategoryId { get; set; }

        [JsonProperty("subCategoryId")]
        public List<long> SubCategoryId { get; set; }
    }



    public class InterestedCategoriesModel
    {
        [JsonProperty("interestedCategory")]
        public List<InterestedCategory> InterestedCategory { get; set; }

        [JsonProperty("interestedSubCategory")]
        public List<InterestedSubCategory> InterestedSubCategory { get; set; }
    }

    public class InterestedCategory
    {
        [JsonProperty("categoryId")]
        public long? CategoryId { get; set; }

        [JsonProperty("categoryName")]
        public string CategoryName { get; set; }
    }

    public class InterestedSubCategory
    {
        [JsonProperty("subCategoryId")]
        public long? SubCategoryId { get; set; }

        [JsonProperty("subCategoryName")]
        public string SubCategoryName { get; set; }
    }


    public class RolesModel
    {
        [JsonProperty("roleId")]
        public long RoleId { get; set; }

        [JsonProperty("roleName")]
        public string RoleName { get; set; }
    }

    public class PaymentLedgerModel
    {
        [JsonProperty("Id")]
        public long Id { get; set; }

        [JsonProperty("amount")]
        public decimal Amount { get; set; }

        [JsonProperty("destination")]
        public string Destination { get; set; }

        [JsonProperty("dueDate")]
        public DateTime DueDate { get; set; }

        [JsonProperty("isPaid")]
        public bool IsPaid { get; set; }
    }


    public class WistListResponse
    {
        [JsonProperty("wishListId")]
        public long WishListId { get; set; }

        [JsonProperty("wishListDescription")]
        public string WishListDescription { get; set; }

        [JsonProperty("createdDate")]
        public DateTime CreatedDate { get; set; }

        [JsonProperty("wishListCategories")]
        public List<Categories> WishListCategories { get; set; }

        [JsonProperty("wishListSubCategories")]
        public List<SubCategories> WishListSubCategories { get; set; }
    }


    public class Categories
    {
        [JsonProperty("categoryId")]
        public long? CategoryId { get; set; }

        [JsonProperty("categoryName")]
        public string CategoryName { get; set; }
    }

    public class SubCategories
    {
        [JsonProperty("subCategoryId")]
        public long? SubCategoryId { get; set; }

        [JsonProperty("subCategoryName")]
        public string SubCategoryName { get; set; }
    }



    public class ApiResponse
    {
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("statusCode")]
        public long StatusCode { get; set; }

        public ApiResponse(string Message, int statusCode)
        {
            this.Message = Message;
            this.StatusCode = statusCode;
        }
    }

    public class ApiResponseWithData
    {
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("statusCode")]
        public long StatusCode { get; set; }

        [JsonProperty("data")]
        public object Data { get; set; }

        public ApiResponseWithData(string Message, int statusCode, object data)
        {
            this.Message = Message;
            this.StatusCode = statusCode;
            this.Data = data;
        }
    }

    public class BecomeSellerQuestionsModel
    {
        [JsonProperty("questionId")]
        public long QuestionId { get; set; }

        [JsonProperty("question")]
        public string Question { get; set; }
    }


    public class BecomeSellerModel
    {
        [JsonProperty("bio")]
        public string Bio { get; set; }

        [JsonProperty("categories")]
        public List<long> Categories { get; set; }

        [JsonProperty("subCategories")]
        public List<long> SubCategories { get; set; }

        [JsonProperty("becomeSellerQuestions")]
        public List<SellerAnswers> BecomeSellerQuestions { get; set; }
    }

    public class DataForStripeModel
    {
        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("accountId")]
        public string AccountId { get; set; }

        [JsonProperty("returnUrl")]
        public string ReturnUrl { get; set; }
    }

    public class SellerAnswers
    {
        [JsonProperty("questionId")]
        public long QuestionId { get; set; }

        [JsonProperty("answer")]
        public string Answer { get; set; }
    }

    public class NewUserRole
    {
        [JsonProperty("userRoles")]
        public List<string> UserRoles { get; set; }
    }

    public class ServiceResponse<T>
    {
        public ServiceResponse(long StatusCode, string Message, T Data)
        {
            this.Message = Message;
            this.StatusCode = StatusCode;
            this.Data = Data;
        }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("statusCode")]
        public long StatusCode { get; set; }

        [JsonProperty("data")]
        public T Data { get; set; }
    }



    public class PagedResponse<T>
    {
        [JsonProperty("pageIndex")]
        public int PageIndex { get; private set; }

        [JsonProperty("totalPages")]
        public int TotalPages { get; private set; }

        [JsonProperty("totalRecords")]
        public int TotalRecords { get; private set; }

        [JsonProperty("data")]
        public IEnumerable<T> Data { get; private set; }

        public PagedResponse(List<T> items, int count, int pageIndex, int pageSize)
        {
            this.PageIndex = pageIndex;
            TotalPages = (int)Math.Ceiling(count / (double)pageSize);
            TotalRecords = count;
            Data = items;
        }

        public bool hasPreviousPage
        {
            get
            {
                return (PageIndex > 0);
            }
        }

        public bool hasNextPage
        {
            get
            {
                return ((PageIndex + 1) < TotalPages);
            }
        }

        public static PagedResponse<T> Create(IEnumerable<T> source, int pageIndex, int pageSize)
        {
            var count = source.Count();
            var items = source.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
            return new PagedResponse<T>(items, count, pageIndex, pageSize);
        }

        public static async Task<PagedResponse<T>> CreateAsync(IQueryable<T> source, int pageIndex, int pageSize)
        {
            var count = await source.CountAsync();
            var items = source.Skip(pageIndex * pageSize).Take(pageSize).ToList();
            return new PagedResponse<T>(items, count, pageIndex, pageSize);
        }

        public static PagedResponse<T> Create(IEnumerable<T> source, int pageIndex, int pageSize, int totalRecords)
        {
            var count = source.Count();
            var items = source.ToList();
            return new PagedResponse<T>(items, totalRecords, pageIndex, pageSize);
        }
    }
}
