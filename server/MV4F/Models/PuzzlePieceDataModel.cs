﻿using Newtonsoft.Json;

namespace MV4F.Models
{
    public class PuzzlePieceDataModel
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        //public Vector3 position;
        //public Vector3 scale;

        [JsonProperty("rotationZ")]
        public float RotationZ { get; set; }

        [JsonProperty("puzzleProgressDataId")]
        public long PuzzleProgressDataId { get; set; }
    }
}
