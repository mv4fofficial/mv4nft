﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace MV4F.Models
{

    public class GameAssetsViewModel
    {
        [JsonProperty("id")]
        public long Id { get; set; }
        
        [JsonProperty("name")]
        public string Name { get; set; }
        
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("image")]
        public string Image { get; set; }

        [JsonProperty("coinPrice")]
        public long CoinPrice { get; set; }

        [JsonProperty("diamondPrice")]
        public long DiamondPrice { get; set; }

        [JsonProperty("createdDate")]
        public DateTime CreatedDate { get; set; }

        [JsonProperty("updatedDate")]
        public DateTime UpdatedDate { get; set; }

    }

    public class AddGameAssetModel
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("image")]
        public string Image { get; set; }

        [JsonProperty("coinPrice")]
        public long CoinPrice { get; set; }

        [JsonProperty("diamondPrice")]
        public long DiamondPrice { get; set; }
    }

}
