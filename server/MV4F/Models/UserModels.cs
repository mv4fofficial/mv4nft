﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace MV4F.Models
{

    public class UpdateWishListModel
    {
        [JsonProperty("wishListId")]
        public long WishListId { get; set; }

        [JsonProperty("categories")]
        public List<long?> Categories { get; set; }

        [JsonProperty("subCategories")]
        public List<long?> SubCategories { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }
    }


    public class AddWishListModel
    {

        [JsonProperty("categories")]
        public List<long?> Categories { get; set; }

        [JsonProperty("subCategories")]
        public List<long?> SubCategories { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }
    }

    public class AddProductWishListModel
    {

        [JsonProperty("productId")]
        public long ProductId { get; set; }
    }

    public class DeleteProductWishListModel
    {

        [JsonProperty("productId")]
        public long ProductId { get; set; }
    }

    public class ViewProductWishListModel
    {

        [JsonProperty("userId")]
        public long UserId { get; set; }

        [JsonProperty("product")]
        public ViewProductModel Product { get; set; }

        [JsonProperty("createdDate")]
        public DateTime CreatedDate { get; set; }

        [JsonProperty("updatedDate")]
        public DateTime UpdatedDate { get; set; }
    }

    public class AddAddressModel
    {
        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("address1")]
        public string Address1 { get; set; }

        [JsonProperty("address2")]
        public string Address2 { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("pincode")]
        public long Pincode { get; set; }

        [JsonProperty("isDefault")]
        public bool IsDefault { get; set; }
    }

    public class GetAddressModel
    {
        [JsonProperty("addressId")]
        public long AddressId { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("address1")]
        public string Address1 { get; set; }

        [JsonProperty("address2")]
        public string Address2 { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("pincode")]
        public long Pincode { get; set; }
    }


    public class PaymentGatewayModel
    {
        [JsonProperty("tokenId")]
        public string TokenId { get; set; }

        [JsonProperty("orderId")]
        public long OrderId { get; set; }

        [JsonProperty("amount")]
        public decimal Amount { get; set; }
    }

    public class PlaceOrderModel
    {
        [JsonProperty("tokenId")]
        public string TokenId { get; set; }

        [JsonProperty("orderAmount")]
        public decimal OrderAmount { get; set; }

        [JsonProperty("cartIds")]
        public List<long> CartIds { get; set; }
    }

    public class PaymentTransferAccountModel
    {
        [JsonProperty("accountId")]
        public string AccountId { get; set; }

        [JsonProperty("orderAmount")]
        public decimal OrderAmount { get; set; }
    }

    public class ClientSecretModel
    {
        [JsonProperty("clientSecret")]
        public string ClientSecret { get; set; }
    }

    public class MultipleCart
    {
        [JsonProperty("id")]
        public long Id { get; set; }
    }

    public class ViewCartModel
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("productName")]
        public string ProductName { get; set; }

        [JsonProperty("productPrice")]
        public decimal ProductPrice { get; set; }

        [JsonProperty("extention")]
        public string Extention { get; set; }

        [JsonProperty("gasFee")]
        public decimal? GasFee { get; set; }

        [JsonProperty("productImage")]
        public string ProductImage { get; set; }

        [JsonProperty("categories")]
        public List<string> Categories { get; set; }

        [JsonProperty("subCategories")]
        public List<string> SubCategories { get; set; }

        [JsonProperty("tags")]
        public List<string> Tags { get; set; }
    }


    public class UserOrderModel
    {
        [JsonProperty("productName")]
        public List<string> ProductName { get; set; }

        [JsonProperty("orderDate")]
        public DateTime OrderDate { get; set; }

        [JsonProperty("order_id")]
        public long OrderId { get; set; }

        [JsonProperty("amount")]
        public decimal Amount { get; set; }

        [JsonProperty("orderStatus")]
        public string OrderStatus { get; set; }
    }

    public class GetBuyerNoitificationModel
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("category")]
        public List<Category> Category { get; set; }

        [JsonProperty("subCategory")]
        public List<SubCategory> SubCategory { get; set; }

        [JsonProperty("requestedByUserId")]
        public long RequestedByUserId { get; set; }

        [JsonProperty("requestedByUser")]
        public string RequestedByUser { get; set; }

        [JsonProperty("requestedDate")]
        public DateTime RequestedDate { get; set; }
    }

    public class Category
    {
        [JsonProperty("id")]
        public long? Id { get; set; }

        [JsonProperty("categoryName")]
        public string CategoryName { get; set; }

    }

    public class SubCategory
    {
        [JsonProperty("id")]
        public long? Id { get; set; }

        [JsonProperty("subCategoryName")]
        public string SubCategoryName { get; set; }

    }


    public class UserFollowModel
    {

        [JsonProperty("followerId")]
        public long FollowerId { get; set; }

        [JsonProperty("email_status")]
        public bool EmailStatus { get; set; }
        
    }

    public class UserFollowRes
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("followerId")]
        public long FollowerId { get; set; }

        [JsonProperty("followeeId")]
        public long FolloweeId { get; set; }

        [JsonProperty("email_status")]
        public bool EmailStatus { get; set; }

        [JsonProperty("delete_flag")]
        public bool DeleteFlag { get; set; }

        [JsonProperty("created_date")]
        public DateTime CreatedDate { get; set; }
    }

    public class TopFollower
    {

        [JsonProperty("followerId")]
        public long FollowerId { get; set; }

        [JsonProperty("followeeId")]
        public long FolloweeId { get; set; }

        // [JsonProperty("followerCount")]
        // public long FollowerCount { get; set; }
        
    }

    public class UserList
    {
        [JsonProperty("userIds")]
        public List<long> UserIds { get; set; }
    }
}
