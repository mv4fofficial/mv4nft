﻿using Newtonsoft.Json;
using System;

namespace MV4F.Models
{
    public class PuzzleDetailViewModel
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        //[Column("puzzle_piece_list")]
        //public List<PuzzlePieceData> PuzzlePieceList { get; set; }

        //[Column("puzzle_piece_group_list")]
        //public List<PuzzlePieceGroupData> PuzzlePieceGroupList { get; set; }

        //[Column("unsolved_piece_id_list")]
        //public List<int> UnsolvedPieceIdList { get; set; }

        //[Column("moved_pieces_id_list")]
        //public List<int> MovedPieceIdList { get; set; }

        [JsonProperty("remainingHints")]
        public int RemainingHints { get; set; }

        [JsonProperty("timer")]
        public int Timer { get; set; }

        [JsonProperty("elapsedTime")]
        public int ElapsedTime { get; set; }

        [JsonProperty("createdDate")]
        public DateTime CreatedDate { get; set; }

        [JsonProperty("updatedDate")]
        public DateTime UpdatedDate { get; set; }

    }

    public class AddPuzzleDetailModel
    {
        //[Column("puzzle_piece_list")]
        //public List<PuzzlePieceData> PuzzlePieceList { get; set; }

        //[Column("puzzle_piece_group_list")]
        //public List<PuzzlePieceGroupData> PuzzlePieceGroupList { get; set; }

        //[Column("unsolved_piece_id_list")]
        //public List<int> UnsolvedPieceIdList { get; set; }

        //[Column("moved_pieces_id_list")]
        //public List<int> MovedPieceIdList { get; set; }

        [JsonProperty("remainingHints")]
        public int RemainingHints { get; set; }

        [JsonProperty("timer")]
        public int Timer { get; set; }

        [JsonProperty("elapsedTime")]
        public int ElapsedTime { get; set; }

        [JsonProperty("createdDate")]
        public DateTime CreatedDate { get; set; }

        [JsonProperty("updatedDate")]
        public DateTime UpdatedDate { get; set; }

    }
}
