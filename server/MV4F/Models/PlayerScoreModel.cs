﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace MV4F.Models
{
    public class PlayerScoreViewModel
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("userName")]
        public string UserName { get; set; }

        [JsonProperty("score")]
        public int Score { get; set; }
    }

    public class PlayerScoreAddModel
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("userId")]
        public long UserId { get; set; }

        [JsonProperty("score")]
        public int Score { get; set; }

        [JsonProperty("createdDate")]
        public DateTime CreatedDate { get; set; }

        [JsonProperty("updatedDate")]
        public DateTime UpdatedDate { get; set; }
    }
}
