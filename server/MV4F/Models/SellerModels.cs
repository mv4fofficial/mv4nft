﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MV4F.Models
{
    public class SellerModels
    {
    }
    public class SellerTransactionModel
    {
        [JsonProperty("userName")]
        public string UserName { get; set; }

        [JsonProperty("productName")]
        public string ProductName { get; set; }

        [JsonProperty("orderId")]
        public long OrderId { get; set; }

        [JsonProperty("amount")]
        public decimal Amount { get; set; }

        [JsonProperty("productIamge")]
        public string ProductImage { get; set; }

        [JsonProperty("extention")]
        public string Extention { get; set; }

        [JsonProperty("orderDate")]
        public DateTime OrderDate { get; set; }
    }

    public class AddProductModel
    {
        [JsonProperty("productName")]
        [Required]
        public string ProductName { get; set; }

        [JsonProperty("categoryId")]
        [Required]
        public List<long> CategoryId { get; set; }

        [JsonProperty("subCategoryId")]
        [Required]
        public List<long> SubCategoryId { get; set; }

        [JsonProperty("tags")]
        public List<string> Tags { get; set; }

        [JsonProperty("price")]
        [Required]
        public decimal Price { get; set; }

        [JsonProperty("rarity")]
        public string Rarity { get; set; }

        [JsonProperty("productImage")]
        [Required]
        public string ProductImage { get; set; }
        
        [JsonProperty("subProductImages")]
        public List<string> SubProductImages { get; set; }

        [JsonProperty("description")]
        [Required]
        public string Description { get; set; }

        [JsonProperty("fileExtension")]
        [Required]
        public string FileExtension { get; set; }

        [JsonProperty("link")]
        public string Link { get; set; }

        [JsonProperty("nftStatus")]
        [Required]
        public bool NFTStatus { get; set; }

        [JsonProperty("forSale")]
        public bool ForSale { get; set; }

        [JsonProperty("quantity")]
        public long? Quantity { get; set; }
    }


    public class EditProductModel
    {
        [JsonProperty("productId")]
        [Required]
        [RegularExpression("([1-9][0-9]*)")]
        public long ProductId { get; set; }

        [JsonProperty("productName")]
        [RegularExpression("([A-Za-z_ ]*)")]
        public string ProductName { get; set; }

        [JsonProperty("categoryId")]
        public List<long> CategoryId { get; set; }

        [JsonProperty("subCategoryId")]
        public List<long> SubCategoryId { get; set; }

        [JsonProperty("tags")]
        public List<string> Tags { get; set; }

        [JsonProperty("price")]
        public decimal Price { get; set; }

        [JsonProperty("productImage")]
        public string ProductImage { get; set; }
        
        [JsonProperty("subProductImages")]
        public List<string> SubProductImages { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("fileExtension")]
        public string FileExtension { get; set; }

        [JsonProperty("link")]
        public string Link { get; set; }

        [JsonProperty("nftStatus")]
        public bool NFTStatus { get; set; }

        [JsonProperty("forSale")]
        public bool ForSale { get; set; }

        [JsonProperty("quantity")]
        public long? Quantity { get; set; }

        [JsonProperty("deletedImages")]
        public List<long> DeletedImages {get; set;}
    }

    public class GetProductModel
    {
        [JsonProperty("productId")]
        public long ProductId { get; set; }

        [JsonProperty("productName")]
        public string ProductName { get; set; }

        [JsonProperty("category")]
        public List<ViewCategory> Category { get; set; }

        [JsonProperty("subCategory")]
        public List<ViewSubCategory> SubCategory { get; set; }

        [JsonProperty("tags")]
        public List<ViewTags> Tags { get; set; }

        [JsonProperty("price")]
        public decimal Price { get; set; }

        [JsonProperty("gasFee")]
        public decimal? GasFee { get; set; }

        [JsonProperty("productImage")]
        public string ProductImage { get; set; }

        [JsonProperty("extention")]
        public string Extention { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("nftStatus")]
        public bool NFTStatus { get; set; }

        [JsonProperty("forSale")]
        public bool ForSale { get; set; }

        [JsonProperty("link")]
        public string Link { get; set; }

        [JsonProperty("quantity")]
        public long? Quantity { get; set; }

        [JsonProperty("createdDate")]
        public DateTime CreatedDate { get; set; }

        [JsonProperty("updatedDate")]
        public DateTime UpdatedDate { get; set; }

        [JsonProperty("sellerId")]
        public long SellerId { get; set; }

        [JsonProperty("seller")]
        public string Seller { get; set; }

        [JsonProperty("sellerEmail")]
        public string SellerEamil { get; set; }


    }

}
