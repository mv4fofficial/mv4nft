﻿using MV4F.Entities;
using Newtonsoft.Json;
using System;

namespace MV4F.Models
{
    public class UserPuzzleDetailViewModel
    {
        [JsonProperty("id")]
        public long Id { get; set; }
        
        [JsonProperty("puzzleId")] 
        public long PuzzleId { get; set; }

        [JsonProperty("puzzleDetailViewModel")]
        public PuzzleDetailViewModel PuzzleDetailViewModel { get; set; }

        [JsonProperty("updated_date")]
        public DateTime UpdatedDate { get; set; }

        [JsonProperty("created_date")]
        public DateTime CreatedDate { get; set; }
    }

    public class AddUserPuzzleDetailModel
    {
        [JsonProperty("userId")]
        public long UserId { get; set; }

        [JsonProperty("puzzleId")]
        public long PuzzleId { get; set; }

        //[JsonProperty("puzzleDetailViewModel")]
        //public PuzzleDetailViewModel PuzzleDetailViewModel { get; set; }

        [JsonProperty("updated_date")]
        public DateTime UpdatedDate { get; set; }

        [JsonProperty("created_date")]
        public DateTime CreatedDate { get; set; }
    }
}
