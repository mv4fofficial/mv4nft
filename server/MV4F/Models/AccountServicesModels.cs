﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MV4F.Models
{
    public class AccountServicesModels
    {
    }

    public class CategoriesDropDownModel
    {
        [JsonProperty("id")]
        public long CategoryId { get; set; }

        [JsonProperty("categoryName")]
        public string CategoryName { get; set; }
    }

    public class SubCategoriesDropDownModel
    {
        [JsonProperty("id")]
        public long SubCategoryId { get; set; }

        [JsonProperty("subCategoryName")]
        public string SubCategoryName { get; set; }

        [JsonProperty("categoryId")]
        public long CategoryId { get; set; }
    }

    public class TagsDropDownModel
    {
        [JsonProperty("id")]
        public long TagId { get; set; }

        [JsonProperty("tagName")]
        public string TagName { get; set; }
    }

    public class RegisterModel
    {
        [JsonProperty("emailId")]
        [Required]
        public string EmailId { get; set; }

        [JsonProperty("password")]
        [Required]
        [StringLength(maximumLength: 25, ErrorMessage = "please enter a strong password", MinimumLength = 6)]
        public string Password { get; set; }

        [JsonProperty("firstName")]
        [Required]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("dateOfBirth")]
        [Required]
        public DateTime DateOfBirth { get; set; }

        [JsonProperty("contactNumber")]
        [Required]
        public long? ContactNumber { get; set; }

        [JsonProperty("countryId")]
        public long CountryId { get; set; }

        [JsonProperty("isAdmin")]
        [Required]
        public bool IsAdmin { get; set; }

        [JsonProperty("roleId")]
        [Required]
        public int RoleId { get; set; }

        [JsonProperty("categories")]
        public List<long> Categories { get; set; }

        [JsonProperty("subCategories")]
        public List<long> SubCategories { get; set; }
    }

    public class LoginModel
    {
        [JsonProperty("emailId")]
        [Required]
        //[RegularExpression("[A-Za-z0-9]+@[a-z]+.[a-z]{2,3}", ErrorMessage = "please enter a valid email")]
        public string EmailId { get; set; }

        [JsonProperty("password")]
        [Required]
        public string Password { get; set; }
    }

    public class ThirdPartyRegisterAndLoginModel
    {
        [JsonProperty("uuid")]
        [Required]
        public string UUID { get; set; }

        [JsonProperty("firstName")]
        [Required]
        public string FirstName { get; set; }
      
        [JsonProperty("profilePicture")]
        [Required]
        public string ProfilePicture { get; set; }
 
        [JsonProperty("emailId")]
        [Required]
        public string EmailId { get; set; }

        [JsonProperty("isEmailVerified")]
        [Required]
        public bool IsEmailVerified { get; set; }

        [JsonProperty("providerId")]
        public string ProviderID { get; set; }
 
    }


    public class LoginResponse
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("accessToken")]
        public string AccessToken { get; set; }

        [JsonProperty("expiresIn")]
        public int ExpiresIn { get; set; }

        [JsonProperty("refreshToken")]
        public string RefreshToken { get; set; }

        [JsonProperty("roles")]
        public List<string> Roles { get; set; }

        [JsonProperty("tokenType")]
        public string TokenType { get; set; }

        [JsonProperty("userId")]
        public long UserId { get; set; }

        [JsonProperty("userName")]
        public string Username { get; set; }
    }

    public class ResetPasswordModel
    {
        [JsonProperty("email")]
        [Required]
        //[RegularExpression("[A-Za-z0-9]+@[a-z]+.[a-z]{2,3}", ErrorMessage = "please enter a valid email")]
        public string Email { get; set; }

        [JsonProperty("token")]
        [Required]
        public string Token { get; set; }

        [JsonProperty("password")]
        [Required]
        public string Password { get; set; }
    }


    public class RefreshTokenModel
    {
        [JsonProperty("refreshToken")]
        public string RefreshToken { get; set; }
    }

    public class GetEmail
    {
        [JsonProperty("email")]
        [Required]
        //[RegularExpression("[A-Za-z0-9]+@[a-z]+.[a-z]{2,3}", ErrorMessage = "please enter a valid email")]
        public string Email { get; set; }
    }

    public class ChangePasswordModel
    {
        [JsonProperty("currentPassword")]
        public string CurrentPassword { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }
    }


    public class CountryModel
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("country_code")]
        public string CountryCode { get; set; }

        [JsonProperty("phone_code")]
        public string PhoneCode { get; set; }

        [JsonProperty("is_restricted")]
        public bool IsRestricted { get; set; }
    }

    public class ContactUsModel
    {
        [JsonProperty("name")]
        [Required]
        public string Name { get; set; }

        [JsonProperty("email")]
        [Required]
        //[RegularExpression("[A-Za-z0-9]+@[a-z]+.[a-z]{2,3}", ErrorMessage = "please enter a valid email")]
        public string Email { get; set; }

        [JsonProperty("phoneNumber")]
        [Required]
        public long PhoneNumber { get; set; }

        [JsonProperty("message")]
        [Required]
        public string Message { get; set; }
    }
}
