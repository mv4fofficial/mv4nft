﻿using Newtonsoft.Json;

using System;
using System.Collections.Generic;

namespace MV4F.Models
{
    public class UserGameAssetsViewModel
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("gameAssetId")]
        public long GameAssetId { get; set; }

        [JsonProperty("gameAssets")]
        public GameAssetsViewModel GameAssets { get; set; }

        [JsonProperty("progress")]
        public int Progress { get; set; }

        [JsonProperty("createdDate")]
        public DateTime CreatedDate { get; set; }

        [JsonProperty("updatedDate")]
        public DateTime UpdatedDate { get; set; }
    }

    public class AddUserGameAssetModel
    {
        [JsonProperty("userId")]
        public long UserId { get; set; }

        [JsonProperty("gameAssetId")]
        public long GameAssetId { get; set; }
        
        [JsonProperty("progress")]
        public int Progress { get; set; }

        [JsonProperty("createdDate")]
        public DateTime CreatedDate { get; set; }

        [JsonProperty("updatedDate")]
        public DateTime UpdatedDate { get; set; }
    }
    
    public class ProgressUpdateModel
    {
        [JsonProperty("gameAssetId")]
        public long GameAssetId { get; set; }
        
        [JsonProperty("progress")]
        public int Progress { get; set; }

        [JsonProperty("updatedDate")]
        public DateTime UpdatedDate { get; set; }
    }
}
