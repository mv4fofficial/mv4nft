﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using MV4F;
using MV4F.Entities;
using MV4F.IRepository;
using MV4F.IServices;
using MV4F.Services;
using Newtonsoft.Json.Serialization;
using Stripe;
using System;
using System.Net;
using System.Text;

namespace mvfourf
{
    public class Startup
    {

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<ForwardedHeadersOptions>(options =>
            {
                options.KnownProxies.Add(IPAddress.Parse("97.74.88.226"));
            });

            services.AddHttpClient();
            services.AddControllers().AddNewtonsoftJson(o =>
            {
                o.SerializerSettings.ContractResolver = new DefaultContractResolver();
                o.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                o.SerializerSettings.DateTimeZoneHandling = Newtonsoft.Json.DateTimeZoneHandling.Utc;
                o.SerializerSettings.DateFormatString = "yyyy'-'MM'-'dd'T'HH':'mm':'ssZ";
            });

            //manual declaration of mysql version
            var serverVersion = new MySqlServerVersion(new Version(8, 0, 28));
            //Database Dependencies
            services.AddDbContext<MV4FContext>(
            dbContextOptions => dbContextOptions
                .UseMySql(Configuration.GetConnectionString("DefaultConnection"), serverVersion)
                .EnableSensitiveDataLogging() // <-- These two calls are optional but help
                .EnableDetailedErrors());

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("MV4F",
                    new OpenApiInfo
                    {
                        Title = "MV4F",
                        Version = "Phase 1",
                        Description = "MV4F",
                        TermsOfService = new Uri("https://mv4f.io"),
                        Contact = new OpenApiContact
                        {
                            Name = "Karthik CN",
                            Email = "karthikchiru1995@gmail.com",
                        },
                    }
                 );
                c.AddSecurityDefinition("bearer", new OpenApiSecurityScheme
                {
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "bearer",
                    BearerFormat = "JWT",
                    In = ParameterLocation.Header,
                    Description = "JWT Authorization header using the Bearer scheme.",
                });

                //important check the class presence
                c.OperationFilter<AuthOperationFilter>();
            });

            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

            })
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = Configuration.GetValue<string>("JwtOptions:Issuer"),
                        ValidAudience = Configuration.GetValue<string>("JwtOptions:Audience"),
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration.GetValue<string>("JwtOptions:SecurityKey")))
                    };
                });

            services.AddCors(o => o.AddPolicy("CorsPolicy", builder =>
            {
                builder
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                      .WithOrigins("http://localhost:3000", "http://localhost:4000"
                        , "https://mv4f.io", "http://mv4f.io"
                        , "https://staging.mv4f.io", "http://staging.mv4f.io"
                        , "https://admin.mv4f.io", "http://admin.mv4f.io"
                        , "https://staging-admin.mv4f.io", "http://staging-admin.mv4f.io"
                        , "https://staging-c.mv4f.io"
                        , "http://mv4f.phoenix-dev.online")
                      .SetIsOriginAllowedToAllowWildcardSubdomains()
                    .AllowCredentials();
            }));

            services.AddHttpContextAccessor();

            services.AddRazorPages();

            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            services.AddEndpointsApiExplorer();

            services.AddTransient<IFileExtentionService, FileExtentionService>();
            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddTransient<IEmailService, EmailServices>();
            services.AddTransient<IStorage, Storage>();
            services.AddTransient<ICurrentUser, CurrentUser>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
        }
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IHost hostBuilder)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            //Sends mail to the developer if there is an internal server error along with api name.
            else
            {
                var _exceptionMail = ActivatorUtilities.CreateInstance<EmailServices>(hostBuilder.Services);

                app.UseExceptionHandler(options =>
                {
                    options.Run(
                        async context =>
                        {
                            var ex = context.Features.Get<IExceptionHandlerFeature>();
                            if (ex != null)
                            {
                                _exceptionMail.SendExceptionMail(ex, context);
                            }
                        });
                });
            }



            // Configure the HTTP request pipeline.
            app.UseStatusCodePages("text/plain", "Status code page, status code: {0}");

            app.UseHttpsRedirection();

            app.UseCookiePolicy();

            app.UseRouting();

            app.UseCors("CorsPolicy");

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseDefaultFiles();

            //app.UseStaticFiles(new StaticFileOptions
            //{
            //    FileProvider = new PhysicalFileProvider(Path.Combine
            //    (Directory.GetCurrentDirectory(), "ProductImages")),
            //    RequestPath = "/ProductImages"
            //});

            app.UseStaticFiles();

            app.UseWebSockets();

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();
            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            //The name must be same that is present in the c.swaggerdoc
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("MV4F/swagger.json", "MV4F");
            });

            app.UseEndpoints(endpolongs =>
            {
                endpolongs.MapControllers();
            });

            app.Use(async (context, next) =>
            {
                context.Request.EnableBuffering();
                await next();
            });

            //When app.Use in used, the application will be redirected to this path when the url is called.
            app.Use(async (context, next) =>
            {
                if (context.Request.Path.HasValue && context.Request.Path.Value != "/")
                {
                    context.Response.ContentType = "text/html";
                    await context.Response.SendFileAsync(
                        env.ContentRootFileProvider.GetFileInfo("wwwroot/index.html")
                    );
                    return;
                }
                await next();
            });

            StripeConfiguration.SetApiKey(Configuration["pk_test_51LfETrL95FNDK6SK7fW9V0g3tZQXknOzFNGIXcoFIzsGU7A7aQz0FW4LqMSUw0G7ozyGsWCtX2Glc76lxaMaZ0aC00oijko6UC"]);
        }
    }
}
