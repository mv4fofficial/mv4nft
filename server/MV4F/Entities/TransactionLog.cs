﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MV4F.Entities
{
    [Table("transaction_log")]
    public class TransactionLog
    {
        [Key]
        [Column("id")]
        public long Id { get; set; }

        [Column("user_id")]
        public long UserId { get; set; }

        [ForeignKey(nameof(UserId))]
        [InverseProperty("TransactionLog")]
        public User User { get; set; }

        [Column("seller_name")]
        public string SellerName { get; set; }

        [Column("buyer_name")]
        public string BuyerName { get; set; }

        [Column("product_id")]
        public long ProductId { get; set; }

        [ForeignKey(nameof(ProductId))]
        [InverseProperty("TransactionLog")]
        public Product Product { get; set; }

        [Column("created_date")]
        public DateTime CreatedDate { get; set; }

        [Column("order_id")]
        public long OrderId { get; set; }

        [Column("net_amount", TypeName = "decimal(16,3)")]
        public decimal NetAmount { get; set; }

        [Column("destination")]
        public string Destination { get; set; }

        [Column("due_date")]
        public DateTime DueDate { get; set; }

        [Column("isPaid")]
        public bool IsPaid { get; set; }

        [ForeignKey(nameof(OrderId))]
        [InverseProperty("TransactionLog")]
        public UserOrders UserOrders { get; set; }

        [Column("amount", TypeName = "decimal(16,3)")]
        public decimal Amount { get; set; }

        [Column("order_status")]
        public string OrderStatus { get; set; }

    }
}
