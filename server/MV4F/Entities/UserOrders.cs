﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MV4F.Entities
{
    [Table("user_orders")]
    public class UserOrders
    {
        public UserOrders()
        {
            UserProduct = new HashSet<UserProduct>();
            UserCart = new HashSet<UserCart>();
            TransactionLog = new HashSet<TransactionLog>();
            PaymentLedger = new HashSet<PaymentLedger>();
        }

        [Key]
        [Column("id")]
        public long Id { get; set; }

        [Column("user_id")]
        public long UserId { get; set; }

        [ForeignKey(nameof(UserId))]
        [InverseProperty("UserOrders")]
        public User User { get; set; }

        [Column("amount", TypeName = "decimal(16,3)")]
        public decimal Amount { get; set; }

        [Column("updated_date")]
        public DateTime UpdatedDate { get; set; }

        [Column("created_date")]
        public DateTime CreatedDate { get; set; }

        [Column("order_status")]
        public string OrderStatus { get; set; }

        [InverseProperty("UserOrders")]
        public virtual ICollection<UserProduct> UserProduct { get; set; }

        [InverseProperty("UserOrders")]
        public virtual ICollection<UserCart> UserCart { get; set; }

        [InverseProperty("UserOrders")]
        public virtual ICollection<TransactionLog> TransactionLog { get; set; }

        [InverseProperty("UserOrders")]
        public virtual ICollection<PaymentLedger> PaymentLedger { get; set; }

    }
}
