﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MV4F.Entities
{
    [Table("puzzle_detail")]
    public class PuzzleDetail
	{
        [Key]
        [Column("id")]
        public long Id { get; set; }

        //[Column("puzzle_piece_list")]
        //public List<PuzzlePieceData> PuzzlePieceList { get; set; }

        //[Column("puzzle_piece_group_list")]
        //public List<PuzzlePieceGroupData> PuzzlePieceGroupList { get; set; }

        //[Column("unsolved_piece_id_list")]
        //public List<int> UnsolvedPieceIdList { get; set; }

        //[Column("moved_pieces_id_list")]
        //public List<int> MovedPieceIdList { get; set; }

        [Column("remaining_hints")]
        public int RemainingHints { get; set; }

        [Column("timer")]
        public int Timer { get; set; }

        [Column("elapsed_time")]
        public int ElapsedTime { get; set; }

        [Column("created_date")]
        public DateTime CreatedDate { get; set; }

        [Column("updated_date")]
        public DateTime UpdatedDate { get; set; }

        [InverseProperty("PuzzleDetail")]
        public UserPuzzleDetail UserPuzzleDetail { get; set; }
	}
}