﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MV4F.Entities
{
    [Table("become_seller_question_master")]
    public class BecomeSellerQuestionsMaster
    {
        public BecomeSellerQuestionsMaster()
        {
            SellerAnswer = new HashSet<SellerAnswer>();
        }

        [Key]
        [Column("id")]
        public long Id { get; set; }

        [Column("question")]
        public string Question { get; set; }

        [InverseProperty("BecomeSellerQuestionsMaster")]
        public virtual ICollection<SellerAnswer> SellerAnswer { get; set; }

        [Column("created_date")]
        public DateTime CreatedDate { get; set; }
    }
}
