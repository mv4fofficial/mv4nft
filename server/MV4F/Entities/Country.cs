﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MV4F.Entities
{
    [Table("country")]
    public class Country
    {
        public Country()
        {
            User = new HashSet<User>();
        }
        [Key]
        [Column("id")]
        public long Id { get; set; }

        [Column("name")]
        [StringLength(150)]
        public string Name { get; set; }

        [Column("country_code")]
        [StringLength(10)]
        public string CountryCode { get; set; }

        [Column("phone_code")]
        [StringLength(10)]
        public string PhoneCode { get; set; }

        [Column("is_restricted")]
        public bool IsRestricted { get; set; }

        [InverseProperty("Country")]

        public virtual ICollection<User> User { get; set; }
    }
}
