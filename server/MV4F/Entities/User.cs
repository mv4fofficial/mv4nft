﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MV4F.Entities
{
    [Table("user")]
    public class User
    {
        public User()
        {
            UserRole = new HashSet<UserRole>();
            UserRequest = new HashSet<UserRequest>();
            RefreshToken = new HashSet<RefreshToken>();
            UserOrders = new HashSet<UserOrders>();
            Product = new HashSet<Product>();
            ProductWishList = new HashSet<ProductWishList>();
            UserProduct = new HashSet<UserProduct>();
            UserCart = new HashSet<UserCart>();
            TransactionLog = new HashSet<TransactionLog>();
            PaymentLedger = new HashSet<PaymentLedger>();
            Addresses = new HashSet<Addresses>();
            InterestedCategories = new HashSet<InterestedCategories>();
            SellerAnswer = new HashSet<SellerAnswer>();
            WishList = new HashSet<WishList>();
            WishListCategoriesAndSubCategories = new HashSet<WishListCategoriesAndSubCategories>();
            Notification = new HashSet<Notifications>();
            Notifications = new HashSet<Notifications>();
        }

        [Key]
        [Column("id")]
        public long Id { get; set; }

        [Column("first_name")]
        public string FirstName { get; set; }

        [Column("last_name")]
        public string LastName { get; set; }

        [Column("profile_picture")]
        public string? ProfilePicture { get; set; }

        [Column("background_picture")]
        public string? BackgroundPicture { get; set; }

        [Column("description")]
        public string? Description { get; set; }

        [Column("contact_number")]
        public long? ContactNumber { get; set; }

        [Column("email_Id")]
        public string EmailId { get; set; }

        [Column("date_of_birth")]
        public DateTime? DateOfBirth { get; set; }

        [Column("bio")]
        public string Bio { get; set; }

        [Column("wallet")]
        public string Wallet { get; set; }

        [Column("password")]
        public string Password { get; set; }

        [Column("created_date")]
        public DateTime CreatedDate { get; set; }

        [Column("updated_date")]
        public DateTime UpdatedDate { get; set; }

        [Column("is_active")]
        public bool IsActive { get; set; }

        [Column("country_id")]
        public long? CountryId { get; set; }

        [ForeignKey(nameof(CountryId))]
        [InverseProperty("User")]

        public Country Country { get; set; }

        [Column("is_email_verified")]
        public bool IsEmailVerified { get; set; }

        [Column("uuid")]
        public string UUID { get; set; }

        [Column("stripe_id")]
        public string StripeId { get; set; }

        [Column("stripe_status")]
        public bool StripeStatus { get; set; }

        [Column("followee_count")]
        public long FolloweeCount { get; set; }

        [Column("follower_count")]
        public long FollowerCount { get; set; }

        [InverseProperty("User")]
        public virtual ICollection<UserRole> UserRole { get; set; }

        [InverseProperty("User")]
        public virtual ICollection<RefreshToken> RefreshToken { get; set; }
        [InverseProperty("User")]
        public virtual ICollection<ProductWishList> ProductWishList { get; set; }
        [InverseProperty("User")]
        public virtual ICollection<UserRequest> UserRequest { get; set; }

        [InverseProperty("Seller")]
        public virtual ICollection<Product> Product { get; set; }

        [InverseProperty("User")]
        public virtual ICollection<UserOrders> UserOrders { get; set; }

        [InverseProperty("User")]
        public virtual ICollection<UserProduct> UserProduct { get; set; }

        [InverseProperty("User")]
        public virtual ICollection<UserCart> UserCart { get; set; }

        [InverseProperty("User")]
        public virtual ICollection<TransactionLog> TransactionLog { get; set; }

        [InverseProperty("User")]
        public virtual ICollection<PaymentLedger> PaymentLedger { get; set; }

        [InverseProperty("User")]
        public virtual ICollection<Addresses> Addresses { get; set; }

        [InverseProperty("User")]
        public virtual ICollection<InterestedCategories> InterestedCategories { get; set; }

        [InverseProperty("User")]
        public virtual ICollection<SellerAnswer> SellerAnswer { get; set; }

        [InverseProperty("User")]
        public virtual ICollection<WishList> WishList { get; set; }

        [InverseProperty("User")]
        public virtual ICollection<WishListCategoriesAndSubCategories> WishListCategoriesAndSubCategories { get; set; }

        [InverseProperty("User")]
        public virtual ICollection<Notifications> Notification { get; set; }

        [InverseProperty("Seller")]
        public virtual ICollection<Notifications> Notifications { get; set; }

        [InverseProperty("User")]
        public PlayerScore PlayerScore { get; set; }

        [InverseProperty("User")]
        public virtual ICollection<UserFollow> UserFollow { get; set; }

    }
}
