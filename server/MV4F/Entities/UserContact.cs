﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MV4F.Entities
{

    [Table("user_contact")]
    public class UserContact
    {

        [Key]
        [Column("id")]
        public long Id { get; set; }

        [Column("name")]
        public string Name { get; set; }


        [Column("contact_number")]
        public long ContactNumber { get; set; }

        [Column("email_Id")]
        public string EmailId { get; set; }


        [Column("comments")]
        public string Comments { get; set; }

        [Column("created_date")]
        public DateTime CreatedDate { get; set; }

    }
}
