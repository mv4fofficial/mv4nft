﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MV4F.Entities
{
    [Table("wish_list_categories_and_subcategories")]
    public class WishListCategoriesAndSubCategories
    {
        [Key]
        [Column("id")]
        public long Id { get; set; }

        [Column("wish_list_id")]
        public long WishListId { get; set; }

        [ForeignKey(nameof(WishListId))]
        [InverseProperty("WishListCategoriesAndSubCategories")]
        public WishList WishList { get; set; }

        [Column("user_id")]
        public long UserId { get; set; }

        [ForeignKey(nameof(UserId))]
        [InverseProperty("WishListCategoriesAndSubCategories")]
        public User User { get; set; }

        [Column("category_id")]
        public long? CategoryId { get; set; }

        [ForeignKey(nameof(CategoryId))]
        [InverseProperty("WishListCategoriesAndSubCategories")]
        public CategoryMaster CategoryMaster { get; set; }

        [Column("subcategory_id")]
        public long? SubCategoryId { get; set; }

        [ForeignKey(nameof(SubCategoryId))]
        [InverseProperty("WishListCategoriesAndSubCategories")]
        public SubCategoryMaster SubCategoryMaster { get; set; }

        [Column("created_date")]
        public DateTime CreatedDate { get; set; }
    }
}
