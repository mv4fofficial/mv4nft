﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MV4F.Entities
{
    [Table("category_master")]
    public class CategoryMaster
    {
        public CategoryMaster()
        {
            ProductCategories = new HashSet<ProductCategories>();
            InterestedCategories = new HashSet<InterestedCategories>();
            WishListCategoriesAndSubCategories = new HashSet<WishListCategoriesAndSubCategories>();
        }

        [Key]
        [Column("id")]
        public long Id { get; set; }

        [Column("name")]
        [StringLength(150)]
        public string Name { get; set; }

        [Column("category_picture")]
        public string CategoryPicture { get; set; }

        [Column("is_deleted")]
        public bool IsDeleted { get; set; }

        [Column("created_date")]
        public DateTime CreatedDate { get; set; }

        [Column("updated_date")]
        public DateTime UpdatedDate { get; set; }

        [InverseProperty("CategoryMaster")]
        public virtual ICollection<ProductCategories> ProductCategories { get; set; }

        [InverseProperty("CategoryMaster")]
        public virtual ICollection<InterestedCategories> InterestedCategories { get; set; }

        [InverseProperty("CategoryMaster")]
        public virtual ICollection<WishListCategoriesAndSubCategories> WishListCategoriesAndSubCategories { get; set; }
    }
}
