﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MV4F.Entities
{
    [Table("interested_categories")]
    public class InterestedCategories
    {
        [Key]
        [Column("id")]
        public long Id { get; set; }

        [Column("user_id")]
        public long UserId { get; set; }

        [ForeignKey(nameof(UserId))]
        [InverseProperty("InterestedCategories")]
        public User User { get; set; }

        [Column("category_id")]
        public long? CategoryId { get; set; }

        [ForeignKey(nameof(CategoryId))]
        [InverseProperty("InterestedCategories")]
        public CategoryMaster CategoryMaster { get; set; }

        [Column("sub_category_id")]
        public long? SubCategoryId { get; set; }

        [ForeignKey(nameof(SubCategoryId))]
        [InverseProperty("InterestedCategories")]
        public SubCategoryMaster SubCategoryMaster { get; set; }

        [Column("created_date")]
        public DateTime CreatedDate { get; set; }
    }
}
