﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MV4F.Entities
{
    [Table("seller_answer")]
    public class SellerAnswer
    {
        [Key]
        [Column("id")]
        public long Id { get; set; }

        [Column("user_id")]
        public long UserId { get; set; }

        [ForeignKey(nameof(UserId))]
        [InverseProperty("SellerAnswer")]
        public User User { get; set; }

        [Column("question_id")]
        public long QuestionId { get; set; }

        [ForeignKey(nameof(QuestionId))]
        [InverseProperty("SellerAnswer")]
        public BecomeSellerQuestionsMaster BecomeSellerQuestionsMaster { get; set; }

        [Column("answer")]
        public string Answer { get; set; }

        [Column("created_date")]
        public DateTime CreatedDate { get; set; }
    }
}
