﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MV4F.Entities
{
    [Table("tags_master")]
    public class TagsMaster
    {
        public TagsMaster()
        {
            ProductTags = new HashSet<ProductTags>();
        }

        [Key]
        [Column("id")]
        public long Id { get; set; }

        [Column("tag_name")]
        public string TagName { get; set; }

        [InverseProperty("TagsMaster")]
        public virtual ICollection<ProductTags> ProductTags { get; set; }

        [Column("created_date")]
        public DateTime CreatedDate { get; set; }

        [Column("updated_date")]
        public DateTime UpdatedDate { get; set; }
    }
}
