﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MV4F.Entities
{
    [Table("product_tags")]
    public class ProductTags
    {
        [Key]
        [Column("id")]
        public long Id { get; set; }

        [Column("product_id")]
        public long ProductId { get; set; }

        [ForeignKey(nameof(ProductId))]
        [InverseProperty("ProductTags")]
        public Product Product { get; set; }


        [Column("tag_id")]
        public long TagId { get; set; }

        [ForeignKey(nameof(TagId))]
        [InverseProperty("ProductTags")]
        public TagsMaster TagsMaster { get; set; }

        [Column("created_date")]
        public DateTime CreatedDate { get; set; }

        [Column("updated_date")]
        public DateTime UpdatedDate { get; set; }
    }
}
