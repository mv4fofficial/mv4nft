﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MV4F.Entities
{
    [Table("product")]
    public class Product
    {
        public Product()
        {
            UserProduct = new HashSet<UserProduct>();
            ProductWishList = new HashSet<ProductWishList>();
            UserCart = new HashSet<UserCart>();
            TransactionLog = new HashSet<TransactionLog>();
            ProductCategories = new HashSet<ProductCategories>();
            ProductSubCategories = new HashSet<ProductSubCategories>();
            ProductTags = new HashSet<ProductTags>();
            Notification = new HashSet<Notifications>();
        }

        [Key]
        [Column("id")]
        public long Id { get; set; }

        [Column("product_title")]
        public string ProductTitle { get; set; }


        [Column("price", TypeName = "decimal(16,3)")]
        public decimal Price { get; set; }

        [Column("gas_fee", TypeName = "decimal(16,3)")]
        public decimal? GasFee { get; set; }

        [Column("product_image")]
        public string ProductImage { get; set; }

        [Column("description")]
        public string Description { get; set; }

        [Column("rarity")]
        public string Rarity { get; set; }

        [Column("quantity")]
        public long? Quantity { get; set; }

        [Column("link")]
        public string Link { get; set; }

        [Column("nft_status")]
        public bool NFTStatus { get; set; }

        [Column("for_sale")]
        public bool ForSale { get; set; }

        [Column("seller_id")]
        public long SellerId { get; set; }

        [ForeignKey(nameof(SellerId))]
        [InverseProperty("Product")]
        public User Seller { get; set; }

        [Column("is_deleted")]
        public bool IsDeleted { get; set; }

        [Column("created_date")]
        public DateTime CreatedDate { get; set; }

        [Column("updated_date")]
        public DateTime UpdatedDate { get; set; }

        [InverseProperty("Product")]
        public virtual ICollection<UserProduct> UserProduct { get; set; }
        [InverseProperty("Product")]
        public virtual ICollection<ProductImages> ProductImages { get; set; }
        [InverseProperty("Product")]
        public virtual ICollection<ProductWishList> ProductWishList { get; set; }

        [InverseProperty("Product")]
        public virtual ICollection<UserCart> UserCart { get; set; }

        [InverseProperty("Product")]
        public virtual ICollection<TransactionLog> TransactionLog { get; set; }

        [InverseProperty("Product")]
        public virtual ICollection<ProductCategories> ProductCategories { get; set; }

        [InverseProperty("Product")]
        public virtual ICollection<ProductSubCategories> ProductSubCategories { get; set; }

        [InverseProperty("Product")]
        public virtual ICollection<ProductTags> ProductTags { get; set; }

        [InverseProperty("Product")]
        public virtual ICollection<Notifications> Notification { get; set; }
    }
}
