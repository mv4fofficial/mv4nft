﻿using Microsoft.EntityFrameworkCore;
using System;
using System.IO; 
using Newtonsoft.Json;
using System.Collections.Generic;

namespace MV4F.Entities
{
    public class MV4FContext : DbContext
    {



        public MV4FContext(DbContextOptions<MV4FContext> options)
            : base(options)
        {
        }

        public virtual DbSet<User> User { get; set; }

        public virtual DbSet<Role> Role { get; set; }

        public virtual DbSet<UserRequest> UserRequest { get; set; }
        
        public virtual DbSet<UserRole> UserRole { get; set; }

        public virtual DbSet<RefreshToken> RefreshToken { get; set; }

        public virtual DbSet<Country> Country { get; set; }

        public virtual DbSet<PasswordResetToken> PasswordResetToken { get; set; }

        public virtual DbSet<CategoryMaster> CategoryMaster { get; set; }

        public virtual DbSet<UserContact> UserContact { get; set; }

        public virtual DbSet<UserFollow> UserFollow { get; set; }

        public virtual DbSet<Product> Product { get; set; }
        
        public virtual DbSet<ProductImages> ProductImages { get; set; }

        public virtual DbSet<UserOrders> UserOrders { get; set; }

        public virtual DbSet<UserCart> UserCart { get; set; }

        public virtual DbSet<UserProduct> UserProduct { get; set; }

        public virtual DbSet<ProductWishList> ProductWishList { get; set; }

        public virtual DbSet<TransactionLog> TransactionLog { get; set; }

        public virtual DbSet<PaymentLedger> PaymentLedger { get; set; }

        public virtual DbSet<ProductCategories> ProductCategories { get; set; }

        public virtual DbSet<SubCategoryMaster> SubCategoryMasters { get; set; }

        public virtual DbSet<ProductSubCategories> ProductSubCategories { get; set; }

        public virtual DbSet<TagsMaster> TagsMaster { get; set; }

        public virtual DbSet<ProductTags> ProductTags { get; set; }

        public virtual DbSet<Addresses> Addresses { get; set; }

        public virtual DbSet<InterestedCategories> InterestedCategories { get; set; }

        public virtual DbSet<BecomeSellerQuestionsMaster> BecomeSellerQuestionsMasters { get; set; }

        public virtual DbSet<SellerAnswer> SellerAnswers { get; set; }

        public virtual DbSet<WishList> WishList { get; set; }

        public virtual DbSet<WishListCategoriesAndSubCategories> WishListCategoriesAndSubCategories { get; set; }

        public virtual DbSet<Notifications> Notifications { get; set; }

        public virtual DbSet<GameAssets> GameAssets { get; set; }

        public virtual DbSet<UserGameAssets> UserGameAssets { get; set; }

        public virtual DbSet<PuzzleDetail> PuzzleDetail { get; set; }
        
        public virtual DbSet<UserPuzzleDetail> UserPuzzleDetail { get; set; }

        public virtual DbSet<PlayerScore> PlayerScore { get; set; }

        public virtual DbSet<PuzzleCategories> PuzzleCategory { get; set; }

        public virtual DbSet<GameAssetCategories> GameAssetCategory { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                var serverVersion = new MySqlServerVersion(new Version(8, 0, 25));
                optionsBuilder.UseMySql("ConnectionStrings : DefaultConnection", serverVersion);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            modelBuilder.Entity<Role>().HasData(
                new Role {
                    Id = 1,
                    RoleName = "Admin",
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow,
                },
                new Role {
                    Id = 2,
                    RoleName = "Seller",
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow,
                },
                new Role {
                    Id = 3,
                    RoleName = "Buyer",
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow,
                }
            );
            // modelBuilder.Entity<Country>()
            //             .Property(f => f.Id)
            //             .ValueGeneratedOnAdd();
            modelBuilder.Entity<Country>().HasData(
                SeedCountryData()
            );


        }

        public List<Country> SeedCountryData()
            {
                var country_data= new List<Country>();
                string filePath = System.IO.Path.GetFullPath("countries.json");
                Console.WriteLine(filePath);
                using (StreamReader r = new StreamReader(filePath))
                {
                    string json = r.ReadToEnd();
                    country_data= JsonConvert.DeserializeObject<List<Country>>(json);
                }
                Console.Write(country_data[0]);
                return country_data;
            }
        }
}
