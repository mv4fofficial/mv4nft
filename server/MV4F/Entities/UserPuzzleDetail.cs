﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MV4F.Entities
{
    [Table("user_puzzle_detail")]
    public class UserPuzzleDetail
    {
        [Key]
        [Column("id")]
        public long Id { get; set; }

        [Column("user_id")]
        public long UserId { get; set; }

        [Column("puzzle_id")]
        public long PuzzleId { get; set; }

        [ForeignKey(nameof(PuzzleId))]
        [InverseProperty("UserPuzzleDetail")]
        public PuzzleDetail PuzzleDetail { get; set; }

        [Column("updated_date")]
        public DateTime UpdatedDate { get; set; }

        [Column("created_date")]
        public DateTime CreatedDate { get; set; }
    }
}
