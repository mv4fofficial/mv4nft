﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MV4F.Entities
{
    [Table("payment_ledger")]
    public class PaymentLedger
    {
        [Key]
        [Column("id")]
        public long Id { get; set; }

        [Column("user_id")]
        public long UserId { get; set; }

        [ForeignKey(nameof(UserId))]
        [InverseProperty("PaymentLedger")]
        public User User { get; set; }

        [Column("amount", TypeName = "decimal(16,3)")]
        public decimal Amount { get; set; }

        [Column("transaction_status")]
        public string TransactionStatus { get; set; }

        [Column("created_date")]
        public DateTime CreatedDate { get; set; }

        [Column("updated_date")]
        public DateTime UpdatedDate { get; set; }

        [Column("order_id")]
        public long OrderId { get; set; }

        [ForeignKey(nameof(OrderId))]
        [InverseProperty("PaymentLedger")]
        public UserOrders UserOrders { get; set; }
    }
}
