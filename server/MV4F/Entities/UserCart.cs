﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MV4F.Entities
{
    [Table("user_cart")]
    public class UserCart
    {
        [Key]
        [Column("id")]
        public long Id { get; set; }

        [Column("user_id")]
        public long UserId { get; set; }

        [ForeignKey(nameof(UserId))]
        [InverseProperty("UserCart")]
        public User User { get; set; }

        [Column("product_id")]
        public long ProductId { get; set; }

        [ForeignKey(nameof(ProductId))]
        [InverseProperty("UserCart")]
        public Product Product { get; set; }

        [Column("order_status")]
        public string OrderStatus { get; set; }

        [Column("created_date")]
        public DateTime CreatedDate { get; set; }

        [Column("updated_date")]
        public DateTime UpdatedDate { get; set; }

        [Column("order_id")]
        public long? OrderId { get; set; }

        [ForeignKey(nameof(OrderId))]
        [InverseProperty("UserCart")]
        public UserOrders UserOrders { get; set; }
    }
}
