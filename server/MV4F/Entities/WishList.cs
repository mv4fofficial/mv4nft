﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MV4F.Entities
{
    [Table("wish_list")]
    public class WishList
    {
        public WishList()
        {
            WishListCategoriesAndSubCategories = new HashSet<WishListCategoriesAndSubCategories>();
            Notification = new HashSet<Notifications>();
        }

        [Key]
        [Column("id")]
        public long Id { get; set; }

        [Column("user_id")]
        public long UserId { get; set; }

        [ForeignKey(nameof(UserId))]
        [InverseProperty("WishList")]
        public User User { get; set; }

        [Column("description")]
        public string Description { get; set; }

        [Column("created_date")]
        public DateTime CreatedDate { get; set; }

        [Column("updated_date")]
        public DateTime UpdatedDate { get; set; }

        [InverseProperty("WishList")]
        public virtual ICollection<WishListCategoriesAndSubCategories> WishListCategoriesAndSubCategories { get; set; }

        [InverseProperty("WishList")]
        public virtual ICollection<Notifications> Notification { get; set; }
    }
}
