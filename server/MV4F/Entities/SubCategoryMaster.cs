﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MV4F.Entities
{
    [Table("sub_category_master")]
    public class SubCategoryMaster
    {
        public SubCategoryMaster()
        {
            ProductSubCategories = new HashSet<ProductSubCategories>();
            InterestedCategories = new HashSet<InterestedCategories>();
            WishListCategoriesAndSubCategories = new List<WishListCategoriesAndSubCategories>();
        }

        [Key]
        [Column("id")]
        public long Id { get; set; }

        [Column("sub_category_name")]
        public string SubCategoryName { get; set; }

        [Column("sub_category_picture")]
        public string SubCategoryPicture { get; set; }

        [Column("category_id")]
        public long CategoryId { get; set; }        

        [InverseProperty("SubCategoryMaster")]
        public virtual ICollection<ProductSubCategories> ProductSubCategories { get; set; }

        [Column("is_deleted")]
        public bool IsDeleted { get; set; }

        [Column("created_date")]
        public DateTime CreatedDate { get; set; }

        [Column("updated_date")]
        public DateTime UpdatedDate { get; set; }

        [InverseProperty("SubCategoryMaster")]
        public virtual ICollection<InterestedCategories> InterestedCategories { get; set; }

        [InverseProperty("SubCategoryMaster")]
        public virtual ICollection<WishListCategoriesAndSubCategories> WishListCategoriesAndSubCategories { get; set; }
    }
}
