﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MV4F.Entities
{
    [Table("user_request")]
    public class UserRequest
    {
        [Key]
        [Column("id")]
        public long Id { get; set; }

        [Column("user_id")]
        public long UserId { get; set; }

        [ForeignKey(nameof(UserId))]
        [InverseProperty("UserRequest")]
        public User User { get; set; }

        [Column("requested_role_id")]
        public long? RequestedRoleId { get; set; }

        [Column("request_type")]
        public string RequestType  { get; set; }

        [Column("description")]
        public string Description  { get; set; }

        [Column("status")]
        public string Status { get; set; }

        [Column("attach_file_url")]
        public string AttachFileUrl { get; set; }

        [Column("created_date")]
        public DateTime CreatedDate { get; set; }

        [Column("updated_date")]
        public DateTime UpdatedDate { get; set; }
    }
}
