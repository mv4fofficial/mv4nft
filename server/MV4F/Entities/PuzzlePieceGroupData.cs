﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MV4F.Entities
{
    [Table("PuzzlePieceGroupData")]
    public class PuzzlePieceGroupData
    {
        [Key]
        [Column("id")]
        public long Id { get; set; }

        [Column("piece_name_list")]
        public List<string> PieceNameList { get; set; }

    }
}
