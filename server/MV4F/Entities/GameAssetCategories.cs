using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MV4F.Entities
{
    [Table("game-asset-categories")]
    public class GameAssetCategories
    {
        [Key]
        [Column("id")]
        public long Id { get; set; }

        [Column("category_id")]
        public long CategoryId { get; set; }

        //[InverseProperty("GameAssetCategories")]
        [Column("puzzle_categories")]
        public PuzzleCategories PuzzleCategories { get; set; }

        [Column("game_asset_id")]
        public long GameAssetId { get; set; }

        //[InverseProperty("GameAssetCategories")]
        [Column("game-assets")]
        public GameAssets GameAssets { get; set; }

        [Column("created_date")]
        public DateTime CreatedDate { get; set; }

        [Column("updated_date")]
        public DateTime UpdatedDate { get; set; }
    }
}
