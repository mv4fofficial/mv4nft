using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MV4F.Entities
{
    [Table("game_assets")]
    public class GameAssets
    {
        [Key]
        [Column("id")]
        public long Id { get; set; }
        
        [Column("name")]
        public string Name { get; set; }
        
        [Column("type")]
        public string Type { get; set; }

        [Column("image")]
        public string Image { get; set; }

        [Column("coin_price")]
        public long CoinPrice { get; set; }

        [Column("diamond_price")]
        public long DiamondPrice { get; set; }

        [Column("created_date")]
        public DateTime CreatedDate { get; set; }

        [Column("updated_date")]
        public DateTime UpdatedDate { get; set; }

        [InverseProperty("GameAssets")]
        public UserGameAssets UserGameAssets { get; set; }

        //[InverseProperty("GameAssets")]
        //public GameAssetCategories GameAssetCategories { get; set; }
    }
}
