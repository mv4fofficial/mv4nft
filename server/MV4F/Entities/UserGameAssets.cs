﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MV4F.Entities
{
	[Table("user_game_assets")]
	public class UserGameAssets
	{
		[Key]
		[Column("id")]
		public long Id { get; set; }

		[Column("user_id")]
		public long UserId { get; set; }

		[Column("game_asset_id")]
		public long GameAssetId { get; set; }

		[ForeignKey(nameof(GameAssetId))]
        [InverseProperty("UserGameAssets")]
        public GameAssets GameAssets { get; set; }

		[Column("progress")]
		public int Progress { get; set; }

		[Column("updated_date")]
        public DateTime UpdatedDate { get; set; }

        [Column("created_date")]
        public DateTime CreatedDate { get; set; }
	}
}