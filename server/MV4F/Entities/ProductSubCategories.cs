﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MV4F.Entities
{
    [Table("product_sub_categories")]
    public class ProductSubCategories
    {
        [Key]
        [Column("id")]
        public long Id { get; set; }

        [Column("product_id")]
        public long ProductId { get; set; }

        [ForeignKey(nameof(ProductId))]
        [InverseProperty("ProductSubCategories")]
        public Product Product { get; set; }

        [Column("sub_category_id")]
        public long SubCategoryId { get; set; }

        [ForeignKey(nameof(SubCategoryId))]
        [InverseProperty("ProductSubCategories")]
        public SubCategoryMaster SubCategoryMaster { get; set; }

        [Column("created_date")]
        public DateTime CreatedDate { get; set; }

        [Column("updated_date")]
        public DateTime UpdatedDate { get; set; }
    }
}
