﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MV4F.Entities
{
    [Table("notification")]
    public class Notifications
    {
        [Key]
        [Column("id")]
        public long Id { get; set; }

        [Column("user_id")]
        public long UserId { get; set; }

        [ForeignKey(nameof(UserId))]
        [InverseProperty("Notification")]
        public User User { get; set; }

        [Column("wish_list_id")]
        public long? WishListId { get; set; }

        [ForeignKey(nameof(WishListId))]
        [InverseProperty("Notification")]
        public WishList WishList { get; set; }

        [Column("seller_id")]
        public long SellerId { get; set; }

        [ForeignKey(nameof(SellerId))]
        [InverseProperty("Notifications")]
        public User Seller { get; set; }

        [Column("product_id")]
        public long? ProductId { get; set; }

        [ForeignKey(nameof(ProductId))]
        [InverseProperty("Notification")]
        public Product Product { get; set; }

        [Column("is_read")]
        public bool IsRead { get; set; }

        [Column("is_seller")]
        public bool IsSeller { get; set; }

        [Column("created_date")]
        public DateTime CreatedDate { get; set; }

        [Column("updated_date")]
        public DateTime UpdatedDate { get; set; }

    }
}
