using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MV4F.Entities
{
    [Table("user_follow")]
    public class UserFollow
    {

        [Key]
        [Column("id")]
        public long Id { get; set; }

        [Column("follower_Id")]
        public long FollowerId { get; set; }

        [Column("followee_Id")]
        public long FolloweeId { get; set; }

        [ForeignKey(nameof(FolloweeId))]
        [InverseProperty("UserFollow")]
        public User User { get; set; }

        [Column("email_status")]
        public bool EmailStatus { get; set; }

        [Column("delete_flag")]
        public bool DeleteFlag { get; set; }

        [Column("created_date")]
        public DateTime CreatedDate { get; set; }

    }
}
