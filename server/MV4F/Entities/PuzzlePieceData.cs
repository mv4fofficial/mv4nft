﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MV4F.Entities
{
    public class PuzzlePieceData
    {
        [Key]
        [Column("id")]
        public long Id { get; set; }

        [Column("name")]
        public string Name { get; set; }

        //public Vector3 position;
        //public Vector3 scale;

        [Column("rotation_z")]
        public float RotationZ { get; set; }
    }
}
