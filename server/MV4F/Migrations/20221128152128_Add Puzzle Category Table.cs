﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MV4F.Migrations
{
    public partial class AddPuzzleCategoryTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "puzzle-categories",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    image = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    created_date = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    updated_date = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_puzzle-categories", x => x.id);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 11, 28, 15, 21, 28, 146, DateTimeKind.Utc).AddTicks(873), new DateTime(2022, 11, 28, 15, 21, 28, 146, DateTimeKind.Utc).AddTicks(876) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 11, 28, 15, 21, 28, 146, DateTimeKind.Utc).AddTicks(877), new DateTime(2022, 11, 28, 15, 21, 28, 146, DateTimeKind.Utc).AddTicks(877) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 11, 28, 15, 21, 28, 146, DateTimeKind.Utc).AddTicks(878), new DateTime(2022, 11, 28, 15, 21, 28, 146, DateTimeKind.Utc).AddTicks(879) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "puzzle-categories");

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 11, 24, 5, 22, 2, 636, DateTimeKind.Utc).AddTicks(8352), new DateTime(2022, 11, 24, 5, 22, 2, 636, DateTimeKind.Utc).AddTicks(8355) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 11, 24, 5, 22, 2, 636, DateTimeKind.Utc).AddTicks(8357), new DateTime(2022, 11, 24, 5, 22, 2, 636, DateTimeKind.Utc).AddTicks(8357) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 11, 24, 5, 22, 2, 636, DateTimeKind.Utc).AddTicks(8358), new DateTime(2022, 11, 24, 5, 22, 2, 636, DateTimeKind.Utc).AddTicks(8358) });
        }
    }
}
