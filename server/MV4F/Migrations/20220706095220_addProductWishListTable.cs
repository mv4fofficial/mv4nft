﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MV4F.Migrations
{
    public partial class addProductWishListTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "product_wishlist",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    user_id = table.Column<long>(type: "bigint", nullable: false),
                    product_id = table.Column<long>(type: "bigint", nullable: false),
                    created_date = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    updated_date = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_product_wishlist", x => x.id);
                    table.ForeignKey(
                        name: "FK_product_wishlist_product_product_id",
                        column: x => x.product_id,
                        principalTable: "product",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_product_wishlist_user_user_id",
                        column: x => x.user_id,
                        principalTable: "user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 7, 6, 9, 52, 19, 828, DateTimeKind.Utc).AddTicks(6011), new DateTime(2022, 7, 6, 9, 52, 19, 828, DateTimeKind.Utc).AddTicks(6012) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 7, 6, 9, 52, 19, 828, DateTimeKind.Utc).AddTicks(6013), new DateTime(2022, 7, 6, 9, 52, 19, 828, DateTimeKind.Utc).AddTicks(6013) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 7, 6, 9, 52, 19, 828, DateTimeKind.Utc).AddTicks(6014), new DateTime(2022, 7, 6, 9, 52, 19, 828, DateTimeKind.Utc).AddTicks(6015) });

            migrationBuilder.CreateIndex(
                name: "IX_product_wishlist_product_id",
                table: "product_wishlist",
                column: "product_id");

            migrationBuilder.CreateIndex(
                name: "IX_product_wishlist_user_id",
                table: "product_wishlist",
                column: "user_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "product_wishlist");

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 7, 6, 8, 24, 58, 181, DateTimeKind.Utc).AddTicks(9414), new DateTime(2022, 7, 6, 8, 24, 58, 181, DateTimeKind.Utc).AddTicks(9415) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 7, 6, 8, 24, 58, 181, DateTimeKind.Utc).AddTicks(9416), new DateTime(2022, 7, 6, 8, 24, 58, 181, DateTimeKind.Utc).AddTicks(9417) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 7, 6, 8, 24, 58, 181, DateTimeKind.Utc).AddTicks(9417), new DateTime(2022, 7, 6, 8, 24, 58, 181, DateTimeKind.Utc).AddTicks(9418) });
        }
    }
}
