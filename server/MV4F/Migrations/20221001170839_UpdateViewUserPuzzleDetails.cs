﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MV4F.Migrations
{
    public partial class UpdateViewUserPuzzleDetails : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 1, 17, 8, 39, 173, DateTimeKind.Utc).AddTicks(4717), new DateTime(2022, 10, 1, 17, 8, 39, 173, DateTimeKind.Utc).AddTicks(4720) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 1, 17, 8, 39, 173, DateTimeKind.Utc).AddTicks(4722), new DateTime(2022, 10, 1, 17, 8, 39, 173, DateTimeKind.Utc).AddTicks(4722) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 1, 17, 8, 39, 173, DateTimeKind.Utc).AddTicks(4723), new DateTime(2022, 10, 1, 17, 8, 39, 173, DateTimeKind.Utc).AddTicks(4723) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 1, 16, 18, 34, 178, DateTimeKind.Utc).AddTicks(3315), new DateTime(2022, 10, 1, 16, 18, 34, 178, DateTimeKind.Utc).AddTicks(3317) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 1, 16, 18, 34, 178, DateTimeKind.Utc).AddTicks(3318), new DateTime(2022, 10, 1, 16, 18, 34, 178, DateTimeKind.Utc).AddTicks(3319) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 1, 16, 18, 34, 178, DateTimeKind.Utc).AddTicks(3320), new DateTime(2022, 10, 1, 16, 18, 34, 178, DateTimeKind.Utc).AddTicks(3321) });
        }
    }
}
