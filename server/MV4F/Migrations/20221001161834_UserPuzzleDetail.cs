﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MV4F.Migrations
{
    public partial class UserPuzzleDetail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "puzzle_detail",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    remaining_hints = table.Column<int>(type: "int", nullable: false),
                    timer = table.Column<int>(type: "int", nullable: false),
                    elapsed_time = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_puzzle_detail", x => x.id);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "user_puzzle_detail",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    user_id = table.Column<long>(type: "bigint", nullable: false),
                    puzzle_id = table.Column<long>(type: "bigint", nullable: false),
                    updated_date = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    created_date = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_user_puzzle_detail", x => x.id);
                    table.ForeignKey(
                        name: "FK_user_puzzle_detail_puzzle_detail_puzzle_id",
                        column: x => x.puzzle_id,
                        principalTable: "puzzle_detail",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 1, 16, 18, 34, 178, DateTimeKind.Utc).AddTicks(3315), new DateTime(2022, 10, 1, 16, 18, 34, 178, DateTimeKind.Utc).AddTicks(3317) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 1, 16, 18, 34, 178, DateTimeKind.Utc).AddTicks(3318), new DateTime(2022, 10, 1, 16, 18, 34, 178, DateTimeKind.Utc).AddTicks(3319) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 1, 16, 18, 34, 178, DateTimeKind.Utc).AddTicks(3320), new DateTime(2022, 10, 1, 16, 18, 34, 178, DateTimeKind.Utc).AddTicks(3321) });

            migrationBuilder.CreateIndex(
                name: "IX_user_puzzle_detail_puzzle_id",
                table: "user_puzzle_detail",
                column: "puzzle_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "user_puzzle_detail");

            migrationBuilder.DropTable(
                name: "puzzle_detail");

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 9, 15, 3, 51, 2, 373, DateTimeKind.Utc).AddTicks(6215), new DateTime(2022, 9, 15, 3, 51, 2, 373, DateTimeKind.Utc).AddTicks(6217) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 9, 15, 3, 51, 2, 373, DateTimeKind.Utc).AddTicks(6218), new DateTime(2022, 9, 15, 3, 51, 2, 373, DateTimeKind.Utc).AddTicks(6218) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 9, 15, 3, 51, 2, 373, DateTimeKind.Utc).AddTicks(6219), new DateTime(2022, 9, 15, 3, 51, 2, 373, DateTimeKind.Utc).AddTicks(6219) });
        }
    }
}
