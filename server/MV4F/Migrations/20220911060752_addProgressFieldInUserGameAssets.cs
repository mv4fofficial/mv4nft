﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MV4F.Migrations
{
    public partial class addProgressFieldInUserGameAssets : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "progress",
                table: "user_game_assets",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 9, 11, 6, 7, 51, 895, DateTimeKind.Utc).AddTicks(44), new DateTime(2022, 9, 11, 6, 7, 51, 895, DateTimeKind.Utc).AddTicks(47) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 9, 11, 6, 7, 51, 895, DateTimeKind.Utc).AddTicks(48), new DateTime(2022, 9, 11, 6, 7, 51, 895, DateTimeKind.Utc).AddTicks(48) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 9, 11, 6, 7, 51, 895, DateTimeKind.Utc).AddTicks(49), new DateTime(2022, 9, 11, 6, 7, 51, 895, DateTimeKind.Utc).AddTicks(50) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "progress",
                table: "user_game_assets");

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 9, 9, 12, 14, 51, 764, DateTimeKind.Utc).AddTicks(5366), new DateTime(2022, 9, 9, 12, 14, 51, 764, DateTimeKind.Utc).AddTicks(5368) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 9, 9, 12, 14, 51, 764, DateTimeKind.Utc).AddTicks(5369), new DateTime(2022, 9, 9, 12, 14, 51, 764, DateTimeKind.Utc).AddTicks(5370) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 9, 9, 12, 14, 51, 764, DateTimeKind.Utc).AddTicks(5370), new DateTime(2022, 9, 9, 12, 14, 51, 764, DateTimeKind.Utc).AddTicks(5371) });
        }
    }
}
