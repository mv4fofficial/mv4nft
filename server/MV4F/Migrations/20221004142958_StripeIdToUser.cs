﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MV4F.Migrations
{
    public partial class StripeIdToUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "stripe_id",
                table: "user",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<bool>(
                name: "stripe_status",
                table: "user",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 4, 14, 29, 57, 621, DateTimeKind.Utc).AddTicks(1050), new DateTime(2022, 10, 4, 14, 29, 57, 621, DateTimeKind.Utc).AddTicks(1110) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 4, 14, 29, 57, 621, DateTimeKind.Utc).AddTicks(1210), new DateTime(2022, 10, 4, 14, 29, 57, 621, DateTimeKind.Utc).AddTicks(1210) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 4, 14, 29, 57, 621, DateTimeKind.Utc).AddTicks(1250), new DateTime(2022, 10, 4, 14, 29, 57, 621, DateTimeKind.Utc).AddTicks(1260) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "stripe_id",
                table: "user");

            migrationBuilder.DropColumn(
                name: "stripe_status",
                table: "user");

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 9, 29, 7, 48, 41, 180, DateTimeKind.Utc).AddTicks(9090), new DateTime(2022, 9, 29, 7, 48, 41, 180, DateTimeKind.Utc).AddTicks(9170) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 9, 29, 7, 48, 41, 180, DateTimeKind.Utc).AddTicks(9230), new DateTime(2022, 9, 29, 7, 48, 41, 180, DateTimeKind.Utc).AddTicks(9230) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 9, 29, 7, 48, 41, 180, DateTimeKind.Utc).AddTicks(9240), new DateTime(2022, 9, 29, 7, 48, 41, 180, DateTimeKind.Utc).AddTicks(9240) });
        }
    }
}
