﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MV4F.Migrations
{
    public partial class LeaderboardView : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 11, 24, 5, 22, 2, 636, DateTimeKind.Utc).AddTicks(8352), new DateTime(2022, 11, 24, 5, 22, 2, 636, DateTimeKind.Utc).AddTicks(8355) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 11, 24, 5, 22, 2, 636, DateTimeKind.Utc).AddTicks(8357), new DateTime(2022, 11, 24, 5, 22, 2, 636, DateTimeKind.Utc).AddTicks(8357) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 11, 24, 5, 22, 2, 636, DateTimeKind.Utc).AddTicks(8358), new DateTime(2022, 11, 24, 5, 22, 2, 636, DateTimeKind.Utc).AddTicks(8358) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 27, 11, 14, 44, 834, DateTimeKind.Utc).AddTicks(9833), new DateTime(2022, 10, 27, 11, 14, 44, 834, DateTimeKind.Utc).AddTicks(9835) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 27, 11, 14, 44, 834, DateTimeKind.Utc).AddTicks(9837), new DateTime(2022, 10, 27, 11, 14, 44, 834, DateTimeKind.Utc).AddTicks(9837) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 11, 2, 7, 11, 33, 65, DateTimeKind.Utc).AddTicks(1250), new DateTime(2022, 11, 2, 7, 11, 33, 65, DateTimeKind.Utc).AddTicks(1250) });
        }
    }
}
