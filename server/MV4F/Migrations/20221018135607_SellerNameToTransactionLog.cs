﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MV4F.Migrations
{
    public partial class SellerNameToTransactionLog : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
         //   migrationBuilder.AddColumn<string>(
         //       name: "destination",
         //       table: "transaction_log",
         //       type: "longtext",
         //       nullable: true)
         //       .Annotation("MySql:CharSet", "utf8mb4");

         //   migrationBuilder.AddColumn<DateTime>(
         //       name: "due_date",
         //       table: "transaction_log",
         //       type: "datetime(6)",
         //       nullable: false,
         //       defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

         //   migrationBuilder.AddColumn<bool>(
         //       name: "isPaid",
         //       table: "transaction_log",
         //       type: "tinyint(1)",
         //       nullable: false,
         //       defaultValue: false);

         //   migrationBuilder.AddColumn<decimal>(
         //       name: "net_amount",
         //       table: "transaction_log",
         //       type: "decimal(16,3)",
         //       nullable: false,
         //       defaultValue: 0m);

         //   migrationBuilder.AddColumn<string>(
         //       name: "seller_name",
         //       table: "transaction_log",
         //       type: "longtext",
         //       nullable: true)
         //       .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 18, 13, 56, 7, 286, DateTimeKind.Utc).AddTicks(8940), new DateTime(2022, 10, 18, 13, 56, 7, 286, DateTimeKind.Utc).AddTicks(9010) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 18, 13, 56, 7, 286, DateTimeKind.Utc).AddTicks(9100), new DateTime(2022, 10, 18, 13, 56, 7, 286, DateTimeKind.Utc).AddTicks(9100) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 18, 13, 56, 7, 286, DateTimeKind.Utc).AddTicks(9110), new DateTime(2022, 10, 18, 13, 56, 7, 286, DateTimeKind.Utc).AddTicks(9110) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "destination",
                table: "transaction_log");

            migrationBuilder.DropColumn(
                name: "due_date",
                table: "transaction_log");

            migrationBuilder.DropColumn(
                name: "isPaid",
                table: "transaction_log");

            migrationBuilder.DropColumn(
                name: "net_amount",
                table: "transaction_log");

            migrationBuilder.DropColumn(
                name: "seller_name",
                table: "transaction_log");

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 4, 14, 30, 18, 490, DateTimeKind.Utc).AddTicks(7380), new DateTime(2022, 10, 4, 14, 30, 18, 490, DateTimeKind.Utc).AddTicks(7430) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 4, 14, 30, 18, 490, DateTimeKind.Utc).AddTicks(7490), new DateTime(2022, 10, 4, 14, 30, 18, 490, DateTimeKind.Utc).AddTicks(7490) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 4, 14, 30, 18, 490, DateTimeKind.Utc).AddTicks(7500), new DateTime(2022, 10, 4, 14, 30, 18, 490, DateTimeKind.Utc).AddTicks(7500) });
        }
    }
}
