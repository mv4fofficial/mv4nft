﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MV4F.Migrations
{
    public partial class addRarityInProduct : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "rarity",
                table: "product",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 12, 9, 36, 53, 215, DateTimeKind.Utc).AddTicks(9290), new DateTime(2022, 10, 12, 9, 36, 53, 215, DateTimeKind.Utc).AddTicks(9291) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 12, 9, 36, 53, 215, DateTimeKind.Utc).AddTicks(9294), new DateTime(2022, 10, 12, 9, 36, 53, 215, DateTimeKind.Utc).AddTicks(9294) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 12, 9, 36, 53, 215, DateTimeKind.Utc).AddTicks(9296), new DateTime(2022, 10, 12, 9, 36, 53, 215, DateTimeKind.Utc).AddTicks(9296) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "rarity",
                table: "product");

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 12, 6, 19, 6, 728, DateTimeKind.Utc).AddTicks(5692), new DateTime(2022, 10, 12, 6, 19, 6, 728, DateTimeKind.Utc).AddTicks(5693) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 12, 6, 19, 6, 728, DateTimeKind.Utc).AddTicks(5694), new DateTime(2022, 10, 12, 6, 19, 6, 728, DateTimeKind.Utc).AddTicks(5695) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 12, 6, 19, 6, 728, DateTimeKind.Utc).AddTicks(5696), new DateTime(2022, 10, 12, 6, 19, 6, 728, DateTimeKind.Utc).AddTicks(5696) });
        }
    }
}
