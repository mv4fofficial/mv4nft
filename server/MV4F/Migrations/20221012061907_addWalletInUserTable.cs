﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MV4F.Migrations
{
    public partial class addWalletInUserTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "wallet",
                table: "user",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 12, 6, 19, 6, 728, DateTimeKind.Utc).AddTicks(5692), new DateTime(2022, 10, 12, 6, 19, 6, 728, DateTimeKind.Utc).AddTicks(5693) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 12, 6, 19, 6, 728, DateTimeKind.Utc).AddTicks(5694), new DateTime(2022, 10, 12, 6, 19, 6, 728, DateTimeKind.Utc).AddTicks(5695) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 12, 6, 19, 6, 728, DateTimeKind.Utc).AddTicks(5696), new DateTime(2022, 10, 12, 6, 19, 6, 728, DateTimeKind.Utc).AddTicks(5696) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "wallet",
                table: "user");

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 4, 17, 15, 45, 573, DateTimeKind.Utc).AddTicks(3078), new DateTime(2022, 10, 4, 17, 15, 45, 573, DateTimeKind.Utc).AddTicks(3083) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 4, 17, 15, 45, 573, DateTimeKind.Utc).AddTicks(3086), new DateTime(2022, 10, 4, 17, 15, 45, 573, DateTimeKind.Utc).AddTicks(3087) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 4, 17, 15, 45, 573, DateTimeKind.Utc).AddTicks(3089), new DateTime(2022, 10, 4, 17, 15, 45, 573, DateTimeKind.Utc).AddTicks(3090) });
        }
    }
}
