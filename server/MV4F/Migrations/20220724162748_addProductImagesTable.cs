﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MV4F.Migrations
{
    public partial class addProductImagesTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "product_images",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    product_id = table.Column<long>(type: "bigint", nullable: false),
                    product_image = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    created_date = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    updated_date = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_product_images", x => x.id);
                    table.ForeignKey(
                        name: "FK_product_images_product_product_id",
                        column: x => x.product_id,
                        principalTable: "product",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 7, 24, 16, 27, 47, 931, DateTimeKind.Utc).AddTicks(661), new DateTime(2022, 7, 24, 16, 27, 47, 931, DateTimeKind.Utc).AddTicks(662) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 7, 24, 16, 27, 47, 931, DateTimeKind.Utc).AddTicks(663), new DateTime(2022, 7, 24, 16, 27, 47, 931, DateTimeKind.Utc).AddTicks(664) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 7, 24, 16, 27, 47, 931, DateTimeKind.Utc).AddTicks(664), new DateTime(2022, 7, 24, 16, 27, 47, 931, DateTimeKind.Utc).AddTicks(665) });

            migrationBuilder.CreateIndex(
                name: "IX_product_images_product_id",
                table: "product_images",
                column: "product_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "product_images");

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 7, 13, 8, 19, 24, 382, DateTimeKind.Utc).AddTicks(3781), new DateTime(2022, 7, 13, 8, 19, 24, 382, DateTimeKind.Utc).AddTicks(3781) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 7, 13, 8, 19, 24, 382, DateTimeKind.Utc).AddTicks(3783), new DateTime(2022, 7, 13, 8, 19, 24, 382, DateTimeKind.Utc).AddTicks(3783) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 7, 13, 8, 19, 24, 382, DateTimeKind.Utc).AddTicks(3784), new DateTime(2022, 7, 13, 8, 19, 24, 382, DateTimeKind.Utc).AddTicks(3784) });
        }
    }
}
