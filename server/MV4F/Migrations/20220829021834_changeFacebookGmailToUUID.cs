﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MV4F.Migrations
{
    public partial class changeFacebookGmailToUUID : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "facebook_uuid",
                table: "user");

            migrationBuilder.RenameColumn(
                name: "gmail_uuid",
                table: "user",
                newName: "uuid");

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 8, 29, 2, 18, 34, 349, DateTimeKind.Utc).AddTicks(1875), new DateTime(2022, 8, 29, 2, 18, 34, 349, DateTimeKind.Utc).AddTicks(1876) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 8, 29, 2, 18, 34, 349, DateTimeKind.Utc).AddTicks(1877), new DateTime(2022, 8, 29, 2, 18, 34, 349, DateTimeKind.Utc).AddTicks(1878) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 8, 29, 2, 18, 34, 349, DateTimeKind.Utc).AddTicks(1879), new DateTime(2022, 8, 29, 2, 18, 34, 349, DateTimeKind.Utc).AddTicks(1879) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "uuid",
                table: "user",
                newName: "gmail_uuid");

            migrationBuilder.AddColumn<string>(
                name: "facebook_uuid",
                table: "user",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 8, 26, 3, 7, 12, 576, DateTimeKind.Utc).AddTicks(3579), new DateTime(2022, 8, 26, 3, 7, 12, 576, DateTimeKind.Utc).AddTicks(3580) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 8, 26, 3, 7, 12, 576, DateTimeKind.Utc).AddTicks(3581), new DateTime(2022, 8, 26, 3, 7, 12, 576, DateTimeKind.Utc).AddTicks(3582) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 8, 26, 3, 7, 12, 576, DateTimeKind.Utc).AddTicks(3583), new DateTime(2022, 8, 26, 3, 7, 12, 576, DateTimeKind.Utc).AddTicks(3583) });
        }
    }
}
