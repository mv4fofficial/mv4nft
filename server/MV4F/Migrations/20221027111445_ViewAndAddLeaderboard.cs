﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MV4F.Migrations
{
    public partial class ViewAndAddLeaderboard : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            // migrationBuilder.DropColumn(
            //     name: "destination",
            //     table: "payment_ledger");

            // migrationBuilder.DropColumn(
            //     name: "due_date",
            //     table: "payment_ledger");

            // migrationBuilder.DropColumn(
            //     name: "isPaid",
            //     table: "payment_ledger");

            // migrationBuilder.AddColumn<string>(
            //     name: "destination",
            //     table: "transaction_log",
            //     type: "longtext",
            //     nullable: true)
            //     .Annotation("MySql:CharSet", "utf8mb4");

            // migrationBuilder.AddColumn<DateTime>(
            //     name: "due_date",
            //     table: "transaction_log",
            //     type: "datetime(6)",
            //     nullable: false,
            //     defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            // migrationBuilder.AddColumn<bool>(
            //     name: "isPaid",
            //     table: "transaction_log",
            //     type: "tinyint(1)",
            //     nullable: false,
            //     defaultValue: false);

            // migrationBuilder.AddColumn<decimal>(
            //     name: "net_amount",
            //     table: "transaction_log",
            //     type: "decimal(16,3)",
            //     nullable: false,
            //     defaultValue: 0m);

            migrationBuilder.CreateTable(
                name: "player_score",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    user_id = table.Column<long>(type: "bigint", nullable: false),
                    score = table.Column<int>(type: "int", nullable: false),
                    created_date = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    updated_date = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_player_score", x => x.id);
                    table.ForeignKey(
                        name: "FK_player_score_user_user_id",
                        column: x => x.user_id,
                        principalTable: "user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 27, 11, 14, 44, 834, DateTimeKind.Utc).AddTicks(9833), new DateTime(2022, 10, 27, 11, 14, 44, 834, DateTimeKind.Utc).AddTicks(9835) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 27, 11, 14, 44, 834, DateTimeKind.Utc).AddTicks(9837), new DateTime(2022, 10, 27, 11, 14, 44, 834, DateTimeKind.Utc).AddTicks(9837) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 27, 11, 14, 44, 834, DateTimeKind.Utc).AddTicks(9838), new DateTime(2022, 10, 27, 11, 14, 44, 834, DateTimeKind.Utc).AddTicks(9838) });

            migrationBuilder.CreateIndex(
                name: "IX_player_score_user_id",
                table: "player_score",
                column: "user_id",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "player_score");

            migrationBuilder.DropColumn(
                name: "destination",
                table: "transaction_log");

            migrationBuilder.DropColumn(
                name: "due_date",
                table: "transaction_log");

            migrationBuilder.DropColumn(
                name: "isPaid",
                table: "transaction_log");

            migrationBuilder.DropColumn(
                name: "net_amount",
                table: "transaction_log");

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 4, 14, 30, 18, 490, DateTimeKind.Utc).AddTicks(7380), new DateTime(2022, 10, 4, 14, 30, 18, 490, DateTimeKind.Utc).AddTicks(7430) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 4, 14, 30, 18, 490, DateTimeKind.Utc).AddTicks(7490), new DateTime(2022, 10, 4, 14, 30, 18, 490, DateTimeKind.Utc).AddTicks(7490) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 4, 14, 30, 18, 490, DateTimeKind.Utc).AddTicks(7500), new DateTime(2022, 10, 4, 14, 30, 18, 490, DateTimeKind.Utc).AddTicks(7500) });
        }
    }
}
