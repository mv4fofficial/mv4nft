﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MV4F.Migrations
{
    public partial class UserFollow : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            // migrationBuilder.AddColumn<string>(
            //     name: "buyer_name",
            //     table: "transaction_log",
            //     type: "longtext",
            //     nullable: true)
            //     .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "user_follow",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    follower_Id = table.Column<long>(type: "bigint", nullable: false),
                    followee_Id = table.Column<long>(type: "bigint", nullable: false),
                    email_status = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    delete_flag = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    created_date = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_user_follow", x => x.id);
                    table.ForeignKey(
                        name: "FK_user_follow_user_followee_Id",
                        column: x => x.followee_Id,
                        principalTable: "user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 11, 2, 7, 11, 33, 65, DateTimeKind.Utc).AddTicks(1100), new DateTime(2022, 11, 2, 7, 11, 33, 65, DateTimeKind.Utc).AddTicks(1150) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 11, 2, 7, 11, 33, 65, DateTimeKind.Utc).AddTicks(1240), new DateTime(2022, 11, 2, 7, 11, 33, 65, DateTimeKind.Utc).AddTicks(1240) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 11, 2, 7, 11, 33, 65, DateTimeKind.Utc).AddTicks(1250), new DateTime(2022, 11, 2, 7, 11, 33, 65, DateTimeKind.Utc).AddTicks(1250) });

            migrationBuilder.CreateIndex(
                name: "IX_user_follow_followee_Id",
                table: "user_follow",
                column: "followee_Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "user_follow");

            // migrationBuilder.DropColumn(
            //     name: "buyer_name",
            //     table: "transaction_log");

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 18, 13, 56, 7, 286, DateTimeKind.Utc).AddTicks(8940), new DateTime(2022, 10, 18, 13, 56, 7, 286, DateTimeKind.Utc).AddTicks(9010) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 18, 13, 56, 7, 286, DateTimeKind.Utc).AddTicks(9100), new DateTime(2022, 10, 18, 13, 56, 7, 286, DateTimeKind.Utc).AddTicks(9100) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 18, 13, 56, 7, 286, DateTimeKind.Utc).AddTicks(9110), new DateTime(2022, 10, 18, 13, 56, 7, 286, DateTimeKind.Utc).AddTicks(9110) });
        }
    }
}
