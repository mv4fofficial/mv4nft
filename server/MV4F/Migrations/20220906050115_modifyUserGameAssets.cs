﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MV4F.Migrations
{
    public partial class modifyUserGameAssets : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "user_game_assets",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    user_id = table.Column<long>(type: "bigint", nullable: false),
                    game_asset_id = table.Column<long>(type: "bigint", nullable: false),
                    updated_date = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    created_date = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_user_game_assets", x => x.id);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 9, 6, 5, 1, 15, 273, DateTimeKind.Utc).AddTicks(7767), new DateTime(2022, 9, 6, 5, 1, 15, 273, DateTimeKind.Utc).AddTicks(7772) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 9, 6, 5, 1, 15, 273, DateTimeKind.Utc).AddTicks(7773), new DateTime(2022, 9, 6, 5, 1, 15, 273, DateTimeKind.Utc).AddTicks(7773) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 9, 6, 5, 1, 15, 273, DateTimeKind.Utc).AddTicks(7774), new DateTime(2022, 9, 6, 5, 1, 15, 273, DateTimeKind.Utc).AddTicks(7774) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "user_game_assets");

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 9, 6, 4, 23, 16, 184, DateTimeKind.Utc).AddTicks(1498), new DateTime(2022, 9, 6, 4, 23, 16, 184, DateTimeKind.Utc).AddTicks(1500) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 9, 6, 4, 23, 16, 184, DateTimeKind.Utc).AddTicks(1502), new DateTime(2022, 9, 6, 4, 23, 16, 184, DateTimeKind.Utc).AddTicks(1502) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 9, 6, 4, 23, 16, 184, DateTimeKind.Utc).AddTicks(1503), new DateTime(2022, 9, 6, 4, 23, 16, 184, DateTimeKind.Utc).AddTicks(1503) });
        }
    }
}
