﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MV4F.Migrations
{
    public partial class AddGameAssets : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "game_assets",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    type = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    image = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    coin_price = table.Column<long>(type: "bigint", nullable: false),
                    diamond_price = table.Column<long>(type: "bigint", nullable: false),
                    created_date = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    updated_date = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_game_assets", x => x.id);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 8, 2, 19, 15, 54, 984, DateTimeKind.Utc).AddTicks(6510), new DateTime(2022, 8, 2, 19, 15, 54, 984, DateTimeKind.Utc).AddTicks(6511) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 8, 2, 19, 15, 54, 984, DateTimeKind.Utc).AddTicks(6513), new DateTime(2022, 8, 2, 19, 15, 54, 984, DateTimeKind.Utc).AddTicks(6513) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 8, 2, 19, 15, 54, 984, DateTimeKind.Utc).AddTicks(6514), new DateTime(2022, 8, 2, 19, 15, 54, 984, DateTimeKind.Utc).AddTicks(6514) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "game_assets");

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 7, 24, 16, 27, 47, 931, DateTimeKind.Utc).AddTicks(661), new DateTime(2022, 7, 24, 16, 27, 47, 931, DateTimeKind.Utc).AddTicks(662) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 7, 24, 16, 27, 47, 931, DateTimeKind.Utc).AddTicks(663), new DateTime(2022, 7, 24, 16, 27, 47, 931, DateTimeKind.Utc).AddTicks(664) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 7, 24, 16, 27, 47, 931, DateTimeKind.Utc).AddTicks(664), new DateTime(2022, 7, 24, 16, 27, 47, 931, DateTimeKind.Utc).AddTicks(665) });
        }
    }
}
