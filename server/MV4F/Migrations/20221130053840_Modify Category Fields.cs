﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MV4F.Migrations
{
    public partial class ModifyCategoryFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 11, 30, 5, 38, 39, 767, DateTimeKind.Utc).AddTicks(997), new DateTime(2022, 11, 30, 5, 38, 39, 767, DateTimeKind.Utc).AddTicks(1000) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 11, 30, 5, 38, 39, 767, DateTimeKind.Utc).AddTicks(1001), new DateTime(2022, 11, 30, 5, 38, 39, 767, DateTimeKind.Utc).AddTicks(1001) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 11, 30, 5, 38, 39, 767, DateTimeKind.Utc).AddTicks(1002), new DateTime(2022, 11, 30, 5, 38, 39, 767, DateTimeKind.Utc).AddTicks(1002) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 11, 29, 7, 11, 1, 663, DateTimeKind.Utc).AddTicks(6203), new DateTime(2022, 11, 29, 7, 11, 1, 663, DateTimeKind.Utc).AddTicks(6205) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 11, 29, 7, 11, 1, 663, DateTimeKind.Utc).AddTicks(6208), new DateTime(2022, 11, 29, 7, 11, 1, 663, DateTimeKind.Utc).AddTicks(6209) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 11, 29, 7, 11, 1, 663, DateTimeKind.Utc).AddTicks(6209), new DateTime(2022, 11, 29, 7, 11, 1, 663, DateTimeKind.Utc).AddTicks(6210) });
        }
    }
}
