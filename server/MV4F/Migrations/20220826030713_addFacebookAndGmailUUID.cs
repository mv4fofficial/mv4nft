﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MV4F.Migrations
{
    public partial class addFacebookAndGmailUUID : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_user_country_country_id",
                table: "user");

            migrationBuilder.AlterColumn<DateTime>(
                name: "date_of_birth",
                table: "user",
                type: "datetime(6)",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)");

            migrationBuilder.AlterColumn<long>(
                name: "country_id",
                table: "user",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AlterColumn<long>(
                name: "contact_number",
                table: "user",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AddColumn<string>(
                name: "facebook_uuid",
                table: "user",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<string>(
                name: "gmail_uuid",
                table: "user",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 8, 26, 3, 7, 12, 576, DateTimeKind.Utc).AddTicks(3579), new DateTime(2022, 8, 26, 3, 7, 12, 576, DateTimeKind.Utc).AddTicks(3580) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 8, 26, 3, 7, 12, 576, DateTimeKind.Utc).AddTicks(3581), new DateTime(2022, 8, 26, 3, 7, 12, 576, DateTimeKind.Utc).AddTicks(3582) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 8, 26, 3, 7, 12, 576, DateTimeKind.Utc).AddTicks(3583), new DateTime(2022, 8, 26, 3, 7, 12, 576, DateTimeKind.Utc).AddTicks(3583) });

            migrationBuilder.AddForeignKey(
                name: "FK_user_country_country_id",
                table: "user",
                column: "country_id",
                principalTable: "country",
                principalColumn: "id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_user_country_country_id",
                table: "user");

            migrationBuilder.DropColumn(
                name: "facebook_uuid",
                table: "user");

            migrationBuilder.DropColumn(
                name: "gmail_uuid",
                table: "user");

            migrationBuilder.AlterColumn<DateTime>(
                name: "date_of_birth",
                table: "user",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "country_id",
                table: "user",
                type: "bigint",
                nullable: false,
                defaultValue: 0L,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "contact_number",
                table: "user",
                type: "bigint",
                nullable: false,
                defaultValue: 0L,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 8, 2, 19, 15, 54, 984, DateTimeKind.Utc).AddTicks(6510), new DateTime(2022, 8, 2, 19, 15, 54, 984, DateTimeKind.Utc).AddTicks(6511) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 8, 2, 19, 15, 54, 984, DateTimeKind.Utc).AddTicks(6513), new DateTime(2022, 8, 2, 19, 15, 54, 984, DateTimeKind.Utc).AddTicks(6513) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 8, 2, 19, 15, 54, 984, DateTimeKind.Utc).AddTicks(6514), new DateTime(2022, 8, 2, 19, 15, 54, 984, DateTimeKind.Utc).AddTicks(6514) });

            migrationBuilder.AddForeignKey(
                name: "FK_user_country_country_id",
                table: "user",
                column: "country_id",
                principalTable: "country",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
