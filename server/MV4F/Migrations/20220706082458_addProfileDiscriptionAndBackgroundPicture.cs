﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MV4F.Migrations
{
    public partial class addProfileDiscriptionAndBackgroundPicture : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "background_picture",
                table: "user",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<string>(
                name: "description",
                table: "user",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 7, 6, 8, 24, 58, 181, DateTimeKind.Utc).AddTicks(9414), new DateTime(2022, 7, 6, 8, 24, 58, 181, DateTimeKind.Utc).AddTicks(9415) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 7, 6, 8, 24, 58, 181, DateTimeKind.Utc).AddTicks(9416), new DateTime(2022, 7, 6, 8, 24, 58, 181, DateTimeKind.Utc).AddTicks(9417) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 7, 6, 8, 24, 58, 181, DateTimeKind.Utc).AddTicks(9417), new DateTime(2022, 7, 6, 8, 24, 58, 181, DateTimeKind.Utc).AddTicks(9418) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "background_picture",
                table: "user");

            migrationBuilder.DropColumn(
                name: "description",
                table: "user");

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 7, 6, 8, 23, 31, 309, DateTimeKind.Utc).AddTicks(348), new DateTime(2022, 7, 6, 8, 23, 31, 309, DateTimeKind.Utc).AddTicks(348) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 7, 6, 8, 23, 31, 309, DateTimeKind.Utc).AddTicks(349), new DateTime(2022, 7, 6, 8, 23, 31, 309, DateTimeKind.Utc).AddTicks(350) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 7, 6, 8, 23, 31, 309, DateTimeKind.Utc).AddTicks(351), new DateTime(2022, 7, 6, 8, 23, 31, 309, DateTimeKind.Utc).AddTicks(351) });
        }
    }
}
