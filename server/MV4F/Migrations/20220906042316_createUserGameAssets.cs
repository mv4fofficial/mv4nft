﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MV4F.Migrations
{
    public partial class createUserGameAssets : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "test_assets",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    type = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    image = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    coin_price = table.Column<long>(type: "bigint", nullable: false),
                    diamond_price = table.Column<long>(type: "bigint", nullable: false),
                    created_date = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    updated_date = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    test_field = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_test_assets", x => x.id);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 9, 6, 4, 23, 16, 184, DateTimeKind.Utc).AddTicks(1498), new DateTime(2022, 9, 6, 4, 23, 16, 184, DateTimeKind.Utc).AddTicks(1500) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 9, 6, 4, 23, 16, 184, DateTimeKind.Utc).AddTicks(1502), new DateTime(2022, 9, 6, 4, 23, 16, 184, DateTimeKind.Utc).AddTicks(1502) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 9, 6, 4, 23, 16, 184, DateTimeKind.Utc).AddTicks(1503), new DateTime(2022, 9, 6, 4, 23, 16, 184, DateTimeKind.Utc).AddTicks(1503) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "test_assets");

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 8, 29, 2, 18, 34, 349, DateTimeKind.Utc).AddTicks(1875), new DateTime(2022, 8, 29, 2, 18, 34, 349, DateTimeKind.Utc).AddTicks(1876) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 8, 29, 2, 18, 34, 349, DateTimeKind.Utc).AddTicks(1877), new DateTime(2022, 8, 29, 2, 18, 34, 349, DateTimeKind.Utc).AddTicks(1878) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 8, 29, 2, 18, 34, 349, DateTimeKind.Utc).AddTicks(1879), new DateTime(2022, 8, 29, 2, 18, 34, 349, DateTimeKind.Utc).AddTicks(1879) });
        }
    }
}
