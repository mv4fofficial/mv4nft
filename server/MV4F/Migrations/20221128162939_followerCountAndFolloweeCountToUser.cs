﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MV4F.Migrations
{
    public partial class followerCountAndFolloweeCountToUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "followee_count",
                table: "user",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "follower_count",
                table: "user",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 11, 28, 16, 29, 38, 911, DateTimeKind.Utc).AddTicks(9470), new DateTime(2022, 11, 28, 16, 29, 38, 911, DateTimeKind.Utc).AddTicks(9520) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 11, 28, 16, 29, 38, 911, DateTimeKind.Utc).AddTicks(9590), new DateTime(2022, 11, 28, 16, 29, 38, 911, DateTimeKind.Utc).AddTicks(9590) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 11, 28, 16, 29, 38, 911, DateTimeKind.Utc).AddTicks(9600), new DateTime(2022, 11, 28, 16, 29, 38, 911, DateTimeKind.Utc).AddTicks(9600) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "followee_count",
                table: "user");

            migrationBuilder.DropColumn(
                name: "follower_count",
                table: "user");

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 11, 2, 7, 11, 33, 65, DateTimeKind.Utc).AddTicks(1100), new DateTime(2022, 11, 2, 7, 11, 33, 65, DateTimeKind.Utc).AddTicks(1150) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 11, 2, 7, 11, 33, 65, DateTimeKind.Utc).AddTicks(1240), new DateTime(2022, 11, 2, 7, 11, 33, 65, DateTimeKind.Utc).AddTicks(1240) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 11, 2, 7, 11, 33, 65, DateTimeKind.Utc).AddTicks(1250), new DateTime(2022, 11, 2, 7, 11, 33, 65, DateTimeKind.Utc).AddTicks(1250) });
        }
    }
}
