﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MV4F.Migrations
{
    public partial class UpdateInverseProperty : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            // migrationBuilder.DropTable(
            //     name: "test_assets");

            migrationBuilder.AddColumn<DateTime>(
                name: "created_date",
                table: "puzzle_detail",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "updated_date",
                table: "puzzle_detail",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 4, 17, 15, 45, 573, DateTimeKind.Utc).AddTicks(3078), new DateTime(2022, 10, 4, 17, 15, 45, 573, DateTimeKind.Utc).AddTicks(3083) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 4, 17, 15, 45, 573, DateTimeKind.Utc).AddTicks(3086), new DateTime(2022, 10, 4, 17, 15, 45, 573, DateTimeKind.Utc).AddTicks(3087) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 4, 17, 15, 45, 573, DateTimeKind.Utc).AddTicks(3089), new DateTime(2022, 10, 4, 17, 15, 45, 573, DateTimeKind.Utc).AddTicks(3090) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "created_date",
                table: "puzzle_detail");

            migrationBuilder.DropColumn(
                name: "updated_date",
                table: "puzzle_detail");

            migrationBuilder.CreateTable(
                name: "test_assets",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    coin_price = table.Column<long>(type: "bigint", nullable: false),
                    created_date = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    diamond_price = table.Column<long>(type: "bigint", nullable: false),
                    image = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    name = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    test_field = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    type = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    updated_date = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_test_assets", x => x.id);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 1, 17, 46, 46, 732, DateTimeKind.Utc).AddTicks(5844), new DateTime(2022, 10, 1, 17, 46, 46, 732, DateTimeKind.Utc).AddTicks(5847) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 1, 17, 46, 46, 732, DateTimeKind.Utc).AddTicks(5848), new DateTime(2022, 10, 1, 17, 46, 46, 732, DateTimeKind.Utc).AddTicks(5849) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 1, 17, 46, 46, 732, DateTimeKind.Utc).AddTicks(5849), new DateTime(2022, 10, 1, 17, 46, 46, 732, DateTimeKind.Utc).AddTicks(5850) });
        }
    }
}
