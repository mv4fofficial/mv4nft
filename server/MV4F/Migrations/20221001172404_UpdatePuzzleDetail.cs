﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MV4F.Migrations
{
    public partial class UpdatePuzzleDetail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 1, 17, 24, 4, 276, DateTimeKind.Utc).AddTicks(4975), new DateTime(2022, 10, 1, 17, 24, 4, 276, DateTimeKind.Utc).AddTicks(4977) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 1, 17, 24, 4, 276, DateTimeKind.Utc).AddTicks(4979), new DateTime(2022, 10, 1, 17, 24, 4, 276, DateTimeKind.Utc).AddTicks(4979) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 1, 17, 24, 4, 276, DateTimeKind.Utc).AddTicks(4980), new DateTime(2022, 10, 1, 17, 24, 4, 276, DateTimeKind.Utc).AddTicks(4980) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 1, 17, 8, 39, 173, DateTimeKind.Utc).AddTicks(4717), new DateTime(2022, 10, 1, 17, 8, 39, 173, DateTimeKind.Utc).AddTicks(4720) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 1, 17, 8, 39, 173, DateTimeKind.Utc).AddTicks(4722), new DateTime(2022, 10, 1, 17, 8, 39, 173, DateTimeKind.Utc).AddTicks(4722) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 1, 17, 8, 39, 173, DateTimeKind.Utc).AddTicks(4723), new DateTime(2022, 10, 1, 17, 8, 39, 173, DateTimeKind.Utc).AddTicks(4723) });
        }
    }
}
