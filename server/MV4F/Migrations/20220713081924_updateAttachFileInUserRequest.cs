﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MV4F.Migrations
{
    public partial class updateAttachFileInUserRequest : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "attach_file_url",
                table: "user_request",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 7, 13, 8, 19, 24, 382, DateTimeKind.Utc).AddTicks(3781), new DateTime(2022, 7, 13, 8, 19, 24, 382, DateTimeKind.Utc).AddTicks(3781) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 7, 13, 8, 19, 24, 382, DateTimeKind.Utc).AddTicks(3783), new DateTime(2022, 7, 13, 8, 19, 24, 382, DateTimeKind.Utc).AddTicks(3783) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 7, 13, 8, 19, 24, 382, DateTimeKind.Utc).AddTicks(3784), new DateTime(2022, 7, 13, 8, 19, 24, 382, DateTimeKind.Utc).AddTicks(3784) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "attach_file_url",
                table: "user_request");

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 7, 13, 7, 6, 12, 194, DateTimeKind.Utc).AddTicks(2465), new DateTime(2022, 7, 13, 7, 6, 12, 194, DateTimeKind.Utc).AddTicks(2466) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 7, 13, 7, 6, 12, 194, DateTimeKind.Utc).AddTicks(2467), new DateTime(2022, 7, 13, 7, 6, 12, 194, DateTimeKind.Utc).AddTicks(2467) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 7, 13, 7, 6, 12, 194, DateTimeKind.Utc).AddTicks(2468), new DateTime(2022, 7, 13, 7, 6, 12, 194, DateTimeKind.Utc).AddTicks(2468) });
        }
    }
}
