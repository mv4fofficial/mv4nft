﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MV4F.Migrations
{
    public partial class includeGameAssetsInViewUserGameAssets : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 9, 9, 12, 14, 51, 764, DateTimeKind.Utc).AddTicks(5366), new DateTime(2022, 9, 9, 12, 14, 51, 764, DateTimeKind.Utc).AddTicks(5368) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 9, 9, 12, 14, 51, 764, DateTimeKind.Utc).AddTicks(5369), new DateTime(2022, 9, 9, 12, 14, 51, 764, DateTimeKind.Utc).AddTicks(5370) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 9, 9, 12, 14, 51, 764, DateTimeKind.Utc).AddTicks(5370), new DateTime(2022, 9, 9, 12, 14, 51, 764, DateTimeKind.Utc).AddTicks(5371) });

            migrationBuilder.CreateIndex(
                name: "IX_user_game_assets_game_asset_id",
                table: "user_game_assets",
                column: "game_asset_id",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_user_game_assets_game_assets_game_asset_id",
                table: "user_game_assets",
                column: "game_asset_id",
                principalTable: "game_assets",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_user_game_assets_game_assets_game_asset_id",
                table: "user_game_assets");

            migrationBuilder.DropIndex(
                name: "IX_user_game_assets_game_asset_id",
                table: "user_game_assets");

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 9, 6, 5, 1, 15, 273, DateTimeKind.Utc).AddTicks(7767), new DateTime(2022, 9, 6, 5, 1, 15, 273, DateTimeKind.Utc).AddTicks(7772) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 9, 6, 5, 1, 15, 273, DateTimeKind.Utc).AddTicks(7773), new DateTime(2022, 9, 6, 5, 1, 15, 273, DateTimeKind.Utc).AddTicks(7773) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 9, 6, 5, 1, 15, 273, DateTimeKind.Utc).AddTicks(7774), new DateTime(2022, 9, 6, 5, 1, 15, 273, DateTimeKind.Utc).AddTicks(7774) });
        }
    }
}
