﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MV4F.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterDatabase()
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "become_seller_question_master",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    question = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    created_date = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_become_seller_question_master", x => x.id);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "category_master",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(type: "varchar(150)", maxLength: 150, nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    category_picture = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    is_deleted = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    created_date = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    updated_date = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_category_master", x => x.id);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "country",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(type: "varchar(150)", maxLength: 150, nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    country_code = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    phone_code = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    is_restricted = table.Column<bool>(type: "tinyint(1)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_country", x => x.id);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "password_reset_token",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    email = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    token = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    expiry_date = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    created_date = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    updated_date = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_password_reset_token", x => x.id);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "role",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    created_date = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    updated_date = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_role", x => x.id);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "sub_category_master",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    sub_category_name = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    sub_category_picture = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    category_id = table.Column<long>(type: "bigint", nullable: false),
                    is_deleted = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    created_date = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    updated_date = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sub_category_master", x => x.id);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "tags_master",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    tag_name = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    created_date = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    updated_date = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tags_master", x => x.id);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "user_contact",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    contact_number = table.Column<long>(type: "bigint", nullable: false),
                    email_Id = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    comments = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    created_date = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_user_contact", x => x.id);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "user",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    first_name = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    last_name = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    profile_picture = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    contact_number = table.Column<long>(type: "bigint", nullable: false),
                    email_Id = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    date_of_birth = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    bio = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    password = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    created_date = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    updated_date = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    is_active = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    country_id = table.Column<long>(type: "bigint", nullable: false),
                    is_email_verified = table.Column<bool>(type: "tinyint(1)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_user", x => x.id);
                    table.ForeignKey(
                        name: "FK_user_country_country_id",
                        column: x => x.country_id,
                        principalTable: "country",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "addresses",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    first_name = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    last_name = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    address1 = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    address2 = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    city = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    pincode = table.Column<long>(type: "bigint", nullable: false),
                    user_id = table.Column<long>(type: "bigint", nullable: false),
                    @default = table.Column<bool>(name: "default", type: "tinyint(1)", nullable: false),
                    created_date = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    updated_date = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_addresses", x => x.id);
                    table.ForeignKey(
                        name: "FK_addresses_user_user_id",
                        column: x => x.user_id,
                        principalTable: "user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "interested_categories",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    user_id = table.Column<long>(type: "bigint", nullable: false),
                    category_id = table.Column<long>(type: "bigint", nullable: true),
                    sub_category_id = table.Column<long>(type: "bigint", nullable: true),
                    created_date = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_interested_categories", x => x.id);
                    table.ForeignKey(
                        name: "FK_interested_categories_category_master_category_id",
                        column: x => x.category_id,
                        principalTable: "category_master",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "FK_interested_categories_sub_category_master_sub_category_id",
                        column: x => x.sub_category_id,
                        principalTable: "sub_category_master",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "FK_interested_categories_user_user_id",
                        column: x => x.user_id,
                        principalTable: "user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "product",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    product_title = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    price = table.Column<decimal>(type: "decimal(16,3)", nullable: false),
                    gas_fee = table.Column<decimal>(type: "decimal(16,3)", nullable: true),
                    product_image = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    description = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    quantity = table.Column<long>(type: "bigint", nullable: true),
                    link = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    nft_status = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    for_sale = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    seller_id = table.Column<long>(type: "bigint", nullable: false),
                    is_deleted = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    created_date = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    updated_date = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_product", x => x.id);
                    table.ForeignKey(
                        name: "FK_product_user_seller_id",
                        column: x => x.seller_id,
                        principalTable: "user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "refresh_token",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    token = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    expires = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    created = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    updated_date = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    user_id = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_refresh_token", x => x.id);
                    table.ForeignKey(
                        name: "FK_refresh_token_user_user_id",
                        column: x => x.user_id,
                        principalTable: "user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "seller_answer",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    user_id = table.Column<long>(type: "bigint", nullable: false),
                    question_id = table.Column<long>(type: "bigint", nullable: false),
                    answer = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    created_date = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_seller_answer", x => x.id);
                    table.ForeignKey(
                        name: "FK_seller_answer_become_seller_question_master_question_id",
                        column: x => x.question_id,
                        principalTable: "become_seller_question_master",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_seller_answer_user_user_id",
                        column: x => x.user_id,
                        principalTable: "user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "user_orders",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    user_id = table.Column<long>(type: "bigint", nullable: false),
                    amount = table.Column<decimal>(type: "decimal(16,3)", nullable: false),
                    updated_date = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    created_date = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    order_status = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_user_orders", x => x.id);
                    table.ForeignKey(
                        name: "FK_user_orders_user_user_id",
                        column: x => x.user_id,
                        principalTable: "user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "user_role",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    user_id = table.Column<long>(type: "bigint", nullable: false),
                    role_id = table.Column<long>(type: "bigint", nullable: false),
                    created_date = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    updated_date = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_user_role", x => x.id);
                    table.ForeignKey(
                        name: "FK_user_role_role_role_id",
                        column: x => x.role_id,
                        principalTable: "role",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_user_role_user_user_id",
                        column: x => x.user_id,
                        principalTable: "user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "wish_list",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    user_id = table.Column<long>(type: "bigint", nullable: false),
                    description = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    created_date = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    updated_date = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_wish_list", x => x.id);
                    table.ForeignKey(
                        name: "FK_wish_list_user_user_id",
                        column: x => x.user_id,
                        principalTable: "user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "product_category",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    product_id = table.Column<long>(type: "bigint", nullable: false),
                    category_id = table.Column<long>(type: "bigint", nullable: false),
                    created_date = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    updated_date = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_product_category", x => x.id);
                    table.ForeignKey(
                        name: "FK_product_category_category_master_category_id",
                        column: x => x.category_id,
                        principalTable: "category_master",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_product_category_product_product_id",
                        column: x => x.product_id,
                        principalTable: "product",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "product_sub_categories",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    product_id = table.Column<long>(type: "bigint", nullable: false),
                    sub_category_id = table.Column<long>(type: "bigint", nullable: false),
                    created_date = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    updated_date = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_product_sub_categories", x => x.id);
                    table.ForeignKey(
                        name: "FK_product_sub_categories_product_product_id",
                        column: x => x.product_id,
                        principalTable: "product",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_product_sub_categories_sub_category_master_sub_category_id",
                        column: x => x.sub_category_id,
                        principalTable: "sub_category_master",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "product_tags",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    product_id = table.Column<long>(type: "bigint", nullable: false),
                    tag_id = table.Column<long>(type: "bigint", nullable: false),
                    created_date = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    updated_date = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_product_tags", x => x.id);
                    table.ForeignKey(
                        name: "FK_product_tags_product_product_id",
                        column: x => x.product_id,
                        principalTable: "product",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_product_tags_tags_master_tag_id",
                        column: x => x.tag_id,
                        principalTable: "tags_master",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "payment_ledger",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    user_id = table.Column<long>(type: "bigint", nullable: false),
                    amount = table.Column<decimal>(type: "decimal(16,3)", nullable: false),
                    transaction_status = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    created_date = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    updated_date = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    order_id = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_payment_ledger", x => x.id);
                    table.ForeignKey(
                        name: "FK_payment_ledger_user_orders_order_id",
                        column: x => x.order_id,
                        principalTable: "user_orders",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_payment_ledger_user_user_id",
                        column: x => x.user_id,
                        principalTable: "user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "transaction_log",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    user_id = table.Column<long>(type: "bigint", nullable: false),
                    product_id = table.Column<long>(type: "bigint", nullable: false),
                    created_date = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    order_id = table.Column<long>(type: "bigint", nullable: false),
                    amount = table.Column<decimal>(type: "decimal(16,3)", nullable: false),
                    order_status = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_transaction_log", x => x.id);
                    table.ForeignKey(
                        name: "FK_transaction_log_product_product_id",
                        column: x => x.product_id,
                        principalTable: "product",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_transaction_log_user_orders_order_id",
                        column: x => x.order_id,
                        principalTable: "user_orders",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_transaction_log_user_user_id",
                        column: x => x.user_id,
                        principalTable: "user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "user_cart",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    user_id = table.Column<long>(type: "bigint", nullable: false),
                    product_id = table.Column<long>(type: "bigint", nullable: false),
                    order_status = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    created_date = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    updated_date = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    order_id = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_user_cart", x => x.id);
                    table.ForeignKey(
                        name: "FK_user_cart_product_product_id",
                        column: x => x.product_id,
                        principalTable: "product",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_user_cart_user_orders_order_id",
                        column: x => x.order_id,
                        principalTable: "user_orders",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "FK_user_cart_user_user_id",
                        column: x => x.user_id,
                        principalTable: "user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "user_products",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    user_id = table.Column<long>(type: "bigint", nullable: false),
                    product_id = table.Column<long>(type: "bigint", nullable: false),
                    order_id = table.Column<long>(type: "bigint", nullable: false),
                    created_date = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    updated_date = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_user_products", x => x.id);
                    table.ForeignKey(
                        name: "FK_user_products_product_product_id",
                        column: x => x.product_id,
                        principalTable: "product",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_user_products_user_orders_order_id",
                        column: x => x.order_id,
                        principalTable: "user_orders",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_user_products_user_user_id",
                        column: x => x.user_id,
                        principalTable: "user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "notification",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    user_id = table.Column<long>(type: "bigint", nullable: false),
                    wish_list_id = table.Column<long>(type: "bigint", nullable: false),
                    seller_id = table.Column<long>(type: "bigint", nullable: false),
                    product_id = table.Column<long>(type: "bigint", nullable: true),
                    is_read = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    is_seller = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    created_date = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    updated_date = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_notification", x => x.id);
                    table.ForeignKey(
                        name: "FK_notification_product_product_id",
                        column: x => x.product_id,
                        principalTable: "product",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "FK_notification_user_seller_id",
                        column: x => x.seller_id,
                        principalTable: "user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_notification_user_user_id",
                        column: x => x.user_id,
                        principalTable: "user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_notification_wish_list_wish_list_id",
                        column: x => x.wish_list_id,
                        principalTable: "wish_list",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "wish_list_categories_and_subcategories",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    wish_list_id = table.Column<long>(type: "bigint", nullable: false),
                    user_id = table.Column<long>(type: "bigint", nullable: false),
                    category_id = table.Column<long>(type: "bigint", nullable: true),
                    subcategory_id = table.Column<long>(type: "bigint", nullable: true),
                    created_date = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_wish_list_categories_and_subcategories", x => x.id);
                    table.ForeignKey(
                        name: "FK_wish_list_categories_and_subcategories_category_master_categ~",
                        column: x => x.category_id,
                        principalTable: "category_master",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "FK_wish_list_categories_and_subcategories_sub_category_master_s~",
                        column: x => x.subcategory_id,
                        principalTable: "sub_category_master",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "FK_wish_list_categories_and_subcategories_user_user_id",
                        column: x => x.user_id,
                        principalTable: "user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_wish_list_categories_and_subcategories_wish_list_wish_list_id",
                        column: x => x.wish_list_id,
                        principalTable: "wish_list",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.InsertData(
                table: "country",
                columns: new[] { "id", "country_code", "is_restricted", "name", "phone_code" },
                values: new object[,]
                {
                    { 1L, "AF", false, "Afghanistan", "+93" },
                    { 2L, "AX", false, "Aland Islands", "+358" },
                    { 3L, "AL", false, "Albania", "+355" },
                    { 4L, "DZ", false, "Algeria", "+213" },
                    { 5L, "AS", false, "AmericanSamoa", "+1684" },
                    { 6L, "AD", false, "Andorra", "+376" },
                    { 7L, "AO", false, "Angola", "+244" },
                    { 8L, "AI", false, "Anguilla", "+1264" },
                    { 9L, "AQ", false, "Antarctica", "+672" },
                    { 10L, "AG", false, "Antigua and Barbuda", "+1268" },
                    { 11L, "AR", false, "Argentina", "+54" },
                    { 12L, "AM", false, "Armenia", "+374" },
                    { 13L, "AW", false, "Aruba", "+297" },
                    { 14L, "AU", false, "Australia", "+61" },
                    { 15L, "AT", false, "Austria", "+43" },
                    { 16L, "AZ", false, "Azerbaijan", "+994" },
                    { 17L, "BS", false, "Bahamas", "+1242" },
                    { 18L, "BH", false, "Bahrain", "+973" },
                    { 19L, "BD", false, "Bangladesh", "+880" },
                    { 20L, "BB", false, "Barbados", "+1246" },
                    { 21L, "BY", false, "Belarus", "+375" },
                    { 22L, "BE", false, "Belgium", "+32" },
                    { 23L, "BZ", false, "Belize", "+501" },
                    { 24L, "BJ", false, "Benin", "+229" },
                    { 25L, "BM", false, "Bermuda", "+1441" },
                    { 26L, "BT", false, "Bhutan", "+975" },
                    { 27L, "BO", false, "Bolivia, Plurinational State of", "+591" },
                    { 28L, "BA", false, "Bosnia and Herzegovina", "+387" },
                    { 29L, "BW", false, "Botswana", "+267" },
                    { 30L, "BR", false, "Brazil", "+55" },
                    { 31L, "IO", false, "British Indian Ocean Territory", "+246" },
                    { 32L, "BN", false, "Brunei Darussalam", "+673" },
                    { 33L, "BG", false, "Bulgaria", "+359" },
                    { 34L, "BF", false, "Burkina Faso", "+226" },
                    { 35L, "BI", false, "Burundi", "+257" },
                    { 36L, "KH", false, "Cambodia", "+855" },
                    { 37L, "CM", false, "Cameroon", "+237" },
                    { 38L, "CA", false, "Canada", "+1" },
                    { 39L, "CV", false, "Cape Verde", "+238" },
                    { 40L, "KY", false, "Cayman Islands", "+ 345" },
                    { 41L, "CF", false, "Central African Republic", "+236" },
                    { 42L, "TD", false, "Chad", "+235" },
                    { 43L, "CL", false, "Chile", "+56" },
                    { 44L, "CN", false, "China", "+86" },
                    { 45L, "CX", false, "Christmas Island", "+61" },
                    { 46L, "CC", false, "Cocos (Keeling) Islands", "+61" },
                    { 47L, "CO", false, "Colombia", "+57" },
                    { 48L, "KM", false, "Comoros", "+269" },
                    { 49L, "CG", false, "Congo", "+242" },
                    { 50L, "CD", false, "Congo, The Democratic Republic of the Congo", "+243" },
                    { 51L, "CK", false, "Cook Islands", "+682" },
                    { 52L, "CR", false, "Costa Rica", "+506" },
                    { 53L, "CI", false, "Cote d'Ivoire", "+225" },
                    { 54L, "HR", false, "Croatia", "+385" },
                    { 55L, "CU", false, "Cuba", "+53" },
                    { 56L, "CY", false, "Cyprus", "+357" },
                    { 57L, "CZ", false, "Czech Republic", "+420" },
                    { 58L, "DK", false, "Denmark", "+45" },
                    { 59L, "DJ", false, "Djibouti", "+253" },
                    { 60L, "DM", false, "Dominica", "+1767" },
                    { 61L, "DO", false, "Dominican Republic", "+1849" },
                    { 62L, "EC", false, "Ecuador", "+593" },
                    { 63L, "EG", false, "Egypt", "+20" },
                    { 64L, "SV", false, "El Salvador", "+503" },
                    { 65L, "GQ", false, "Equatorial Guinea", "+240" },
                    { 66L, "ER", false, "Eritrea", "+291" },
                    { 67L, "EE", false, "Estonia", "+372" },
                    { 68L, "ET", false, "Ethiopia", "+251" },
                    { 69L, "FK", false, "Falkland Islands (Malvinas)", "+500" },
                    { 70L, "FO", false, "Faroe Islands", "+298" },
                    { 71L, "FJ", false, "Fiji", "+679" },
                    { 72L, "FI", false, "Finland", "+358" },
                    { 73L, "FR", false, "France", "+33" },
                    { 74L, "GF", false, "French Guiana", "+594" },
                    { 75L, "PF", false, "French Polynesia", "+689" },
                    { 76L, "GA", false, "Gabon", "+241" },
                    { 77L, "GM", false, "Gambia", "+220" },
                    { 78L, "GE", false, "Georgia", "+995" },
                    { 79L, "DE", false, "Germany", "+49" },
                    { 80L, "GH", false, "Ghana", "+233" },
                    { 81L, "GI", false, "Gibraltar", "+350" },
                    { 82L, "GR", false, "Greece", "+30" },
                    { 83L, "GL", false, "Greenland", "+299" },
                    { 84L, "GD", false, "Grenada", "+1473" },
                    { 85L, "GP", false, "Guadeloupe", "+590" },
                    { 86L, "GU", false, "Guam", "+1671" },
                    { 87L, "GT", false, "Guatemala", "+502" },
                    { 88L, "GG", false, "Guernsey", "+44" },
                    { 89L, "GN", false, "Guinea", "+224" },
                    { 90L, "GW", false, "Guinea-Bissau", "+245" },
                    { 91L, "GY", false, "Guyana", "+595" },
                    { 92L, "HT", false, "Haiti", "+509" },
                    { 93L, "VA", false, "Holy See (Vatican City State)", "+379" },
                    { 94L, "HN", false, "Honduras", "+504" },
                    { 95L, "HK", false, "Hong Kong", "+852" },
                    { 96L, "HU", false, "Hungary", "+36" },
                    { 97L, "IS", false, "Iceland", "+354" },
                    { 98L, "IN", false, "India", "+91" },
                    { 99L, "ID", false, "Indonesia", "+62" },
                    { 100L, "IR", false, "Iran, Islamic Republic of Persian Gulf", "+98" },
                    { 101L, "IQ", false, "Iraq", "+964" },
                    { 102L, "IE", false, "Ireland", "+353" },
                    { 103L, "IM", false, "Isle of Man", "+44" },
                    { 104L, "IL", false, "Israel", "+972" },
                    { 105L, "IT", false, "Italy", "+39" },
                    { 106L, "JM", false, "Jamaica", "+1876" },
                    { 107L, "JP", false, "Japan", "+81" },
                    { 109L, "JE", false, "Jersey", "+44" },
                    { 110L, "JO", false, "Jordan", "+962" },
                    { 111L, "KZ", false, "Kazakhstan", "+77" },
                    { 112L, "KE", false, "Kenya", "+254" },
                    { 113L, "KI", false, "Kiribati", "+686" },
                    { 114L, "KP", false, "Korea, Democratic People's Republic of Korea", "+850" },
                    { 115L, "KR", false, "Korea, Republic of South Korea", "+82" },
                    { 116L, "KW", false, "Kuwait", "+965" },
                    { 117L, "KG", false, "Kyrgyzstan", "+996" },
                    { 118L, "LA", false, "Laos", "+856" },
                    { 119L, "LV", false, "Latvia", "+371" },
                    { 120L, "LB", false, "Lebanon", "+961" },
                    { 121L, "LS", false, "Lesotho", "+266" },
                    { 122L, "LR", false, "Liberia", "+231" },
                    { 123L, "LY", false, "Libyan Arab Jamahiriya", "+218" },
                    { 124L, "LI", false, "Liechtenstein", "+423" },
                    { 125L, "LT", false, "Lithuania", "+370" },
                    { 126L, "LU", false, "Luxembourg", "+352" },
                    { 127L, "MO", false, "Macao", "+853" },
                    { 128L, "MK", false, "Macedonia", "+389" },
                    { 129L, "MG", false, "Madagascar", "+261" },
                    { 130L, "MW", false, "Malawi", "+265" },
                    { 131L, "MY", false, "Malaysia", "+60" },
                    { 132L, "MV", false, "Maldives", "+960" },
                    { 133L, "ML", false, "Mali", "+223" },
                    { 134L, "MT", false, "Malta", "+356" },
                    { 135L, "MH", false, "Marshall Islands", "+692" },
                    { 136L, "MQ", false, "Martinique", "+596" },
                    { 137L, "MR", false, "Mauritania", "+222" },
                    { 138L, "MU", false, "Mauritius", "+230" },
                    { 139L, "YT", false, "Mayotte", "+262" },
                    { 140L, "MX", false, "Mexico", "+52" },
                    { 141L, "FM", false, "Micronesia, Federated States of Micronesia", "+691" },
                    { 142L, "MD", false, "Moldova", "+373" },
                    { 143L, "MC", false, "Monaco", "+377" },
                    { 144L, "MN", false, "Mongolia", "+976" },
                    { 145L, "ME", false, "Montenegro", "+382" },
                    { 146L, "MS", false, "Montserrat", "+1664" },
                    { 147L, "MA", false, "Morocco", "+212" },
                    { 148L, "MZ", false, "Mozambique", "+258" },
                    { 149L, "MM", false, "Myanmar", "+95" },
                    { 150L, "NA", false, "Namibia", "+264" },
                    { 151L, "NR", false, "Nauru", "+674" },
                    { 152L, "NP", false, "Nepal", "+977" },
                    { 153L, "NL", false, "Netherlands", "+31" },
                    { 154L, "AN", false, "Netherlands Antilles", "+599" },
                    { 155L, "NC", false, "New Caledonia", "+687" },
                    { 156L, "NZ", false, "New Zealand", "+64" },
                    { 157L, "NI", false, "Nicaragua", "+505" },
                    { 158L, "NE", false, "Niger", "+227" },
                    { 159L, "NG", false, "Nigeria", "+234" },
                    { 160L, "NU", false, "Niue", "+683" },
                    { 161L, "NF", false, "Norfolk Island", "+672" },
                    { 162L, "MP", false, "Northern Mariana Islands", "+1670" },
                    { 163L, "NO", false, "Norway", "+47" },
                    { 164L, "OM", false, "Oman", "+968" },
                    { 165L, "PK", false, "Pakistan", "+92" },
                    { 166L, "PW", false, "Palau", "+680" },
                    { 167L, "PS", false, "Palestinian Territory, Occupied", "+970" },
                    { 168L, "PA", false, "Panama", "+507" },
                    { 169L, "PG", false, "Papua New Guinea", "+675" },
                    { 170L, "PY", false, "Paraguay", "+595" },
                    { 171L, "PE", false, "Peru", "+51" },
                    { 172L, "PH", false, "Philippines", "+63" },
                    { 173L, "PN", false, "Pitcairn", "+872" },
                    { 174L, "PL", false, "Poland", "+48" },
                    { 175L, "PT", false, "Portugal", "+351" },
                    { 176L, "PR", false, "Puerto Rico", "+1939" },
                    { 177L, "QA", false, "Qatar", "+974" },
                    { 178L, "RO", false, "Romania", "+40" },
                    { 179L, "RU", false, "Russia", "+7" },
                    { 180L, "RW", false, "Rwanda", "+250" },
                    { 181L, "RE", false, "Reunion", "+262" },
                    { 182L, "BL", false, "Saint Barthelemy", "+590" },
                    { 183L, "SH", false, "Saint Helena, Ascension and Tristan Da Cunha", "+290" },
                    { 184L, "KN", false, "Saint Kitts and Nevis", "+1869" },
                    { 185L, "LC", false, "Saint Lucia", "+1758" },
                    { 186L, "MF", false, "Saint Martin", "+590" },
                    { 187L, "PM", false, "Saint Pierre and Miquelon", "+508" },
                    { 188L, "VC", false, "Saint Vincent and the Grenadines", "+1784" },
                    { 189L, "WS", false, "Samoa", "+685" },
                    { 190L, "SM", false, "San Marino", "+378" },
                    { 191L, "ST", false, "Sao Tome and Principe", "+239" },
                    { 192L, "SA", false, "Saudi Arabia", "+966" },
                    { 193L, "SN", false, "Senegal", "+221" },
                    { 194L, "RS", false, "Serbia", "+381" },
                    { 195L, "SC", false, "Seychelles", "+248" },
                    { 196L, "SL", false, "Sierra Leone", "+232" },
                    { 197L, "SG", false, "Singapore", "+65" },
                    { 198L, "SK", false, "Slovakia", "+421" },
                    { 199L, "SI", false, "Slovenia", "+386" },
                    { 200L, "SB", false, "Solomon Islands", "+677" },
                    { 201L, "SO", false, "Somalia", "+252" },
                    { 202L, "ZA", false, "South Africa", "+27" },
                    { 203L, "SS", false, "South Sudan", "+211" },
                    { 204L, "GS", false, "South Georgia and the South Sandwich Islands", "+500" },
                    { 205L, "ES", false, "Spain", "+34" },
                    { 206L, "LK", false, "Sri Lanka", "+94" },
                    { 207L, "SD", false, "Sudan", "+249" },
                    { 208L, "SR", false, "Suriname", "+597" },
                    { 209L, "SJ", false, "Svalbard and Jan Mayen", "+47" },
                    { 210L, "SZ", false, "Swaziland", "+268" },
                    { 211L, "SE", false, "Sweden", "+46" },
                    { 212L, "CH", false, "Switzerland", "+41" },
                    { 213L, "SY", false, "Syrian Arab Republic", "+963" },
                    { 214L, "TW", false, "Taiwan", "+886" },
                    { 215L, "TJ", false, "Tajikistan", "+992" },
                    { 216L, "TZ", false, "Tanzania, United Republic of Tanzania", "+255" },
                    { 217L, "TH", false, "Thailand", "+66" },
                    { 218L, "TL", false, "Timor-Leste", "+670" },
                    { 219L, "TG", false, "Togo", "+228" },
                    { 220L, "TK", false, "Tokelau", "+690" },
                    { 221L, "TO", false, "Tonga", "+676" },
                    { 222L, "TT", false, "Trinidad and Tobago", "+1868" },
                    { 223L, "TN", false, "Tunisia", "+216" },
                    { 224L, "TR", false, "Turkey", "+90" },
                    { 225L, "TM", false, "Turkmenistan", "+993" },
                    { 226L, "TC", false, "Turks and Caicos Islands", "+1649" },
                    { 227L, "TV", false, "Tuvalu", "+688" },
                    { 228L, "UG", false, "Uganda", "+256" },
                    { 229L, "UA", false, "Ukraine", "+380" },
                    { 230L, "AE", false, "United Arab Emirates", "+971" },
                    { 231L, "GB", false, "United Kingdom", "+44" },
                    { 232L, "US", false, "United States", "+1" },
                    { 233L, "UY", false, "Uruguay", "+598" },
                    { 234L, "UZ", false, "Uzbekistan", "+998" },
                    { 235L, "VU", false, "Vanuatu", "+678" },
                    { 236L, "VE", false, "Venezuela, Bolivarian Republic of Venezuela", "+58" },
                    { 237L, "VN", false, "Vietnam", "+84" },
                    { 238L, "VG", false, "Virgin Islands, British", "+1284" },
                    { 239L, "VI", false, "Virgin Islands, U.S.", "+1340" },
                    { 240L, "WF", false, "Wallis and Futuna", "+681" },
                    { 241L, "YE", false, "Yemen", "+967" },
                    { 242L, "ZM", false, "Zambia", "+260" },
                    { 243L, "ZW", false, "Zimbabwe", "+263" }
                });

            migrationBuilder.InsertData(
                table: "role",
                columns: new[] { "id", "created_date", "name", "updated_date" },
                values: new object[,]
                {
                    { 1L, new DateTime(2022, 6, 7, 4, 56, 31, 698, DateTimeKind.Utc).AddTicks(24), "Admin", new DateTime(2022, 6, 7, 4, 56, 31, 698, DateTimeKind.Utc).AddTicks(25) },
                    { 2L, new DateTime(2022, 6, 7, 4, 56, 31, 698, DateTimeKind.Utc).AddTicks(26), "Seller", new DateTime(2022, 6, 7, 4, 56, 31, 698, DateTimeKind.Utc).AddTicks(26) },
                    { 3L, new DateTime(2022, 6, 7, 4, 56, 31, 698, DateTimeKind.Utc).AddTicks(27), "Buyer", new DateTime(2022, 6, 7, 4, 56, 31, 698, DateTimeKind.Utc).AddTicks(27) }
                });

            migrationBuilder.CreateIndex(
                name: "IX_addresses_user_id",
                table: "addresses",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_interested_categories_category_id",
                table: "interested_categories",
                column: "category_id");

            migrationBuilder.CreateIndex(
                name: "IX_interested_categories_sub_category_id",
                table: "interested_categories",
                column: "sub_category_id");

            migrationBuilder.CreateIndex(
                name: "IX_interested_categories_user_id",
                table: "interested_categories",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_notification_product_id",
                table: "notification",
                column: "product_id");

            migrationBuilder.CreateIndex(
                name: "IX_notification_seller_id",
                table: "notification",
                column: "seller_id");

            migrationBuilder.CreateIndex(
                name: "IX_notification_user_id",
                table: "notification",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_notification_wish_list_id",
                table: "notification",
                column: "wish_list_id");

            migrationBuilder.CreateIndex(
                name: "IX_payment_ledger_order_id",
                table: "payment_ledger",
                column: "order_id");

            migrationBuilder.CreateIndex(
                name: "IX_payment_ledger_user_id",
                table: "payment_ledger",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_product_seller_id",
                table: "product",
                column: "seller_id");

            migrationBuilder.CreateIndex(
                name: "IX_product_category_category_id",
                table: "product_category",
                column: "category_id");

            migrationBuilder.CreateIndex(
                name: "IX_product_category_product_id",
                table: "product_category",
                column: "product_id");

            migrationBuilder.CreateIndex(
                name: "IX_product_sub_categories_product_id",
                table: "product_sub_categories",
                column: "product_id");

            migrationBuilder.CreateIndex(
                name: "IX_product_sub_categories_sub_category_id",
                table: "product_sub_categories",
                column: "sub_category_id");

            migrationBuilder.CreateIndex(
                name: "IX_product_tags_product_id",
                table: "product_tags",
                column: "product_id");

            migrationBuilder.CreateIndex(
                name: "IX_product_tags_tag_id",
                table: "product_tags",
                column: "tag_id");

            migrationBuilder.CreateIndex(
                name: "IX_refresh_token_user_id",
                table: "refresh_token",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_seller_answer_question_id",
                table: "seller_answer",
                column: "question_id");

            migrationBuilder.CreateIndex(
                name: "IX_seller_answer_user_id",
                table: "seller_answer",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_transaction_log_order_id",
                table: "transaction_log",
                column: "order_id");

            migrationBuilder.CreateIndex(
                name: "IX_transaction_log_product_id",
                table: "transaction_log",
                column: "product_id");

            migrationBuilder.CreateIndex(
                name: "IX_transaction_log_user_id",
                table: "transaction_log",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_user_country_id",
                table: "user",
                column: "country_id");

            migrationBuilder.CreateIndex(
                name: "IX_user_cart_order_id",
                table: "user_cart",
                column: "order_id");

            migrationBuilder.CreateIndex(
                name: "IX_user_cart_product_id",
                table: "user_cart",
                column: "product_id");

            migrationBuilder.CreateIndex(
                name: "IX_user_cart_user_id",
                table: "user_cart",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_user_orders_user_id",
                table: "user_orders",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_user_products_order_id",
                table: "user_products",
                column: "order_id");

            migrationBuilder.CreateIndex(
                name: "IX_user_products_product_id",
                table: "user_products",
                column: "product_id");

            migrationBuilder.CreateIndex(
                name: "IX_user_products_user_id",
                table: "user_products",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_user_role_role_id",
                table: "user_role",
                column: "role_id");

            migrationBuilder.CreateIndex(
                name: "IX_user_role_user_id",
                table: "user_role",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_wish_list_user_id",
                table: "wish_list",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_wish_list_categories_and_subcategories_category_id",
                table: "wish_list_categories_and_subcategories",
                column: "category_id");

            migrationBuilder.CreateIndex(
                name: "IX_wish_list_categories_and_subcategories_subcategory_id",
                table: "wish_list_categories_and_subcategories",
                column: "subcategory_id");

            migrationBuilder.CreateIndex(
                name: "IX_wish_list_categories_and_subcategories_user_id",
                table: "wish_list_categories_and_subcategories",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_wish_list_categories_and_subcategories_wish_list_id",
                table: "wish_list_categories_and_subcategories",
                column: "wish_list_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "addresses");

            migrationBuilder.DropTable(
                name: "interested_categories");

            migrationBuilder.DropTable(
                name: "notification");

            migrationBuilder.DropTable(
                name: "password_reset_token");

            migrationBuilder.DropTable(
                name: "payment_ledger");

            migrationBuilder.DropTable(
                name: "product_category");

            migrationBuilder.DropTable(
                name: "product_sub_categories");

            migrationBuilder.DropTable(
                name: "product_tags");

            migrationBuilder.DropTable(
                name: "refresh_token");

            migrationBuilder.DropTable(
                name: "seller_answer");

            migrationBuilder.DropTable(
                name: "transaction_log");

            migrationBuilder.DropTable(
                name: "user_cart");

            migrationBuilder.DropTable(
                name: "user_contact");

            migrationBuilder.DropTable(
                name: "user_products");

            migrationBuilder.DropTable(
                name: "user_role");

            migrationBuilder.DropTable(
                name: "wish_list_categories_and_subcategories");

            migrationBuilder.DropTable(
                name: "tags_master");

            migrationBuilder.DropTable(
                name: "become_seller_question_master");

            migrationBuilder.DropTable(
                name: "product");

            migrationBuilder.DropTable(
                name: "user_orders");

            migrationBuilder.DropTable(
                name: "role");

            migrationBuilder.DropTable(
                name: "category_master");

            migrationBuilder.DropTable(
                name: "sub_category_master");

            migrationBuilder.DropTable(
                name: "wish_list");

            migrationBuilder.DropTable(
                name: "user");

            migrationBuilder.DropTable(
                name: "country");
        }
    }
}
