﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MV4F.Migrations
{
    public partial class ChangeUserPuzzleDetailList : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            // migrationBuilder.DropIndex(
            //     name: "IX_user_puzzle_detail_puzzle_id",
            //     table: "user_puzzle_detail");

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 1, 17, 46, 46, 732, DateTimeKind.Utc).AddTicks(5844), new DateTime(2022, 10, 1, 17, 46, 46, 732, DateTimeKind.Utc).AddTicks(5847) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 1, 17, 46, 46, 732, DateTimeKind.Utc).AddTicks(5848), new DateTime(2022, 10, 1, 17, 46, 46, 732, DateTimeKind.Utc).AddTicks(5849) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 1, 17, 46, 46, 732, DateTimeKind.Utc).AddTicks(5849), new DateTime(2022, 10, 1, 17, 46, 46, 732, DateTimeKind.Utc).AddTicks(5850) });

            migrationBuilder.CreateIndex(
                name: "IX_user_puzzle_detail_puzzle_id",
                table: "user_puzzle_detail",
                column: "puzzle_id",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_user_puzzle_detail_puzzle_id",
                table: "user_puzzle_detail");

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 1, 17, 24, 4, 276, DateTimeKind.Utc).AddTicks(4975), new DateTime(2022, 10, 1, 17, 24, 4, 276, DateTimeKind.Utc).AddTicks(4977) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 1, 17, 24, 4, 276, DateTimeKind.Utc).AddTicks(4979), new DateTime(2022, 10, 1, 17, 24, 4, 276, DateTimeKind.Utc).AddTicks(4979) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 1, 17, 24, 4, 276, DateTimeKind.Utc).AddTicks(4980), new DateTime(2022, 10, 1, 17, 24, 4, 276, DateTimeKind.Utc).AddTicks(4980) });

            migrationBuilder.CreateIndex(
                name: "IX_user_puzzle_detail_puzzle_id",
                table: "user_puzzle_detail",
                column: "puzzle_id");
        }
    }
}
