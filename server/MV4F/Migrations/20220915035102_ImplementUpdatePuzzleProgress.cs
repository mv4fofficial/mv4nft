﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MV4F.Migrations
{
    public partial class ImplementUpdatePuzzleProgress : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 9, 15, 3, 51, 2, 373, DateTimeKind.Utc).AddTicks(6215), new DateTime(2022, 9, 15, 3, 51, 2, 373, DateTimeKind.Utc).AddTicks(6217) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 9, 15, 3, 51, 2, 373, DateTimeKind.Utc).AddTicks(6218), new DateTime(2022, 9, 15, 3, 51, 2, 373, DateTimeKind.Utc).AddTicks(6218) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 9, 15, 3, 51, 2, 373, DateTimeKind.Utc).AddTicks(6219), new DateTime(2022, 9, 15, 3, 51, 2, 373, DateTimeKind.Utc).AddTicks(6219) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 9, 11, 6, 7, 51, 895, DateTimeKind.Utc).AddTicks(44), new DateTime(2022, 9, 11, 6, 7, 51, 895, DateTimeKind.Utc).AddTicks(47) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 9, 11, 6, 7, 51, 895, DateTimeKind.Utc).AddTicks(48), new DateTime(2022, 9, 11, 6, 7, 51, 895, DateTimeKind.Utc).AddTicks(48) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 9, 11, 6, 7, 51, 895, DateTimeKind.Utc).AddTicks(49), new DateTime(2022, 9, 11, 6, 7, 51, 895, DateTimeKind.Utc).AddTicks(50) });
        }
    }
}
