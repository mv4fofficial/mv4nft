﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MV4F.Migrations
{
    public partial class addUserRequestTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "user_request",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    user_id = table.Column<long>(type: "bigint", nullable: false),
                    requested_role_id = table.Column<long>(type: "bigint", nullable: true),
                    request_type = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    description = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    status = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    created_date = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    updated_date = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_user_request", x => x.id);
                    table.ForeignKey(
                        name: "FK_user_request_user_user_id",
                        column: x => x.user_id,
                        principalTable: "user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 7, 13, 7, 6, 12, 194, DateTimeKind.Utc).AddTicks(2465), new DateTime(2022, 7, 13, 7, 6, 12, 194, DateTimeKind.Utc).AddTicks(2466) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 7, 13, 7, 6, 12, 194, DateTimeKind.Utc).AddTicks(2467), new DateTime(2022, 7, 13, 7, 6, 12, 194, DateTimeKind.Utc).AddTicks(2467) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 7, 13, 7, 6, 12, 194, DateTimeKind.Utc).AddTicks(2468), new DateTime(2022, 7, 13, 7, 6, 12, 194, DateTimeKind.Utc).AddTicks(2468) });

            migrationBuilder.CreateIndex(
                name: "IX_user_request_user_id",
                table: "user_request",
                column: "user_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "user_request");

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 7, 6, 9, 52, 19, 828, DateTimeKind.Utc).AddTicks(6011), new DateTime(2022, 7, 6, 9, 52, 19, 828, DateTimeKind.Utc).AddTicks(6012) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 7, 6, 9, 52, 19, 828, DateTimeKind.Utc).AddTicks(6013), new DateTime(2022, 7, 6, 9, 52, 19, 828, DateTimeKind.Utc).AddTicks(6013) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 7, 6, 9, 52, 19, 828, DateTimeKind.Utc).AddTicks(6014), new DateTime(2022, 7, 6, 9, 52, 19, 828, DateTimeKind.Utc).AddTicks(6015) });
        }
    }
}
