﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MV4F.Migrations
{
    public partial class GameAssetCategories : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_puzzle-categories_game_assets_game_asset_id",
                table: "puzzle-categories");

            migrationBuilder.DropIndex(
                name: "IX_puzzle-categories_game_asset_id",
                table: "puzzle-categories");

            migrationBuilder.DropColumn(
                name: "game_asset_id",
                table: "puzzle-categories");

            migrationBuilder.CreateTable(
                name: "game-asset-categories",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    category_id = table.Column<long>(type: "bigint", nullable: false),
                    game_asset_id = table.Column<long>(type: "bigint", nullable: false),
                    created_date = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    updated_date = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_game-asset-categories", x => x.id);
                    table.ForeignKey(
                        name: "FK_game-asset-categories_game_assets_game_asset_id",
                        column: x => x.game_asset_id,
                        principalTable: "game_assets",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_game-asset-categories_puzzle-categories_category_id",
                        column: x => x.category_id,
                        principalTable: "puzzle-categories",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 11, 29, 5, 27, 0, 735, DateTimeKind.Utc).AddTicks(461), new DateTime(2022, 11, 29, 5, 27, 0, 735, DateTimeKind.Utc).AddTicks(464) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 11, 29, 5, 27, 0, 735, DateTimeKind.Utc).AddTicks(467), new DateTime(2022, 11, 29, 5, 27, 0, 735, DateTimeKind.Utc).AddTicks(468) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 11, 29, 5, 27, 0, 735, DateTimeKind.Utc).AddTicks(470), new DateTime(2022, 11, 29, 5, 27, 0, 735, DateTimeKind.Utc).AddTicks(471) });

            migrationBuilder.CreateIndex(
                name: "IX_game-asset-categories_category_id",
                table: "game-asset-categories",
                column: "category_id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_game-asset-categories_game_asset_id",
                table: "game-asset-categories",
                column: "game_asset_id",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "game-asset-categories");

            migrationBuilder.AddColumn<long>(
                name: "game_asset_id",
                table: "puzzle-categories",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 11, 28, 15, 51, 40, 578, DateTimeKind.Utc).AddTicks(5555), new DateTime(2022, 11, 28, 15, 51, 40, 578, DateTimeKind.Utc).AddTicks(5556) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 11, 28, 15, 51, 40, 578, DateTimeKind.Utc).AddTicks(5557), new DateTime(2022, 11, 28, 15, 51, 40, 578, DateTimeKind.Utc).AddTicks(5558) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 11, 28, 15, 51, 40, 578, DateTimeKind.Utc).AddTicks(5558), new DateTime(2022, 11, 28, 15, 51, 40, 578, DateTimeKind.Utc).AddTicks(5559) });

            migrationBuilder.CreateIndex(
                name: "IX_puzzle-categories_game_asset_id",
                table: "puzzle-categories",
                column: "game_asset_id",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_puzzle-categories_game_assets_game_asset_id",
                table: "puzzle-categories",
                column: "game_asset_id",
                principalTable: "game_assets",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
