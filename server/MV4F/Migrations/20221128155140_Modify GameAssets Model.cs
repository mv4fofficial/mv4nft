﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MV4F.Migrations
{
    public partial class ModifyGameAssetsModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "game_asset_id",
                table: "puzzle-categories",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 11, 28, 15, 51, 40, 578, DateTimeKind.Utc).AddTicks(5555), new DateTime(2022, 11, 28, 15, 51, 40, 578, DateTimeKind.Utc).AddTicks(5556) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 11, 28, 15, 51, 40, 578, DateTimeKind.Utc).AddTicks(5557), new DateTime(2022, 11, 28, 15, 51, 40, 578, DateTimeKind.Utc).AddTicks(5558) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 11, 28, 15, 51, 40, 578, DateTimeKind.Utc).AddTicks(5558), new DateTime(2022, 11, 28, 15, 51, 40, 578, DateTimeKind.Utc).AddTicks(5559) });

            migrationBuilder.CreateIndex(
                name: "IX_puzzle-categories_game_asset_id",
                table: "puzzle-categories",
                column: "game_asset_id",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_puzzle-categories_game_assets_game_asset_id",
                table: "puzzle-categories",
                column: "game_asset_id",
                principalTable: "game_assets",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_puzzle-categories_game_assets_game_asset_id",
                table: "puzzle-categories");

            migrationBuilder.DropIndex(
                name: "IX_puzzle-categories_game_asset_id",
                table: "puzzle-categories");

            migrationBuilder.DropColumn(
                name: "game_asset_id",
                table: "puzzle-categories");

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 11, 28, 15, 21, 28, 146, DateTimeKind.Utc).AddTicks(873), new DateTime(2022, 11, 28, 15, 21, 28, 146, DateTimeKind.Utc).AddTicks(876) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 11, 28, 15, 21, 28, 146, DateTimeKind.Utc).AddTicks(877), new DateTime(2022, 11, 28, 15, 21, 28, 146, DateTimeKind.Utc).AddTicks(877) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 11, 28, 15, 21, 28, 146, DateTimeKind.Utc).AddTicks(878), new DateTime(2022, 11, 28, 15, 21, 28, 146, DateTimeKind.Utc).AddTicks(879) });
        }
    }
}
