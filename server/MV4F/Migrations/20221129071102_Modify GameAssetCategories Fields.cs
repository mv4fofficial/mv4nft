﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MV4F.Migrations
{
    public partial class ModifyGameAssetCategoriesFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_game-asset-categories_game_assets_game_asset_id",
                table: "game-asset-categories");

            migrationBuilder.DropForeignKey(
                name: "FK_game-asset-categories_puzzle-categories_category_id",
                table: "game-asset-categories");

            migrationBuilder.DropIndex(
                name: "IX_game-asset-categories_category_id",
                table: "game-asset-categories");

            migrationBuilder.DropIndex(
                name: "IX_game-asset-categories_game_asset_id",
                table: "game-asset-categories");

            migrationBuilder.AddColumn<long>(
                name: "GameAssetsId",
                table: "game-asset-categories",
                type: "bigint",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "PuzzleCategoriesId",
                table: "game-asset-categories",
                type: "bigint",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 11, 29, 7, 11, 1, 663, DateTimeKind.Utc).AddTicks(6203), new DateTime(2022, 11, 29, 7, 11, 1, 663, DateTimeKind.Utc).AddTicks(6205) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 11, 29, 7, 11, 1, 663, DateTimeKind.Utc).AddTicks(6208), new DateTime(2022, 11, 29, 7, 11, 1, 663, DateTimeKind.Utc).AddTicks(6209) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 11, 29, 7, 11, 1, 663, DateTimeKind.Utc).AddTicks(6209), new DateTime(2022, 11, 29, 7, 11, 1, 663, DateTimeKind.Utc).AddTicks(6210) });

            migrationBuilder.CreateIndex(
                name: "IX_game-asset-categories_GameAssetsId",
                table: "game-asset-categories",
                column: "GameAssetsId");

            migrationBuilder.CreateIndex(
                name: "IX_game-asset-categories_PuzzleCategoriesId",
                table: "game-asset-categories",
                column: "PuzzleCategoriesId");

            migrationBuilder.AddForeignKey(
                name: "FK_game-asset-categories_game_assets_GameAssetsId",
                table: "game-asset-categories",
                column: "GameAssetsId",
                principalTable: "game_assets",
                principalColumn: "id");

            migrationBuilder.AddForeignKey(
                name: "FK_game-asset-categories_puzzle-categories_PuzzleCategoriesId",
                table: "game-asset-categories",
                column: "PuzzleCategoriesId",
                principalTable: "puzzle-categories",
                principalColumn: "id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_game-asset-categories_game_assets_GameAssetsId",
                table: "game-asset-categories");

            migrationBuilder.DropForeignKey(
                name: "FK_game-asset-categories_puzzle-categories_PuzzleCategoriesId",
                table: "game-asset-categories");

            migrationBuilder.DropIndex(
                name: "IX_game-asset-categories_GameAssetsId",
                table: "game-asset-categories");

            migrationBuilder.DropIndex(
                name: "IX_game-asset-categories_PuzzleCategoriesId",
                table: "game-asset-categories");

            migrationBuilder.DropColumn(
                name: "GameAssetsId",
                table: "game-asset-categories");

            migrationBuilder.DropColumn(
                name: "PuzzleCategoriesId",
                table: "game-asset-categories");

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 11, 29, 5, 27, 0, 735, DateTimeKind.Utc).AddTicks(461), new DateTime(2022, 11, 29, 5, 27, 0, 735, DateTimeKind.Utc).AddTicks(464) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 11, 29, 5, 27, 0, 735, DateTimeKind.Utc).AddTicks(467), new DateTime(2022, 11, 29, 5, 27, 0, 735, DateTimeKind.Utc).AddTicks(468) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 11, 29, 5, 27, 0, 735, DateTimeKind.Utc).AddTicks(470), new DateTime(2022, 11, 29, 5, 27, 0, 735, DateTimeKind.Utc).AddTicks(471) });

            migrationBuilder.CreateIndex(
                name: "IX_game-asset-categories_category_id",
                table: "game-asset-categories",
                column: "category_id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_game-asset-categories_game_asset_id",
                table: "game-asset-categories",
                column: "game_asset_id",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_game-asset-categories_game_assets_game_asset_id",
                table: "game-asset-categories",
                column: "game_asset_id",
                principalTable: "game_assets",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_game-asset-categories_puzzle-categories_category_id",
                table: "game-asset-categories",
                column: "category_id",
                principalTable: "puzzle-categories",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
