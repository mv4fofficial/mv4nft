﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MV4F.Migrations
{
    public partial class StripeStatusToUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 4, 14, 30, 18, 490, DateTimeKind.Utc).AddTicks(7380), new DateTime(2022, 10, 4, 14, 30, 18, 490, DateTimeKind.Utc).AddTicks(7430) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 4, 14, 30, 18, 490, DateTimeKind.Utc).AddTicks(7490), new DateTime(2022, 10, 4, 14, 30, 18, 490, DateTimeKind.Utc).AddTicks(7490) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 4, 14, 30, 18, 490, DateTimeKind.Utc).AddTicks(7500), new DateTime(2022, 10, 4, 14, 30, 18, 490, DateTimeKind.Utc).AddTicks(7500) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 1L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 4, 14, 29, 57, 621, DateTimeKind.Utc).AddTicks(1050), new DateTime(2022, 10, 4, 14, 29, 57, 621, DateTimeKind.Utc).AddTicks(1110) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 2L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 4, 14, 29, 57, 621, DateTimeKind.Utc).AddTicks(1210), new DateTime(2022, 10, 4, 14, 29, 57, 621, DateTimeKind.Utc).AddTicks(1210) });

            migrationBuilder.UpdateData(
                table: "role",
                keyColumn: "id",
                keyValue: 3L,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2022, 10, 4, 14, 29, 57, 621, DateTimeKind.Utc).AddTicks(1250), new DateTime(2022, 10, 4, 14, 29, 57, 621, DateTimeKind.Utc).AddTicks(1260) });
        }
    }
}
