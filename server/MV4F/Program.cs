using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using System;
using System.Diagnostics;
using Stripe;

namespace mvfourf
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // SendMail();
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>

            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });

        // public static void SendMail() {
        //     Console.WriteLine("Send Mail");
        //     try
        //     {
        //         // Console.WriteLine("Send Mail");

        //         StripeConfiguration.ApiKey = "sk_test_51KWhwZLKdDbFtRftb2ddnfbnuHPDkF6HslE8ev7C4P8ICAGabdNg7fOS9DF4TD7NJJUmwV5GtF8xg2QKRHbLEE1600J3JGoybu";

        //         var options = new TransferCreateOptions
        //         {
        //             Amount = 500,
        //             Currency = "sgd",
        //             Destination = "acct_1LSOraPx4FRboLpM",
        //             TransferGroup = "ORDER_95",
        //         };
        //         var service = new TransferService();
        //         service.Create(options);    
        //     }
        //     catch (System.Exception)
        //     {
                
        //         throw;
        //     }
        // }
    }
}
