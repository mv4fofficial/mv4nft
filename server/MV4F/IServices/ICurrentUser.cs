﻿namespace MV4F.IRepository
{
    public interface ICurrentUser
    {
        public long UserID { get; }

        public string Email { get; }

        public string Name { get; }

        public int GroupID { get; }
    }
}