﻿using MV4F.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MV4F.IRepository
{
    public interface IGameServices
    {
        Task<ServiceResponse<List<GameAssetsViewModel>>> ViewGameAssets(string type);

        Task<ServiceResponse<GameAssetsViewModel>> ViewGameAssetById(long id);

        Task<ApiResponse> AddGameAsset(AddGameAssetModel model);
    }
}
