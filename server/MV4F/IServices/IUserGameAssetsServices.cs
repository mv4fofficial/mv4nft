﻿using MV4F.Models;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace MV4F.IRepository
{
    public interface IUserGameAssetsServices
    {
        Task<ServiceResponse<List<UserGameAssetsViewModel>>> ViewUserGameAssets();
        
        Task<ServiceResponse<List<UserGameAssetsViewModel>>> ViewInProgressPuzzles();

        Task<ApiResponse> AddUserGameAsset(AddUserGameAssetModel model);

        Task<ApiResponse> UpdateProgress(ProgressUpdateModel model);
    }
}
