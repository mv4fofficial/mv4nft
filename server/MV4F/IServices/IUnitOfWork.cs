﻿using MV4F.IRepository;

namespace MV4F.IServices
{
    public interface IUnitOfWork
    {
        IAccountServices AccountServices { get; }

        IAdminService AdminServices { get; }

        IEmailService EmailServices { get; }

        IStorage Storage { get; }

        ICurrentUser CurrentUser { get; }

        ISellerServices SellerServices { get; }

        IUserServices UserServices { get; }

        IDropDownServices DropDownServices { get; }

        IPaymentService PaymentService { get; }

        IHomeServices HomeServices { get; }

        IGameServices GameServices { get; }

        IUserGameAssetsServices UserGameAssetsServices { get; }
        
        IPuzzleDetailServices PuzzleDetailServices { get; }

        IUserPuzzleDetailServices UserPuzzleDetailServices { get; }

        ILeaderboardServices LeaderboardServices { get; }

        IPuzzleCategoryServices PuzzleCategoryServices { get; }

        IGameAssetCategoryServices GameAssetCategoryServices { get; }
    }
}
