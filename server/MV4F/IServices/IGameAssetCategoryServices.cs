﻿using MV4F.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MV4F.IRepository
{
    public interface IGameAssetCategoryServices
    {
        Task<ServiceResponse<List<GameAssetCategoryViewModel>>> ViewGameAssetCategories();

        Task<ServiceResponse<GameAssetCategoryViewModel>> ViewGameAssetsByCategoryId(long id);

        Task<ApiResponse> AddGameAssetCategory(GameAssetCategoryAddModel model);
    }
}
