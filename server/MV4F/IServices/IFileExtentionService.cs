﻿namespace MV4F.IServices
{
    public interface IFileExtentionService
    {
        string GetExtension(string subString);
    }
}
