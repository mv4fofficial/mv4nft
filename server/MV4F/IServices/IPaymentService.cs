﻿using MV4F.Models;
using System.Threading.Tasks;

namespace MV4F.IServices
{
    public interface IPaymentService
    {
        Task<ApiResponse> PlaceOrder(PlaceOrderModel order);

        Task<ApiResponse> PaymentTransferToSeller();

        Task<ApiResponse> TestConnect();

        Task<ServiceResponse<string>> CreateAccount(DataForStripeModel dataForStripe);

        Task<ServiceResponse<object>> CompleteAccount(DataForStripeModel dataForStripe);

        Task<ServiceResponse<object>> CheckAccount(DataForStripeModel dataForStripe);

        Task<ServiceResponse<object>> GetBalance(DataForStripeModel dataForStripe);

        Task<ServiceResponse<object>> FetchTransactions(DataForStripeModel dataForStripe);

        Task<ServiceResponse<object>> FetchStripeTransactions();

        Task<ServiceResponse<object>> FetchStripeConnectAccountList();

        Task<ApiResponse> UpdateCompleteAccountStatus();

        Task<ServiceResponse<object>> CreateLoginLink(DataForStripeModel dataForStripe);
    }
}
