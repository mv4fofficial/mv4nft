﻿using MV4F.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MV4F.IServices
{
    public interface IAdminService
    {

        Task<ApiResponse> AddCatagory(AddCategoryModel model);

        Task<ApiResponse> AddTags(AddTagModel model);

        Task<ApiResponse> UpdateCatagory(UpdateCategotyModel model);

        Task<ApiResponse> UpdateTag(UpdateTagModel model);

        Task<ServiceResponse<List<UpdateCategotyModel>>> ViewAllCategories();

        Task<ApiResponse> DeleteCatagory(int Id);

        Task<ApiResponse> DeleteTag(int Id);

        Task<ServiceResponse<List<RegisteredUserModel>>> ViewAllUsers(long roleId);

        Task<ServiceResponse<List<PaymentLedgerResponse>>> ViewAllPaymentDetails();


        Task<ApiResponse> AddSubCategory(AddSubCategoryModel model);

        Task<ApiResponse> UpdateSubCatagory(UpdateSubCategotyModel model);

        Task<ApiResponse> DeleteSubCatagory(int Id);

        Task<ApiResponse> ActionUserRequest(ActionRequestModel model);

        Task<ServiceResponse<ProductSummary>> ProductsSummary(ProductSummaryModel model);


    }
}
