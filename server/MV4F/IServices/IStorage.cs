﻿using Amazon.S3.Model;
using Microsoft.AspNetCore.Http;
using MV4F.Models;
using System.IO;
using System.Threading.Tasks;

namespace MV4F.IRepository
{
    public interface IStorage
    {
        public bool ValidateFilesSize(IFormFile file);

        public bool IsExtensionsAvailable(string extension);

        public string GenerateS3Path(string path, string fileName);

        public string GetFileExtension(IFormFile file);

        public Task<ServiceResponse<string>> UploadFile(string path, IFormFile file);

        public Task<ServiceResponse<string>> UploadFile(string path, MemoryStream stream, string fileName);

        Task<ServiceResponse<string>> UploadFile(string path, string file, string extension);

        public Task<Stream> GetFile(string path);

        public string BaseUrl { get; }

        public Task<ServiceResponse<DeleteObjectResponse>> DeleteObject(string filepath);

        Task<ApiResponse> DeleteFile(string path);

        Task<ServiceResponse<string>> SaveFile(string file, string extention);

        Task<ApiResponse> DeleteLocalFile(string imagePath);


        ImageConvert GetBase64(string path);
    }

}
