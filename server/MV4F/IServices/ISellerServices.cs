﻿using MV4F.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MV4F.IServices
{
    public interface ISellerServices
    {
        Task<ServiceResponse<long>> AddProduct(AddProductModel model);

        Task<ApiResponse> UpdateProduct(EditProductModel model);

        Task<ApiResponse> DeleteProuct(long productId);

        Task<ServiceResponse<PagedResponse<GetProductModel>>> GetSellerProduct(FilterModel model, long id);

        Task<ServiceResponse<List<SellerTransactionModel>>> SellerPaymentDetails();

        Task<ServiceResponse<List<GetBuyerNoitificationModel>>> GetAllSellerNoitification();

    }
}
