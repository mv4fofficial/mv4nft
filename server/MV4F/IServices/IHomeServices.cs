﻿using MV4F.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MV4F.IServices
{
    public interface IHomeServices
    {
        Task<ServiceResponse<PagedResponse<ViewProductModel>>> AllProducts(FilterModel model);

        Task<ServiceResponse<ViewProductModel>> ViewProductById(long Id);

        Task<ViewProductForNftModel> ViewProductForNftById(long Id);

        Task<ServiceResponse<List<RolesModel>>> AllRoles();

        Task<ServiceResponse<InterestedCategoriesModel>> UserInterestedCategoriesAndSubCategories();

        Task<ServiceResponse<object>> GetTopFollower();
    }
}
