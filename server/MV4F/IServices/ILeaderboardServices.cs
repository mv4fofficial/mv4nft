﻿using MV4F.Models;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace MV4F.IRepository
{
    public interface ILeaderboardServices
    {
        Task<ServiceResponse<List<PlayerScoreViewModel>>> ViewLeaderboard();
        Task<ApiResponse> AddPlayerScore(PlayerScoreAddModel model);
    }
}
