﻿using MV4F.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MV4F.IRepository
{
    public interface IPuzzleCategoryServices
    {
        Task<ServiceResponse<List<PuzzleCategoryViewModel>>> ViewPuzzleCategories();

        Task<ServiceResponse<PuzzleCategoryViewModel>> ViewPuzzlesByCategoryId(long id);

        Task<ApiResponse> AddPuzzleCategory(PuzzleCategoryAddModel model);
    }
}
