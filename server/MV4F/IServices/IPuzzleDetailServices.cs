﻿using MV4F.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MV4F.IRepository
{
    public interface IPuzzleDetailServices
    {
        Task<ServiceResponse<List<PuzzleDetailViewModel>>> ViewPuzzleDetails();

        Task<ServiceResponse<PuzzleDetailViewModel>> ViewPuzzleDetailById(long id);

        Task<ApiResponse> AddPuzzleDetail(AddPuzzleDetailModel model);
    }
}
