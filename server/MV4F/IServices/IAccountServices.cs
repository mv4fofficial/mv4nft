﻿using MV4F.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MV4F.IRepository
{
    public interface IAccountServices
    {
        Task<ApiResponse> Register(RegisterModel model);

        Task<ServiceResponse<LoginResponse>> Login(LoginModel model);

        Task<ServiceResponse<LoginResponse>> ThirdPartyRegisterAndLogin(ThirdPartyRegisterAndLoginModel model);

        Task<ApiResponse> ResetPassword(string email, string token, string password);

        Task<ApiResponse> ForgotPassword(GetEmail model);

        Task<ServiceResponse<LoginResponse>> RefreshToken(RefreshTokenModel model);

        Task<ApiResponse> ChangePassword(string currentEmailId, ChangePasswordModel model);

        Task<ApiResponse> ContactUs(ContactUsModel model);

        Task<ServiceResponse<ProfileViewModel>> ViewProfile(long id);

        Task<ServiceResponse<List<UserRequestViewModel>>> ViewUserRequests();

        Task<ApiResponse> UpdateProfile(ProfileUpdateModel model);

        Task<ApiResponse> UpgradeUserRole(ProfileUpdateModel model);

        Task<ApiResponse> VerifyEmail(string token);

        Task<ApiResponse> ResendVerifyEmail(string email);

        Task<ServiceResponse<List<BecomeSellerQuestionsModel>>> SellerQuestions();

        Task<ApiResponse> BecomeSeller(BecomeSellerModel model);

        Task<ServiceResponse<NewUserRole>> VerifySellerEmail(string token);

        bool IsPrime(int candidate);

        Task<ApiResponse> FollowUser(UserFollowModel model);

        Task<ServiceResponse<FollowUserData>> GetFollow(UserFollowModel model);

        Task<ServiceResponse<object>> RecommendedForYou();

        Task<ServiceResponse<object>> GetUsers(UserList userList);
    }
}
