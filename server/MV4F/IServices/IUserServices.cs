﻿using MV4F.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MV4F.IServices
{
    public interface IUserServices
    {
        Task<ApiResponse> AddToCart(long productId);

        Task<ApiResponse> RemoveFromCart(long productId);

        Task<ServiceResponse<List<ViewCartModel>>> ViewCart();

        Task<ServiceResponse<List<UserOrderModel>>> ViewUserOrders();

        Task<ServiceResponse<List<ViewUserProductModel>>> ViewUserProcuts(GetUserProductsFilter model);

        Task<ApiResponse> AddAddress(AddAddressModel model);

        Task<ServiceResponse<GetAddressModel>> GetDefaultAddress();

        Task<ApiResponse> AddWishListItem(AddWishListModel model);

        Task<ApiResponse> AddProductWishList(AddProductWishListModel model);

        Task<ApiResponse> DeleteProductWishList(DeleteProductWishListModel model);

        Task<ServiceResponse<List<ViewProductWishListModel>>> ViewProductWishLists();

        Task<ApiResponse> DeleteWishListItem(int wishListId);

        Task<ApiResponse> UpdateReadNotification(int notificationId);

        Task<ServiceResponse<List<GetBuyerNoitificationModel>>> GetAllBuyerNoitification();

        Task<ServiceResponse<List<WistListResponse>>> UserWishList();

        Task<ServiceResponse<WistListResponse>> UserWishListById(long Id);

        Task<ApiResponse> UpdateWishList(UpdateWishListModel model);


    }
}
