﻿using MV4F.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MV4F.IServices
{
    public interface IUserPuzzleDetailServices
    {
        Task<ServiceResponse<List<UserPuzzleDetailViewModel>>> ViewUserPuzzleDetails();

        Task<ApiResponse> AddUserPuzzleDetail(AddUserPuzzleDetailModel model);
    }
}
