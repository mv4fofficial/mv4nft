﻿using MV4F.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MV4F.IServices
{
    public interface IDropDownServices
    {
        Task<List<CountryModel>> GetCountry();

        Task<List<CategoriesDropDownModel>> GetCategories();

        Task<List<SubCategoriesDropDownModel>> GetSubCategories();

        Task<List<TagsDropDownModel>> GetTags();


    }
}
