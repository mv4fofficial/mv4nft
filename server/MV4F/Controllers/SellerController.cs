﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MV4F.IServices;
using MV4F.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MV4F.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SellerController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;

        public SellerController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }


        [Authorize(Roles = "Seller")]
        [HttpPost("[action]")]
        public async Task<ServiceResponse<long>> AddProduct(AddProductModel model)
        {
            if (!ModelState.IsValid)
                return new ServiceResponse<long>(400, "invalid input", 0);

            var result = await _unitOfWork.SellerServices.AddProduct(model);
            return result;
        }


        [Authorize(Roles = "Seller")]
        [HttpPost("[action]")]
        public async Task<ApiResponse> UpdateProduct(EditProductModel model)
        {
            if (!ModelState.IsValid)
                return new ApiResponse("invalid input", 400);

            var result = await _unitOfWork.SellerServices.UpdateProduct(model);
            return result;
        }


        [Authorize(Roles = "Seller")]
        [HttpDelete("[action]")]
        public async Task<ApiResponse> DeleteProuct(long productId)
        {
            if (!ModelState.IsValid)
                return new ApiResponse("invalid input", 400);

            var result = await _unitOfWork.SellerServices.DeleteProuct(productId);
            return result;
        }

        [Authorize(Roles = "Seller,Buyer")]
        [HttpPost("[action]")]
        public async Task<ServiceResponse<PagedResponse<GetProductModel>>> GetSellerProduct(FilterModel model, long id = 0)
        {
            var result = await _unitOfWork.SellerServices.GetSellerProduct(model, id);
            return result;
        }



        [Authorize(Roles = "Seller")]
        [HttpGet("[action]")]
        public async Task<ServiceResponse<List<SellerTransactionModel>>> PaymentDetailsForSeller()
        {
            var result = await _unitOfWork.SellerServices.SellerPaymentDetails();
            return result;
        }

        [Authorize(Roles = "Seller,Buyer")]
        [HttpGet("[action]")]
        public async Task<ServiceResponse<List<GetBuyerNoitificationModel>>> GetAllSellerNoitification()
        {
            var result = await _unitOfWork.SellerServices.GetAllSellerNoitification();
            return result;
        }

    }
}
