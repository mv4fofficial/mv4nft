﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MV4F.IServices;
using MV4F.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MV4F.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PuzzleDetailController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;

        public PuzzleDetailController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [AllowAnonymous]
        [HttpGet("[action]")]
        public async Task<ServiceResponse<List<PuzzleDetailViewModel>>> ViewPuzzleDetails()
        {
            var result = await _unitOfWork.PuzzleDetailServices.ViewPuzzleDetails();
            return result;
        }

        [AllowAnonymous]
        [HttpGet("[action]")]
        public async Task<ServiceResponse<PuzzleDetailViewModel>> ViewPuzzleDetailById(long id = 0)
        {
            var result = await _unitOfWork.PuzzleDetailServices.ViewPuzzleDetailById(id);
            return result;
        }

        [Authorize(Roles = "Admin")]
        [HttpPost("[action]")]
        public async Task<ApiResponse> AddPuzzleDetail(AddPuzzleDetailModel model)
        {
            if (!ModelState.IsValid)
                return new ApiResponse("Invalid Input", 400);

            var result = await _unitOfWork.PuzzleDetailServices.AddPuzzleDetail(model);
            return result;
        }

    }
}
