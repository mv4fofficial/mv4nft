﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MV4F.IServices;
using MV4F.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MV4F.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PuzzleCategoriesController
    {
        private readonly IUnitOfWork _unitOfWork;

        public PuzzleCategoriesController(IUnitOfWork unityOfWork)
        {
            _unitOfWork = unityOfWork;
        }

        [Authorize]
        [HttpGet("[action]")]
        public async Task<ServiceResponse<List<PuzzleCategoryViewModel>>> ViewPuzzleCategories() 
        {
            var result = await _unitOfWork.PuzzleCategoryServices.ViewPuzzleCategories();
            return result;
        }

        [Authorize]
        [HttpGet("[action]")]
        public async Task<ServiceResponse<PuzzleCategoryViewModel>> ViewPuzzlesByCategoryId(long id)
        {
            var result = await _unitOfWork.PuzzleCategoryServices.ViewPuzzlesByCategoryId(id);
            return result;
        }

        [Authorize]
        [HttpPost("[action]")]
        public async Task<ApiResponse> AddPuzzleCategory(PuzzleCategoryAddModel model)
        {
            var result = await _unitOfWork.PuzzleCategoryServices.AddPuzzleCategory(model);
            return result;
        }
    }
}
