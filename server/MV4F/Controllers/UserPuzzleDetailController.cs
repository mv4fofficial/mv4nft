﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MV4F.IServices;
using MV4F.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MV4F.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class UserPuzzleDetailController : ControllerBase
	{
		private readonly IUnitOfWork _unitOfWork;

		public UserPuzzleDetailController(IUnitOfWork unitOfWork)
		{
			_unitOfWork = unitOfWork;
		}

		[Authorize]
		[HttpGet("[action]")]
		public async Task<ServiceResponse<List<UserPuzzleDetailViewModel>>> ViewUserPuzzleDetails()
		{
			var result = await _unitOfWork.UserPuzzleDetailServices.ViewUserPuzzleDetails();
			return result;
		}

		[Authorize]
		[HttpPost("[action]")]
		public async Task<ApiResponse> AddUserGameAsset(AddUserPuzzleDetailModel model)
		{
			if (!ModelState.IsValid)
				return new ApiResponse("Invalid Input", 400);

			var result = await _unitOfWork.UserPuzzleDetailServices.AddUserPuzzleDetail(model);
			return result;
		}
	}
}
