﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MV4F.IServices;
using MV4F.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MV4F.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdminController : ControllerBase
    {

        private readonly IUnitOfWork _unitOfWork;

        public AdminController(IUnitOfWork unitOfWork)
        {

            _unitOfWork = unitOfWork;
        }


        [Authorize(Roles = "Admin")]
        [HttpPost("[action]")]
        public async Task<ApiResponse> AddCatagory(AddCategoryModel model)
        {
            if (!ModelState.IsValid)
                return new ApiResponse("Invalid Input", 400);

            var result = await _unitOfWork.AdminServices.AddCatagory(model);
            return result;
        }

        [Authorize(Roles = "Admin")]
        [HttpPost("[action]")]
        public async Task<ApiResponse> ActionUserRequest(ActionRequestModel model)
        {
            if (!ModelState.IsValid)
                return new ApiResponse("Invalid Input", 400);

            var result = await _unitOfWork.AdminServices.ActionUserRequest(model);
            return result;
        }

        [Authorize(Roles = "Admin")]
        [HttpPost("[action]")]
        public async Task<ApiResponse> AddSubCatagory(AddSubCategoryModel model)
        {
            if (!ModelState.IsValid)
                return new ApiResponse("Invalid Input", 400);

            var result = await _unitOfWork.AdminServices.AddSubCategory(model);
            return result;
        }


        [Authorize(Roles = "Admin")]
        [HttpPost("[action]")]
        public async Task<ApiResponse> AddTag(AddTagModel model)
        {
            if (!ModelState.IsValid)
                return new ApiResponse("Invalid Input", 400);

            var result = await _unitOfWork.AdminServices.AddTags(model);
            return result;
        }



        [Authorize(Roles = "Admin")]
        [HttpPut("[action]")]
        public async Task<ApiResponse> UpdateCatagory(UpdateCategotyModel model)
        {
            if (!ModelState.IsValid)
                return new ApiResponse("Invalid Input", 400);

            var result = await _unitOfWork.AdminServices.UpdateCatagory(model);
            return result;
        }


        [Authorize(Roles = "Admin")]
        [HttpPut("[action]")]
        public async Task<ApiResponse> UpdateSubCatagory(UpdateSubCategotyModel model)
        {
            if (!ModelState.IsValid)
                return new ApiResponse("Invalid Input", 400);

            var result = await _unitOfWork.AdminServices.UpdateSubCatagory(model);
            return result;
        }

        [Authorize(Roles = "Admin")]
        [HttpPut("[action]")]
        public async Task<ApiResponse> UpdateTag(UpdateTagModel model)
        {
            if (!ModelState.IsValid)
                return new ApiResponse("Invalid Input", 400);

            var result = await _unitOfWork.AdminServices.UpdateTag(model);
            return result;
        }


        [Authorize(Roles = "Admin")]
        [HttpDelete("[action]")]
        public async Task<ApiResponse> DeleteCatagory(int Id)
        {
            if (!ModelState.IsValid)
                return new ApiResponse("Invalid Input", 400);

            var result = await _unitOfWork.AdminServices.DeleteCatagory(Id);
            return result;
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete("[action]")]
        public async Task<ApiResponse> DeleteSubCatagory(int Id)
        {
            if (!ModelState.IsValid)
                return new ApiResponse("Invalid Input", 400);

            var result = await _unitOfWork.AdminServices.DeleteSubCatagory(Id);
            return result;
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete("[action]")]
        public async Task<ApiResponse> DeleteTag(int Id)
        {
            if (!ModelState.IsValid)
                return new ApiResponse("Invalid Input", 400);

            var result = await _unitOfWork.AdminServices.DeleteTag(Id);
            return result;
        }

        [AllowAnonymous]
        [HttpGet("[action]")]
        public async Task<ServiceResponse<List<UpdateCategotyModel>>> ViewAllCategories()
        {
            var result = await _unitOfWork.AdminServices.ViewAllCategories();
            return result;
        }


        [Authorize(Roles = "Admin")]
        [HttpGet("[action]")]
        public async Task<ServiceResponse<List<RegisteredUserModel>>> ViewAllUsersByRole(long roleid)
        {
            if (!ModelState.IsValid)
                return new ServiceResponse<List<RegisteredUserModel>>(StatusCodes.Status400BadRequest, "invalid input", null);
            var result = await _unitOfWork.AdminServices.ViewAllUsers(roleid);
            return result;
        }

        [Authorize(Roles = "Admin")]
        [HttpGet("[action]")]
        public async Task<ServiceResponse<List<PaymentLedgerResponse>>> ViewAllPaymentDetails()
        {
            var result = await _unitOfWork.AdminServices.ViewAllPaymentDetails();
            return result;
        }


        [Authorize(Roles = "Admin")]
        [HttpPost("[action]")]
        public async Task<ServiceResponse<ProductSummary>> ProductSummary(ProductSummaryModel model)
        {
            if (!ModelState.IsValid)
                return new ServiceResponse<ProductSummary>(StatusCodes.Status400BadRequest, "invalid input", null);

            var result = await _unitOfWork.AdminServices.ProductsSummary(model);
            return result;
        }


    }
}
