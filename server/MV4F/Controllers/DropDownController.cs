﻿using Microsoft.AspNetCore.Mvc;
using MV4F.IServices;
using MV4F.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MV4F.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DropDownController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;

        public DropDownController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }


        [HttpGet("[action]")]
        public async Task<List<CountryModel>> GetCountry()
        {
            var Query = await _unitOfWork.DropDownServices.GetCountry();
            return Query;
        }

        [HttpGet("[action]")]
        public async Task<List<CategoriesDropDownModel>> GetCategories()
        {
            var Query = await _unitOfWork.DropDownServices.GetCategories();
            return Query;
        }

        [HttpGet("[action]")]
        public async Task<List<SubCategoriesDropDownModel>> GetSubCategories()
        {
            var Query = await _unitOfWork.DropDownServices.GetSubCategories();
            return Query;
        }

        [HttpGet("[action]")]
        public async Task<List<TagsDropDownModel>> GetTags()
        {
            var Query = await _unitOfWork.DropDownServices.GetTags();
            return Query;
        }
    }
}
