﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MV4F.IServices;
using MV4F.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MV4F.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BuyerController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;

        public BuyerController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        //[Authorize(Roles = "Buyer")]
        [Authorize(Roles = "Buyer,Seller")]
        [HttpPost("[action]")]
        public async Task<ApiResponse> AddToCart(long productId)
        {
            if (!ModelState.IsValid)
                return new ApiResponse("invalid input", 400);
            var result = await _unitOfWork.UserServices.AddToCart(productId);
            return result;
        }


        [Authorize(Roles = "Buyer,Seller")]
        [HttpPost("[action]")]
        public async Task<ApiResponse> RemoveFromCart(long cartId)
        {
            if (!ModelState.IsValid)
                return new ApiResponse("invalid input", StatusCodes.Status400BadRequest);
            var result = await _unitOfWork.UserServices.RemoveFromCart(cartId);
            return result;
        }

        [Authorize(Roles = "Buyer,Seller")]
        [HttpGet("[action]")]
        public async Task<ServiceResponse<List<ViewCartModel>>> ViewCart()
        {
            var result = await _unitOfWork.UserServices.ViewCart();
            return result;
        }

        [Authorize(Roles = "Buyer,Seller")]
        [HttpGet("[action]")]
        public async Task<ServiceResponse<List<UserOrderModel>>> ViewUserOrders()
        {
            var result = await _unitOfWork.UserServices.ViewUserOrders();
            return result;
        }

        [Authorize(Roles = "Buyer,Seller")]
        [HttpPost("[action]")]
        public async Task<ServiceResponse<List<ViewUserProductModel>>> ViewUserProcuts(GetUserProductsFilter model)
        {
            var results = await _unitOfWork.UserServices.ViewUserProcuts(model);
            return results;
        }

        [Authorize(Roles = "Buyer,Seller")]
        [HttpPost("[action]")]
        public async Task<ApiResponse> AddAddress(AddAddressModel model)
        {
            if (!ModelState.IsValid)
                return new ApiResponse("invalid input", StatusCodes.Status400BadRequest);

            var result = await _unitOfWork.UserServices.AddAddress(model);
            return result;
        }

        [Authorize(Roles = "Buyer,Seller")]
        [HttpGet("[action]")]
        public async Task<ServiceResponse<GetAddressModel>> GetDefaultAddress()
        {
            var result = await _unitOfWork.UserServices.GetDefaultAddress();
            return result;
        }


        [Authorize(Roles = "Buyer,Seller")]
        [HttpPost("[action]")]
        public async Task<ApiResponse> AddWishListItem(AddWishListModel model)
        {
            if (!ModelState.IsValid)
                return new ApiResponse("invalid input", StatusCodes.Status400BadRequest);

            var result = await _unitOfWork.UserServices.AddWishListItem(model);
            return result;

        }

        [Authorize]
        [HttpPost("[action]")]
        public async Task<ApiResponse> AddProductWishList(AddProductWishListModel model)
        {
            if (!ModelState.IsValid)
                return new ApiResponse("invalid input", StatusCodes.Status400BadRequest);

            var result = await _unitOfWork.UserServices.AddProductWishList(model);
            return result;

        }

        [Authorize]
        [HttpPost("[action]")]
        public async Task<ApiResponse> DeleteProductWishList(DeleteProductWishListModel model)
        {
            if (!ModelState.IsValid)
                return new ApiResponse("invalid input", StatusCodes.Status400BadRequest);

            var result = await _unitOfWork.UserServices.DeleteProductWishList(model);
            return result;

        }
        [Authorize]
        [HttpGet("[action]")]
        public async Task<ServiceResponse<List<ViewProductWishListModel>>> ViewProductWishLists()
        {
            var result = await _unitOfWork.UserServices.ViewProductWishLists();
            return result;
        }

        [Authorize(Roles = "Buyer,Seller")]
        [HttpDelete("[action]")]
        public async Task<ApiResponse> DeleteWishListItem(int wishListId)
        {
            if (!ModelState.IsValid)
                return new ApiResponse("invalid input", StatusCodes.Status400BadRequest);

            var result = await _unitOfWork.UserServices.DeleteWishListItem(wishListId);
            return result;
        }


        [Authorize(Roles = "Buyer,Seller")]
        [HttpPost("[action]")]
        public async Task<ApiResponse> UpdateReadNotification(int notificationId)
        {
            var result = await _unitOfWork.UserServices.UpdateReadNotification(notificationId);
            return result;
        }

        [Authorize(Roles = "Buyer,Seller")]
        [HttpPost("[action]")]
        public async Task<ServiceResponse<List<GetBuyerNoitificationModel>>> GetAllBuyerNoitification()
        {
            var result = await _unitOfWork.UserServices.GetAllBuyerNoitification();
            return result;
        }

        [Authorize(Roles = "Buyer,Seller")]
        [HttpPost("[action]")]
        public async Task<ServiceResponse<WistListResponse>> UserWishListById(long Id)
        {
            var result = await _unitOfWork.UserServices.UserWishListById(Id);
            return result;

        }

        [Authorize(Roles = "Buyer,Seller")]
        [HttpGet("[action]")]
        public async Task<ServiceResponse<List<WistListResponse>>> UserWishList()
        {
            var result = await _unitOfWork.UserServices.UserWishList();
            return result;
        }

        [Authorize(Roles = "Buyer,Seller")]
        [HttpPost("[action]")]
        public async Task<ApiResponse> UpdateWishList(UpdateWishListModel model)
        {
            var result = await _unitOfWork.UserServices.UpdateWishList(model);
            return result;
        }

    }
}
