﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MV4F.IServices;
using MV4F.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MV4F.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GameAssetCategoriesController
    {
        private readonly IUnitOfWork _unitOfWork;

        public GameAssetCategoriesController(IUnitOfWork unityOfWork)
        {
            _unitOfWork = unityOfWork;
        }

        [Authorize]
        [HttpGet("[action]")]
        public async Task<ServiceResponse<List<GameAssetCategoryViewModel>>> ViewGameAssetCategories() 
        {
            var result = await _unitOfWork.GameAssetCategoryServices.ViewGameAssetCategories();
            return result;
        }

        [Authorize]
        [HttpGet("[action]")]
        public async Task<ServiceResponse<GameAssetCategoryViewModel>> ViewGameAssetsByCategoryId(long id)
        {
            var result = await _unitOfWork.GameAssetCategoryServices.ViewGameAssetsByCategoryId(id);
            return result;
        }

        [Authorize]
        [HttpPost("[action]")]
        public async Task<ApiResponse> AddGameAssetCategory(GameAssetCategoryAddModel model)
        {
            var result = await _unitOfWork.GameAssetCategoryServices.AddGameAssetCategory(model);
            return result;
        }
    }
}
