﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MV4F.IServices;
using MV4F.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MV4F.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;

        public AccountController(IUnitOfWork unitOfWork)
        {
            //acvd
            _unitOfWork = unitOfWork;
        }

        [AllowAnonymous]
        [HttpPut("[action]")]
        public async Task<ServiceResponse<LoginResponse>> Login([FromBody] LoginModel model)
        {
            if (!ModelState.IsValid)
                return new ServiceResponse<LoginResponse>(400, "Invalid Input", null);

            var result = await _unitOfWork.AccountServices.Login(model);
            return result;
        }

        [AllowAnonymous]
        [HttpPut("[action]")]
        public async Task<ServiceResponse<LoginResponse>> ThirdPartyRegisterAndLogin([FromBody] ThirdPartyRegisterAndLoginModel model)
        {
           // if (!ModelState.IsValid)
           //     return new ServiceResponse<LoginResponse>(400, "Invalid Input", null);

            var result = await _unitOfWork.AccountServices.ThirdPartyRegisterAndLogin(model);
            return result;
        }

       
        [AllowAnonymous]
        [HttpPost("[action]")]
        public async Task<ApiResponse> Register([FromBody] RegisterModel model)
        {
            if (!ModelState.IsValid)
                return new ApiResponse("Invalid Input", 400);

            var result = await _unitOfWork.AccountServices.Register(model);
            return result;
        }


        [AllowAnonymous]
        [HttpPost("[action]")]
        public async Task<ApiResponse> VerifyEmail(string token)
        {
            if (!ModelState.IsValid)
                return new ApiResponse("invalid token", StatusCodes.Status400BadRequest);

            var result = await _unitOfWork.AccountServices.VerifyEmail(token);
            return result;
        }

        [AllowAnonymous]
        [HttpPost("[action]")]
        public async Task<ApiResponse> ResendVerifyEmail(string email)
        {
            if (!ModelState.IsValid)
                return new ApiResponse("invalid Email", StatusCodes.Status400BadRequest);
            var result = await _unitOfWork.AccountServices.ResendVerifyEmail(email);
            return result;
        }


        [AllowAnonymous]
        [HttpPost("[action]")]
        public async Task<ApiResponse> ResetPassword(ResetPasswordModel model)
        {
            if (!ModelState.IsValid)
                return new ApiResponse("Invalid Input", 400);

            var res = await _unitOfWork.AccountServices.ResetPassword(model.Email, model.Token, model.Password);
            return res;
        }


        [AllowAnonymous]
        [HttpPut("[action]")]
        public async Task<ServiceResponse<LoginResponse>> RefreshToken([FromBody] RefreshTokenModel model)
        {
            if (model.RefreshToken is null)
                return new ServiceResponse<LoginResponse>(400, "RefrestToken Parameter is Mandatory", null);

            var result = await _unitOfWork.AccountServices.RefreshToken(model);
            return result;
        }

        [AllowAnonymous]
        [HttpPost("[action]")]
        public async Task<ApiResponse> ForgotPassword([FromBody] GetEmail model)
        {
            if (!ModelState.IsValid)
                return new ApiResponse("Invalid Input", 400);

            var result = await _unitOfWork.AccountServices.ForgotPassword(model);
            return result;
        }


        [Authorize]
        [HttpPatch("[action]")]
        public async Task<ApiResponse> UpdatePassword(ChangePasswordModel model)
        {
            if (!ModelState.IsValid)
                return new ApiResponse("Invalid Input", 400);

            var result = await _unitOfWork.AccountServices.ChangePassword(_unitOfWork.CurrentUser.Email, model);
            return result;
        }


        [AllowAnonymous]
        [HttpPost("[action]")]
        public async Task<ApiResponse> ContactUs(ContactUsModel model)
        {
            if (!ModelState.IsValid)
                return new ApiResponse("Invalid Input", 400);

            var result = await _unitOfWork.AccountServices.ContactUs(model);
            return result;

        }


        [AllowAnonymous]
        [HttpGet("[action]")]
        public async Task<ServiceResponse<ProfileViewModel>> ViewProfile(long id = 0)
        {
            var result = await _unitOfWork.AccountServices.ViewProfile(id);
            return result;
        }

        [Authorize(Roles = "Admin")]
        [HttpGet("[action]")]
        public async Task<ServiceResponse<List<UserRequestViewModel>>> ViewUserRequests()
        {
            var result = await _unitOfWork.AccountServices.ViewUserRequests();
            return result;
        }

        [Authorize(Roles = "Admin ,Buyer ,Seller")]
        [HttpPost("[action]")]
        public async Task<ApiResponse> UpdateProfile(ProfileUpdateModel model)
        {
            if (!ModelState.IsValid)
                return new ApiResponse("invalid input", 400);

            var res = await _unitOfWork.AccountServices.UpdateProfile(model);
            return res;
        }

        [Authorize(Roles = "Admin ,Buyer ,Seller")]
        [HttpPost("[action]")]
        public async Task<ApiResponse> UpgradeUserRole(ProfileUpdateModel model)
        {
            if (!ModelState.IsValid)
                return new ApiResponse("invalid input", 400);

            var res = await _unitOfWork.AccountServices.UpgradeUserRole(model);
            return res;
        }

        [AllowAnonymous]
        [HttpGet("[action]")]
        public async Task<ServiceResponse<List<BecomeSellerQuestionsModel>>> SellerQuestions()
        {
            var result = await _unitOfWork.AccountServices.SellerQuestions();
            return result;
        }


        [Authorize(Roles = "Buyer")]
        [HttpPost("[action]")]
        public async Task<ApiResponse> BecomeSeller(BecomeSellerModel model)
        {
            if (!ModelState.IsValid)
                return new ApiResponse("Invalid Input", 400);

            var result = await _unitOfWork.AccountServices.BecomeSeller(model);
            return result;
        }


        //[AllowAnonymous]
        //[HttpPost("[action]")]
        //public async Task<ServiceResponse<NewUserRole>> VerifySellerEmail(string token)
        //{
        //    if (!ModelState.IsValid)
        //        return new ServiceResponse<NewUserRole>( StatusCodes.Status400BadRequest,"invalid token",null);

        //    var result = await _unitOfWork.AccountServices.VerifySellerEmail(token);
        //    return result;
        //}

        [Authorize(Roles = "Buyer ,Seller")]
        [HttpPost("[action]")]
        public async Task<ApiResponse> FollowUser(UserFollowModel model)
        {
            if (!ModelState.IsValid)
                return new ApiResponse("invalid input", 400);

            var res = await _unitOfWork.AccountServices.FollowUser(model);
            return res;
        }

        // [Authorize(Roles = "Buyer ,Seller")]
        [HttpPost("[action]")]
        public async Task<ServiceResponse<FollowUserData>> GetFollow(UserFollowModel model)
        {
            var res = await _unitOfWork.AccountServices.GetFollow(model);
            return res;
        }

        [Authorize(Roles = "Buyer ,Seller")]
        [HttpGet("[action]")]
        public async Task<ServiceResponse<object>> RecommendedForYou()
        {
            var res = await _unitOfWork.AccountServices.RecommendedForYou();
            return res;
        }

        // [Authorize(Roles = "Buyer ,Seller")]
        [HttpPost("[action]")]
        public async Task<ServiceResponse<object>> GetUsers(UserList userList)
        {
            var res = await _unitOfWork.AccountServices.GetUsers(userList);
            return res;
        }
    }
}
