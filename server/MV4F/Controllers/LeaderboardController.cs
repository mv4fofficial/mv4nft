﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MV4F.IServices;
using MV4F.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MV4F.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class LeaderboardController : ControllerBase
	{
		private readonly IUnitOfWork _unitOfWork;

		public LeaderboardController(IUnitOfWork unitOfWork)
		{
			_unitOfWork = unitOfWork;
		}

		[Authorize]
		[HttpGet("[action]")]
		public async Task<ServiceResponse<List<PlayerScoreViewModel>>> ViewLeaderboard()
		{
			var result = await _unitOfWork.LeaderboardServices.ViewLeaderboard();
			return result;
		}

		[Authorize]
		[HttpPost("[action]")]
		public async Task<ApiResponse> AddPlayerScore(PlayerScoreAddModel model)
		{
			if (!ModelState.IsValid)
				return new ApiResponse("Invalid Input", 400);


			var result = await _unitOfWork.LeaderboardServices.AddPlayerScore(model);
			return result;
		}

	}
}
