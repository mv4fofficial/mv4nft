﻿
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MV4F.IServices;
using MV4F.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MV4F.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;

        public HomeController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }


        [AllowAnonymous]
        [HttpPost("[action]")]
        public async Task<ServiceResponse<PagedResponse<ViewProductModel>>> AllProducts(FilterModel model)
        {
            var results = await _unitOfWork.HomeServices.AllProducts(model);
            return results;
        }


        [AllowAnonymous]
        [HttpGet("[action]")]
        public async Task<ServiceResponse<ViewProductModel>> ViewProductById(long Id)
        {
            if (!ModelState.IsValid)
                return new ServiceResponse<ViewProductModel>(StatusCodes.Status400BadRequest, "invalid input", null);

            var result = await _unitOfWork.HomeServices.ViewProductById(Id);
            return result;
        }

        [AllowAnonymous]
        [HttpGet("[action]/{Id}")]
        public async Task<ViewProductForNftModel> ViewProductForNftById(long Id)
        {
            var result = await _unitOfWork.HomeServices.ViewProductForNftById(Id);
            return result;
        }

        [AllowAnonymous]
        [HttpGet("[action]")]
        public async Task<ServiceResponse<List<RolesModel>>> AllRoles()
        {
            var result = await _unitOfWork.HomeServices.AllRoles();
            return result;
        }


        [Authorize(Roles = "Buyer ,Seller")]
        [HttpGet("[action]")]
        public async Task<ServiceResponse<InterestedCategoriesModel>> UserInterestedCategoriesAndSubCategories()
        {
            var res = await _unitOfWork.HomeServices.UserInterestedCategoriesAndSubCategories();
            return res;
        }


        [HttpGet("[action]")]
        public async Task<ServiceResponse<object>> GetTopFollower()
        {
            var res = await _unitOfWork.HomeServices.GetTopFollower();
            return res;
        }
    }
}
