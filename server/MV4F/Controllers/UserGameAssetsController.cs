﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using MV4F.Entities;
using MV4F.IServices;
using MV4F.Models;

namespace MV4F.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class UserGameAssetsController : ControllerBase
	{
		private readonly IUnitOfWork _unitOfWork;

		public UserGameAssetsController(IUnitOfWork unitOfWork)
		{
			_unitOfWork = unitOfWork;
		}

        [Authorize]
		[HttpGet("[action]")]
		public async Task<ServiceResponse<List<UserGameAssetsViewModel>>> ViewUserGameAssets()
		{
			var result = await _unitOfWork.UserGameAssetsServices.ViewUserGameAssets();
			return result;
		}

		[Authorize]
		[HttpGet("[action]")]
		public async Task<ServiceResponse<List<UserGameAssetsViewModel>>> ViewInProgressPuzzles()
		{
			var result = await _unitOfWork.UserGameAssetsServices.ViewInProgressPuzzles();
			return result;
		}


		[Authorize]
        [HttpPost("[action]")]
        public async Task<ApiResponse> AddUserGameAsset(AddUserGameAssetModel model)
        {
            if (!ModelState.IsValid)
                return new ApiResponse("Invalid Input", 400);

            var result = await _unitOfWork.UserGameAssetsServices.AddUserGameAsset(model);
            return result;
        }

		[Authorize]
        [HttpPatch("[action]")]
        public async Task<ApiResponse> UpdateProgress(ProgressUpdateModel model)
        {
            if (!ModelState.IsValid)
                return new ApiResponse("Invalid Input", 400);

            var result = await _unitOfWork.UserGameAssetsServices.UpdateProgress(model);
            return result;
        }
	}

}