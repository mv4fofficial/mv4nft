﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MV4F.IServices;
using MV4F.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MV4F.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GameController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;

        public GameController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [AllowAnonymous]
        [HttpGet("[action]")]
        public async Task<ServiceResponse<List<GameAssetsViewModel>>> ViewGameAssets(string type)
        {
            var result = await _unitOfWork.GameServices.ViewGameAssets(type);
            return result;
        }

        [AllowAnonymous]
        [HttpGet("[action]")]
        public async Task<ServiceResponse<GameAssetsViewModel>> ViewGameAssetById(long id = 0)
        {
            var result = await _unitOfWork.GameServices.ViewGameAssetById(id);
            return result;
        }

        [Authorize(Roles = "Admin")]
        [HttpPost("[action]")]
        public async Task<ApiResponse> AddGameAsset(AddGameAssetModel model)
        {
            if (!ModelState.IsValid)
                return new ApiResponse("Invalid Input", 400);

            var result = await _unitOfWork.GameServices.AddGameAsset(model);
            return result;
        }

    }
}
