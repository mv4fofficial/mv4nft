﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MV4F.IServices;
using MV4F.Models;
using Stripe;
using System.Threading.Tasks;

namespace MV4F.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentController : ControllerBase
    {

        private readonly IUnitOfWork _unitOfWork;

        public PaymentController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }




        [Authorize(Roles = "Buyer,Seller")]
        [HttpPost("[action]")]
        public async Task<ApiResponse> PlaceOrder([FromBody] PlaceOrderModel order)
        {
            if (!ModelState.IsValid)
                return new ApiResponse("All fields are mandatory", 400);

            var result = await _unitOfWork.PaymentService.PlaceOrder(order);
            return result;

        }


        [HttpPost("[action]")]
        public async Task<ApiResponse> PaymentTransferToSeller()
        {
            if (!ModelState.IsValid)
                return new ApiResponse("All fields are mandatory", 400);

            var result = await _unitOfWork.PaymentService.PaymentTransferToSeller();
            return result;

        }

        [HttpPost("[action]")]
        public async Task<ApiResponse> TestConnect()
        {
            if (!ModelState.IsValid)
                return new ApiResponse("All fields are mandatory", 400);

            var result = await _unitOfWork.PaymentService.TestConnect();
            return result;

        }

        [HttpPost("[action]")]
        public async Task<ServiceResponse<string>> CreateAccount(DataForStripeModel dataForStripe)
        {
            var result = await _unitOfWork.PaymentService.CreateAccount(dataForStripe);
            return result;
        }

        [HttpPost("[action]")]
        public async Task<ServiceResponse<object>> CompleteAccount(DataForStripeModel dataForStripe)
        {
            var result = await _unitOfWork.PaymentService.CompleteAccount(dataForStripe);
            return result;
        }


        [HttpPost("[action]")]
        public async Task<ServiceResponse<object>> CheckAccount(DataForStripeModel dataForStripe)
        {
            var result = await _unitOfWork.PaymentService.CheckAccount(dataForStripe);
            return result;
        }

        [HttpPost("[action]")]
        public async Task<ServiceResponse<object>> GetBalance(DataForStripeModel dataForStripe)
        {
            var result = await _unitOfWork.PaymentService.GetBalance(dataForStripe);
            return result;
        }
        

        [HttpPost("[action]")]
        public async Task<ServiceResponse<object>> FetchTransactions(DataForStripeModel dataForStripe)
        {
            var result = await _unitOfWork.PaymentService.FetchTransactions(dataForStripe);
            return result;
        }

        [HttpPost("[action]")]
        public async Task<ServiceResponse<object>> FetchStripeTransactions()
        {
            var result = await _unitOfWork.PaymentService.FetchStripeTransactions();
            return result;
        }

        [HttpPost("[action]")]
        public async Task<ServiceResponse<object>> FetchStripeConnectAccountList()
        {
            var result = await _unitOfWork.PaymentService.FetchStripeConnectAccountList();
            return result;
        }
        

        
        [HttpPost("[action]")]
        public async Task<ApiResponse> UpdateCompleteAccountStatus()
        {
            var result = await _unitOfWork.PaymentService.UpdateCompleteAccountStatus();
            return result;
        }

        
        [HttpPost("[action]")]
        public async Task<ServiceResponse<object>> CreateLoginLink(DataForStripeModel dataForStripe)
        {
            var result = await _unitOfWork.PaymentService.CreateLoginLink(dataForStripe);
            return result;
        }

        // // [Authorize(Roles = "Buyer,Seller")]
        [HttpPost("[action]")]
        public async Task<ApiResponseWithData> GetClientSecret ([FromBody] PlaceOrderModel order)
        {
            // Stripe.StripeConfiguration.SetApiKey("pk_test_51LfETrL95FNDK6SK7fW9V0g3tZQXknOzFNGIXcoFIzsGU7A7aQz0FW4LqMSUw0G7ozyGsWCtX2Glc76lxaMaZ0aC00oijko6UC");
            StripeConfiguration.ApiKey = "sk_test_51KWhwZLKdDbFtRftb2ddnfbnuHPDkF6HslE8ev7C4P8ICAGabdNg7fOS9DF4TD7NJJUmwV5GtF8xg2QKRHbLEE1600J3JGoybu";

            var options = new PaymentIntentCreateOptions
            {
                Amount = ((long?)(order.OrderAmount * 100)),
                Currency = "sgd",
                AutomaticPaymentMethods = new PaymentIntentAutomaticPaymentMethodsOptions
                {
                    Enabled = true,
                },
            };
            var service = new PaymentIntentService();
            var intent = service.Create(options);

            return new ApiResponseWithData("success", 200, intent.ClientSecret);
        }

        // [Authorize(Roles = "Buyer,Seller")]
        [HttpPost("[action]")]
        public async Task<ApiResponse> ConfirmPayment ([FromBody] ClientSecretModel secret)
        {
            // Stripe.StripeConfiguration.SetApiKey("pk_test_51LfETrL95FNDK6SK7fW9V0g3tZQXknOzFNGIXcoFIzsGU7A7aQz0FW4LqMSUw0G7ozyGsWCtX2Glc76lxaMaZ0aC00oijko6UC");
            StripeConfiguration.ApiKey = "sk_test_51KWhwZLKdDbFtRftb2ddnfbnuHPDkF6HslE8ev7C4P8ICAGabdNg7fOS9DF4TD7NJJUmwV5GtF8xg2QKRHbLEE1600J3JGoybu";

            // To create a PaymentIntent for confirmation, see our guide at: https://stripe.com/docs/payments/payment-intents/creating-payment-intents#creating-for-automatic
            var options = new PaymentIntentConfirmOptions
            {
                PaymentMethod = "pm_card_visa",
                ReturnUrl = "https://localhost:5001/swagger/index.html",
            };
            var service = new PaymentIntentService();
            service.Confirm(
                secret.ClientSecret,
                options
            );

            return new ApiResponse("success", 200);
        }
    }
}
