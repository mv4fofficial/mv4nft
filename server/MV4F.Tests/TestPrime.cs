using MV4F.IRepository;
using MV4F.IServices;
using MV4F.Controllers;
using FakeItEasy; 
using System;

namespace MV4F.UnitTests.Services
{
    public class PrimeService_IsPrimeShould
    {

        [Fact]
        public void IsPrime_InputIs1_ReturnFalse()
        {
            var dataStore = A.Fake<IUnitOfWork>();
            A.CallTo(() => dataStore.AccountServices.IsPrime(1)).Returns(false);
            // mock.Setup(p => p.AccountServices.IsPrime(1)).Returns(true);
            var primeService = new AccountController(dataStore);
            bool result = primeService.IsPrime(2);

            Assert.False(result, "1 should not be prime");
        }
    }
}