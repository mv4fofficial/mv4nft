-- MySQL dump 10.13  Distrib 8.0.28, for Win64 (x86_64)
--
-- Host: 103.50.212.140    Database: mv4f
-- ------------------------------------------------------
-- Server version	5.7.27-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `user_products`
--

DROP TABLE IF EXISTS `user_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_products` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `order_id` bigint(20) NOT NULL,
  `created_date` datetime(6) NOT NULL,
  `updated_date` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IX_user_products_order_id` (`order_id`),
  KEY `IX_user_products_product_id` (`product_id`),
  KEY `IX_user_products_user_id` (`user_id`),
  CONSTRAINT `FK_user_products_product_product_id` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_user_products_user_orders_order_id` FOREIGN KEY (`order_id`) REFERENCES `user_orders` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_user_products_user_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_products`
--

LOCK TABLES `user_products` WRITE;
/*!40000 ALTER TABLE `user_products` DISABLE KEYS */;
INSERT INTO `user_products` VALUES (1,23,1,3,'2022-03-16 09:07:35.635793','2022-03-16 09:07:35.635794'),(2,7,1,17,'2022-03-24 12:09:03.343229','2022-03-24 12:09:03.343306'),(3,7,16,17,'2022-03-24 12:09:03.633190','2022-03-24 12:09:03.633190'),(4,7,10,18,'2022-03-24 12:20:47.244134','2022-03-24 12:20:47.324498'),(5,7,2,19,'2022-03-24 12:48:47.453961','2022-03-24 12:48:47.496232'),(6,7,3,20,'2022-03-24 12:54:17.602872','2022-03-24 12:54:17.602872'),(7,7,5,22,'2022-03-25 04:53:58.215526','2022-03-25 04:53:58.258652'),(8,22,16,23,'2022-03-26 10:07:55.154283','2022-03-26 10:07:55.154284'),(9,22,37,23,'2022-03-26 10:07:55.192700','2022-03-26 10:07:55.192700'),(10,22,35,23,'2022-03-26 10:07:55.195773','2022-03-26 10:07:55.195773'),(11,22,28,23,'2022-03-26 10:07:55.198216','2022-03-26 10:07:55.198216'),(12,22,36,24,'2022-03-26 10:16:29.359108','2022-03-26 10:16:29.359108'),(13,22,29,26,'2022-03-26 10:22:32.670941','2022-03-26 10:22:32.670941'),(14,22,31,26,'2022-03-26 10:22:32.673541','2022-03-26 10:22:32.673541'),(15,22,36,27,'2022-03-26 10:25:19.790913','2022-03-26 10:25:19.790913'),(16,22,28,27,'2022-03-26 10:25:19.796922','2022-03-26 10:25:19.796922'),(17,22,28,28,'2022-03-26 17:17:06.385801','2022-03-26 17:17:06.385801'),(18,22,16,28,'2022-03-26 17:17:06.420169','2022-03-26 17:17:06.420169'),(19,22,28,29,'2022-03-26 18:21:15.685896','2022-03-26 18:21:15.685896'),(20,22,26,29,'2022-03-26 18:21:15.689100','2022-03-26 18:21:15.689100'),(21,22,16,29,'2022-03-26 18:21:15.692177','2022-03-26 18:21:15.692177'),(22,22,27,30,'2022-03-27 19:01:17.863946','2022-03-27 19:01:17.863946'),(23,22,26,30,'2022-03-27 19:01:17.900808','2022-03-27 19:01:17.900808'),(24,22,19,30,'2022-03-27 19:01:17.903928','2022-03-27 19:01:17.903928'),(25,22,1,30,'2022-03-27 19:01:17.906123','2022-03-27 19:01:17.906123'),(26,22,27,31,'2022-03-27 19:13:30.957404','2022-03-27 19:13:30.957404'),(27,22,1,32,'2022-03-28 06:03:19.101498','2022-03-28 06:03:19.101498'),(28,22,16,32,'2022-03-28 06:03:19.153238','2022-03-28 06:03:19.153238'),(29,22,4,33,'2022-03-28 15:20:23.985413','2022-03-28 15:20:23.985414'),(30,22,5,33,'2022-03-28 15:20:24.022910','2022-03-28 15:20:24.022911'),(31,33,4,34,'2022-03-30 14:36:08.086692','2022-03-30 14:36:08.086692'),(32,33,9,34,'2022-03-30 14:36:08.156028','2022-03-30 14:36:08.156028'),(33,33,5,34,'2022-03-30 14:36:08.159226','2022-03-30 14:36:08.159226'),(34,22,11,35,'2022-03-31 12:24:57.238266','2022-03-31 12:24:57.238266'),(35,22,4,35,'2022-03-31 12:24:57.289673','2022-03-31 12:24:57.289673'),(36,22,5,35,'2022-03-31 12:24:57.292209','2022-03-31 12:24:57.292209'),(37,22,6,35,'2022-03-31 12:24:57.294595','2022-03-31 12:24:57.294595'),(38,22,10,35,'2022-03-31 12:24:57.296796','2022-03-31 12:24:57.296797'),(39,45,6,36,'2022-04-11 19:33:54.348984','2022-04-11 19:33:54.348985'),(40,45,12,36,'2022-04-11 19:33:54.384777','2022-04-11 19:33:54.384777'),(41,45,5,37,'2022-04-12 08:52:11.369670','2022-04-12 08:52:11.369670'),(42,45,12,37,'2022-04-12 08:52:11.415273','2022-04-12 08:52:11.415273'),(43,33,5,38,'2022-04-12 09:05:03.381496','2022-04-12 09:05:03.381496'),(44,33,6,38,'2022-04-12 09:05:03.383863','2022-04-12 09:05:03.383863'),(45,33,7,39,'2022-04-12 09:08:04.546792','2022-04-12 09:08:04.546792');
/*!40000 ALTER TABLE `user_products` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-04-12 17:55:08
