-- MySQL dump 10.13  Distrib 8.0.28, for Win64 (x86_64)
--
-- Host: 103.50.212.140    Database: mv4f
-- ------------------------------------------------------
-- Server version	5.7.27-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `interested_categories`
--

DROP TABLE IF EXISTS `interested_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `interested_categories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `category_id` bigint(20) DEFAULT NULL,
  `sub_category_id` bigint(20) DEFAULT NULL,
  `created_date` datetime(6) NOT NULL DEFAULT '0001-01-01 00:00:00.000000',
  PRIMARY KEY (`id`),
  KEY `IX_interested_categories_category_id` (`category_id`),
  KEY `IX_interested_categories_sub_category_id` (`sub_category_id`),
  KEY `IX_interested_categories_user_id` (`user_id`),
  CONSTRAINT `FK_interested_categories_category_master_category_id` FOREIGN KEY (`category_id`) REFERENCES `category_master` (`id`),
  CONSTRAINT `FK_interested_categories_sub_category_master_sub_category_id` FOREIGN KEY (`sub_category_id`) REFERENCES `sub_category_master` (`id`),
  CONSTRAINT `FK_interested_categories_user_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `interested_categories`
--

LOCK TABLES `interested_categories` WRITE;
/*!40000 ALTER TABLE `interested_categories` DISABLE KEYS */;
INSERT INTO `interested_categories` VALUES (1,7,3,NULL,'0001-01-01 00:00:00.000000'),(2,7,14,NULL,'0001-01-01 00:00:00.000000'),(3,7,15,NULL,'0001-01-01 00:00:00.000000'),(4,7,NULL,1,'0001-01-01 00:00:00.000000'),(5,7,NULL,2,'0001-01-01 00:00:00.000000'),(6,7,NULL,3,'0001-01-01 00:00:00.000000'),(7,22,3,NULL,'0001-01-01 00:00:00.000000'),(8,22,14,NULL,'0001-01-01 00:00:00.000000'),(9,22,NULL,1,'0001-01-01 00:00:00.000000'),(10,22,NULL,2,'0001-01-01 00:00:00.000000'),(11,28,3,NULL,'0001-01-01 00:00:00.000000'),(12,28,14,NULL,'0001-01-01 00:00:00.000000'),(13,28,15,NULL,'0001-01-01 00:00:00.000000'),(14,28,NULL,1,'0001-01-01 00:00:00.000000'),(15,28,NULL,2,'0001-01-01 00:00:00.000000'),(16,22,15,NULL,'0001-01-01 00:00:00.000000'),(17,22,NULL,5,'0001-01-01 00:00:00.000000'),(18,22,17,NULL,'0001-01-01 00:00:00.000000'),(19,22,NULL,5,'0001-01-01 00:00:00.000000'),(21,22,14,NULL,'0001-01-01 00:00:00.000000'),(22,22,NULL,6,'0001-01-01 00:00:00.000000'),(23,22,3,NULL,'0001-01-01 00:00:00.000000'),(24,22,NULL,1,'0001-01-01 00:00:00.000000'),(25,22,NULL,2,'0001-01-01 00:00:00.000000'),(26,33,14,NULL,'0001-01-01 00:00:00.000000'),(27,33,NULL,2,'0001-01-01 00:00:00.000000');
/*!40000 ALTER TABLE `interested_categories` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-04-12 17:54:59
