-- MySQL dump 10.13  Distrib 8.0.28, for Win64 (x86_64)
--
-- Host: 103.50.212.140    Database: mv4f
-- ------------------------------------------------------
-- Server version	5.7.27-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `user_orders`
--

DROP TABLE IF EXISTS `user_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_orders` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `amount` decimal(16,3) NOT NULL,
  `updated_date` datetime(6) NOT NULL,
  `created_date` datetime(6) NOT NULL,
  `order_status` longtext,
  PRIMARY KEY (`id`),
  KEY `IX_user_orders_user_id` (`user_id`),
  CONSTRAINT `FK_user_orders_user_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_orders`
--

LOCK TABLES `user_orders` WRITE;
/*!40000 ALTER TABLE `user_orders` DISABLE KEYS */;
INSERT INTO `user_orders` VALUES (1,23,100.000,'0001-01-01 00:00:00.000000','2022-03-16 08:58:41.216882','Expired API Key provided: sk_test_*********************************************************************************************ZRCPKl'),(2,23,100.000,'0001-01-01 00:00:00.000000','2022-03-16 09:01:53.143553','Payment Inititated'),(3,23,100.000,'0001-01-01 00:00:00.000000','2022-03-16 09:04:06.924448','succeeded'),(4,23,100.000,'0001-01-01 00:00:00.000000','2022-03-16 09:57:01.634649','Payment Inititated'),(5,23,100.000,'0001-01-01 00:00:00.000000','2022-03-16 09:57:55.012012','Expired API Key provided: sk_test_*********************************************************************************************ZRCPKl'),(6,23,100.000,'0001-01-01 00:00:00.000000','2022-03-16 09:59:58.496770','Expired API Key provided: sk_test_*********************************************************************************************ZRCPKl'),(7,23,100.000,'0001-01-01 00:00:00.000000','2022-03-16 10:01:02.227250','Expired API Key provided: sk_test_*********************************************************************************************ZRCPKl'),(8,23,100.000,'0001-01-01 00:00:00.000000','2022-03-16 10:03:24.991101','Payment Inititated'),(9,23,100.000,'0001-01-01 00:00:00.000000','2022-03-16 10:05:07.575356','Payment Inititated'),(10,23,100.000,'0001-01-01 00:00:00.000000','2022-03-16 10:06:30.821234','Payment Inititated'),(11,23,100.000,'0001-01-01 00:00:00.000000','2022-03-16 10:07:01.902223','Payment Inititated'),(12,23,100.000,'0001-01-01 00:00:00.000000','2022-03-16 10:08:12.063357','Payment Inititated'),(13,22,0.000,'0001-01-01 00:00:00.000000','2022-03-23 13:53:58.434853','Payment Inititated'),(14,22,0.000,'0001-01-01 00:00:00.000000','2022-03-23 13:57:24.603873','Payment Inititated'),(15,7,1000.000,'0001-01-01 00:00:00.000000','2022-03-24 11:51:53.939996','Payment Inititated'),(16,7,109.000,'0001-01-01 00:00:00.000000','2022-03-24 12:04:00.964064','Expired API Key provided: sk_test_*********************************************************************************************ZRCPKl'),(17,7,100.000,'0001-01-01 00:00:00.000000','2022-03-24 12:08:50.045968','succeeded'),(18,7,675.000,'0001-01-01 00:00:00.000000','2022-03-24 12:19:33.851833','succeeded'),(19,7,12.000,'0001-01-01 00:00:00.000000','2022-03-24 12:48:06.504821','succeeded'),(20,7,37.000,'0001-01-01 00:00:00.000000','2022-03-24 12:54:15.516537','succeeded'),(21,22,4502.000,'0001-01-01 00:00:00.000000','2022-03-24 18:10:37.380495','No such token: \'tok_1Kgv7JSFYwylPfKh9cguyBzy\''),(22,7,24.000,'0001-01-01 00:00:00.000000','2022-03-25 04:53:27.906007','succeeded'),(23,22,4669.000,'0001-01-01 00:00:00.000000','2022-03-26 10:07:52.773556','succeeded'),(24,22,1000.000,'0001-01-01 00:00:00.000000','2022-03-26 10:16:27.917104','succeeded'),(25,22,0.000,'0001-01-01 00:00:00.000000','2022-03-26 10:21:47.712323','This value must be greater than or equal to 1.'),(26,22,100.000,'0001-01-01 00:00:00.000000','2022-03-26 10:22:31.481894','succeeded'),(27,22,1000.000,'0001-01-01 00:00:00.000000','2022-03-26 10:25:18.277156','succeeded'),(28,22,4356.000,'0001-01-01 00:00:00.000000','2022-03-26 17:17:04.508242','succeeded'),(29,22,4479.000,'0001-01-01 00:00:00.000000','2022-03-26 18:21:10.282749','succeeded'),(30,22,469.000,'0001-01-01 00:00:00.000000','2022-03-27 19:01:15.740993','succeeded'),(31,22,123.000,'0001-01-01 00:00:00.000000','2022-03-27 19:13:29.532098','succeeded'),(32,22,4456.000,'0001-01-01 00:00:00.000000','2022-03-28 06:03:16.968809','succeeded'),(33,22,258.000,'0001-01-01 00:00:00.000000','2022-03-28 15:20:21.903661','succeeded'),(34,33,292.000,'0001-01-01 00:00:00.000000','2022-03-30 14:36:05.540925','succeeded'),(35,22,979.000,'0001-01-01 00:00:00.000000','2022-03-31 12:24:54.893010','succeeded'),(36,45,436.000,'0001-01-01 00:00:00.000000','2022-04-11 19:33:52.167287','succeeded'),(37,45,459.000,'0001-01-01 00:00:00.000000','2022-04-12 08:52:09.248487','succeeded'),(38,33,25.000,'0001-01-01 00:00:00.000000','2022-04-12 09:05:01.827871','succeeded'),(39,33,34.000,'0001-01-01 00:00:00.000000','2022-04-12 09:08:03.012537','succeeded');
/*!40000 ALTER TABLE `user_orders` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-04-12 17:54:43
