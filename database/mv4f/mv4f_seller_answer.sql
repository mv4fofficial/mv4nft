-- MySQL dump 10.13  Distrib 8.0.28, for Win64 (x86_64)
--
-- Host: 103.50.212.140    Database: mv4f
-- ------------------------------------------------------
-- Server version	5.7.27-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `seller_answer`
--

DROP TABLE IF EXISTS `seller_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `seller_answer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `question_id` bigint(20) NOT NULL,
  `answer` longtext,
  `created_date` datetime(6) NOT NULL DEFAULT '0001-01-01 00:00:00.000000',
  PRIMARY KEY (`id`),
  KEY `IX_seller_answer_question_id` (`question_id`),
  KEY `IX_seller_answer_user_id` (`user_id`),
  CONSTRAINT `FK_seller_answer_become_seller_question_master_question_id` FOREIGN KEY (`question_id`) REFERENCES `become_seller_question_master` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_seller_answer_user_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seller_answer`
--

LOCK TABLES `seller_answer` WRITE;
/*!40000 ALTER TABLE `seller_answer` DISABLE KEYS */;
INSERT INTO `seller_answer` VALUES (1,22,1,'','0001-01-01 00:00:00.000000'),(2,22,3,'','0001-01-01 00:00:00.000000'),(3,22,4,'\\ProductImages\\48ec84ff-a4f7-46d9-a55a-f220e0b188d3.pdf','0001-01-01 00:00:00.000000'),(4,22,1,'','0001-01-01 00:00:00.000000'),(5,22,3,'','0001-01-01 00:00:00.000000'),(6,22,4,'\\ProductImages\\f9ce6443-620b-4b67-8df8-f1ba57cb70fc.pdf','0001-01-01 00:00:00.000000'),(7,22,1,'que one','0001-01-01 00:00:00.000000'),(8,22,3,'Artwork','0001-01-01 00:00:00.000000'),(9,22,4,'\\ProductImages\\cd554b14-7988-44e0-a1d3-f65b8160b712.pdf','0001-01-01 00:00:00.000000'),(10,33,1,'3D images','0001-01-01 00:00:00.000000'),(11,33,3,'www.lookupitsolutions.com','0001-01-01 00:00:00.000000'),(12,33,4,'\\ProductImages\\f7a91683-d45e-47a2-b3fb-9947d27da327.pdf','0001-01-01 00:00:00.000000');
/*!40000 ALTER TABLE `seller_answer` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-04-12 17:54:47
