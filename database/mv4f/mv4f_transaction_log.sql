-- MySQL dump 10.13  Distrib 8.0.28, for Win64 (x86_64)
--
-- Host: 103.50.212.140    Database: mv4f
-- ------------------------------------------------------
-- Server version	5.7.27-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `transaction_log`
--

DROP TABLE IF EXISTS `transaction_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transaction_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `created_date` datetime(6) NOT NULL,
  `order_id` bigint(20) NOT NULL,
  `amount` decimal(16,3) NOT NULL,
  `order_status` longtext,
  PRIMARY KEY (`id`),
  KEY `IX_transaction_log_order_id` (`order_id`),
  KEY `IX_transaction_log_product_id` (`product_id`),
  KEY `IX_transaction_log_user_id` (`user_id`),
  CONSTRAINT `FK_transaction_log_product_product_id` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_transaction_log_user_orders_order_id` FOREIGN KEY (`order_id`) REFERENCES `user_orders` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_transaction_log_user_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction_log`
--

LOCK TABLES `transaction_log` WRITE;
/*!40000 ALTER TABLE `transaction_log` DISABLE KEYS */;
INSERT INTO `transaction_log` VALUES (1,7,2,'2022-03-24 12:48:48.563218',19,12.000,'succeeded'),(2,7,3,'2022-03-24 12:54:17.621219',20,34.000,'succeeded'),(3,7,5,'2022-03-25 04:53:59.308167',22,24.000,'succeeded'),(4,22,16,'2022-03-26 10:07:55.173674',23,4356.000,'succeeded'),(5,22,37,'2022-03-26 10:07:55.194147',23,120.000,'succeeded'),(6,22,35,'2022-03-26 10:07:55.196619',23,193.000,'succeeded'),(7,22,28,'2022-03-26 10:07:55.198968',23,0.000,'succeeded'),(8,22,36,'2022-03-26 10:16:29.359877',24,1000.000,'succeeded'),(9,22,29,'2022-03-26 10:22:32.671876',26,0.000,'succeeded'),(10,22,31,'2022-03-26 10:22:32.674363',26,100.000,'succeeded'),(11,22,36,'2022-03-26 10:25:19.791704',27,1000.000,'succeeded'),(12,22,28,'2022-03-26 10:25:19.798792',27,0.000,'succeeded'),(13,22,28,'2022-03-26 17:17:06.401545',28,0.000,'succeeded'),(14,22,16,'2022-03-26 17:17:06.421225',28,4356.000,'succeeded'),(15,22,28,'2022-03-26 18:21:15.687147',29,0.000,'succeeded'),(16,22,26,'2022-03-26 18:21:15.690268',29,123.000,'succeeded'),(17,22,16,'2022-03-26 18:21:15.693041',29,4356.000,'succeeded'),(18,22,27,'2022-03-27 19:01:17.880572',30,123.000,'succeeded'),(19,22,26,'2022-03-27 19:01:17.902172',30,123.000,'succeeded'),(20,22,19,'2022-03-27 19:01:17.904902',30,123.000,'succeeded'),(21,22,1,'2022-03-27 19:01:17.906590',30,100.000,'succeeded'),(22,22,27,'2022-03-27 19:13:30.958356',31,123.000,'succeeded'),(23,22,1,'2022-03-28 06:03:19.119888',32,100.000,'succeeded'),(24,22,16,'2022-03-28 06:03:19.154153',32,4356.000,'succeeded'),(25,22,4,'2022-03-28 15:20:24.002150',33,234.000,'succeeded'),(26,22,5,'2022-03-28 15:20:24.024207',33,24.000,'succeeded'),(27,33,4,'2022-03-30 14:36:08.105797',34,234.000,'succeeded'),(28,33,9,'2022-03-30 14:36:08.157228',34,34.000,'succeeded'),(29,33,5,'2022-03-30 14:36:08.159969',34,24.000,'succeeded'),(30,22,11,'2022-03-31 12:24:57.259162',35,45.000,'succeeded'),(31,22,4,'2022-03-31 12:24:57.290576',35,234.000,'succeeded'),(32,22,5,'2022-03-31 12:24:57.292987',35,24.000,'succeeded'),(33,22,6,'2022-03-31 12:24:57.295298',35,1.000,'succeeded'),(34,22,10,'2022-03-31 12:24:57.297551',35,675.000,'succeeded'),(35,45,6,'2022-04-11 19:33:54.368275',36,1.000,'succeeded'),(36,45,12,'2022-04-11 19:33:54.385829',36,435.000,'succeeded'),(37,45,5,'2022-04-12 08:52:11.392078',37,24.000,'succeeded'),(38,45,12,'2022-04-12 08:52:11.416256',37,435.000,'succeeded'),(39,33,5,'2022-04-12 09:05:03.382283',38,24.000,'succeeded'),(40,33,6,'2022-04-12 09:05:03.384537',38,1.000,'succeeded'),(41,33,7,'2022-04-12 09:08:04.547770',39,34.000,'succeeded');
/*!40000 ALTER TABLE `transaction_log` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-04-12 17:54:46
