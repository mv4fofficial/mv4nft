-- MySQL dump 10.13  Distrib 8.0.28, for Win64 (x86_64)
--
-- Host: 103.50.212.140    Database: mv4f
-- ------------------------------------------------------
-- Server version	5.7.27-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `notification`
--

DROP TABLE IF EXISTS `notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `notification` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `wish_list_id` bigint(20) NOT NULL,
  `seller_id` bigint(20) NOT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  `is_read` tinyint(1) NOT NULL,
  `is_seller` tinyint(1) NOT NULL,
  `created_date` datetime(6) NOT NULL,
  `updated_date` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IX_notification_product_id` (`product_id`),
  KEY `IX_notification_seller_id` (`seller_id`),
  KEY `IX_notification_user_id` (`user_id`),
  KEY `IX_notification_wish_list_id` (`wish_list_id`),
  CONSTRAINT `FK_notification_product_product_id` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  CONSTRAINT `FK_notification_user_seller_id` FOREIGN KEY (`seller_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_notification_user_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_notification_wish_list_wish_list_id` FOREIGN KEY (`wish_list_id`) REFERENCES `wish_list` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notification`
--

LOCK TABLES `notification` WRITE;
/*!40000 ALTER TABLE `notification` DISABLE KEYS */;
INSERT INTO `notification` VALUES (2,7,7,7,NULL,1,0,'2022-04-06 12:19:19.432046','2022-04-06 12:48:07.389932'),(3,23,8,7,NULL,0,0,'2022-04-11 06:29:39.680994','2022-04-11 06:29:39.726978'),(4,23,8,28,NULL,0,0,'2022-04-11 06:29:46.284896','2022-04-11 06:29:46.334484'),(5,23,9,28,NULL,0,0,'2022-04-11 06:29:46.284896','2022-04-11 06:29:46.334484'),(6,23,10,28,NULL,0,0,'2022-04-11 06:29:46.284896','2022-04-11 06:29:46.334484'),(7,22,12,7,NULL,0,0,'2022-04-11 07:55:10.916494','2022-04-11 07:55:10.916494'),(8,22,12,28,NULL,0,0,'2022-04-11 07:55:10.916505','2022-04-11 07:55:10.916505'),(10,33,20,7,NULL,0,0,'2022-04-12 09:03:14.153897','2022-04-12 09:03:14.153897'),(11,33,20,28,NULL,0,0,'2022-04-12 09:03:14.153907','2022-04-12 09:03:14.153907');
/*!40000 ALTER TABLE `notification` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-04-12 17:54:56
