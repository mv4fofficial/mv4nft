-- MySQL dump 10.13  Distrib 8.0.28, for Win64 (x86_64)
--
-- Host: 103.50.212.140    Database: mv4f
-- ------------------------------------------------------
-- Server version	5.7.27-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `product_title` longtext,
  `price` decimal(16,3) NOT NULL,
  `gas_fee` decimal(16,3) DEFAULT NULL,
  `product_image` longtext,
  `description` longtext,
  `quantity` bigint(20) DEFAULT NULL,
  `link` longtext,
  `nft_status` tinyint(1) NOT NULL,
  `seller_id` bigint(20) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `created_date` datetime(6) NOT NULL,
  `updated_date` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IX_product_seller_id` (`seller_id`),
  CONSTRAINT `FK_product_user_seller_id` FOREIGN KEY (`seller_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,'new pic 1',100.000,NULL,'\\ProductImages\\969fde82-d5b1-44cb-9e38-aedd64318b14.jpeg','.jpeg',NULL,NULL,1,7,0,'2022-03-04 22:59:03.565930','2022-03-04 22:59:03.601414'),(2,'new pic 2',12.000,NULL,'\\ProductImages\\2a9d2ba6-0d76-4298-a5d0-f059e6025537.png','demo 2',NULL,NULL,1,7,0,'2022-03-16 22:59:03.565930','2022-03-16 22:59:03.565930'),(3,'new pic 3',34.000,NULL,'\\ProductImages\\214b559c-71ab-44a4-b7ef-ee6c12256f21.png','demo 3',NULL,NULL,1,7,0,'2022-03-16 22:59:03.565930','2022-03-16 22:59:03.565930'),(4,'new pic 4',234.000,NULL,'\\ProductImages\\969fde82-d5b1-44cb-9e38-aedd64318b14.jpeg','demo 4',NULL,NULL,1,7,0,'2022-03-16 22:59:03.565930','2022-03-16 22:59:03.565930'),(5,'new pic 5',24.000,NULL,'\\ProductImages\\2a9d2ba6-0d76-4298-a5d0-f059e6025537.png','demo 5',NULL,NULL,1,7,0,'2022-03-16 22:59:03.565930','2022-03-16 22:59:03.565930'),(6,'new pic 6',1.000,NULL,'\\ProductImages\\969fde82-d5b1-44cb-9e38-aedd64318b14.jpeg','demo 6',NULL,NULL,1,7,0,'2022-03-16 22:59:03.565930','2022-03-16 22:59:03.565930'),(7,'new pic 7',34.000,NULL,'\\ProductImages\\969fde82-d5b1-44cb-9e38-aedd64318b14.jpeg','demo 07',NULL,NULL,1,7,0,'2022-03-16 22:59:03.565930','2022-03-16 22:59:03.565930'),(8,'new pic 8',23.000,NULL,'\\ProductImages\\2a9d2ba6-0d76-4298-a5d0-f059e6025537.png','demo 08',NULL,NULL,1,23,0,'2022-03-16 22:59:03.565930','2022-03-16 22:59:03.565930'),(9,'new pic 9',34.000,NULL,'\\ProductImages\\2a9d2ba6-0d76-4298-a5d0-f059e6025537.png','demo 09',NULL,NULL,1,23,0,'2022-03-16 22:59:03.565930','2022-03-16 22:59:03.565930'),(10,'new pic 10',675.000,NULL,'\\ProductImages\\969fde82-d5b1-44cb-9e38-aedd64318b14.jpeg','demo 10',NULL,NULL,1,23,0,'2022-03-16 22:59:03.565930','2022-03-16 22:59:03.565930'),(11,'new pic 11',45.000,NULL,'\\ProductImages\\214b559c-71ab-44a4-b7ef-ee6c12256f21.png','demo 11',NULL,NULL,1,23,0,'2022-03-16 22:59:03.565930','2022-03-16 22:59:03.565930'),(12,'new pic 12',435.000,NULL,'\\ProductImages\\214b559c-71ab-44a4-b7ef-ee6c12256f21.png','demo 12',NULL,NULL,1,23,0,'2022-03-16 22:59:03.565930','2022-03-16 22:59:03.565930'),(13,'new pic 13',36.000,NULL,'\\ProductImages\\06d25c1b-15f7-4943-b083-dad1d5e5677b.png','demo 13',NULL,NULL,1,23,0,'2022-03-16 22:59:03.565930','2022-03-16 22:59:03.565930'),(14,'new pic 14',234.000,NULL,'\\ProductImages\\214b559c-71ab-44a4-b7ef-ee6c12256f21.png','demo 14',NULL,NULL,1,23,0,'2022-03-16 22:59:03.565930','2022-03-16 22:59:03.565930'),(16,'update sample',4356.000,NULL,'\\ProductImages\\29c0716a-5ae7-4627-b4d9-99bcd669890a.png','string',NULL,'string',1,23,1,'2022-03-16 18:29:19.162377','2022-03-17 09:12:47.717410'),(18,'screen shot',123.000,NULL,'\\ProductImages\\0a17032d-ed76-4875-8003-a87006ad0c0b.png','screenshot for testing',NULL,NULL,1,7,0,'2022-03-24 15:39:43.564115','2022-03-24 15:39:43.606598'),(19,'screen shot',123.000,NULL,'\\ProductImages\\29c0716a-5ae7-4627-b4d9-99bcd669890a.png','screenshot for testing',NULL,NULL,1,7,0,'2022-03-24 15:43:14.247969','2022-03-24 15:43:14.300820'),(21,'screen shot1',123.000,NULL,'\\ProductImages\\a57ba54f-35a5-4088-9134-9ea1d84aedb5.png','screenshot for testing',NULL,NULL,1,7,0,'2022-03-24 15:52:23.987855','2022-03-24 15:52:23.987957'),(22,'screen shot3',123.000,NULL,'\\ProductImages\\4dda0675-b05a-4bc3-88d0-42311156749f.png','screenshot for testing',NULL,NULL,1,7,0,'2022-03-24 15:57:48.569973','2022-03-24 15:57:48.624556'),(23,'screen shot4',123.000,NULL,'\\ProductImages\\e1960b16-ef46-48a7-acbc-5201f109e0fc.png','screenshot for testing',NULL,NULL,1,7,0,'2022-03-24 16:00:18.390873','2022-03-24 16:00:18.390875'),(24,'screen shot9',123.000,NULL,'\\ProductImages\\523515e3-b250-4f83-983b-66feee3d88e9.png','screenshot for testing',NULL,NULL,1,7,0,'2022-03-24 16:03:11.756546','2022-03-24 16:03:11.756605'),(25,'screen shot8',123.000,NULL,'\\ProductImages\\72a38cd1-6a85-483d-924d-8c2f7a602c48.png','screenshot for testing',NULL,NULL,1,7,0,'2022-03-24 16:07:44.932236','2022-03-24 16:07:44.932300'),(26,'screen shot7',123.000,NULL,'\\ProductImages\\af0eb39a-d832-45b1-b591-f420669d0be1.png','screenshot for testing',NULL,NULL,1,7,0,'2022-03-24 16:10:12.611591','2022-03-24 16:10:12.611653'),(27,'screen shot10',123.000,NULL,'\\ProductImages\\06d25c1b-15f7-4943-b083-dad1d5e5677b.png','screenshot for testing',NULL,NULL,1,7,0,'2022-03-24 16:21:56.015145','2022-03-24 16:21:56.015226'),(28,'string',0.000,NULL,'\\ProductImages\\83430e55-0db6-42b6-955f-04f0870a8ca5.png','string',NULL,NULL,1,28,1,'2022-03-26 03:16:58.463286','2022-03-26 03:16:58.463288'),(29,'Test01',0.000,NULL,'\\ProductImages\\9f117ded-ac5f-4c99-8ca1-2f5ef5fc14bb.png','string',NULL,NULL,1,28,1,'2022-03-26 03:17:37.713039','2022-03-26 03:17:37.713043'),(30,'Test 03',250.000,NULL,'\\ProductImages\\061e1e93-c4e8-4ad2-9148-8e188fa71be1.png','Test Description',NULL,NULL,0,28,1,'2022-03-26 05:33:37.748925','2022-03-26 05:33:37.748927'),(31,'test 3',100.000,NULL,'\\ProductImages\\d13585a5-8621-4ecf-97b8-5dc4d839a852.png','uasgkjd',NULL,NULL,0,28,1,'2022-03-26 05:45:08.946649','2022-03-26 05:45:08.946650'),(32,'test 04',100.000,NULL,'\\ProductImages\\8b3e68c4-1381-4ff7-b843-0bf5e5753a4d.png','aystd',NULL,NULL,0,28,1,'2022-03-26 05:50:37.902799','2022-03-26 05:50:37.902800'),(33,'test 05',100.000,NULL,'\\ProductImages\\d47bfc2a-ac2b-45e2-bb41-f08bcd95ccb0.png','aystd',NULL,NULL,0,28,1,'2022-03-26 05:52:01.598874','2022-03-26 05:52:01.598875'),(34,'test 06',120.000,NULL,'\\ProductImages\\6adfa3fe-d092-440e-87b1-135ccbeba21b.png','kjasdk',NULL,NULL,0,28,1,'2022-03-26 05:54:59.528056','2022-03-26 05:54:59.528058'),(35,'Test 07',193.000,NULL,'\\ProductImages\\bb81bac8-60b4-4bf3-a125-e49d13ebd17d.png','aslkdj',NULL,NULL,0,28,1,'2022-03-26 05:56:40.005891','2022-03-26 05:56:40.005892'),(36,'Test 08',1000.000,NULL,'\\ProductImages\\c2824af2-1f48-4622-a487-64784d4ca88d.png','asdlkad',NULL,NULL,0,28,1,'2022-03-26 07:14:10.947975','2022-03-26 07:14:10.947976'),(37,'Test 09',120.000,NULL,'\\ProductImages\\74d090bf-5877-4c17-b9ba-bfc74372acca.png','Description',NULL,NULL,0,28,1,'2022-03-26 07:17:53.672447','2022-03-26 07:17:53.672449'),(38,'Test imag 01',101.000,NULL,'\\ProductImages\\2937cbe5-afae-4765-83dc-5dfdd1e44f67.png','Description - 01',NULL,NULL,0,28,1,'2022-03-28 16:51:01.157716','2022-03-28 16:51:01.157824'),(39,'Prod 01',100.000,NULL,'\\ProductImages\\45284e62-7b33-4ee9-9136-8092b122b73c.png','Description test 123',NULL,NULL,0,28,0,'2022-04-07 05:30:25.480442','2022-04-07 05:30:25.480517'),(40,'Test Prod 02',340.000,NULL,'\\ProductImages\\a6ed1c3c-981c-49a8-8553-267ef1169628.jpeg','Testing 123...',NULL,NULL,0,28,0,'2022-04-10 02:17:14.615828','2022-04-10 02:17:14.615913');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-04-12 17:54:52
